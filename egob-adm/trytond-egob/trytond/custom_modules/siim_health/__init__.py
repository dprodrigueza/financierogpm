# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import configuration
from . import hr
from . import patient_evaluation
from . import medical_certificate
from . import report
from . import patient
from . import party



__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        hr.Contract,
        hr.AssistanceConsultationDetail,
        medical_certificate.MedicalCertificate,
        medical_certificate.ConsultingMedicalCertificateTypeContext,
        medical_certificate.ConsultingMedicalCertificateType,
        medical_certificate.ConsultingMedicalCertificateAbsenceContext,
        medical_certificate.ConsultingMedicalCertificateAbsence,
        medical_certificate.MedicalCertificateType,
        medical_certificate.CustomizeGroupPayslip,
        medical_certificate.MedicalCertificateEntity,
        medical_certificate.MedicalCertificateProfessional,
        medical_certificate.MedicalCertificateRankPayslip,
        medical_certificate.MedicalCertificatePayslipContext,
        medical_certificate.MedicalCertificatePayslip,
        medical_certificate.MedicalCertificateDisease,
        medical_certificate.MedicalCertificateSendEmail,
        medical_certificate.MedicalCertificateDetailedReportWizardStart,
        patient_evaluation.PatientEvaluation,
        patient_evaluation.PatientEvaluationJobContext,
        patient_evaluation.PatientEvaluationQuery,
        patient_evaluation.PatientEvaluationQueryByDisease,
        patient_evaluation.PatientEvaluationByDiseaseContext,
        patient_evaluation.PatientEvaluationQueryByPosition,
        patient_evaluation.PatientEvaluationByPositionContext,
        patient_evaluation.PatientOcupationalHazardContext,
        patient_evaluation.PatientOcupationalHazardQuery,
        patient_evaluation.PatientEvaluationAbsenceQuery,
        patient_evaluation.PatientEvaluationAbsenceContext,
        patient_evaluation.PatientMedicalCertificateContext,
        patient_evaluation.PatientMedicalCertificateQuery,
        patient.Patient,
        party.Party,
        module='siim_health', type_='model')
    Pool.register(
        medical_certificate.MedicalCertificateDetailedReportWizard,
        module='siim_health', type_='wizard')
    Pool.register(
        report.MedicalCertificate,
        report.PatientEvaluationQueryReport,
        report.PatientEvaluationByDiseaseReport,
        report.PatientEvaluationByPositionReport,
        report.PatientOcupationalHazardReport,
        report.PatientEvaluationAbsenceReport,
        report.ConsultingMedicalCertificateTypeReport,
        report.ConsultingMedicalCertificateAbsenceReport,
        report.MedicalCertificateExcel,
        report.MedicalCertificateDetailedReportExport,
        report.PatientMedicalCertificateTypeReport,
        module='siim_health', type_='report')
