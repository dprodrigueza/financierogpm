from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval

__all__ = [
    'Configuration'
]

class Configuration(metaclass=PoolMeta):
    __name__ = 'hr_ec.configuration'

    medical_certificate_sequence = fields.MultiValue(
        fields.Many2One(
            'ir.sequence', 'Secuencia Certificado Médico',
            domain=[
                ('code', '=', 'galeno.medical.certificate'),
            ]
        )
    )
    certificate_notification_email = fields.Many2One('notification.email',
        'Configuración de notificación por correo electrónico para '
        'certificados médicos')
    notification_medical_certificate = fields.One2Many('party.party',
        'config_medical_certificate', 'Notificación de certificados médicos',
        help=('Dirección de correo electrónico, para notificar novedades de '
              'certificados médicos'))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['medical_certificate_sequence']:
            return pool.get('permission.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_medical_certificate_sequence(cls, **pattern):
        return cls.multivalue_model(
            'medical_certificate_sequence').default_medical_certificate_sequence()


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'permission.configuration.sequence'

    medical_certificate_sequence = fields.Many2One(
        'ir.sequence', "Secuencia Certificado Médico", required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'galeno.medical.certificate'),
        ],
        depends=['company'])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('medical_certificate_sequence')
        value_names.append('medical_certificate_sequence')
        super(ConfigurationSequence, cls)._migrate_property(field_names, value_names, fields)

    @classmethod
    def default_medical_certificate_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'siim_health', 'sequence_medical_certificate')
        except KeyError:
            return None