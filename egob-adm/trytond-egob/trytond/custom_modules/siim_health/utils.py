from sql.functions import Function
from unicodedata import normalize


class GenerateSeries(Function):
    __slots__ = ()
    _function = 'GENERATE_SERIES'


def normalize_text(text):
    trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
    result = normalize('NFKC', normalize('NFKD', text).translate(trans_tab))
    return result