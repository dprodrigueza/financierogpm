from trytond.pool import PoolMeta
from trytond.model import ModelSQL, ModelView, fields

__all__ = ['Party']




class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    certificate = fields.Many2One('galeno.medical.certificate',
                                  'Certificados médicos')


