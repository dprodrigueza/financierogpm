from datetime import timedelta, datetime
from trytond.pool import PoolMeta,Pool
from trytond.pyson import Eval, Bool
from trytond.model import (Workflow, ModelView, ModelSQL, fields)
from trytond.transaction import Transaction
from sql import Window, Literal, Union, Null
from sql.functions import RowNumber, CurrentTimestamp
from sql.operators import Concat
from sql.aggregate import Count, Sum
from sql import Values, With, Literal, Window, Union
from sql.conditionals import Coalesce, Case

__all__ = ['PatientEvaluation']


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'galeno.patient.evaluation'

    medical_certificate = fields.One2Many(
        'galeno.medical.certificate', 'evaluation', 'Certificado médico',
        states={
            'readonly': True,
        })
    is_medical_certificate = fields.Function(
        fields.Boolean('Hay certificados de esta evaluacion',
                       states={'invisible': True}),
        'on_change_with_is_medical_certificate'
    )

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        cls._buttons.update({
            'button_certificate': {
                'invisible': ~(Eval('reason').in_([
                    'disease', 'occupational_disease', 'accident',
                    'occupational_accident'])
                ),
                'readonly': Eval('is_medical_certificate'),
                'icon': 'galeno-evaluation',
            },
        })

    @fields.depends('medical_certificate', 'state', 'end_date')
    def on_change_with_is_medical_certificate(self, name=None):
        Date = Pool().get('ir.date')
        if self.state == 'finish' and self.end_date.date() != Date.today():
            return True
        return False


    @classmethod
    @ModelView.button_action('siim_health.act_medical_certificate_evaluation')
    def button_certificate(cls, evaluations):
        pass

    @classmethod
    def view_attributes(cls):
        return super(PatientEvaluation, cls).view_attributes() + [(
            '//page[@id="medical_certificate"]', 'states',{
                'invisible': ~(Bool(Eval('medical_certificate'))),
            }
        )]

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
               query=False):
        context = Transaction().context
        if context.get('evaluation_avaliable'):
            new_domain = cls.search_evaluation_avaliable(
                context.get('patient_evaluate'))
            if new_domain:
                domain += new_domain
        return super(PatientEvaluation, cls).search(
            domain, offset=offset, limit=limit, order=order, count=count,
            query=query)

    @classmethod
    def search_evaluation_avaliable(cls, patient):
        new_domain = []
        pool = Pool()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        Date = pool.get('ir.date')
        m_c = MedicalCertificate.search_read([
            ('evaluation', '!=', None),
            ('register_type', '=', 'days'),
            ('state', '!=', 'cancel'),
        ], fields_names=['evaluation'])
        evaluation_ids = [row['evaluation'] for row in m_c]
        new_domain += [
            ('patient', '=', patient),
            ('id', 'not in', evaluation_ids),
            ['OR',
             ('state', '=', 'initial'),
             [
                 ('state', '=', 'finish'),
                 ('end_date', '>=', f"{str(Date.today())} 00:00:00"),
                 ('end_date', '<=', f"{str(Date.today())} 23:59:59"),
              ],
             ]
        ]
        return new_domain


class PatientEvaluationJobContext(ModelView):
    'Patient Evaluation Job Context'
    __name__ = 'galeno.patient.job.evaluation.context'

    start_date = fields.DateTime('Fecha Inicio')
    end_date = fields.DateTime('Fecha Fin')
    reason = fields.Selection(
        [
            ('disease', 'Enfermedad Común'),
            ('occupational_disease', 'Enfermedad Profesional'),
            ('accident', 'Accidente Común'),
            ('occupational_accident', 'Accidente Laboral'),
            ('tracing', 'Seguimiento'),
            ('routine', 'Exploración de rutina'),
            ('occupational_record_init', 'Ficha Ocupacional - Inicio'),
            ('occupational_record_periodic', 'Ficha Ocupacional - Periódica'),
            ('occupational_record_end', 'Ficha Ocupacional - Final'),
            ('occupational_record_relocation',
             'Ficha Ocupacional - Reubicación'),
            ('occupational_record_re-entry', 'Ficha Ocupacional - Reingreso'),
        ], 'Razón',)
    disease = fields.One2Many('galeno.disease',None, 'Disease')
    position = fields.One2Many('company.position',None, 'Puesto de trabajo')
    employees = fields.One2Many('company.employee', None,'Empleados')
    injury_type = fields.Selection(
        [
            ('', ''),
            ('slight', 'Leve'),
            ('severe', 'Grave'),
            ('mortal', 'Mortal'),
        ], 'Tipo de lesión',
        states={
            'invisible': ~(Eval('reason').in_([
                'accident', 'occupational_accident'])),
        }, depends=['reason'])
    body_part = fields.One2Many(
        'galeno.patient.evaluation.body_part', None, 'Área del cuerpo',
        states={
            'invisible': ~(Eval('reason').in_([
                'accident', 'occupational_accident'])),
        }, depends=['reason'])

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()


class PatientEvaluationQuery(ModelSQL, ModelView):
    'Patient Evaluation Query'
    __name__ = 'galeno.patient.evaluation.query'
    number = fields.Integer('Número')
    sentence = fields.Char('Sentencia', readonly=True)
    evaluation = fields.Many2One('galeno.patient.evaluation', 'Evaluación')
    patient = fields.Many2One('galeno.patient', 'Paciente')
    start_date = fields.Date('Fecha inicio', readonly=True)
    end_date = fields.Date('Fecha Fin',readonly=True)
    reason = fields.Selection(
        [
            ('disease', 'Enfermedad Común'),
            ('occupational_disease', 'Enfermedad Profesional'),
            ('accident', 'Accidente Común'),
            ('occupational_accident', 'Accidente Laboral'),
            ('tracing', 'Seguimiento'),
            ('routine', 'Exploración de rutina'),
            ('occupational_record_init', 'Ficha Ocupacional - Inicio'),
            ('occupational_record_periodic', 'Ficha Ocupacional - Periódica'),
            ('occupational_record_end', 'Ficha Ocupacional - Final'),
            ('occupational_record_relocation',
             'Ficha Ocupacional - Reubicación'),
            ('occupational_record_re-entry', 'Ficha Ocupacional - Reingreso'),
        ], 'Razón',states={'readonly': True})
    reason_translated = reason.translated('reason')
    employee = fields.Many2One('company.employee', 'Empleado')
    contract_position = fields.Function(
        fields.Many2One('company.position', 'Puesto de trabajo'),
        'on_change_with_contract_position')
    diagnostics = fields.Many2One('galeno.patient.evaluation.diagnosis',
                                  'Diagnóstico')
    disease = fields.Many2One('galeno.disease', 'Enfermedad')

    @fields.depends('employee')
    def on_change_with_contract_position(self, name=None):
        if self.employee and self.employee.contract_position:
            return self.employee.contract_position.id
        return None

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Conract = pool.get('company.contract')
        contract= Conract.__table__()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Evaluation = pool.get('galeno.patient.evaluation')
        evaluation = Evaluation.__table__()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        certificate = MedicalCertificate.__table__()
        MedicalCertificateType = pool.get('galeno.medical.certificate.type')
        certificatetype = MedicalCertificateType.__table__()
        Diagnosis = pool.get('galeno.patient.evaluation.diagnosis')
        diagnostics = Diagnosis.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()

        where = Literal(True)
        sentence = Concat('', '')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat(
                'DESDE: ', context.get('start_date')))
            where &= (evaluation.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                    Concat('  HASTA: ', context.get('end_date')))
            where &= (evaluation.end_date <= context.get('end_date'))
        if context.get('reason'):
            if evaluation.reason =='disease':
                sentence = Concat(sentence,'____ Razón: Enfermedad Comun')
            elif evaluation.reason=='occupational_disease':
                sentence = Concat(sentence,'____ Razón: Enfermedad Profesional')
            elif evaluation.reason =='accident':
                sentence = Concat(sentence,'____ Razón: Accidente Común')
            where &= (evaluation.reason == context.get('reason'))

        if context.get('injury_type'):
            if evaluation.injury_type =='slight':
                sentence = Concat(sentence,' Tipo de lesión: Leve')
            elif evaluation.injury_type=='severe':
                sentence = Concat(sentence,' Tipo de lesión: Grave')
            elif evaluation.injury_type =='accident':
                sentence = Concat(sentence,' Tipo de lesión: Mortal')
            where &= (evaluation.injury_type == context.get('injury_type'))

        if context.get('body_part'):
            ids = []
            for data in context.get('body_part'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' Área afectada: ')
            # Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (evaluation.body_part.in_(ids))

        if context.get('disease'):
            ids = []
            for data in context.get('disease'):
                ids.append(data['id'])
            sentence = Concat(sentence,' _Enfermendad: ')
            #Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (diagnostics.disease.in_(ids)
                      )
        if context.get('position'):
            ids = []
            for data in context.get('position'):
                ids.append(data['id'])
            sentence = Concat(sentence,' _ Puesto de trabajo: ')
            #Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (contract.position.in_(ids))

        if context.get('employees'):
            ids = []
            for data in context.get('employees'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' _ Empleado: ')
            where &= (employee.id.in_(ids))

        query = patient.join(employee, condition=patient.employee == employee.id
        ).join(contract, condition=employee.id == contract.employee
        ).join( evaluation, condition=evaluation.patient == patient.id
        ).join(diagnostics, condition=evaluation.id == diagnostics.evaluation
        ).join(disease, condition= diagnostics.disease == disease.id
        ).select(
            sentence.as_('sentence'),
            evaluation.id.as_('evaluation'),
            evaluation.patient.as_('patient'),
            evaluation.reason.as_('reason'),
            evaluation.start_date.as_('start_date'),
            evaluation.end_date.as_('end_date'),
            employee.id.as_('employee'),
            diagnostics.id.as_('diagnostics'),
            diagnostics.disease.as_('disease'),
            Count(disease.id).as_('number'),

            where=where,
            group_by=[
                sentence,
                evaluation.id,
                evaluation.patient,
                evaluation.reason,
                evaluation.start_date,
                evaluation.end_date,
                employee.id,
                diagnostics.disease,
                diagnostics.id,
                disease.id,
            ]
        )

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.sentence,
            query.evaluation,
            query.patient,
            query.reason,
            query.start_date,
            query.end_date,
            query.employee,
            query.disease,
            query.diagnostics,
            query.number,

        )

    @classmethod
    def __setup__(cls):
        super(PatientEvaluationQuery, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]


class PatientEvaluationByDiseaseContext(ModelView):
    'Patient Evaluation By Disease Context'
    __name__ = 'galeno.patient.evaluation.by.disease.context'

    start_date = fields.DateTime('Fecha Inicio')
    end_date = fields.DateTime('Fecha Fin')
    reason = fields.Selection(
        [
            ('disease', 'Enfermedad Común'),
            ('occupational_disease', 'Enfermedad Profesional'),
            ('accident', 'Accidente Común'),
            ('occupational_accident', 'Accidente Laboral'),
            ('tracing', 'Seguimiento'),
            ('routine', 'Exploración de rutina'),
            ('occupational_record_init', 'Ficha Ocupacional - Inicio'),
            ('occupational_record_periodic', 'Ficha Ocupacional - Periódica'),
            ('occupational_record_end', 'Ficha Ocupacional - Final'),
            ('occupational_record_relocation',
             'Ficha Ocupacional - Reubicación'),
            ('occupational_record_re-entry', 'Ficha Ocupacional - Reingreso'),
        ], 'Razón',)
    disease = fields.One2Many('galeno.disease',None, 'Disease')
    limit_disease = fields.Boolean('Límite ?')
    limit_disease_number = fields.Numeric('Total a mostrar', states={
        'invisible': ~Bool(Eval('limit_disease')),
        'required': Bool(Eval('limit_disease')),
    })

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_limit_disease_number():
        return 10


class PatientEvaluationQueryByDisease(ModelSQL, ModelView):
    'Patient Evaluation By Disease'
    __name__ = 'galeno.patient.evaluation.query.by.disease'
    number = fields.Integer('Número')
    sentence = fields.Char('Sentencia', readonly=True)
    reason = fields.Selection(
        [
            ('disease', 'Enfermedad Común'),
            ('occupational_disease', 'Enfermedad Profesional'),
            ('accident', 'Accidente Común'),
            ('occupational_accident', 'Accidente Laboral'),
            ('tracing', 'Seguimiento'),
            ('routine', 'Exploración de rutina'),
            ('occupational_record_init', 'Ficha Ocupacional - Inicio'),
            ('occupational_record_periodic', 'Ficha Ocupacional - Periódica'),
            ('occupational_record_end', 'Ficha Ocupacional - Final'),
            ('occupational_record_relocation',
             'Ficha Ocupacional - Reubicación'),
            ('occupational_record_re-entry', 'Ficha Ocupacional - Reingreso'),
        ], 'Razón', states={'readonly': True})
    reason_translated = reason.translated('reason')
    disease = fields.Many2One('galeno.disease', 'Enfermedad')



    @classmethod
    def __setup__(cls):
        super(PatientEvaluationQueryByDisease, cls).__setup__()
        cls._order = [
            ('number', 'DESC'),
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Conract = pool.get('company.contract')
        contract = Conract.__table__()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Evaluation = pool.get('galeno.patient.evaluation')
        evaluation = Evaluation.__table__()
        Diagnosis = pool.get('galeno.patient.evaluation.diagnosis')
        diagnostics = Diagnosis.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()


        MedicalCertificate = pool.get('galeno.medical.certificate')
        certificate = MedicalCertificate.__table__()
        MedicalCertificateType = pool.get('galeno.medical.certificate.type')
        certificate_type = MedicalCertificateType.__table__()

        CertificateDisease = pool.get('galeno.medical.certificate.disease')
        certificate_disease = CertificateDisease.__table__()

        total_limit = 999
        if context.get('limit_disease'):
            total_limit = int(context.get('limit_disease_number'))

        where = Literal(True)
        sentence = Concat('', '')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat(
                'DESDE: ', context.get('start_date')))
            where &= (evaluation.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                              Concat('  HASTA: ', context.get('end_date')))
            where &= (evaluation.end_date <= context.get('end_date'))
        if context.get('reason'):
            if evaluation.reason == 'disease':
                sentence = Concat(sentence, '____ Razón: Enfermedad Comun')
            elif evaluation.reason == 'occupational_disease':
                sentence = Concat(sentence,
                                  '____ Razón: Enfermedad Profesional')
            elif evaluation.reason == 'accident':
                sentence = Concat(sentence, '____ Razón: Accidente Común')
            where &= (evaluation.reason == context.get('reason'))
        if context.get('disease'):
            ids = []
            for data in context.get('disease'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' _Enfermendad: ')
            # Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (diagnostics.disease.in_(ids))

        query = patient.join(employee, condition=patient.employee == employee.id
            ).join(contract,
            condition=employee.id == contract.employee
            ).join(evaluation,
            condition=evaluation.patient == patient.id
            ).join(diagnostics,
            condition=evaluation.id == diagnostics.evaluation
            ).join(disease,
             condition=diagnostics.disease == disease.id
             ).select(
            sentence.as_('sentence'),
            evaluation.reason.as_('reason'),
            Count(diagnostics.disease).as_('number'),
            diagnostics.disease.as_('disease'),
            where=where,
            group_by=[
                sentence,
                evaluation.reason,
                diagnostics.disease,
                disease.id,
            ],
            limit=total_limit,
            order_by=[Count(diagnostics.disease).desc]
        )

        query_offset = patient.join(employee,
            condition=patient.employee == employee.id
            ).join(contract,
            condition=employee.id == contract.employee
            ).join(evaluation,
            condition=evaluation.patient == patient.id
            ).join(diagnostics,
            condition=evaluation.id == diagnostics.evaluation
            ).join(disease,
            condition=diagnostics.disease == disease.id
            ).select(
            sentence.as_('sentence'),
            evaluation.reason.as_('reason'),
            Count(diagnostics.disease).as_('number'),
            diagnostics.disease.as_('disease'),
            where=where,
            group_by=[
                sentence,
                evaluation.reason,
                diagnostics.disease,
                disease.id,
            ],
            offset=total_limit,
            order_by=[Count(diagnostics.disease).desc]

        )

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.sentence,
            query.reason,
            query.disease,
            query.number,
        )

    @classmethod
    def __setup__(cls):
        super(PatientEvaluationQueryByDisease, cls).__setup__()
        cls._order = [
            ('number', 'DESC')
        ]


class PatientEvaluationByPositionContext(ModelView):
    'Patient Evaluation By Position Context'
    __name__ = 'galeno.patient.evaluation.by.position.context'

    start_date = fields.DateTime('Fecha Inicio')
    end_date = fields.DateTime('Fecha Fin')
    position = fields.One2Many('company.position', None, 'Puesto de trabajo')
    disease = fields.One2Many('galeno.disease', None, 'Disease')
    limit_disease = fields.Boolean('Límite ?')
    limit_disease_number = fields.Numeric('Total a mostrar', states={
        'invisible': ~Bool(Eval('limit_disease')),
        'required': Bool(Eval('limit_disease')),
    })

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_limit_disease_number():
        return 10


class PatientEvaluationQueryByPosition(ModelSQL, ModelView):
    'Patient Evaluation Query By Position'
    __name__ = 'galeno.patient.evaluation.query.by.position'
    number = fields.Integer('Número Enfermos')
    sentence = fields.Char('Sentencia', readonly=True)
    position = fields.Many2One('company.position', 'Puesto de Trabajo')
    disease = fields.Many2One('galeno.disease', 'Enfermedad')

    @classmethod
    def __setup__(cls):
        super(PatientEvaluationQueryByPosition, cls).__setup__()
        cls._order = [
            ('number', 'DESC'),
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()

        Contract = pool.get('company.contract')
        contract = Contract.__table__()
        Position = pool.get('company.position')
        position = Contract.__table__()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Evaluation = pool.get('galeno.patient.evaluation')
        evaluation = Evaluation.__table__()
        Diagnosis = pool.get('galeno.patient.evaluation.diagnosis')
        diagnostics = Diagnosis.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()

        total_limit = 999
        if context.get('limit_disease'):
            total_limit = int(context.get('limit_disease_number'))

        where = Literal(True)
        sentence = Concat('', '')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat(
                'DESDE: ', context.get('start_date')))
            where &= (evaluation.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                              Concat('  HASTA: ', context.get('end_date')))
            where &= (evaluation.end_date <= context.get('end_date'))

        if context.get('position'):
            ids = []
            for data in context.get('position'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' _Puesto: ')
            # Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (contract.position.in_(ids))

        query_temp = position.join(
            contract, condition=contract.position==position.id
        ).join(employee, condition=contract.employee==employee.id
        ).join(patient, condition=patient.employee==employee.id
        ).join(evaluation,condition=evaluation.patient==patient.id
        ).join(diagnostics, condition=diagnostics.evaluation==evaluation.id
        ).join(disease, condition=diagnostics.disease==disease.id
        ).select(
            sentence.as_('sentence'),
            contract.position.as_('position'),
            disease.id.as_('disease'),
            Count(diagnostics.evaluation).as_('number'),
            where=where,

        group_by = [
            sentence,
            contract.position,
            disease.id
        ],
        limit=total_limit,
        )

        return query_temp.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_temp.sentence,
            query_temp.position,
            query_temp.disease,
            query_temp.number,

        )


class PatientOcupationalHazardContext(ModelView):
    'Patient Ocupational Hazard Context'
    __name__ = 'galeno.patient.ocupational.hazard.context'

    position = fields.One2Many('company.position', None, 'Puesto de trabajo',
                               )


class PatientOcupationalHazardQuery(ModelSQL, ModelView):
    'Patient Ocupational Hazard Query'
    __name__ = 'galeno.patient.ocupational.hazard.query'

    position = fields.Many2One('company.position', 'Puesto de Trabajo')
    ocupational_hazard = fields.Many2One(
    'galeno.disease.professional.company.position','Enfermedades Profecionales')
    disease = fields.Many2One('galeno.disease.professional', 'Enfermedad')

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()

        Position = pool.get('company.position')
        position = Position.__table__()
        DiseaseProfessional = pool.get(
        'galeno.disease.professional.company.position')
        disease_professional = DiseaseProfessional.__table__()
        Disease = pool.get('galeno.disease.professional')
        disease= Disease.__table__()

        where = Literal(True)
        if context.get('position'):
            ids = []
            for data in context.get('position'):
                ids.append(data['id'])
            where &= (position.id.in_(ids))

        query_temp = position.join(
            disease_professional,
            condition= position.id == disease_professional.position
        ).join(disease,
            condition=disease.id == disease_professional.disease_professional
        ).select(
            position.id.as_('position'),
            disease_professional.disease_professional.as_('ocupational_hazard'),
            disease.id.as_('disease'),
            where = where

        )
        return query_temp.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_temp.position,
            query_temp.ocupational_hazard,
            query_temp.disease,
        )


class PatientEvaluationAbsenceContext(ModelView):
    'Patient Evaluation Absence Context'
    __name__ = 'galeno.patient.evaluation.absence.context'

    start_date = fields.DateTime('Fecha Inicio')
    end_date = fields.DateTime('Fecha Fin')
    reason = fields.One2Many('galeno.medical.certificate.type',
                           None,'Razón')
    disease = fields.One2Many('galeno.disease', None, 'Disease')
    #position = fields.One2Many('company.position', None, 'Puesto de trabajo')
    employees = fields.One2Many('company.employee', None, 'Empleados')
    limit = fields.Numeric('Limite')

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_limit():
        return 10

class PatientEvaluationAbsenceQuery(ModelSQL, ModelView):
    'Patient Evaluation Absence Query'
    __name__ = 'galeno.patient.evaluation.absence.query'
    #number = fields.Integer('Número')
    sentence = fields.Char('Sentencia', readonly=True)
    certificate = fields.Many2One('galeno.medical.certificate','Certificado')
    start_date = fields.Date('Fecha inicio', readonly=True)
    end_date = fields.Date('Fecha Fin', readonly=True)
    reason = fields.Many2One('galeno.medical.certificate.type',
       'Razón')
    total_days = fields.Function(fields.Integer('Total Días'), 'on_change_with_total_days')
    employee = fields.Many2One('company.employee', 'Empleado')
    # evaluation = fields.Many2One('galeno.patient.evaluation', 'Evaluación')
    # patient = fields.Many2One('galeno.patient', 'Paciente')
    # contract_position = fields.Function(
    #     fields.Many2One('company.position', 'Puesto de trabajo'),
    #     'on_change_with_contract_position')
    # diagnostics = fields.Many2One('galeno.patient.evaluation.diagnosis',
    #                               'Diagnóstico')
    disease = fields.Many2One('galeno.disease', 'Enfermedad')

    @fields.depends('employee')
    def on_change_with_contract_position(self, name=None):
        if self.employee and self.employee.contract_position:
            return self.employee.contract_position.id
        return None

    @fields.depends('start_date', 'end_date', 'register_type')
    def on_change_with_total_days(self, name=None):
        if self.start_date and self.end_date:
            days = ((self.end_date + timedelta(days=1)) - self.start_date).days
            return days
        return None

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Conract = pool.get('company.contract')
        contract= Conract.__table__()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Evaluation = pool.get('galeno.patient.evaluation')
        evaluation = Evaluation.__table__()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        certificate = MedicalCertificate.__table__()
        MedicalCertificateType = pool.get('galeno.medical.certificate.type')
        certificatetype = MedicalCertificateType.__table__()
        Diagnosis = pool.get('galeno.patient.evaluation.diagnosis')
        diagnostics = Diagnosis.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()
        CertificateDisease= pool.get('galeno.medical.certificate.disease')
        certificate_disease = CertificateDisease.__table__()

        where = Literal(True)
        sentence = Concat('', '')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat(
                'DESDE: ', context.get('start_date')))
            where &= (certificate.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                    Concat('  HASTA: ', context.get('end_date')))
            where &= (certificate.end_date <= context.get('end_date'))

        if context.get('reason'):
            ids = []
            for data in context.get('reason'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' Tipo: ')
            # Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (certificate.certificate_type.in_(ids))

        if context.get('disease'):
            ids = []
            for data in context.get('disease'):
                ids.append(data['id'])
            sentence = Concat(sentence,' _Enfermendad: ')
            #Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (disease.id.in_(ids)
                      )
        if context.get('position'):
            ids = []
            for data in context.get('position'):
                ids.append(data['id'])
            sentence = Concat(sentence,' _ Puesto de trabajo: ')
            #Concat(disease.code, Concat(" ",Concat(disease.name)))))
            where &= (contract.position.in_(ids))

        if context.get('employees'):
            ids = []
            for data in context.get('employees'):
                ids.append(data['id'])
            sentence = Concat(sentence, ' _ Empleado: ')
            where &= (employee.id.in_(ids))

        if context.get('limit'):
            limit = int(context.get('limit'))

        query = patient.join(employee, condition=patient.employee == employee.id
        ).join(contract, condition=employee.id == contract.employee
        ).join( certificate, condition=certificate.patient == patient.id
        #).join(diagnostics, condition=evaluation.id == diagnostics.evaluation
        ).join(certificate_disease,
            condition=certificate.id == certificate_disease.medical_certificate
        ).join(disease, condition=certificate_disease.disease == disease.id
        ).select(
            sentence.as_('sentence'),
            certificate.id.as_('certificate'),
            certificate.employee.as_('employee'),
            certificate.certificate_type.as_('reason'),
            certificate.start_date.as_('start_date'),
            certificate.end_date.as_('end_date'),
            disease.id.as_('disease'),
            where=where,

        group_by = [
            certificate.id,
            certificate.employee,
            certificate.certificate_type,
            disease.id
        ],

        limit=limit,

        )

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.sentence,
            query.certificate,
            query.employee,
            query.reason,
            query.start_date,
            query.end_date,
            query.disease,

        )

    @classmethod
    def __setup__(cls):
        super(PatientEvaluationAbsenceQuery, cls).__setup__()
        cls._order = [
            ('employee', 'ASC')
        ]


class PatientMedicalCertificateContext(ModelView):
    'PatientMedicalCertificateContext'
    __name__ = 'patient.medical.certificate.context'

    start_date = fields.Date('Fecha Inicio', states={
        'required': True,
    })
    end_date = fields.Date('Fecha Fin', states={
        'required': True,
    })
    certificate_type = fields.Many2One('galeno.medical.certificate.type',
        'Tipo certificado')
    disease = fields.One2Many('galeno.disease', None, 'Enfermedad')
    limit_disease = fields.Boolean('Límite ?')
    limit_disease_number = fields.Numeric('Total a mostrar', states={
        'invisible': ~Bool(Eval('limit_disease')),
        'required': Bool(Eval('limit_disease')),
    })
    kind = fields.Selection([
        ('particular', 'Particular'),
        ('iess', 'IESS'),
        ('internal', 'Interno'),
        ('msp', 'Ministerio de Salud Pública'),
    ], 'Origen', )
    kind_translated = kind.translated('kind')
    employees = fields.One2Many('company.employee', None, 'Empleados')
    position = fields.One2Many('company.position',
        None, 'Puesto de trabajo',)

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_limit_disease_number():
        return 10


class PatientMedicalCertificateQuery(ModelSQL, ModelView):
    'PatientMedicalCertificateQuery'
    __name__ = 'patient.medical.certificate.query'

    origin = fields.Char('Origen')
    certificate_type = fields.Char('Tipo certificado')
    disease = fields.Char('Código enfermedad')
    position = fields.Char('Puesto de trabajo')
    number = fields.Integer('Cantidad')


    @classmethod
    def __setup__(cls):
        super(PatientMedicalCertificateQuery, cls).__setup__()
        cls._order = [
            ('number', 'DESC'),
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        certificate = MedicalCertificate.__table__()
        MedicalCertificateType = pool.get('galeno.medical.certificate.type')
        certificate_type = MedicalCertificateType.__table__()

        CertificateDisease = pool.get('galeno.medical.certificate.disease')
        certificate_disease = CertificateDisease.__table__()
        Conract = pool.get('company.contract')
        contract = Conract.__table__()
        Position = pool.get('company.position')
        position = Position.__table__()

        total_limit = 999
        if context.get('limit_disease'):
            total_limit = int(context.get('limit_disease_number'))

        where = Literal(True)
        if context.get('start_date'):
            where &= (certificate.start_date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (certificate.end_date <= context.get('end_date'))
        if context.get('kind'):
            where &= (certificate.kind == context.get(
                'kind'))
        if context.get('certificate_type'):
            where &= (certificate.certificate_type == context.get('certificate_type'))
        if context.get('disease'):
            ids = []
            for data in context.get('disease'):
                ids.append(data['id'])
            where &= (disease.id.in_(ids))

        if context.get('position'):
            ids = []
            for data in context.get('position'):
                ids.append(data['id'])
            where &= (position.id.in_(ids))

        if context.get('employees'):
            ids = []
            for data in context.get('employees'):
                ids.append(data['id'])
            where &= (employee.id.in_(ids))


        query_limit = position.join(
            contract, condition=contract.position == position.id
            ).join(employee, condition=contract.employee == employee.id
            ).join(patient, condition=patient.employee == employee.id
            ).join(certificate,
                   condition=certificate.employee == employee.id
            ).join(certificate_type,
                   condition=certificate.certificate_type == certificate_type.id
            ).join(certificate_disease,
                   condition=certificate.id == certificate_disease.medical_certificate
            ).join(disease,
                   condition=(certificate_disease.disease == disease.id)
            ).select(
                position.name.as_('position'),
                certificate.kind.as_('origin'),
                certificate_type.name.as_('certificate_type'),
                Count(certificate_disease.disease).as_('number'),
                disease.code.as_('disease'),
            where=where,

            group_by=[
                position.name,
                certificate.kind,
                certificate_type.name,
                certificate_disease.disease,
                disease.code,
            ],
            limit=total_limit,
            order_by=[Count(certificate_disease.disease).desc]
        )

        query_offset = position.join(
            contract, condition=contract.position == position.id
            ).join(employee, condition=contract.employee == employee.id
            ).join(patient, condition=patient.employee == employee.id
            ).join(certificate,
                   condition=certificate.employee == employee.id
            ).join(certificate_type,
                   condition=certificate.certificate_type == certificate_type.id
            ).join(certificate_disease,
                   condition=certificate.id == certificate_disease.medical_certificate
            ).join(disease,
                   condition=(certificate_disease.disease == disease.id)
            ).select(
                position.name.as_('position'),
                certificate.kind.as_('origin'),
                certificate_type.name.as_('certificate_type'),
                Count(certificate_disease.disease).as_('number'),
                disease.code.as_('disease'),
            where=where,

            group_by=[
                position.name,
                certificate.kind,
                certificate_type.name,
                certificate_disease.disease,
                disease.code,
            ],
            offset=total_limit,
            order_by=[Count(certificate_disease.disease).desc]
        )

        query_1 = query_limit.select(
            query_limit.origin,
            query_limit.certificate_type,
            query_limit.disease,
            query_limit.position,
            query_limit.number,
        )
        query_2 = query_offset.select(
            Literal('Otros').as_('origin'),
            Literal('Otros').as_('certificate_type'),
            Literal('Otros').as_('position'),
            Literal('Otros').as_('disease'),
            Coalesce(Sum(query_offset.number), 0),
        )
        query = Union(query_1, query_2, all_=True)

        query_result = query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.origin,
            query.certificate_type,
            query.disease,
            query.position,
            query.number,
        )
        return query_result




