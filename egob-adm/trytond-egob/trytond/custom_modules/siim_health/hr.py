from decimal import Decimal
from dateutil.relativedelta import relativedelta
from sql import Literal, Union, Window, Null
from sql.functions import ToNumber, ToChar, CurrentTimestamp, RowNumber, Lower
from sql.operators import NotIn
from trytond.transaction import Transaction
from trytond.model import fields, ModelSQL, ModelView, Unique
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from .utils import GenerateSeries


__all__ = [
    'Contract', 'AssistanceConsultationDetail',
]


class Contract(metaclass=PoolMeta):
    __name__ = 'company.contract'
    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']

    medical_benefit = fields.Boolean(
        'Ha venido percibiendo derechos a subsidio por enfermedad?',
        help='Si el check está desactivado, significa que el servidor, que '
             'viene de otra institución pública, no ha percibido en su trabajo '
             'anterior derechos a subsidio por enfermedad, de manera que para '
             'comenzar a gozar de esos derechos en su trabajo actual es '
             'necesario que transcurran una cantidad mínima de meses. La '
             'cantidad mínima depende del tipo de certificado médico '
             '(enfermedad, accidente, maternidad), esa configuración se '
             'encuentra en el formulario Rangos de pago de los certificados '
             'médicos.\n\nSi el check está activado, significa que el servidor '
             'viene del sector público a trabajar en la institución pero en su '
             'trabajo anterior ya percibía derecho a subsidio por enfermedad, '
             'de manera que en su trabajo actual debe continuar haciéndolo de '
             'forma inmediata sin tener que esperar un número mínimo meses.',
        states=_states, depends=_depends)
    number_month_medical_benefit = fields.Function(
        fields.Integer('Número de meses para subsidio médico',
        help='Campo que se calcula de forma automática, muestra un valor '
             'entero que representa al número de meses transcurridos desde que '
             'inició el contrato del servidor hasta la fecha actual (o hasta '
             'la fecha fin de contrato en caso de que haya finalizado), esto '
             'con la finalidad de validar el número mínimo de meses que debe '
             'cumplir para acceder a beneficios médicos en caso de que no haya '
             'venido percibiendo dichos beneficios en su trabajo anterior.'),
        'on_change_with_number_month_medical_benefit'
    )

    @staticmethod
    def default_medical_benefit():
        return False

    @fields.depends('start_date', 'end_date')
    def on_change_with_number_month_medical_benefit(self, name=None):
        if self.start_date:
            pool = Pool()
            Date = pool.get('ir.date')
            start_date = self.start_date
            end_date = self.end_date
            if not end_date:
                end_date = Date.today()
            return relativedelta(
                (end_date + relativedelta(days=1)), start_date).months + (
                    relativedelta(end_date, start_date).years * 12)
        return None


class AssistanceConsultationDetail(metaclass=PoolMeta):
    __name__ = 'company.assistance.consultation.detail'

    @classmethod
    def table_query(cls):
        context = Transaction().context
        pool = Pool()
        DialingRegisterConsulting = pool.get(
            'company.dialing.register.consulting')
        query_c = DialingRegisterConsulting.table_query()

        Absence = pool.get('hr_ec.absence')
        absence = Absence.__table__()
        Permission = pool.get('hr_ec.permission')
        permission = Permission.__table__()
        Workshift = pool.get('company.workshift')
        workshift = Workshift.__table__()
        WorkshiftWorkingDay = pool.get('company.workshift.workingday')
        workshift_working_day = WorkshiftWorkingDay.__table__()
        WorkshiftTimetable = pool.get('company.workshift.workingday.timetable')
        workshift_timetable = WorkshiftTimetable.__table__()
        TypeDialing = pool.get('company.workshift.type.dialing')
        type_dialing = TypeDialing.__table__()
        MedicalCertificate = Pool().get('galeno.medical.certificate')
        medical_cer = MedicalCertificate.__table__()
        Holidays = pool.get('hr_ec.holiday')

        intervals = TypeDialing.search(['kind', '=', 'entry'])

        interval = intervals[0].dialing_evaluation_interval

        where_absence = Literal(True)
        where_permission = Literal(True)
        where_medical_c = Literal(True)
        if context.get('start_date'):
            where_absence &= (absence.start_date >= context.get('start_date'))
            where_permission &= (
                    permission.start_date >= context.get('start_date'))
            where_medical_c &= (
                        medical_cer.start_date >= context.get('start_date'))

        if context.get('end_date'):
            where_absence &= (absence.end_date <= context.get('end_date'))
            where_permission &= (permission.end_date <= context.get('end_date'))
            where_medical_c &= (medical_cer.end_date <= context.get('end_date'))

        if context.get('employee'):
            where_absence &= (absence.employee == context.get('employee'))
            where_permission &= (permission.employee == context.get('employee'))
            where_medical_c &= (medical_cer.employee == context.get('employee'))

        holidays = []
        for holiday in Holidays.search([
            ('company', '=', context.get('company'))]):
            holidays.append(holiday.date_discount)
        if len(holidays) == 0:
            holidays.append(None)

        query_absence = absence.select(
            GenerateSeries(
                absence.start_date, absence.end_date, '1 day').as_('date'),
            absence.employee,
            where=((absence.state == 'done') &
                   (absence.kind == 'days') & where_absence)
        )

        query_permission = permission.select(
            GenerateSeries(
                permission.start_date, permission.end_date, '1 day').as_(
                'date'),
            permission.employee,
            where=((permission.state == 'done') &
                   (permission.kind == 'days') & where_permission)
        )

        query_medical_c = medical_cer.select(
            GenerateSeries(
                medical_cer.start_date, medical_cer.end_date, '1 day').as_(
                'date'),
            medical_cer.employee,
            where=((medical_cer.state == 'done') &
                   (medical_cer.register_type == 'days') & where_medical_c)
        )

        query_absence_permission = Union(
            query_absence, query_permission, query_medical_c)

        number_day = ToNumber(ToChar(query_c.date, 'D'), '99999')

        query_all = query_c.join(query_absence_permission, type_='LEFT',
             condition=(query_c.employee == query_absence_permission.employee)
                       & (query_c.date == query_absence_permission.date)
        ).join(workshift, type_='LEFT',
            condition=(workshift.id == query_c.workshift)
        ).join(workshift_working_day, type_='LEFT',
            condition=(workshift_working_day.workshift == workshift.id) &
            (number_day == (ToNumber(workshift_working_day.day, '99999') + 1))
        ).join(workshift_timetable, type_='LEFT',
            condition=(workshift_timetable.workingday == workshift_working_day.id)
        ).join(type_dialing, type_='LEFT',
            condition=(
                type_dialing.id == workshift_timetable.type_dialing)
        ).select(
            query_c.id,
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_c.company,
            query_c.employee,
            query_c.dialing_date,
            query_c.date,
            query_c.hour,
            query_c.workshift,
            query_c.nocturne_surcharge,
            RowNumber(window=Window([
                query_c.company, query_c.employee, query_c.date
            ], order_by=[
                query_c.company, query_c.employee, query_c.date, query_c.hour
            ]
            )).as_('row_number'),
            where=((query_absence_permission.employee == Null) &
                   (NotIn(query_c.date, holidays)) &
                   (((workshift_timetable.hour - interval) <= query_c.hour) &
                    ((workshift_timetable.hour + interval) >= query_c.hour)) &
                   ((Lower(type_dialing.name) == 'entrada') |
                    (Lower(type_dialing.name) == 'ingreso'))) |
                  ((query_absence_permission.employee == Null) &
                   (query_c.workshift == Null)))

        # TODO: use type_dialing.code instead hard coded name
        # se quema código con entrada ó ingreso, eso quiere decir que se debe
        # poner un tipo de marcación llamado de esa forma que sea la entrada
        # a la jornada laboral.

        query = query_all.select(
            where=(query_all.row_number == 1)
        )
        return query
