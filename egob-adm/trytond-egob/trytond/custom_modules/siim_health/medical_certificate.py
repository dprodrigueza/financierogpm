from decimal import Decimal
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
from collections import defaultdict

from sql import Values, With, Literal, Window, Union
from sql.aggregate import Max, Sum, Count
from sql.conditionals import Coalesce, Case
from sql.functions import CurrentTimestamp, RowNumber, Extract
from sql.operators import Concat

from trytond.model import (
    fields, ModelSQL, ModelView, Workflow, Unique, DeactivableMixin)
from trytond.wizard import Wizard, StateAction, StateView, Button, StateReport
from trytond.pyson import Eval, Bool, If
from trytond.pool import Pool
from trytond.config import config
from trytond.transaction import Transaction

__all__ = [
    'MedicalCertificate', 'ConsultingMedicalCertificateTypeContext',
    'ConsultingMedicalCertificateType',
    'ConsultingMedicalCertificateAbsenceContext',
    'ConsultingMedicalCertificateAbsence', 'MedicalCertificateType',
    'CustomizeGroupPayslip', 'MedicalCertificateRankPayslip',
    'MedicalCertificatePayslip', 'MedicalCertificatePayslipContext',
    'MedicalCertificateDisease', 'MedicalCertificateEntity',
    'MedicalCertificateProfessional', 'MedicalCertificateDetailedReportWizard',
    'MedicalCertificateDetailedReportWizardStart'
]

number_days_week = config.getboolean(
    'siim_health', 'number_days_week', default=False)

_ZERO = Decimal("0.0")


def get_days_payslip_of_medical_certificate(start_date=None, end_date=None):
    if not start_date or not end_date:
        return None
    pool = Pool()
    RankPayslip = pool.get(
        'galeno.medical.certificate.rank.payslip')
    MedicalCertificate = pool.get('galeno.medical.certificate')
    field_name = [
        'employee', 'certificate_type', 'group_payslip', 'start_date',
        'end_date', 'employee_medical_benefit', 'number_month_medical_benefit'
    ]
    field_name_rank = [
        'until_number_days', 'percentage_payslip', 'order_',
        'number_months_minimum'
    ]
    medical_certificates = MedicalCertificate.search_read([
        ('state', '=', 'done'),
        ('register_type', '=', 'days'),
        ['OR',
         [
             ('start_date', '>=', start_date),
             ('start_date', '<=', end_date),
         ],
         [
             ('end_date', '>=', start_date),
             ('end_date', '<=', end_date),
         ]
         ],
    ], fields_names=field_name)

    dict_rank_false = {}  # ranks group-is_medical_certificate false
    dict_rank_true = {}  # ranks group-is_medical_certificate true
    dict_e_c_t = {}  # employee-certificate_type
    dict_date = {}  # employee-certificate_date
    dict_e_days = {}  # employee-certificate_type number days back
    for m_c in medical_certificates:
        key = f"{m_c['employee']}-{m_c['certificate_type']}"
        key_rank = f"{m_c['group_payslip']}"
        if not dict_e_c_t.get(key):
            dict_e_c_t.setdefault(key, [m_c])
        else:
            dict_e_c_t.get(key).append(m_c)
        if not dict_date.get(key):
            dict_date.setdefault(key, [m_c['start_date'], m_c['end_date']])
        elif dict_date.get(key)[1] > m_c['end_date']:
            temp = {
                key: [m_c['start_date'], m_c['end_date']]
            }
            dict_date.update(temp)
        if not dict_rank_true.get(key_rank):
            rank_payslips_false = RankPayslip.search_read([
                ('group_payslip', '=', m_c['group_payslip']),
                ('is_medical_benefit', '=', False),
            ], order=[('from_number_days', 'ASC')],
                fields_names=field_name_rank)
            dict_rank_false.setdefault(f"{key_rank}", rank_payslips_false)
            rank_payslip_true = RankPayslip.search_read([
                ('group_payslip', '=', m_c['group_payslip']),
                ('is_medical_benefit', '=', True),
            ], order=[('from_number_days', 'ASC')],
                fields_names=field_name_rank)
            dict_rank_true.setdefault(f"{key_rank}", rank_payslip_true)

    dict_two_months = defaultdict(lambda: defaultdict(lambda: []))
    certificates_two_months = MedicalCertificate.search_read([
        ('state', '=', 'done'),
        ('register_type', '=', 'days'),
        ('company', '=', Transaction().context['company']),
    ], fields_names=['end_date', 'start_date', 'employee', 'certificate_type'])
    for row in certificates_two_months:
        dict_two_months[row['employee']][row['certificate_type']].append(row)
    for keys in dict_e_c_t.keys():
        employee_id = keys.split("-", len(keys))[0]
        certificate_type_id = keys.split("-", len(keys))[1]
        number_days_two_months = get_days_by_type(
            dict_two_months, int(employee_id), int(certificate_type_id),
            (start_date
             if dict_date.get(keys)[0] < start_date
             else dict_date.get(keys)[0]),
            end_date
            if dict_date.get(keys)[1] > end_date
            else dict_date.get(keys)[1])
        if number_days_two_months > 0:
            dict_e_days.setdefault(keys, number_days_two_months)

    result = []
    for keys in dict_e_c_t.keys():
        employee_id = keys.split("-", len(keys))[0]
        certificate_type_id = keys.split("-", len(keys))[1]
        total = 0
        if dict_e_days.get(keys):
            total = dict_e_days.get(keys)
        for e_c_t in dict_e_c_t.get(keys):
            rank_payslips_no_benefits = dict_rank_false.get(
                f"{e_c_t['group_payslip']}")
            if not rank_payslips_no_benefits:
                continue
            is_medical_benefit = False
            for rank_payslip in rank_payslips_no_benefits:
                if (e_c_t['number_month_medical_benefit'] >=
                        rank_payslip['number_months_minimum']):
                    is_medical_benefit = True
                if is_medical_benefit:
                    break
            if is_medical_benefit:
                ranks = dict_rank_true.get(f"{e_c_t['group_payslip']}")
            else:
                ranks = rank_payslips_no_benefits
            number_days = 1 + (
                    (end_date
                     if e_c_t['end_date'] > end_date
                     else e_c_t['end_date']) -
                    (start_date
                     if e_c_t['start_date'] < start_date
                     else e_c_t['start_date'])).days
            for rank in ranks:
                days = None
                if number_days <= 0:
                    break
                if ((total == 0 and number_days <= rank['until_number_days'])
                        or (total > 0 and (
                        (total + number_days) <= rank['until_number_days']))):
                    days = number_days
                elif total == 0 and number_days > rank['until_number_days']:
                    days = rank['until_number_days']
                elif total > 0 and total < rank['until_number_days'] and (
                        (total + number_days) > rank['until_number_days']):
                    days = rank['until_number_days'] - total
                if days:
                    number_days -= days
                    total += days
                    result.append(
                         [Decimal(employee_id), Decimal(certificate_type_id),
                          days, total, rank['percentage_payslip'],
                          (1 - rank['percentage_payslip']), rank['order_']]
                    )
    return result


def get_days_by_type(dict_two_months, employee, certificate_type,
        start_date, end_date, total=0):
    temp = 59
    key = f"{employee}-{certificate_type}"
    start_date_two_months = (
            (start_date - relativedelta(days=1)) - relativedelta(days=temp))
    end_date_two_months = start_date - relativedelta(days=1)
    medical_certificates_two_months = []
    for mc in dict_two_months[employee][certificate_type]:
        if (((mc['start_date'] >= start_date_two_months) and
                (mc['start_date'] <= end_date_two_months)) or
                ((mc['end_date'] >= start_date_two_months) and
                 (mc['end_date'] <= end_date_two_months))):
            medical_certificates_two_months.append(mc)
    dict_date = {}
    for m_c in medical_certificates_two_months:
        total += 1 + (
                (end_date_two_months
                 if m_c['end_date'] > end_date_two_months
                 else m_c['end_date']) -
                (start_date_two_months
                 if m_c['start_date'] < start_date_two_months
                 else m_c['start_date'])).days
        if not dict_date.get(key):
            dict_date.setdefault(key, [m_c['start_date'], m_c['end_date']])
        elif dict_date.get(key)[1] > m_c['end_date']:
            temp = {key: [m_c['start_date'], m_c['end_date']]}
            dict_date.update(temp)
    if medical_certificates_two_months:
        total = get_days_by_type(dict_two_months, employee, certificate_type,
            (start_date_two_months if dict_date.get(
                key)[0] < start_date_two_months
            else dict_date.get(key)[0]),
            (end_date_two_months if dict_date.get(key)[1] > end_date_two_months
            else dict_date.get(key)[1]), total)
    return total


def get_notification_email_conf():
    Configuration = Pool().get('hr_ec.configuration')
    configuration = Configuration(1)
    return configuration.certificate_notification_email


class MedicalCertificate(Workflow, ModelSQL, ModelView):
    "Medical Certificate"
    __name__ = 'galeno.medical.certificate'
    _history = True

    _states = {
        'readonly': ~(Eval('state') == 'draft'),
    }
    _depends = ['state']

    number = fields.Char('Número', states={'readonly': True})
    employee = fields.Many2One(
        'company.employee', 'Empleado', required=True, states=_states,
        depends=_depends)
    company = fields.Many2One(
        'company.company', 'Compañia',
        states={
            'required': True,
            'readonly': True,
        })
    patient = fields.Many2One(
        'galeno.patient', 'Paciente', states=_states, depends=_depends,
        required=True)
    certificate_type = fields.Many2One(
        'galeno.medical.certificate.type', 'Tipo certificado', states=_states,
        depends=_depends, select=True, required=True)
    recategorized = fields.Selection([
        ('no', 'No'),
        ('recategorized_to_disease',
         'Recategorizado a ENFERMEDAD por informe de IESS'),
        ('recategorized_to_accident',
         'Recategorizado a ACCIDENTE por informe de IESS'),
    ], 'Recategorizado', required=True,
        states={
            'readonly': (Eval('state') == 'cancel'),
        }, depends=_depends)
    recategorized_translated = recategorized.translated('recategorized')
    kind = fields.Selection([
        ('particular', 'Particular'),
        ('iess', 'IESS'),
        ('internal', 'Interno'),
        ('msp', 'Ministerio de Salud Pública'),
    ], 'Origen', required=True, states=_states, depends=_depends)
    kind_translated = kind.translated('kind')
    concept = fields.Text(
        'Concepto', states=_states, depends=_depends)
    register_date = fields.DateTime('Fecha de registro',
        states={
            'readonly': True,
        }, required=True)
    entity = fields.Many2One('galeno.medical.certificate.entity',
        'Entidad Emisora', states=_states, depends=_depends, required=True)
    proffesional = fields.Many2One('galeno.medical.certificate.professional',
        'Nombre del Médico', states=_states, depends=_depends, required=True)
    notes = fields.Text('Notas', states=_states, depends=_depends)
    start_date = fields.Date('Fecha Inicio',
        domain=[
            ('start_date', '<=', Eval('end_date', None)),
            ('start_date', '<=', Eval('is_register_date')),
        ], states=_states, depends=_depends + ['end_date', 'is_register_date'],
        required=True)
    is_register_date = fields.Function(
        fields.Date('is register date'), 'on_change_with_is_register_date'
    )
    end_date = fields.Date('Fecha Fin',
        domain=[
            ('end_date', '>=', Eval('start_date', None)),
        ],
        states={
            'readonly': (Eval('state') != 'draft') |
            (Eval('register_type') == 'hours'),
        }, depends=_depends + ['start_date'], required=True)
    total_days = fields.Function(
        fields.Integer('Total Días'), 'on_change_with_total_days')
    disease_lines = fields.Many2Many(
        'galeno.medical.certificate.disease', 'medical_certificate', 'disease',
        'Enfermedades', states={
            'readonly': ~(Eval('state') == 'draft'),
            # 'required': True,
        }, depends=_depends)
    file = fields.Binary(
        'Documento', filename='filename', file_id='path', states=_states,
        depends=_depends)
    filename = fields.Char('Nombre del Archivo')
    path = fields.Char('Ubicación del archivo', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado')
    ], 'Estado', required=True, readonly=True)
    state_translated = state.translated('state')
    employee_medical_benefit = fields.Boolean(
        'Subsidio medico del empleado',
        states={
            'invisible': True,
        }, depends=['state'])
    number_month_medical_benefit = fields.Integer(
        'Número de meses para subsidio médico',
        states={
            'invisible': True,
            'required': Bool(Eval('state') == 'done') & Bool(
                Eval('employee.contract')),
        }, depends=['state'])
    group_payslip = fields.Many2One(
        'galeno.customize.group.payslip', 'Grupo de pago',
        states={
            'invisible': True,
            'required': Bool(Eval('state') == 'done') & Bool(
                Eval('employee.contract')),
        }, depends=['state'])
    payslip = fields.Function(
        fields.Boolean('Rol de pagos'), 'on_change_with_payslip'
    )
    evaluation = fields.Many2One(
        'galeno.patient.evaluation', 'Evaluación',
        context={
            'evaluation_avaliable': True,
            'patient_evaluate': Eval('patient', -1),
        },
        states={
            'readonly': ~(Eval('state') == 'draft'),
        }, depends=['state', 'patient'])
    register_type = fields.Selection([
        ('days', 'Días'),
        ('hours', 'Horas'),
    ], 'Tipo registro', required=True, states=_states, depends=_depends)
    register_type_translated = register_type.translated('register_type')
    start_time = fields.Time(
        'Hora Inicio',
        states={'invisible': Eval('register_type') == 'days',
                'readonly': Eval('state') != 'draft',
                'required': Eval('register_type') == 'hours',
                }, depends=['kind', 'state'])
    end_time = fields.Time(
        'Hora Fin',
        states={
            'invisible': Eval('register_type') == 'days',
            'readonly': Eval('state') != 'draft',
            'required': Eval('register_type') == 'hours',
        },
        domain=[
            If(Eval('register_type') == 'hours',
               ('end_time', '>=', Eval('start_time')),
               ())
        ], depends=['register_type', 'state', 'start_time'])

    # NOTIFICATION MAIL
    email_recipient_to = fields.One2Many('party.party', 'certificate',
        'e-mail', states={
            'readonly': False,
            'invisible': True
        })

    @classmethod
    def __setup__(cls):
        super(MedicalCertificate, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC'),
            ('employee', 'ASC'),
            ]
        cls._transitions |= set((
                ('draft', 'done'),
                ('draft', 'cancel'),
                ('done', 'draft'),
                ('done', 'cancel'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Bool(Eval('payslip')),
                    'depends': ['payslip'],
                },
                'done': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'depends': ['state'],
                    },
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'done']),
                    'depends': ['state'],
                    },
                })
        cls._error_messages.update({
            'certificates_overlap': (
                'El certificado "%(first)s" no se puede generar debido a que '
                'el certificado "%(second)s" ya existe'),
            'certificate_duration': (
                'La duración del certificado: "%(certificate)s" es mayor a '
                '30 días'),
            'medical_benefit': (
                'El empleado "%(employee)s": no tiene derecho a subsidio '
                'por enfermedad'),
            'no_sequence_defined': (
                'No existe secuencia definida para los certificados médicos.'
                'Por favor, diríjase a "Configuración de Talento Humano" y '
                'configure una secuencia.'),
            'not_diseases': (
                'Para el certificado médico por Días es necesario al '
                'menos una enfermedad."'),
            'employees_without_email': ('No se han encontrado direcciones de '
                'correos electrónicos para los siguientes empleados:\n'
                '%(employees)s\n\nLa notificación de certificado médico no '
                'será enviada.'),
            'employees_without_email_leader': ('No se han encontrado '
                'direcciones de correos electrónicos para el jefe:\n'
                '%(employees)s\n\nLa notificación de certificado médico no '
                'será enviada.'),
                }),

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_recategorized():
        return 'no'

    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_register_date():
        return datetime.now()

    @staticmethod
    def default_start_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_register_type():
        return 'days'

    @fields.depends('employee', 'company')
    def on_change_employee(self):
        if self.employee and self.employee.company:
            self.company = self.employee.company
        else:
            self.company = None

    @fields.depends('evaluation')
    def on_change_evaluation(self):
        if self.evaluation:
            self.patient = self.evaluation.patient
            self.kind = 'internal'
            self.start_date = self.evaluation.start_date.date()
            if self.evaluation.symptoms:
                self.concept = self.evaluation.symptoms
            self.company = Transaction().context.get('company')
            if self.evaluation.patient.employee:
                self.employee = self.evaluation.patient.employee
            diseases = []
            for row in self.evaluation.diagnostics:
                diseases.append(row.disease)
            self.disease_lines = diseases
            pool = Pool()
            Entity = pool.get('galeno.medical.certificate.entity')
            entitys = Entity.search([
                ('default', '=', True),
            ])
            if entitys:
                self.entity = entitys[0]
            Professional = pool.get('galeno.medical.certificate.professional')
            professionals = Professional.search([
                ('name', 'ilike', self.evaluation.professional.name),
            ])
            if professionals:
                self.proffesional = professionals[0]
            Type = pool.get('galeno.medical.certificate.type')
            types = None
            if 'accident' in self.evaluation.reason:
                types = Type.search([
                    ('name', 'ilike', 'accidente'),
                ])
            if self.evaluation.reason == 'disease':
                types = Type.search([
                    ('name', '=', 'Enfermedad Común'),
                ])
            if self.evaluation.reason == 'occupational_disease':
                types = Type.search([
                    ('name', '=', 'Enfermedad Profesional'),
                ])
            if types:
                self.certificate_type = types[0]

    @fields.depends('start_date', 'end_date', 'register_type')
    def on_change_with_total_days(self, name=None):
        if self.start_date and self.end_date and self.register_type == 'days':
            days = ((self.end_date + timedelta(days=1)) - self.start_date).days
            return days
        return None

    @fields.depends('register_date')
    def on_change_with_is_register_date(self, name=None):
        if self.register_date:
            return self.register_date.date()
        return None

    @fields.depends('start_date', 'register_type')
    def on_change_with_end_date(self, name=None):
        if self.register_type == 'hours':
            return self.start_date
        return None

    @fields.depends('start_date', 'end_date', 'state')
    def on_change_with_payslip(self, name=None):
        if self.start_date and self.end_date and self.state and (
                self.state == 'done'):
            # TODO agregar funcion para averiguar si esta pagado el rol
            return False
        return True

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, certificates):
        for certificate in certificates:
            certificate.up_down_workshift_delay_line(justify=False)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, certificates):
        pool = Pool()
        GroupPayslip = pool.get('galeno.customize.group.payslip')
        Contract = pool.get('company.contract')
        Sequence = pool.get('ir.sequence')
        Config = pool.get('hr_ec.configuration')
        config = Config(1)
        for certificate in certificates:
            # se comenta validación de 30 dias ya que los certificados duran mas
            # certificate.check_duration()
            contracts = Contract.search([
                ('employee', '=', certificate.employee.id),
                ('state', '=', 'done')
            ])
            if contracts:
                certificate.employee_medical_benefit = (
                    contracts[0].medical_benefit)
                certificate.number_month_medical_benefit = (
                    contracts[0].number_month_medical_benefit)
                group_payslips = GroupPayslip.search([
                    ('medical_certificate_type', '=',
                     certificate.certificate_type.id),
                    ('work_relationship', '=',
                     contracts[0].work_relationship.id),
                ])
                if group_payslips:
                    certificate.group_payslip = group_payslips[0]
            if certificate.number == '/':
                if config.medical_certificate_sequence:
                    certificate.number = Sequence.get_id(
                        config.medical_certificate_sequence.id)
                else:
                    certificate.raise_user_error('no_sequence_defined')
            certificate.up_down_absence('done')
            certificate.up_down_workshift_delay_line(justify=True)
        cls.save(certificates)

        # Va despues de estar en DONE por que existe una validacion en el html
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_conf()
        Config = Pool().get('hr_ec.configuration')
        config = Config(1)
        if notification_email:
            for item in certificates:
                if (not item.employee.contract_department.leader.email):
                    cls.raise_user_warning('without_email_leader_%d' % item.id,
                        'employees_without_email_leader', {
                           'employees': item.employee.contract_department.leader
                        })
                to_ = []
                to_.append(item.employee.contract_department.leader.party.id)
                for party in config.notification_medical_certificate:
                    to_.append(party.id)
                item.email_recipient_to = to_
                NotificationEmail.trigger(certificates,
                                          notification_email.triggers[0])

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, certificates):
        for certificate in certificates:
            certificate.up_down_absence('medical_certificate')
            certificate.up_down_workshift_delay_line(justify=False)

    @classmethod
    def validate(cls, medical_certificates):
        super(MedicalCertificate, cls).validate(medical_certificates)
        for certificate in medical_certificates:
            if (certificate.register_type == 'days' and
                    len(certificate.disease_lines) < 1):
                cls.raise_user_error('not_diseases')

            certificate.check_dates()

    def check_dates(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        cursor.execute(*table.select(table.id,
                where=(((table.start_date <= self.start_date)
                        & (table.end_date >= self.start_date))
                    | ((table.start_date <= self.end_date)
                        & (table.end_date >= self.end_date))
                    | ((table.start_date >= self.start_date)
                        & (table.end_date <= self.end_date)))
                & (table.employee == self.employee.id)
                & (table.state == 'done')
                & (table.id != self.id)))
        certificate_id = cursor.fetchone()
        if certificate_id:
            overlapping_certificate = self.__class__(certificate_id[0])
            self.raise_user_error('certificates_overlap', {
                'first': self.rec_name,
                'second': overlapping_certificate.rec_name,
            })

    def check_duration(self):
        if self.total_days and self.total_days > 30:
            self.raise_user_error('certificate_duration', {
                'certificate': self.rec_name,
            })

    def get_rec_name(self, name):
        number = f'N°{self.number} ' if self.number else ''
        return '%s%s - %s' % (
            number, self.employee.party.name, self.certificate_type.rec_name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('employee.party.name',) + tuple(clause[1:]),
                  ('certificate_type',) + tuple(clause[1:])
                  ]
        return domain

    def up_down_absence(self, done_canel):
        pool = Pool()
        Absence = pool.get('hr_ec.absence')
        absences = Absence.search([
            ('employee', '=', self.employee),
            ('start_date', '>=', self.start_date),
            ('end_date', '<=', self.end_date),
            ('state', '=', done_canel),
        ])
        for absence in absences:
            absence.state = (
                'medical_certificate' if done_canel == 'done' else 'done')
        if absences:
            # TODO Review this, is a source of errors
            try:
                Absence.save(absences)
            except Exception:
                pass

    def up_down_workshift_delay_line(self, justify=True):
        WorkshiftDelayLine = Pool().get('hr_ec.workshift.delay.line')
        delay_lines = WorkshiftDelayLine.search([
            ('workshift_delay.company', '=', self.company),
            ('workshift_delay.employee', '=', self.employee),
            ('date', '>=', self.start_date),
            ('date', '<=', self.end_date)
        ])
        to_save = []
        observation = '(Justificado por CERTIFICADO MEDICO)'
        for dl in delay_lines:
            text = observation
            if justify:
                dl.is_justified = True
                obs = dl.observation if dl.observation else ''
                if text not in obs:
                    dl.observation = text + ' ' + obs
            else:
                dl.is_justified = False
                obs = dl.observation if dl.observation else ''
                if text in obs:
                    dl.observation = obs.replace(text, '')
            to_save.append(dl)
        WorkshiftDelayLine.save(to_save)


class ConsultingMedicalCertificateTypeContext(ModelView):
    'Consulting Medical Certificate Type Context'
    __name__ = 'consulting.medical.certificate.type.context'

    start_date = fields.Date('Fecha Inicio', states={
        'required': True,
    })
    end_date = fields.Date('Fecha Fin', states={
        'required': True,
    })
    certificate_type = fields.Many2One('galeno.medical.certificate.type',
        'Tipo certificado')
    disease = fields.One2Many('galeno.disease', None, 'Enfermedad')
    limit_disease = fields.Boolean('Límite ?')
    limit_disease_number = fields.Numeric('Total a mostrar', states={
        'invisible': ~Bool(Eval('limit_disease')),
        'required': Bool(Eval('limit_disease')),
    })

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_limit_disease_number():
        return 10


class ConsultingMedicalCertificateType(ModelSQL, ModelView):
    'Consulting Medical Certificate Type'
    __name__ = 'consulting.medical.certificate.type'

    certificate_type = fields.Char('Tipo certificado')
    disease = fields.Char('Código enfermedad')
    number = fields.Integer('Cantidad')

    @classmethod
    def __setup__(cls):
        super(ConsultingMedicalCertificateType, cls).__setup__()
        cls._order = [
            ('number', 'DESC'),
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Patient = pool.get('galeno.patient')
        patient = Patient.__table__()
        Diseases = pool.get('galeno.disease')
        disease = Diseases.__table__()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        certificate = MedicalCertificate.__table__()
        MedicalCertificateType = pool.get('galeno.medical.certificate.type')
        certificate_type = MedicalCertificateType.__table__()

        CertificateDisease = pool.get('galeno.medical.certificate.disease')
        certificate_disease = CertificateDisease.__table__()

        total_limit = 999
        if context.get('limit_disease'):
            total_limit = int(context.get('limit_disease_number'))

        where = Literal(True)
        if context.get('start_date'):
            where &= (certificate.start_date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (certificate.end_date <= context.get('end_date'))
        if context.get('certificate_type'):
            where &= (certificate.certificate_type == context.get('certificate_type'))
        if context.get('disease'):
            ids = []
            for data in context.get('disease'):
                ids.append(data['id'])
            where &= (disease.id.in_(ids))

        query_limit = patient.join(employee, condition=patient.employee == employee.id
            ).join(certificate,
                   condition=certificate.employee == employee.id
            ).join(certificate_type,
                   condition=certificate.certificate_type == certificate_type.id
            ).join(certificate_disease,
                   condition=certificate.id == certificate_disease.medical_certificate
            ).join(disease,
                   condition=(certificate_disease.disease == disease.id)
            ).select(
                certificate_type.name.as_('certificate_type'),
                Count(certificate_disease.disease).as_('number'),
                disease.code.as_('disease'),
            where=where,

            group_by=[
                certificate_type.name,
                certificate_disease.disease,
                disease.code,
            ],
            limit=total_limit,
            order_by=[Count(certificate_disease.disease).desc]
        )

        query_offset = patient.join(employee, condition=patient.employee == employee.id
            ).join(certificate,
                   condition=certificate.employee == employee.id
            ).join(certificate_type,
                   condition=certificate.certificate_type == certificate_type.id
            ).join(certificate_disease,
                   condition=certificate.id == certificate_disease.medical_certificate
            ).join(disease,
                   condition=(certificate_disease.disease == disease.id)
            ).select(
                certificate_type.name.as_('certificate_type'),
                Count(certificate_disease.disease).as_('number'),
                disease.code.as_('disease'),
            where=where,

            group_by=[
                certificate_type.name,
                certificate_disease.disease,
                disease.code,
            ],
            offset=total_limit,
            order_by=[Count(certificate_disease.disease).desc]
        )

        query_1 = query_limit.select(
            query_limit.certificate_type,
            query_limit.disease,
            query_limit.number,
        )
        query_2 = query_offset.select(
            Literal('Otros').as_('certificate_type'),
            Literal('Otros').as_('disease'),
            Coalesce(Sum(query_offset.number), 0),
        )
        query = Union(query_1, query_2, all_=True)

        query_result = query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.certificate_type,
            query.disease,
            query.number,
        )
        return query_result


class ConsultingMedicalCertificateAbsenceContext(ModelView):
    'Consulting Medical Certificate Absence Context'
    __name__ = 'consulting.medical.certificate.absence.context'

    start_date = fields.Date('Fecha Inicio', states={
        'required': True,
    })
    end_date = fields.Date('Fecha Fin', states={
        'required': True,
    })
    contracts = fields.One2Many('company.contract', None, 'Empleados')
    is_certificate = fields.Boolean('Certificados médicos ?', states={
        'required': (~Bool(Eval('is_certificate')) &
                     ~Bool(Eval('is_permission')) &
                     ~Bool(Eval('is_absence'))),
    })
    is_permission = fields.Boolean('Permisos ?')
    is_absence = fields.Boolean('Faltas ?')
    limit_disease = fields.Boolean('Límite ?')
    limit_disease_number = fields.Numeric('Total a mostrar', states={
        'invisible': ~Bool(Eval('limit_disease')),
        'required': Bool(Eval('limit_disease')),
    })

    @staticmethod
    def default_start_date():
        return datetime.today().date()

    @staticmethod
    def default_end_date():
        return datetime.today().date()

    @staticmethod
    def default_is_certificate():
        return True

    @staticmethod
    def default_is_permission():
        return False

    @staticmethod
    def default_is_absence():
        return False

    @staticmethod
    def default_limit_disease_number():
        return 10


class ConsultingMedicalCertificateAbsence(ModelSQL, ModelView):
    'Consulting Medical Certificate Absence'
    __name__ = 'consulting.medical.certificate.absence'

    employee = fields.Char('Empleados')
    total_days = fields.Numeric('Total días', digits=(16, 2))

    @classmethod
    def __setup__(cls):
        super(ConsultingMedicalCertificateAbsence, cls).__setup__()
        cls._order = [
            ('total_days', 'DESC'),
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        Contract = pool.get('company.contract')
        certificate = MedicalCertificate.__table__()
        Party = pool.get('party.party')
        party = Party.__table__()
        Permission = pool.get('hr_ec.permission')
        permission = Permission.__table__()
        Absence = pool.get('hr_ec.absence')
        absence = Absence.__table__()

        total_limit = 999
        if context.get('limit_disease'):
            total_limit = int(context.get('limit_disease_number'))

        where_certificate = Literal(True)
        where_permission = Literal(True)
        where_absence = Literal(True)
        if context.get('start_date'):
            where_certificate &= (certificate.start_date >= context.get('start_date'))
            where_permission &= (permission.start_date >= context.get('start_date'))
            where_absence &= (absence.start_date >= context.get('start_date'))
        if context.get('end_date'):
            where_certificate &= (certificate.end_date <= context.get('end_date'))
            where_permission &= (permission.end_date <= context.get('end_date'))
            where_absence &= (absence.end_date <= context.get('end_date'))
        if context.get('contracts'):
            ids = []
            for data in context.get('contracts'):
                contract, = Contract.search([('id', '=', data['id'])])
                ids.append(contract.employee.id)
            where_certificate &= (employee.id.in_(ids))
            where_permission &= (employee.id.in_(ids))
            where_absence &= (employee.id.in_(ids))

        query_certificate = employee.join(certificate,
                condition=certificate.employee == employee.id
        ).join(party,
                condition=employee.party == party.id
        ).select(
            party.name.as_('employee'),
            Sum(Case(
                (certificate.register_type == 'days',
                (certificate.end_date - certificate.start_date) + 1),
                else_=((Extract('SECOND',
                    (certificate.end_time - certificate.start_time) / 28800))),
            )).as_(
                'total_days'),
            where=where_certificate & (certificate.state == 'done'),
            group_by=[party.name]
        )

        query_permissions = employee.join(permission,
                condition=permission.employee == employee.id
        ).join(party,
                condition=employee.party == party.id
        ).select(
            party.name.as_('employee'),
            Sum(Case(
                (permission.kind == 'days',
                (permission.end_date - permission.start_date) + 1),
                else_=((Extract('SECOND',
                    (permission.end_time - permission.start_time) / 28800))),
            )).as_('total_days'),
            where=where_permission & (permission.state == 'done'),
            group_by=[party.name]
        )
        query_absences = employee.join(absence,
                condition=absence.employee == employee.id
        ).join(party,
                condition=employee.party == party.id
        ).select(
            party.name.as_('employee'),
            Sum(Case(
                (absence.kind == 'days',
                 (absence.end_date - absence.start_date) + 1),
                else_=((Extract('SECOND',
                    (absence.end_time - absence.start_time) / 28800))),
            )).as_('total_days'),
            where=where_absence & (absence.state == 'done'),
            group_by=[party.name]
        )

        if (context.get('is_certificate') and context.get('is_permission')
                and context.get('is_absence')):
            query = Union(query_certificate, query_permissions, query_absences,
                          all_=True)
        elif (context.get('is_certificate') and context.get('is_permission')
              and not context.get('is_absence')):
            query = Union(query_certificate, query_permissions, all_=True)
        elif (context.get('is_certificate') and not context.get('is_permission')
              and context.get('is_absence')):
            query = Union(query_certificate, query_absences, all_=True)
        elif (not context.get('is_certificate') and context.get('is_permission')
              and context.get('is_absence')):
            query = Union(query_permissions, query_absences, all_=True)
        elif (not context.get('is_certificate') and
              not context.get('is_permission') and context.get('is_absence')):
            query = query_absences
        elif (not context.get('is_certificate') and context.get('is_permission')
              and not context.get('is_absence')):
            query = query_permissions
        elif (context.get('is_certificate') and not context.get('is_permission')
              and not context.get('is_absence')):
            query = query_certificate


        query_union = query.select(
            query.employee,
            Sum(query.total_days).as_('total_days'),
            group_by=[query.employee],
            order_by=[Sum(query.total_days).desc]
        )

        query_limit = query_union.select(
            limit=total_limit,
            order_by=[query_union.total_days.desc]
        )

        query_offset = query_union.select(
            offset=total_limit,
            order_by=[query_union.total_days.desc]
        )

        query_1 = query_limit.select(
            query_limit.employee,
            query_limit.total_days,
        )

        query_2 = query_offset.select(
            Literal('Otros').as_('employee'),
            Coalesce(Sum(query_offset.total_days), 0),
        )
        final_query = Union(query_1, query_2, all_=True)

        query = final_query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            final_query.employee,
            Sum(final_query.total_days).as_('total_days'),
            group_by=[final_query.employee],
            order_by=[Sum(final_query.total_days).desc]
        )
        return query


class MedicalCertificateEntity(ModelSQL, ModelView):
    'Medical Certificate Entity'
    __name__ = 'galeno.medical.certificate.entity'

    name = fields.Char('Nombre', required=True)
    default = fields.Boolean('Por defecto')

    @classmethod
    def __setup__(cls):
        super(MedicalCertificateEntity, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]


class MedicalCertificateProfessional(ModelSQL, ModelView):
    'Medical Certificate Professional'
    __name__ = 'galeno.medical.certificate.professional'

    name = fields.Char('Nombre', required=True)

    @classmethod
    def __setup__(cls):
        super(MedicalCertificateProfessional, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]


class MedicalCertificateType(ModelSQL, ModelView):
    'Medical Certificate Type'
    __name__ = 'galeno.medical.certificate.type'

    name = fields.Char(
        'Nombre',
        states={
            'required': True,
        })

    @classmethod
    def __setup__(cls):
        super(MedicalCertificateType, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]

    def get_rec_name(self, name):
        name = getattr(self, 'name', None)
        text = name if name else str(self.id)
        return '%s' % (text)


class MedicalCertificateDisease(ModelSQL):
    'Medical Certificate Disease'
    __name__ = 'galeno.medical.certificate.disease'

    medical_certificate = fields.Many2One(
        'galeno.medical.certificate', 'Certificado Médico')
    disease = fields.Many2One('galeno.disease', 'Enfermedades')


class CustomizeGroupPayslip(ModelSQL, ModelView):
    'Customize Group Payslip'
    __name__ = 'galeno.customize.group.payslip'

    medical_certificate_type = fields.Many2One(
        'galeno.medical.certificate.type', 'Tipo de certificado médico',
        required=True)
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Relación laboral', required=True)

    @classmethod
    def __setup__(cls):
        super(CustomizeGroupPayslip, cls).__setup__()
        t = cls.__table__()
        cls._error_messages.update({
            'medical_certificate_type_work_relationship_uniq': (
                'Relacion labolar y tipo de certificado debe ser unicos.'
            ),
        })
        cls._sql_constraints += [
            ('medical_certificate_type_work_relationship_uniq',
             Unique(t, t.medical_certificate_type, t.work_relationship),
             'medical_certificate_type_work_relationship_uniq'),
        ]
        cls._order = [
            ('medical_certificate_type.name', 'ASC'),
            ('work_relationship.name', 'ASC'),
        ]

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('medical_certificate_type',) + tuple(clause[1:]),
            ('work_relationship',) + tuple(clause[1:]),
            ]
        return domain

    def get_rec_name(self, name):
        return "%s - %s" % (
            self.medical_certificate_type.rec_name,
            self.work_relationship.rec_name)


class MedicalCertificateRankPayslip(ModelSQL, ModelView, DeactivableMixin):
    'Medical Certificate Rank Payslip'
    __name__ = 'galeno.medical.certificate.rank.payslip'

    group_payslip = fields.Many2One(
        'galeno.customize.group.payslip', 'Grupo de pago',
        states={
            'required': True,
        })
    number_months_minimum = fields.Integer(
        'Número de meses mínimo',
        states={
            'readonly': Bool(Eval('is_medical_benefit')),
            'required': True,
        },
        domain=[
            ('number_months_minimum', '>=', '0'),
        ], depends=['is_medical_benefit'])
    from_number_days = fields.Numeric(
        'Desde # dias', digits=(16, 2),
        states={
            'required': True,
        })
    until_number_days = fields.Numeric(
        'Hasta # dias', digits=(16, 2),
        states={
            'required': True,
        })
    percentage_payslip = fields.Numeric(
        'Porcentaje del Sueldo', digits=(16, 2),
        states={
            'required': True,
        },
        domain=[
            ('percentage_payslip', '>=', Decimal("0")),
            ('percentage_payslip', '<=', Decimal("1")),
        ])
    order_ = fields.Integer('Orden', required=True)
    is_medical_benefit = fields.Boolean('Tiene beneficios medicos?')

    @classmethod
    def __setup__(cls):
        super(MedicalCertificateRankPayslip, cls).__setup__()
        t = cls.__table__()
        cls._order = [
            ('group_payslip.medical_certificate_type.name', 'ASC'),
            ('group_payslip.work_relationship.name', 'ASC'),
            ('is_medical_benefit', 'ASC'),
            ('from_number_days', 'ASC'),
        ]
        cls._sql_constraints += [
            ('rank_uniq',
             Unique(t, t.group_payslip, t.order_, t.is_medical_benefit),
             'Grupo de pago, Orden y Tiene beneficios medicos deben ser unicos')
        ]

    @classmethod
    def default_number_months_minimum(cls):
        return 0

    @classmethod
    def default_is_medical_benefit(cls):
        return True

    @fields.depends('number_months_minimum', 'is_medical_benefit')
    def on_change_is_medical_benefit(self):
        if self.is_medical_benefit:
            self.number_months_minimum = 0


class MedicalCertificatePayslipContext(ModelView):
    'Medical Certificate Paylsip Context'
    __name__ = 'galeno.medical.certificate.payslip.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today() - relativedelta(months=1)

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()


class MedicalCertificatePayslip(ModelView, ModelSQL):
    'Medical Certificate Payslip'
    __name__ = 'galeno.medical.certificate.payslip'

    employee = fields.Many2One('company.employee', 'Empleado', readonly=True)
    certificate_type = fields.Many2One(
        'galeno.medical.certificate.type', 'Tipo de Certificado', readonly=True)
    number_days_month = fields.Integer('Total días', readonly=True)
    number_days_two_months = fields.Integer('Total (60 días)', readonly=True)
    percentage_payslip = fields.Numeric(
        'Porcentaje de pago (Empleador)', readonly=True, digits=(16, 2))
    percentage_payslip_iess = fields.Numeric(
        'Porcentaje de pago (IESS)', readonly=True, digits=(16, 2))
    order_ = fields.Integer('Orden', readonly=True)

    @classmethod
    def __setup__(cls):
        super(MedicalCertificatePayslip, cls).__setup__()
        cls._order = [
            ('employee', 'ASC'),
            ('certificate_type', 'ASC'),
            ('order_', 'ASC'),
        ]

    @classmethod
    def table_query(cls):
        context = Transaction().context
        start_date = context.get('start_date')
        end_date = context.get('end_date')
        result = get_days_payslip_of_medical_certificate(start_date, end_date)
        query_temp = With()
        columns = [
            'employee', 'certificate_type', 'number_days_month',
            'number_days_two_months', 'percentage_payslip',
            'percentage_payslip_iess', 'order_',
        ]
        query_temp.columns = columns
        query_temp.query = Values(result if result
            else [[-1, -1, _ZERO, _ZERO, _ZERO, _ZERO, Literal(1)]])
        query_all = query_temp.select(with_=[query_temp])
        query = query_all.select(
            RowNumber(window=Window([])).as_('id'),
            query_all.employee,
            query_all.certificate_type,
            query_all.percentage_payslip,
            query_all.percentage_payslip_iess,
            query_all.order_,
            Sum(query_all.number_days_month).as_('number_days_month'),
            Max(query_all.number_days_two_months).as_('number_days_two_months'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            group_by=([
                query_all.employee,
                query_all.certificate_type,
                query_all.percentage_payslip,
                query_all.percentage_payslip_iess,
                query_all.order_,
            ])
        )
        return query

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('employee.party.name',) + tuple(clause[1:]),
                  ('certificate_type',) + tuple(clause[1:])
                  ]
        return domain


class MedicalCertificateSendEmail(ModelSQL, ModelView):
    'Medical Certificate Send Email'
    __name__ = 'galeno.medical.certificate.send.email'

    email = fields.Many2One('company.employee', 'Empleado')


class MedicalCertificateDetailedReportWizardStart(ModelView):
    'Medical Certificate Detailed Report Wizard Start'
    __name__ = 'medical.certificate.detailed.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    start_date = fields.Date('Fecha inicio',
        domain=[
            ('start_date', '<=', Eval('end_date'))
        ], depends=['end_date'], required=True)
    end_date = fields.Date('Fecha fin',
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date'], required=True)
    type_export = fields.Selection([
            ('all', 'Todos Empleados'),
            ('specific', 'Empleados especificos'),
        ], 'Tipo exportación')
    employees = fields.One2Many('company.employee', None, 'Empleados',
        domain=[
            ('company', '=', Eval('company'))
        ],
        states={
            'invisible': Bool(Eval('type_export') == 'all'),
        }, depends=['company', 'type_export'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type_export():
        return 'all'

    @staticmethod
    def default_start_date():
        pool = Pool()
        Period = pool.get('account.period')
        Date = pool.get('ir.date')
        today = Date.today()
        periods = Period.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
        ])
        if periods:
            return periods[0].payslip_start_date
        return today

    @staticmethod
    def default_end_date():
        pool = Pool()
        Period = pool.get('account.period')
        Date = pool.get('ir.date')
        today = Date.today()
        periods = Period.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
        ])
        if periods:
            return periods[0].payslip_end_date
        return today


class MedicalCertificateDetailedReportWizard(Wizard):
    'Medical Certificate Detailed Report Wizard'
    __name__ = 'medical.certificate.detailed.report.wizard'

    start = StateView('medical.certificate.detailed.report.wizard.start',
        'siim_health.medical_certificate_detailed_report_wizard_start_view_form', #noqa
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('medical.certificate.detailed.report.export')

    def do_export_(self, action):
        start_date = self.start.start_date
        end_date = self.start.end_date
        type_export = self.start.type_export
        employees = self.start.employees

        medical_certificates = self.get_medical_certificates(
            start_date, end_date, employees, type_export)

        data = {
            'company': self.start.company.id,
            'start_date': start_date,
            'end_date': end_date,
            'ids_medical_certificates': medical_certificates
        }
        return action, data

    def get_medical_certificates(self, start_date, end_date, employees,
            type_export):
        Employee = Pool().get('company.employee')
        MedicalCertificate = Pool().get('galeno.medical.certificate')

        result = defaultdict(lambda: [])

        if type_export == 'all':
            employees = Employee.search([], order=[('party.name', 'ASC')])

        for employee in employees:
            medical_certificates_ids = []
            medical_certificates = MedicalCertificate.search([
                ('employee', '=', employee),
                ('start_date', '>=', start_date),
                ('end_date', '<=', end_date),
                ('state', '=', 'done'),
            ])
            for medical_certificate in medical_certificates:
                medical_certificates_ids.append(medical_certificate.id)

            if medical_certificates_ids:
                result[employee.identifier] = medical_certificates_ids
        return result
