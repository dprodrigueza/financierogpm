from collections import defaultdict

from trytond.pool import PoolMeta, Pool
from trytond.model import fields

__all__ = [
    'Patient'
]


class Patient(metaclass=PoolMeta):
    __name__ = 'galeno.patient'

    medical_certificates = fields.Function(
        fields.One2Many(
            'galeno.medical.certificate', None, 'Certificados medicos',
        ), 'get_medical_certificates'
    )

    @classmethod
    def get_medical_certificates(cls, patients, names=None):
        certificates = defaultdict(lambda: [])
        MedicalCertificate = Pool().get('galeno.medical.certificate')
        medical_certificates = MedicalCertificate.search_read([
            ('state', '=', 'done'),
        ], fields_names=['id', 'patient'], order=[('start_date', 'DESC')])
        for row in medical_certificates:
            certificates[row['patient']].append(row['id'])
        return {
            'medical_certificates': certificates,
        }