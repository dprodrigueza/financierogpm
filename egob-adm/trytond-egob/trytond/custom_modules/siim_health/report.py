from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.modules.hr_ec.company import CompanyReportSignature
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
mpl.use('Agg')
from io import BytesIO
from collections import defaultdict

__all__ = [
    'MedicalCertificate', 'ConsultingMedicalCertificateTypeReport',
    'ConsultingMedicalCertificateAbsenceReport', 'MedicalCertificateExcel',
    'MedicalCertificateDetailedReportExport'
]

class MedicalCertificate(CompanyReportSignature):
    __name__ = 'galeno.medical.certificate'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MedicalCertificate, cls).get_context(
            records, data)
        context = Transaction().context
        employee_id = context.get('employee')
        if employee_id:
            pool = Pool()
            Employee = pool.get('company.employee')
            employee, = Employee.browse([employee_id])
            report_context['doctor'] = employee.party.name
            if employee.contract and employee.contract.position:
                report_context['position_doctor'] = (
                    employee.contract.position.rec_name)
            else:
                report_context['position_doctor'] = f"Configurar usuario"
        else:
            report_context['doctor'] = f"Configurar usuario"
            report_context['position_doctor'] = f"Configurar usuario"
        return report_context


class PatientEvaluationQueryReport(CompanyReportSignature):
    __name__ = 'galeno.patient.evaluation.query.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PatientEvaluationQueryReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)
        return report_context

    @classmethod
    def _get_sentences(cls, evaluations):
        sentence_dict = {}
        for evaluation in evaluations:
            if evaluation.sentence not in sentence_dict:
                sentence_dict[evaluation.sentence] = []
            sentence_dict[evaluation.sentence].append(evaluation)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class PatientEvaluationByDiseaseReport(CompanyReportSignature):
    __name__ = 'galeno.patient.evaluation.by.disease.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PatientEvaluationByDiseaseReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)

        fig, ax = plt.subplots(figsize=(12, 8), subplot_kw=dict(
            aspect="equal"))
        labels = [x.disease.name for x in records]
        data = [x.number for x in records]

        def func(pct, allvals):
            absolute = int(pct / 100. * np.sum(allvals))
            return "{:.1f}%\n({:d} )".format(pct, absolute)
        wedges, texts, autotexts = ax.pie(data,
                  autopct=lambda pct: func(pct, data),
                  textprops=dict(color="w"))
        ax.legend(wedges, labels,
                  title="Enfermedades",
                  loc="center left",
                  bbox_to_anchor=(0.7, 0.2,))
        plt.setp(autotexts, size=6, weight="bold")


        f = BytesIO()
        ax.set_title("Enfermedades")
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        return report_context

    @classmethod
    def _get_sentences(cls, evaluations):
        sentence_dict = {}
        for evaluation in evaluations:
            if evaluation.sentence not in sentence_dict:
                sentence_dict[evaluation.sentence] = []
            sentence_dict[evaluation.sentence].append(evaluation)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class PatientEvaluationByPositionReport(CompanyReportSignature):
    __name__ = 'galeno.patient.evaluation.by.position.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PatientEvaluationByPositionReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)

        fig, ax = plt.subplots(figsize=(6, 4), subplot_kw=dict(
            aspect="equal"))
        labels = [x.position.name for x in records]
        data = [x.number for x in records]

        def func(pct, allvals):
            absolute = int(pct / 100. * np.sum(allvals))
            return "{:.1f}%\n({:d} )".format(pct, absolute)
        wedges, texts, autotexts = ax.pie(data,
                  autopct=lambda pct: func(pct, data),
                  textprops=dict(color="w"))
        ax.legend(wedges, labels,
                  title="Puesto de trabajo",
                  loc="center left",
                  bbox_to_anchor=(1, 0, 0.5, 1))
        plt.setp(autotexts, size=10, weight="bold")
        f = BytesIO()
        ax.set_title("Por Puesto de trabajo")
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        return report_context

    @classmethod
    def _get_sentences(cls, evaluations):
        sentence_dict = {}
        for evaluation in evaluations:
            if evaluation.sentence not in sentence_dict:
                sentence_dict[evaluation.sentence] = []
            sentence_dict[evaluation.sentence].append(evaluation)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class PatientOcupationalHazardReport(CompanyReportSignature):
    __name__ = 'galeno.patient.ocupational.hazard.report'


class PatientEvaluationAbsenceReport(CompanyReportSignature):
    __name__ = 'galeno.patient.evaluation.absence.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PatientEvaluationAbsenceReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)

        labels = [str(x.employee.party.last_name) for x in records]
        days = [x.total_days for x in records]
        data = [len(records)]

        x_pos = [i for i, _ in enumerate(labels)]
        plt.figure(figsize=(11, 15))
        plt.bar(labels, days, align='center', alpha=0.5)
        plt.xlabel("Empleado")
        plt.ylabel("Dias")
        plt.title("Ausentismo")
        plt.xticks(x_pos, labels,rotation=90)

        f = BytesIO()
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        return report_context

    @classmethod
    def _get_sentences(cls, certifications):
        sentence_dict = {}
        for certificate in certifications:
            if certificate.sentence not in sentence_dict:
                sentence_dict[certificate.sentence] = []
            sentence_dict[certificate.sentence].append(certificate)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class ConsultingMedicalCertificateTypeReport(CompanyReportSignature):
    __name__ = 'consulting.medical.certificate.type.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ConsultingMedicalCertificateTypeReport,
                               cls).get_context(records, data)

        context = Transaction().context

        fig, ax_pie = plt.subplots(figsize=(10, 7), subplot_kw=dict(aspect="equal"))

        labels = []
        data = []
        types_certificates = set()
        for record in records:
            labels.append(f"{record.disease} ({record.number})")
            data.append(record.number)
            types_certificates.add(record.certificate_type)

        wedges, texts, autotexts = ax_pie.pie(data,
            autopct=lambda pct: cls.total_percentage(pct, data),
            textprops=dict(color="w"))

        ax_pie.legend(wedges, labels,
                  title="Códigos",
                  loc="center left",
                  bbox_to_anchor=(1, 0, 0, 1),
                  prop={'size': 6})
        plt.setp(autotexts, size=6)

        f_pie = BytesIO()
        types = ''
        for type in list(types_certificates):
            types += ' - '
            types += type
        ax_pie.set_title(f"Razón: {types[2:len(types)]}")
        plt.savefig(f_pie, format="png")
        report_context['image_pie'] = f_pie.getvalue()

        # _______________________________________________ BAR _________________
        fig, ax_bar = plt.subplots(figsize=(11, 15))
        labels = []
        for record in records:
            labels.append(cls._get_name_disease(record.disease))
        ax_bar.bar(labels, data, align='center', alpha=0.5)
        plt.xticks(rotation=90)

        f_bar = BytesIO()
        # ax.set_title(f"Ausentismo")
        plt.savefig(f_bar, format="png")
        report_context['image_bar'] = f_bar.getvalue()
        # ___________________________________________ END BAR _________________

        report_context['start_date'] = context.get('start_date')
        report_context['end_date'] = context.get('end_date')
        report_context['_get_name_disease'] = cls._get_name_disease
        return report_context


    @classmethod
    def total_percentage(cls, pct, allvals):
        absolute = int(pct / 100. * float(np.sum(allvals)))
        return "{:.1f}%".format(pct, absolute)

    @classmethod
    def _get_name_disease(cls, code_desease):
        Disease = Pool().get('galeno.disease')

        diseases = Disease.search([
            ('code', '=', code_desease),
        ])
        if diseases:
            return f"{code_desease} - {diseases[0].name}"
        else:
            return f"{code_desease} - Otros"


class ConsultingMedicalCertificateAbsenceReport(CompanyReportSignature):
    __name__ = 'consulting.medical.certificate.absence.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ConsultingMedicalCertificateAbsenceReport,
                               cls).get_context(records, data)

        context = Transaction().context

        fig, ax_pie = plt.subplots(figsize=(11, 8), subplot_kw=dict(aspect="equal"))

        labels = []
        data = []
        for record in records:
            labels.append(record.employee)
            data.append(record.total_days)

        wedges, texts, autotexts = ax_pie.pie(data,
            autopct=lambda pct: cls.total_percentage(pct, data),
            textprops=dict(color="w"))

        ax_pie.legend(wedges, labels,
                  title="Nombres",
                  loc="center left",
                  bbox_to_anchor=(1, 0, 0, 1),
                  prop={'size': 6})
        plt.setp(autotexts, size=6)

        f_pie = BytesIO()
        plt.savefig(f_pie, format="png")
        report_context['image_pie'] = f_pie.getvalue()
        # _______________________________________________ BAR _________________
        fig, ax_bar = plt.subplots(figsize=(11, 15))

        ax_bar.bar(labels, data, align='center', alpha=0.5)
        plt.xticks(rotation=90)

        f_bar = BytesIO()
        # ax.set_title(f"Ausentismo")
        plt.savefig(f_bar, format="png")
        report_context['image_bar'] = f_bar.getvalue()
        # ___________________________________________ END BAR _________________

        report_context['start_date'] = context.get('start_date')
        report_context['end_date'] = context.get('end_date')
        return report_context


    @classmethod
    def total_percentage(cls, pct, allvals):
        absolute = int(pct / 100. * float(np.sum(allvals)))
        return "{:.1f}%".format(pct, absolute)


class MedicalCertificateExcel(CompanyReportSignature):
    __name__ = 'galeno.medical.certificate.excel'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MedicalCertificateExcel, cls).get_context(
            records, data)
        return report_context


class MedicalCertificateDetailedReportExport(CompanyReportSignature):
    __name__ = 'medical.certificate.detailed.report.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            MedicalCertificateDetailedReportExport, cls).get_context(
                records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Employee = pool.get('company.employee')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        company = Company.search([('id', '=', data['company'])])[0]
        company_ruc = company.party.identifiers[0].code

        records = defaultdict(lambda: [])
        for identifier in data['ids_medical_certificates']:
            employees = Employee.search([
                ('identifier', '=', identifier)
            ])
            if employees:
                employee = employees[0]
                medical_certificates = MedicalCertificate.search([
                    ('id', 'in',
                        data['ids_medical_certificates'].get(identifier))
                ])
                records[employee] = medical_certificates

        report_context['company_ruc'] = company_ruc
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['records'] = records



class PatientMedicalCertificateTypeReport(CompanyReportSignature):
    __name__ = 'patient.medical.certificate.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PatientMedicalCertificateTypeReport,
        cls).get_context(records, data)

        context = Transaction().context

        labels = []
        data = []
        types_certificates = set()
        for record in records:
            labels.append(f"{record.disease} ({record.number})")
            data.append(record.number)
            types_certificates.add(record.certificate_type)
        # _______________________________________________ BAR _________________
        fig, ax_bar = plt.subplots(figsize=(8,5))
        labels = []
        for record in records:
            labels.append(cls._get_name_disease(record.disease))
        ax_bar.bar(labels, data,)
        # plt.grid(True)
        # plt.grid(color='b', linestyle='-', linewidth=1)
        #plt.figure(figsize=(10, 10))
        plt.xticks(rotation=270,)
        plt.xlabel('Enfermedades')
        plt.ylabel('Nro.')
        f_bar = BytesIO()
        plt.savefig(f_bar, format="png")
        report_context['image_bar'] = f_bar.getvalue()

        # ___________________________________________ END BAR _________________

        report_context['start_date'] = context.get('start_date')
        report_context['end_date'] = context.get('end_date')
        report_context['_get_name_disease'] = cls._get_name_disease
        plt.close("all")
        return report_context


    @classmethod
    def total_percentage(cls, pct, allvals):
        absolute = int(pct / 100. * float(np.sum(allvals)))
        return "{:.1f}%".format(pct, absolute)

    @classmethod
    def _get_name_disease(cls, code_desease):
        Disease = Pool().get('galeno.disease')

        diseases = Disease.search([
            ('code', '=', code_desease),
        ])
        if diseases:
            return f"{code_desease} - {diseases[0].name}"
        else:
            return f"{code_desease} - Otros"




