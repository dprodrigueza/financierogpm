from pathlib import Path
import csv
import collections

from trytond.config import config
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

import pandas as pd

__all__ = [
    'Requirement', 'MaterialRequirement', 'PurchaseRequest', 'Consolidation'
]


def sort_fields(data=[]):
    '''
    :param data: must be a list of dictionaries, for example:
        [{key: value}, {key: value}]
    :return: a list of sorted dictionaries
    '''
    _data = []
    for d in data:
        d = collections.OrderedDict(sorted(d.items()))
        _data.append(d)
    return _data


class Requirement(metaclass=PoolMeta):
    __name__ = 'purchase.requirement.line'

    @classmethod
    def generate_file(cls, context, path_name, filename):
        fields = [
            'id', 'description', 'uom', 'type', 'requirements.date',
            'requirements.request_by.party.name',
            'requirements.request_date', 'requirements.department.name',
            'request_quantity', 'requirements.done_by.party.name',
            'requirements.done_date', 'aproved_quantity', 'requirements.state'
        ]
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/public_pac/requirement/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{filename}.csv"
        with Transaction().set_context(context):
            data = cls.search_read([], fields_names=fields)
            data = sort_fields(data)
            data_frame = pd.DataFrame.from_dict(data)
            data_frame.reindex(columns=sorted(data_frame.columns))
            data_frame.to_csv(
                filename, sep='|', encoding='utf-8', index=False,
                quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            cls.generate_file(None, bi_path, 'requirements')


class MaterialRequirement(metaclass=PoolMeta):
    __name__ = 'purchase.material.requirement.line'

    @classmethod
    def generate_file(cls, context, path_name, filename):
        fields = [
            'id', 'requirement_line_origin', 'recommend_buy',
            'requirement.state', 'delivered_quantity', 'purchase_quantity'
        ]
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/public_pac/material_requirement/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{filename}.csv"
        with Transaction().set_context(context):
            data = cls.search_read([], fields_names=fields)
            data = sort_fields(data)
            data_frame = pd.DataFrame.from_dict(data)
            data_frame.reindex(columns=sorted(data_frame.columns))
            data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
                              quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            cls.generate_file(None, bi_path, 'material_requirement')


class Consolidation(metaclass=PoolMeta):
    __name__ = 'purchase.consolidation.line.detail'

    @classmethod
    def generate_file(cls, context, path_name, filename):
        fields = [
            'line', 'detail.requirement_material_line',
            'line.purchase_quantity', 'line.purchase_consolidation.state'
        ]
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/public_pac/consolidation/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{filename}.csv"
        with Transaction().set_context(context):
            data = cls.search_read([], fields_names=fields)
            data = sort_fields(data)
            data_frame = pd.DataFrame.from_dict(data)
            data_frame.reindex(columns=sorted(data_frame.columns))
            data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
                              quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            cls.generate_file(None, bi_path, 'consolidation')


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'

    @classmethod
    def generate_file(cls, context, path_name, filename):
        fields = [
            'purchase_header.state', 'purchase_header.request_by.party.name',
            'purchase_header.request_date', 'purchase_header.done_by.party.name',
            'purchase_header.done_date',
            'purchase_header.suggested_procedure.type_contract',
            'purchase_header.number', 'consolidation_origin',
            'estimated_unit_price', 'estimated_amount', 'product.template.name',
            'preferred_quotation_line.unit_price'
        ]
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/public_pac/purchase_request/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{filename}.csv"
        with Transaction().set_context(context):
            data = cls.search_read([], fields_names=fields)
            data = sort_fields(data)
            data_frame = pd.DataFrame.from_dict(data)
            data_frame.reindex(columns=sorted(data_frame.columns))
            data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
                quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            cls.generate_file(None, bi_path, 'purchase_request')
        