from trytond.pool import Pool
from . import bi

__all__ = ['register']


def register():
    Pool.register(
        bi.Requirement,
        bi.MaterialRequirement,
        bi.Consolidation,
        bi.PurchaseRequest,
        module='public_pac_bi_data', type_='model')
    Pool.register(
        module='public_pac_bi_data', type_='wizard')
    Pool.register(
        module='public_pac_bi_data', type_='report')
