from trytond.modules.company import CompanyReport

__all__ = ['AlertEmail']

class AlertEmail(CompanyReport):
    __name__ = 'siim.alert.email'