from email.header import Header
from email.utils import formataddr, getaddresses
from collections import defaultdict

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.report import get_email
from trytond.config import config
from trytond.sendmail import sendmail_transactional, SMTPDataManager

__all__ = ['Alert', 'AlertType', 'AlertTypeEmployee']


class Alert(Workflow, ModelSQL, ModelView):
    'Alert'
    __name__ = "siim.alert"
    # En employee se guarda el empleado a cual va dirigido el mail, en
    # description una breve descripcion y en observation un mensaje de error si
    # no se envia el mail. En model se guarda el objeto de la alerta, como
    # ejemplo la referencia del contrato

    STATES_ = {
        'readonly': (Eval('state') != 'draft'),
    }

    DEPENDS_ = ['state']

    company = fields.Many2One(
        'company.company', 'Compañia', required=True,
        states={
            'readonly': True,
        })
    model = fields.Reference(
        'Modelos', selection='get_reference',
        states={
            'readonly': True,
        })
    description = fields.Text(
        'Descripción', required=True, states=STATES_, depends=DEPENDS_)
    employee = fields.Many2One(
        'company.employee', 'Empleado destino', states=STATES_, depends=DEPENDS_)
    send_email = fields.Boolean(
        'Envio e-mail', states=STATES_, depends=DEPENDS_)
    observation = fields.Text('Observacion', states=STATES_, depends=DEPENDS_)
    type = fields.Many2One(
        'siim.alert.type.employee', 'Tipo de Alerta', states=STATES_,
        depends=DEPENDS_)
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('done', 'Realizado'),
            ('cancel', 'Cancelado'),
        ], 'Estado', readonly=True, required=True
    )

    @classmethod
    def __setup__(cls):
        super(Alert, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
            ('done', 'cancel')
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': ~Eval('state').in_(['done']),
            },
            'done': {
                'invisible': ~Eval('state').in_(['draft']),
            }
        })
        cls._order.insert(0, ('create_date', 'DESC'))
        cls._order.insert(1, ('type', 'ASC'))

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def get_reference(cls):
        pool = Pool()
        Model = pool.get('ir.model')
        models = Model.search([])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, alerts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, alerts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, alerts):
        pass

    @fields.depends('user')
    def on_change_with_company(self, name=None):
        if self.user:
            return self.user.company.id

    @classmethod
    def trigger_create(cls, alerts):
        cls.send_emails(alerts)

    @classmethod
    def send_emails(cls, alerts):
        pool = Pool()
        datamanager = SMTPDataManager()
        Configuration = pool.get('ir.configuration')
        Lang = pool.get('ir.lang')
        Template = pool.get('ir.action.report')
        context = Transaction().context
        if context.get('email_from') and context['email_from'] is not None:
            from_ = context['email_from']
        else:
            from_ = config.get('email', 'from')
        for alert in alerts:
            to = []
            if alert.send_email:
                name = str(Header(alert.employee.party.name))
                to.append(formataddr((name, alert.employee.email)))
                cc = []
                bcc = []
                languages = set()
                lang, = Lang.search([
                    ('code', '=', Configuration.get_language()),
                ], limit=1)
                languages.add(lang)

                Data = pool.get('ir.model.data')
                template_id = Data.get_id('siim_alert', 'siim_alert_email')
                template = Template(template_id)

                msg = alert._email(from_, to, cc, bcc, languages, template)
                to_addrs = [e for _, e in getaddresses(to + cc + bcc)]
                if to_addrs:
                    if not pool.test:
                        sendmail_transactional(
                            from_, to_addrs, msg, datamanager=datamanager)

    def _email(self, from_, to, cc, bcc, languages, template):
        msg, title = get_email(template, self, languages)
        msg['From'] = from_
        msg['To'] = ', '.join(to)
        msg['Cc'] = ', '.join(cc)
        msg['Bcc'] = ', '.join(bcc)
        msg['Subject'] = Header(title, 'utf-8')
        msg['Auto-Submitted'] = 'auto-generated'
        return msg


class AlertType(ModelSQL, ModelView):
    'Alert Type'
    __name__ = 'siim.alert.type'

    name = fields.Char('Nombre', required=True)


class AlertTypeEmployee(ModelSQL, ModelView):
    'Alert Type Employee'
    __name__ = 'siim.alert.type.employee'

    STATES_ = {
        'readonly': Bool(Eval('template')),
    }

    DEPENDS_ = ['template']

    template = fields.Boolean('Plantilla')
    type = fields.Many2One(
        'siim.alert.type', 'Tipo de Alerta', required=True,
        states=STATES_, depends=DEPENDS_)
    employee = fields.Many2One('company.employee', 'Empleado')
    text_message = fields.Text('Mensaje de texto')
    days_before = fields.Integer(
        '# días para activar alerta',
        domain=[
            ('days_before', '!=', 0)
        ])

    @classmethod
    def default_template(cls):
        return False

    @classmethod
    def defualt_text_message(cls):
        return ''

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    def get_rec_name(self, name):
        title = f"{self.type.name}"
        if self.employee:
            title += f" - {self.employee.party.name}"
        return title
