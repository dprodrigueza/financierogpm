# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.siim_alert.tests.test_siim_alert import suite
except ImportError:
    from .test_siim_alert import suite

__all__ = ['suite']
