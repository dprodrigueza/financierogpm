# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import siim_alert
from . import report

__all__ = ['register']


def register():
    Pool.register(
        siim_alert.Alert,
        siim_alert.AlertType,
        siim_alert.AlertTypeEmployee,
        module='siim_alert', type_='model')
    Pool.register(
        module='siim_alert', type_='wizard')
    Pool.register(
        report.AlertEmail,
        module='siim_alert', type_='report')
