import datetime
from collections import defaultdict
from decimal import Decimal

from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.model import ModelView, fields, ModelSQL
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tools import cursor_dict
from trytond.modules.hr_ec_payslip import utils


__all__ = ['TransparencyMonthlyRemunerationWizardStart',
    'TransparencyMonthlyRemunerationWizard',
    'ManualInputReportWizardStart', 'ManualInputReportWizard',
    'RemunerationHistoryReportWizardStart',
    'RemunerationHistoryReportWizard',
    'ConfigurationMonthlyRemunerations', 'MonthlyRemunerationRule',
    'ConfigurationDynamicReportRulesPayslip', 'DynamicReportRules',
    'DynamicReportRulesPayslipWizardStart',
    'DynamicReportRulesPayslipWizard', 'ReportRulesTypePayslipStart',
    'ReportRulesTypePayslip', 'TemplateToTransparencyExports']


class TransparencyMonthlyRemunerationWizardStart(ModelView):
    ' Transparency Monthly Remuneration Wizard Start'
    __name__ = 'transparency.monthly.remuneration.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    responsable = fields.Many2One('company.employee', 'Responsable',
        domain=[
            ('company', '=', Eval('company')),
            ('contract', '!=', None)
        ], depends=['company'], required=True)
    department = fields.Many2One('company.department', 'Departamento',
        readonly=True)
    phone = fields.Char('Teléfono')
    email = fields.Char('Correo institucional')
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_end_date')
    payslip_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_start_date')
    payslip_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_end_date')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @staticmethod
    def default_responsable():
        Contract = Pool().get('company.contract')
        Employee = Pool().get('company.employee')
        employees = Employee.search([
            ('id', '=', Transaction().context.get('employee'))
        ])
        if employees:
            contracts = Contract.search([
                ('employee', '=', employees[0]),
                ('state', '=', 'done')
            ])
            if contracts:
                return employees[0].id
        return None

    @fields.depends('company', 'fiscalyear', 'period')
    def on_change_company(self, name=None):
        if not self.company:
            self.fiscalyear = None
            self.period = None

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self, name=None):
        if not self.fiscalyear:
            self.period = None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_start_date(self, name=None):
        if self.period:
            return self.period.payslip_start_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_end_date(self, name=None):
        if self.period:
            return self.period.payslip_end_date
        return None

    @fields.depends('responsable', 'department', 'email', 'phone')
    def on_change_responsable(self, name=None):
        if self.responsable:
            # Email
            if self.responsable.email:
                self.email = self.responsable.email

            # Phone
            if self.responsable.institutional_phone:
                phone = self.responsable.institutional_phone
                extension = ''
                if self.responsable.ext_institutional_phone:
                    extension = f'{self.responsable.ext_institutional_phone}'
                    extension = f' EXTENSIÓN {extension}'
                self.phone = f'{phone}{extension}'

            # Department
            if (self.responsable.contract and
                    self.responsable.contract.department):
                self.department = self.responsable.contract.department.id


class TransparencyMonthlyRemunerationWizard(Wizard):
    ' Transparency Monthly Remuneration '
    __name__ = 'transparency.monthly.remuneration.wizard'
    start = StateView('transparency.monthly.remuneration.wizard.start',
        'hr_ec_payslip.transp_monthly_remuneration_wizard_start_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('transparency.monthly.remuneration')

    @classmethod
    def __setup__(cls):
        super(TransparencyMonthlyRemunerationWizard, cls).__setup__()
        cls._error_messages.update({
            'no_remunerations': ('No se han encontrado registros de '
                'remuneraciones mensuales para el período "%(period)s".')
        })

    def do_export_(self, action):
        self.headers_codes = []
        period_start_date = self.start.period_start_date
        period_end_date = self.start.period_end_date
        payslip_start_date = self.start.payslip_start_date
        payslip_end_date = self.start.payslip_end_date
        remunerations = self.get_monthly_remunerations(payslip_start_date,
            payslip_end_date)

        if not remunerations:
            self.raise_user_error('no_remunerations', {
                'period': self.start.period.name
            })

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'responsable': self.start.responsable.id,
            'department': self.start.department.id,
            'phone': self.start.phone,
            'email': self.start.email,
            'period_start_date': period_start_date,
            'period_end_date': period_end_date,
            'payslip_start_date': payslip_start_date,
            'payslip_end_date': payslip_end_date,
            'remunerations': remunerations,
            'headers': self.headers_codes,
        }
        return action, data

    def get_monthly_remunerations(self, payslip_start_date, payslip_end_date):
        pool = Pool()
        MonthlyRemunerations = pool.get('configuration.monthly.remunerations')

        # Configuration
        conf_monthly_remuneration = MonthlyRemunerations.search([()])
        conf_payslip_rules = []
        conf_law_benefit_types = []
        for conf in conf_monthly_remuneration:
            for rule in conf.codes_rules:
                # Payslip rules
                if rule not in conf_payslip_rules:
                    conf_payslip_rules.append(rule)
                # Law benefit types
                lb_type = rule.law_benefit_type
                if lb_type:
                    if lb_type not in conf_law_benefit_types:
                        conf_law_benefit_types.append(lb_type)

        result = defaultdict(lambda: 0)
        payslips = self.get_payslips(payslip_start_date, payslip_end_date)

        if payslips:
            for cont, payslip in enumerate(payslips):
                lines = payslip.lines
                if lines:
                    dict_rule_amount = {}
                    dict_law_benefit_amount = {}
                    amounts_codes = []
                    self.headers_codes = []

                    # Payslip lines
                    for line in lines:
                        if line.rule in conf_payslip_rules:
                            dict_rule_amount[line.rule.code] = line.amount

                    # Accumulated law benefits
                    law_benefits = payslip.law_benefits
                    for lb in law_benefits:
                        if lb.type_ in conf_law_benefit_types:
                            if lb.kind == 'accumulate':
                                dict_law_benefit_amount[lb.type_.code] = (
                                    utils.quantize_currency(lb.amount))

                    for conf in conf_monthly_remuneration:
                        self.headers_codes.append(conf.name)
                        amount = 0
                        for rule in conf.codes_rules:
                            # Rule amount
                            rule_amount = dict_rule_amount.get(rule.code, None)
                            if rule_amount:
                                if rule.type_ == 'deduction':
                                    amount -= rule_amount
                                else:
                                    amount += rule_amount
                            # Accumulated law benefit amount
                            lb_type = rule.law_benefit_type
                            if lb_type:
                                lb_amount = dict_law_benefit_amount.get(
                                    lb_type.code, None)
                                if lb_amount:
                                    if rule.type_ == 'deduction':
                                        amount -= lb_amount
                                    else:
                                        amount += lb_amount

                        # The remuneration transparency report must not show
                        # negative values
                        amount = amount if amount >= 0 else 0
                        amounts_codes.append(amount)

                    rm_annual = (payslip.salary * 12) if payslip.salary else 0

                    result[(cont + 1)] = {
                        'contract_id': payslip.contract.id,
                        'rm_annual': rm_annual,
                        'amounts': amounts_codes
                    }
        return result

    def get_payslips(self, payslip_start_date, payslip_end_date):
        pool = Pool()
        TemplateToTransparencyExports = pool.get('transparency.exports.template')
        codes_templates = TemplateToTransparencyExports.get_code_templates()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_period = Period.__table__()
        tbl_employee = Employee.__table__()
        tbl_party = Party.__table__()

        query = tbl_payslip.join(tbl_period,
            condition=(tbl_period.id == tbl_payslip.period)
        ).join(tbl_payslip_template,
            condition=(tbl_payslip_template.id == tbl_payslip.template),
        ).join(tbl_employee,
            condition=(tbl_employee.id == tbl_payslip.employee),
        ).join(tbl_party,
            condition=(tbl_party.id == tbl_employee.party),
        ).select(
            tbl_payslip.id,
            where=((tbl_period.payslip_start_date >= payslip_start_date) &
                   (tbl_period.payslip_end_date <= payslip_end_date) &
                   (tbl_payslip_template.accumulated_law_benefits == False) &
                   (tbl_payslip_template.type == 'payslip') &
                   (tbl_payslip_template.code.in_(codes_templates)) &
                   (tbl_payslip.state == 'done')),
            order_by=[tbl_party.last_name.asc, tbl_party.first_name.asc]
        )
        cursor.execute(*query)
        payslip_ids = [row['id'] for row in cursor_dict(cursor)]
        payslips = Payslip.browse(payslip_ids)
        return payslips


class ManualInputReportWizardStart(ModelView):
    ' Manual Input Reports Wizard Start'
    __name__ = 'manual.input.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año Fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    type = fields.Many2One('payslip.entry.type', 'Type', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'fiscalyear', 'period')
    def on_change_company(self, name=None):
        if not self.company:
            self.fiscalyear = None
            self.period = None

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self, name=None):
        if not self.fiscalyear:
            self.period = None


class ManualInputReportWizard(Wizard):
    ' Manual Input Reports Wizard'
    __name__ = 'manual.input.report.wizard'

    start = StateView('manual.input.report.wizard.start',
        'hr_ec_payslip.wizard_manual_input_report_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export', 'tryton-executable'),
        ])
    export = StateReport('payslip.entry.export')

    @classmethod
    def __setup__(cls):
        super(ManualInputReportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_remunerations': 'No hay registros de remuneracion mensuales '
                                'para el periodo "%(period)s".'
        })

    def do_export(self, action):
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'type': self.start.type.id,
        }
        return action, data


class RemunerationHistoryReportWizardStart(ModelView):
    'Remuneration History Reports Wizard Start'
    __name__ = 'remuneration.history.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    employee = fields.Many2One('company.employee', 'Empleado', required=True)
    start_date = fields.Date('Fecha inicio',
        domain=[
            ('start_date', '<=', Eval('end_date')),
        ], depends=['end_date'], required=True)
    end_date = fields.Date('Fecha fin',
        domain=[
            ('end_date', '>=', Eval('start_date')),
        ], depends=['start_date'], required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee')

    @staticmethod
    def default_start_date():
        return datetime.date.today().replace(day=1, month=1)

    @staticmethod
    def default_end_date():
        return datetime.date.today()

    @fields.depends('company', 'employee')
    def on_change_company(self, name=None):
        if not self.company:
            self.employee = None


class RemunerationHistoryReportWizard(Wizard):
    'Remuneration History Report Wizard'
    __name__ = 'remuneration.history.report.wizard'

    start = StateView('remuneration.history.report.wizard.start',
        'hr_ec_payslip.wizard_remuneration_history_report_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export', 'tryton-executable'),
        ])
    export = StateReport('remuneration.history.export')

    @classmethod
    def __setup__(cls):
        super(RemunerationHistoryReportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_remuneration_history': ('No se ha encontrado un historial de '
                'remuneraciones mensuales entre el "%(start_date)s" y el '
                '"%(end_date)s".')
        })

    def do_export(self, action):
        company = self.start.company
        employee = self.start.employee
        start_date = self.start.start_date
        end_date = self.start.end_date
        payslips = self.get_remunerations_history(
            company, employee, start_date, end_date)

        if not payslips:
            self.raise_user_error('no_remuneration_history', {
                'start_date': start_date,
                'end_date': end_date
            })

        data = {
            'company': company.id,
            'employee': employee.id,
            'start_date': start_date,
            'end_date': end_date,
            'payslips': payslips,
        }
        return action, data

    def get_remunerations_history(self, company, employee, start_date,
            end_date):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        Period = pool.get('account.period')
        payslip = Payslip.__table__()
        period = Period.__table__()
        query = payslip.join(period,
            condition=(payslip.period == period.id)
        ).select(
            payslip.id,
            where=((period.start_date >= start_date) &
                   (period.end_date <= end_date) &
                   (payslip.company == company.id) &
                   (payslip.employee == employee.id) &
                   (payslip.state == 'done'))
        )
        cursor.execute(*query)
        result = [row['id'] for row in cursor_dict(cursor)]
        return result


class ConfigurationMonthlyRemunerations(ModelSQL, ModelView):
    'Configuration Monthly Remunerations'
    __name__ = 'configuration.monthly.remunerations'
    _history = True

    name = fields.Char('Nombre Rubros', required=True)
    codes_rules = fields.Many2Many('monthly.remunerations.rule',
            'monthly_remuneration', 'rule', 'Reglas', required=True)
    codes_rules_info = fields.Function(fields.Char('Codigos Reglas'),
        'on_change_with_codes_rules_info')

    @fields.depends('codes_rules')
    def on_change_with_codes_rules_info(self, name=None):
        text = ""
        if self.codes_rules:
            for line in self.codes_rules:
                if line.code:
                    text = (
                        f"{text} {line.code}, ")
            if len(text) > 0:
                text = text[:-2]
            return text
        return None


class MonthlyRemunerationRule(ModelSQL):
    'Monthly Remuneration Rule'
    __name__ = 'monthly.remunerations.rule'

    monthly_remuneration = fields.Many2One(
        'configuration.monthly.remunerations', 'Remuneracion',
        ondelete='CASCADE', required=True, select=True)

    rule = fields.Many2One('payslip.rule', 'Regla',
        ondelete='RESTRICT', required=True, select=True)


class ConfigurationDynamicReportRulesPayslip(ModelSQL, ModelView):
    'Configuration Dynamic Report Rules Payslip'
    __name__ = 'configuration.dynamic.report.rules.payslip'
    _history = True

    name = fields.Char('Nombre Reporte', required=True)
    codes_rules = fields.Many2Many('dynamic.report.rules', 'config_report',
        'rule', 'Reglas', required=True)
    codes_rules_info = fields.Function(fields.Char('Codigos Reglas'),
        'on_change_with_codes_rules_info')

    @fields.depends('codes_rules')
    def on_change_with_codes_rules_info(self, name=None):
        text = ""
        if self.codes_rules:
            for line in self.codes_rules:
                if line.code:
                    text = (
                        f"{text} {line.code}, ")
            if len(text) > 0:
                text = text[:-2]
            return text
        return None


class DynamicReportRules(ModelSQL):
    'Dynamic Report Rules'
    __name__ = 'dynamic.report.rules'

    config_report = fields.Many2One(
        'configuration.dynamic.report.rules.payslip', 'Report',
        ondelete='CASCADE', required=True, select=True)
    rule = fields.Many2One('payslip.rule', 'Regla',
        ondelete='RESTRICT', required=True, select=True)


class DynamicReportRulesPayslipWizardStart(ModelView):
    'DynamicReport Rules Payslip Wizard Start'
    __name__ = 'dynamic.report.rules.payslip.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_end_date')
    payslip_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_start_date')
    payslip_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_end_date')
    rules = fields.One2Many('payslip.rule', None, 'Reglas')
    type_report = fields.Many2One('configuration.dynamic.report.rules.payslip',
        'Tipo de reporte')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'fiscalyear', 'period')
    def on_change_company(self, name=None):
        if not self.company:
            self.fiscalyear = None
            self.period = None

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self, name=None):
        if not self.fiscalyear:
            self.period = None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_start_date(self, name=None):
        if self.period:
            return self.period.payslip_start_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_end_date(self, name=None):
        if self.period:
            return self.period.payslip_end_date
        return None

    @fields.depends('type_report')
    def on_change_with_rules(self, name=None):
        if self.type_report:
            list_ids_rules = []
            for rule in self.type_report.codes_rules:
                list_ids_rules.append(rule.id)
            return list_ids_rules
        return None


class DynamicReportRulesPayslipWizard(Wizard):
    'Dynamic Report Rules Payslip'
    __name__ = 'dynamic.report.rules.payslip.wizard'
    start = StateView('dynamic.report.rules.payslip.wizard.start',
                      'hr_ec_payslip.dynamic_report_rules_payslip_wizard',
                      [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Exportar', 'export_', 'tryton-executable'),
                      ])
    export_ = StateReport('dynamic.report.rules.payslip')

    @classmethod
    def __setup__(cls):
        super(DynamicReportRulesPayslipWizard, cls).__setup__()
        cls._error_messages.update({
            'no_amounts': ('No se han encontrado valores para las reglas '
                           'seleccionadas en el período "%(period)s".')
        })

    def do_export_(self, action):
        self.headers_codes = []
        payslip_start_date = self.start.payslip_start_date
        payslip_end_date = self.start.payslip_end_date
        dict_rules = self.get_dict_rules(payslip_start_date, payslip_end_date)

        if not dict_rules:
            self.raise_user_error('no_amounts', {
                'period': self.start.period.name
            })

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'dict_rules': dict_rules,
            'headers': self.headers_codes,
        }
        return action, data

    def get_dict_rules(self, payslip_start_date, payslip_end_date):
        result = defaultdict(lambda: 0)
        payslips = self.get_payslips(payslip_start_date, payslip_end_date)
        if payslips:
            for cont, payslip in enumerate(payslips):
                lines = payslip.lines
                if lines:
                    dict_rule_amount = {}
                    for line in lines:
                        dict_rule_amount[line.rule.code] = line.amount
                    for lines_law in payslip.law_benefits:
                        if lines_law.kind == 'accumulate':
                            key = f"{lines_law.type_.code}_accumulate"
                            dict_rule_amount[key] = lines_law.amount
                    amounts_codes = []
                    self.headers_codes = []
                    for rule in self.start.rules:
                        self.headers_codes.append(rule.name)
                        amount = 0
                        if dict_rule_amount.get(rule.code):
                            amount = dict_rule_amount.get(rule.code)
                        amounts_codes.append(amount)
                    result[(cont + 1)] = {
                        'payslip_id': payslip.id,
                        'amounts': amounts_codes
                    }
        return result

    def get_payslips(self, payslip_start_date, payslip_end_date):
        pool = Pool()
        TemplateToTransparencyExports = pool.get('transparency.exports.template')
        codes_templates = TemplateToTransparencyExports.get_code_templates()
        Payslip = pool.get('payslip.payslip')
        payslips = Payslip.search([
            ('period.payslip_start_date', '>=', payslip_start_date),
            ('period.payslip_end_date', '<=', payslip_end_date),
            ('state', '!=', 'cancel'),
            ('template.accumulated_law_benefits', '=', False),
            ('template.code', 'in', codes_templates),
        ], order=[('employee.party', 'ASC')])
        return payslips


class ReportRulesTypePayslipStart(ModelView):
    'Report Rules Type Payslip Start'
    __name__ = 'report.rules.type.payslip.start'

    company = fields.Many2One('company.company', 'Empresa', required=True,
        states={
            'readonly': True
        })
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_period_end_date')
    payslip_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_start_date')
    payslip_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'on_change_with_payslip_end_date')
    payslip_template = fields.Many2One('payslip.template', 'Plantilla Rol',
        required=True)
    lines_rules = fields.One2Many('payslip.template.line', None, 'Reglas',
        domain=[
            ('template', '=', Eval('payslip_template')),
        ],
        states={
            'required': True
        }, depends=['payslip_template'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'fiscalyear', 'period')
    def on_change_company(self, name=None):
        if not self.company:
            self.fiscalyear = None
            self.period = None

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self, name=None):
        if not self.fiscalyear:
            self.period = None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_start_date(self, name=None):
        if self.period:
            return self.period.payslip_start_date
        return None

    @fields.depends('period')
    def on_change_with_payslip_end_date(self, name=None):
        if self.period:
            return self.period.payslip_end_date
        return None


class ReportRulesTypePayslip(Wizard):
    'Report Rules Type Payslip'
    __name__ = 'report.rules.type.payslip'
    start = StateView('report.rules.type.payslip.start',
        'hr_ec_payslip.report_rules_type_payslip', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('report.rules.type.payslip')

    @classmethod
    def __setup__(cls):
        super(ReportRulesTypePayslip, cls).__setup__()

    def do_export_(self, action):
        self.headers_codes = []

        ids_rules = []
        for line in self.start.lines_rules:
            ids_rules.append(line.rule.id)

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period_id': self.start.period.id,
            'template_id': self.start.payslip_template.id,
            'rules_id': ids_rules,
        }
        return action, data


class TemplateToTransparencyExports(ModelSQL, ModelView):
    'Template To Transparency Exports'
    __name__ = 'transparency.exports.template'

    template = fields.Many2One(
        'payslip.template', 'Plantillas de roles',
        states={
            'required': True,
        },
        domain=[
            ('accumulated_law_benefits', '=', False)
        ]
    )

    @classmethod
    def get_code_templates(cls):
        templates = cls.search([])
        codes = []
        for template in templates:
            codes.append(template.template.code)
        return codes