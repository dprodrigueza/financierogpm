from decimal import Decimal
import pandas as pd
from io import BytesIO
from collections import defaultdict
from dateutil.relativedelta import relativedelta
from datetime import datetime

from sql import Literal, Window
from sql.aggregate import Sum
from sql.functions import CurrentTimestamp, RowNumber

from trytond.pyson import Eval, Bool, If
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, Button, StateTransition, StateView
from trytond.tools import file_open, reduce_ids, grouped_slice, cursor_dict

from trytond.modules.hr_ec.hr_ec import month_hours
from trytond.modules.hr_ec_payslip import utils

__all__ = [
    'Contract', 'ContractPayslipTemplate', 'MedicalCertificate',
    'PayslipTemplateAccountBudget', 'PayslipRuleAccountBudget',
    'TypeIncomeDeduction', 'ContractFixedIncome', 'ContractFixedDeduction',
    'EmployerTypeCode', 'WorkingRelationship', 'SectorialActivity',
    'OvertimeType', 'CommissionSubrogationType', 'EmployeeLoan',
    'LoanType', 'LoanTypeAccount', 'Retired', 'RetiredPayslipTemplate',
    'WorkshiftDelayLine', 'ManagePayslipTemplatesContractsWizardStart',
    'ManagePayslipTemplatesContractsWizardSucceed',
    'ManagePayslipTemplatesContractsWizard', 'RulePrincipal',
    'BudgetAnalisysByItem', 'BudgetAnalisysByItemContext'
]

_LAW_BENEFITS_PAYMENT_FORMS = [
    ('monthlyse', 'Mensualizado'),
    ('accumulate', 'Acumulado'),
]

CENT = Decimal('0.01')


# TODO: remove hard coded normalization, instead use:
# unicodedata.normalize('NFKD', value)
def normalize_string(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ü", "u"),
        ("Á", "A"),
        ("É", "E"),
        ("Í", "I"),
        ("Ó", "O"),
        ("Ú", "U"),
        ("ü", "u"),
        ("Ü", "U"),
        ("ñ", "n"),
        ("Ñ", "N"),
    )
    for a, b in replacements:
        s = s.replace(a, b)
    return s


def get_records_considered_in_payslips(model, items, search_field, states=[]):
    search_field = str(search_field)
    result = defaultdict(lambda: None)
    for item in items:
        recovered_records = model.search([
            (search_field, '=', item),
            ('was_considered', '=', True),
            ('payslip.state', 'in', states)
        ])
        if recovered_records:
            result[item.id] = recovered_records[0].payslip.id
    return result


def set_field_states(fields, _new_value, state_key=None):
    for field in fields:
        if not field.states.get(state_key, None):
            field.states.update({
                'readonly': _new_value
            })
        else:
            if field.states[state_key] != True:
                field.states[state_key] &= _new_value


def set_field_depends(fields, _depends, operation='add'):
    for field in fields:
        if operation == 'add':
            field.depends += _depends
        elif operation == 'replace':
            field.depends = _depends


class Contract(metaclass=PoolMeta):
    __name__ = 'company.contract'
    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']

    budget = fields.Many2One('public.budget', 'Partida',
        domain=[
            ('kind', '=', 'other'),
        ], states=_states, depends=_depends)
    budget_activity = fields.Function(
        fields.Many2One('public.planning.unit', 'Actividad'),
        'get_budget_field', searcher='search_budget_field')
    budget_parent = fields.Function(
        fields.Many2One('public.budget', 'Partida padre'),
        'get_budget_field', searcher='search_budget_field')
    payslip_templates = fields.Many2Many('company.contract.payslip.template',
        'contract', 'payslip_template', 'Plantillas de rol de pagos',
        states=_states, depends=_depends, datetime_field='finalized_date')
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuenta y Partida de Plantilla de Rol', states=_states,
        datetime_field='finalized_date')
    lines_income = fields.One2Many('company.contract.fixed.income',
        'contract', 'Ingresos fijos', states=_states, depends=_depends)
    lines_deduction = fields.One2Many('company.contract.fixed.deduction',
        'contract', 'Deducciones fijas', states=_states, depends=_depends)
    min_salary_rate_to_receive = fields.Numeric(
        'Rango mínimo de salario a recibir', help='Indica el valor mínimo '
        'de salario que un empleado puede recibir en el rol de pagos después de'
        'aplicar las deducciones. El valor mínimo se obtiene aplicando el '
        'porcentaje especificado en este campo sobre el salario que percibe el '
        'servidor.',
        domain=[
            ('min_salary_rate_to_receive', '>=', 0.0)
        ], states=_states, depends=_depends + ['min_salary_rate_to_receive'],
        digits=(16, 2))
    maximum_percentage_loan = fields.Numeric(
        'Máximo porcentaje de préstamo', help='Permite obtener el valor máximo '
        'de préstamo o anticipo al cual el servidor puede acceder. El valor '
        'máximo se obtiene aplicando el porcentaje definido en este campo '
        'sobre el valor máximo de préstamo definido en las configuraciones de '
        'la relación laboral. Esto permite definir configuraciones de valores '
        'máximos a nivel de contrato y no a nivel general (por relación '
        'laboral), si se desea omitir esta configuración, se deberá '
        'especificar en este campo el valor de 100%.',
        domain=[
            ('maximum_percentage_loan', '>=', Decimal("0")),
            ('maximum_percentage_loan', '<=', Decimal("1")),
        ], states=_states, depends=_depends)
    xiii = fields.Selection(_LAW_BENEFITS_PAYMENT_FORMS, 'XIII', states=_states,
        depends=_depends)
    xiv = fields.Selection(_LAW_BENEFITS_PAYMENT_FORMS, 'XIV', states=_states,
        depends=_depends)
    have_fr = fields.Boolean('¿Tiene fondos de reserva?', states=_states,
        depends=_depends)
    absence_apply = fields.Boolean('¿Aplican faltas en el Rol?', states=_states,
        depends=_depends)
    fr = fields.Selection(_LAW_BENEFITS_PAYMENT_FORMS, 'Fondos de reserva',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': ~Eval('have_fr')},
        depends=_depends + ['have_fr'])
    xiii_translated = xiii.translated('xiii')
    xiv_translated = xiv.translated('xiv')
    fr_translated = fr.translated('fr')
    register_date_iess = fields.Date('Fecha de registro',
        help='Fecha de registro en el IESS', states=_states)
    workingday_iess = fields.Selection([
        (None, ''),
        ('1', 'Jornada Normal'),
    ], 'Jornada', states=_states)
    code_iess = fields.Selection([
        (None, ''),
        ('R', 'Ley de Seguro Social Vigente-Ley 21'),
        ('M', 'Seguro Mixto'),
    ], 'Código de seguro social', states=_states)
    employer_type_code_iess = fields.Many2One('employer.type.code',
        'Código de tipo de empleador', states=_states)
    working_relationship_iess = fields.Many2One('working.relationship',
        'Relación de trabajo', domain=[
            ('employer_type', '=', Eval('employer_type_code_iess'))],
            depends=['employer_type_code_iess'], states=_states)
    code_sectoral_activity = fields.Many2One('sectorial.activity',
        'Código de actividad sectorial', states=_states)
    origin_payment_iess = fields.Selection([
        (None, ''),
        ('P', 'Fondos Propios'),
        ('E', 'Presupuesto del Estado'),
    ], 'Origen de pago', states=_states)

    @classmethod
    def __setup__(cls):
        super(Contract, cls).__setup__()

        cls._error_messages.update({
            'not_cancel_used_in_payslip': (
                'No puede puede cancelar un contrato que ya está siendo usado '
                'en un rol de pagos.'),
        })

    @classmethod
    def default_min_salary_rate_to_receive(cls):
        return 0

    @classmethod
    def default_xiii(cls):
        return 'monthlyse'

    @classmethod
    def default_absence_apply(cls):
        return True

    @classmethod
    def default_maximum_percentage_loan(cls):
        return 1

    @classmethod
    def default_xiv(cls):
        return 'monthlyse'

    @classmethod
    def default_fr(cls):
        return 'monthlyse'

    @classmethod
    def default_workingday_iess(cls):
        return None

    def get_budget_field(self, name):
        if self.budget:
            field = getattr(self.__class__, name)
            if name.startswith('budget_'):
                name = name[7:]
            value = getattr(self.budget, name)
            if isinstance(value, ModelSQL):
                if field._type == 'reference':
                    return str(value)
                return value.id
            return value
        return None

    @classmethod
    def search_budget_field(cls, name, clause):
        nested = clause[0].lstrip(name)
        if name.startswith('budget_'):
            name = name[7:]
        return [('budget.' + name + nested,) + tuple(clause[1:])]

    def get_hour_cost(self, name):
        if self.salary:
            return self.salary / month_hours
        return Decimal(0)

    @classmethod
    def done(cls, contracts):
        pool = Pool()
        CD_Tem_Budget = pool.get('company.department.template.budget')
        for c in contracts:
            templates = CD_Tem_Budget.search([
                ('department', '=', c.department.id),
                ('work_relationship', '=', c.work_relationship.id),
                ('template_budget.payslip_template', 'in', c.payslip_templates),
            ])

            if c.payslip_template_account_budget:
                continue

            if len(templates) == 1:
                c.payslip_template_account_budget = templates[0].template_budget
            else:
                cls.raise_user_warning('not_template_account_%d' % c.id,
                    'not_template_account', {})
        cls.save(contracts)
        return super(Contract, cls).done(contracts)

    @classmethod
    def cancel(cls, contracts):
        # Check that the contract is not used in a payslip
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        tbl_payslip = Payslip.__table__()

        for contract in contracts:
            query = tbl_payslip.select(
                tbl_payslip.id,
                where=((tbl_payslip.contract == contract.id) &
                      ~(tbl_payslip.state.in_(['cancel']))))
            cursor.execute(*query)
            ids = [row['id'] for row in cursor_dict(cursor)]
            if ids:
                cls.raise_user_error('not_cancel_used_in_payslip', {
                    'contract': contract.rec_name.upper()
                })
        # Super
        super(Contract, cls).cancel(contracts)


class ContractPayslipTemplate(ModelSQL, ModelView):
    'Clase Intermedia Contract - PayslipTemplate'
    __name__ = 'company.contract.payslip.template'

    contract = fields.Many2One('company.contract', 'Contrato',
        ondelete='CASCADE')
    payslip_template = fields.Many2One('payslip.template',
        'Plantilla de rol de pagos', ondelete='CASCADE')
    lines = fields.One2Many('company.contract.payslip.template',
        'contract_payslip_template', 'Cambio de reglas a contrato')

    # Contract fields
    contract_department = fields.Function(
        fields.Many2One('company.department', 'Departamento'),
        'get_contract_field', searcher='search_contract_field')
    contract_position = fields.Function(
        fields.Many2One('company.position', 'Puesto de trabajo'),
        'get_contract_field', searcher='search_contract_field')
    contract_work_relationship = fields.Function(
        fields.Many2One('company.work_relationship', 'Relación laboral'),
        'get_contract_field', searcher='search_contract_field')
    contract_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('done', 'Realizado'),
            ('disabled', 'Inhabilitado'),
            ('finalized', 'Finalizado'),
            ('cancel', 'Cancelado')
        ], 'Estado de contrato'), 'get_contract_field',
        searcher='search_contract_field')
    contract_start_date = fields.Function(fields.Date('Fecha inicio'),
        'get_contract_field', searcher='search_contract_field')
    contract_end_date = fields.Function(fields.Date('Fecha fin'),
        'get_contract_field', searcher='search_contract_field')

    @classmethod
    def __setup__(cls):
        super(ContractPayslipTemplate, cls).__setup__()
        cls._order.insert(0, ('contract', 'ASC'))
        cls._order.insert(1, ('payslip_template', 'ASC'))

    def get_contract_field(self, name):
        field = getattr(self.__class__, name)
        if name.startswith('contract_'):
            name = name[9:]
        value = getattr(self.contract, name)
        if isinstance(value, ModelSQL):
            if field._type == 'reference':
                return str(value)
            return value.id
        return value

    @classmethod
    def search_contract_field(cls, name, clause):
        nested = clause[0].lstrip(name)
        if name.startswith('contract_'):
            name = name[9:]
        return [('contract.' + name + nested,) + tuple(clause[1:])]

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('payslip_template',) + tuple(clause[1:]),
            ('contract',) + tuple(clause[1:]),
            ('contract_department.rec_name',) + tuple(clause[1:]),
            ('contract_position.rec_name',) + tuple(clause[1:]),
            ('contract_work_relationship.rec_name',) + tuple(clause[1:]),
        ]


class PayslipTemplateAccountBudget(ModelSQL, ModelView):
    'Cuentas y Partidas de Plantilla de Rol'
    __name__ = 'payslip.template.account.budget'
    _history = True

    company = fields.Many2One('company.company', 'Empresa', required=True,
        domain=[(
            'id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)), ])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True,
        domain=[
            ('company', '=', Eval('company')),
        ],
        depends=['company'])
    name = fields.Char('Nombre')
    payslip_template = fields.Many2One('payslip.template',
        'Plantilla de rol de pagos', ondelete='CASCADE')
    lines = fields.One2Many('payslip.rule.account.budget',
        'template_account_budget', 'Cuentas y Partidas de Reglas de Rol')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_fiscalyear(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        return context.get(
            'fiscalyear',
            FiscalYear.find(context.get('company'), exception=False))


class PayslipRuleAccountBudget(ModelSQL, ModelView):
    'Cuentas y Partidas de Regla de Rol'
    __name__ = 'payslip.rule.account.budget'
    _history = True
    template_account_budget = fields.Many2One('payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol', ondelete='CASCADE')
    payslip_rule = fields.Many2One('payslip.rule',
        'Regla',
        states={
            'required': True,
        },
        depends=['template_account_budget', 'rules_allowed'])
    rules_allowed = fields.Function(fields.Many2Many('payslip.rule',
        None, None, 'Reglas del rol'),
        'on_change_with_rules_allowed')
    payable_account = fields.Many2One('account.account', 'Cuenta Credito',
        domain=[
            ('kind', '!=', 'view'),
            ['OR',
                ('budget_credit', 'parent_of', Eval('budget_debit'), 'parent'),
                ('code', 'like', '112%'),
            ]
        ],
        states={
            'required': True,
        }, depends=['budget_debit'])
    expense_account = fields.Many2One('account.account', 'Cuenta Debito',
        domain=[
            ('kind', 'in', ['expense', 'payable']),
        ],
        states={
            'required': True,
        })
    budget_debit = fields.Function(fields.Many2One('public.budget', 'Partida'),
        'get_budget_debit')
    receivable_account = fields.Many2One('account.account', 'Cuenta por Cobrar',
        domain=[
            ('kind', '!=', 'view'),
        ],
        states={
            'required': Eval('payslip_rule.type_') == 'deduction',
        }, help='Solo necesario en caso de que la regla signifique un ingreso'
                'para el ente público.')
    revenue_account = fields.Many2One('account.account', 'Cuenta de Ingreso',
        domain=[
            ('kind', '!=', 'view'),
            ('kind', '!=', 'other'),
        ],
        states={
            'required': Eval('payslip_rule.type_') == 'deduction',
        }, help='Solo necesario en caso de que la regla signifique un ingreso'
                'para el ente público.')
    budget = fields.Many2One('public.budget', 'Partida',
        states={
            'required': Eval('payable_account.type').in_(['expense', 'payable'])
        },
        domain=[
            ('kind', '=', 'view'),
            ('id', 'in', Eval('budgets_allowed')),
        ], depends=['expense_account', 'payable_account', 'budgets_allowed'],
    )
    budgets_allowed = fields.Function(fields.Many2Many('public.budget', None,
        None, 'Partidas permitidas'), 'on_change_with_budgets_allowed')

    @classmethod
    def __setup__(cls):
        super(PayslipRuleAccountBudget, cls).__setup__()
        cls._error_messages.update({
            'income_with_default_party': ('La cuenta no debe tener tercero por '
                                          'defecto'),
        })

    @fields.depends('payable_account', 'expense_account')
    def on_change_with_budgets_allowed(self, name=None):
        budgets = []
        pool = Pool()
        Budget = pool.get('public.budget')
        if (self.expense_account and self.expense_account.budget_debit
                and self.expense_account.budget_debit.type == 'expense'):
            budgets.append(self.expense_account.budget_debit.id)
            bs = Budget.search([
                ('kind', '=', 'view'),
                ('parent', 'child_of', [self.expense_account.budget_debit.id]),
            ])
            for b in bs:
                budgets.append(b.id)
        return budgets

    @fields.depends('template_account_budget')
    def on_change_with_rules_allowed(self, name=None):
        rules = []
        if (self.template_account_budget
                and self.template_account_budget.payslip_template):
            for line in self.template_account_budget.payslip_template.lines:
                rules.append(line.rule.id)
        return rules

    @fields.depends('expense_account', 'payslip_rule')
    def on_change_expense_account(self):
        if (self.expense_account and self.expense_account.budget_debit
                and self.expense_account.budget_debit.type == 'expense'):
            self.budget = self.expense_account.budget_debit
        if (self.expense_account and self.payslip_rule
            and self.payslip_rule.type_ == 'income'
            and self.expense_account.default_party):
            self.raise_user_error('income_with_default_party')

    @fields.depends('expense_account')
    def on_change_with_budget_debit(self, name=None):
        if (self.expense_account and self.expense_account.kind == 'expense'
            and self.expense_account.budget_debit):
            return self.expense_account.budget_debit.id
        return None

    @classmethod
    def get_budget_debit(cls, rules, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Account = pool.get('account.account')
        account = Account.__table__()
        table = cls.__table__()

        budgets = defaultdict(lambda: None)
        ids = [r.id for r in rules]
        for sub_ids in grouped_slice(ids):
            where = reduce_ids(table.id, sub_ids)
            query = table.join(account,
                condition=((table.expense_account == account.id)
                           & (account.kind == 'expense'))
            ).select(table.id, account.budget_debit,
                where=where)
            cursor.execute(*query)
            for id, budget in cursor.fetchall():
                budgets[id] = budget
        return {
            'budget_debit': budgets,
        }


class RulePrincipal(ModelSQL, ModelView):
    'Rule Principal'
    __name__ = 'payslip.rule.principal'

    rule = fields.Many2One(
        'payslip.rule', 'Rubros principales', states={
            'required': True,
        },
        domain=[
            ('type_', '=', 'income'),
            ('active', '=', True)
        ]
    )


class TypeIncomeDeduction(ModelSQL, ModelView):
    'Type Income Deduction'
    __name__ = 'company.contract.type.income.deduction'
    _history = True

    rule = fields.One2Many('payslip.rule', 'fixed_income_deduction_type',
                           'Regla')
    description = fields.Char('Nombre', required=True)
    code = fields.Char('Código', required=True)
    kind = fields.Selection([
        ('income', 'Ingreso'),
        ('deduction', 'Deducción'),
    ], 'Tipo', required=True)
    max_value = fields.Numeric('Valor Máximo', digits=(16, 2))

    @classmethod
    def __setup__(cls):
        super(TypeIncomeDeduction, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('type_income_deduction_code_uniq', Unique(t, t.code),
             'El código ingresado para este tipo de ingreso/deducción fija ya '
             'existe!'),
        ]

        cls._error_messages.update({
            'rules_update': ('Se van actualizar las siguientes reglas:\n'
                             '%(names)s'),
        })

    def get_rec_name(self, name):
        return '%s (%s)' % (self.description, self.code)

    @fields.depends('description')
    def on_change_with_code(self, name=None):
        if self.description:
            code = normalize_string(self.description).lower().replace(' ', '_')
            return code
        else:
            return None

    @classmethod
    def write(cls, incomes_deductions, vals, *args):

        PayslipRule = Pool().get('payslip.rule')

        to_save = []
        if incomes_deductions:
            for income_deduction in incomes_deductions:
                rule_names_update = ''
                for rule in income_deduction.rule:
                    rule_names_update += '\n[' + rule.name + ']'

                generals_rules = PayslipRule.search([
                    ('is_manual_entry', '!=', True),
                    ('is_fixed_income_deduction', '!=', True),
                    ('is_law_benefit', '!=', True), ])

                for rule in generals_rules:
                    if rule.expression.find(income_deduction.code) > 0 or (
                            rule.condition.find(income_deduction.code) > 0):
                        rule_names_update += '\n[' + rule.name + ']'

                if rule_names_update != '':
                    cls.raise_user_warning('rules_update', 'rules_update', {
                        'names': rule_names_update})

                for rule in generals_rules:
                    if rule.expression.find(income_deduction.code) > 0 or (
                            rule.condition.find(income_deduction.code) > 0):
                        expression = rule.expression
                        expression = expression.replace(income_deduction.code,
                                                        vals['code'])
                        condition = rule.condition
                        condition = condition.replace(income_deduction.code,
                                                      vals['code'])
                        rule.expression = expression
                        rule.condition = condition
                        to_save.append(rule)

                for rule in income_deduction.rule:

                    if vals.get('code'):
                        expression = rule.expression
                        expression = expression.replace(income_deduction.code,
                                                        vals['code'])

                        condition = rule.condition
                        condition = condition.replace(income_deduction.code,
                                                      vals['code'])
                        rule.expression = expression
                        rule.condition = condition

                    if vals.get('kind'):
                        rule.type_ = vals['kind']
                        expression = rule.expression
                        if vals['kind'] == 'income':
                            expression = expression.replace(
                                'fixed_deduction_amounts',
                                'fixed_income_amounts')
                        elif vals['kind'] == 'deduction':
                            expression = expression.replace(
                                'fixed_income_amounts',
                                'fixed_deduction_amounts')

                        rule.expression = expression

                    to_save.append(rule)

        PayslipRule.save(to_save)

        super(TypeIncomeDeduction, cls).write(incomes_deductions, vals, *args)


class IncomeDeductionTimeMixin(ModelSQL, ModelView):
    'IncomeDeductionTimeMixin'

    contract = fields.Many2One('company.contract', 'Contrato',
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])
    reason = fields.Char('Razón', required=True,
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])
    amount = fields.Numeric('Valor', digits=(16, 2), required=True,
        domain=[
            ('amount', '>=', 0)
        ],
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])
    is_active = fields.Boolean('Activo',
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])
    date = fields.Date("Fecha Inicio", required=False, select=True,
        states={
            'invisible': True,
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])

    @classmethod
    def __setup__(cls):
        super(IncomeDeductionTimeMixin, cls).__setup__()
        cls._error_messages.update({
            'error_max_value': (
                'El registro %(id)s, no puede exceder el Valor Máximo de Carga '
                'en Tipo de Entrada: %(max_value)s.'),
        })

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_is_active():
        return True


class ContractFixedIncome(IncomeDeductionTimeMixin):
    'Contract Fixed Income'
    __name__ = 'company.contract.fixed.income'
    _history = True

    type_income_deduction = fields.Many2One(
        'company.contract.type.income.deduction', 'Tipo', required=True,
        domain=[
            ('kind', '=', 'income'),
        ],
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])

    @classmethod
    def validate(cls, fixed_income):
        for income in fixed_income:
            income.check_value()

    @fields.depends('amount', 'type_income_deduction', 'max_value')
    def on_change_with_amount(self, name=None):
        if self.amount:
            self.check_value()
        return self.amount

    def check_value(self):
        if self.type_income_deduction.max_value:
            if self.type_income_deduction.max_value < self.amount:
                self.raise_user_error('error_max_value', {
                    'max_value': self.type_income_deduction.max_value,
                    'id': self.id,
                    })


class ContractFixedDeduction(IncomeDeductionTimeMixin):
    'Contract Fixed Deduction'
    __name__ = 'company.contract.fixed.deduction'
    _history = True

    type_income_deduction = fields.Many2One(
        'company.contract.type.income.deduction', 'Tipo', required=True,
        domain=[
            ('kind', '=', 'deduction'),
        ],
        states={
            'readonly': Bool(
                Eval('_parent_contract', {}).get(
                    'state', -1).in_(['done', 'cancel']))
        }, depends=['contract'])

    @classmethod
    def validate(cls, fixed_deduction):
        for deduction in fixed_deduction:
            deduction.check_value()

    @fields.depends('amount', 'type_income_deduction', 'max_value')
    def on_change_with_amount(self, name=None):
        if self.amount:
            self.check_value()
        return self.amount

    def check_value(self):
        if self.type_income_deduction.max_value:
            if self.type_income_deduction.max_value < self.amount:
                self.raise_user_error('error_max_value', {
                    'max_value': self.type_income_deduction.max_value,
                    'id': self.id,
                })


class UploadLawBenefitStart(ModelView):
    'Upload Law Benefit Start'
    __name__ = 'company.contract.law.benefit.template.start'

    source_file = fields.Binary('Archivo a cargar', required=True)
    template = fields.Binary('Plantilla de archivo',
        filename='template_filename', file_id='template_path', readonly=True)
    template_filename = fields.Char('PlantillaBeneficioDeLey',
        states={
            'invisible': True,
        })
    template_path = fields.Char('Ruta de ubicación', readonly=True)
    read_column_has_fr = fields.Boolean(
        'Leer columna ¿Tiene derecho a fondos de reserva?')
    read_column_payment_form_fr = fields.Boolean(
        'Leer columna ¿Acumula fondos de reserva?')

    @staticmethod
    def default_template_path():
        return './data/plantilla_actualizar_fondos_reserva.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('plantilla_actualizar_fondos_reserva', 'xlsx')

    @staticmethod
    def default_template():
        path = 'hr_ec_payslip/data/plantilla_actualizar_fondos_reserva.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @staticmethod
    def default_read_column_has_fr():
        return True

    @staticmethod
    def default_read_column_payment_form_fr():
        return True


class UploadLawBenefitSucceed(ModelView):
    'Upload Law Benefit Succeed'
    __name__ = 'company.contract.law.benefit.template.succeed'


class UploadLawBenefit(Wizard):
    'Upload Law Benefit Start'
    __name__ = 'company.contract.law.benefit.template'

    start = StateView('company.contract.law.benefit.template.start',
        'hr_ec_payslip.upload_law_benefit_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    succeed = StateView('company.contract.law.benefit.template.succeed',
        'hr_ec_payslip.upload_law_benefit_start_succeed_view_form', [
            Button('Ok', 'end', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UploadLawBenefit, cls).__setup__()
        cls._error_messages.update({
            'empty_data': ('Uno de los campos en la fila: %(field)s está vació '
                'en el archivo.'),
            'format_error': ('La linea %(field)s , tiene un problema de '
                'formato en uno de los campos.'),
            'index_error': ('No se a encontrado registros con los datos de la '
                'línea %(field)s'),
            'ci_error': ('No existe empleado registrado con número de cédula '
                '%(value)s.'),
            })

    def transition_import_(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        Contract = pool.get('company.contract')

        file_data = fields.Binary.cast(self.start.source_file)
        read_column_has_fr = self.start.read_column_has_fr
        read_column_payment_form_fr = self.start.read_column_payment_form_fr

        field_names = ['ci', 'complete_name', 'ac_fr', 'fr']
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        df = df.fillna(False)
        for row in df.itertuples():
            num_row = row[0]
            ci = str(row[1])
            ci = ci if len(ci) == 10 else f"{0}{ci}"
            complete_name = row[2]
            ac_fr = row[3]
            fr = row[4]
            if ci and complete_name and fr and ac_fr:
                try:
                    employee = Employee.search(
                        [('identifier', '=', str(ci))])
                    if not employee:
                        self.raise_user_error('ci_error', {
                            'value': str(ci)
                            })
                    contracts = Contract.search([
                        ('employee', '=', employee[0])
                    ])
                    # Get contracts that can be updated in this period
                    contracts = self.get_contracts_to_update(contracts)
                    for contract in contracts:
                        if read_column_has_fr:
                            if fr == "NO":
                                contract.have_fr = False
                            elif fr == "SI":
                                contract.have_fr = True
                        if read_column_payment_form_fr:
                            if ac_fr == "NO":
                                contract.fr = 'monthlyse'
                            elif ac_fr == "SI":
                                contract.fr = 'accumulate'
                        contract.save()
                except ValueError:
                    self.raise_user_error('format_error', {
                        'field': num_row
                        })
                except IndexError:
                    self.raise_user_error('index_error', {
                        'field': num_row
                        })
            else:
                self.raise_user_error('empty_data', {
                    'field': num_row
                })
        return 'succeed'

    def get_contracts_to_update(self, contracts):
        '''
        It is possible update the information of reserve funds of finalized
        contracts whose finalized month is longer than the current month. And
        of the contracts that ended in the current period, it must be updated
        as long as the payslip configuration for this period considers the
        finalized contracts.
        '''
        pool = Pool()
        Date = pool.get('ir.date')
        SeveraConfiguration = Pool().get('payslip.several.configurations')

        payslips_conf = SeveraConfiguration(1)
        consider_finalized_contracts_payslip_month = (payslips_conf.
            consider_finalized_contracts_during_payslip_period)

        result = []
        today = Date.today()
        current_month = today.month
        current_year = today.year

        for contract in contracts:
            if contract.state != 'cancel':
                if contract.state == 'finalized':
                    if contract.end_date:
                        end_date_month = contract.end_date.month
                        end_date_year = contract.end_date.year
                        if current_year == end_date_year:
                            if current_month < end_date_month:
                                result.append(contract)
                            elif current_month == end_date_month:
                                if consider_finalized_contracts_payslip_month:
                                    result.append(contract)
                        elif current_year < end_date_year:
                            result.append(contract)
                else:
                    result.append(contract)
        return result


class CommissionSubrogation(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation'

    @classmethod
    def __setup__(cls):
        super(CommissionSubrogation, cls).__setup__()
        cls._error_messages.update({
            'used_on_payslip': ('No puede "Cancelar" este registro ya que una '
                'de sus lineas se usa en "%(payslip)s".'),
        })

    @classmethod
    def cancel(cls, items):
        for item in items:
            if item.detail:
                for line in item.detail:
                    if line.payslip:
                        cls.raise_user_error('used_on_payslip', {
                            'payslip': line.payslip.rec_name
                        })
        super(CommissionSubrogation, cls).cancel(items)


class CommissionSubrogationDetail(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation.detail'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipCommSubDetail = Pool().get(
            'payslip.payslip.commission.subrogation.detail')
        result = get_records_considered_in_payslips(
            PayslipCommSubDetail, items, 'comm_sub_detail',
            states=['confirm', 'done'])
        return {
            'payslip': result
        }


class Permission(metaclass=PoolMeta):
    __name__ = 'hr_ec.permission'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')

    @classmethod
    def __setup__(cls):
        super(Permission, cls).__setup__()
        cls._error_messages.update({
            'used_on_payslip': ('No puede "Cancelar" este registro ya que se '
                'usa en "%(payslip)s".'),
        })

    @classmethod
    def cancel(cls, items):
        for item in items:
            if item.payslip:
                cls.raise_user_error('used_on_payslip', {
                    'payslip': item.payslip.rec_name
                })
        super(Permission, cls).cancel(items)

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipPermission = Pool().get('payslip.payslip.hr_ec.permission')
        result = get_records_considered_in_payslips(PayslipPermission, items,
            'permission', states=['confirm', 'done'])
        return {
            'payslip': result
        }


class Absence(metaclass=PoolMeta):
    __name__ = 'hr_ec.absence'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')

    @classmethod
    def __setup__(cls):
        super(Absence, cls).__setup__()
        cls._error_messages.update({
            'used_on_payslip': ('No puede "Cancelar" este registro ya que se '
                'usa en "%(payslip)s".'),
        })

    @classmethod
    def cancel(cls, items):
        for item in items:
            if item.payslip:
                cls.raise_user_error('used_on_payslip', {
                    'payslip': item.payslip.rec_name
                })
        super(Absence, cls).cancel(items)

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipAbsence = Pool().get('payslip.payslip.hr_ec.absence')
        result = get_records_considered_in_payslips(
            PayslipAbsence, items, 'absence', states=['confirm', 'done'])
        return {
            'payslip': result
        }


class Overtime(metaclass=PoolMeta):
    __name__ = 'hr_ec.overtime'

    @classmethod
    def __setup__(cls):
        super(Overtime, cls).__setup__()
        cls._error_messages.update({
            'used_on_payslip': ('No puede "Cancelar" este registro ya que una '
                'de sus lineas se usa en "%(payslip)s".'),
        })

    @classmethod
    def cancel(cls, items):
        for item in items:
            if item.lines:
                for line in item.lines:
                    if line.payslip:
                        cls.raise_user_error('used_on_payslip', {
                            'payslip': line.payslip.rec_name
                        })
        super(Overtime, cls).cancel(items)


class OvertimeLine(metaclass=PoolMeta):
    __name__ = 'hr_ec.overtime.line'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipOvertimeLine = Pool().get('payslip.payslip.overtime.line')
        result = get_records_considered_in_payslips(
            PayslipOvertimeLine, items, 'overtime_line',
            states=['confirm', 'done'])
        return {
            'payslip': result
        }


class EmployeeLoan(metaclass=PoolMeta):
    __name__ = 'company.employee.loan'

    @classmethod
    def __setup__(cls):
        super(EmployeeLoan, cls).__setup__()

        cls.payment_capacity.help = (
            "Indica el valor máximo de cuota mensual que el empleado es capaz "
            "de pagar.\n\nRepresenta al 40% de los ingresos netos menos las "
            "deducciones netas percibidadas en el mes inmediatamente anterior "
            "a la fecha de pago de la primera cuota.")

        cls._error_messages.update({
            'used_on_payslip': ('No puede "Cancelar" este registro ya que una '
                'de sus lineas se usa en "%(payslip)s".'),
        })

    @classmethod
    def cancel(cls, loans):
        for loan in loans:
            lines = loan.lines if loan.lines else []
            for line in lines:
                if line.payslip:
                    cls.raise_user_error('used_on_payslip', {
                        'payslip': line.payslip.rec_name
                    })
        super(EmployeeLoan, cls).cancel(loans)

    @fields.depends('start_date', 'employee')
    def on_change_with_payment_capacity(self, name=None):
        capacity = Decimal('0.00')
        if self.start_date and self.employee:
            capacity = self.__class__.calculate_payment_capacity(self)
        return capacity

    @classmethod
    def calculate_payment_capacity(cls, loan):
        '''
        Calculate 40% of the net income less the net deductions received in the
        month immediately prior to the date of payment of the first fee payment.
        It is likely that this method has been inherited in another specific
        module of each institution.
        '''
        Payslip = Pool().get('payslip.payslip')
        total_income = 0
        total_deduction = 0
        consulting_date = loan.start_date - relativedelta(months=1)
        if loan.start_date and loan.employee:
            payslips = Payslip.search([
                ('period.start_date', '<=', consulting_date),
                ('period.end_date', '>=', consulting_date),
                ('employee', '=', loan.employee),
                ('state', '=', 'done'),
                ('template.type', '=', 'payslip'),
            ])
            for payslip in payslips:
                total_income += payslip.total_income
                total_deduction += payslip.total_deduction
        capacity = (total_income - total_deduction) * Decimal('0.4')
        if capacity < 0:
            capacity = Decimal('0.00')
        return Decimal(str(capacity)).quantize(CENT)


class EmployeeLoanLine(metaclass=PoolMeta):
    __name__ = 'company.employee.loan.line'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipDeductionLoanLine = Pool().get(
            'payslip.payslip.deduction.loan.line')
        result = get_records_considered_in_payslips(
            PayslipDeductionLoanLine, items, 'loan_line',
            states=['confirm', 'done'])
        return {
            'payslip': result
        }


class EmployerTypeCode(ModelView, ModelSQL):
    'Employer Type Code'
    __name__ = 'employer.type.code'

    code = fields.Char('Código tipo de Empleador')
    name = fields.Char('Nombre')

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class WorkingRelationship(ModelView, ModelSQL):
    'Working Relationship'
    __name__ = 'working.relationship'

    employer_type = fields.Many2One('employer.type.code', 'Tipo de empleador')
    code = fields.Char('Relación de trabajo')
    name = fields.Char('Nombre')

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class SectorialActivity(ModelView, ModelSQL):
    'Sectorial Activity'
    __name__ = 'sectorial.activity'

    sectorial_code = fields.Char('Código Sectorial')
    name = fields.Char('Nombre')

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class MedicalCertificate(metaclass=PoolMeta):
    __name__ = 'galeno.medical.certificate'

    payslip_paid = fields.Function(fields.Boolean('Rol',
        states={
            'invisible': True
        }), 'on_change_with_payslip_paid')
    state_button_cancel = fields.Function(fields.Boolean(
        'Estado de boton cancelar'), 'on_change_with_state_button_cancel')

    @classmethod
    def __setup__(cls):
        super(MedicalCertificate, cls).__setup__()
        cls._buttons['cancel'].update({
            'invisible': Bool(Eval('state_button_cancel')),
            'depends': ['state_button_cancel']
        })
        cls._error_messages.update({
            'no_delete_certificates_payslip_paid': ('No es posible eliminar el '
                'certificado médico "%(certificate)s" debido a que ya ha sido '
                'agregado a un rol de pagos.')
        })

    @classmethod
    def delete(cls, certificates):
        for certificate in certificates:
            if certificate.payslip_paid:
                cls.raise_user_error('no_delete_certificates_payslip_paid', {
                    'certificate': certificate.rec_name
                })
        super(MedicalCertificate, cls).delete(certificates)

    @fields.depends('state')
    def on_change_with_payslip_paid(self, name=None):
        if self.state == 'done':
            pool = Pool()
            Payslip = pool.get('payslip.payslip')
            payslips = Payslip.search_read([
                ('employee', '=', self.employee.id),
                ('state', '=', 'done'),
                ['OR',
                 [
                     ('period.payslip_start_date', '<=', self.start_date),
                     ('period.payslip_end_date', '>=', self.start_date),
                 ],
                 [
                     ('period.payslip_start_date', '<=', self.end_date),
                     ('period.payslip_end_date', '>=', self.end_date),
                 ],
                ],
            ], fields_names=['medical_certificates'])
            for row in payslips:
                if self.id in row['medical_certificates']:
                    return True
        return False

    @fields.depends('state', 'payslip_paid')
    def on_change_with_payslip(self, name=None):
        if self.state and self.state in ['done']:
            return self.payslip_paid
        return True

    @fields.depends('start_date', 'end_date', 'state', 'payslip_paid')
    def on_change_with_state_button_cancel(self, name=None):
        if self.state and self.state in ['done', 'draft']:
            return self.payslip_paid
        return True


class OvertimeType(metaclass=PoolMeta):
    __name__ = 'hr_ec.overtime.type'

    @classmethod
    def __setup__(cls):
        super(OvertimeType, cls).__setup__()
        cls._error_messages.update({
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
        })

    @classmethod
    def write(cls, overtime_types, vals, *args):
        if overtime_types:
            # Search if the description is used in any payslip rule
            utils.set_text_value_on_rules(
                cls, overtime_types, vals, 'description')
        super(OvertimeType, cls).write(overtime_types, vals, *args)


class CommissionSubrogationType(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation.type'

    @classmethod
    def __setup__(cls):
        super(CommissionSubrogationType, cls).__setup__()
        cls._error_messages.update({
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
        })

    @classmethod
    def write(cls, comm_sub_types, vals, *args):
        if comm_sub_types:
            # Search if the name is used in any payslip rule
            utils.set_text_value_on_rules(
                cls, comm_sub_types, vals, 'name')
        super(CommissionSubrogationType, cls).write(comm_sub_types, vals, *args)


class LoanType(metaclass=PoolMeta):
    __name__ = 'company.employee.loan.type'

    account_lines = fields.One2Many(
        'company.employee.loan.type.account', 'loan_type', 'Detalle cuentas',
        states={
            'invisible': ~Bool(Eval('is_quincenal_loan'))
        }, depends=['is_quincenal_loan'])

    @classmethod
    def __setup__(cls):
        super(LoanType, cls).__setup__()
        cls._error_messages.update({
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
        })
        cls.account.states.update({
            'invisible': Bool(Eval('is_quincenal_loan'))
        })
        cls.account.depends += ['is_quincenal_loan']

    @classmethod
    def write(cls, loan_types, vals, *args):
        if loan_types:
            # Search if the name is used in any payslip rule
            utils.set_text_value_on_rules(
                cls, loan_types, vals, 'name')
        super(LoanType, cls).write(loan_types, vals, *args)


class LoanTypeAccount(ModelSQL, ModelView):
    'Company Employee Loan Type Account'
    __name__ = 'company.employee.loan.type.account'
    _history = True

    loan_type = fields.Many2One('company.employee.loan.type', 'Anticipo',
        required=True, ondelete='RESTRICT')
    payslip_template = fields.Many2One('payslip.template', 'Plantilla de Rol',
        required=True, ondelete='RESTRICT')
    account = fields.Many2One(
        'account.account', 'Cuenta',
        domain=[
            ('code', 'like', '112.%'),
            ('active', '=', True),
            ('kind', '!=', 'view')
        ], required=True)


class Retired(metaclass=PoolMeta):
    __name__ = 'company.retired'
    _STATES = {
        'readonly': Eval('state') != 'draft',
        'required': True
    }
    _DEPENDS = ['state']

    payslip_templates = fields.Many2Many('company.retired.payslip.template',
        'retired', 'payslip_template', 'Plantillas de rol de pagos',
        domain=[
            ('type', '=', 'employer_retirement')
        ],
        states={
            'readonly': _STATES['readonly']
        }, depends=_DEPENDS)
    xiii = fields.Selection(_LAW_BENEFITS_PAYMENT_FORMS, 'XIII',
        states={
            'readonly': _STATES['readonly'] | (Eval('payment_form') == 'global')
        }, depends=_DEPENDS + ['payment_form'])
    xiv = fields.Selection(_LAW_BENEFITS_PAYMENT_FORMS, 'XIV',
        states={
            'readonly': _STATES['readonly'] | (Eval('payment_form') == 'global')
        }, depends=_DEPENDS + ['payment_form'])
    xiii_translated = xiii.translated('xiii')
    xiv_translated = xiv.translated('xiv')
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuenta y Partida de Plantilla de Rol',
        states={
            'readonly': _STATES['readonly']
        }, depends=_DEPENDS)

    @classmethod
    def default_xiii(cls):
        return 'monthlyse'

    @classmethod
    def default_xiv(cls):
        return 'monthlyse'

    @fields.depends('payment_form', 'xiii', 'xiv')
    def on_change_payment_form(self):
        super(Retired, self).on_change_payment_form()
        if self.payment_form and self.payment_form == 'global':
            self.xiii = 'accumulate'
            self.xiv = 'accumulate'


class RetiredPayslipTemplate(ModelSQL, ModelView):
    'Relación entre Jubilados y Plantillas de Rol de Pagos'
    __name__ = 'company.retired.payslip.template'

    retired = fields.Many2One('company.retired', 'Jubilado', ondelete='CASCADE')
    payslip_template = fields.Many2One('payslip.template',
        'Plantilla de rol de pagos', ondelete='CASCADE')


class WorkshiftDelayLine(metaclass=PoolMeta):
    __name__ = 'hr_ec.workshift.delay.line'

    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual'), 'get_payslips')
    payslip_delay_lines = fields.One2Many(
        'payslip.payslip.hr_ec.workshift.delay.line', 'workshift_delay_line',
        'Linea de atraso en roles')

    @classmethod
    def __setup__(cls):
        super(WorkshiftDelayLine, cls).__setup__()

        # Fields readonly state
        _readonly_state = Bool(Eval('is_justified')) | Bool(Eval('payslip'))
        _depends = ['is_justified', 'payslip']
        fields = [
            cls.workshift_delay,
            cls.date,
            cls.workshift,
            cls.dialing_type,
            cls.dialing_hour,
            cls.is_justified,
            cls.observation,
            cls.department,
            cls.position]
        set_field_states(fields, _readonly_state, state_key='readonly')
        set_field_depends(fields, _depends, operation='add')

        cls._error_messages.update({
            'used_on_payslip': ('No puede eliminar la línea de atraso '
                '"%(delay_line)s" esta se usa en "%(payslip)s".'),
            'warning_delete_info': ('Usted desea eliminar %(total_selected)s '
                'registros; sin embargo, se han presentado los siguientes '
                'detalles que debe considerar antes de eliminarlos: '
                '%(message)s')
        })

        if cls._buttons['manage_delay'].get('readonly', None):
            cls._buttons['manage_delay']['readonly'] |= Eval('payslip')
        else:
            cls._buttons['manage_delay'].update({'readonly': Eval('payslip')})

    @classmethod
    def delete(cls, delay_lines):
        cls.manage_deletion(delay_lines)

    @classmethod
    def manage_deletion(cls, delay_lines):
        PayslipWorkshiftDelayLine = Pool().get(
            'payslip.payslip.hr_ec.workshift.delay.line')

        del_payslip_delay_lines_draft_payslip = []
        del_delay_lines_draft_payslip = []
        del_payslip_delay_lines_cancel_payslip = []
        del_delay_lines_cancel_payslip = []
        del_payslip_delay_lines = []
        del_delay_lines = []

        preserve_payslip_delay_lines = []
        preserve_delay_lines = []

        to_delete_payslip_delay_lines = []
        to_delete_delay_lines = []

        for delay_line in delay_lines:
            if not delay_line.payslip_delay_lines:
                del_delay_lines.append(delay_line)
            else:
                for pdl in delay_line.payslip_delay_lines:
                    if not pdl.was_considered:
                        del_payslip_delay_lines.append(pdl)
                        del_delay_lines.append(pdl.workshift_delay_line)
                    else:
                        payslip = pdl.payslip
                        if payslip.state == 'cancel':
                            del_payslip_delay_lines_cancel_payslip.append(pdl)
                            del_delay_lines_cancel_payslip.append(
                                pdl.workshift_delay_line)
                        elif payslip.state == 'draft':
                            del_payslip_delay_lines_draft_payslip.append(pdl)
                            del_delay_lines_draft_payslip.append(
                                pdl.workshift_delay_line)
                        else:
                            preserve_payslip_delay_lines.append(pdl)
                            preserve_delay_lines.append(
                                pdl.workshift_delay_line)

        if (del_payslip_delay_lines_cancel_payslip or
                del_payslip_delay_lines_draft_payslip or
                    del_delay_lines or preserve_payslip_delay_lines):
            del_delay_lines_cancel_payslip = (
                list(set(del_delay_lines_cancel_payslip)))
            del_delay_lines_draft_payslip = (
                list(set(del_delay_lines_draft_payslip)))
            del_delay_lines = (
                list(set(del_delay_lines)))
            preserve_delay_lines = (
                list(set(preserve_delay_lines)))

            if (del_delay_lines_cancel_payslip or
                    del_delay_lines_draft_payslip or
                        preserve_delay_lines):
                warning_message = cls.get_warning_deletion_message(
                    len(del_delay_lines_cancel_payslip),
                    len(del_delay_lines_draft_payslip),
                    len(preserve_delay_lines),
                    len(del_delay_lines))

                cls.raise_user_warning('warning_delete_info',
                    'warning_delete_info', {
                        'total_selected': len(delay_lines),
                        'message': warning_message
                    })

            # TO DELETE RECORDS OF INTERMEDIATE TABLE
            to_delete_payslip_delay_lines += (
                del_payslip_delay_lines +
                del_payslip_delay_lines_cancel_payslip +
                del_payslip_delay_lines_draft_payslip)

            # TO DELETE RECORDS
            to_delete_delay_lines += (
                del_delay_lines +
                del_delay_lines_cancel_payslip +
                del_delay_lines_draft_payslip)

            # DELETE RECORDS OF INTERMEDIATE TABLE AND MODEL TABLE
            to_delete_payslip_delay_lines = (
                list(set(to_delete_payslip_delay_lines)))
            to_delete_delay_lines = list(set(to_delete_delay_lines))
            PayslipWorkshiftDelayLine.delete(to_delete_payslip_delay_lines)
            super(WorkshiftDelayLine, cls).delete(to_delete_delay_lines)

    @classmethod
    def get_warning_deletion_message(cls, total_items_cancel_payslip,
        total_items_draft_payslip, total_items_to_preserve, total_items):
        warning_message = ""
        if total_items_cancel_payslip > 0:
            warning_message += (
                f'\n\nREGISTROS USADOS EN UN ROL EN ESTADO "CANCELADO": '
                f'{total_items_cancel_payslip}\n*Luego de '
                f'eliminarlas, estos desapareceran de los registros del '
                f'rol cancelado.')

        if total_items_draft_payslip > 0:
            warning_message += (
                f'\n\nREGISTROS USADOS EN UN ROL EN ESTADO "BORRADOR": '
                f'{total_items_draft_payslip}\n*Luego de '
                f'eliminarlos, usted deberá volver a generar los valores '
                f'de los roles involucrados.')

        if total_items_to_preserve > 0:
            warning_message += (
                f'\n\nREGISTROS QUE NO SERÁN ELIMINADOS: '
                f'{total_items_to_preserve}.\n*Estos registros no se '
                f'eliminarán debido a que forman parte de un rol de pagos '
                f'que no está en estado borrador o cancelado.')

        if total_items > 0:
            warning_message += (
                f'\n\nLos demás registros ({total_items}) no presentan '
                f'inconvenientes.')
        return warning_message

    @classmethod
    def get_payslips(cls, items, names=None):
        PayslipWorkshiftDelayLine = Pool().get(
            'payslip.payslip.hr_ec.workshift.delay.line')
        result = get_records_considered_in_payslips(
            PayslipWorkshiftDelayLine, items, 'workshift_delay_line',
            states=['confirm', 'done'])
        return {
            'payslip': result
        }


class ManagePayslipTemplatesContractsWizardStart(ModelView):
    'Manage Payslip Templates Contracts Wizard Start'
    __name__ = 'manage.payslip.templates.contracts.wizard.start'

    operation = fields.Selection([
            ('add', 'Agregar'),
            ('remove', 'Eliminar'),
        ], 'Operación',
        states={
            'required': True,
        })
    payslip_template = fields.Many2One('payslip.template', 'Plantilla de rol',
        states={
            'required': True,
        })
    contracts = fields.Many2Many('company.contract', None, None, 'Contratos',
        domain=[
            If(Bool(Eval('consider_finalized_contracts')),
               ('state', 'in', ['draft', 'done', 'disabled', 'finalized']),
               ('state', 'in', ['draft', 'done', 'disabled']),)
        ],
        states={
            'required': Eval('operation') == 'add',
            'invisible': Eval('operation') != 'add',
        }, depends=['operation'])
    template_contracts = fields.One2Many('company.contract.payslip.template',
        None, 'Contratos con la plantilla seleccionada',
        domain=[
            ('payslip_template', '=', Eval('payslip_template')),
            If(Bool(Eval('consider_finalized_contracts')),
               ('contract_state', 'in',
                    ['draft', 'done', 'disabled', 'finalized']),
               ('contract_state', 'in',
                    ['draft', 'done', 'disabled']), )
        ],
        states={
            'required': Eval('operation') == 'remove',
            'invisible': Eval('operation') != 'remove',
        }, depends=['operation'])
    total = fields.Integer('Contratos seleccionados',
        states={
            'readonly': True,
        })
    consider_finalized_contracts = fields.Boolean(
        'Considerar contratos finalizados?')

    @classmethod
    def view_attributes(cls):
        return super(ManagePayslipTemplatesContractsWizardStart,
                cls).view_attributes() + [
            ('//group[@id="grp_label_info_add"]', 'states', {
                 'invisible': Eval('operation') != 'add'
            }),
            ('//group[@id="grp_label_info_remove"]', 'states', {
                 'invisible': Eval('operation') != 'remove'
            }),
        ]

    @classmethod
    def default_operation(cls):
        return 'add'

    @classmethod
    def default_consider_finalized_contracts(cls):
        return False

    @fields.depends('operation', 'contracts', 'template_contracts')
    def on_change_with_total(self, name=None):
        if self.operation:
            if self.operation == 'add':
                return len(self.contracts)
            elif self.operation == 'remove':
                return len(self.template_contracts)
        return 0

    @fields.depends('operation', 'template_contracts')
    def on_change_payslip_template(self, name=None):
        if self.operation:
            if self.operation == 'remove':
                self.template_contracts = None

    @fields.depends('consider_finalized_contracts', 'contracts',
        'template_contracts')
    def on_change_consider_finalized_contracts(self, name=None):
        if self.template_contracts:
            self.template_contracts = None
        if self.contracts:
            self.contracts = None


class ManagePayslipTemplatesContractsWizardSucceed(ModelView):
    'Manage Payslip Templates Contracts Wizard Succeed'
    __name__ = 'manage.payslip.templates.contracts.wizard.succeed'


class ManagePayslipTemplatesContractsWizard(Wizard):
    'Manage Payslip Templates Contracts Wizard'
    __name__ = 'manage.payslip.templates.contracts.wizard'

    start = StateView('manage.payslip.templates.contracts.wizard.start',
        'hr_ec_payslip.manage_payslip_templates_contracts_wizard_start_view_form', #noqa
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Ok', 'ok_', 'tryton-executable'),
        ])
    succeed = StateView('manage.payslip.templates.contracts.wizard.succeed',
        'hr_ec_payslip.manage_payslip_templates_contracts_wizard_succeed_view_form', #noqa
        [
            Button('Ok', 'end', 'tryton-executable'),
        ])
    ok_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(ManagePayslipTemplatesContractsWizard, cls).__setup__()
        cls._error_messages.update({
            'operation_not_allowed': ('Operación no permitida')
        })

    def transition_ok_(self):
        operation = self.start.operation
        payslip_template = self.start.payslip_template
        contracts = self.start.contracts
        template_contracts = self.start.template_contracts

        if operation == 'add':
            self.add_payslips_templates(payslip_template, contracts)
        elif operation == 'remove':
            self.remove_payslip_templates(template_contracts)
        else:
            self.raise_user_error('operation_not_allowed')

        return 'succeed'

    def add_payslips_templates(self, payslip_template, contracts):
        pool = Pool()
        ContractPayslipTemplate = pool.get('company.contract.payslip.template')

        to_save = []
        for contract in contracts:
            if payslip_template not in contract.payslip_templates:
                contract_payslip_template = ContractPayslipTemplate()
                contract_payslip_template.contract = contract
                contract_payslip_template.payslip_template = payslip_template
                to_save.append(contract_payslip_template)
        ContractPayslipTemplate.save(to_save)

    def remove_payslip_templates(self, template_contracts):
        pool = Pool()
        ContractPayslipTemplate = pool.get('company.contract.payslip.template')
        ContractPayslipTemplate.delete(template_contracts)


class BudgetAnalisysByItemContext(ModelView):
    'Budget Analisys By Item'
    __name__ = 'payslip.budget.analisys.by.item.context'

    fiscalyear = fields.Many2One(
        'account.fiscalyear', 'Año fiscal', states={'required': True})
    period = fields.Many2One(
        'account.period', 'Periodo',
        states={'required': True},
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'])
    rule = fields.Many2One(
        'payslip.rule', 'Regla / Rubro', states={'required': True})
    templates = fields.One2Many(
        'payslip.template', None, 'Plantillas')

    @classmethod
    def default_fiscalyear(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        return context.get(
            'fiscalyear',
            FiscalYear.find(context.get('company'), exception=False))

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None


class BudgetAnalisysByItem(ModelView, ModelSQL):
    'Budget Analisys By Item'
    __name__ = 'payslip.budget.analisys.by.item'

    department = fields.Many2One('company.department', 'Departamento')
    activity = fields.Many2One('public.planning.unit', 'Actividad')
    budget = fields.Many2One('public.budget', 'Partida')
    previous_amount = fields.Numeric('Monto afectado')
    periods = fields.Integer('Periodos')
    initial_assignment = fields.Function(
        fields.Numeric('Monto inicial'),
        'get_data'
    )
    available_balance = fields.Function(
        fields.Numeric('Saldo inicial'),
        'get_data'
    )
    expenditure_projection  = fields.Function(
        fields.Numeric('Proyeccion de gastos'),
        'get_data'
    )
    starting_deficit = fields.Function(
        fields.Numeric('Superávit/Déficit de Partida'),
        'get_data'
    )

    @classmethod
    def __setup__(cls):
        super(BudgetAnalisysByItem, cls).__setup__()
        cls._order.insert(0, ('activity', 'ASC'))
        cls._order.insert(1, ('department', 'ASC'))

    @classmethod
    def get_data(cls, records, names=None):
        pool = Pool()
        PublicBudget = pool.get('public.budget')
        dict_initial = defaultdict(lambda : None)
        dict_available = defaultdict(lambda : None)
        dict_expenditure = defaultdict(lambda : None)
        dict_deficit = defaultdict(lambda : None)
        for row in records:
            initial = Decimal("0")
            if row.budget:
                budget = PublicBudget(row.budget)
                initial += budget.initial_assignment
            previous = row.previous_amount
            available = (initial - previous).quantize(Decimal("0.01"))
            expenditure = (previous / row.periods).quantize(Decimal("0.01"))
            deficit = (available - expenditure).quantize(Decimal("0.01"))
            dict_initial[row.id] = initial
            dict_available[row.id] = available
            dict_expenditure[row.id] = expenditure
            dict_deficit[row.id] = deficit
        return {
            'initial_assignment': dict_initial,
            'available_balance': dict_available,
            'expenditure_projection': dict_expenditure,
            'starting_deficit': dict_deficit
        }

    @classmethod
    def table_query(cls):
        context = Transaction().context
        rule = context.get('rule')
        templates = context.get('templates')
        fiscalyear = context.get('fiscalyear')

        pool = Pool()
        Period = pool.get('account.period')
        Payslip = pool.get('payslip.payslip')
        PayslipLine = pool.get('payslip.line')
        BudgetDepartment = pool.get('company.department.template.budget')
        BudgetRule = pool.get('payslip.rule.account.budget')
        payslip = Payslip.__table__()
        payslip_line = PayslipLine.__table__()
        budget_department = BudgetDepartment.__table__()
        budget_rule = BudgetRule.__table__()

        period = Period(context.get('period'))
        periods = Period.search_read([
            ('fiscalyear', '=', fiscalyear),
            ('start_date', '<', period.start_date)
        ], fields_names=['id'])
        period_ids = [x['id'] for x in periods]

        columns = [
            RowNumber(window=Window([])).as_('id'),
            payslip.department,
            budget_department.activity,
            budget_rule.budget,
            Sum(payslip_line.amount).as_('previous_amount'),
            Literal(len(period_ids)).as_('periods'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date')
        ]

        where = (payslip.state.in_(['done', 'posted'])) & (
            payslip.period.in_(period_ids)) & (
            payslip_line.rule == rule)

        if templates:
            templates_ids = [x['id'] for x in templates]
            where &= (payslip.template.in_(templates_ids))

        query = payslip.join(
            payslip_line, 'LEFT',
            condition=payslip.id == payslip_line.payslip
        ).join(
            budget_department, 'LEFT',
            condition=(payslip.payslip_template_account_budget == budget_department.template_budget) & (
                budget_department.department == payslip.department)
        ).join(
            budget_rule, 'LEFT',
            condition=(budget_rule.template_account_budget == payslip.payslip_template_account_budget) & (
                budget_rule.payslip_rule == payslip_line.rule)
        ).select(
            *columns,
            where=where,
            group_by=[payslip.department, budget_department.activity,
                      budget_rule.budget],
            order_by=[budget_department.activity.asc, payslip.department.asc]
        )

        return query