import datetime
import pendulum
from calendar import monthrange

from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.model import ModelView, fields, ModelSQL
from trytond.pyson import Eval, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction


__all__ = [
    'NoticeNewSalaryExportWizardStart', 'NoticeNewSalaryExportWizard',
    'ExtraIncomeExportWizardStart', 'ExtraIncomeExportWizard',
    'ReserveFundsExportWizardStart', 'ReserveFundsExportWizard',
    'OutputNoveltiesExportWizardStart', 'OutputNoveltiesExportWizard',
    'DaysNotWorkedtExportWizardStart', 'DaysNotWorkedtExportWizard',
    'NoticeInputWizardStart', 'NoticeInputWizard',
    'RetroactiveDifferenceWizardStart', 'RetroactiveDifferenceWizard',
    'ExtraIncomeExportRule', 'ConfigurationReportsBatch'
]


def get_configuration_fields(period, model, field_date):
    pool = Pool()
    Config = pool.get('configuration.reports.batch')
    config = Config.search([
        ('reference_model.model', '=', model),
        ('reference_field.name', '=', field_date),
    ])
    if config:
        Period = pool.get('account.period')
        period_field, = Period.search_read([
            ('id', '=', period.id)
        ], fields_names=[config[0].field_date.name])
        return period_field.get(config[0].field_date.name)


class NoticeNewSalaryExportWizardStart(ModelView):
    ' Notice New Salary Export Wizard Start'
    __name__ = 'notice.new.salary.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    date = fields.Date('Fecha', required=True)
    contracts = fields.One2Many('company.contract', None, 'Contratos',
        domain=[
            ('company', '=', Eval('company')),
            ('state', '=', 'done')
        ],
        context={
            'salary_date': Eval('date')
        }, depends=['company', 'date'])
    branch_office_code = fields.Char('Código de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])
    movement_type = fields.Char('Tipo de movimiento')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_movement_type():
        return 'MSU'

    @staticmethod
    def default_date():
        return datetime.date.today()

    @fields.depends('company', 'establishment', 'branch_office_code')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'


class NoticeNewSalaryExportWizard(Wizard):
    ' Notice New Salary Export Wizard'
    __name__ = 'notice.new.salary.export.wizard'

    start = StateView('notice.new.salary.export.wizard.start',
        'hr_ec_payslip.notice_new_salary_export_wizard_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Export', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('notice.new.salary.export')

    @classmethod
    def __setup__(cls):
        super(NoticeNewSalaryExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_selected_contracts': 'No contract has been selected.',
            'no_identifier_found': ('The cedula number for the employee '
                '"%(employee)s" has not been found.')
        })

    def do_export_(self, action):
        salary_employees = self.get_salary_by_employee(self.start.contracts)

        if not salary_employees:
            self.raise_user_error('no_selected_contracts')

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type
        }

        data = {
            'company': self.start.company.id,
            'date': self.start.date,
            'salary_employees': salary_employees,
            'config': config
        }
        return action, data

    def get_salary_by_employee(self, contracts):
        salary_employees = {}
        if contracts:
            for contract in contracts:
                employee = contract.employee
                if (not employee.identifier):
                    self.raise_user_error('no_identifier_found')
                salary_employees.update({
                    employee.identifier: contract.salary
                })
        return salary_employees


class ExtraIncomeExportRule(ModelView, ModelSQL):
    'Extra Income Export Rule'
    __name__ = 'extra.income.export.rule'

    rule = fields.Many2One('payslip.rule', 'Regla', required=True)


class ExtraIncomeExportWizardStart(ModelView):
    'Extra Income Wizard Start'
    __name__ = 'extra.income.export.wizard.start'

    company = fields.Many2One('company.company', 'Compañía', required=True,
        states={
            'readonly': True
        })
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    branch_office_code = fields.Char('Código de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'],
        help='Use cuatro dígitos, por ejemplo: 0001')
    movement_type = fields.Char('Tipo de movimiento', help='Ejemplo: INS')
    cause = fields.Char('Causa', help="Use un digito, por ejemplo: 0")
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ],
        states={
            'invisible': ~(Bool(Eval('by_periods'))),
            'required': Bool(Eval('by_periods'))
        }, depends=['company', 'by_periods'], required=False)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ],
        states={
            'invisible': ~(Bool(Eval('by_periods'))),
            'required': Bool(Eval('by_periods'))
        }, depends=['fiscalyear', 'by_periods'], required=False)

    start_date = fields.Date('Inicio de corte',
        domain=[
            ('start_date', '<=', Eval('end_date', None))
        ],
        states={
            'invisible': ~(Bool(Eval('by_dates'))),
            'required': Bool(Eval('by_dates'))
        }, depends=['end_date', 'by_dates'], required=False)
    end_date = fields.Date('Fin de corte',
        domain=[
            ('end_date', '>=', Eval('start_date', None))
        ],
        states={
            'invisible': ~(Bool(Eval('by_dates'))),
            'required': Bool(Eval('by_dates'))
        }, depends=['start_date', 'by_dates'], required=False)
    payslip_generals = fields.One2Many('payslip.general', None,
        'Roles generales para búsqueda de la información',
        domain=[
            ('company', '=', Eval('company')),
            ('state', '!=', 'cancel'),
        ],
        states={
            'invisible': ~(Bool(Eval('by_payslip_generals'))),
            'required': Bool(Eval('by_payslip_generals'))
        }, depends=['company', 'by_payslip_generals'])
    rules = fields.Many2Many('extra.income.export.rule', None, 'rule', 'Reglas',
        required=True)

    # Configuration params
    by_periods = fields.Boolean('Buscar datos por PERIODO',
        states={
            'readonly': (Bool(Eval('by_dates')) |
                         Bool(Eval('by_payslip_generals')))
        }, depends=['by_dates', 'by_payslip_generals'])
    by_dates = fields.Boolean('Buscar datos en FECHAS DE CORTE',
        states={
            'readonly': (Bool(Eval('by_periods')) |
                         Bool(Eval('by_payslip_generals')))
        }, depends=['by_periods', 'by_payslip_generals'])
    by_payslip_generals = fields.Boolean('Buscar datos por ROLES GENERALES',
        states={
            'readonly': (Bool(Eval('by_periods')) |
                         Bool(Eval('by_dates')))
        }, depends=['by_periods', 'by_dates'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @staticmethod
    def default_movement_type():
        return 'INS'

    @staticmethod
    def default_cause():
        return 'O'

    @staticmethod
    def default_by_periods():
        return True

    @staticmethod
    def default_by_dates():
        return False

    @staticmethod
    def default_by_payslip_generals():
        return False

    @fields.depends('company', 'establishment', 'branch_office_code',
        'fiscalyear', 'period', 'start_date', 'end_date')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'

    @fields.depends('company')
    def on_change_with_rules(self):
        pool = Pool()
        Rule = pool.get('extra.income.export.rule')
        rules = Rule.search([])
        return [rule.rule.id for rule in rules]

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self, name=None):
        if not self.fiscalyear:
            self.period = None

    @fields.depends('by_periods', 'fiscalyear', 'period', 'start_date',
        'end_date', 'payslip_generals')
    def on_change_by_periods(self):
        if self.by_periods:
            self.start_date = None
            self.end_date = None
            self.payslip_generals = None

    @fields.depends('by_dates', 'fiscalyear', 'period', 'start_date',
        'end_date', 'payslip_generals')
    def on_change_by_dates(self):
        if self.by_dates:
            self.fiscalyear = None
            self.period = None
            self.payslip_generals = None

    @fields.depends('by_payslip_generals', 'fiscalyear', 'period', 'start_date',
        'end_date', 'payslip_generals')
    def on_change_by_payslip_generals(self):
        if self.by_payslip_generals:
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None


class ExtraIncomeExportWizard(Wizard):
    'Extra Income Export Wizard'
    __name__ = 'extra.income.export.wizard'

    start = StateView('extra.income.export.wizard.start',
        'hr_ec_payslip.extra_income_export_wizard_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Export', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('extra.income.export')

    @classmethod
    def __setup__(cls):
        super(ExtraIncomeExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_extra_incomes_by_periods': ('No se han encontrado registros de '
                'ingresos extras en el periodo "%(period)s".'),
            'no_extra_incomes_by_dates': ('No se han encontrado registros de '
                'ingresos extras entre la fecha de inicio "%(start_date)s" y la'
                'fecha de finalización "%(end_date)s".'),
            'no_extra_incomes_by_payslip_generals': ('No se han encontrado '
                'registros de ingresos extras en los roles generales que ha '
                'seleccionado.'),
            'no_rule': ('Hay que definir las reglas'),
        })

    def do_export_(self, action):
        if not self.start.rules:
            self.raise_user_error('no_rule')

        extra_incomes = self.get_extra_income_amounts()

        if not extra_incomes:
            self.show_error_no_extra_incomes()

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
            'cause': self.start.cause
        }

        data = {
            'company': self.start.company.id,
            'extra_incomes': extra_incomes,
            'config': config
        }
        return action, data

    def get_extra_income_amounts(self):
        result = {}
        rules = [rule.code for rule in self.start.rules]
        payslips = self.get_payslips()
        if payslips:
            for payslip in payslips:
                income_lines = payslip.income_lines
                if income_lines:
                    for income_line in income_lines:
                        code = income_line.rule.code
                        if code in rules:
                            amount = income_line.amount
                            if amount > 0:
                                self.set_result(result, payslip, amount)
        return result

    def set_result(self, dict, payslip, amount):
        employee = payslip.employee
        period = payslip.period
        key = f'{employee.identifier}-{period.name}'
        if key not in dict:
            dict.update({
                key: {
                    'employee_ced': employee.identifier,
                    'period_year': str(period.start_date.year),
                    'period_month': str(period.start_date.month).rjust(2, '0'),
                    'amount': amount
                }
            })
        else:
            dict[key]['amount'] += amount

    def get_payslips(self):
        Payslip = Pool().get('payslip.payslip')
        payslips = []
        _domain = []
        if self.start.by_periods:
            _domain = [
                ('period', '=', self.start.period)
            ]
        elif self.start.by_dates:
            _domain = [
                ('start_date', '>=', self.start.start_date),
                ('end_date', '<=', self.start.end_date),
            ]
        elif self.start.by_payslip_generals:
            ids = [general.id for general in self.start.payslip_generals]
            _domain = [
                ('general', 'in', ids),
            ]
        if _domain:
            _domain.append(('state', '=', 'done'))
            payslips = Payslip.search(_domain)
        return payslips

    def show_error_no_extra_incomes(self):
        if self.start.by_periods:
            self.raise_user_error('no_extra_incomes_by_periods', {
                'period': self.start.period.rec_name
            })
        elif self.start.by_dates:
            self.raise_user_error('no_extra_incomes_by_date', {
                'start_date': self.start.start_date,
                'end_date': self.start.end_date
            })
        elif self.start.by_payslip_generals:
            self.raise_user_error('no_extra_incomes_by_payslip_generals', {
                'period': self.start.period.rec_name
            })


class ReserveFundsExportWizardStart(ModelView):
    'Reserve Funds Export Wizard Start'
    __name__ = 'reserve.funds.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    start_date = fields.Date('Fecha inicio',
        states={
            'readonly': True,
        },
        domain=[
            ('start_date', '<=', Eval('end_date'))
        ], depends=['end_date'], required=True)
    end_date = fields.Date('Fecha fin',
        states={
            'readonly': True,
        },
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date', 'type'], required=True)
    months = fields.Function(fields.Integer('Meses trabajados'),
        'on_change_with_months')
    type = fields.Selection(
        [
            ('monthlyse', 'Mensualizar'),
            ('accumulate', 'Acumular')
        ], 'Type', required=True)
    type_translated = type.translated('type')
    branch_office_code = fields.Char('Codigo de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])
    movement_type = fields.Char('Tipo de movimiento')
    period_type = fields.Char('Tipo de periodo',
        states={
            'invisible': ~(Eval('type').in_(['monthlyse']))
        }, depends=['type'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'monthlyse'

    @staticmethod
    def default_movement_type():
        return 'PFM'

    @staticmethod
    def default_period_type():
        return 'G'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'establishment', 'branch_office_code',
        'start_date', 'end_date')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'

    @fields.depends('start_date', 'end_date', 'months')
    def on_change_start_date(self, name=None):
        if self.start_date:
            last_day = monthrange(
                self.start_date.year, self.start_date.month)[1]
            self.end_date = self.start_date.replace(day=last_day)
            self.months = self.get_months(
                self.start_date, self.end_date)

    @fields.depends('type')
    def on_change_with_movement_type(self, name=None):
        if self.type:
            if self.type == 'accumulate':
                return 'PFR'
        return 'PFM'

    @fields.depends('start_date', 'end_date')
    def on_change_with_months(self, name=None):
        return self.get_months(self.start_date, self.end_date)

    def get_months(self, d1, d2):
        if (d1 and d2):
            diff = pendulum.period(d1, d2).months
            return (diff + 1) if diff >= 0 else None
        return None

    @fields.depends('period', 'start_date', 'end_date', 'months')
    def on_change_period(self):
        if self.period:
            if self.period.payslip_start_date and self.period.payslip_end_date:
                self.start_date = self.period.payslip_start_date
                self.end_date = self.period.payslip_end_date
            else:
                self.start_date = self.period.start_date
                self.end_date = self.period.end_date
            self.months = self.on_change_with_months()
        else:
            self.start_date = None
            self.end_date = None


class ReserveFundsExportWizard(Wizard):
    'Extra Income Export Wizard'
    __name__ = 'reserve.funds.export.wizard'

    start = StateView('reserve.funds.export.wizard.start',
        'hr_ec_payslip.reserve_funds_export_wizard_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('reserve.funds.export')

    @classmethod
    def __setup__(cls):
        super(ReserveFundsExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_reserve_funds': ('No se han encontrado registros de fondos de '
                'reserva con fecha inicio "%(start_date)s" y fecha fin '
                '"%(end_date)s".')
        })

    def do_export_(self, action):
        start_date = self.start.start_date
        end_date = self.start.end_date
        type = self.start.type

        reserve_funds = self.get_reserve_funds_amounts(
            start_date, end_date, type)

        if not reserve_funds:
            self.raise_user_error('no_reserve_funds', {
                'start_date': start_date,
                'end_date': end_date
            })

        config = {
            'type': self.start.type,
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
            'period_type': self.start.period_type
        }

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'start_date': start_date,
            'end_date': end_date,
            'reserve_funds': reserve_funds,
            'config': config
        }
        return action, data

    def get_reserve_funds_amounts(self, start_date, end_date, type):
        result = {}
        payslips = self.get_payslips(start_date, end_date)
        if payslips:
            for payslip in payslips:
                lines = None
                if type == 'monthlyse':
                    lines = payslip.income_lines
                else:
                    lines = payslip.internal_lines
                if lines:
                    for line in lines:
                        if line.rule.code == 'fr' or (
                                line.rule.code == 'fr_accumulate'):
                            amount = line.amount
                            if amount > 0:
                                employee_ced = payslip.employee.identifier
                                self.set_result(
                                    result, employee_ced, 'amount', amount)
                                self.set_result(
                                    result, employee_ced, 'months', 1)
        return result

    def set_result(self, dict, key1, key2, amount):
        if key1 not in dict:
            dict.update({
                key1: {key2: amount}
            })
        else:
            if key2 not in dict[key1]:
                dict[key1].update({
                    key2: amount
                })
            else:
                dict[key1][key2] += amount

    def get_payslips(self, start_date, end_date):
        Payslip = Pool().get('payslip.payslip')
        payslips = Payslip.search([
            ('start_date', '>=', start_date),
            ('end_date', '<=', end_date),
            ('state', '=', 'done')
        ])
        return payslips


class OutputNoveltiesExportWizardStart(ModelView):
    'Output Novelties Export Wizard Start'
    __name__ = 'output.novelties.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    period_start_date = fields.Date('Fecha inicio', states={'readonly': True})
    period_end_date = fields.Date('Fecha fin', states={'readonly': True})

    branch_office_code = fields.Char('Codigo de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])
    movement_type = fields.Char('Tipo de movimiento')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_movement_type():
        return 'SAL'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'establishment', 'branch_office_code',
        'start_date', 'end_date')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'

    @fields.depends('period', 'period_start_date', 'period_end_date')
    def on_change_period(self):
        if self.period:
            self.period_start_date = self.period.start_date
            self.period_end_date = self.period.end_date
        else:
            self.period_start_date = None
            self.period_end_date = None


class OutputNoveltiesExportWizard(Wizard):
    'Output Novelties Export Wizard'
    __name__ = 'output.novelties.export.wizard'

    start = StateView('output.novelties.export.wizard.start',
        'hr_ec_payslip.output_novelties_export_wizard_start_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Exportar', 'export_', 'tryton-executable'),
                      ])
    export_ = StateReport('output.novelties.export')

    @classmethod
    def __setup__(cls):
        super(OutputNoveltiesExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_output_novelties_funds': ('No se han encontrado registros de '
                                        'salidas en el periodo "%(period)s".'),
            'no_identifier_found': ('El número de cédula del empleado '
                                    '"%(employee)s" no ha sido encontrado.')
        })

    def do_export_(self, action):
        start_date = self.start.period_start_date
        end_date = self.start.period_end_date
        period = self.start.period

        output_novelties = self.get_output_novelties(
            start_date, end_date)

        if not output_novelties:
            self.raise_user_error('no_output_novelties_funds', {
                'period': period.rec_name
            })

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
        }

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'output_novelties': output_novelties,
            'config': config
        }
        return action, data

    def get_output_novelties(self, start_date, end_date):
        salary_employees = {}
        finalize_contracts = self.get_finalize_contracts(start_date, end_date)

        if finalize_contracts:
            for finalize_contract in finalize_contracts:
                if finalize_contract.reason_finalize.code == 'F':
                    date_death = (
                        str(finalize_contract.date_death)).replace('-', '')
                else:
                    date_death = '00000000'
                if finalize_contract.reason_finalize.code:
                    salary_employees.update({
                        finalize_contract.employee.identifier: {
                            'output_date': (
                                str(finalize_contract.output_date)).replace("-", ''),  # noqa
                            'reason': finalize_contract.reason_finalize.name,
                            'date_death': date_death}
                    })
        return salary_employees

    def get_finalize_contracts(self, start_date, end_date):
        FinalizeContract = Pool().get('company.finalize.contract')
        finalize_contracts = FinalizeContract.search([
            ('output_date', '>=', start_date),
            ('output_date', '<=', end_date),
            ('state', '=', 'done')
        ])
        return finalize_contracts


class DaysNotWorkedtExportWizardStart(ModelView):
    'Days Not Worked Export Wizard Start'
    __name__ = 'days.not.worked.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    movement_type = fields.Char('Tipo de movimiento')
    branch_office_code = fields.Char('Código de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_movement_type():
        return 'MND'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'establishment', 'branch_office_code')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'


class DaysNotWorkedtExportWizard(Wizard):
    'Days Not Worked Export Wizard'
    __name__ = 'days.not.worked.export.wizard'

    start = StateView('days.not.worked.export.wizard.start',
        'hr_ec_payslip.days_not_worked_export_wizard_start_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Exportar', 'export_', 'tryton-executable'),
                      ])
    export_ = StateReport('days.not.worked.export')

    @classmethod
    def __setup__(cls):
        super(DaysNotWorkedtExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_days_worked_funds': ('No se han encontrado registros de dias no'
                ' laborados por los empleados en el periodo %(period)s .')
        })

    def do_export_(self, action):

        period = self.start.period
        id_company = self.start.company.id

        days_not_workeds = self.get_days_not_worked(period, id_company)

        if not days_not_workeds:
            self.raise_user_error('no_days_worked_funds', {
                'period': period.name,
            })

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
        }

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'days_not_workeds': days_not_workeds,
            'config': config
        }
        return action, data

    def get_days_not_worked(self, period, id_company):
        dict_result = {}
        absences = self.get_payslips(period, id_company)
        if absences:
            for absence in sorted(absences):
                if absence.total_absences > 0:
                    date_temp = (
                        absence.absences[0].start_date
                        if (absence.absences[0].start_date.month ==
                            period.payslip_end_date.month)
                        else period.payslip_end_date)
                    date = str(date_temp)[0:10].replace("-", '')
                    dict_aux = {
                        'total_days': absence.total_absences_with_normative,
                        'start_date': date}
                    dict_result[absence.employee.identifier] = dict_aux

        return dict_result

    def get_payslips(self, period, id_company):
        Payslip = Pool().get('payslip.payslip')
        payslips = Payslip.search([
            ('company', '=', id_company),
            ('period', '=', period),
            ('template.accumulated_law_benefits', '=', False),
            ('state', '=', 'done')
        ])
        return payslips


class NoticeInputWizardStart(ModelView):
    'Notice Input Wizard Start'
    __name__ = 'notice.input.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Mes actual',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    period_start_date = fields.Date('Fecha Inicio', states={'readonly': True})
    period_end_date = fields.Date('Fecha Fin', states={'readonly': True})
    movement_type = fields.Char('Tipo de movimiento')

    branch_office_code = fields.Char('Codigo de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_movement_type():
        return 'ENT'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'establishment', 'branch_office_code',
        'start_date', 'end_date')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'

    @fields.depends('period', 'period_start_date', 'period_end_date')
    def on_change_period(self):
        if self.period:
            self.period_start_date = self.period.start_date
            self.period_end_date = self.period.end_date
        else:
            self.period_start_date = None
            self.period_end_date = None


class NoticeInputWizard(Wizard):
    'Notice Input Wizard'
    __name__ = 'notice.input.wizard'

    start = StateView('notice.input.wizard.start',
        'hr_ec_payslip.notice_input_wizard_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Exportar', 'export_', 'tryton-executable'),
                      ])
    export_ = StateReport('notice.input.export')

    @classmethod
    def __setup__(cls):
        super(NoticeInputWizard, cls).__setup__()
        cls._error_messages.update({
            'no_notice_input_funds': ('No se han encontrado registros de '
                'novedades de entrada en la fecha inicio "%(start_date)s" '
                'y fecha fin "%(end_date)s" '),
            'no_identifier_found': ('El número de cédula del empleado '
                '"%(employee)s" no ha sido encontrado.'),
            'no_iess_data': ('El contrato del empleado "%(employee)s", no tiene'
                ' la información necesaria del seguro social'),
        })

    def do_export_(self, action):
        start_date = self.start.period_start_date
        end_date = self.start.period_end_date
        notice_input = self.get_notice_input(start_date, end_date)

        if not notice_input:
            self.raise_user_error('no_notice_input_funds', {
                'start_date': start_date,
                'end_date': end_date,
            })

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
        }

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            # 'start_date': start_date,
            'end_date': end_date,
            'notice_input': notice_input,
            'config': config
        }
        return action, data

    def get_notice_input(self, start_date, end_date):
        employees = {}
        contracts = self.get_contracts(start_date, end_date)

        if contracts:
            for contract in contracts:
                employee = contract.employee
                if (not employee.identifier):
                    self.raise_user_error('no_identifier_found', {
                        'employee': employee.rec_name})
                if not contract.register_date_iess or (
                        not contract.code_iess or (
                        not contract.workingday_iess or (
                        not contract.employer_type_code_iess or (
                        not contract.working_relationship_iess or (
                        not contract.code_sectoral_activity or (
                        not contract.origin_payment_iess)))))):
                    self.raise_user_error('no_iess_data', {
                        'employee': employee.rec_name})
                dict_data = {}
                dict_data.update({'start_date': str(
                    contract.start_date).replace("-", ''),
                            'job_position': contract.position.rec_name[0: 62],
                            'salary': contract.salary,
                            'register_date_iess': (
                            str(contract.register_date_iess).replace("-", '')),
                            'working_day': contract.workingday_iess,
                            'social_code': contract.code_iess,
                            'employer_code': (
                                contract.employer_type_code_iess.code),
                            'working_relationship': (
                                contract.working_relationship_iess.code),
                            'activity_code': (
                                contract.code_sectoral_activity.sectorial_code),
                            'origin_payment': contract.origin_payment_iess})
                employees.update({
                    employee.identifier: dict_data
                })
        return employees

    def get_contracts(self, start_date, end_date):

        Contract = Pool().get('company.contract')
        contracts = Contract.search([
            ('start_date', '>=', start_date),
            ('start_date', '<=', end_date),
            ('state', '=', 'done')
        ])
        return contracts


class RetroactiveDifferenceWizardStart(ModelView):
    'Retroactive Difference Wizard Start'
    __name__ = 'retroactive.difference.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    establishment = fields.Many2One('company.establishment', 'Establecimiento',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True, readonly=True)
    period = fields.Many2One('account.period', 'Mes Actual ',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    start_date = fields.Date('Fecha inicio',
        states={
            'readonly': True,
        },
        domain=[
            ('start_date', '<=', Eval('end_date'))
        ], depends=['end_date'], required=True)
    end_date = fields.Date('Fecha fin',
        states={
            'readonly': True,
        },
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date'], required=True)
    movement_type = fields.Char('Tipo de movimiento')
    increment_amount = fields.Numeric('Valor de incremento', digits=(16, 8))

    branch_office_code = fields.Char('Codigo de sucursal',
        states={
            'readonly': ~(~Eval('establishment'))
        }, depends=['establishment'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_movement_type():
        return 'PRA'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @fields.depends('company', 'establishment', 'branch_office_code',
        'start_date', 'end_date')
    def on_change_company(self, name=None):
        if not self.company:
            self.establishment = None
            self.fiscalyear = None
            self.period = None
            self.start_date = None
            self.end_date = None
        else:
            self.establishment = None
            for establishment in self.company.establishments:
                if establishment.active:
                    self.establishment = establishment.id
                    break
            if self.establishment:
                self.branch_office_code = str(
                    self.establishment.number).rjust(4, '0')
            else:
                self.branch_office_code = '0001'

    @fields.depends('period', 'start_date', 'end_date')
    def on_change_period(self):
        if self.period:
            self.start_date = self.period.start_date
            self.end_date = self.period.end_date
        else:
            self.start_date = None
            self.end_date = None


class RetroactiveDifferenceWizard(Wizard):
    'Retroactive Difference Wizard Start'
    __name__ = 'retroactive.difference.wizard'

    start = StateView('retroactive.difference.wizard.start',
        'hr_ec_payslip.retroactive_difference_wizard_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Exportar', 'export_', 'tryton-executable'),
                      ])
    export_ = StateReport('retroactive.difference.export')

    @classmethod
    def __setup__(cls):
        super(RetroactiveDifferenceWizard, cls).__setup__()
        cls._error_messages.update({
            'no_retroactive_difference_funds': ('No se han encontrado registros'
            ' de retroactivos o differencias con fecha inicio "%(start_date)s" '
            'y fecha fin "%(end_date)s".'),
            'no_identifier_found': ('El número de cédula del empleado '
                                '"%(employee)s" no ha sido encontrado.')
        })

    def do_export_(self, action):
        start_date = self.start.start_date
        end_date = self.start.end_date

        retroactive_differences = self.get_retroactive_differences(
            start_date, end_date)

        if not retroactive_differences:
            self.raise_user_error('no_retroactive_difference_funds', {
                'start_date': start_date,
                'end_date': end_date
            })

        config = {
            'branch_office_code': self.start.branch_office_code,
            'movement_type': self.start.movement_type,
        }

        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'period': self.start.period.id,
            'start_date': start_date,
            'end_date': end_date,
            'retroactive_differences': retroactive_differences,
            'config': config
        }
        return action, data

    def get_retroactive_differences(self, start_date, end_date):
        result = {}
        payslips_entries = self.get_payslip_manual_entries(start_date, end_date)
        if payslips_entries:
            for payslip_entry in payslips_entries:
                employee = payslip_entry.employee
                if (not employee.identifier):
                    self.raise_user_error('no_identifier_found')
                dict_data = {}
                dict_data.update({
                    'amount': payslip_entry.amount,
                    'subscription_date': str(
                        payslip_entry.entry_date).replace('-', '')
                })
                result.update({
                    employee.identifier: dict_data
                })
        return result

    def get_payslip_manual_entries(self, start_date, end_date):
        PayslipEntry = Pool().get('payslip.entry')
        payslip_entries = PayslipEntry.search([
            ('entry_date', '>=', start_date),
            ('entry_date', '<=', end_date),
            ('retroactive_difference', '=', True),
            ('state', '=', 'done')
        ])
        return payslip_entries


class ConfigurationReportsBatch(ModelView, ModelSQL):
    'Configuration Reports Batch'
    __name__ = 'configuration.reports.batch'

    reference_model = fields.Many2One(
        'ir.model', 'Modelo reporte batch', required=True,
        domain=[
            ('module', '=', 'hr_ec_payslip'),
            ('model', 'like', '%start%'),
        ])

    contain_period = fields.Function(
        fields.Boolean('Contiene periodo'), 'on_change_with_contain_period')

    reference_field = fields.Many2One(
        'ir.model.field', 'Campo', required=True,
        states={
            'readonly': ~Bool(Eval('contain_period')),
        },
        domain=[
            ('model', '=', Eval('reference_model')),
            ('ttype', '=', 'date'),
        ], depends=['reference_model', 'contain_period'])

    field_date = fields.Many2One(
        'ir.model.field', 'Campo fecha', required=True,
        domain=[
            ('model.model', '=', 'account.period'),
            ('ttype', '=', 'date'),
        ])

    @fields.depends('reference_model', 'reference_field')
    def on_change_reference_model(self):
        if not self.reference_model:
            self.reference_field = None

    @fields.depends('reference_model')
    def on_change_with_contain_period(self, name=None):
        if self.reference_model:
            for row in self.reference_model.fields:
                if row.relation == 'account.period':
                    return True
        return False
