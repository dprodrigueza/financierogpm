# Hr Ec Payslip Module

DESCRIPCIÓN DE UNA PLANTILLA DE ROL
----------------------------------------

Las plantillas de rol definen las reglas que deberán ser tomadas en cuenta para llevar a cabo el cálculo de un rol de pagos específico para un empleado o un conjunto de empleados. Estas plantillas cuentan con los siguientes campos:
- Compañía: La compañía/empresa/municipio a la que pertenece una plantilla de rol, este campo se carga de forma automática según el contexto.
- Código: El código asociado a la plantilla.
- Nombre: El nombre descriptivo de la plantilla.
- Líneas: El conjunto de reglas que definen la plantilla. Cada regla se calcula en base a un orden definido por el responsable de talento humano. Este campo posee dos sub-campos:
  - Secuencia: Número que define el orden en el que se ejecutará una regla. La regla con la secuencia menor será la primera en ser ejecutada.
  - Regla: La regla específica a ser calculada.
- Para calcular beneficios de ley acumulados: Representa un checkbox que, de estar activo, indica que la plantilla será usada para calcular beneficios de ley (décimos) acumulados únicamente.


CREACIÓN DE UNA PLANTILLA DE ROL
----------------------------------------
A continuación, se especifican los pasos a seguir para definir plantillas de rol. 
1. Ingresar el código de la plantilla de rol (Ejemplo: NOMBRAMIENTO)
2. Ingresar el nombre de la plantilla de rol (Ejemplo: NOMBRAMIENTO)
3. A continuación, en el campo líneas se agregan cada una de las reglas (una a la vez):

    3.1. Buscar la regla (Ejemplo: REMUNERACIÓN MENSUAL UNIFICADA (rmu))
    
    3.2. Ingresar número de secuencia (Ejemplo: 1)
    
    3.3. Agregar el resto de reglas siguiendo los pasos 3.1 y 3.2
    
3. Determinar si esta plantilla será usada únicamente para el cálculo de décimos (beneficios de ley) acumulados.


EJEMPLOS DE PLANTILLAS DE ROL
----------------------------------------

En esta sección se presentan plantillas de rol ejemplo para el cálculo de roles mensuales considerando ingresos, deducciones y beneficios de ley (acumulados y mensualizados) de los empleados que tienen nombramiento.

EJEMPLO 1

PLANTILLA DE ROL "NOMBRAMIENTO"

Descripción: Define reglas que calculan los ingresos de los empleados en sus roles mensuales.

Estructura:
1. Código: NOMBRAMIENTO
2. Nombre: NOMBRAMIENTO
3. Líneas:

	3.1  Secuencia: 1;  Regla: REMUNERACIÓN MENSUAL UNIFICADA (rmu)
    
    3.2  Secuencia: 10; Regla: HORAS EXTRA (overtimes)
    
    3.3  Secuencia: 20; Regla: ENCARGOS/SUBROGACIONES (comms_subs)

    3.4  Secuencia: 30; Regla: PRESTAMOS/ANTICIPOS (loans)
	
    3.5  Secuencia: 40; Regla: APORTE PERSONAL 11.45% (per_losep)
	
    3.6  Secuencia: 50; Regla: APORTE PATRONAL 11.15% (pat_losep)
	
    3.7  Secuencia: 60; Regla: IECE (iece)

    3.8  Secuencia: 70;  Regla: DÉCIMO TERCERO (xiii)
    
    3.9  Secuencia: 80;  Regla: DÉCIMO CUARTO (xiv)
    
    3.10 Secuencia: 90; Regla: FONDOS DE RESERVA (fr)
    
    3.11 Secuencia: 100; Regla: IMPUESTO A LA RENTA (imp_renta)

4. Para calcular beneficios de ley acumulados: No seleccionar.

Consideraciones:
- La regla 3.1 calcula el sueldo de un empleado en base a: el número de días trabajados y el sueldo base del empleado según la tabla salarial vigente.
- La regla 3.2 calcula los valores a pagar a un empleado correspondientes a las horas extra que este pudo haber realizado.
- La regla 3.3 calcula los valores a pagar a un empleado por concepto de encargos o subrogaciones.
- La regla 3.4 obtiene los valores a descontar por razón de préstamos o anticipos que la entidad asignó a un empleado.
- Las reglas 3.5, 3.6 y 3.7 calculan los aportes mensuales de los empleados.
- Las reglas 3.8 y 3.9 calculan los valores correspondientes a los décimos tercero y cuarto. Estos valores son creados siempre mensualmente, sin embargo, en este rol se presentarán únicamente los registros de las personas que mensualizan; los décimos acumulados estarán almacenados temporalmente hasta que sean agregados a una plantilla de rol específica para beneficios de ley acumulados (Ver plantilla de rol EJEMPLO 2: DECIMO TERCERO ACUMULADO y EJEMPLO 3: DECIMO CUARTO ACUMULADO).
- La regla 3.10 calcula los fondos de reserva de un empleado siempre que este haya trabajado en la entidad por un año o más, aquí se debe considerar que el empleado puede mensualizar o acumular los valores asignados por fondos de reserva. Si el empleado mensualiza, el valor se incluirá en los ingresos del rol de pagos. Si el empleado acumula, el valor se calculará de forma mensual pero la entidad deberá realizar el depósito de los valores generados en las cuentas designadas por el IESS.
- La regla 3.11 calcula el impuesto a la renta de un empleado en base a los valores presentes en el formulario 107 del empleado en cuestión. Esto deja entrever que para el cálculo del impuesto a la renta, es estrictamente necesario haber llenado previamente el formulario 107 en el módulo correspondiente del sistema. Caso contrario, el valor de este impuesto será de $0,00.
- En este caso, el checkbox que indica si la plantilla calcula los beneficios de ley acumulados no está seleccionado, ya que la intención de esta plantilla es calcular los valores a pagar y deducir de un empleado considerando horas extra, encargos, anticipos, fondos de reserva, etc. No sólo calcular los beneficios de ley acumulados.


EJEMPLO 2

PLANTILLA DE ROL "DECIMO TERCERO ACUMULADO"

Descripción: Define reglas que calculan los décimos terceros acumulados.

Estructura:
1. Código: DECIMO_TERCERO_ACUMULADO
2. Nombre: DECIMO TERCERO ACUMULADO
3. Líneas:

    3.1 Secuencia: 1;  Regla: DÉCIMO TERCERO ACUMULADO (xiii_accumulate)

4. Para calcular beneficios de ley acumulados: Seleccionar.

Consideraciones:
* La regla 3.1 obtiene los décimos terceros previamente calculados y almacenados temporalmente a la espera de su asignación a este rol.
* No te que el checkbox "Para calcular beneficios de ley acumulados" debe ser seleccionado, puesto que esta es una plantilla destinada al cálculo de valores de décimos acumulados.
* No olvide que el décimo tercero acumulado se paga a un empleado en el mes de diciembre y corresponde a los períodos que van desde el 1 diciembre del año anterior hasta el 30 noviembre del año en curso. 

EJEMPLO 3

PLANTILLA DE ROL "DECIMO CUARTO ACUMULADO"

Descripción: Define reglas que calculan los décimos cuartos acumulados.

Estructura:
1. Código: DECIMO_CUARTO_ACUMULADO
2. Nombre: DECIMO CUARTO ACUMULADO
3. Líneas:
	
    3.1 Secuencia: 1;  Regla: DÉCIMO CUARTO ACUMULADO (xiv_accumulate)

4. Para calcular beneficios de ley acumulados: Seleccionar.

Consideraciones:
- La regla 3.1 obtiene los décimos cuartos previamente calculados y almacenados temporalmente a la espera de su asignación a este rol.
- No te que el checkbox "Para calcular beneficios de ley acumulados" debe ser seleccionado, puesto que esta es una plantilla destinada al cálculo de valores de décimos acumulados.
- No olvide que el décimo cuarto acumulado se paga a un empleado en el mes de marzo (región costa e insular) o agosto (región sierra) y corresponde a los siguientes períodos:
  - Región costa e insular: desde el 1 de marzo del año anterior hasta el 28 de febrero del año en curso.
  - Región sierra: desde el 1 de agosto del año anterior hasta el 31 de julio del año en curso. 

Debido a que el décimo tercero y el décimo cuarto se calculan en períodos diferentes, es necesaria la definición de dos plantillas diferentes para calcular los valores de estos beneficios de ley de forma separada.