from unicodedata import normalize
from decimal import Decimal, ROUND_HALF_UP

from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tools import cursor_dict


ZERO = Decimal('0.0')
CENT = Decimal('0.01')
CENT_LB = Decimal('0.0000000001')


def quantize_currency(value, exp=CENT, rounding=ROUND_HALF_UP):
    value = Decimal(str(value))
    amount = value.quantize(exp, rounding=rounding)
    return amount


def normalize_text(text):
    trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
    result = normalize('NFKC', normalize('NFKD', text).translate(trans_tab))
    return result


def manual_entries_in_rule_expression(expression):
    pool = Pool()
    cursor = Transaction().connection.cursor()
    EntryType = pool.get('payslip.entry.type')
    tbl_entry_type = EntryType.__table__()
    result_codes = []
    query = tbl_entry_type.select(tbl_entry_type.code)
    cursor.execute(*query)
    for row in cursor_dict(cursor):
        code = row['code']
        if code in expression:
            result_codes.append(code)
    return result_codes


def fixed_values_in_rule_expression(expression):
    pool = Pool()
    cursor = Transaction().connection.cursor()
    TypeIncomeDeduction = pool.get('company.contract.type.income.deduction')
    tbl_type_income_deduction = TypeIncomeDeduction.__table__()
    result_codes = []
    query = tbl_type_income_deduction.select(tbl_type_income_deduction.code)
    cursor.execute(*query)
    for row in cursor_dict(cursor):
        code = row['code']
        if code in expression:
            result_codes.append(code)
    return result_codes


def get_rule_names_that_use_next_codes(codes, rules):
    if codes and rules:
        rule_names = ''
        for code in codes:
            for rule in rules:
                if is_code_used_on_rule(code, rule):
                    rule_names += '[' + rule.name + ']\n'
                    continue
        if rule_names != '':
            return f"REGLAS DE ROL DE PAGOS:\n{rule_names}"
    return None


def get_law_benefit_type_names_that_use_next_codes(codes, law_benefit_types):
    if codes and law_benefit_types:
        lb_type_names = ''
        for code in codes:
            for lb_type in law_benefit_types:
                if is_code_used_on_law_benefit_type(code, lb_type):
                    lb_type_names += '[' + lb_type.name + ']\n'
                    continue
        if lb_type_names != '':
            return f"TIPOS DE BENEFICIOS DE LEY:\n{lb_type_names}"
    return None


def update_code_on_rules(old_code, new_code, rules):
    if old_code and new_code and rules:
        PayslipRule = Pool().get('payslip.rule')
        to_save_payslip_rules = []
        for rule in rules:
            if is_code_used_on_rule(old_code, rule):
                if isinstance(rule, PayslipRule):
                    # Condition
                    condition = rule.condition
                    condition = condition.replace(old_code, new_code)
                    rule.condition = condition
                    # Expression
                    expression = rule.expression
                    expression = expression.replace(old_code, new_code)
                    rule.expression = expression
                    to_save_payslip_rules.append(rule)
        PayslipRule.save(to_save_payslip_rules)


def update_code_on_law_benefit_types(old_code, new_code, law_beneift_types):
    if old_code and new_code and law_beneift_types:
        LawBenefitType = Pool().get('payslip.law.benefit.type')
        to_save_law_benefit_types = []
        for lb_type in law_beneift_types:
            if is_code_used_on_law_benefit_type(old_code, lb_type):
                if isinstance(lb_type, LawBenefitType):
                    # Expression
                    expression = lb_type.expression
                    expression = expression.replace(old_code, new_code)
                    lb_type.expression = expression
                    to_save_law_benefit_types.append(lb_type)
        LawBenefitType.save(to_save_law_benefit_types)


def is_code_used_on_rule(code, rule):
    PayslipRule = Pool().get('payslip.rule')
    if isinstance(rule, PayslipRule):
        condition = rule.condition
        expression = rule.expression
        if (code in condition) or (code in expression):
            return True
    return False


def is_code_used_on_law_benefit_type(code, law_benefit_type):
    LawBenefitType = Pool().get('payslip.law.benefit.type')
    if isinstance(law_benefit_type, LawBenefitType):
        expression = law_benefit_type.expression
        if code in expression:
            return True
    return False


def set_text_value_on_rules(register_class, registers, new_vals, fieldname):
    if new_vals.get(fieldname, None):
        _cls = register_class
        PayslipRule = Pool().get('payslip.rule')
        LawBenefitType = Pool().get('payslip.law.benefit.type')

        payslip_rules = PayslipRule.search([])
        law_benefit_types = LawBenefitType.search([])

        for register in registers:
            # The old and new value is obtained according to the modifications
            # made by the user
            old_value = getattr(register, fieldname, None)
            new_value = new_vals.get(fieldname, None)

            # Keys in dictionary may be enclosed in single quotation marks or
            # double quotation marks
            old_codes = [f"'{old_value}'", f'"{old_value}"']
            new_code = f"'{new_value}'"

            # Looking for the rules or types of law benefits in which the old
            # codes are used
            rule_names_update = (
                get_rule_names_that_use_next_codes(
                    old_codes, payslip_rules))
            lb_type_names_update = (
                get_law_benefit_type_names_that_use_next_codes(
                    old_codes, law_benefit_types))

            # If used, the warning message is displayed for the user to choose
            # what to do
            if rule_names_update or lb_type_names_update:
                _messagge = ''
                if rule_names_update:
                    _messagge += f'{rule_names_update}\n'
                if lb_type_names_update:
                    _messagge += f'{lb_type_names_update}'
                _cls.raise_user_warning('rules_update', 'rules_update',
                    {'names': _messagge})

            # If the user approves the modifications, the old code is replaced
            # by the new code
            for old_code in old_codes:
                if rule_names_update:
                    update_code_on_rules(
                        old_code, new_code, payslip_rules)
                if lb_type_names_update:
                    update_code_on_law_benefit_types(
                        old_code, new_code, law_benefit_types)


def get_wrong_records_domain_message(records, limit=10):
    limit = limit if limit > 0 else 1
    count = 1
    message = ''
    for record in records:
        if count <= limit:
            message += f'{count}) {record.rec_name}\n'
            count += 1
        else:
            message += '[...]'
            break
    return message