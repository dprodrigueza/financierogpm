from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['FiscalYear', 'Period']


class FiscalYear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'

    base_salary = fields.Integer('Salario base', required=True,
        domain=[('base_salary', '>=', 1)])

    @staticmethod
    def default_base_salary():
        return 1


class Period(metaclass=PoolMeta):
    __name__ = 'account.period'

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._error_messages.update({
            'no_period_before_xiii_found': ('No se ha encontrado el período '
                'anterior al periodo de pago del décimo tercero correspondiente '
                'a las fechas %(xiii_start_date)s - %(xiii_end_date)s.')
        })
