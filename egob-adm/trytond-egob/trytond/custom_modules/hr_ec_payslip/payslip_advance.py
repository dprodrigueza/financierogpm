from datetime import date, datetime, timedelta
from decimal import Decimal, ROUND_HALF_UP
from collections import defaultdict
from calendar import monthrange
from dateutil.relativedelta import relativedelta

from sql.aggregate import Sum
from sql.conditionals import Case
from sql import Null

from trytond.model import (ModelSQL, ModelView, Workflow, fields,
    sequence_ordered)
from trytond.pyson import Eval, If, Bool, Greater, Len
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.tools import grouped_slice, reduce_ids, cursor_dict

from . import evaluator

import pandas as pd
import numpy as np
import pendulum

from trytond.modules.hr_ec_payslip import utils
from trytond.modules.hr_ec_payslip.payslip import (get_fields_values,
    get_single_field_value, apply_contract_data, _KIND,
    get_contracts_with_wrong_domain)


__all__ = ['PayslipGeneralAdvance', 'PayslipAdvance', 'PayslipAdvanceLine',
           'PayslipAdvanceCommSubDetail', 'PayslipAdvanceLoanLine',
           'PayslipAdvanceManualEntry']

ZERO = Decimal('0.0')
CENT = Decimal('0.01')

_STATES = {
    'readonly': Eval('state') != 'draft',
}

_DEPENDS = ['state']

_PAYSLIP_ADVANCE_ITEM_TYPES = [
    ('income', 'Ingreso'),
    ('deduction', 'Deducción'),
]


def get_payslips_configuration():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration


def get_notification_email_payslip_advance_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.notification_email_payslip_advance


def get_previous_period(current_period):
    Period = Pool().get('account.period')
    previous_period = None
    search_year = current_period.start_date.year
    search_month = current_period.start_date.month - 1
    search_day = 1
    if search_month <= 0:
        search_month = 12
        search_year -= 1
    search_date = datetime(search_year, search_month, search_day).date()
    periods = Period.search([
        ('start_date', '<=', search_date),
        ('end_date', '>=', search_date)
    ], order=[('end_date', 'DESC')])

    for period in periods:
        last_day = monthrange(period.end_date.year, period.end_date.month)[1]
        diff_days = np.busday_count(
            period.start_date,
            (period.end_date + timedelta(days=1)),
            weekmask='1111111')
        if last_day == diff_days:
            previous_period = period
            break
    return previous_period

def remove_contracts_with_unpaid_license(period, contracts):
    PayslipGeneral = Pool().get('payslip.general')
    contracts = PayslipGeneral.remove_contracts_with_unpaid_license(period,
        contracts)
    return contracts

def remove_disabled_contracts(period, contracts):
    PayslipGeneral = Pool().get('payslip.general')
    contracts = PayslipGeneral.remove_disabled_contracts(period, contracts)
    return contracts


class PayslipGeneralAdvance(Workflow, ModelSQL, ModelView):
    'Payslip General Advance'
    __name__ = 'payslip.general.advance'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], readonly=True, required=True)
    template = fields.Many2One('payslip.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
            ('type', '=', 'biweekly_advance')
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'get_period_dates')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'get_period_dates')
    effective_date = fields.Date('Fecha efectiva',
        domain=[
            ('effective_date', '>=', Eval('period_start_date')),
            ('effective_date', '<=', Eval('period_end_date')),
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS + ['period_end_date', 'period_start_date'])
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('confirm', 'Confirmado'),
            ('cancel', 'Cancelado'),
            ('done', 'Realizado')
        ], 'Estado', readonly=True, required=True, select=True)
    lines = fields.One2Many('payslip.advance',
        'general', 'Lineas',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'])
    total_general = fields.Function(fields.Numeric('Total general'),
        'get_total_general')
    total_income = fields.Function(fields.Numeric('Total ingresos'),
        'get_total_general')
    total_deduction = fields.Function(fields.Numeric('Total deducciones'),
        'get_total_general')

    @classmethod
    def __setup__(cls):
        super(PayslipGeneralAdvance, cls).__setup__()
        cls._order = [
            ('company', 'ASC'),
            ('effective_date', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': True,
                #'invisible': Eval('state').in_(['cancel', 'draft']),
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'load_lines': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-import'
            },
            'generate_values': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'send_payment_notice_to_emails': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._error_messages.update({
            'no_delete': ('Solamente es posible eliminar registros que estén en'
                ' estado "Borrador".'),
            'contain_lines': ('Existen líneas, primero debe eliminar'),
            'biweekly_payslip_has_not_been_enabled': ('No se ha habilitado en '
                'su compañía el pago de roles quincenales.'),
            'loan_paid': (
                'No se puede cancelar debido a que existe un anticipo pagado'),
            'generated_values_error': ('Los valores de algunos anticipos de '
                'rol no han sido generados.'),
            'loaded_advances_error': ('Los anticipos de rol no han sido '
                'cargados.'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para anticipos de '
                'rol quincenal. Por favor, diríjase a Talento Humano / Roles '
                'de Pagos / Configuración / Configuraciones varias y defina la '
                'configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
            'employees_without_email': ('No se han encontrado direcciones de '
                'correos electrónicos para los siguientes empleados:\n'
                '%(employees)s\n\nLos avisos de pago no serán enviados a estas '
                'personas.'),
            'wrong_records_domain': ('No se puede ejecutar la acción '
                'solicitada ya que los siguientes contratos están en estado '
                'BORRADOR:\n\n%(message)s\n\nPor favor, revise el estado de '
                'dichos contratos o elimínelos de este rol de pagos.'),
        })
        cls._order.insert(0, ('period.end_date', 'DESC'))
        cls._order.insert(1, ('period.start_date', 'DESC'))
        cls._order.insert(2, ('template.name', 'ASC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @staticmethod
    def default_effective_date():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        effective_date = None
        if periods:
            period = periods[0]
            effective_date = datetime(
                period.end_date.year, period.end_date.month, 15).date()
        return effective_date

    @classmethod
    def delete(cls, generals):
        for general in generals:
            if not Transaction().context.get('general_payslip_cancel_state'):
                if general.state in ['done', 'confirm', 'cancel']:
                    cls.raise_user_error('no_delete')
        super(PayslipGeneralAdvance, cls).delete(generals)

    @classmethod
    @ModelView.button
    def send_payment_notice_to_emails(cls, generals):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_advance_conf()
        payslips_without_emails = ''
        if notification_email:
            records_to_notify = []
            for general in generals:
                for payslip_advance in general.lines:
                    if not payslip_advance.employee.email:
                        payslips_without_emails += (
                            '\n[' + payslip_advance.employee.rec_name + ']')
                    else:
                        records_to_notify.append(payslip_advance)

            if payslips_without_emails != '':
                cls.raise_user_warning('employees_without_email',
                    'employees_without_email', {
                        'employees': payslips_without_emails
                    })

            if records_to_notify:
                NotificationEmail.trigger(
                    records_to_notify, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, generals):
        for general in generals:
            general.change_lines_states('draft')

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, generals):
        PayslipGeneralAdvance = Pool().get('payslip.general.advance')
        to_delete = []
        for general in generals:
            general.change_lines_states('cancel')
            # There can not be two payslip general advances with the same
            # template, period and state (In "cancel" state, that could happen)
            canceled_generals = PayslipGeneralAdvance.search([
                ('template', '=', general.template),
                ('period', '=', general.period),
                ('state', '=', 'cancel'),
            ])
            if canceled_generals:
                to_delete += canceled_generals
        # This ignores the restriction of not deleting records in cancel state
        with Transaction().set_context(general_payslip_cancel_state=True):
            PayslipGeneralAdvance.delete(to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, generals):
        # Check contract domain
        cls.check_contract_domains(generals)

        for general in generals:
            general.check_lines_loaded()
            general.change_lines_states('confirm')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, generals):
        # Check contract domain
        cls.check_contract_domains(generals)

        for general in generals:
            general.change_lines_states('done')

    def change_lines_states(self, new_state):
        pool = Pool()
        PayslipAdvanceLine = pool.get('payslip.advance')
        if new_state == 'draft':
            PayslipAdvanceLine.draft(self.lines)
        elif new_state == 'confirm':
            PayslipAdvanceLine.confirm(self.lines)
        elif new_state == 'done':
            PayslipAdvanceLine.done(self.lines)
        elif new_state == 'cancel':
            PayslipAdvanceLine.cancel(self.lines)

    def check_lines_loaded(self):
        if not self.lines:
            self.raise_user_error('loaded_advances_error')
        else:
            for line in self.lines:
                if not (line.income_lines or line.deduction_lines):
                    self.raise_user_error('generated_values_error')

    @classmethod
    def check_contract_domains(cls, generals):
        wrong_records_message = get_contracts_with_wrong_domain(generals)
        if wrong_records_message:
            cls.raise_user_error('wrong_records_domain', {
                'message': wrong_records_message
            })

    @classmethod
    @ModelView.button
    def load_lines(cls, generals):
        pool = Pool()
        Contract = pool.get('company.contract')
        PayslipAdvance = pool.get('payslip.advance')
        DisableContract = pool.get('company.disable.contract')
        to_save = []

        payslips_conf = get_payslips_configuration()
        consider_finalized_contracts_during_payslip_period = (
            payslips_conf.consider_finalized_contracts_during_payslip_period)
        if not payslips_conf.loan_fortnightly:
            cls.raise_user_error('biweekly_payslip_has_not_been_enabled')

        for general in generals:
            # First: Delete payslip advance lines created before for this
            # general advance
            advances = PayslipAdvance.search([
                ('general', '=', general),
                ('state', '!=', 'cancel'),
            ])
            if advances:
                PayslipAdvance.delete(advances)

            # Second: Get contracts information
            _domain = [
                ('payslip_templates', 'in', [general.template.id]),
                ('state', 'in', ['done', 'finalized', 'disabled']),
                ('start_date', '<=', general.effective_date)
            ]
            if consider_finalized_contracts_during_payslip_period:
                _domain.append(['OR',
                    ('end_date', '=', None),
                    ('end_date', '>=', general.period.start_date)
                ])
            else:
                _domain.append(['OR',
                    ('end_date', '=', None),
                    ('end_date', '>', general.period.end_date)
                ])
            contracts = Contract.search(_domain)

            # Contratos con permisos de licencia sin sueldo que no se deben
            # considerar en el rol
            contracts = remove_contracts_with_unpaid_license(
                general.period, contracts)

            # Contratos en estado inhabilitado que no se deben considerar
            # en el rol
            contracts = remove_disabled_contracts(
                general.period, contracts)

            for contract in contracts:
                advances = PayslipAdvance.search([
                    ('contract', '=', contract),
                    ('period', '=', general.period),
                    ('state', '!=', 'cancel'),
                ])
                # Do not create repeated payslip advances for the same contract
                if not advances:
                    if contract.state in ['done', 'finalized', 'disabled']:
                        contract_end_date = contract.end_date
                        if not contract_end_date:
                            contract_end_date = datetime.max.date()
                        if contract_end_date >= general.period.start_date and (
                                contract.start_date < general.period.end_date):
                            # A payslip advance will be created
                            advance = PayslipAdvance()
                            advance.general = general
                            advance.template = general.template
                            advance.company = general.company
                            advance.contract = contract
                            advance.employee = contract.employee
                            advance.email_recipient = contract.employee.party
                            advance.fiscalyear = general.fiscalyear
                            advance.period = general.period
                            advance.effective_date = general.effective_date
                            to_save.append(advance)
        PayslipAdvance.save(to_save)

    @classmethod
    @ModelView.button
    def generate_values(cls, generals):
        pool = Pool()
        PayslipAdvance = pool.get('payslip.advance')
        Contract = pool.get('company.contract')

        # Check contract domain
        cls.check_contract_domains(generals)

        for general in generals:
            salaries, hour_costs = {}, {}
            p_ids = [(p.id, p.contract.id) for p in general.lines]
            with Transaction().set_context(salary_date=general.period.end_date):
                salaries_aux, hour_cost_aux = {}, {}
                # Get contracts
                c_ids = [p.contract.id for p in general.lines]
                contracts = Contract.browse(c_ids)
                # Get salary and hour cost by contract
                info_salaries = Contract.get_salary(contracts, 'salary')
                salaries_aux = info_salaries['salary']
                hour_cost_aux = info_salaries['hour_cost']
                # Set information
                for position in p_ids:
                    salaries[position[0]] = salaries_aux[position[1]]
                    hour_costs[position[0]] = hour_cost_aux[position[1]]
            info_salaries = {
                'salaries': salaries,
                'hour_costs': hour_costs
            }
            with Transaction().set_context(info_salaries=info_salaries):
                PayslipAdvance.load_lines(general.lines)

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @classmethod
    def get_period_dates(cls, items, names=None):
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        for item in items:
            if item.period:
                start_date[item.id] = item.period.start_date
                end_date[item.id] = item.period.end_date
        return {
            'period_start_date': start_date,
            'period_end_date': end_date
        }

    @classmethod
    def get_total_general(cls, generals, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: Decimal("0"))
        result_income = defaultdict(lambda: Decimal("0"))
        result_deduction = defaultdict(lambda: Decimal("0"))
        PayslipAdvance = pool.get('payslip.advance')
        tbl_payslip_advance = PayslipAdvance.__table__()
        query = tbl_payslip_advance.select(
            tbl_payslip_advance.general,
            Sum(tbl_payslip_advance.total_value).as_('total_general'),
            Sum(tbl_payslip_advance.total_income).as_('total_income'),
            Sum(tbl_payslip_advance.total_deduction).as_('total_deduction'),
            group_by=[tbl_payslip_advance.general]
        )
        cursor.execute(*query)
        for (general, total_general, total_income,
                total_deduction) in cursor.fetchall():
            result[general] = total_general
            result_income[general] = total_income
            result_deduction[general] = total_deduction

        return {
            'total_general': result,
            'total_income': result_income,
            'total_deduction': result_deduction,
        }

    def get_rec_name(self, name):
        return 'ANTICIPO DE ROL GENERAL %s: %s' % (
            self.period.rec_name, self.template.rec_name)


class PayslipAdvance(Workflow, ModelSQL, ModelView):
    'Payslip Advance'
    __name__ = 'payslip.advance'

    general = fields.Many2One('payslip.general.advance',
        'Anticipo de rol general',
        domain=[
            ('company', '=', Eval('company')),
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('period', '=', Eval('period')),
        ], depends=['company', 'fiscalyear', 'period'], ondelete='CASCADE')
    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], readonly=True, required=True)
    template = fields.Many2One('payslip.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['biweekly_advance'])
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    employee = fields.Many2One('company.employee', 'Empleado',
        states={
            'readonly': True
        }, required=True)
    contract = fields.Many2One('company.contract', 'Contrato',
        domain=[
            ('company', '=', Eval('company')),
            ('state', 'in', ['done', 'finalized', 'disabled'])
        ],
        context={
            'salary_date': Eval('effective_date')
        }, states=_STATES, depends=_DEPENDS + ['template', 'company'],
        required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo',
        states={
            'invisible': True
        }), 'get_period_dates')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo',
        states={
            'invisible': True
        }), 'get_period_dates')
    effective_date = fields.Date('Fecha efectiva',
        domain=[
            ('effective_date', '>=', Eval('period_start_date')),
            ('effective_date', '<=', Eval('period_end_date')),
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=['period_start_date', 'period_end_date'])
    loan = fields.Many2One('company.employee.loan', 'Registro de anticipo',
        states={
            'readonly': True,
            'invisible': Eval('state').in_(['draft', 'confirm']),
        }, ondelete='CASCADE')
    lines = fields.One2Many('payslip.advance.line', 'payslip_advance', 'Líneas')
    income_lines = fields.One2Many('payslip.advance.line', 'payslip_advance',
        'Ingresos',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'income')])
    deduction_lines = fields.One2Many('payslip.advance.line', 'payslip_advance',
        'Deducciones',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'deduction')])
    comms_subs = fields.Function(
        fields.One2Many('company.commission.subrogation.detail', None,
        'Encargos/Subrogaciones',
        states={
            'invisible': ~Greater(Len(Eval('comms_subs')), 1, True)
        }), 'get_considered_comms_subs_details')
    deduction_loans = fields.Function(
        fields.One2Many('company.employee.loan.line', None,
            'Préstamos/Anticipos',
            domain=[
                ('loan.employee', '=', Eval('employee')),
            ],
            states={
                'readonly': _STATES['readonly'],
            }, depends=_DEPENDS + ['employee']),
        'get_considered_deduction_loans', setter='set_deduction_loans')
    income_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Ingresos manuales',
        states={
            'invisible': ~Greater(Len(Eval('income_entries')), 1, True)
        }), 'get_considered_entries')
    deduction_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Deducciones manuales',
        domain=[
            ('employee', '=', Eval('employee')),
            ('state', '=', 'done')
        ], states=_STATES, depends=_DEPENDS + ['employee']),
        'get_considered_entries', setter='set_deduction_entries')
    total_income = fields.Numeric('Total de ingresos', digits=(16, 2),
        states={'readonly': True})
    total_deduction = fields.Numeric('Total de deducciones', digits=(16, 2),
        states={'readonly': True})
    total_value = fields.Numeric('Total a pagar', digits=(16, 2),
        states={'readonly': True})
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('confirm', 'Confirmado'),
            ('cancel', 'Cancelado'),
            ('done', 'Realizado')
        ], 'Estado', readonly=True, required=True, select=True)
    manual_revision = fields.Boolean('Requiere revisión manual',
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('manual_revision'))
        })

    # EXTRA INFO
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': True
        })
    position = fields.Many2One('company.position', 'Puesto',
        states={
            'readonly': True
        })
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Relación laboral',
        states={
            'readonly': True
        })
    contract_type = fields.Many2One(
        'company.contract_type', 'Tipo de contrato',
        states={
            'readonly': True
        })
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol',
        states={
            'readonly': True
        })
    email_recipient = fields.Many2One('party.party', 'Destinatario e-mail',
        states={
            'readonly': _STATES['readonly'],
            'invisible': True
        }, depends=_DEPENDS)

    # ICONS
    manual_revision_color = fields.Function(fields.Char('color'),
        'get_manual_revision_color')

    @classmethod
    def __setup__(cls):
        super(PayslipAdvance, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': True,
                #'invisible': Eval('state').in_(['cancel', 'draft']),
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'load_lines': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'recalculate_advance': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-refresh'
            },
            'send_payment_notice_to_email': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._error_messages.update({
            'loan_paid': ('No se puede cancelar el %(payslip_advance)s debido '
                'a que su préstamo/anticipo correspondiente ya se encuentra '
                'pagado.'),
            'expression_error': ('Error de expresión en la regla "%(rule)s": '
                '"%(error_message)s"'),
            'condition_error': ('Error de condición en la regla "%(rule)s": '
                '"%(error_message)s".'),
            'invalid_result': ('Resultado inválido "%(result)s" en la regla '
                '"%(rule)s" y empleado "%(employee)s"'),
            'quincenal_loan_type_not_found': ('No se ha encontrado en el '
                'sistema un tipo de anticipo quincenal. Recuerde que los roles '
                'quincenales generan anticipos del tipo mencionado (con 1 '
                'cuota).\n\nPor favor, asegúrese de que el tipo de anticipo '
                'que se desea usar tenga activado el check "Es anticipo '
                'quincenal".'),
            'header_error_wrong_template': ('La plantilla del rol '
                '"%(payslip)s" no coincide con la plantilla de su rol general '
                '"%(general)s". Para actualizar la información, diríjase al '
                'rol general, presione el botón "Cargar roles" y luego el '
                'botón "Generar valores".'),
            'header_error_wrong_period': ('El periodo del rol "%(payslip)s" no '
                'coincide con el periodo de su rol general "%(general)s". Para '
                'actualizar la información, diríjase al rol general, presione '
                'el botón "Cargar roles" y luego el botón "Generar valores".'),
            'contract_with_wrong_salary': ('El contrato "%(contract)s" tiene '
                'mal definido el salario "$%(salary)s".'),
            'contract_with_wrong_hour_cost': ('El contrato "%(contract)s" '
                'tiene mal definido el salario por hora "$%(hour_cost)s".'),
            'no_negative_or_zero_advances': ('No puede generar anticipos '
                'negativos o de valor 0.\n\nPor favor, revise el anticipo '
                'del empleado %(employee)s cuyo valor es de $%(total_value)s.'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para anticipos de '
                'rol pagos quincenal. Por favor, diríjase a Talento Humano / '
                'Roles de Pagos / Configuración / Configuraciones varias y '
                'defina la configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
        })
        cls._order.insert(0, ('manual_revision', 'DESC'))
        cls._order.insert(1, ('period.end_date', 'DESC'))
        cls._order.insert(2, ('period.start_date', 'DESC'))
        cls._order.insert(3, ('employee.party.name', 'ASC'))

    @classmethod
    def view_attributes(cls):
        return super(PayslipAdvance, cls).view_attributes() + [(
            '//page[@id="join_payslip_general"]', 'states', {
                'invisible': Bool(Eval('general')), })
        ]

    @classmethod
    def delete(cls, lines):
        # TODO: remove unused below variables
        # Loan = Pool().get('company.employee.loan')
        # to_delete_loans = [line.loan for line in lines if line.loan]
        super(PayslipAdvance, cls).delete(lines)
        # Loan.delete(to_delete_loans)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @staticmethod
    def default_effective_date():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        effective_date = None
        if periods:
            period = periods[0]
            effective_date = datetime(
                period.end_date.year, period.end_date.month, 15).date()
        return effective_date

    @classmethod
    def default_total_income(cls):
        return 0

    @classmethod
    def default_total_deduction(cls):
        return 0

    @classmethod
    def default_total_value(cls):
        return 0

    @classmethod
    def default_manual_revision(cls):
        return False

    @classmethod
    @ModelView.button
    def send_payment_notice_to_email(cls, payslips):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_advance_conf()
        if notification_email:
            for payslip_advance in payslips:
                if not payslip_advance.employee.email:
                    cls.raise_user_error('employee_without_email', {
                        'employee': payslip_advance.employee.rec_name
                    })
            NotificationEmail.trigger(payslips, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, advances):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, advances):
        for advance in advances:
            loan = advance.loan
            if loan and loan.state == 'paid':
                cls.raise_user_error('loan_paid', {
                    'payslip_advance': advance.rec_name
                })
            else:
                loan.cancel([loan])

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, advances):
        cls.check_header_information(advances)
        for advance in advances:
            if advance.total_value <= 0:
                cls.raise_user_error('no_negative_or_zero_advances', {
                    'employee': advance.employee.rec_name,
                    'total_value': advance.total_value
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, advances):
        cls.check_header_information(advances)
        for advance in advances:
            if advance.total_value <= 0:
                cls.raise_user_error('no_negative_or_zero_advances', {
                    'employee': advance.employee.rec_name,
                    'total_value': advance.total_value
                })
        cls.generate_loans(advances)

    @fields.depends('contract')
    def on_change_with_email_recipient(self, name=None):
        if self.contract:
            return self.contract.employee.party.id
        return None

    @fields.depends('contract')
    def on_change_with_employee(self, name=None):
        if self.contract:
            return self.contract.employee.id
        return None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('income_lines')
    def on_change_with_total_income(self, name=None):
        total = 0
        if self.income_lines:
            for income in self.income_lines:
                if income.amount:
                    total += income.amount
        return total

    @fields.depends('deduction_lines')
    def on_change_with_total_deduction(self, name=None):
        total = 0
        if self.deduction_lines:
            for deduction in self.deduction_lines:
                if deduction.amount:
                    total += deduction.amount
        return total

    @fields.depends('total_income', 'total_deduction')
    def on_change_with_total_value(self, name=None):
        total = 0
        if self.total_income and self.total_deduction:
            total = self.total_income - self.total_deduction
        return total

    @fields.depends('total_value')
    def on_change_with_manual_revision(self, name=None):
        if self.total_value:
            return (self.total_value <= 0)
        return False

    @classmethod
    def get_period_dates(cls, items, names=None):
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        for item in items:
            if item.period:
                start_date[item.id] = item.period.start_date
                end_date[item.id] = item.period.end_date
        return {
            'period_start_date': start_date,
            'period_end_date': end_date
        }

    @classmethod
    def apply_contract_data(cls, payslips):
        apply_contract_data(cls, payslips)

    @classmethod
    @ModelView.button
    def recalculate_advance(cls, payslips):
        cls.load_lines(payslips, is_recalculate=True)

    @classmethod
    @ModelView.button
    def load_lines(cls, advances, is_recalculate=False):
        # Verify headers of individual payslip advances match with general
        # payslip advances
        cls.check_header_information(advances)

        # Set extra info in every payslip advances
        cls.apply_contract_data(advances)

        # Get models
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        PayslipAdvance = pool.get('payslip.advance')
        PayslipAdvanceLine = pool.get('payslip.advance.line')
        PayslipAdvanceManualEntry = pool.get('payslip.advance.manual.entry')
        PayslipAdvanceCommSubDetail = pool.get(
            'payslip.advance.commission.subrogation.detail')
        PayslipAdvanceLoanLine = pool.get('payslip.advance.loan.line')
        Contract = pool.get('company.contract')

        # Subrogations
        comms_subs = cls.get_all_comms_subs_details(advances)['comms_subs']

        # Dedduction loans
        deduction_loans = cls.get_all_deduction_loans(advances,
            is_recalculate=is_recalculate)['deduction_loans']

        # Manual entries
        manual_entries = cls.get_all_entries(advances,
            is_recalculate=is_recalculate)
        income_entries = manual_entries['income_entries']
        deduction_entries = manual_entries['deduction_entries']

        # Get amounts
        comms_subs_amounts = PayslipAdvance.get_comms_subs_amounts(
            advances, comms_subs)

        loans_amounts = PayslipAdvance.get_loans_amounts(advances,
            deduction_loans)

        comms_subs_amounts_by_type = (
            PayslipAdvance.get_comms_subs_amounts_by_type(advances, comms_subs))

        loans_amounts_by_type = PayslipAdvance.get_loans_amounts_by_type(
            advances, deduction_loans)

        # Get amounts by mannual entry
        manual_entry_amounts = PayslipAdvance.get_manual_entries_amounts(
            advances, income_entries, deduction_entries)

        # # Get total values previous period
        total_values_previous_period = (
            PayslipAdvance.get_total_values_previous_period(advances))

        # Get context
        context = Transaction().context

        to_save_payslip_advance_manual_entries = []
        to_save_payslip_advance_deduction_loans = []
        to_save_payslip_comm_subs_details = []
        to_update_advances = []
        to_save_advance_lines = []
        to_delete_advance_lines = []

        totals = defaultdict(lambda: [])
        salaries = defaultdict(lambda: [])
        hour_costs = defaultdict(lambda: [])

        for advance in advances:
            with Transaction().set_context(salary_date=advance.period.end_date):
                # Get employee salaries and hour_costs
                if context.get('info_salaries'):
                    info_salaries = context['info_salaries']
                    salaries[advance.id] = info_salaries['salaries'][advance.id]
                    hour_costs[advance.id] = utils.quantize_currency(
                        info_salaries['hour_costs'][advance.id])
                else:
                    # Get salary and hour cost by contract
                    contract = advance.contract
                    info_salaries = Contract.get_salary([contract], 'salary')
                    salaries_aux = info_salaries['salary']
                    hour_cost_aux = info_salaries['hour_cost']

                    # Set salary and hour cost information
                    salaries[advance.id] = salaries_aux[contract.id]
                    hour_costs[advance.id] = hour_cost_aux[contract.id]

                if salaries[advance.id] <= 0:
                    cls.raise_user_error('contract_with_wrong_salary', {
                        'contract': advance.contract.rec_name,
                        'salary': salaries[advance.id]
                    })

                if hour_costs[advance.id] <= 0:
                    cls.raise_user_error('contract_with_wrong_hour_cost', {
                        'contract': advance.contract.rec_name,
                        'hour_cost': hour_costs[advance.id]
                    })

                # Totals definition
                totals[advance.id] = {
                    'total_income': ZERO,
                    'total_deduction': ZERO,
                }

                # Delete previous lines
                to_delete_advance_lines += advance.lines

                # Get work relationship
                work_relationship = ''
                if advance.contract.work_relationship:
                    work_relationship = advance.contract.work_relationship.name

                # Get contract type
                contract_type = ''
                if advance.contract.contract_type:
                    contract_type = advance.contract.contract_type.name

                occupational_group = ''
                if advance.contract.occupational_group:
                    occupational_group = advance.contract.occupational_group.name

                hierarchical_level = ''
                if advance.contract.hierarchical_level:
                    hierarchical_level = advance.contract.hierarchical_level.name

                # Get number of family charges
                num_family_burdens = Payslip.get_number_family_burdens(
                    advance.employee)

                # Dictionary of codes used in the definition of rules
                names = {
                    # INFO Libraries and Objects
                    'np': np,
                    'pendulum': pendulum,
                    'pandas': pd,
                    'utils': utils,
                    'date': date,
                    'timedelta': timedelta,
                    'relativedelta': relativedelta,
                    'monthrange': monthrange,
                    'Decimal': Decimal,
                    'PayslipAdvance': PayslipAdvance,
                    # INFO Advance
                    'advance': advance,
                    'contract': advance.contract,
                    'employee': advance.employee,
                    'period_start_date': advance.period.start_date,
                    'period_end_date': advance.period.end_date,
                    'contract_start_date': advance.contract.start_date,
                    'contract_end_date': advance.contract.end_date,
                    'occupational_group': occupational_group,
                    'hierarchical_level': hierarchical_level,
                    # INFO Salary'
                    'base_salary': Decimal(advance.fiscalyear.base_salary),
                    'salary': salaries[advance.id],
                    'hour_cost': hour_costs[advance.id],
                    # INFO Employee
                    'contract_type': contract_type,
                    'work_relationship': work_relationship,
                    # INFO Family burdens
                    'total_burdens': num_family_burdens['total'],
                    'total_burdens_under_age': num_family_burdens['under_age'],
                    'total_burdens_legal_age': num_family_burdens['legal_age'],
                    'total_burdens_with_subsidy':
                        num_family_burdens['with_subsidy'],
                    'total_burdens_with_kindergarten':
                        num_family_burdens['with_kindergarten'],
                    'total_burdens_with_disability':
                        num_family_burdens['with_disability'],
                    'total_burdens_with_spouse_extension':
                        num_family_burdens['with_spouse_extension'],
                    'have_spouse_extension':
                        (num_family_burdens['with_spouse_extension'] > 0),
                    # INFO Previous payments
                    'total_values_previous_period':
                        total_values_previous_period[advance.id],
                    # INFO Manual entries
                    'manual_entry_amounts':
                        manual_entry_amounts[advance.id],
                    # INFO Totals separated amounts by type
                    'total_comms_subs_by_type':
                        comms_subs_amounts_by_type[advance.id],
                    'total_loans_by_type':
                        loans_amounts_by_type[advance.id],
                    # INFO Totals
                    'total_loans': loans_amounts[advance.id],
                    'total_comms_subs': comms_subs_amounts[advance.id],
                    'total_income': 0,
                    'total_deduction': 0,
                    'total_by_rule': defaultdict(lambda: 0),
                }

                parser = evaluator.RuleParser(names)
                result = None

                if not advance.template.accumulated_law_benefits:
                    # Generate result
                    result = PayslipAdvance.calculate_rules(
                        advance, parser, names)

                    # Create or update considered payslip comms subs
                    to_save_payslip_comm_subs_details += (
                        cls.manage_payslip_comm_subs_details(
                            advance,
                            comms_subs[advance.id])['to_save'])

                    # Create or update considered payslip deduction loans
                    to_save_payslip_advance_deduction_loans += (
                        cls.manage_payslip_advance_loan_lines(
                            advance,
                            deduction_loans[advance.id])['to_save'])

                    # Create or update considered payslip manual entries
                    considered_entries_codes = (
                        result['considered_entries_codes'] if result else [])
                    to_save_payslip_advance_manual_entries += (
                        cls.manage_payslip_advance_manual_entries(
                            advance,
                            income_entries[advance.id],
                            deduction_entries[advance.id],
                            considered_entries_codes)['to_save'])

                if result:
                    to_save_advance_lines += result['to_save_lines']

                    # Get partial totals
                    totals[advance.id] = {
                        'total_income': result['total_income'],
                        'total_deduction': result['total_deduction'],
                    }

                    # Get final totals
                    income = totals[advance.id]['total_income']
                    deduction = totals[advance.id]['total_deduction']

                    # Update totals in payslip advance
                    advance.total_income = utils.quantize_currency(income)
                    advance.total_deduction = utils.quantize_currency(deduction)
                    advance.total_value = utils.quantize_currency(
                        (advance.total_income - advance.total_deduction))

                    # Update dict codes with totals
                    names['total_income'] = advance.total_income
                    names['total_deduction'] = advance.total_deduction
                    names['total_value'] = advance.total_value

                    # Was the total value less or equal than 0?
                    advance.manual_revision = (advance.total_value <= 0)

                    # Update payslip
                    to_update_advances.append(advance)

        # Save changes
        PayslipAdvance.save(to_update_advances)
        PayslipAdvanceLine.delete(to_delete_advance_lines)
        PayslipAdvanceLine.save(to_save_advance_lines)
        PayslipAdvanceManualEntry.save(to_save_payslip_advance_manual_entries)
        PayslipAdvanceCommSubDetail.save(to_save_payslip_comm_subs_details)
        PayslipAdvanceLoanLine.save(to_save_payslip_advance_deduction_loans)

    @classmethod
    def calculate_rules(cls, advance, parser, names):
        considered_entries_codes = []
        to_save_lines = []

        for line in advance.template.lines:
            rule = line.rule
            if not rule.active:
                continue

            parser.aeval.symtable['rule'] = rule
            condition = parser.eval_expr(rule.condition)
            if parser.error:
                cls.raise_user_error('condition_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not condition:
                continue

            result = parser.eval_expr(rule.expression)
            if parser.error:
                cls.raise_user_error('expression_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not isinstance(result, list):
                result = [result]
            parser.aeval.symtable[rule.code] = 0

            # Get mannual entry codes used in every expression
            considered_entries_codes += (
                utils.manual_entries_in_rule_expression(rule.expression))

            for res in result:
                if not isinstance(res, dict):
                    cls.raise_user_error('invalid_result', {
                        'result': res,
                        'rule': rule.rec_name,
                        'employee': advance.employee.rec_name,
                    })

                value = Decimal(res['value']) if res else ZERO
                amount = utils.quantize_currency(value)
                parser.aeval.symtable[rule.code] += amount

                if rule.type_ == 'income':
                    parser.aeval.symtable['total_income'] += amount
                elif rule.type_ == 'deduction':
                    parser.aeval.symtable['total_deduction'] += amount

                # Save amount of rule in dict_names
                if rule.code not in names['total_by_rule']:
                    names['total_by_rule'].update({rule.code: amount})
                else:
                    names['total_by_rule'][rule.code] += amount

                # Create line
                if amount > 0:
                    pline = PayslipAdvance.get_new_payslip_advance_line(
                        advance, rule, rule.type_, amount)
                    to_save_lines.append(pline)

        t_income = parser.aeval.symtable['total_income']
        t_deduction = parser.aeval.symtable['total_deduction']

        # Delete duplicates
        considered_entries_codes = list(set(considered_entries_codes))

        return {
            'total_income': utils.quantize_currency(t_income),
            'total_deduction': utils.quantize_currency(t_deduction),
            'to_save_lines': to_save_lines,
            'considered_entries_codes': considered_entries_codes
        }

    @classmethod
    def check_header_information(cls, advances):
        if advances:
            for advance in advances:
                if advance.general:
                    if advance.template != advance.general.template:
                        cls.raise_user_error('header_error_wrong_template', {
                            'advance': advance.rec_name,
                            'general': advance.general.rec_name
                        })
                    elif advance.period != advance.general.period:
                        cls.raise_user_error('header_error_wrong_period', {
                            'advance': advance.rec_name,
                            'general': advance.general.rec_name
                        })

    @classmethod
    def generate_loans(cls, advances):
        pool = Pool()
        PayslipAdvance = pool.get('payslip.advance')
        Loan = pool.get('company.employee.loan')
        LoanLine = pool.get('company.employee.loan.line')
        LoanType = pool.get('company.employee.loan.type')

        to_save_loans = []
        to_save_loans_lines = []
        to_save_advance_lines = []

        loan_types = LoanType.search([
            ('is_quincenal_loan', '=', 'True')
        ])
        if not loan_types:
            cls.raise_user_error('quincenal_loan_type_not_found')
        type = loan_types[0]

        for advance in advances:
            # Create Loan
            contract = advance.contract
            loan = Loan()
            loan.employee = contract.employee
            loan.company = loan.employee.company
            loan.on_change_employee()
            loan.request_date = advance.effective_date
            loan.start_date = advance.effective_date
            loan.end_date = advance.period.end_date
            loan.fee_amount = advance.total_value
            loan.total_amount = advance.total_value
            loan.pending_amount = loan.on_change_with_pending_amount()
            loan.type = type
            loan.is_quincenal_loan = loan.on_change_with_is_quincenal_loan()
            loan.maximum_amount = loan.on_change_with_maximum_amount()
            loan.maximum_amount = utils.quantize_currency(
                loan.maximum_amount, exp=ZERO)
            loan.percentage_loan_type = (
                loan.on_change_with_percentage_loan_type())
            loan.state = 'draft'

            # Create Loan Line
            loan_line = LoanLine()
            loan_line.loan = loan
            loan_line.capital_amount = advance.total_value
            loan_line.start_date = advance.effective_date
            loan_line.effective_date = advance.effective_date
            loan_line.state = 'pending'

            # To save loans data
            to_save_loans.append(loan)
            to_save_loans_lines.append(loan_line)

            # To save payslip advance data
            advance.loan = loan
            to_save_advance_lines.append(advance)

        # Save data
        Loan.save(to_save_loans)
        LoanLine.save(to_save_loans_lines)
        PayslipAdvance.save(to_save_advance_lines)

        # Change Loan state to request state
        Loan.request(to_save_loans)

    @classmethod
    def get_considered_comms_subs_details(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipAdvanceCommSubDetail = pool.get(
            'payslip.advance.commission.subrogation.detail')
        table = cls.__table__()
        tbl_payslip_advance_comm_sub_detail = (
            PayslipAdvanceCommSubDetail.__table__())

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(tbl_payslip_advance_comm_sub_detail,
                condition=(table.id ==
                    tbl_payslip_advance_comm_sub_detail.payslip_advance)
            ).select(
                table.id,
                tbl_payslip_advance_comm_sub_detail.comm_sub_detail,
                where=red_sql &
                    (tbl_payslip_advance_comm_sub_detail.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['comm_sub_detail'])

        return {
            'comms_subs': result,
        }

    @classmethod
    def get_considered_deduction_loans(cls, advances, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: [])
        table = cls.__table__()
        PayslipAdvanceLoanLine = pool.get('payslip.advance.loan.line')
        tbl_payslip_advance_loan_line = PayslipAdvanceLoanLine.__table__()
        ids = [a.id for a in advances]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(tbl_payslip_advance_loan_line,
                condition=(
                    table.id == tbl_payslip_advance_loan_line.payslip_advance)
                ).select(
                    table.id,
                    tbl_payslip_advance_loan_line.loan_line,
                where=red_sql & (
                        tbl_payslip_advance_loan_line.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['loan_line'])
        return {
            'deduction_loans': result,
        }

    @classmethod
    def get_considered_entries(cls, advances, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipAdvanceManualEntry = pool.get('payslip.advance.manual.entry')
        table = cls.__table__()
        tbl_payslip_manual_entry = PayslipAdvanceManualEntry.__table__()

        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])

        ids = [p.id for p in advances]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(tbl_payslip_manual_entry,
                condition=(table.id == tbl_payslip_manual_entry.payslip_advance)
            ).select(
                table.id,
                tbl_payslip_manual_entry.entry,
                tbl_payslip_manual_entry.kind,
                where=red_sql & (tbl_payslip_manual_entry.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['kind'] == 'income':
                    incomes[row['id']].append(row['entry'])
                elif row['kind'] == 'deduction':
                    deductions[row['id']].append(row['entry'])

        return {
            'income_entries': incomes,
            'deduction_entries': deductions,
        }

    @classmethod
    def set_deduction_loans(cls, advances, name, value):
        pool = Pool()
        EmployeeLoanLine = pool.get('company.employee.loan.line')
        PayslipAdvanceLoanLine = pool.get('payslip.advance.loan.line')
        to_save = []
        for advance in advances:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    loan = EmployeeLoanLine.search([('id', '=', id)])[0]
                    payslip_advance_loan_lines = PayslipAdvanceLoanLine.search([
                        ('payslip_advance', '=', advance),
                        ('loan_line', '=', loan)
                    ])
                    if operation == 'remove':
                        if loan in advance.deduction_loans:
                            payslip_advance_loan_lines[0].was_considered = False
                            to_save.append(payslip_advance_loan_lines[0])
                    elif operation == 'add':
                        if len(payslip_advance_loan_lines) > 0:
                            payslip_advance_loan_lines[0].was_considered = True
                            to_save.append(payslip_advance_loan_lines[0])
                        elif loan not in advance.deduction_loans:
                            payslip_advance_loan_line = PayslipAdvanceLoanLine()
                            payslip_advance_loan_line.payslip_advance = advance
                            payslip_advance_loan_line.loan_line = loan
                            payslip_advance_loan_line.was_considered = True
                            to_save.append(payslip_advance_loan_line)
        PayslipAdvanceLoanLine.save(to_save)

    @classmethod
    def set_deduction_entries(cls, advances, name, value):
        pool = Pool()
        PayslipEntry = pool.get('payslip.entry')
        PayslipAdvanceManualEntry = pool.get('payslip.advance.manual.entry')
        to_save = []
        for advance in advances:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    entry = PayslipEntry.search([('id', '=', id)])[0]
                    p_advance_manual_entries = PayslipAdvanceManualEntry.search([  # noqa
                        ('payslip_advance', '=', advance),
                        ('entry', '=', entry)
                    ])
                    if operation == 'remove':
                        if entry in advance.deduction_entries:
                            p_advance_manual_entries[0].was_considered = False
                            to_save.append(p_advance_manual_entries[0])
                    elif operation == 'add':
                        if len(p_advance_manual_entries) > 0:
                            p_advance_manual_entries[0].was_considered = True
                            to_save.append(p_advance_manual_entries[0])
                        elif entry not in advance.deduction_entries:
                            p_advance_manual_entry = PayslipAdvanceManualEntry()
                            p_advance_manual_entry.payslip = advance
                            p_advance_manual_entry.entry = entry
                            p_advance_manual_entry.kind = entry.kind
                            p_advance_manual_entry.was_considered = True
                            to_save.append(p_advance_manual_entry)
        PayslipAdvanceManualEntry.save(to_save)

    @classmethod
    def get_all_comms_subs_details(cls, advances, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        CommSub = pool.get('company.commission.subrogation')
        Detail = pool.get('company.commission.subrogation.detail')
        PayslipAdvanceCommSubDetail = pool.get(
            'payslip.advance.commission.subrogation.detail')
        ActionStaff = pool.get('company.action.staff')
        tbl_comm_sub = CommSub.__table__()
        tbl_detail = Detail.__table__()
        tbl_payslip_advance_comm_sub_detail = (
            PayslipAdvanceCommSubDetail.__table__())
        tbl_action_staff = ActionStaff.__table__()

        result = defaultdict(lambda: [])
        for advance in advances:
            # Get possible comm/sub details to consider in this payslip advance
            table_A = tbl_comm_sub.join(tbl_detail,
                condition=(tbl_comm_sub.id == tbl_detail.commission)
            ).join(tbl_action_staff,
                condition=(tbl_action_staff.id == tbl_comm_sub.action_staff)
            ).select(
                tbl_action_staff.employee,
                tbl_detail.id.as_('comm_sub_detail'),
                tbl_detail.end_date,
                where=((tbl_comm_sub.state == 'done') &
                       (tbl_action_staff.employee == advance.employee.id) &
                       (tbl_detail.start_date <= advance.effective_date)
                )
            )
            # Get comm/sub details considered in other payslip advances
            table_B = tbl_payslip_advance_comm_sub_detail.join(tbl_detail,
                condition=(tbl_detail.id ==
                    tbl_payslip_advance_comm_sub_detail.comm_sub_detail)
            ).select(
                tbl_payslip_advance_comm_sub_detail.comm_sub_detail.as_(
                    'comm_sub_detail_aux'),
                tbl_payslip_advance_comm_sub_detail.payslip_advance,
                where=((tbl_payslip_advance_comm_sub_detail.was_considered ==
                        't') & (tbl_detail.start_date <= advance.effective_date)
                )
            )
            # Get comm/sub details to consider in this payslip
            query = table_A.join(table_B, type_='LEFT OUTER',
                condition=(
                    table_A.comm_sub_detail == table_B.comm_sub_detail_aux)
            ).select(
                table_A.comm_sub_detail,
                where=((table_B.payslip_advance == advance.id) |
                       (table_B.comm_sub_detail_aux == Null)
                )
            )
            # Execute
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[advance.id].append(row['comm_sub_detail'])
        return {
            'comms_subs': result,
        }

    @classmethod
    def get_all_deduction_loans(cls, advances, names=None,
            is_recalculate=False):
        result = defaultdict(lambda: [])

        # If you have not requested to re-calculate the payslip advance, the
        # system obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            cursor = Transaction().connection.cursor()
            Loan = pool.get('company.employee.loan')
            LoanLine = pool.get('company.employee.loan.line')
            LoanType = pool.get('company.employee.loan.type')
            PayslipAdvanceLoanLine = pool.get('payslip.advance.loan.line')
            tbl_loan = Loan.__table__()
            tbl_loan_line = LoanLine.__table__()
            tbl_loan_type = LoanType.__table__()
            tbl_payslip_advance_loan_line = PayslipAdvanceLoanLine.__table__()

            for advance in advances:
                # Get possible loan lines to consider in this payslip advance
                table_A = tbl_loan.join(tbl_loan_line,
                        condition=(tbl_loan.id == tbl_loan_line.loan)
                    ).join(tbl_loan_type,
                        condition=(tbl_loan.type == tbl_loan_type.id)
                    ).select(
                        tbl_loan.employee,
                        tbl_loan_line.id.as_('loan_line'),
                        tbl_loan_line.start_date,
                        where=((tbl_loan.state == 'open') &
                               (tbl_loan.employee == advance.employee.id) &
                               (tbl_loan_line.state == 'pending') &
                               ((tbl_loan_type.is_quincenal_loan != 't') |
                                (tbl_loan_type.is_quincenal_loan == Null)) &
                               (tbl_loan_line.start_date <=
                                    advance.period.end_date)
                        )
                )
                # Get loan lines considered in other payslip advances
                table_B = tbl_payslip_advance_loan_line.join(tbl_loan_line,
                    condition=(tbl_loan_line.id ==
                        tbl_payslip_advance_loan_line.loan_line)
                    ).select(
                        tbl_payslip_advance_loan_line.loan_line.as_(
                            'loan_line_aux'),
                        tbl_payslip_advance_loan_line.payslip_advance,
                    where=((tbl_payslip_advance_loan_line.was_considered == 't')
                           & (tbl_loan_line.start_date <=
                                  advance.period.end_date))
                )
                # Get loan lines to consider in this payslip advance
                query = table_A.join(table_B, type_='LEFT OUTER',
                        condition=(table_A.loan_line == table_B.loan_line_aux)
                    ).select(
                        table_A.loan_line,
                        where=((table_B.payslip_advance == advance.id) |
                               ((table_B.loan_line_aux == Null) &
                                (table_A.start_date <=
                                    advance.period.end_date)))
                )
                # Execute
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    result[advance.id].append(row['loan_line'])
        # Else, the system works with the information manipulated by the
        # user in the form.
        else:
            for advance in advances:
                for tbl_loan_line in advance.deduction_loans:
                    result[advance.id].append(tbl_loan_line.id)
        return {
            'deduction_loans': result,
        }

    @classmethod
    def get_all_entries(cls, advances, names=None, is_recalculate=False):
        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])
        # If you have not requested to re-calculate the payslip advance, the
        # system obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            cursor = Transaction().connection.cursor()
            Entry = pool.get('payslip.entry')
            PayslipAdvanceManualEntry = pool.get('payslip.advance.manual.entry')
            PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
            tbl_entry = Entry.__table__()
            tbl_payslip_advance_manual_entry = (
                PayslipAdvanceManualEntry.__table__())
            tbl_payslip_manual_entry = PayslipManualEntry.__table__()

            considered_entries_month_payslips = []

            for advance in advances:
                # Get entries considered in monthly payslips (these entries
                # should no longer be considered in the payslip advance
                # calculations)
                query_payslip_entries = tbl_payslip_manual_entry.join(tbl_entry,
                    condition=(tbl_entry.id == tbl_payslip_manual_entry.entry)
                ).select(
                    tbl_payslip_manual_entry.entry,
                    tbl_payslip_manual_entry.payslip,
                    where=((tbl_payslip_manual_entry.was_considered == 't') &
                           (tbl_entry.employee == advance.employee.id) &
                           (tbl_entry.entry_date <= advance.period.end_date)))

                cursor.execute(*query_payslip_entries)
                for row in cursor_dict(cursor):
                    considered_entries_month_payslips.append(row['entry'])

                # Get possible entries to consider in this payslip advance
                table_A = tbl_entry.select(
                    tbl_entry.employee,
                    tbl_entry.id.as_('entry'),
                    tbl_entry.kind,
                    tbl_entry.entry_date,
                    where=((tbl_entry.employee == advance.employee.id) &
                           (tbl_entry.state == 'done') &
                           (tbl_entry.entry_date <= advance.effective_date)
                           & (tbl_entry.entry_date >= advance.period.start_date)
                    )
                )
                # Get entries considered in other payslip advance
                table_B = tbl_payslip_advance_manual_entry.join(tbl_entry,
                    condition=(
                        tbl_entry.id == tbl_payslip_advance_manual_entry.entry)
                ).select(
                    tbl_payslip_advance_manual_entry.entry.as_('entry_aux'),
                    tbl_payslip_advance_manual_entry.payslip_advance,
                    where=((tbl_payslip_advance_manual_entry.was_considered == 't') &  # noqa
                           (tbl_entry.entry_date <= advance.effective_date)
                    )
                )
                # Get entries to consider in this payslip advance
                query = table_A.join(table_B, type_='LEFT OUTER',
                    condition=(table_A.entry == table_B.entry_aux)
                ).select(
                    table_A.entry,
                    table_A.kind,
                    where=((table_B.payslip_advance == advance.id) |
                           (table_B.entry_aux == Null)
                    )
                )
                # Execute
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    if row['entry'] not in considered_entries_month_payslips:
                        if row['kind'] == 'income':
                            incomes[advance.id].append(row['entry'])
                        elif row['kind'] == 'deduction':
                            deductions[advance.id].append(row['entry'])
        else:
            # Else, the system works with the information manipulated by the
            # user in the form.
            for advance in advances:
                for income_entry in advance.income_entries:
                    incomes[advance.id].append(income_entry.id)
                for deduction_entry in advance.deduction_entries:
                    deductions[advance.id].append(deduction_entry.id)

        return {
            'income_entries': incomes,
            'deduction_entries': deductions
        }

    @classmethod
    def get_comms_subs_amounts(cls, advances, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')

        field_name = 'amount'
        result = defaultdict(lambda: [])
        for advance in advances:
            fields = get_fields_values(
                CommSubDetail, comms_subs_details[advance.id], [field_name])
            amount = sum([field[field_name] for field in fields])
            result[advance.id] = amount
        return result

    @classmethod
    def get_loans_amounts(cls, advances, deductions_loans):
        pool = Pool()
        LoanLine = pool.get('company.employee.loan.line')

        field_name = 'fee_residue'
        result = defaultdict(lambda: [])
        for advance in advances:
            fields = get_fields_values(
                LoanLine, deductions_loans[advance.id], [field_name])
            amount = sum([field[field_name] for field in fields])
            result[advance.id] = amount
        return result

    @classmethod
    def get_manual_entries_amounts(cls, advances, income_entries,
            deduction_entries):
        pool = Pool()
        Entry = pool.get('payslip.entry')
        EntryType = pool.get('payslip.entry.type')

        manual_entries_amounts = defaultdict(lambda: [])
        for payslip in advances:
            entries = defaultdict(lambda: 0)
            # Union entries
            list_entries = []
            if income_entries:
                list_entries += income_entries[payslip.id]
            if deduction_entries:
                list_entries += deduction_entries[payslip.id]
            # Manual entries
            if list_entries:
                fields = get_fields_values(
                    Entry, list_entries, ['type_', 'real_amount', 'amount'])
                for field in fields:
                    code_entry = get_single_field_value(
                        EntryType, field['type_'], 'code')
                    amount = field['real_amount']
                    # amount = amount if amount is None else field['amount']
                    if code_entry not in entries:
                        entries[code_entry] = amount
                    else:
                        entries[code_entry] += amount
            # Results
            manual_entries_amounts[payslip.id] = entries
        return manual_entries_amounts

    @classmethod
    def get_total_values_previous_period(cls, advances):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        result = defaultdict(lambda: None)

        for advance in advances:
            names = defaultdict(lambda: 0)
            previous_period = get_previous_period(advance.period)
            if previous_period:
                payslips = Payslip.search([
                    ('contract', '=', advance.contract),
                    ('period', '=', previous_period),
                    ('template.accumulated_law_benefits', '=', False),
                    ('state', 'not in', ['draft', 'confirm', 'cancel'])
                ])
                if payslips:
                    names['total_value'] = 0
                    names['total_income'] = 0
                    names['total_deduction'] = 0
                    # GET GENERAL TOTALS
                    for payslip in payslips:
                        names['total_value'] += payslip.total_value
                        names['total_income'] += payslip.total_income
                        names['total_deduction'] += payslip.total_deduction
                        # GET LINE TOTALS
                        for line in payslip.lines:
                            if line.rule:
                                if line.rule.code not in names:
                                    names[line.rule.code] = line.amount
                                else:
                                    names[line.rule.code] += line.amount
            result[advance.id] = names
        return result

    @classmethod
    def get_comms_subs_amounts_by_type(cls, advances, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')
        CommSubType = pool.get('company.commission.subrogation.type')

        field_amount = 'amount'
        field_type = 'type_'
        result = defaultdict(lambda: [])
        for advance in advances:
            items = defaultdict(lambda: 0)
            fields = get_fields_values(
                CommSubDetail, comms_subs_details[advance.id], [
                    field_amount, field_type])
            for field in fields:
                name_type = get_single_field_value(
                    CommSubType, field[field_type], 'name')
                amount = field[field_amount]
                if name_type not in items:
                    items[name_type] = amount
                else:
                    items[name_type] += amount
            result[advance.id] = items
        return result

    @classmethod
    def get_loans_amounts_by_type(cls, advances, loans):
        pool = Pool()
        LoanLine = pool.get('company.employee.loan.line')
        LoanType = pool.get('company.employee.loan.type')

        field_amount = 'fee_residue'
        field_type = 'type'
        result = defaultdict(lambda: [])
        for advance in advances:
            items = defaultdict(lambda: 0)
            fields = get_fields_values(
                LoanLine, loans[advance.id], [field_amount, field_type])
            for field in fields:
                name_type = get_single_field_value(
                    LoanType, field[field_type], 'name')
                amount = field[field_amount]
                if name_type not in items:
                    items[name_type] = amount
                else:
                    items[name_type] += amount
            result[advance.id] = items
        return result

    @classmethod
    def manage_payslip_comm_subs_details(cls, advance, comm_subs_details):
        pool = Pool()
        PayslipAdvanceCommSubDetail = pool.get(
            'payslip.advance.commission.subrogation.detail')

        # Create or update considered payslips comm subs details
        to_save = []
        for detail in comm_subs_details:
            payslip_advance_comm_subs_details = (
                PayslipAdvanceCommSubDetail.search([
                    ('comm_sub_detail', '=', detail)
                ]))
            if payslip_advance_comm_subs_details:
                payslip_advance_comm_subs_details[0].payslip_advance = advance
                payslip_advance_comm_subs_details[0].was_considered = True
                to_save.append(payslip_advance_comm_subs_details[0])
            else:
                payslip_advance_comm_subs_detail = PayslipAdvanceCommSubDetail()
                payslip_advance_comm_subs_detail.payslip_advance = advance
                payslip_advance_comm_subs_detail.comm_sub_detail = detail
                payslip_advance_comm_subs_detail.was_considered = True
                to_save.append(payslip_advance_comm_subs_detail)

        # Set payslip comm sub details to not considered
        payslip_advance_comm_subs_details = PayslipAdvanceCommSubDetail.search([
            ('payslip_advance', '=', advance)
        ])
        for pa_comm_subs_detail in payslip_advance_comm_subs_details:
            if pa_comm_subs_detail not in to_save:
                pa_comm_subs_detail.was_considered = False
                to_save.append(pa_comm_subs_detail)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_advance_loan_lines(cls, advance, loans):
        pool = Pool()
        PayslipAdvanceLoanLine = pool.get('payslip.advance.loan.line')

        # Create or update considered payslip advances loan lines
        to_save = []
        for loan in loans:
            payslip_advance_loan_lines = PayslipAdvanceLoanLine.search([
                ('loan_line', '=', loan)
            ])
            if payslip_advance_loan_lines:
                payslip_advance_loan_lines[0].payslip = advance
                payslip_advance_loan_lines[0].was_considered = True
                to_save.append(payslip_advance_loan_lines[0])
            else:
                payslip_advance_loan_line = PayslipAdvanceLoanLine()
                payslip_advance_loan_line.payslip_advance = advance
                payslip_advance_loan_line.loan_line = loan
                payslip_advance_loan_line.was_considered = True
                to_save.append(payslip_advance_loan_line)

        # Set payslip advance loan lines to not considered
        payslip_advance_loan_lines = PayslipAdvanceLoanLine.search([
            ('payslip_advance', '=', advance)
        ])
        for payslip_advance_loan_line in payslip_advance_loan_lines:
            if payslip_advance_loan_line not in to_save:
                payslip_advance_loan_line.was_considered = False
                to_save.append(payslip_advance_loan_line)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_advance_manual_entries(cls, advance, income_entries,
            deduction_entries, considered_entries_codes):
        pool = Pool()
        PayslipAdvanceManualEntry = pool.get('payslip.advance.manual.entry')
        Entry = pool.get('payslip.entry')

        list_entries = []
        if income_entries:
            list_entries += income_entries
        if deduction_entries:
            list_entries += deduction_entries

        # Create or update considered payslip advances manual entries
        to_save = []
        for entry in Entry.browse(list_entries):
            if entry.type_.code in considered_entries_codes:
                payslip_advance_manual_entries = (
                    PayslipAdvanceManualEntry.search([
                        ('entry', '=', entry)
                    ]))
                if payslip_advance_manual_entries:
                    payslip_advance_manual_entries[0].payslip = advance
                    payslip_advance_manual_entries[0].was_considered = True
                    to_save.append(payslip_advance_manual_entries[0])
                else:
                    payslip_advance_manual_entries = PayslipAdvanceManualEntry()
                    payslip_advance_manual_entries.payslip_advance = advance
                    payslip_advance_manual_entries.entry = entry
                    payslip_advance_manual_entries.kind = entry.kind
                    payslip_advance_manual_entries.was_considered = True
                    to_save.append(payslip_advance_manual_entries)

        # Set payslip deduction entries to not considered
        payslip_advance_manual_entries = PayslipAdvanceManualEntry.search([
            ('payslip_advance', '=', advance)
        ])
        for payslip_advance_manual_entry in payslip_advance_manual_entries:
            if payslip_advance_manual_entry not in to_save:
                payslip_advance_manual_entry.was_considered = False
                to_save.append(payslip_advance_manual_entry)
        return {
            'to_save': to_save
        }

    @classmethod
    def get_new_payslip_advance_line(cls, advance, rule, type_, amount):
        PayslipAdvanceLine = Pool().get('payslip.advance.line')
        pline = PayslipAdvanceLine()
        pline.payslip_advance = advance
        pline.rule = rule
        pline.type_ = type_
        pline.amount = utils.quantize_currency(amount)
        return pline

    @classmethod
    def get_manual_revision_color(cls, advances, name=None):
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        result = {}

        ids = [a.id for a in advances]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.select(
                table.id,
                Case(
                    (table.manual_revision == 't', 'lightcoral'),
                    else_='lightgreen'),
                where=red_sql,
            )
            cursor.execute(*query)
            result.update(dict(cursor.fetchall()))
        return result

    def get_rec_name(self, name):
        return 'ANTICIPO DE ROL %s: %s - %s' % (
            self.period.rec_name, self.template.rec_name,
            self.employee.party.name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('employee',) + tuple(clause[1:]),
            ('contract',) + tuple(clause[1:]),
            ('period',) + tuple(clause[1:]),
            ('template',) + tuple(clause[1:]),
            ]
        return domain


class PayslipAdvanceLine(sequence_ordered(), ModelSQL, ModelView):
    'Payslip Advance Line'
    __name__ = 'payslip.advance.line'

    payslip_advance = fields.Many2One('payslip.advance', 'Anticipo de rol',
        required=True, ondelete='CASCADE',
        states={
            'readonly': True
        })
    rule = fields.Many2One('payslip.rule', 'Regla', required=True,
        domain=[
            ('company', '=', Eval(
                '_parent_payslip_advance', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        })
    type_ = fields.Selection(_PAYSLIP_ADVANCE_ITEM_TYPES, 'Tipo',
        required=True, sort=False,
        states={
            'readonly': True
        })
    type_translated = type_.translated('type_')
    amount = fields.Numeric('Valor', required=True, digits=(16, 2),
        states={
            'readonly': True
        })

    @classmethod
    def __setup__(cls):
        super(PayslipAdvanceLine, cls).__setup__()

    @staticmethod
    def default_amount():
        return ZERO


class PayslipAdvanceCommSubDetail(ModelSQL, ModelView):
    'Intermediate class between PayslipAdvance and CommSubDetail'
    __name__ = 'payslip.advance.commission.subrogation.detail'

    payslip_advance = fields.Many2One('payslip.advance',
        'Anticipo de rol de pagos', required=True, ondelete='CASCADE')
    comm_sub_detail = fields.Many2One('company.commission.subrogation.detail',
        'Línea de préstamo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipAdvanceLoanLine(ModelSQL, ModelView):
    'Intermediate class between PayslipAdvance and EmployeeLoanLine'
    __name__ = 'payslip.advance.loan.line'

    payslip_advance = fields.Many2One('payslip.advance',
        'Anticipo de rol de pagos', required=True, ondelete='CASCADE')
    loan_line = fields.Many2One('company.employee.loan.line',
        'Línea de préstamo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipAdvanceManualEntry(ModelSQL, ModelView):
    'Intermediate class between PayslipAdvance and Manual Entries'
    __name__ = 'payslip.advance.manual.entry'

    payslip_advance = fields.Many2One('payslip.advance', 'Aticipo de rol',
        required=True, ondelete='CASCADE')
    entry = fields.Many2One('payslip.entry', 'Entrada manual', required=True)
    kind = fields.Selection(_KIND, 'Tipo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True
