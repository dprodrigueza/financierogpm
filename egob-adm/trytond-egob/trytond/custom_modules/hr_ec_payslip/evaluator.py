from asteval import Interpreter, make_symbol_table

__all__ = ['RuleParser']


class RuleParser(object):
    'Rule Parser and Evaluator using asteval'

    def __init__(self, symbols={}):
        syms = make_symbol_table(use_numpy=True, **symbols)
        self.aeval = Interpreter(symtable=syms)
        self.error = None

    def eval_expr(self, expr):
        v = self.aeval(expr)
        self.error = None
        if self.aeval.error:
            self.error = self.aeval.error[0].get_error()[1]
        return v
