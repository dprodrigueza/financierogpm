import datetime

from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.model import ModelView, fields, ModelSQL, ModelSingleton
from trytond.pyson import Eval, Bool, If
from trytond.pool import Pool
from trytond.transaction import Transaction

from calendar import different_locale, month_name, monthrange
from dateutil.relativedelta import relativedelta


__all__ = ['LaborMinistry', 'LaborMinistryReportWizardStart',
    'LaborMinistryXIVReportWizardStart', 'LaborMinistryXIV',
    'LaborMinistryXIIIReportWizardStart', 'LaborMinistryXIII',
    'ConfigurationLaborMinistry', 'LaborMinistryRule']


class LaborMinistryReportWizardStart(ModelView):
    ' Labor Ministry Wizard Start'
    __name__ = 'labor.ministry.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    period = fields.Many2One('account.period', 'Mes Actual',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'], required=True)
    start_date = fields.Date('Fecha inicio',
        states={
            'readonly': True,
        },
        domain=[
            ('start_date', '<=', Eval('end_date'))
        ], depends=['end_date'], required=True)
    end_date = fields.Date('Fecha fin',
        states={
            'readonly': True,
        },
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date'], required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.date.today()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None


class LaborMinistry(Wizard):
    ' Labor Ministry Wizard'
    __name__ = 'labor.ministry.report.wizard'

    start = StateView('labor.ministry.report.wizard.start',
        'hr_ec_payslip.wizard_labor_ministry_report_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export', 'tryton-executable'),
        ])
    export = StateReport('payslip.labor.export')

    @classmethod
    def __setup__(cls):
        super(LaborMinistry, cls).__setup__()
        cls._error_messages.update({
            'no_remunerations': 'No hay registros de remuneracion mensuales '
                                'para el periodo "%(period)s".'
        })

    def do_export(self, action):
        pool = Pool()
        ConfLaborMinistry = pool.get('configuration.labor.ministry')
        conf_labor_ministry = ConfLaborMinistry.search([()])
        rules_codes = []

        for conf in conf_labor_ministry:
            if conf.name == "labor.ministry":
                for rule in conf.codes_rules:
                    rules_codes.append(rule.code)
        data = {
            'company': self.start.company.id,
            'fiscalyear': self.start.fiscalyear.id,
            'payslips': [x.id for x in self.get_payslip_by_code(rules_codes)],
            'rules': rules_codes
        }
        return action, data

    def get_payslip_by_code(self, rules_codes):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        payslips = Payslip.search([
            ('fiscalyear', '=', self.start.fiscalyear.id),
            ('period', '=', self.start.period.id),
            ('general.template.code', 'in', rules_codes),
        ], order=[('employee.party.name', 'ASC')])
        return payslips


class LaborMinistryXIVReportWizardStart(ModelView):
    ' Labor Ministry XIV Wizard Start'
    __name__ = 'labor.ministry_xiv.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], states={
            'readonly': True
        })
    start_date = fields.Date('Fecha inicio', required=True,
        domain=[
            ('start_date', '=<', Eval('end_date'))
        ], depends=['end_date'])
    end_date = fields.Date('Fecha fin', required=True,
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date'])
    retention_rules = fields.One2Many('payslip.rule', None,
        'Reglas para el campo "Total Retenido"')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('start_date')
    def on_change_with_end_date(self, name=None):
        if self.start_date:
            end_date = self.start_date + relativedelta(months=11)
            year = end_date.year
            month = end_date.month
            last_day = monthrange(year, month)[1]
            return datetime.date(year=year, month=month, day=last_day)
        return None


class LaborMinistryXIV(Wizard):
    ' Labor Ministry XIV Wizard'
    __name__ = 'labor.ministry_xiv.report.wizard'

    start = StateView('labor.ministry_xiv.report.wizard.start',
        'hr_ec_payslip.wizard_labor_ministry_xiv_report_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export', 'tryton-executable'),
        ])
    export = StateReport('payslip.labor_xiv.export')

    @classmethod
    def __setup__(cls):
        super(LaborMinistryXIV, cls).__setup__()

    def do_export(self, action):
        retention_rules_ids = [rule.id for rule in self.start.retention_rules]
        data = {
            'company_id': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'retention_rules_ids': retention_rules_ids
        }
        return action, data


class LaborMinistryXIIIReportWizardStart(ModelView):
    ' Labor Ministry XIII Wizard Start'
    __name__ = 'labor.ministry_xiii.report.wizard.start'

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], states={
            'readonly': True
        })
    start_date = fields.Date('Fecha inicio', required=True,
        domain=[
            ('start_date', '=<', Eval('end_date'))
        ], depends=['end_date'])
    end_date = fields.Date('Fecha fin', required=True,
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], depends=['start_date'])
    retention_rules = fields.One2Many('payslip.rule', None,
        'Reglas para el campo "Total Retenido"')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_start_date():
        now = datetime.date.today()
        year = (now.year - 1) if (now.month == 12) else (now.year - 2)
        return datetime.date(year, 12, 1)

    @fields.depends('start_date')
    def on_change_with_end_date(self, name=None):
        if self.start_date:
            end_date = self.start_date + relativedelta(months=11)
            year = end_date.year
            month = end_date.month
            last_day = monthrange(year, month)[1]
            return datetime.date(year=year, month=month, day=last_day)
        return None


class LaborMinistryXIII(Wizard):
    ' Labor Ministry XIII Wizard'
    __name__ = 'labor.ministry_xiii.report.wizard'

    start = StateView('labor.ministry_xiii.report.wizard.start',
        'hr_ec_payslip.wizard_labor_ministry_xiii_report_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export', 'tryton-executable'),
        ])
    export = StateReport('payslip.labor_xiii.export')

    @classmethod
    def __setup__(cls):
        super(LaborMinistryXIII, cls).__setup__()

    def do_export(self, action):
        retention_rules_ids = [rule.id for rule in self.start.retention_rules]
        data = {
            'company_id': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'retention_rules_ids': retention_rules_ids
        }
        return action, data


class ConfigurationLaborMinistry(ModelSQL, ModelView):
    'Configuration Labor Ministry'
    __name__ = 'configuration.labor.ministry'
    _history = True

    name = fields.Char('Nombre codigo', required=True)
    codes_rules = fields.Many2Many('labor.ministry.rule',
            'labor_ministry', 'rule', 'Reglas', required=True)
    codes_rules_info = fields.Function(fields.Char('Codigos Reglas'),
        'on_change_with_codes_rules_info')

    @fields.depends('codes_rules')
    def on_change_with_codes_rules_info(self, name=None):
        text = ""
        if self.codes_rules:
            for line in self.codes_rules:
                if line.code:
                    text = (
                        f"{text} {line.code}, ")
            if len(text) > 0:
                text = text[:-2]
            return text
        return None


class LaborMinistryRule(ModelSQL):
    'Labor Ministry Rule'
    __name__ = 'labor.ministry.rule'

    labor_ministry = fields.Many2One(
        'configuration.labor.ministry', 'Ministerio',
        ondelete='CASCADE', required=True, select=True)

    rule = fields.Many2One('payslip.template', 'Codigo: ',
        ondelete='RESTRICT', required=True, select=True)