from io import BytesIO
from trytond.model import ModelView, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, Button, StateTransition, StateView)
from trytond.tools import file_open, cursor_dict
from trytond.pyson import Eval
from functools import reduce
from sql import Null

from collections import defaultdict

from decimal import Decimal
from datetime import date, datetime, timedelta, time
from dateutil.relativedelta import relativedelta
from calendar import monthrange, isleap
import pendulum
import pandas as pd
import numpy as np

from . import evaluator

from trytond.modules.hr_ec_payslip import utils


__all__ = ['UploadPayslipRulesStart', 'UploadPayslipRules',
    'UploadPayslipStart', 'UploadPayslip',
    'MedicalCertificatesDetailForMigratedPaylipsStart',
    'MedicalCertificatesDetailForMigratedPaylipsCompleted',
    'MedicalCertificatesDetailForMigratedPaylips',
    'AccumulatedLawBenefitsForMigratedPaylipsStart',
    'AccumulatedLawBenefitsForMigratedPaylipsCompleted',
    'AccumulatedLawBenefitsForMigratedPaylips',
    'AssignPayslipTemplatesToContracts',
    'AssignPayslipTemplatesToContractsStart',
    'AssignPayslipTemplatesToContractsCompleted'
]


CENT = Decimal('0.01')

# PARAMS
CODES_TO_EVALUATE = ['xiv']
DATE_FROM = None
DATE_TO = datetime.strptime('31-07-2019', '%d-%m-%Y').date()


def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        value = row[i]
        if isinstance(value, str):
            value = value.strip()
        _dict.update({field_name: value})
        i += 1
    return _dict


def replace_blanks_by_none(_dict):
    for key in _dict.keys():
        if not validateBlankField(_dict[key]):
            _dict[key] = None


def validateBlankField(item):
    if ((item != None) and
            (str(item).lower() != 'nan') and
                (str(item).lower() != 'none')):
        return True
    else:
        return False


class UploadPayslipRulesStart(ModelView):
    'Upload Payslip Rules Start'
    __name__ = 'payslip.rule.migration.start'

    source_file = fields.Binary('Archivo a cargar', required=True)
    template = fields.Binary('Plantilla de archivo',
        filename='template_filename', file_id='template_path', readonly=True)
    template_filename = fields.Char('PlantillaDeReglasDeRol',
        states={
            'invisible': True,
        })
    template_path = fields.Char('Ruta de ubicación', readonly=True)

    @staticmethod
    def default_template_path():
        return './data/plantilla_reglas_rol.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('plantilla_reglas_rol', 'xlsx')

    @staticmethod
    def default_template():
        path = 'hr_ec_payslip/data/plantilla_reglas_rol.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class UploadPayslipRules(Wizard):
    'Upload Payslip Rule Start'
    __name__ = 'payslip.rule.migration'

    start = StateView('payslip.rule.migration.start',
        'hr_ec_payslip.upload_payslip_rules_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UploadPayslipRules, cls).__setup__()
        cls._error_messages.update({
            'empty_data': ('Uno de los campos (NOMBRE, CODIGO, TIPO) en la '
            'fila: %(row)s está vació en el archivo'),
            'format_error': ('La linea %(field)s ,\
                    tiene un problema de formato en uno de los campos.'),
            'index_error': ('No se ha encontrado registros con los datos \
                    de la línea %(field)s'),
            })

    def transition_import_(self):
        pool = Pool()
        PayslipRule = pool.get('payslip.rule')

        field_names = ['NOMBRE', 'CODIGO', 'TIPO', 'BENEFICIO DE LEY',
                       'ENTRADA EXCEL', 'INGRESO DEDUCCION FIJO', 'AFECTA XIII',
                       'AFECTA XIV', 'AFECTA FR', 'AFECTA IR',
                       'AFECTA APORTE PERSONAL', 'AFECTA APORTE PATRONAL']
        file_data = fields.Binary.cast(self.start.source_file)
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        df = df.fillna(False)

        to_save_rules = []

        for cont, row in enumerate(df.itertuples()):
            items = get_dict_from_readed_row(field_names, row)
            if str(items['NOMBRE']) == 'False' or (
                   str(items['CODIGO']) == 'False' or (
                       str(items['TIPO']) == 'False')):
                self.raise_user_error('empty_data', {'row': (cont + 2)})
            try:
                rules = PayslipRule.search([
                    ('code', '=', items['CODIGO'])
                ])
                rule = rules[0] if rules else None

                if rule:
                    # Actualizar información de reglas
                    rule = self.get_rule_update(rule, items)
                    to_save_rules.append(rule)

                else:
                    rule = self.get_rule_object(items)
                    to_save_rules.append(rule)
            except ValueError:
                self.raise_user_error('format_error', {
                    'field': row[0]
                    })
            except IndexError:
                self.raise_user_error('index_error', {
                    'field': row[0]
                    })
        PayslipRule.save(to_save_rules)
        return 'end'

    def get_rule_object(self, items):
        pool = Pool()
        Company = pool.get('company.company')
        PayslipRule = pool.get('payslip.rule')

        name = items['NOMBRE']
        code = items['CODIGO']
        type = items['TIPO']

        payslip_position = None
        if type == 'ingreso':
            type = 'income'
            payslip_position = 'left'
        elif type == 'deduccion':
            type = 'deduction'
            payslip_position = 'right'
        elif type == 'interno':
            type = 'internal'
            payslip_position = 'general'
        else:
            type = None

        is_law_benefit = False
        is_manual_entry = False
        is_income_deduction = False

        law_benefit_type = None
        entry_type = None
        fixed_income_deduction_type = None

        expression = self.get_expression_general(code, type, None)

        if str(items['BENEFICIO DE LEY']).lower() == 'x':
            is_law_benefit = True
            is_manual_entry = False
            is_income_deduction = False
            law_benefit_type = self.get_law_benefit_type_object(code, name)
            expression = self.get_expression_general(code, type, 'law_benefit')
        elif str(items['ENTRADA EXCEL']).lower() == 'x':
            is_law_benefit = False
            is_manual_entry = True
            is_income_deduction = False
            entry_type = self.get_entry_type_object(code, name, type)
            expression = self.get_expression_general(code, type, 'manual_entry')
        elif str(items['INGRESO DEDUCCION FIJO']).lower() == 'x':
            is_law_benefit = False
            is_manual_entry = False
            is_income_deduction = True
            fixed_income_deduction_type = self.get_income_deduction_type_object(
                code, name, type)
            expression = self.get_expression_general(code, type,
                                                     'income_deducction')

        condition = self.get_condition_general()

        rule = PayslipRule()
        rule.company = Company.browse([Transaction().context.get('company')])[0]
        rule.active = True
        rule.code = code
        rule.name = name
        rule.type_ = type
        rule.payslip_position = payslip_position
        rule.condition = condition
        rule.expression = expression
        rule.is_law_benefit = is_law_benefit
        rule.is_manual_entry = is_manual_entry
        rule.is_fixed_income_deduction = is_income_deduction
        rule.entry_type = entry_type
        rule.fixed_income_deduction_type = fixed_income_deduction_type
        rule.law_benefit_type = law_benefit_type
        rule.dependencies = []

        return rule

    def get_rule_update(self, rule, items):

        name = items['NOMBRE']
        code = items['CODIGO']
        type = items['TIPO']

        rule.name = name
        rule.code = code
        type = type

        payslip_position = None
        if type == 'ingreso':
            type = 'income'
            payslip_position = 'left'
        elif type == 'deduccion':
            type = 'deduction'
            payslip_position = 'right'
        elif type == 'interno':
            type = 'internal'
            payslip_position = 'general'
        else:
            type = None

        condition = self.get_condition_general()
        expression = self.get_expression_general(code, type, None)

        rule.type_ = type
        rule.payslip_position = payslip_position
        rule.condition = condition

        is_law_benefit = False
        is_manual_entry = False
        is_income_deduction = False

        if str(items['BENEFICIO DE LEY']).lower() == 'x':
            is_law_benefit = True
            is_manual_entry = False
            is_income_deduction = False
            law_benefit_type = self.get_law_benefit_type_object(code, name)
            rule.law_benefit_type = law_benefit_type
            expression = self.get_expression_general(code, type, 'law_benefit')
        elif str(items['ENTRADA EXCEL']).lower() == 'x':
            is_law_benefit = False
            is_manual_entry = True
            is_income_deduction = False
            entry_type = self.get_entry_type_object(code, name, type)
            rule.entry_type = entry_type
            expression = self.get_expression_general(code, type, 'manual_entry')
        elif str(items['INGRESO DEDUCCION FIJO']).lower() == 'x':
            is_law_benefit = False
            is_manual_entry = False
            is_income_deduction = True
            fixed_income_deduction_type = self.get_income_deduction_type_object(
                code, name, type)
            rule.fixed_income_deduction_type = fixed_income_deduction_type
            expression = self.get_expression_general(code, type,
                                                     'income_deducction')

        rule.expression = expression
        rule.is_law_benefit = is_law_benefit
        rule.is_manual_entry = is_manual_entry
        rule.is_fixed_income_deduction = is_income_deduction

        return rule

    def get_law_benefit_type_object(self, code, name):

        PayslipLawBenefit = Pool().get('payslip.law.benefit.type')

        law_benefit_types = PayslipLawBenefit.search([('code', '=', code)])
        if law_benefit_types:
            law_benefit_type = law_benefit_types[0]
        else:
            law_benefit_type = PayslipLawBenefit()
            law_benefit_type.code = code
            law_benefit_type.name = name
            law_benefit_type.expression = 'result = 0 \nresult'

        return law_benefit_type

    def get_entry_type_object(self, code, name, type):

        PayslipEntryType = Pool().get('payslip.entry.type')

        entries_types = PayslipEntryType.search([('code', '=', code)])
        if entries_types:
            entry_type = entries_types[0]
        else:
            entry_type = PayslipEntryType()
            entry_type.name = name
            entry_type.code = code
            entry_type.kind = type

        return entry_type

    def get_income_deduction_type_object(self, code, name, kind):
        pool = Pool()
        TypeIncomeDeduction = pool.get('company.contract.type.income.deduction')

        income_deduction_types = TypeIncomeDeduction.search([
            ('code', '=', code)])
        if income_deduction_types:
            income_deduction_type = income_deduction_types[0]
        else:
            income_deduction_type = TypeIncomeDeduction()
            income_deduction_type.description = name
            income_deduction_type.code = code
            income_deduction_type.kind = kind

        return income_deduction_type

    def get_condition_general(self):
        expression = (
            f"True"
        )
        return expression

    def get_expression_general(self, code, type, kind):

        expression = None

        if kind == 'law_benefit':
            expression = (
                f"result = {'{'}\n"
                f"    'value': law_benefit_types['{code}']\n"
                f"{'}'}\n"
                f"result"
            )

        elif kind == 'income_deducction':

            if type == 'income':
                expression = (
                    f"result = {'{'}\n"
                    f"    'value': fixed_income_amounts['{code}']\n"
                    f"{'}'}\n"
                    f"result"
                )
            elif type == 'deduction':
                expression = (
                    f"result = {'{'}\n"
                    f"    'value': fixed_deduction_amounts['{code}']\n"
                    f"{'}'}\n"
                    f"result"
                )

        elif kind == 'manual_entry':
            expression = (
                f"result = {'{'}\n"
                f"    'value': manual_entry_amounts['{code}']\n"
                f"{'}'}\n"
                f"result"
            )

        else:
            expression = (
                f"result = {'{'}\n"
                f"    'value': 0\n"
                f"{'}'}\n"
                f"result"
            )
        return expression


class UploadPayslipStart(ModelView):
    'Upload Payslip Start'
    __name__ = 'payslip.migration.start'

    source_file_header = fields.Binary('Archivo cabecera a cargar',
        required=True)
    source_file_detail = fields.Binary('Archivo detalle a cargar',
        required=True)
    template_header = fields.Binary('Plantilla de archivo cabecera',
        filename='template_header_filename', file_id='template_header_path',
        readonly=True)
    template_header_filename = fields.Char('PlantillaDeCabeceraDeRol',
        states={
            'invisible': True,
        })
    template_header_path = fields.Char('Ubicación de archivo cabecera',
        readonly=True)
    template_detail = fields.Binary('Plantilla de archivo detalle',
        filename='template_detail_filename', file_id='template_detail_path',
        readonly=True)
    template_detail_filename = fields.Char('PlantillaDeDetalleDeRol',
        states={
            'invisible': True,
        })
    template_detail_path = fields.Char('Ubicación de archivo detalle',
        readonly=True)

    @staticmethod
    def default_template_header_path():
        return './data/plantilla_roles_pagos_cabecera.xlsx'

    @staticmethod
    def default_template_header_filename():
        return "%s.%s" % ('plantilla_roles_pagos_cabecera', 'xlsx')

    @staticmethod
    def default_template_header():
        path = 'hr_ec_payslip/data/plantilla_roles_pagos_cabecera.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @staticmethod
    def default_template_detail_path():
        return './data/plantilla_roles_pagos_detalle.xlsx'

    @staticmethod
    def default_template_detail_filename():
        return "%s.%s" % ('plantilla_roles_pagos_detalle', 'xlsx')

    @staticmethod
    def default_template_detail():
        path = 'hr_ec_payslip/data/plantilla_roles_pagos_detalle.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class UploadPayslip(Wizard):
    'Upload Payslip'
    __name__ = 'payslip.migration'

    start = StateView('payslip.migration.start',
        'hr_ec_payslip.upload_payslip_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UploadPayslip, cls).__setup__()
        cls._error_messages.update({
            'no_rule': ('La regla: %(rule)s no fué encontrada para la línea ('
                'Plantilla: %(template)s, Cédula: %(employee)s, '
                'Periodo: %(period)s)'),
            'no_template': ('No se ha encontrado en el sistema la plantilla '
                '%(template)s. Revise el valor del campo PLANTILLA en la fila #'
                ' %(cont)s.'),
            'no_employee': ('No se ha encontrado el empleado %(employee)s en el'
                ' sistema. , Revise el valor del campo CEDULA en la fila # '
                '%(cont)s.'),
            'no_contract': ('No se ha encontrado el contrato del empleado '
                '%(employee)s en el sistema. , Revise el valor del campo '
                'CEDULA en la fila # %(cont)s.'),
            'no_period': ("No se ha encontrado en el sistema un período con "
                "nombre '%(period)s'. Revise el valor del campo PERIODO en la "
                "fila #%(cont)s)."),
            'empty_data': ('Uno de los campos en la fila # %(cont)s está vació '
                           'en el archivo.'),
            })

    def transition_import_(self):
        pool = Pool()
        PayslipGeneral = pool.get('payslip.general')
        PayslipPayslip = pool.get('payslip.payslip')
        PayslipRule = pool.get('payslip.rule')
        PayslipLine = pool.get('payslip.line')
        Period = pool.get('account.period')
        PayslipTemplate = pool.get('payslip.template')
        Employee = pool.get('company.employee')
        PayslipLawBenefit = pool.get('payslip.law.benefit')
        Contract = pool.get('company.contract')

        context = Transaction().context

        field_names_header = ['PLANTILLA', 'CEDULA', 'APELLIDOS Y NOMBRES',
            'PERIODO', 'FECHA INICIO CORTE', 'FECHA FIN CORTE', 'SUELDO',
            'DIAS TRABAJADOS', 'FALTAS', 'DIAS ENFERMEDAD ACCIDENTE',
            'TOTAL INGRESOS', 'TOTAL DEDUCCIONES', 'TOTAL  PAGAR',
            'MENSUALIZA XIII', 'MENSUALIZA XIV', 'MENSUALIZA FR']

        file_data_header = fields.Binary.cast(self.start.source_file_header)
        data_header = pd.read_excel(
            BytesIO(file_data_header), names=field_names_header, dtype='object',
            header=0)
        data_header = data_header.fillna(False)

        list_payslips = []
        list_general_payslips = []
        dic_headers = {}
        dic_generals = {}
        ignore_id = []
        for cont, row in enumerate(data_header.itertuples()):

            self.validate_empty_data_header(cont, row)

            if(row[2] in ignore_id):
                continue

            employee = self.get_employee(cont, row, Employee, ignore_id)
            period = self.get_period(cont, row, Period)
            template = self.get_template(cont, row, PayslipTemplate)
            general = self.get_general_payslip(row, context['company'], period,
                template, PayslipGeneral, list_general_payslips, dic_generals)
            contracts = Contract.search([
                ('employee', '=', employee),
                ('end_date', '>', row[5]),
            ])
            if not contracts:
                contracts = Contract.search([
                ('employee', '=', employee),
                ])

            if not contracts:
                self.raise_user_warning('no_contract_%d' % cont, 'no_contract', {
                        'template': row[1],
                        'employee': row[3],
                        'period': row[4],
                        'cont': (cont + 2)})

            if employee and contracts:
                contract = contracts[0]

                payslip = PayslipPayslip()
                payslip.company = context['company']
                payslip.general = general
                payslip.template = self.get_template(cont, row, PayslipTemplate)
                payslip.employee = employee
                payslip.contract = contract
                payslip.period = period
                payslip.fiscalyear = period.fiscalyear
                payslip.start_date = (
                    datetime.strptime(row[5], '%Y-%m-%d').date())
                payslip.end_date = (
                    datetime.strptime(row[6], '%Y-%m-%d').date())
                payslip.state = 'done'
                payslip.period_days = 30
                payslip.worked_period_days = 30
                payslip.worked_days = row[8]
                payslip.worked_days_with_normative = row[8]
                payslip.total_absences = row[9]
                payslip.total_absences_with_normative = row[9]
                payslip.total_permissions = 0
                payslip.total_days_disease = row[10]
                payslip.total_income = Decimal(str(row[11])).quantize(CENT)
                payslip.total_deduction = Decimal(str(row[12])).quantize(CENT)
                payslip.total_value = Decimal(str(row[13])).quantize(CENT)
                payslip.manual_revision = False
                payslip.min_salary_to_receive = 0
                # payslip.payslip_manual_entries = None
                payslip.xiii = self.get_monthlyse_accumulate(row[14])
                payslip.xiv = self.get_monthlyse_accumulate(row[15])
                payslip.fr = self.get_monthlyse_accumulate(row[16])
                # payslip.dict_codes
                # payslip.dict_codes_template
                payslip.salary = Decimal(str(row[7])).quantize(CENT)
                payslip.is_migrated = True
                payslip.department = contract.department
                payslip.position = contract.position
                payslip.work_relationship = contract.work_relationship
                payslip.contract_type = contract.contract_type
                payslip.done_date = datetime.today()

                list_payslips.append(payslip)

                if not dic_headers.get(f"{row[1]}-{row[2]}-{row[4]}"):
                    dic_headers[f"{row[1]}-{row[2]}-{row[4]}"] = payslip

        field_names_detail = ['PLANTILLA', 'CEDULA', 'APELLIDOS Y NOMBRES',
                              'PERIODO', 'CODIGO REGLA', 'VALOR']

        file_data_detail = fields.Binary.cast(self.start.source_file_detail)
        data_detail = pd.read_excel(
            BytesIO(file_data_detail), names=field_names_detail, dtype='object',
            header=0)
        data_detail = data_detail.fillna(False)

        list_payslip_lines = []
        list_law_benefits = []
        dict_lines = {}
        for cont, row in enumerate(data_detail.itertuples()):
            self.validate_empty_data_detail(cont, row)
            payslip = self.get_payslip(cont, row, dic_headers)
            rule = self.get_rule(cont, row, PayslipRule)

            if payslip:
                if rule.is_law_benefit:
                    self.create_law_benefit(rule, row, payslip,
                        PayslipLawBenefit, list_law_benefits)

                if not dict_lines.get(f"{row[1]}-{row[2]}-{row[4]}-{row[5]}"):
                    line = PayslipLine()
                    line.payslip = payslip
                    line.rule = rule
                    if rule.code == 'fr' and payslip.fr == 'accumulate':
                        line.type_ = 'internal'
                    else:
                        line.type_ = rule.type_

                    line.amount = Decimal(str(row[6])).quantize(CENT)
                    if (rule.code == 'xiii' and payslip.xiii == 'accumulate'
                    ) or (rule.code == 'xiv' and payslip.xiv == 'accumulate'):
                        pass
                    else:
                        list_payslip_lines.append(line)
                    dict_lines[f"{row[1]}-{row[2]}-{row[4]}-{row[5]}"] = line

                else:
                    line = dict_lines.get(
                        f"{row[1]}-{row[2]}-{row[4]}-{row[5]}")
                    amount = line.amount + Decimal(str(row[6])).quantize(CENT)
                    line.amount = amount

        PayslipGeneral.save(list_general_payslips)
        PayslipPayslip.save(list_payslips)
        PayslipLawBenefit.save(list_law_benefits)
        PayslipLine.save(list_payslip_lines)

        return 'end'

    def validate_empty_data_header(self, cont, row):
        for i in range(len(row)):
            if row[i] is None or str(row[i]) == 'False':
                self.raise_user_warning('empty_data_header_%d' % cont,
                        'empty_data', {'cont': (cont + 2)})

    def validate_empty_data_detail(self, cont, row):
        for i in range(len(row)):
            if row[i] is None or str(row[i]) == 'False':
                self.raise_user_warning('empty_data_detail_%d' % cont,
                        'empty_data', {'cont': (cont + 2)})

    def get_payslip(self, cont, row, dic_headers):
        payslip = dic_headers.get(f"{row[1]}-{row[2]}-{row[4]}")
        return payslip

    def get_rule(self, cont, row, PayslipRule):
        rules = PayslipRule.search([('code', '=', row[5])])
        rule = None
        if rules:
            rule = rules[0]
        else:
            self.raise_user_warning('no_rule_%d' % cont, 'no_rule', {
                'rule': row[5],
                'template': row[1],
                'employee': row[3],
                'period': row[4]})

        return rule

    def create_law_benefit(self, rule, row, payslip, PayslipLawBenefit,
            list_law_benefits):
        law_benefit = PayslipLawBenefit()
        law_benefit.company = payslip.company
        law_benefit.period = payslip.period
        law_benefit.employee = payslip.employee
        law_benefit.payslip = payslip
        law_benefit.payslip_date = payslip.end_date
        law_benefit.amount = Decimal(str(row[6])).quantize(CENT)
        if rule.code == 'xiii':
            law_benefit.kind = payslip.xiii
        elif rule.code == 'xiv':
            law_benefit.kind = payslip.xiv
        elif rule.code == 'fr':
            law_benefit.kind = payslip.fr

        law_benefit.type_ = rule.law_benefit_type
        list_law_benefits.append(law_benefit)

    def get_template(self, cont, row, PayslipTemplate):
        templates = PayslipTemplate.search([('code', '=', row[1])])
        template = None
        if templates:
            template = templates[0]
        else:
            self.raise_user_warning('no_template_%d' % cont, 'no_template', {
                'template': row[1],
                'employee': row[3],
                'period': row[4],
                'cont': (cont + 2)})

        return template

    def get_employee(self, cont, row, Employee, ignore_id=[]):
        employees = Employee.search(['identifier', '=', row[2]])
        employee = None
        if employees:
            employee = employees[0]
        else:
            ignore_id.append(row[2])
            self.raise_user_warning('no_employee_%d' % cont, 'no_employee', {
                'template': row[1],
                'employee': row[3],
                'period': row[4],
                'cont': (cont + 2)})

        return employee

    def get_period(self, cont, row, Period):
        periods = Period.search(['name', '=', row[4]])
        period = None
        if periods:
            period = periods[0]
        else:
            self.raise_user_warning('no_period_%d' % cont, 'no_period', {
                'cont': (cont + 2),
                'period': row[4]
            })

        return period

    def get_monthlyse_accumulate(self, camp):
        if camp.lower() == 'si':
            result = 'monthlyse'
        else:
            result = 'accumulate'

        return result

    def get_general_payslip(self, row, company, period, template,
            PayslipGeneral, list_general_payslips, dic_generals):
        payslip_generals = PayslipGeneral.search([
            ('company', '=', company),
            ('template', '=', template),
            ('fiscalyear', '=', period.fiscalyear),
            ('period', '=', period),
        ])
        key_gen = f"{company}-{template}-{period.fiscalyear}-{period.rec_name}"

        if payslip_generals:
            payslip_general = payslip_generals[0]
        elif dic_generals.get(key_gen):
            payslip_general = dic_generals.get(key_gen)
        else:
            payslip_general = PayslipGeneral()
            payslip_general.company = company
            payslip_general.fiscalyear = period.fiscalyear
            payslip_general.period = period
            payslip_general.start_date = (
                datetime.strptime(row[5], '%Y-%m-%d').date())
            payslip_general.end_date = (
                datetime.strptime(row[6], '%Y-%m-%d').date())
            payslip_general.template = template
            payslip_general.state = 'done'
            dic_generals[key_gen] = payslip_general
            list_general_payslips.append(payslip_general)

        return payslip_general


class MedicalCertificatesDetailForMigratedPaylipsStart(ModelView):
    'Medical Certificates Detail To Migrated Paylips Start'
    __name__ = 'medical.certificate.detail.migrated.payslips.start'


class MedicalCertificatesDetailForMigratedPaylipsCompleted(ModelView):
    'Medical Certificates Detail To Migrated Paylips Completed'
    __name__ = 'medical.certificate.detail.migrated.payslips.completed'


class MedicalCertificatesDetailForMigratedPaylips(Wizard):
    'Medical Certificates Detail To Migrated Paylips'
    __name__ = 'medical.certificate.detail.migrated.payslips'

    start = StateView('medical.certificate.detail.migrated.payslips.start',
        'hr_ec_payslip.med_cert_detail_migrated_payslips_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Generar', 'generate_', 'tryton-executable'),
        ])
    completed = StateView(
        'medical.certificate.detail.migrated.payslips.completed',
        'hr_ec_payslip.med_cert_detail_migrated_payslips_completed_view_form',
        [
            Button('Ok', 'end', 'tryton-executable'),
        ])
    generate_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(MedicalCertificatesDetailForMigratedPaylips, cls).__setup__()

    def transition_generate_(self):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')

        to_save_managed_med_cert_dis = []
        to_delete_managed_med_cert_dis = []

        print('Buscando roles migrados en la base de datos...')
        payslips = Payslip.search([
            ('is_migrated', '=', True)
        ])
        print('Total de roles migrados: ' + str(len(payslips)))

        if payslips:
            print('Recuperando información de certificados médicos...')
            info_medical = Payslip.get_all_medical_certificates_info(payslips)
            medical_certificates_info = info_medical[
                'medical_certificates_info']

            for payslip in payslips:
                print('Asignando detalle a rol: ' + str(payslip.id) + ' ' +
                      payslip.period.rec_name + ' ' + payslip.rec_name)
                # Create payslip medical certificate discount lines
                managed_med_cert_disc = (
                    Payslip.manage_payslip_medical_certificate_discounts(
                        payslip, medical_certificates_info[payslip.id]))
                to_save_managed_med_cert_dis += (
                    managed_med_cert_disc['to_save'])
                to_delete_managed_med_cert_dis += (
                    managed_med_cert_disc['to_delete'])

            print('Detalle de certificados médicos a eliminar: ' +
                  str(len(to_delete_managed_med_cert_dis)))
            print('Detalle de certificados médicos a guardar: ' +
                  str(len(to_save_managed_med_cert_dis)))

            print('Eliminando detalle de certificados médicos...')
            PayslipMedicalCertificateDiscount.delete(
                to_delete_managed_med_cert_dis)
            print('Guardando detalle de certificados médicos...')
            PayslipMedicalCertificateDiscount.save(to_save_managed_med_cert_dis)

        print('Finalizado...')
        return 'completed'


class AccumulatedLawBenefitsForMigratedPaylipsStart(ModelView):
    'Accumulated Law Benefits For Migrated Paylips Start'
    __name__ = 'accumulated.law.benefits.migrated.payslips.start'

    date_from = fields.Date('Desde', required=True,
        domain=[
            ('date_from', '<=', Eval('date_to'))
        ], depends=['date_to'])
    date_to = fields.Date('Hasta', required=True,
        domain=[
            ('date_to', '>=', Eval('date_from'))
        ], depends=['date_from'])
    law_benefit_types = fields.One2Many('payslip.law.benefit.type', None,
        'Tipos de beneficios de ley',
        domain=[
            ('code', 'in', ['xiii', 'xiv', 'fr'])
        ],
        states={
            'required': True
        })

    @staticmethod
    def default_date_to():
        date_to = datetime.today().date()
        return date_to

    @staticmethod
    def default_date_from():
        date_from = datetime.today().date() - timedelta(days=365)
        return date_from

    @staticmethod
    def default_law_benefit_types():
        LawBenefitType = Pool().get('payslip.law.benefit.type')
        types = LawBenefitType.search([
            ('code', 'in', ['xiii', 'xiv', 'fr'])
        ])
        ids = [type.id for type in types]
        return ids


class AccumulatedLawBenefitsForMigratedPaylipsCompleted(ModelView):
    'Accumulated Law Benefits For Migrated Paylips Completed'
    __name__ = 'accumulated.law.benefits.migrated.payslips.completed'


class AccumulatedLawBenefitsForMigratedPaylips(Wizard):
    'Accumulated Law Benefits For Migrated Paylips'
    __name__ = 'accumulated.law.benefits.migrated.payslips'

    start = StateView('accumulated.law.benefits.migrated.payslips.start',
        'hr_ec_payslip.acc_law_benefits_migrated_payslips_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Generar', 'generate_', 'tryton-executable'),
        ])
    completed = StateView(
        'accumulated.law.benefits.migrated.payslips.completed',
        'hr_ec_payslip.acc_law_benefits_migrated_payslips_completed_view_form',
        [
            Button('Ok', 'end', 'tryton-executable'),
        ])
    generate_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AccumulatedLawBenefitsForMigratedPaylips, cls).__setup__()
        cls._error_messages.update({
            'no_type_codes': ('Error! No se ha elegido ningun tipo de '
                'beneficio de ley.'),
            'no_addmited_codes': ('Error! Los códigos que se admiten en este '
                'script son %(addmited_codes)s.'),
            'date_from_greater_date_to': ('Error! La fecha de inicio '
                '%(date_from)s es mayor que la fecha fin %(date_to)s.'),
            'law_benefit_type_not_found': ('Error! No se encontró el tipo de '
                'beneficio de ley "%(code)s".'),
            'expression_error': ('%(error)s')
        })

    def transition_generate_(self):
        type_codes = [type.code for type in self.start.law_benefit_types]
        date_from = self.start.date_from
        date_to = self.start.date_to
        self.recalculate_law_benefits(type_codes=type_codes,
            date_from=date_from, date_to=date_to)
        return 'completed'

    def recalculate_law_benefits(self, type_codes=['xiii', 'xiv'],
            date_from=None, date_to=None):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        LawBenefit = pool.get('payslip.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        if not type_codes:
            print('Error! No se ha elegido ningun tipo de beneficio de ley.')
            self.raise_user_error('no_type_codes')

        ADMITED_BENEFIT_TYPES = ['xiii', 'xiv', 'fr']
        for type_code in type_codes:
            if type_code not in ADMITED_BENEFIT_TYPES:
                print('Error! Los códigos que se admiten en este script son ' +
                      str(ADMITED_BENEFIT_TYPES) + '.')
                self.raise_user_error('no_addmited_codes', {
                    'addmited_codes': str(ADMITED_BENEFIT_TYPES)
                })

        # Se recuperan los tipos de beneficios de ley con los que se trabajará
        law_benefit_types = defaultdict(lambda: None)
        for code in type_codes:
            if code not in law_benefit_types:
                lb_type = LawBenefitType.search([
                    ('code', '=', code)]
                )
                if lb_type:
                    law_benefit_types[code] = lb_type[0]
                else:
                    print('Error! No se encontró el tipo de beneficio de ley "'
                        + code + '".')
                    self.raise_user_error('law_benefit_type_not_found', {
                        'code': code
                    })

        to_save = []
        to_save_xiii = []
        to_save_xiv = []
        to_save_fr = []
        to_delete = []

        # Se eliminan los beneficios de ley acumulados creados para el período
        # de búsqueda
        to_delete = LawBenefit.search([
            ('type_.code', 'in', type_codes),
            ('kind', '=', 'accumulate'),
            ('period.start_date', '>=', date_from),
            ('period.end_date', '<=', date_to),
        ])

        # Se recuperan los roles de pago de la base de datos
        print(('Recuperando información de roles de pago...'))
        payslips = Payslip.search([
            ('period.start_date', '>=', date_from),
            ('period.end_date', '<=', date_to),
            ('template.accumulated_law_benefits', '=', False),
            ('state', 'not in', ['cancel']),
            ('template.code', 'like', 'rol_%')
            # ('contract.state', 'in', ['done', 'finalized']),
        ])

        # Subrogations
        comms_subs, comms_subs_work_days = self.get_all_comms_subs_details(
            payslips, True, False)

        for payslip in payslips:
            benefit_types_to_calculate = []
            if payslip.xiii == 'accumulate':
                lb_type = law_benefit_types['xiii']
                if law_benefit_types['xiii']:
                    benefit_types_to_calculate.append(lb_type)
            if payslip.xiv == 'accumulate':
                lb_type = law_benefit_types['xiv']
                if lb_type:
                    benefit_types_to_calculate.append(lb_type)
            if payslip.fr == 'accumulate':
                lb_type = law_benefit_types['fr']
                if law_benefit_types['fr']:
                    benefit_types_to_calculate.append(lb_type)

            if benefit_types_to_calculate:
                print('     Calculando beneficios ' + payslip.period.name +
                      ' de ' + payslip.employee.rec_name + ': ')

                temp_work_days = 30 if payslip.period.start_date.month==2 and (
                        comms_subs_work_days[payslip.id] >= 28) else (
                    comms_subs_work_days[payslip.id])

                # Period and worked period days
                period_days = payslip.period_days
                if not period_days:
                    period_days = 0

                worked_period_days = payslip.worked_period_days
                if not worked_period_days:
                    worked_period_days = period_days

                # Absences and permissions
                total_absences = payslip.total_absences
                if not total_absences:
                    total_absences = 0

                total_absences_with_normative = payslip.total_absences_with_normative  # noqa
                if not total_absences_with_normative:
                    total_absences_with_normative = total_absences

                total_permissions = payslip.total_permissions
                if not total_permissions:
                    total_permissions = 0

                # Medical certificates
                info_medical = self.get_all_medical_certificates_info([payslip])
                medical_certificates_info = info_medical[
                    'medical_certificates_info']

                # Days disease
                total_days_disease = payslip.total_days_disease
                if not total_days_disease:
                    total_days_disease = 0

                # Establish worked days info
                # worked_days = (
                #     worked_period_days -
                #     total_absences -
                #     total_permissions)
                worked_days = payslip.worked_days
                if worked_days < 0:
                    worked_days = 0

                # worked_days_with_normative = (
                #     worked_period_days -
                #     total_absences_with_normative -
                #     total_permissions)
                worked_days_with_normative = payslip.worked_days_with_normative
                if worked_days_with_normative < 0:
                    worked_days_with_normative = 0

                manual_entry_amounts = defaultdict(lambda: Decimal('0.00'))

                # Dict codes
                names = {
                    # INFO Libraries and Objects
                    'np': np,
                    'pendulum': pendulum,
                    'pandas': pd,
                    'utils': utils,
                    'date': date,
                    'timedelta': timedelta,
                    'relativedelta': relativedelta,
                    'monthrange': monthrange,
                    'Decimal': Decimal,
                    'Pool': Pool,
                    'Payslip': Payslip,
                    'comms_subs_work_days': temp_work_days,
                    # INFO Payslip
                    'payslip': payslip,
                    'initial_cut_date': payslip.start_date,
                    'final_cut_date': payslip.end_date,
                    'period_start_date': payslip.period.start_date,
                    'period_end_date': payslip.period.end_date,
                    'base_salary': Decimal(payslip.fiscalyear.base_salary),
                    'salary': payslip.salary,
                    # INFO Days
                    'period_days': period_days,
                    'worked_period_days': worked_period_days,
                    'worked_days': worked_days,
                    'worked_days_with_normative':
                        worked_days_with_normative,
                    'absences': total_absences,
                    'absences_with_normative':
                        total_absences_with_normative,
                    'permissions': total_permissions,
                    'days_disease': total_days_disease,
                    'medical_certificates_info':
                        medical_certificates_info[payslip.id],
                    # INFO Manual entries
                    'manual_entry_amounts':
                        manual_entry_amounts[payslip.id],
                }

                for law_benefit_type in benefit_types_to_calculate:
                    result = self.generate(payslip, law_benefit_type, names)
                    error = result['error']
                    benefit = result['benefit']
                    if error:
                        print(error)
                        self.raise_user_error('expression_error', {
                            'error': error
                        })
                    if benefit:
                        if law_benefit_type.code == 'xiii':
                            print('         XIII: ' + str(benefit.amount))
                            to_save_xiii.append(benefit)
                        elif law_benefit_type.code == 'xiv':
                            print('         XIV : ' + str(benefit.amount))
                            to_save_xiv.append(benefit)
                        elif law_benefit_type.code == 'fr':
                            print('         FR : ' + str(benefit.amount))
                            to_save_fr.append(benefit)

        print('Total XIII a crear: ' + str(len(to_save_xiii)))
        print('Total XIV a crear: ' + str(len(to_save_xiv)))
        print('Total FR a crear: ' + str(len(to_save_fr)))

        to_save += to_save_xiii
        to_save += to_save_xiv
        to_save += to_save_fr

        print('Eliminando décimos acumulados anteriores...')
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete)

        print('Guardando décimos acumulados generados...')
        LawBenefit.save(to_save)

        print('Finalizado!')


    def get_all_comms_subs_details(
            self, payslips, commission = False, subrogation = False, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        CommSub = pool.get('company.commission.subrogation')
        CommSubType = pool.get('company.commission.subrogation.type')
        Detail = pool.get('company.commission.subrogation.detail')
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        ActionStaff = pool.get('company.action.staff')
        comm_sub = CommSub.__table__()
        comm_sub_type = CommSubType.__table__()
        detail = Detail.__table__()
        payslip_comm_sub_detail = PayslipCommSubDetail.__table__()
        action_staff = ActionStaff.__table__()

        where = (comm_sub.state == 'done')
        if commission:
            where |= (comm_sub_type.name == 'Encargo')
        if subrogation:
            where |= (comm_sub_type.name == 'Subrogación')

        result = defaultdict(lambda: [])
        result_work_days = defaultdict(lambda: [])
        for payslip in payslips:
            # If an commission/subrogation was registered at the end of the
            # month and did not enter the corresponding payslip, it is
            # considered in the current role if it has registered the specified
            # number of days (11) before the start of the current payslip
            payslip_period_end_date = payslip.period.end_date
            payslip_period_start_date = (
                    payslip.period.start_date - timedelta(days=11))

            # Get possible comm/sub details to consider in this payslip
            table_A = comm_sub.join(detail,
                                    condition=(comm_sub.id == detail.commission)
                                    ).join(comm_sub_type,
                                           condition=(comm_sub_type.id == comm_sub.type_),
                                           ).join(action_staff,
                                                  condition=(action_staff.id == comm_sub.action_staff)
                                                  ).select(
                action_staff.employee,
                detail.id.as_('comm_sub_detail'),
                detail.end_date,
                detail.work_days,
                comm_sub_type.name.as_('comm_sub_type'),
                where=(where &
                       (action_staff.employee == payslip.employee.id) &
                       (detail.end_date <= payslip_period_end_date) &
                       (detail.start_date >= payslip_period_start_date)
                       )
            )
            # Get comm/sub details considered in other payslips
            table_B = payslip_comm_sub_detail.join(detail,
                                                   condition=(detail.id == payslip_comm_sub_detail.comm_sub_detail)
                                                   ).select(
                payslip_comm_sub_detail.comm_sub_detail.as_(
                    'comm_sub_detail_aux'),
                payslip_comm_sub_detail.payslip,
                where=((payslip_comm_sub_detail.was_considered == 't') &
                       (detail.end_date <= payslip.period.end_date)
                       )
            )
            # Get comm/sub details to consider in this payslip
            query = table_A.join(table_B, type_='LEFT OUTER',
                                 condition=(
                                         table_A.comm_sub_detail == table_B.comm_sub_detail_aux)
                                 ).select(
                table_A.comm_sub_detail,
                table_A.work_days,
                table_A.comm_sub_type,
                where=((table_B.payslip == payslip.id) |
                       (table_B.comm_sub_detail_aux == Null)
                       )
            )
            # Execute
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[payslip.id].append(row['comm_sub_detail'])
                if row['comm_sub_type'] == 'Encargo':
                    result_work_days[payslip.id].append(row['work_days'])
        _work_days = defaultdict(lambda: 0)
        for row in result_work_days.items():
            _work_days[row[0]] = reduce(lambda a, b: a + b, [x for x in row[1]])
        return result, _work_days

    def generate(self, payslip, law_benefit_type, dict_names):
        error, benefit = None, None
        expression = self.get_expression(law_benefit_type)
        parser = evaluator.RuleParser(dict_names)
        amount = parser.eval_expr(expression)
        if parser.error:
            error = ('¡¡Error calculando el ' + law_benefit_type.code + ' de ' +
                payslip.employee.rec_name + ': "' + str(parser.error) + '"!!')
        else:
            amount = utils.quantize_currency(amount)
            if amount >= 0:
                benefit = self.get_new_law_benefit(payslip, law_benefit_type,
                    amount)
        return {
            'error': error,
            'benefit': benefit
        }

    def get_expression(self, law_benefit_type):
        expression = None
        if law_benefit_type.code == 'xiii':
            expression = (
                f"dependencias_ingresos = ['rmu', 'encargos', 'subrogaciones']\n"
                f"# Total de ingresos\n"
                f"total_ingresos = 0\n"
                f"for rubro in payslip.lines:\n"
                f"    if rubro.rule.code in dependencias_ingresos:\n"
                f"        total_ingresos += rubro.amount\n"
                f"# Valor descontado por enfermedad/accidente (para evitar "
                f"hacer un ajuste de XIII)\n"
                f"sueldo_diario = salary / period_days\n"
                f"total_enfermedad = 0\n"
                f"for certificate in medical_certificates_info:\n"
                f"    total_enfermedad += (certificate['total_days']  *  "
                f"sueldo_diario  * (1 -  certificate['percentage']))\n"
                f"result = ((total_ingresos + total_enfermedad) / 12)\n"
                f"result\n"
            )
        elif law_benefit_type.code == 'xiv':
            expression = law_benefit_type.expression
        elif law_benefit_type.code == 'fr':
            expression = (
                f"dependencias_ingresos = ['commission', 'horas_extras_cm', 'xiii', 'xiv', 'rmu_febrero_2021', 'retroactivo_rmu', 'retroactivo_encargo']\n"
                f"dias_trabajados = worked_period_days - absences_with_normative - permissions - days_disease - comms_subs_work_days \n"
                f"sueldo = 0 if comms_subs_work_days == 30 else salary \n"
                f"sueldo_diario = sueldo / period_days \n"
                f"# Total de ingresos\n"
                f"total_ingresos = 0\n"
                f"for rubro in payslip.lines:\n"
                f"    if rubro.rule.code in dependencias_ingresos:\n"
                f"        total_ingresos += rubro.amount\n"
                f"total_ing = (sueldo_diario * dias_trabajados) + total_ingresos\n"
                f"total_fr = total_ing * Decimal('8.33') / 100\n"
                f"total_fr\n"
            )
        return expression

    def get_new_law_benefit(self, payslip, law_benefit_type, amount,
            law_benefit_kind=None, generator_payslip=None):
        pool = Pool()
        LawBenefit = pool.get('payslip.law.benefit')
        benefit = LawBenefit()
        benefit.company = payslip.company
        benefit.employee = payslip.employee
        benefit.period = payslip.period
        benefit.amount = amount
        benefit.type_ = law_benefit_type
        if generator_payslip:
            benefit.generator_payslip = generator_payslip
        else:
            benefit.generator_payslip = payslip
        if law_benefit_kind:
            benefit.kind = law_benefit_kind
        else:
            if law_benefit_type.code == 'xiii':
                benefit.kind = payslip.xiii
            elif law_benefit_type.code == 'xiv':
                benefit.kind = payslip.xiv
            elif law_benefit_type.code == 'fr':
                benefit.kind = payslip.fr
        return benefit

    def get_all_medical_certificates_info(self, payslips):
        medical_certificates = defaultdict(lambda: [])
        total_days_disease = defaultdict(lambda: 0)
        for payslip in payslips:
            medical_certificates[payslip.id] = []
            total_days_disease[payslip.id] = 0
            for med in payslip.medical_certificates_discount:
                medical_certificates[payslip.id].append({
                    'certificate_type_id': med.certificate_type.id,
                    'certificate_type': med.certificate_type.name,
                    'total_days': med.number_days_month,
                    'percentage': med.percentage_payslip,
                    'order_': med.order_
                })
                total_days_disease[payslip.id] += med.number_days_month

        return {
            'medical_certificates_info': medical_certificates,
            'total_days_disease': total_days_disease
        }


class AssignPayslipTemplatesToContractsStart(ModelView):
    'Assign Payslip Templates To Contracts Start'
    __name__ = 'assign.payslip.templates.to.contracts.start'

    source_file = fields.Binary('Archivo a cargar',
        required=True)
    template_info = fields.Binary('Plantilla de información',
        filename='template_info_filename',
        file_id='template_info_path', readonly=True)
    template_info_filename = fields.Char('Plantilla',
        states={
            'invisible': True,
        })
    template_info_path = fields.Char('Ubicación de archivo',
        readonly=True)
    template_example_info = fields.Binary('Ejemplo de información',
        filename='template_example_info_filename',
        file_id='template_example_info_path', readonly=True)
    template_example_info_filename = fields.Char('Ejemplo',
        states={
            'invisible': True,
        })
    template_example_info_path = fields.Char('Ubicación de archivo ejemplo',
        readonly=True)

    @staticmethod
    def default_template_info_path():
        return './data/plantilla_roles_contratos.xlsx'

    @staticmethod
    def default_template_info_filename():
        return "%s.%s" % ('Plantilla_roles_contratos', 'xlsx')

    @staticmethod
    def default_template_info():
        path = 'hr_ec_payslip/data/plantilla_roles_contratos.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @staticmethod
    def default_template_example_info_path():
        return './data/plantilla_roles_contratos_ejemplo.xlsx'

    @staticmethod
    def default_template_example_info_filename():
        return "%s.%s" % ('Ejemplo_plantilla_roles_contratos', 'xlsx')

    @staticmethod
    def default_template_example_info():
        path = 'hr_ec_payslip/data/plantilla_roles_contratos_ejemplo.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class AssignPayslipTemplatesToContractsCompleted(ModelView):
    'Assign Payslip Templates To Contracts Completed'
    __name__ = 'assign.payslip.templates.to.contracts.completed'


class AssignPayslipTemplatesToContracts(Wizard):
    'Assign Payslip Templates To Contracts'
    __name__ = 'assign.payslip.templates.to.contracts'

    start = StateView('assign.payslip.templates.to.contracts.start',
        'hr_ec_payslip.assign_payslip_templates_to_contracts_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    completed = StateView('assign.payslip.templates.to.contracts.completed',
        'hr_ec_payslip.assign_payslip_templates_to_contracts_completed_view_form',
        [
            Button('Ok', 'end', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AssignPayslipTemplatesToContracts, cls).__setup__()
        cls._error_messages.update({
            'no_contracts': ('No se ha encontrado en el sistema un contrato '
                'para el empleado %(employee)s con cédula %(identifier)s.'),
            'no_payslip_templates': ('No se ha encontrado en el sistema una '
                'plantilla de rol con nombre "%(name)s".'),
            'more_than_one_template': ('Los siguientes contratos tienen más de '
                'una plantilla:\n\n%(message)s')
        })

    def transition_import_(self):
        pool = Pool()

        Contract = pool.get('company.contract')
        PayslipTemplate = pool.get('payslip.template')
        ContractPayslipTemplate = pool.get('company.contract.payslip.template')

        field_names = ['CEDULA', 'NOMBRES', 'PLANTILLA DE ROL']

        file_data = fields.Binary.cast(self.start.source_file)
        df = pd.read_excel(BytesIO(file_data),
            names=field_names, dtype='object', header=0)

        to_save = []
        considered_contracts = defaultdict(lambda: [])
        for row in df.itertuples():
            # GET A DICTIONARY WITH VALUES
            items = get_dict_from_readed_row(field_names, row)

            # REPLACE BLANK CELLS BY NONE VALUE
            replace_blanks_by_none(items)

            # IDENTIFIER
            if len(str(items['CEDULA'])) == 9:
                identifier_temp = f"0{items['CEDULA']}"
            else:
                identifier_temp = str(items['CEDULA'])

            contracts = Contract.search([
                ('employee.identifier', '=', identifier_temp)])

            payslip_templates = PayslipTemplate.search([
                ('name', '=', items['PLANTILLA DE ROL'])
            ])

            if not contracts:
                self.raise_user_error('no_contracts', {
                    'identifier': str(items['CEDULA']),
                    'employee': str(items['NOMBRES'])
                })

            if not payslip_templates:
                self.raise_user_error('no_payslip_templates', {
                    'name': str(items['PLANTILLA DE ROL'])
                })

            for contract in contracts:
                for payslip_template in payslip_templates:
                    contract_payslip_template = ContractPayslipTemplate()
                    contract_payslip_template.contract = contract
                    contract_payslip_template.payslip_template = payslip_template #noqa

                    considered_contracts[contract.rec_name].append(
                        payslip_template.name)
                    to_save.append(contract_payslip_template)

        message = ''
        for key, values in considered_contracts.items():
            if len(values) > 1:
                message += f'{key}: {str(values)}\n'
        if message:
            self.raise_user_warning('more_than_one_template',
                'more_than_one_template', {'message': message})

        ContractPayslipTemplate.save(to_save)
        return 'completed'
