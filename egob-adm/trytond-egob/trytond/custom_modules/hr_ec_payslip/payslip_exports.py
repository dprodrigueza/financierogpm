import datetime
import pendulum
from calendar import monthrange
from collections import defaultdict

from trytond.wizard import Wizard, StateReport, StateView, Button
from trytond.model import ModelView, fields, ModelSQL
from trytond.pyson import Eval, If, Bool, In
from trytond.pool import Pool
from trytond.tools import cursor_dict, grouped_slice, reduce_ids
from trytond.transaction import Transaction


__all__ = [
    'PayslipOvertimeLinesExportWizardStart', 'PayslipOvertimeLinesExportWizard',
    'ConglomerateGeneralRolWizardStart', 'ConglomerateGeneralRolWizard',
    'PayslipMedicalCertificateDiscountExportWizardStart',
    'PayslipMedicalCertificateDiscountExportWizard',
    'PayslipSubsidiesExportWizardStart', 'PayslipSubsidiesExportWizard'
]



class PayslipOvertimeLinesExportWizardStart(ModelView):
    'Payslip Overtime Lines Export Wizard Start'
    __name__ = 'payslip.overtime.line.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True,
        states={
            'readonly': True
        })
    generals = fields.One2Many('payslip.general', None,
        'Roles de pago generales',
        domain=[
            ('company', '=', Eval('company')),
        ],
        states={
            'required': True
        }, depends=['company'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_generals():
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Date = pool.get('ir.date')
        PayslipGeneral = pool.get('payslip.general')
        Period = pool.get('account.period')
        tbl_payslip_general = PayslipGeneral.__table__()
        tbl_period = Period.__table__()

        today = Date.today()
        result = []
        query = tbl_payslip_general.join(tbl_period,
            condition=(tbl_payslip_general.period == tbl_period.id)
            ).select(
                tbl_payslip_general.id,
                where=((tbl_period.start_date <= today) &
                       (tbl_period.end_date >= today))
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['id'])
        return result


class PayslipOvertimeLinesExportWizard(Wizard):
    'Payslip Overtime Lines Export Wizard'
    __name__ = 'payslip.overtime.line.export.wizard'

    start = StateView('payslip.overtime.line.export.wizard.start',
        'hr_ec_payslip.payslip_overtime_line_export_wizard_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('payslip.overtime.line.export')

    @classmethod
    def __setup__(cls):
        super(PayslipOvertimeLinesExportWizard, cls).__setup__()

    def do_export_(self, action):
        generals = self.start.generals
        payslips = []

        ids_generals = []
        for general in generals:
            ids_generals.append(general.id)
            if general.lines:
                payslips += [p for p in general.lines]

        ids_payslip_overtime_lines = (
            self.get_payslip_overtime_lines_ids(payslips))

        data = {
            'ids_generals': ids_generals,
            'ids_payslip_overtime_lines': ids_payslip_overtime_lines
        }
        return action, data

    def get_payslip_overtime_lines_ids(self, payslips):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = []

        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        Payslip = pool.get('payslip.payslip')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')

        tbl_payslip_overtime_line = PayslipOvertimeLine.__table__()
        tbl_payslip = Payslip.__table__()
        tbl_employee = Employee.__table__()
        tbl_party = Party.__table__()

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(tbl_payslip_overtime_line.payslip, sub_ids)
            query = tbl_payslip_overtime_line.join(tbl_payslip,
                    condition=(
                        tbl_payslip_overtime_line.payslip == tbl_payslip.id)
                ).join(tbl_employee,
                    condition=(
                        tbl_payslip.employee == tbl_employee.id)
                ).join(tbl_party,
                    condition=(
                        tbl_employee.party == tbl_party.id)
                ).select(
                    tbl_payslip_overtime_line.id,
                    where=(red_sql &
                           (tbl_payslip_overtime_line.was_considered == True)),
                    order_by=[
                        tbl_party.name
                    ]
                )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['id'])
        return result


class ConglomerateGeneralRolWizardStart(ModelView):
    'Conglomerate General Rol Wizard Start'
    __name__ = 'conglomerate.general.rol.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    general_rol = fields.Many2One('payslip.general', 'Rol General', domain=[
        ('state', '!=', 'cancel')])


    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None


class ConglomerateGeneralRolWizard(Wizard):
    'Conglomerate GeneralRol Wizard'
    __name__ = 'conglomerate.general.rol.wizard'
    start = StateView('conglomerate.general.rol.wizard.start',
        'hr_ec_payslip.conglomerate_general_rol_wizard_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('conglomerate.general.rol.export')

    @classmethod
    def __setup__(cls):
        super(ConglomerateGeneralRolWizard, cls).__setup__()
        cls._error_messages.update({
            'no_amounts': ('No se han encontrado valores para las reglas '
                'seleccionadas en el período "%(period)s".')
        })

    def do_export_(self, action):

        data = {
            'general_id': self.start.general_rol.id,
        }
        return action, data

    def get_dict_departments(self):
        result = {}
        return result


class PayslipMedicalCertificateDiscountExportWizardStart(ModelView):
    'Payslip Medical Certificate Discount Export Wizard Start'
    __name__ = 'payslip.medical.certificate.discount.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True,
        states={
            'readonly': True
        })
    generals = fields.One2Many('payslip.general', None,
        'Roles de pago generales',
        domain=[
            ('company', '=', Eval('company')),
            ('template.type', '=', 'payslip'),
        ],
        states={
            'required': True
        }, depends=['company'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_generals():
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Date = pool.get('ir.date')
        PayslipGeneral = pool.get('payslip.general')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        tbl_payslip_general = PayslipGeneral.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_period = Period.__table__()

        today = Date.today()
        result = []
        query = tbl_payslip_general.join(tbl_period,
            condition=(tbl_payslip_general.period == tbl_period.id)
        ).join(tbl_payslip_template,
            condition=(tbl_payslip_template.id == tbl_payslip_general.template)
        ).select(
            tbl_payslip_general.id,
            where=((tbl_period.start_date <= today) &
                   (tbl_period.end_date >= today) &
                   (tbl_payslip_template.type == 'payslip'))
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['id'])
        return result


class PayslipMedicalCertificateDiscountExportWizard(Wizard):
    'Payslip Medical Certificate Discount Export Wizard'
    __name__ = 'payslip.medical.certificate.discount.export.wizard'

    start = StateView(
        'payslip.medical.certificate.discount.export.wizard.start',
        'hr_ec_payslip.payslip_med_cert_discount_export_wizard_start_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('payslip.medical.certificate.discount.export')

    @classmethod
    def __setup__(cls):
        super(PayslipMedicalCertificateDiscountExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_records': ('No ha elegido ningún registro de rol general.'),
        })

    def do_export_(self, action):
        generals = self.start.generals
        if generals:
            ids_generals = [g.id for g in generals]
        else:
            self.raise_user_error('no_records')

        data = {
            'ids_generals': ids_generals,
        }
        return action, data


class PayslipSubsidiesExportWizardStart(ModelView):
    'Payslip Subsidies Export Wizard Start'
    __name__ = 'payslip.subsidies.export.wizard.start'

    company = fields.Many2One('company.company', 'Empresa', required=True,
        states={
            'readonly': True
        })
    generals = fields.One2Many('payslip.general', None,
        'Roles de pago generales',
        domain=[
            ('company', '=', Eval('company')),
            ('template.type', '=', 'payslip'),
        ],
        states={
            'required': True
        }, depends=['company'])
    rules = fields.One2Many('payslip.rule', None, 'Reglas de rol',
        states={
            'required': True
        })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_generals():
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Date = pool.get('ir.date')
        PayslipGeneral = pool.get('payslip.general')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        tbl_payslip_general = PayslipGeneral.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_period = Period.__table__()

        today = Date.today()
        result = []
        query = tbl_payslip_general.join(tbl_period,
            condition=(tbl_payslip_general.period == tbl_period.id)
        ).join(tbl_payslip_template,
            condition=(tbl_payslip_template.id == tbl_payslip_general.template)
        ).select(
            tbl_payslip_general.id,
            where=((tbl_period.start_date <= today) &
                   (tbl_period.end_date >= today) &
                   (tbl_payslip_template.type == 'payslip'))
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['id'])
        return result

    @staticmethod
    def default_rules():
        pool = Pool()
        PayslipRule = pool.get('payslip.rule')
        subsidy_rules = PayslipRule.search([('name', 'ilike', 'subsidio %')])
        ids = [r.id for r in subsidy_rules]
        return ids


class PayslipSubsidiesExportWizard(Wizard):
    'Payslip Subsidies Export Wizard'
    __name__ = 'payslip.subsidies.export.wizard'

    start = StateView('payslip.subsidies.export.wizard.start',
        'hr_ec_payslip.payslip_subsidies_export_wizard_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Exportar', 'export_', 'tryton-executable'),
        ])
    export_ = StateReport('payslip.subsidies.export')

    @classmethod
    def __setup__(cls):
        super(PayslipSubsidiesExportWizard, cls).__setup__()
        cls._error_messages.update({
            'no_generals': ('No ha elegido ningún registro de rol general.'),
            'no_rules': ('No ha elegido ningún registro de reglas de rol.'),
        })

    def do_export_(self, action):
        generals = self.start.generals
        rules = self.start.rules
        ids_generals = [g.id for g in generals]
        ids_rules = [r.id for r in rules]

        if not ids_generals:
            self.raise_user_error('no_generals')
        if not ids_rules:
            self.raise_user_error('no_rules')

        data = {
            'ids_generals': ids_generals,
            'ids_rules': ids_rules,
        }
        return action, data