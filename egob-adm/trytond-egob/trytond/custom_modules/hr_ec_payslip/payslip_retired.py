from datetime import date, datetime, timedelta, time

from dateutil.relativedelta import relativedelta
from decimal import Decimal
from collections import defaultdict
from io import BytesIO
from calendar import monthrange
from operator import attrgetter
import pendulum

from sql.aggregate import Sum, Count, Max
from sql.conditionals import Case, Coalesce
from sql import Null, With, Values, Union
from trytond import backend
import ast

from trytond.model import (ModelSQL, ModelView, Workflow, fields, Unique,
    sequence_ordered)
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.pyson import Eval, If, Bool, Greater, Len, Or
from trytond.transaction import Transaction
from trytond.tools import (grouped_slice, reduce_ids, cursor_dict, Literal,
    file_open)
from trytond.bus import notify

from . import evaluator

import pandas as pd
import numpy as np

from trytond.modules.hr_ec.retired import _PENSION_PAYMENT_FORM
from trytond.modules.hr_ec_payslip import utils
from trytond.modules.hr_ec_payslip.payslip import (ZERO, CENT, CENT_LB, _KIND,
    _PAYSLIP_ITEM_TYPES, _PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS,
    get_payslips_configuration, get_default_period_days_info, get_fields_values,
    get_single_field_value)


__all__ = ['PayslipGeneralRetired', 'PayslipRetired', 'PayslipRetiredLine',
           'PayslipRetiredLawBenefit', 'PayslipRetiredManualEntry',
           'PayslipRetiredDictCodes', 'TemplatePayslipRetiredDictCodes']


_STATES = {
    'readonly': Eval('state') != 'draft',
}

_DEPENDS = ['state']

_PAYSLIP_PENSION_PAYMENT_FORM = _PENSION_PAYMENT_FORM + [
    (None, 'No considerado')
]


def apply_contract_data(ClassModel, payslips):
    for payslip in payslips:
        payslip.department = None
        payslip.position = None
        payslip.payslip_template_account_budget = None
        payslip.payment_form = None
        payslip.management_start_date = None
        payslip.management_end_date = None
        payslip.param_service_years = None
        payslip.monthly_pension = None
        payslip.global_pension = None
        if payslip.retired:
            payslip.department = payslip.retired.department
            payslip.position = payslip.retired.position
            payslip.payment_form = payslip.retired.payment_form
            payslip.management_start_date = payslip.retired.management_start_date #noqa
            payslip.management_end_date = payslip.retired.management_end_date
            payslip.param_service_years = payslip.retired.param_service_years
            payslip.monthly_pension = payslip.retired.monthly_pension
            payslip.global_pension = payslip.retired.global_pension
            if payslip.retired.payslip_template_account_budget:
                payslip.payslip_template_account_budget = (
                    payslip.retired.payslip_template_account_budget)
    ClassModel.save(payslips)



def get_notification_email_payslip_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.notification_email_payslip_retired



def get_retireds_with_wrong_domain(generals):
    wrong_records = []
    for general in generals:
        for payslip in general.lines:
            if payslip.retired and payslip.retired.state in ['draft']:
                wrong_records.append(payslip.retired)
    if wrong_records:
        message = utils.get_wrong_records_domain_message(wrong_records)
        return message
    return None



class PayslipGeneralRetired(Workflow, ModelSQL, ModelView):
    'Payslip Retired General'
    __name__ = 'payslip.general.retired'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], states=_STATES, depends=_DEPENDS, readonly=True, required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ],
        states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio del periodo'),
        'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin del periodo'),
        'on_change_with_period_end_date')
    start_date = fields.Date('Fecha inicio', required=True,
        states=_STATES, depends=_DEPENDS + ['end_date', 'period_start_date',
            'template_for_accumulated_law_benefits'])
    end_date = fields.Date('Fecha fin', required=True,
        states=_STATES, depends=_DEPENDS + ['start_date', 'period_end_date',
            'template_for_accumulated_law_benefits'])
    template = fields.Many2One('payslip.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['employer_retirement', 'accumulated_law_benefits'])
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    template_for_accumulated_law_benefits = fields.Function(fields.Boolean(
        'Es una plantilla de beneficios de ley acumulados?'),
        'on_change_with_template_for_accumulated_law_benefits',
        searcher='search_template_for_accumulated_law_benefits')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirm', 'Confirmado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado'),
        ('post', 'Contabilizar'),
    ], 'Estado', readonly=True)
    state_translated = state.translated('state')
    lines = fields.One2Many('payslip.retired', 'general', 'Roles individuales',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'])
    total_general = fields.Function(fields.Numeric('Total general'),
        'get_total_general')
    total_income = fields.Function(fields.Numeric('Total ingresos'),
        'get_total_general')
    total_deduction = fields.Function(fields.Numeric('Total deducciones'),
        'get_total_general')

    # TODO VERIFY
    move = fields.Many2One('account.move', 'Asiento de Rol',
        readonly=True, domain=[('company', '=', Eval('company', -1)), ],
        depends=['company'])
    move_lb = fields.Many2One('account.move', 'Asiento de Provisiones',
                              readonly=True, domain=[('company', '=', Eval('company', -1)), ],
                              depends=['company'])
    journal = fields.Many2One('account.journal', 'Diario',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                         | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
            'required': Eval('state').in_(
                ['post']
            ),
        },
        depends=['state'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                          | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
            'readonly': Bool(Eval('budget_impact_direct')),
            'required': Eval('state').in_(
                ['post']
            ),
        }, ondelete='RESTRICT')
    budget_impact_direct = fields.Boolean('Impacto de Partida Directo',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                          | Bool(Eval('template_for_accumulated_law_benefits'))
            )
        })
    move_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('posted', 'Contabilizado'),
        ], 'Estado Asiento Rol', states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                          | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
        }), 'get_move_state',
        searcher='search_move_state')
    # TODO END

    @classmethod
    def get_total_general(cls, generals, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: Decimal("0"))
        result_income = defaultdict(lambda: Decimal("0"))
        result_deduction = defaultdict(lambda: Decimal("0"))
        PayslipRetired = pool.get('payslip.retired')
        payslip = PayslipRetired.__table__()
        query = payslip.select(
            payslip.general,
            Sum(payslip.total_value).as_('total_general'),
            Sum(payslip.total_income).as_('total_income'),
            Sum(payslip.total_deduction).as_('total_deduction'),
            group_by=[payslip.general]
        )
        cursor.execute(*query)
        for (general, total_general, total_income,
                total_deduction) in cursor.fetchall():
            result[general] = total_general
            result_income[general] = total_income
            result_deduction[general] = total_deduction

        return {
            'total_general': result,
            'total_income': result_income,
            'total_deduction': result_deduction,
        }

    @classmethod
    def __setup__(cls):
        super(PayslipGeneralRetired, cls).__setup__()
        cls._error_messages.update({
            'loaded_payslips_error': 'Los roles no se han cargado.',
            'generated_values_error': ('Los valores de algunos roles no han '
                'sido generados.'),
            'no_delete': 'Solamente es posible eliminar registros que estén en '
                'estado "Borrador".',
            'payslip_move_already_posted': 'El asiento ya fue contabilizado',
            'error_unique_constraint': ('Ya existe un rol de pagos general con '
                'la plantilla "%(template)s" para el período "%(period)s".'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para roles de '
                'pago. Por favor, diríjase a Talento Humano / Roles de Pagos / '
                'Configuración / Configuraciones varias y defina la '
                'configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
            'employees_without_email': ('No se han encontrado direcciones de '
                'correos electrónicos para los siguientes empleados:\n'
                '%(employees)s\n\nLos avisos de pago no serán enviados a estas '
                'personas.'),
            'account_error': 'La cuenta %(account)s es de vista.',
            'posted_move': 'El asiento ya esta contabilizado.',
            'set_journal': 'Debe asignar el diario.',
            'set_compromise': 'Seleccione el compromiso o marque '
                'impacto directo.',
            'rules_without_budget': ('No se han encontrado configuraciones\n'
                'para las siguientes reglas:\n'
                '%(rules)s.'),
            'rule_without_configuration': ('Regla sin configuracion para rol:\n'
                '%(rule)s.'),
            'account_without_budget': ('Cuenta: %(account)s sin partida \n'
                'especifica para %(budget)s.'),
            'budget_not_exist': ('Partida no codificada %(description)s.'),
            'wrong_records_domain': ('No se puede ejecutar la acción '
                'solicitada ya que los siguientes registros de jubilación '
                'patronal están en estado BORRADOR:\n\n%(message)s\n\nPor '
                'favor, revise el estado de dichos registros o elimínelos de '
                'este rol de pagos.'),
        })
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': Eval('state').in_(['done', 'cancel', 'draft',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'post': {
                'invisible': Or(Eval('state').in_(['confirm', 'draft',
                    'cancel']), Eval('move_state').in_(['posted']),
                    Bool(Eval('template_for_accumulated_law_benefits'))),
                'depends': ['state'],
            },
            'load_lines': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-import'
            },
            'generate_values': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'send_payment_notice_to_emails': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._order.insert(0, ('end_date', 'DESC'))
        cls._order.insert(1, ('template', 'ASC'))
        cls._order.insert(2, ('start_date', 'DESC'))

    @classmethod
    def validate(cls, generals):
        cls.check_general_payslips_with_same_template_and_period(generals)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @classmethod
    def write(cls, payslips_generals, vals, *args):
        # Change start and end date of each individual payslip in order to
        # match with start and end date of general payslip.
        super(PayslipGeneralRetired, cls).write(payslips_generals, vals, *args)
        pool = Pool()
        Payslip = pool.get('payslip.retired')
        to_save = []
        if payslips_generals:
            for payslip_general in payslips_generals:
                for line in payslip_general.lines:
                    line.start_date = payslip_general.start_date
                    line.end_date = payslip_general.end_date
                    to_save.append(line)
        Payslip.save(to_save)

    @classmethod
    def delete(cls, generals):
        for general in generals:
            if not Transaction().context.get('general_payslip_cancel_state'):
                if general.state in ['done', 'confirm', 'cancel']:
                    cls.raise_user_error('no_delete')
        super(PayslipGeneralRetired, cls).delete(generals)

    @classmethod
    @ModelView.button
    def send_payment_notice_to_emails(cls, generals):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_conf()
        payslips_without_emails = ''
        if notification_email:
            records_to_notify = []
            for general in generals:
                for payslip in general.lines:
                    if not payslip.employee.email:
                        payslips_without_emails += (
                            '\n[' + payslip.employee.rec_name + ']')
                    else:
                        records_to_notify.append(payslip)

            if payslips_without_emails != '':
                cls.raise_user_warning('employees_without_email',
                    'employees_without_email', {
                        'employees': payslips_without_emails
                    })

            if records_to_notify:
                NotificationEmail.trigger(
                    records_to_notify, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, payslips):
        for payslip in payslips:
            payslip.change_lines_states('draft')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, payslips):
        # Check retired domain
        cls.check_retired_domains(payslips)

        for payslip in payslips:
            payslip.change_lines_states('done')
        # TODO CHECK
        # pool = Pool()
        # Move = pool.get('account.move')
        # MoveLine = pool.get('account.move.line')
        # Compromise = pool.get('public.budget.compromise')
        # Certificate = pool.get('public.budget.certificate')
        #
        # moves = []
        # for payslip in payslips:
        #     payslip.change_lines_states('done')
        #     if payslip.move and payslip.move.state == 'posted':
        #         cls.raise_user_error('payslip_move_already_posted')
        #     if payslip.move:
        #         moves.append(payslip.move)
        # if moves:
        #     for m in moves:
        #         MoveLine.delete(m.lines)
        #
        #         certificates_to_delete = []
        #         compromises_to_delete = []
        #
        #         for c in m.compromises:
        #             if c.number == "#" and c.certificate.number == '#':
        #                 c.state = 'draft'
        #                 Compromise.save([c])
        #                 compromises_to_delete.append(c)
        #                 c.certificate.state = 'draft'
        #                 Certificate.save([c.certificate])
        #                 certificates_to_delete.append(c.certificate)
        #
        #         m.compromises = []
        #         Move.save([m])
        #         Compromise.delete(compromises_to_delete)
        #         Certificate.delete(certificates_to_delete)
        # TODO END

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, payslips):
        PayslipGeneralRetired = Pool().get('payslip.general.retired')
        to_delete = []
        for general in payslips:
            # Change individual payslips to canceled status
            general.change_lines_states('cancel')

            # There can not be two general payslips with the same template,
            # period and state (In "cancel" state, that could happen)
            canceled_generals = PayslipGeneralRetired.search([
                ('template', '=', general.template),
                ('period', '=', general.period),
                ('state', '=', 'cancel'),
            ])
            if canceled_generals:
                to_delete += canceled_generals

        # This ignores the restriction of not deleting records in cancel state
        with Transaction().set_context(general_payslip_cancel_state=True):
            PayslipGeneralRetired.delete(to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, payslips):
        # Check retired domain
        cls.check_retired_domains(payslips)

        for payslip in payslips:
            payslip.check_lines_loaded()
            payslip.change_lines_states('confirm')

    @classmethod
    @ModelView.button
    # @Workflow.transition('post')
    def post(cls, payslips):
        # Check retired domain
        cls.check_retired_domains(payslips)
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        for payslip in payslips:
            if not payslip.journal:
                cls.raise_user_error('set_journal')
            if ((not payslip.budget_impact_direct
                 or payslip.budget_impact_direct == False)
                    and not payslip.compromise):
                cls.raise_user_error('set_compromise')
            if payslip.move and payslip.move.state == 'posted':
                cls.raise_user_error('posted_move')
            if payslip.move and payslip.move.state == 'draft':
                if payslip.move.lines:
                    MoveLine.delete(payslip.move.lines)
            move = payslip.get_payslip_move()
            payslip.move = move

            if payslip.move_lb and payslip.move_lb.state == 'draft':
                if payslip.move_lb.lines:
                    MoveLine.delete(payslip.move_lb.lines)
            move_lb = payslip.get_payslip_move_lb()
            payslip.move_lb = move_lb


            payslip.check_lines_loaded()
            payslip.change_lines_states('post')

        cls.save(payslips)

        # TODO CHECK
        # pool = Pool()
        # Move = pool.get('account.move')
        # MoveLine = pool.get('account.move.line')
        # moves = []
        # for payslip in payslips:
        #     if not payslip.journal:
        #         cls.raise_user_error('set_journal')
        #     if ((not payslip.budget_impact_direct
        #             or payslip.budget_impact_direct == False)
        #             and not payslip.compromise):
        #         cls.raise_user_error('set_compromise')
        #     if payslip.move and payslip.move.state == 'posted':
        #         cls.raise_user_error('posted_move')
        #     if payslip.move and payslip.move.state == 'draft':
        #         MoveLine.delete(payslip.move.lines)
        #     move = payslip.get_payslip_move()
        #     payslip.move = move
        #     moves.append(move)
        #
        #     payslip.check_lines_loaded()
        #     payslip.change_lines_states('post')
        #
        # if moves:
        #     Move.save(moves)
        # cls.save(payslips)
        # TODO END

    def change_lines_states(self, new_state):
        pool = Pool()
        Line = pool.get('payslip.retired')
        if new_state == 'draft':
            Line.draft(self.lines)
        elif new_state == 'confirm':
            Line.confirm(self.lines)
        elif new_state == 'done':
            Line.done(self.lines)
        elif new_state == 'cancel':
            Line.cancel(self.lines)

    def check_lines_loaded(self):
        if not self.lines:
            self.raise_user_error('loaded_payslips_error')
        else:
            for line in self.lines:
                if not (line.income_lines or line.deduction_lines):
                    self.raise_user_error('generated_values_error')

    @classmethod
    def check_general_payslips_with_same_template_and_period(cls, generals):
        payslips_conf = get_payslips_configuration()
        do_check = (payslips_conf.
            allow_multiple_general_payslips_with_same_template_and_period)
        if not do_check:
            PayslipGeneralRetired = Pool().get('payslip.general.retired')
            for general in generals:
                list_generals = PayslipGeneralRetired.search([
                    ('id', '!=', general.id),
                    ('company', '=', general.company),
                    ('template', '=', general.template),
                    ('period', '=', general.period),
                ])
                if list_generals:
                    cls.raise_user_error('error_unique_constraint', {
                        'template': general.template.rec_name,
                        'period': general.period.rec_name,
                    })

    @classmethod
    def check_retired_domains(cls, generals):
        wrong_records_message = get_retireds_with_wrong_domain(generals)
        if wrong_records_message:
            cls.raise_user_error('wrong_records_domain', {
                'message': wrong_records_message
            })

    @fields.depends('template')
    def on_change_with_template_for_accumulated_law_benefits(self, name=None):
        if self.template:
            return self.template.accumulated_law_benefits
        return None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_start_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - relativedelta(months=12)
            else:
                if self.period.start_date:
                    return self.period.start_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_end_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - timedelta(days=1)
            else:
                if self.period.end_date:
                    return self.period.end_date
        return None

    @fields.depends('start_date', 'lines')
    def on_change_start_date(self, name=None):
        if self.start_date:
            for line in self.lines:
                line.start_date = self.start_date

    @classmethod
    def get_move_state(cls, payslip_generals, name):
        # TODO CHECK
        ids = [pg.id for pg in payslip_generals]

        cursor = Transaction().connection.cursor()
        Move = Pool().get('account.move')
        move = Move.__table__()
        payslip_general = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payslip_general.id, sub_ids)
            query = payslip_general.join(move, 'LEFT',
                condition=(move.id == payslip_general.move)
                ).select(
                payslip_general.id,
                move.state,
                where=red_sql
            )
            cursor.execute(*query)

            result.update(dict(cursor.fetchall()))

        return result
        # TODO END

    @classmethod
    @ModelView.button
    def load_lines(cls, generals):
        pool = Pool()
        Retired = pool.get('company.retired')
        PayslipRetired = pool.get('payslip.retired')
        to_save = []

        payslips_conf = get_payslips_configuration()

        for general in generals:
            # First: Delete payslips created before for this general payslip
            payslips = PayslipRetired.search([
                ('general', '=', general),
                ('state', '!=', 'cancel'),
            ])
            if payslips:
                PayslipRetired.delete(payslips)

            # Second: Get retireds information
            retireds = []
            if general.template.accumulated_law_benefits:
                # To calculate accumulated XIII, XIV, the system searches
                # the database for those benefits of the law pending payment
                # belonging to the range of the current role
                law_benefit_type = general.template.law_benefit_type
                aux_retireds = (
                    cls.get_retireds_with_pending_law_benefits_to_pay(
                        general.start_date, general.end_date,
                        law_benefit_type))
                for r in aux_retireds:
                    if r.state in ['done']:
                        retireds.append(r)
            else:
                _domain = [
                    ('payslip_templates', 'in', [general.template.id]),
                    ('state', 'in', ['done']),
                    ('management_end_date', '<=', general.period.start_date)
                ]
                retireds = Retired.search(_domain)
            for retired in retireds:
                payslips = PayslipRetired.search([
                    ('retired', '=', retired),
                    ('period', '=', general.period),
                    ('template', '=', general.template),
                    ('state', '!=', 'cancel'),
                ])
                # Do not create repeated payslips for the same retired
                if not payslips:
                    if retired.state in ['done']:
                        payslip = PayslipRetired()
                        payslip.general = general
                        payslip.template = general.template
                        payslip.employee = retired.employee
                        payslip.retired = retired
                        payslip.email_recipient = retired.employee.party
                        payslip.fiscalyear = general.fiscalyear
                        payslip.period = general.period
                        # Define payslip start date
                        payslip.start_date = general.start_date
                        # Define payslip end date
                        payslip.end_date = general.end_date
                        # PayslipRetired law benefit information
                        payslip.xiii = retired.xiii
                        payslip.xiv = retired.xiv
                        to_save.append(payslip)
        PayslipRetired.save(to_save)

    @classmethod
    @ModelView.button
    def generate_values(cls, generals):
        pool = Pool()
        PayslipRetired = pool.get('payslip.retired')

        # Check retired domain
        cls.check_retired_domains(generals)
        for general in generals:
            PayslipRetired.load_lines(general.lines)

    def get_payslip_move(self):
        def prepare_move_line(account, debit, credit, party, budget, compromise):
            payable_budget = None
            line_account = account_values[account]
            if line_account.kind == 'payable' and credit > 0:
                payable_budget = budget
            compromise_to_line = None
            if compromise:
                compromise_to_line = compromise
            else:
                compromise_to_line = self.compromise
            return self.get_move_line(
                move,
                line_account,
                utils.quantize_currency(debit),
                utils.quantize_currency(credit), {
                    'compromise': compromise_to_line,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': payable_budget,
                    'payable_cost_center': default_cost_center
                },
                budget
            )

        self.update_payslip_line_account_budget()
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Party = pool.get('party.party')

        cursor = Transaction().connection.cursor()

        move_lines = defaultdict(lambda: [])
        negative_party = []

        accounting_date = (self.period_start_date
                           if self.end_date < self.period_start_date
                           else self.end_date)

        period_id = Period.find(self.company.id, date=accounting_date)

        move = self.move if self.move else Move(number="#")
        # move = Move(move.id)
        move.type = 'financial'
        move.description = 'Rol de pagos del periodo %s' % self.period.name
        move.journal = self.journal
        move.period = period_id
        move.date = (self.period_start_date
                     if self.end_date < self.period_start_date
                     else self.end_date)
        move.origin = self
        move.company = self.company
        if not self.budget_impact_direct:
            move.compromises = [self.compromise]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        with Transaction().set_context(company=self.company.id,
                                       start_date=self.start_date,
                                       end_date=self.end_date,
                                       state='done',
                                       general=self.id):
            Account = pool.get('account.account')
            AccountMoveLine = pool.get('account.move.line')
            CostCenter = pool.get('public.cost.center')

            default_cost_center, = CostCenter.search([])

            query = self.payslip_query_get()
            cursor.execute(*query)
            result = cursor.fetchall()

            account_values = defaultdict(lambda: Decimal(0))
            account_ids = []
            parent_account_ids = []
            parent_lines = {}
            for (account, debit, credit, party, budget, parent, compromise) in result:
                if account is not None:
                    account_ids.append(account)
                parent_account_ids.append(parent)
            for account in Account.browse(account_ids):
                account_values[account.id] = account
            for (account, debit, credit, party, budget, parent, compromise) in result:
                c_aux = compromise if compromise else ""
                if account is not None:
                    if debit == 0 and credit == 0:
                        continue
                    if debit != 0 and credit != 0:
                        move_lines[party] += prepare_move_line(
                            account, 0, credit, party, budget, None)
                        move_lines[party] += prepare_move_line(
                            account, debit, 0, party, budget, compromise)
                        move_lines[party][-1].payable_move_line = (
                            move_lines[party][-2])
                        if parent:
                            move_lines[party][-2].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    else:
                        move_lines[party] += prepare_move_line(
                            account, debit, credit, party, budget, compromise)
                        if (debit != 0 and account in parent_account_ids
                                and not parent):
                            parent_lines[
                                f'{account}-{budget}-{c_aux}'] = move_lines[party][-1]
                        if parent:
                            move_lines[party][-1].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    if party and credit < 0:
                        negative_party.append(party)

        for np_ in negative_party:
            party = Party(np_)
            negative_val = {}
            negative_amount = 0
            negative_account = None
            neg_move_lines = move_lines.get(np_)
            for line in neg_move_lines:
                if line.debit == 0 and line.credit < 0:
                    negative_val[line.parent.budget.code] = line
                    # negative_amount = line.credit
                    # negative_account = line.parent.account
                    neg_move_lines.remove(line)

            # mismo budget y parent
            lines_to_remove1 = []
            keys_to_remove = []
            party_affected = defaultdict(lambda: Decimal('0.0'))
            is_break = False
            for bud_neg in negative_val.keys():
                if is_break:
                    break
                line_neg = negative_val.get(bud_neg)
                for party in move_lines.keys():
                    if is_break:
                        break
                    if not party:
                        continue
                    lines_to_remove1 = []
                    lines_party = move_lines.get(party)
                    for l in lines_party:
                        if (not l.account.code.startswith('213')
                                or l.debit != 0):
                            continue
                        if l.parent.account.code != line_neg.parent.account.code and l.parent.budget.code != line_neg.parent.budget.code:
                            continue
                        if l.credit > abs(line_neg.credit):
                            l.credit += line_neg.credit
                            l.credit = utils.quantize_currency(l.credit)
                            is_break = True
                            keys_to_remove.append(bud_neg)
                            party_affected[party] += abs(line_neg.credit)
                            break
                        else:
                            line_neg.credit += l.credit
                            line_neg.credit = utils.quantize_currency(line_neg.credit)
                            party_affected[party] += l.credit
                            lines_to_remove1.append(l)

                    for l in lines_to_remove1:
                        lines_party.remove(l)
                        keys_to_remove.append(bud_neg)
                    move_lines[party] = lines_party

            for k in keys_to_remove:
                if negative_val.get(k):
                    negative_val.pop(k)

            for pa in party_affected.keys():
                aditional_lines = []
                for line_employee in move_lines.get(np_):
                    if (not line_employee.account.code.startswith('213')
                            or line_employee.debit != 0):
                        continue
                    if party_affected.get(pa) > 0 and line_employee.credit > 0:
                        if line_employee.credit > party_affected.get(pa):
                            line_aux, = prepare_move_line(
                                line_employee.account.id, 0, utils.quantize_currency(party_affected.get(pa)), pa, None, None)
                            line_aux.parent = line_employee.parent
                            line_aux.move = line_employee.move

                            line_employee.credit -= party_affected.get(pa)
                            line_employee.credit = utils.quantize_currency(line_employee.credit)
                            aditional_lines.append(line_aux)
                            break
                        else:
                            line_employee.party = pa
                            party_affected[pa] -= line_employee.credit
                for al in aditional_lines:
                    move_lines.get(np_).append(al)

            lines_to_remove = []
            if negative_val:
                for budget_negative in negative_val.keys():
                    for line in neg_move_lines:
                        if (not line.account.code.startswith('213')
                                or line.debit != 0 or line.parent.budget.code[:2] != budget_negative.code[:2]):
                            continue
                        if line.debit == 0 and line.credit > abs(negative_val.get(budget_negative).credit):
                            line.credit += negative_val.get(budget_negative).credit
                            break
                        else:
                            negative_val[budget_negative].credit += line.credit
                            lines_to_remove.append(line)
                    for line in neg_move_lines:
                        if (not line.account.code.startswith('213')
                                or line.debit != 0):
                            continue
                        if line.debit == 0 and line.credit > abs(negative_val.get(budget_negative).credit):
                            line.credit += negative_val.get(budget_negative).credit
                            break
                        else:
                            negative_val[budget_negative].credit += line.credit
                            lines_to_remove.append(line)
                    for line in lines_to_remove:
                        neg_move_lines.remove(line)
            move_lines[np_] = neg_move_lines

        for p in move_lines.keys():
            if p is None:
                continue
            debit_accounts = defaultdict(lambda: None)
            credit_accounts = defaultdict(lambda: None)
            debit_accounts_to_add = []
            debit_accounts_to_remove = []
            for l in move_lines.get(p):
                if l.debit != 0:
                    id_b = l.budget.id if l.budget else -1
                    debit_accounts[f'{l.account.id}-{id_b}'] = l
                elif l.credit != 0:
                    id_b = l.payable_budget.id if l.payable_budget else -1
                    credit_accounts[f'{l.account.id}-{id_b}'] = l
            for a in debit_accounts.keys():
                party = Party(p)
                pending_debit = 0.0
                fixed_line = None
                # Sin Cuenta
                if a not in credit_accounts.keys():
                    debit = debit_accounts.get(a).debit
                    debit_accounts_to_remove.append(debit_accounts.get(a))
                    pending_debit = debit
                # Ajuste
                elif (debit_accounts.get(a).debit
                      > credit_accounts.get(a).credit):
                    debit = debit_accounts.get(a).debit
                    credit = credit_accounts.get(a).credit
                    pending_debit = utils.quantize_currency((debit - credit))
                    debit_accounts.get(a).debit = credit
                    fixed_line = credit_accounts.get(a)

                for lc in credit_accounts.values():
                    if (lc == fixed_line or lc.account.code.startswith('112')
                            or (not lc.party and not lc.account.kind == 'payable')):  # noqa
                        continue
                    if pending_debit == 0:
                        break
                    amount = 0.0
                    if lc.credit > pending_debit:
                        amount = pending_debit
                    else:
                        amount = lc.credit
                    lines = prepare_move_line(
                        lc.account.id, amount, 0.0, lc.party,
                        lc.parent.budget, lc.compromise)
                    for li in lines:
                        debit_accounts_to_add.append(li)
                        debit_accounts_to_add[-1].payable_move_line = lc
                    pending_debit = utils.quantize_currency(
                        (pending_debit - amount))

            if not debit_accounts_to_add:
                continue
            move_lines[p] = debit_accounts_to_add
            for d in debit_accounts.values():
                move_lines[p].append(d)
            move_lines[p] = [i for i in move_lines[p]
                             if i not in debit_accounts_to_remove]
            for c in credit_accounts.values():
                move_lines[p].append(c)

        mld1 = ()
        mld2 = ()
        mlc = ()
        comp = []
        for ls in move_lines.values():
            for l in ls:
                if l.compromise:
                    comp.append(l.compromise)
                if l.debit != 0 and l.account.kind == 'expense':
                    mld1 += (l,)
                elif l.debit != 0:
                    mld2 += (l,)
                else:
                    mlc += (l,)

        move.compromises = comp
        move.save()

        AccountMoveLine.save(mld1)
        print("mld1")
        AccountMoveLine.save(mlc)
        print("mlc")
        AccountMoveLine.save(mld2)
        print("mld2")

        return move

    def get_payslip_move_lb(self):
        def prepare_move_line(account, debit, credit, party, budget, compromise):
            payable_budget = None
            line_account = account_values[account]
            if line_account.kind == 'payable' and credit > 0:
                payable_budget = budget
            compromise_to_line = None
            if compromise:
                compromise_to_line = compromise
            else:
                compromise_to_line = self.compromise
            return self.get_move_line(
                move,
                line_account,
                debit,
                credit, {
                    'compromise': compromise_to_line,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': payable_budget,
                    'payable_cost_center': default_cost_center
                },
                budget
            )

        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')

        cursor = Transaction().connection.cursor()

        move_lines = defaultdict(lambda: [])

        accounting_date = (self.period_start_date
                           if self.end_date < self.period_start_date
                           else self.end_date)

        period_id = Period.find(self.company.id, date=accounting_date)

        move = self.move_lb if self.move_lb else Move(number="#")
        move.type = 'financial'
        move.description = (
                'Rol de Beneficios Sociales del periodo %s' % self.period.name)
        move.journal = self.journal
        move.period = period_id
        move.date = (self.period_start_date
                     if self.end_date < self.period_start_date
                     else self.end_date)
        move.origin = self
        move.company = self.company
        if not self.budget_impact_direct:
            move.compromises = [self.compromise_lb]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        with Transaction().set_context(company=self.company.id,
                                       start_date=self.start_date, end_date=self.end_date,
                                       state='done', general=self.id):
            Account = pool.get('account.account')
            AccountMoveLine = pool.get('account.move.line')
            CostCenter = pool.get('public.cost.center')

            default_cost_center, = CostCenter.search([])

            query_lb = self.payslip_lb_query_get()
            cursor.execute(*query_lb)
            result = cursor.fetchall()

            account_values = defaultdict(lambda: Decimal(0))
            account_ids = []
            parent_account_ids = []
            parent_lines = {}
            for (account, debit, credit, party, budget, parent, compromise) in result:
                if account is not None:
                    account_ids.append(account)
                parent_account_ids.append(parent)
            for account in Account.browse(account_ids):
                account_values[account.id] = account
            for (account, debit, credit, party, budget, parent, compromise) in result:
                c_aux = compromise if compromise else ""
                if account is not None:
                    if debit == 0 and credit == 0:
                        continue
                    if debit != 0 and credit != 0:
                        move_lines[party] += prepare_move_line(
                            account, 0, credit, party, budget, None)
                        move_lines[party] += prepare_move_line(
                            account, debit, 0, party, budget, compromise)
                        move_lines[party][-1].payable_move_line = (
                            move_lines[party][-2])
                        if parent:
                            move_lines[party][-2].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    else:
                        move_lines[party] += prepare_move_line(
                            account, debit, credit, party, budget, compromise)
                        if (debit != 0 and account in parent_account_ids
                                and not parent):
                            parent_lines[f'{account}-{budget}-{c_aux}'] = move_lines[party][-1]
                        if parent:
                            move_lines[party][-1].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))

        mld1 = ()
        mld2 = ()
        mlc = ()
        comp = []
        for ls in move_lines.values():
            for l in ls:
                if l.compromise:
                    comp.append(l.compromise)
                if l.debit != 0 and l.account.kind == 'expense':
                    mld1 += (l,)
                elif l.debit != 0:
                    mld2 += (l,)
                else:
                    mlc += (l,)

        move.compromises = comp
        move.save()
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        return move

    def update_payslip_line_account_budget(self):
        # Account Budget Rules
        account_budget_rules = self.get_all_rules_account_budget()

        pool = Pool()
        PayslipLine = pool.get('payslip.retired.line')
        PayslipLawBenefit = pool.get('payslip.retired.law.benefit')
        transaction = Transaction()
        errors_msg = ""
        rules_without_budget = []
        line_to_save = []
        line_lb_to_save = []
        for p in self.lines:
            ls = []
            lbs = []
            for l in p.lines:
                if not p.payslip_template_account_budget:
                    self.raise_user_error('payslip_template_ab_not_exist', {
                        'description': (" Rol:%s" % (
                                            p.id))
                    })
                config = account_budget_rules.get(str(p.id) + "-" + str(l.rule.id))
                if config is not None:
                    l.expense_account = config.get('expense_account')
                    l.payable_account = config.get('payable_account')
                    l.receivable_account = config.get('receivable_account')
                    l.revenue_account = config.get('revenue_account')
                    l.budget = config.get('budget')
                    if l.expense_account.kind == 'expense' and not l.budget:
                        rules_without_budget.append("%s - %s - %s - %s" % (
                            #p.contract.id,
                            #p.contract.department.name,
                            #p.contract.work_relationship.name,
                            l.rule.name))
                    if l.budget and l.budget.kind == 'view':
                        self.raise_user_error('budget_not_exist', {
                            'description': " Rol:%s" % (
                                               p.id)
                        })
                    ls.append(l)
                else:
                    #self.raise_user_error('rule_without_configuration', {
                    #    'rule': " Rol:%s, Regla:%s" % (
                    #                p.id, l.rule.name
                    #            )
                    #})
                    errors_msg += ("\n Rol:%s, Regla:%s" % (p.id, l.rule.name))
                    continue
            for lb in p.law_benefits:
                if lb.kind != 'accumulate':
                    continue
                r = None
                for rl in lb.type_.rule:
                    if 'accumulate' in rl.code:
                        r = rl
                        break
                config = account_budget_rules.get(
                    str(p.id) + "-" + str(r.id))
                if config is not None:
                    lb.expense_account = config.get('expense_account')
                    lb.payable_account = config.get('payable_account')
                    lb.budget = config.get('budget')
                    lbs.append(lb)
                else:
                    #self.raise_user_error('rule_without_configuration', {
                    #    'rule': " Rol:%s, Regla:%s" % (
                    #                p.id, l.rule.name
                    #            )
                    #})
                    errors_msg += ("\n Rol:%s, Regla:%s" % (p.id, r.name))
                    continue

            line_to_save.extend(ls)
            line_lb_to_save.extend(lbs)
        PayslipLine.save(line_to_save)
        PayslipLawBenefit.save(line_lb_to_save)
        transaction.commit()
        if errors_msg != '':
            self.raise_user_error('budget_not_exist', {
                'description': errors_msg
            })

        if rules_without_budget:
            error_rules = ""
            for e in rules_without_budget:
                error_rules += e + "\n"
            self.raise_user_error('rules_without_budget', {
                'rules': error_rules
            })

    def get_all_rules_account_budget(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.retired')
        DepartmentBudget = pool.get('company.department.template.budget')
        PayslipTemplateAccountBudget = pool.get(
            'payslip.template.account.budget')
        PayslipRuleAccountBudget = pool.get('payslip.rule.account.budget')
        Budget = pool.get('public.budget')
        FiscalYear = pool.get('account.fiscalyear')
        payslip = Payslip.__table__()
        dep_budget = DepartmentBudget.__table__()
        budget = Budget.__table__()
        pt_acc_budget = (
            PayslipTemplateAccountBudget.__table__())
        # Payslip Rule Account Budget
        pr_acc_budget = PayslipRuleAccountBudget.__table__()
        fiscalyear = FiscalYear.__table__()

        result = defaultdict(lambda: {lambda: None})
        ids = [p.id for p in self.lines]
        # TODO: id_fiscalyear = self.fiscal_year
        id_fiscalyear = -1
        if self.lines:
            id_fiscalyear = self.lines[0].fiscalyear.id
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payslip.id, sub_ids)
            query = payslip.join(pt_acc_budget,
                                 condition=(
                                         pt_acc_budget.id == payslip.payslip_template_account_budget)  # noqa
                                 ).join(pr_acc_budget,
                                        condition=(pr_acc_budget.template_account_budget == pt_acc_budget.id)  # noqa
                                        ).join(fiscalyear,
                                              condition=(
                                                      #(fiscalyear.id == dep_budget.fiscalyear) &
                                                      (fiscalyear.id == id_fiscalyear))
                                              ).join(budget, 'LEFT',
                                                     condition=(
                                                             (budget.parent == pr_acc_budget.budget) #&
                                                             #(budget.activity == dep_budget.activity)
                                                     )
                                                     ).select(
                pr_acc_budget.expense_account,
                pr_acc_budget.payable_account,
                pr_acc_budget.receivable_account,
                pr_acc_budget.revenue_account,
                Coalesce(budget.id, pr_acc_budget.budget).as_('budget'),
                pr_acc_budget.payslip_rule,
                payslip.id,
                where=red_sql
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[f"{row['id']}-{row['payslip_rule']}"] = {
                    'expense_account': row['expense_account'],
                    'payable_account': row['payable_account'],
                    'receivable_account': row['receivable_account'],
                    'revenue_account': row['revenue_account'],
                    'budget': row['budget'],
                }
        return result

    def get_move_line(self, move, account, debit=0, credit=0, expense_data={},
                      budget=None):
        if debit == 0 and credit == 0:
            return []
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Compromise = pool.get('public.budget.compromise')
        Party = pool.get('party.party')
        if account.kind == 'view':
            self.raise_user_error('account_error', {
                'account': account.rec_name
            })

        def add_line(move, account, debit, credit, budget=None):
            line = MoveLine(cost_center=expense_data.get('cost_center'),
                            payable_budget=None,
                            payable_cost_center=None)
            line.parent = None
            line.move = move
            line.type = 'financial'
            line.account, line.debit, line.credit = account, debit, credit
            line.compromise = expense_data.get('compromise')
            if line.account.party_required:
                if expense_data['party'] is not None:
                    line.party = Party(expense_data['party'])
                elif account.default_party:
                    line.party = account.default_party
                elif line.compromise is not None:
                    line.party = line.compromise.party
            line.budget = budget
            if expense_data['cost_center']:
                line.cost_center = expense_data['cost_center']
            if line.budget and not line.cost_center:
                line.on_change_budget()
            if account.kind == 'payable' and credit > 0:
                line.payable_budget = expense_data.get('payable_budget')
                line.payable_cost_center = expense_data.get(
                    'payable_cost_center')

            return line

        def set_budget_data(line):
            move = line.move
            budget_required = line.on_change_with_budget_required()
            with Transaction().set_context(
                    start_date=move.period.fiscalyear.start_date,
                    end_date=move.date):
                if budget_required:
                    if expense_data['compromise']:
                        line.compromise = Compromise(expense_data['compromise'])
                    allowed_budgets = line.get_allowed_budgets()
                    if allowed_budgets and not line.budget:
                        line.budget = allowed_budgets[0]
                    if line.budget and line.budget.type == 'expense':
                        if expense_data['cost_center']:
                            line.cost_center = expense_data['cost_center']
                        else:
                            costs = line.get_allowed_cost_centers()
                            if costs and not line.cost_center:
                                line.cost_center = \
                                    line.get_allowed_cost_centers()[0]  # noqa
                    if line.budget is None:
                        budget = (line.account.budget_debit or
                                  line.account.budget_credit)
                        self.raise_user_error('account_without_budget', {
                            'account': line.account.rec_name,
                            'budget': budget.rec_name,
                        })
                    if not line.cost_center:
                        line.cost_center = expense_data.get(
                            'payable_cost_center')
                else:
                    line.budget = None
                    line.cost_center = None
                    line.compromise = None

        line = add_line(move, account, debit, credit, budget)
        set_budget_data(line)
        lines = [line]
        if expense_data.get('previous_year_account'):
            for pya in expense_data['previous_year_account'].keys():
                for pya_year in expense_data[
                    'previous_year_account'].get(pya).keys():
                    amount = expense_data[
                        'previous_year_account'].get(pya).get(pya_year)
                    new_line_2 = add_line(move,
                                          pya, amount if line.debit > 0 else line.debit,
                                          amount if line.credit > 0 else line.credit)
                    set_budget_data(new_line_2)
                    lines.append(new_line_2)

            new_line_1 = add_line(move, line.account, line.credit, line.debit)
            set_budget_data(new_line_1)
            lines.append(new_line_1)

        return lines

    @classmethod
    def payslip_lb_query_get(cls):
        pool = Pool()
        Payslip = pool.get('payslip.retired')
        PayslipRule = pool.get('payslip.rule')
        PayslipLawBenefit = pool.get('payslip.retired.law.benefit')
        PayslipLawBenefitType = pool.get('payslip.law.benefit.type')
        Account = pool.get('account.account')
        #Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')
        context = Transaction().context

        payslip = Payslip.__table__()
        payslip_rule = PayslipRule.__table__()
        payslip_lb = PayslipLawBenefit.__table__()
        payslip_lb_type = PayslipLawBenefitType.__table__()
        account = Account.__table__()
        #contract = Contract.__table__()
        employee = Employee.__table__()

        debit_lb_columns = [
            payslip.id.as_('id'),
            Case((account.party_required == True,
                  (Coalesce(account.default_party, employee.party))),
                 else_=None).as_('party'),
            payslip_lb.expense_account.as_('account'),
            payslip_lb.budget.as_('budget'),
            payslip_lb.amount.as_('debit'),
            Literal(0.0).as_('credit'),
            Literal(None).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        credit_lb_columns = [
            payslip.id.as_('id'),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
                                               (Coalesce(account.default_party, employee.party))),
                                              else_=None)).as_('party'),
            payslip_lb.payable_account.as_('account'),
            payslip_lb.budget.as_('budget'),
            Literal(0.0).as_('debit'),
            payslip_lb.amount.as_('credit'),
            Case((account.kind == 'payable',
                  payslip_lb.expense_account),
                 else_=Literal(None)).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        where = Literal(True)
        if context.get('general'):
            where &= payslip.general == context.get('general')

        if context.get('company'):
            where &= payslip.company == context.get('company')

        if context.get('state'):
            where &= payslip.state == context.get('state')

        debit_lb_query = payslip.join(payslip_lb,
            condition=payslip.id == payslip_lb.generator_payslip
        ).join(employee,
            condition=employee.id == payslip.employee
        ).join(account,
            condition=account.id == payslip_lb.expense_account
        ).join(payslip_lb_type,
            condition=payslip_lb_type.id == payslip_lb.type_
        ).join(payslip_rule,
            condition=((payslip_rule.law_benefit_type == payslip_lb_type.id)
                    & (payslip_rule.code.like('%accumulate')))
        ).select(*debit_lb_columns,
            where=where,
        )

        credit_lb_query = payslip.join(payslip_lb,
            condition=payslip.id == payslip_lb.generator_payslip
        ).join(employee,
            condition=employee.id == payslip.employee
        ).join(account,
            condition=account.id == payslip_lb.payable_account
        ).join(payslip_lb_type,
            condition=payslip_lb_type.id == payslip_lb.type_
        ).join(payslip_rule,
            condition=((payslip_rule.law_benefit_type == payslip_lb_type.id)
                    & (payslip_rule.code.like('%accumulate')))
        ).select(*credit_lb_columns,
            where=where,
        )

        union = Union(debit_lb_query, credit_lb_query, all_=True)

        query = union.select(
            union.account,
            Sum(union.debit).as_('debit'),
            Sum(union.credit).as_('credit'), union.party, union.budget,
            Max(union.parent),
            union.compromise,
            group_by=(union.account, union.budget, union.party, union.compromise, union.parent),
            order_by=[Sum(union.credit) > 0])

        return query

    @classmethod
    def payslip_query_get(cls):
        pool = Pool()
        Payslip = pool.get('payslip.retired')
        PayslipLine = pool.get('payslip.retired.line')
        PayslipRule = pool.get('payslip.rule')
        Account = pool.get('account.account')
        #Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')
        context = Transaction().context

        payslip = Payslip.__table__()
        payslip_line = PayslipLine.__table__()
        payslip_rule = PayslipRule.__table__()
        account = Account.__table__()
        deduction_account = Account.__table__()
        #contract = Contract.__table__()
        employee = Employee.__table__()

        debit_columns = [
            payslip.id.as_('id'),
            Case((account.party_required == True,
                  (Coalesce(account.default_party, employee.party))),
                 else_=None).as_('party'),
            payslip_line.expense_account.as_('account'),
            payslip_line.budget.as_('budget'),
            Case(((payslip_line.type_ == 'deduction')
                  & (account.kind == 'expense'),
                  Literal(0.0) * payslip_line.amount),
                 else_=payslip_line.amount).as_('debit'),
            Literal(0.0).as_('credit'),
            Literal(None).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        credit_columns = [
            payslip.id.as_('id'),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
                                               (Coalesce(account.default_party, employee.party))),
                                              else_=None)).as_('party'),
            payslip_line.payable_account.as_('account'),
            payslip_line.budget.as_('budget'),
            #Case((payslip_line.revenue_account != Null, payslip_line.amount),
            #     else_=Literal(0.0)).as_('debit'),
            Literal(0.0).as_('debit'),
            payslip_line.amount.as_('credit'),
            Case((account.kind == 'payable',
                  payslip_line.expense_account),
                 else_=Literal(None)).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        deduction_credit_columns = [
            payslip.id.as_('id'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'commission') | (
                    payslip_rule.code == 'smu') | (
                    payslip_rule.code == 'rmu_servicios_personales') | (
                    payslip_rule.code == 'smu_servicios_personales') | (
                    payslip_rule.code == 'jubilados_neto_jubilacion_patronal') | (
                    payslip_rule.code == 'horas_extras_cm')),
                  Coalesce(payslip_rule.party, Case((account.party_required == True,
                                                     (Coalesce(account.default_party, employee.party))),
                                                    else_=None))), else_=None).as_('party'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'commission') | (
                    payslip_rule.code == 'smu') | (
                    payslip_rule.code == 'rmu_servicios_personales') | (
                    payslip_rule.code == 'smu_servicios_personales') | (
                    payslip_rule.code == 'jubilados_neto_jubilacion_patronal') | (
                    payslip_rule.code == 'horas_extras_cm')),
                  payslip_line.payable_account),
                 else_=None).as_('account'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'commission') | (
                    payslip_rule.code == 'smu') | (
                    payslip_rule.code == 'rmu_servicios_personales') | (
                    payslip_rule.code == 'smu_servicios_personales') | (
                    payslip_rule.code == 'jubilados_neto_jubilacion_patronal') | (
                    payslip_rule.code == 'horas_extras_cm')),
                  payslip_line.budget),
                 else_=None).as_('budget'),
            Literal(0.0).as_('debit'),
            Case(((payslip_line.type_ == 'deduction')
                  & (deduction_account.kind == 'expense'),
                  Literal(-1.0) * payslip_line.amount),
                 else_=Literal(0.0)).as_('credit'),
            Case((account.kind == 'payable',
                  payslip_line.expense_account),
                 else_=Literal(None)).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        debit_income_columns = [
            payslip.id.as_('id'),
            Case((account.party_required == True,
                  (Coalesce(account.default_party, employee.party))),
                 else_=None).as_('party'),
            #payslip_line.receivable_account.as_('account'),
            Literal(None).as_('budget'),
            payslip_line.amount.as_('debit'),
            payslip_line.amount.as_('credit'),
            Literal(None).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        credit_income_columns = [
            payslip.id.as_('id'),
            Case((account.party_required == True,
                  (Coalesce(account.default_party, employee.party))),
                 else_=None).as_('party'),
            #payslip_line.revenue_account.as_('account'),
            Literal(None).as_('budget'),
            Literal(0.0).as_('debit'),
            payslip_line.amount.as_('credit'),
            Literal(None).as_('parent'),
            Literal(0).as_('compromise'),
        ]

        where = Literal(True)
        if context.get('general'):
            where &= payslip.general == context.get('general')

        if context.get('company'):
            where &= payslip.company == context.get('company')

        if context.get('state'):
            where &= payslip.state == context.get('state')

        exclude_lb = (~payslip_rule.is_law_benefit
                      | (payslip_rule.is_law_benefit
                         & ~payslip_rule.code.like('%accumulate')))

        debit_query = payslip.join(payslip_line,
                                   condition=payslip.id == payslip_line.payslip
                                   ).join(employee,
                                         condition=employee.id == payslip.employee
                                         ).join(account,
                                                condition=account.id == payslip_line.expense_account
                                                ).join(payslip_rule,
                                                       condition=payslip_rule.id == payslip_line.rule
                                                       ).select(*debit_columns,
                                                                where=(where & exclude_lb),
                                                                )

        credit_query = payslip.join(payslip_line,
                                    condition=payslip.id == payslip_line.payslip
                                    ).join(employee,
                                          condition=employee.id == payslip.employee
                                          ).join(account,
                                                 condition=account.id == payslip_line.payable_account
                                                 ).join(payslip_rule,
                                                        condition=payslip_rule.id == payslip_line.rule
                                                        ).select(*credit_columns,
                                                                 where=(where & exclude_lb),
                                                                 )

        deduction_credit_query = payslip.join(payslip_line,
                                              condition=payslip.id == payslip_line.payslip
                                              ).join(employee,
                                                    condition=employee.id == payslip.employee
                                                    ).join(account,
                                                           condition=account.id == payslip_line.payable_account
                                                           ).join(deduction_account,
                                                                  condition=deduction_account.id == payslip_line.expense_account
                                                                  ).join(payslip_rule,
                                                                         condition=payslip_rule.id == payslip_line.rule
                                                                         ).select(*deduction_credit_columns,
                                                                                  where=(where & exclude_lb),
                                                                                  )

        deduction_credit_query = deduction_credit_query.select(
            deduction_credit_query.id.as_('id'),
            Max(deduction_credit_query.party).as_('party'),
            Max(deduction_credit_query.account).as_('account'),
            Max(deduction_credit_query.budget).as_('budget'),
            Sum(deduction_credit_query.debit).as_('debit'),
            Sum(deduction_credit_query.credit).as_('credit'),
            deduction_credit_query.parent.as_('parent'),
            deduction_credit_query.compromise.as_('compromise'),
            group_by=[deduction_credit_query.id,
                      deduction_credit_query.parent,
                      deduction_credit_query.compromise]
        )

        debit_income_query = payslip.join(payslip_line,
                                          condition=payslip.id == payslip_line.payslip
                                          ).join(employee,
                                                condition=employee.id == payslip.employee
                                                ).join(account,
                                                       condition=account.id == payslip_line.receivable_account
                                                       ).join(payslip_rule,
                                                              condition=payslip_rule.id == payslip_line.rule
                                                              ).select(*debit_income_columns,
                                                                       where=where
                                                                       )

        credit_income_query = payslip.join(payslip_line,
                                           condition=payslip.id == payslip_line.payslip
                                           ).join(employee,
                                                 condition=employee.id == payslip.employee
                                                 ).join(account,
                                                        condition=account.id == payslip_line.revenue_account
                                                        ).join(payslip_rule,
                                                               condition=payslip_rule.id == payslip_line.rule
                                                               ).select(*credit_income_columns,
                                                                        where=where
                                                                        )

        union = Union(debit_query, credit_query, deduction_credit_query,
                      #debit_income_query,
                      #credit_income_query,
                      all_=True)

        query = union.select(
            union.account,
            Sum(union.debit).as_('debit'),
            Sum(union.credit).as_('credit'), union.party, union.budget,
            Max(union.parent),
            union.compromise,
            group_by=(union.account, union.budget, union.party, union.compromise,union.parent),
            order_by=[(Sum(union.credit) < 0).asc,
                      ((Sum(union.debit) > 0) & (Sum(union.credit) != 0)).asc,
                      ((Sum(union.debit) > 0) & (Sum(union.credit) == 0) & (union.party == None)).desc,
                      ((Sum(union.debit) > 0) & (Sum(union.credit) == 0)).desc,
                      (Sum(union.credit) > 0).desc])

        return query

    @classmethod
    def delete_wrong_payslips(cls, general):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipRetired = pool.get('payslip.retired')
        PayslipRetiredLine = pool.get('payslip.retired.line')
        PayslipRule = pool.get('payslip.rule')

        tbl_payslip_retired = PayslipRetired.__table__()
        tbl_payslip_retired_line = PayslipRetiredLine.__table__()
        tbl_payslip_rule = PayslipRule.__table__()

        # GET WRONG PAYSLIPS AND ITS LINES
        select_query_B = tbl_payslip_retired.join(tbl_payslip_retired_line,
            condition=(
                tbl_payslip_retired.id == tbl_payslip_retired_line.payslip)
        ).join(tbl_payslip_rule,
            condition=(
                tbl_payslip_retired_line.rule == tbl_payslip_rule.id)
        ).select(
            tbl_payslip_retired.id,
            where=((tbl_payslip_retired.general == general.id)
                   & ((tbl_payslip_rule.payslip_position == 'left')
                   | (tbl_payslip_rule.payslip_position == 'right'))),
        )

        select_query_A = tbl_payslip_retired.join(tbl_payslip_retired_line,
            condition=(
                tbl_payslip_retired.id == tbl_payslip_retired_line.payslip)
        ).join(tbl_payslip_rule,
            condition=(
                tbl_payslip_retired_line.rule == tbl_payslip_rule.id)
        ).select(
            tbl_payslip_retired.id.as_('payslip'),
            tbl_payslip_retired_line.id.as_('line'),
            where=(((tbl_payslip_retired.general == general.id)
                & (tbl_payslip_rule.payslip_position == 'general'))
                & (~tbl_payslip_retired.id.in_(select_query_B))),
        )

        cursor.execute(*select_query_A)
        to_delete_payslips = []
        to_delete_lines = []
        for id_payslip, id_line in cursor.fetchall():
            if not (id_payslip in to_delete_payslips):
                to_delete_payslips.append(id_payslip)
            if not (id_line in to_delete_lines):
                to_delete_lines.append(id_line)

        # DELETE DATA
        if to_delete_payslips:
            PayslipRetiredLine.delete(PayslipRetiredLine.browse(to_delete_lines))
            PayslipRetired.delete(PayslipRetired.browse(to_delete_payslips))
            notify('Information',
                'No se han encontrado líneas de ingresos y deducciones para '
                'algunos empleados en este rol de pagos general, así que los '
                'roles de pagos individuales para dichos empleados, han sido '
                'removidos.', priority=3)

    @classmethod
    def get_retireds_with_pending_law_benefits_to_pay(cls, start_date,
            end_date, law_benefit_type):
        # TODO Check
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')
        Retired = pool.get('company.retired')
        PayslipRetired = pool.get('payslip.retired')
        tbl_payslip = PayslipRetired.__table__()
        tbl_period = Period.__table__()
        tbl_law_benefit = LawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()
        tbl_retired = Retired.__table__()

        retireds = []

        if end_date.year == 2021:
            select_query = tbl_retired.select(
                tbl_retired.id,
                where=(tbl_retired.state == 'done'),
            )
        else:

            # Get retireds from previously saved law benefits
            select_query = tbl_law_benefit.join(tbl_law_benefit_type,
                condition=tbl_law_benefit.type_ == tbl_law_benefit_type.id
            ).join(tbl_period,
                condition=tbl_law_benefit.period == tbl_period.id
            ).join(tbl_payslip,
                condition=tbl_law_benefit.generator_payslip == tbl_payslip.id
            ).join(tbl_retired,
                condition=tbl_payslip.retired == tbl_retired.id
            ).select(
                tbl_retired.id,
                where=((tbl_law_benefit.kind == 'accumulate') &
                       (tbl_law_benefit_type.code == law_benefit_type.code) &
                       (tbl_payslip.state == 'done') &
                       (tbl_period.start_date >= start_date) &
                       (tbl_period.end_date <= end_date)),
                distinct_on=tbl_retired.id
            )

        cursor.execute(*select_query)
        ids = [row['id'] for row in cursor_dict(cursor)]

        for retired in Retired.browse(ids):
            retireds.append(retired)

        return retireds
        # TODO END

    def get_rec_name(self, name):
        return 'ROL GENERAL %s: %s' % (
            self.period.rec_name, self.template.rec_name)

    @classmethod
    def search_move_state(cls, name, clause):
        # TODO Check
        return [('move.state',) + tuple(clause[1:])]
        # TODO END

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('period',) + tuple(clause[1:]),
                  ('template',) + tuple(clause[1:]),
                  ]
        return domain

    @classmethod
    def search_template_for_accumulated_law_benefits(cls, name, clause):
        return [('template.accumulated_law_benefits',) + tuple(clause[1:])]

class PayslipRetired(Workflow, ModelSQL, ModelView):
    'Payslip Retired'
    __name__ = 'payslip.retired'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS, readonly=True, required=True)
    general = fields.Many2One('payslip.general.retired', 'General',
        domain=[
            ('company', '=', Eval('company')),
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('period', '=', Eval('period')),
        ], depends=['company', 'fiscalyear', 'period'], ondelete='CASCADE')
    template = fields.Many2One('payslip.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['employer_retirement', 'accumulated_law_benefits'])
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    template_for_accumulated_law_benefits = fields.Function(fields.Boolean(
        'Es una plantilla de beneficios de ley acumulados?'),
        'on_change_with_template_for_accumulated_law_benefits')
    employee = fields.Many2One('company.employee', 'Empleado',
        states={
            'readonly': True
        }, required=True)
    retired = fields.Many2One('company.retired', 'Jubilado',
        domain=[
            ('company', '=', Eval('company')),
            ('state', 'in', ['done'])
        ],
        states=_STATES, depends=_DEPENDS + ['state', 'company'], required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ],
        states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo'),
        'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo'),
        'on_change_with_period_end_date')
    start_date = fields.Date('Fecha inicio', required=True,
        states=_STATES, depends=_DEPENDS + ['end_date', 'period_start_date',
            'template_for_accumulated_law_benefits'])
    end_date = fields.Date('Fecha fin', required=True,
        states=_STATES, depends=_DEPENDS + ['start_date', 'period_end_date',
            'template_for_accumulated_law_benefits'])
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirm', 'Confirmado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado'),
    ], 'Estado', readonly=True)
    lines = fields.One2Many('payslip.retired.line', 'payslip', 'Rubros')
    income_lines = fields.One2Many('payslip.retired.line', 'payslip',
        'Ingresos',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'income')])
    deduction_lines = fields.One2Many('payslip.retired.line', 'payslip',
        'Deducciones',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'deduction')])
    internal_lines = fields.One2Many('payslip.retired.line', 'payslip',
        'Internas',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'internal')])
    law_benefits = fields.Function(
        fields.One2Many('payslip.retired.law.benefit', None,
        'Beneficios de ley',
        states={
            'invisible': ~Greater(Len(Eval('law_benefits')), 1, True)
        }, order=[('period.end_date', 'DESC')]), 'get_calculated_law_benefits')
    income_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Ingresos manuales',
        states={
            'invisible': ~Greater(Len(Eval('income_entries')), 1, True)
        }), 'get_considered_entries')
    deduction_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Deducciones manuales',
        domain=[
            ('employee', '=', Eval('employee')),
            ('state', '=', 'done'),
            ('entry_date', '<=', Eval('period_end_date'))
        ], states=_STATES, depends=_DEPENDS + ['employee', 'period_end_date']),
        'get_considered_entries', setter='set_deduction_entries')
    period_days = fields.Numeric('Días de periodo', readonly=True,
        states={
            'invisible': Bool(Eval('template_for_accumulated_law_benefits'))
        }, depends=['template_for_accumulated_law_benefits'])
    total_income = fields.Numeric('Total de ingresos', digits=(16, 2),
        states={'readonly': True})
    total_deduction = fields.Numeric('Total de deducciones', digits=(16, 2),
        states={'readonly': True})
    total_value = fields.Numeric('Total a pagar', digits=(16, 2),
        states={'readonly': True})
    manual_revision = fields.Boolean('Requiere revisión manual',
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('manual_revision'))
        })
    min_salary_to_receive = fields.Numeric('Valor mínimo a recibir',
        states={
            'readonly': True,
            'invisible': Bool(Eval('template_for_accumulated_law_benefits'))
        }, depends=['template_for_accumulated_law_benefits'], digits=(16, 2))
    payslip_manual_entries = fields.Many2Many('payslip.retired.manual.entry',
        'payslip', 'entry', 'Entrada manual de rol de pagos')

    # Dict of codes used on a payslip
    dict_codes = fields.One2Many('payslip.retired.dict.codes', 'payslip',
        'Diccionario de códigos',
        states={
            'readonly': True
        })
    dict_codes_template = fields.Function(fields.One2Many(
        'template.payslip.retired.dict.codes', 'payslip',
        'Plantilla diccionario de códigos',
        context={
            'payslip': Eval('id')
        }, order=[('code', 'ASC')]), 'on_change_with_dict_codes_template')

    # EXTRA INFO
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': True
        })
    position = fields.Many2One('company.position', 'Puesto',
        states={
            'readonly': True
        })
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol',
        states={
            'readonly': False
        })
    monthly_pension = fields.Numeric('Pensión mensual', digits=(16, 2),
        states={
            'readonly': True
        })
    global_pension = fields.Numeric('Pensión global', digits=(16, 2),
        states={
            'readonly': True
        })
    email_recipient = fields.Many2One('party.party', 'Destinatario e-mail',
        states={
            'readonly': True
        })
    payment_form = fields.Selection(_PAYSLIP_PENSION_PAYMENT_FORM,
        'Forma de pago',
        states={
            'readonly': True
        })
    management_start_date = fields.Date('Fecha de inicio de gestión',
        states={
            'readonly': True
        })
    management_end_date = fields.Date('Fecha de fin de gestión',
        states={
            'readonly': True
        })
    param_service_years = fields.Numeric(
        'Tiempo de servicio en años', digits=(16, 2),
        states={
            'readonly': True
        })
    xiii = fields.Selection(_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS, 'XIII',
        states={
            'readonly': True
        })
    xiv = fields.Selection(_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS, 'XIV',
        states={
            'readonly': True
        })
    xiii_translated = xiii.translated('xiii')
    xiv_translated = xiv.translated('xiv')

    # Information about migration
    is_migrated = fields.Boolean('Migración',
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('is_migrated')),
        }, depends=['is_migrated'])

    # ICONS
    manual_revision_color = fields.Function(fields.Char('color'),
        'get_manual_revision_color')

    @classmethod
    def __setup__(cls):
        super(PayslipRetired, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('payslip_uniq', Unique(
                t, t.template, t.retired, t.period, t.state),
                'Rol individual debe ser único por registro de jubilado, '
                'plantilla, período y estado.'),
        ]
        cls._error_messages.update({
            'no_delete': ('Sólo se pueden eliminar registros en estado '
                '"Borrador".'),
            'expression_error': ('Error de expresión en la regla "%(rule)s":'
                '\n\nError:\n>>>>\n%(error_message)s\n<<<<'),
            'expression_error_employee': ('Error de expresión en la regla '
                '"%(rule)s" del empleado "%(employee)s":'
                '\n\nError:\n>>>>\n%(error_message)s\n<<<<'),
            'condition_error': ('Error de condición en la regla "%(rule)s": '
                '"%(error_message)s"'),
            'payslip_state': 'El rol "%(payslip)s"  debe estar en borrador.',
            'retired_with_wrong_monthly_pension': ('El jubilado "%(retired)s" '
                'tiene mal definida su pensión mensual '
                '"$%(monthly_pension)s".'),
            'retired_with_wrong_global_pension': ('El jubilado "%(retired)s" '
                'tiene mal definida su pensión global '
                '"$%(global_pension)s".'),
            'contract_with_wrong_hour_cost': ('El contrato "%(contract)s" '
                'tiene mal definido el salario por hora "$%(hour_cost)s".'),
            'no_account': ('No existe cuenta por pagar y/o cuenta de gasto '
                'definidas para la regla "%(rule)s" y el empleado '
                '"%(employee)s."'),
            'invalid_result': ('Resultado inválido "%(result)s" en la regla '
                '"%(rule)s" y empleado "%(employee)s".\n\nLa regla debe '
                'retornar un diccionario con la siguiente estructura:\n'
                '   {"value": valor calculado o un objeto}\n\n'
                'O en su lugar, retornar un valor numérico directamente, pero '
                'en ningún caso deberá retornar un valor Nulo (None).'),
            'invalid_result_key_value': ('Error en la regla "%(rule)s" del '
                'empleado "%(employee)s".\n\nSi la regla retorna un '
                'diccionario, es obligatorio incluir en él la clave "value" '
                'con el valor calculado, así: {"value": valor_calculado}'),
            'sri_form_107_not_found': ('No es posible calcular el ingreso de '
                '"%(employee)s" debido a que su Formulario SRI 107 del '
                'año fiscal "%(fiscalyear)s" no ha sido encontrado.'),
            'sri_conf_income_tax_error': ('No hay valores en la tabla de '
                'configuracion de impuestos de ingresos que concuerden con los'
                'ingresos proyectados definidos por el empleado en su '
                'formulario 107'),
            'sri_conf_basic_fraction_error': ('La fracción basica del impuesto '
                'de ingreso para el año fiscal vigente para el formulario 107 '
                ' no puede ser determinada.'),
            'total_value_less_than_minimum': ('El rol de pagos del periodo '
                '"%(period)s" para el empleado "%(employee)s" requiere '
                'una revision manual. El valor total ($%(total_value)s) a pagar'
                ' es menor al mínimo requerido por el empleado '
                '($%(minimum_required)s). Por favor elimine algunas '
                'deducciones.'),
            'law_benefit_xiv_not_found': ('El beneficio de ley correspondiente '
                'al Décimo Cuarto debe tener asignado el código "xiv". '
                'Porfavor, asigne el código indicado al registro '
                'correspondiente o bien, proceda a crearlo.'),
            'expression_error_updating_xiv': ('No se han podido actualizar los '
                'valores del Décimo Cuarto Sueldo por que ha ocurrido un error '
                'de expresión en la regla:\n\nError:\n>>>>\n%(error_message)s\n'
                '<<<<'),
            'header_error_wrong_template': ('La plantilla del rol '
                '"%(payslip)s" no coincide con la plantilla de su rol general '
                '"%(general)s". Para actualizar la información, diríjase al '
                'rol general, presione el botón "Cargar roles" y luego el '
                'botón "Generar valores".'),
            'header_error_wrong_period': ('El periodo del rol "%(payslip)s" no '
                'coincide con el periodo de su rol general "%(general)s". Para '
                'actualizar la información, diríjase al rol general, presione '
                'el botón "Cargar roles" y luego el botón "Generar valores".'),
            'error_unique_constraint': ('Ya existe un rol de pagos individual '
                'del empleado con contrato "%(contract)s" para el período '
                '"%(period)s", dicho rol usa la plantilla "%(template)s".'),
            'undefined_period': ('No se ha definido en el sistema un período '
                'que contenga la fecha "%(date)s" del historial de pagos del '
                'empleado "%(employee)s".'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para roles de '
                'pago. Por favor, diríjase a Talento Humano / Roles de Pagos / '
                'Configuración / Configuraciones varias y defina la '
                'configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
            'error_payslip_dates_overtime_dates': ('Error en el rol de pagos '
                'con plantilla %(template)s.\n\nPor razones de seguridad, '
                'usted no puede generar el rol de pagos si, en el período del '
                'rol (%(period)s), la fecha de inicio de aprobación de horas '
                'extra (%(overtime_start_date)s) no es igual a la fecha de '
                'inicio de corte del rol de pagos (%(payslip_start_date)s).\n\n'
                'Diríjase a "[..] / Ejercicios fiscales / Períodos", busque el '
                'período %(period)s y solucione el conflicto.'),
            'warning_fr_info': ('El sistema ha determinado que es necesario '
                'pagar fondos de reserva a los siguientes empleados:'
                '\n%(message)s\n\nSin embargo, sus contratos no tienen '
                'activado el check de fondos de reserva.\n\nSi Ud desea '
                'continuar, pulse en SI y el sistema procederá a realizar el '
                'pago y activará el check de Fondos de Reserva '
                'automáticamente.')
        })
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancel', 'draft']),
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'load_lines': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'recalculate_payslip': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-refresh'
            },
            'send_payment_notice_to_email': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._order.insert(0, ('manual_revision', 'DESC'))
        cls._order.insert(1, ('period.end_date', 'DESC'))
        cls._order.insert(2, ('period.start_date', 'DESC'))
        cls._order.insert(3, ('employee.party.name', 'ASC'))

    @classmethod
    def view_attributes(cls):
        return super(PayslipRetired, cls).view_attributes() + [
            ('//page[@id="join_payslip_general"]', 'states',
             {
                 'invisible': Bool(Eval('general'))
             }),
        ]

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @classmethod
    def default_total_income(cls):
        return 0

    @classmethod
    def default_total_deduction(cls):
        return 0

    @classmethod
    def default_total_value(cls):
        return 0

    @classmethod
    def default_min_salary_to_receive(cls):
        return 0

    @classmethod
    def default_xiii(cls):
        return 'monthlyse'

    @classmethod
    def default_xiv(cls):
        return 'monthlyse'

    @staticmethod
    def order_employee(tables):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        table, _ = tables[None]
        if 'employee' not in tables:
            employee = Employee.__table__()
            tables['employee'] = {
                None: (employee, table.employee == employee.id),
            }
        else:
            employee = tables['employee']
        if 'party' not in tables:
            party = Party.__table__()
            tables['party'] = {
                None: (party, employee.party == party.id),
            }
        else:
            party = tables['party']
        return Party.name.convert_order('name', tables['party'], Party)

    @classmethod
    def validate(cls, payslips):
        PayslipRetired = Pool().get('payslip.retired')
        for payslip in payslips:
            # VALIDATE UNIQUE PAYSLIPS
            if not payslip.template.accumulated_law_benefits:
                list_payslips = PayslipRetired.search([
                    ('id', '!=', payslip.id),
                    ('company', '=', payslip.company),
                    ('retired', '=', payslip.retired),
                    ('template.accumulated_law_benefits', '=', False),
                    ('period', '=', payslip.period),
                    ('state', 'not in', ['cancel']),
                    ('template', '=', payslip.template)
                ])
                if list_payslips:
                    cls.raise_user_error('error_unique_constraint', {
                        'retired': payslip.retired.rec_name,
                        'period': payslip.period.rec_name,
                        'template': list_payslips[0].template.name,
                    })

    @classmethod
    def delete(cls, payslips):
        pool = Pool()
        LawBenefit = pool.get('payslip.retired.law.benefit')
        to_delete_lb = []
        to_save_lb = []
        for payslip in payslips:
            if not Transaction().context.get('payslip_cancel_state'):
                if payslip.state in ['done', 'confirm', 'cancel']:
                    cls.raise_user_error('no_delete')

            if payslip.template_for_accumulated_law_benefits:
                if payslip.law_benefits:
                    for benefit in payslip.law_benefits:
                        if benefit.generator_payslip == payslip:
                            to_delete_lb.append(benefit)
                        else:
                            benefit.payslip = None
                            to_save_lb.append(benefit)
            else:
                law_benefits = LawBenefit.search([
                    ('generator_payslip', '=', payslip)
                ])
                if law_benefits:
                    to_delete_lb += law_benefits

        LawBenefit.save(to_save_lb)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_lb)
        super(PayslipRetired, cls).delete(payslips)

    @classmethod
    @ModelView.button
    def send_payment_notice_to_email(cls, payslips):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_conf()
        if notification_email:
            for payslip in payslips:
                if not payslip.employee.email:
                    cls.raise_user_error('employee_without_email', {
                        'employee': payslip.employee.rec_name
                    })
            NotificationEmail.trigger(payslips, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, payslips):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, payslips):
        cls.check_header_information(payslips)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, payslips):
        pool = Pool()
        PayslipRetired = pool.get('payslip.retired')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        PayslipRetiredManualEntry = pool.get('payslip.retired.manual.entry')

        to_delete_lb = []
        to_delete_me = []

        for payslip in payslips:
            # Delete Law Benefits
            to_delete_lb = LawBenefit.search([
                ('generator_payslip', '=', payslip)
            ])

            # Delete register of considered models in this payslip
            considered_models = [
                (to_delete_me, PayslipRetiredManualEntry),
            ]
            for model in considered_models:
                to_delete_list = model[0]
                considered_model = model[1]
                instances = considered_model.search([
                    ('payslip', '=', payslip.id)
                ])
                if instances:
                    to_delete_list += instances

        # There can not be two individual payslips with the same template,
        # employee, period and state in "cancel" state (that could happen)
        to_delete = []
        for payslip in payslips:
            canceled_payslips = PayslipRetired.search([
                ('template', '=', payslip.template),
                ('employee', '=', payslip.employee),
                ('period', '=', payslip.period),
                ('state', '=', 'cancel'),
            ])
            if canceled_payslips:
                to_delete += canceled_payslips

        # Execute deletions
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_lb)
        PayslipRetiredManualEntry.delete(to_delete_me)

        # This ignores the restriction of not deleting records in cancel state
        with Transaction().set_context(payslip_cancel_state=True):
            PayslipRetired.delete(to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, payslips):
        cls.check_header_information(payslips)
        cls.check_min_salary_to_receive(payslips)

    @classmethod
    def check_header_information(cls, payslips):
        if payslips:
            for payslip in payslips:
                if payslip.general:
                    if payslip.template != payslip.general.template:
                        cls.raise_user_error('header_error_wrong_template', {
                            'payslip': payslip.rec_name,
                            'general': payslip.general.rec_name
                        })
                    elif payslip.period != payslip.general.period:
                        cls.raise_user_error('header_error_wrong_period', {
                            'payslip': payslip.rec_name,
                            'general': payslip.general.rec_name
                        })

    @classmethod
    def check_min_salary_to_receive(cls, payslips):
        for payslip in payslips:
            min_salary = payslip.min_salary_to_receive
            min_salary = min_salary if min_salary else 0
            if payslip.total_value < min_salary:
                cls.raise_user_error('total_value_less_than_minimum', {
                    'period': payslip.period.rec_name,
                    'employee': payslip.employee.rec_name,
                    'total_value': payslip.total_value,
                    'minimum_required': payslip.min_salary_to_receive
                })

    @fields.depends('contract')
    def on_change_with_email_recipient(self, name=None):
        if self.contract:
            return self.contract.employee.party.id
        return None

    @fields.depends('template')
    def on_change_with_template_for_accumulated_law_benefits(self, name=None):
        if self.template:
            return self.template.accumulated_law_benefits
        return None

    @fields.depends('income_lines')
    def on_change_with_total_income(self, name=None):
        total = 0
        if self.income_lines:
            for income in self.income_lines:
                if income.amount:
                    total += income.amount
        return total

    @fields.depends('deduction_lines')
    def on_change_with_total_deduction(self, name=None):
        total = 0
        if self.deduction_lines:
            for deduction in self.deduction_lines:
                if deduction.amount:
                    total += deduction.amount
        return total

    @fields.depends('total_income', 'total_deduction')
    def on_change_with_total_value(self, name=None):
        total = 0
        if self.total_income and self.total_deduction:
            total = self.total_income - self.total_deduction
        return total

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_start_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - relativedelta(months=12)
            else:
                if self.period.start_date:
                    return self.period.start_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_end_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - timedelta(days=1)
            else:
                if self.period.end_date:
                    return self.period.end_date
        return None

    @fields.depends('retired')
    def on_change_with_employee(self, name=None):
        if self.retired:
            return self.retired.employee.id
        return None

    @fields.depends('id')
    def on_change_with_dict_codes_template(self, name=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        DictCodes = pool.get('template.payslip.retired.dict.codes')

        dict_codes = None
        with Transaction().set_context(payslip=self.id):
            dict_codes = DictCodes.table_query()
        ids_dict_codes = []
        cursor.execute(*dict_codes)
        for dict_code in cursor.fetchall():
            ids_dict_codes.append(dict_code[0])

        return ids_dict_codes

    @fields.depends('total_value', 'min_salary_to_receive', 'period_days',
        'template_for_accumulated_law_benefits', 'is_migrated')
    def on_change_with_manual_revision(self, name=None):
        if not self.is_migrated:
            if self.total_value and self.min_salary_to_receive:
                return (self.total_value < self.min_salary_to_receive)
        return False

    @classmethod
    def apply_retired_data(cls, payslips):
        apply_contract_data(cls, payslips)

    @classmethod
    @ModelView.button
    def recalculate_payslip(cls, payslips):
        cls.load_lines(payslips, is_recalculate=True)

    @classmethod
    @ModelView.button
    def load_lines(cls, payslips, is_recalculate=False):
        # Verify headers of individual payslips match with general payslips
        cls.check_header_information(payslips)

        # Set departments in every individual payslips
        cls.apply_retired_data(payslips)

        accumulated, no_accumulated = [], []
        for payslip in payslips:
            if payslip.template.accumulated_law_benefits:
                accumulated.append(payslip)
            else:
                no_accumulated.append(payslip)

        if accumulated:
            cls.load_lines_accumulated_templates(accumulated,
                is_recalculate=is_recalculate)
        if no_accumulated:
            cls.load_lines_no_accumulated_templates(no_accumulated,
                is_recalculate=is_recalculate)

    @classmethod
    def load_lines_no_accumulated_templates(cls, payslips,
            is_recalculate=False):
        # Get models
        pool = Pool()
        PayslipRetired = pool.get('payslip.retired')
        Line = pool.get('payslip.retired.line')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        PayslipRetiredManualEntry = pool.get(
            'payslip.retired.manual.entry')
        PayslipRetiredDictCodes = Pool().get('payslip.retired.dict.codes')

        # Manual entries
        manual_entries = cls.get_all_entries(payslips,
            is_recalculate=is_recalculate)
        income_entries = manual_entries['income_entries']
        deduction_entries = manual_entries['deduction_entries']

        # Get all law benefits types
        law_benefit_types = cls.get_law_benefit_types()

        # Get work days information
        period_days = get_default_period_days_info()

        # Get amounts by mannual entry
        manual_entry_amounts = PayslipRetired.get_manual_entries_amounts(
            payslips, income_entries, deduction_entries)

        to_save_payslip_manual_entries = []
        to_update_payslips = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []
        to_delete_lines = []
        to_save_dict_codes = []
        to_delete_dict_codes = []

        totals = defaultdict(lambda: [])

        for payslip in payslips:
            # Set period days
            payslip.period_days = period_days['value']

            # Set retired monthly pension
            monthly_pension = payslip.retired.monthly_pension
            if monthly_pension == None or monthly_pension < 0:
                cls.raise_user_error('retired_with_wrong_monthly_pension', {
                        'retired': payslip.retired.rec_name,
                        'monthly_pension': monthly_pension
                     })

            # Set retired global pension
            global_pension = payslip.retired.global_pension
            if global_pension == None or global_pension < 0:
                cls.raise_user_error('retired_with_wrong_global_pension', {
                        'retired': payslip.retired.rec_name,
                        'global_pension': global_pension
                     })

            # Set pensions on payslip
            payslip.monthly_pension = monthly_pension
            payslip.global_pension = global_pension

            # Totals definition
            totals[payslip.id] = {
                'total_income': ZERO,
                'total_deduction': ZERO,
                'total_income_law_benefits': ZERO
            }

            # Delete previous lines
            to_delete_lines += payslip.lines

            # Get payment form XIII, XIV
            law_benefits_payment_form = (
                cls.get_law_benefits_payment_form_by_payslip(payslip))

            # Update payment form XIII, XIV on payslip
            payslip.xiii = law_benefits_payment_form['xiii']
            payslip.xiv = law_benefits_payment_form['xiv']

            # Get minimum of salary to receive for an employee
            min_salary_to_receive = (
                payslip.get_min_salary_to_receive(monthly_pension))

            # Dictionary of codes used in the definition of rules
            names = {
                # INFO Libraries and Objects
                'np': np,
                'pendulum': pendulum,
                'pandas': pd,
                'utils': utils,
                'date': date,
                'timedelta': timedelta,
                'relativedelta': relativedelta,
                'monthrange': monthrange,
                'Decimal': Decimal,
                'Pool': Pool,
                'PayslipRetired': PayslipRetired,
                # INFO PayslipRetired
                'payslip_retired': payslip,
                'retired': payslip.retired,
                'employee': payslip.employee,
                'start_date': payslip.start_date,
                'end_date': payslip.end_date,
                'period_start_date': payslip.period.start_date,
                'period_end_date': payslip.period.end_date,
                # INFO Pnesion and Salaries'
                'base_salary': Decimal(payslip.fiscalyear.base_salary),
                'monthly_pension': monthly_pension,
                'global_pension': global_pension,
                'min_salary_to_receive': min_salary_to_receive,
                # INFO Period days
                'period_days': period_days['value'],
                # INFO Law benefits
                'law_benefit_types': law_benefit_types,
                'payment_form_xiii': law_benefits_payment_form['xiii'],
                'payment_form_xiv': law_benefits_payment_form['xiv'],
                # INFO Manual entries
                'manual_entry_amounts':
                    manual_entry_amounts[payslip.id],
                # INFO Totals
                'total_income': 0,
                'total_deduction': 0,
                'total_law_benefits': 0,
                'total_by_rule': defaultdict(lambda: 0),
            }

            parser = evaluator.RuleParser(names)

            # Generate result
            result = PayslipRetired.calculate_rules_no_accumulated_templates(
                payslip, parser, names)

            # Create or update considered payslip manual entries
            considered_entries_codes = (
                result['considered_entries_codes'] if result else [])
            to_save_payslip_manual_entries += (
                cls.manage_payslip_manual_entries(
                    payslip,
                    income_entries[payslip.id],
                    deduction_entries[payslip.id],
                    considered_entries_codes)['to_save'])

            if result:
                to_save_lines += result['to_save_lines']
                to_save_law_benefits += result['to_save_law_benefits']
                to_delete_law_benefits += result['to_delete_law_benefits']

                # Get partial totals
                totals[payslip.id] = {
                    'total_income': result['total_income'],
                    'total_deduction': result['total_deduction'],
                    'total_law_benefits': result['total_law_benefits']
                }

                # Get final totals
                income = totals[payslip.id]['total_income']
                deduction = totals[payslip.id]['total_deduction']
                income_lb = totals[payslip.id]['total_law_benefits']

                # Update totals in payslip
                payslip.total_income = utils.quantize_currency(
                    (income + income_lb))
                payslip.total_deduction = utils.quantize_currency(deduction)
                payslip.total_value = utils.quantize_currency(
                    (payslip.total_income - payslip.total_deduction))

                # Update dict codes with totals
                names['total_law_benefits'] = income_lb
                names['total_income'] = payslip.total_income
                names['total_deduction'] = payslip.total_deduction
                names['total_value'] = payslip.total_value

                # Update payslip dict codes
                to_delete_dict_codes += payslip.dict_codes
                dict_code = cls.get_new_payslip_dict_code(payslip, names)
                to_save_dict_codes.append(dict_code)

                # Was the minimum wage to be received exceeded?
                payslip.min_salary_to_receive = min_salary_to_receive
                payslip.manual_revision = (payslip.total_value <
                    payslip.min_salary_to_receive)

                # Update payslip
                to_update_payslips.append(payslip)

        # Save changes
        PayslipRetired.save(to_update_payslips)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_law_benefits)
        LawBenefit.save(to_save_law_benefits)
        Line.delete(to_delete_lines)
        Line.save(to_save_lines)
        PayslipRetiredManualEntry.save(to_save_payslip_manual_entries)
        PayslipRetiredDictCodes.delete(to_delete_dict_codes)
        PayslipRetiredDictCodes.save(to_save_dict_codes)

    @classmethod
    def load_lines_accumulated_templates(cls, payslips, is_recalculate=False):
        # Get models
        pool = Pool()
        PayslipRetired = pool.get('payslip.retired')
        Line = pool.get('payslip.retired.line')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        PayslipRetiredManualEntry = pool.get(
            'payslip.retired.manual.entry')
        PayslipRetiredDictCodes = Pool().get('payslip.retired.dict.codes')

        # Manual entries
        manual_entries = cls.get_all_entries(payslips,
            is_recalculate=is_recalculate)
        income_entries = manual_entries['income_entries']
        deduction_entries = manual_entries['deduction_entries']

        # Get all law benefits types
        law_benefit_types = cls.get_law_benefit_types()

        # Get work days information
        period_days = get_default_period_days_info()

        # Get amounts by mannual entry
        manual_entry_amounts = PayslipRetired.get_manual_entries_amounts(
            payslips, income_entries, deduction_entries)

        to_save_payslip_manual_entries = []
        to_update_payslips = []
        to_save_lines = []
        to_save_law_benefits = []
        to_update_law_benefits = []
        to_delete_law_benefits = []
        to_delete_lines = []
        to_save_dict_codes = []
        to_delete_dict_codes = []

        totals = defaultdict(lambda: [])

        result_lb = cls.get_law_benefits_to_update(payslips)
        to_update_law_benefits += result_lb['to_update']
        to_delete_law_benefits += result_lb['to_delete']

        for payslip in payslips:
            # Set period days
            payslip.period_days = period_days['value']

            # Set retired monthly pension
            monthly_pension = payslip.retired.monthly_pension
            if monthly_pension == None or monthly_pension < 0:
                cls.raise_user_error('retired_with_wrong_monthly_pension', {
                        'retired': payslip.retired.rec_name,
                        'monthly_pension': monthly_pension
                     })

            # Set retired global pension
            global_pension = payslip.retired.global_pension
            if global_pension == None or global_pension < 0:
                cls.raise_user_error('retired_with_wrong_global_pension', {
                        'retired': payslip.retired.rec_name,
                        'global_pension': global_pension
                     })

            # Set pensions on payslip
            payslip.monthly_pension = monthly_pension
            payslip.global_pension = global_pension

            # Totals definition
            totals[payslip.id] = {
                'total_income': ZERO,
                'total_deduction': ZERO,
                'total_income_law_benefits': ZERO
            }

            # Delete previous lines
            to_delete_lines += payslip.lines

            # Get payment form XIII, XIV
            law_benefits_payment_form = (
                cls.get_law_benefits_payment_form_by_payslip(payslip))

            # Update payment form XIII, XIV on payslip
            payslip.xiii = law_benefits_payment_form['xiii']
            payslip.xiv = law_benefits_payment_form['xiv']

            # Get minimum of salary to receive for an employee
            min_salary_to_receive = (
                payslip.get_min_salary_to_receive(monthly_pension))

            # Dictionary of codes used in the definition of rules
            names = {
                # INFO Libraries and Objects
                'np': np,
                'pendulum': pendulum,
                'pandas': pd,
                'utils': utils,
                'date': date,
                'timedelta': timedelta,
                'relativedelta': relativedelta,
                'monthrange': monthrange,
                'Decimal': Decimal,
                'Pool': Pool,
                'PayslipRetired': PayslipRetired,
                # INFO PayslipRetired
                'payslip_retired': payslip,
                'retired': payslip.retired,
                'employee': payslip.employee,
                'start_date': payslip.start_date,
                'end_date': payslip.end_date,
                'period_start_date': payslip.period.start_date,
                'period_end_date': payslip.period.end_date,
                # INFO Salaries'
                'base_salary': Decimal(payslip.fiscalyear.base_salary),
                'monthly_pension': monthly_pension,
                'global_pension': global_pension,
                'min_salary_to_receive': min_salary_to_receive,
                # INFO Period days
                'period_days': period_days['value'],
                # INFO Law benefits
                'law_benefit_types': law_benefit_types,
                'payment_form_xiii': law_benefits_payment_form['xiii'],
                'payment_form_xiv': law_benefits_payment_form['xiv'],
                # INFO Manual entries
                'manual_entry_amounts':
                    manual_entry_amounts[payslip.id],
                # INFO Totals
                'total_income': 0,
                'total_deduction': 0,
                'total_law_benefits': 0,
                'total_by_rule': defaultdict(lambda: 0),
            }

            parser = evaluator.RuleParser(names)

            # Generate result
            result = PayslipRetired.calculate_rules_accumulated_templates(
                payslip, parser, names)

            # Create or update considered payslip manual entries
            considered_entries_codes = (
                result['considered_entries_codes'] if result else [])
            to_save_payslip_manual_entries += (
                cls.manage_payslip_manual_entries(
                    payslip,
                    income_entries[payslip.id],
                    deduction_entries[payslip.id],
                    considered_entries_codes)['to_save'])

            if result:
                to_save_lines += result['to_save_lines']
                to_save_law_benefits += result['to_save_law_benefits']
                to_delete_law_benefits += result['to_delete_law_benefits']

                # Get partial totals
                totals[payslip.id] = {
                    'total_income': result['total_income'],
                    'total_deduction': result['total_deduction'],
                    'total_law_benefits': result['total_law_benefits']
                }

                # Get final totals
                income = totals[payslip.id]['total_income']
                deduction = totals[payslip.id]['total_deduction']
                income_lb = totals[payslip.id]['total_law_benefits']

                # Update totals in payslip
                payslip.total_income = utils.quantize_currency(
                    (income + income_lb))
                payslip.total_deduction = utils.quantize_currency(deduction)
                payslip.total_value = utils.quantize_currency(
                    (payslip.total_income - payslip.total_deduction))

                # Update dict codes with totals
                names['total_law_benefits'] = income_lb
                names['total_income'] = payslip.total_income
                names['total_deduction'] = payslip.total_deduction
                names['total_value'] = payslip.total_value

                # Update payslip dict codes
                to_delete_dict_codes += payslip.dict_codes
                dict_code = cls.get_new_payslip_dict_code(payslip, names)
                to_save_dict_codes.append(dict_code)

                # Was the minimum wage to be received exceeded?
                payslip.min_salary_to_receive = min_salary_to_receive
                payslip.manual_revision = (payslip.total_value <
                    payslip.min_salary_to_receive)

                # Update payslip
                to_update_payslips.append(payslip)

        # Save changes
        PayslipRetired.save(to_update_payslips)
        LawBenefit.save(to_update_law_benefits)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_law_benefits)
        LawBenefit.save(to_save_law_benefits)
        Line.delete(to_delete_lines)
        Line.save(to_save_lines)
        PayslipRetiredManualEntry.save(to_save_payslip_manual_entries)
        PayslipRetiredDictCodes.delete(to_delete_dict_codes)
        PayslipRetiredDictCodes.save(to_save_dict_codes)

    @classmethod
    def calculate_rules_no_accumulated_templates(cls, payslip, parser, names):
        pool = Pool()
        PayslipLawBenefitType = pool.get('payslip.law.benefit.type')

        considered_entries_codes = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []

        for line in payslip.template.lines:
            rule = line.rule
            if not rule.active:
                continue

            parser.aeval.symtable['rule'] = rule
            condition = parser.eval_expr(rule.condition)
            if parser.error:
                cls.raise_user_error('condition_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not condition:
                if rule.is_law_benefit:
                    # XIII and XIV are calculated and saved on data base every
                    # month regardless of whether the employee monthlyizes or
                    # accumulates
                    pass
                else:
                    continue

            result = parser.eval_expr(rule.expression)
            if parser.error:
                cls.raise_user_error('expression_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not isinstance(result, list):
                result = [result]
            parser.aeval.symtable[rule.code] = 0

            # Get mannual entry codes used in every expression
            considered_entries_codes += (
                utils.manual_entries_in_rule_expression(rule.expression))

            for res in result:
                amount = 0
                create_new_payslip_line = True
                value = cls.get_value_from_expression_rule_result(
                    payslip, rule, res)

                if not rule.is_law_benefit:
                    amount = utils.quantize_currency(value)
                    parser.aeval.symtable[rule.code] += amount
                    if amount >= 0:
                        if rule.type_ == 'income':
                            parser.aeval.symtable['total_income'] += amount
                        elif rule.type_ == 'deduction':
                            parser.aeval.symtable['total_deduction'] += amount
                else:
                    # The value could be a type of law benefit
                    if isinstance(value, PayslipLawBenefitType):
                        # Get law benefit type and its expression
                        law_benefit_type = value
                        expression = law_benefit_type.expression
                        # Get mannual entry codes used in the expression
                        considered_entries_codes += (utils.
                            manual_entries_in_rule_expression(expression))
                        # Get amount
                        _res = parser.eval_expr(expression)
                        amount = cls.get_amount_from_expression_rule_result(
                            payslip, rule, _res)
                    # Else, the value could be a expression that return a
                    # number directly
                    else:
                        law_benefit_type = rule.law_benefit_type
                        amount = utils.quantize_currency(value)

                    # Delete previous law benefits created for this
                    # period, employee and type_
                    to_delete_law_benefits += (
                        cls.get_law_benefits_to_delete(
                            payslip, law_benefit_type))

                    # Calculation and creation of a new law benefit for
                    # this period, employee and type_
                    if amount >= 0:
                        benefit = (cls.get_new_law_benefit(
                            payslip, law_benefit_type, amount))

                        # XIII, XIV
                        if benefit.kind == 'monthlyse':
                            parser.aeval.symtable[
                                'total_law_benefits'] += amount
                            benefit.payslip = payslip
                            benefit.payslip_date = (
                                payslip.period.end_date)
                        elif benefit.kind == 'accumulate':
                            # The accumulated XIII, XIV are calculated
                            # monthly and saved in the database but not
                            # shown in the current payslip.
                            create_new_payslip_line = False
                        to_save_law_benefits.append(benefit)

                # Save amount of rule in dict_names
                if rule.code not in names['total_by_rule']:
                    names['total_by_rule'].update({rule.code: amount})
                else:
                    names['total_by_rule'][rule.code] += amount

                # Create line
                if create_new_payslip_line and amount > 0:
                    pline = PayslipRetired.get_new_payslip_line(payslip, rule,
                        rule.type_, amount)
                    to_save_lines.append(pline)

        t_income = parser.aeval.symtable['total_income']
        t_deduction = parser.aeval.symtable['total_deduction']
        t_law_benefit = parser.aeval.symtable['total_law_benefits']

        # Delete duplicates
        considered_entries_codes = list(set(considered_entries_codes))

        return {
            'total_income': utils.quantize_currency(t_income),
            'total_deduction': utils.quantize_currency(t_deduction),
            'total_law_benefits': utils.quantize_currency(t_law_benefit),
            'to_save_lines': to_save_lines,
            'to_save_law_benefits': to_save_law_benefits,
            'to_delete_law_benefits': to_delete_law_benefits,
            'considered_entries_codes': considered_entries_codes,
        }

    @classmethod
    def calculate_rules_accumulated_templates(cls, payslip, parser, names):
        PayslipLawBenefitType = Pool().get('payslip.law.benefit.type')

        considered_entries_codes = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []

        for line in payslip.template.lines:
            rule = line.rule
            if not rule.active:
                continue

            parser.aeval.symtable['rule'] = rule
            condition = parser.eval_expr(rule.condition)
            if parser.error:
                cls.raise_user_error('condition_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not condition:
                continue

            result = parser.eval_expr(rule.expression)
            if parser.error:
                cls.raise_user_error('expression_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not isinstance(result, list):
                result = [result]
            parser.aeval.symtable[rule.code] = 0

            # Get mannual entry codes used in every expression
            considered_entries_codes += (
                utils.manual_entries_in_rule_expression(rule.expression))

            for res in result:
                amount = 0
                create_new_payslip_line = True
                value = cls.get_value_from_expression_rule_result(
                    payslip, rule, res)

                if not rule.is_law_benefit:
                    amount = utils.quantize_currency(value)
                    parser.aeval.symtable[rule.code] += amount
                    if amount >= 0:
                        if rule.type_ == 'income':
                            parser.aeval.symtable['total_income'] += amount
                        elif rule.type_ == 'deduction':
                            parser.aeval.symtable['total_deduction'] += amount
                else:
                    value = res['value'] if res else None
                    # It is possible to send in the key 'value' the type of law
                    # benefit that is being calculated when you want the value
                    # to be calculated internally in the system based on the sum
                    # of all the accumulated records and that the benefits of
                    # law are automatically managed what to consider in the role
                    if isinstance(value, PayslipLawBenefitType):
                        if value:
                            law_benefit_type = value
                            calculation_result = (
                                cls.calculate_accumulated_law_benefits(
                                    payslip, law_benefit_type))

                            accumulated_benefit = calculation_result['benefit']
                            if accumulated_benefit:
                                to_save_law_benefits += (
                                    calculation_result['to_save_law_benefits'])
                                amount = accumulated_benefit.amount
                                parser.aeval.symtable[
                                    'total_law_benefits'] += amount
                    else:
                        # It is also possible to send in the 'value' key the
                        # value calculated by the rule, in this case, the
                        # automatic management of the benefits of law considered
                        # in this role must be explicitly specified.
                        value = Decimal(str(value)) if value else ZERO
                        amount = utils.quantize_currency(value)
                        parser.aeval.symtable[rule.code] += amount
                        if amount >= 0:
                            if rule.type_ == 'income':
                                parser.aeval.symtable['total_income'] += amount
                            elif rule.type_ == 'deduction':
                                parser.aeval.symtable[
                                    'total_deduction'] += amount
                        # The explicit way of indicating that you want the
                        # system to manage the benefits of law considered in the
                        # role is done by sending the type of law benefit in the
                        # key 'manage_law_benefits'
                        if 'manage_law_benefits' in res:
                            law_benefit_type = res['manage_law_benefits']
                            if (isinstance(law_benefit_type,
                                    PayslipLawBenefitType)):
                                calculation_result = (
                                    cls.calculate_accumulated_law_benefits(
                                        payslip, law_benefit_type))
                                accumulated_benefit = calculation_result[
                                    'benefit']
                                if accumulated_benefit:
                                    to_save_law_benefits += (
                                        calculation_result[
                                            'to_save_law_benefits'])

                # Save amount of rule in dict_names
                if rule.code not in names['total_by_rule']:
                    names['total_by_rule'].update({rule.code: amount})
                else:
                    names['total_by_rule'][rule.code] += amount

                # Create line
                if create_new_payslip_line and amount > 0:
                    pline = PayslipRetired.get_new_payslip_line(payslip, rule,
                        rule.type_, amount)
                    to_save_lines.append(pline)

        t_income = parser.aeval.symtable['total_income']
        t_deduction = parser.aeval.symtable['total_deduction']
        t_law_benefit = parser.aeval.symtable['total_law_benefits']

        # Delete duplicates
        considered_entries_codes = list(set(considered_entries_codes))

        return {
            'total_income': utils.quantize_currency(t_income),
            'total_deduction': utils.quantize_currency(t_deduction),
            'total_law_benefits': utils.quantize_currency(t_law_benefit),
            'to_save_lines': to_save_lines,
            'to_save_law_benefits': to_save_law_benefits,
            'to_delete_law_benefits': to_delete_law_benefits,
            'considered_entries_codes': considered_entries_codes,
        }

    @classmethod
    def get_amount_from_expression_rule_result(cls, payslip, rule, result):
        value = cls.get_value_from_expression_rule_result(
            payslip, rule, result)

        if not (isinstance(value, int) or isinstance(value, Decimal) or
                isinstance(value, float)):
            message = 'La regla no retorna un valor numérico'
            cls.raise_expression_error_employee(rule, payslip, message)

        amount = Decimal(str(value)) if value else ZERO
        return utils.quantize_currency(amount)

    @classmethod
    def get_value_from_expression_rule_result(cls, payslip, rule, result):
        value = None
        if not (isinstance(result, dict) or isinstance(result, int) or
                isinstance(result, Decimal) or isinstance(result, float)):
            cls.raise_user_error('invalid_result', {
                'result': result,
                'rule': rule.rec_name,
                'employee': payslip.employee.rec_name,
            })

        if isinstance(result, dict):
            if 'value' not in result:
                message = ('Si la regla retorna un diccionario, es obligatorio '
                    'incluir en él la clave "value" con el valor a retornar, '
                    'así:\n   {"value": valor_calculado u objeto}')
                cls.raise_expression_error_employee(rule, payslip, message)
            else:
                value = result['value']
        else:
            value = Decimal(str(result)) if result else ZERO

        if value is None:
            message = 'La regla no debe retornar un valor Nulo'
            cls.raise_expression_error_employee(rule, payslip, message)
        return value

    @classmethod
    def raise_expression_error_employee(cls, rule, payslip, message):
        cls.raise_user_error('expression_error_employee', {
            'rule': rule.rec_name,
            'employee': payslip.employee.rec_name,
            'error_message': message,
        })

    @classmethod
    def get_law_benefits_to_update(cls, payslips):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        LawBenefit = pool.get('payslip.retired.law.benefit')
        tbl_law_benefit = LawBenefit.__table__()
        tbl_payslip = cls.__table__()

        to_update = []
        to_delete = []

        for payslip in payslips:
            to_update_ids = []
            to_delete_ids = []

            query = tbl_payslip.join(tbl_law_benefit,
                condition=((tbl_payslip.id == tbl_law_benefit.payslip) |
                    (tbl_payslip.id == tbl_law_benefit.generator_payslip))
            ).select(
                tbl_law_benefit.id,
                tbl_law_benefit.payslip,
                tbl_law_benefit.generator_payslip,
                tbl_law_benefit.kind,
                where=(tbl_payslip.id == payslip.id)
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                lb_id = row['id']
                lb_payslip = row['payslip']
                lb_generator_payslip = row['generator_payslip']
                if lb_generator_payslip == payslip.id:
                    to_delete_ids.append(lb_id)
                elif lb_payslip == payslip.id:
                    to_update_ids.append(lb_id)

            for lb in LawBenefit.browse(to_update_ids):
                lb.payslip = None
                to_update.append(lb)

            to_delete += list(LawBenefit.browse(to_delete_ids))

        return {
            'to_update': to_update,
            'to_delete': to_delete
        }

    def get_min_salary_to_receive(self, monthly_pension):
        # TODO Check
        min_value = 0
        return min_value
        # TODO END

    @classmethod
    def get_law_benefit_types(cls):
        LawBenefitType = Pool().get('payslip.law.benefit.type')
        result = defaultdict(lambda: None)
        law_benefits_types = LawBenefitType.search(None)
        if law_benefits_types:
            for law_benefit_type in law_benefits_types:
                result.update({
                    law_benefit_type.code: law_benefit_type
                })
        return result

    @classmethod
    def get_law_benefits_to_delete(cls, payslip, law_benefit_type):
        cursor = Transaction().connection.cursor()
        LawBenefit = Pool().get('payslip.retired.law.benefit')
        law_benefit = LawBenefit.__table__()
        to_delete = []
        delete_query = law_benefit.select(
            law_benefit.id,
            where=(
                (law_benefit.generator_payslip == payslip.id) &
                (law_benefit.period == payslip.period.id) &
                (law_benefit.employee == payslip.employee.id) &
                (law_benefit.type_ == law_benefit_type.id)
            )
        )
        cursor.execute(*delete_query)
        ids = [id for (id,) in cursor.fetchall()]
        for lb in LawBenefit.browse(ids):
            to_delete.append(lb)
        return to_delete


    @classmethod
    def get_law_benefits_payment_form_by_payslip(cls, payslip):
        result = defaultdict(lambda: 'not_considered')

        # Check if exist law benefits rules
        law_benefits_to_calculate = []
        for line in payslip.template.lines:
            rule = line.rule
            if rule.is_law_benefit:
                if rule.law_benefit_type.code not in law_benefits_to_calculate:
                    law_benefits_to_calculate.append(rule.law_benefit_type.code)

        # Get payment form
        for code in law_benefits_to_calculate:
            result[code] = payslip.get_law_benefit_payment_form_by_field(code)

        # Check accumulated templates
        if payslip.template.accumulated_law_benefits:
            code = payslip.template.law_benefit_type.code
            if code in result:
                result[code] = 'accumulate'
        return result

    def get_law_benefit_payment_form_by_field(self, field):
        return getattr(self.retired, field)

    @classmethod
    def calculate_accumulated_law_benefits(cls, payslip, law_benefit_type):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        table = cls.__table__()
        tbl_period = Period.__table__()
        tbl_law_benefit = LawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()

        to_save_law_benefits = []
        current_base_salary = payslip.fiscalyear.base_salary
        total_amount = Decimal('0.00')
        total_adjustment = Decimal('0.00')
        total_rounding_error = Decimal('0.00')

        # Evaluate consulting dates
        start_date = payslip.start_date
        end_date = payslip.end_date

        # Get law benefits generated using payslip functionalities on SIIM
        select_query = tbl_law_benefit.join(tbl_law_benefit_type,
                condition=tbl_law_benefit.type_ == tbl_law_benefit_type.id
            ).join(tbl_period,
                condition=tbl_law_benefit.period == tbl_period.id
            ).join(table,
                condition=tbl_law_benefit.generator_payslip == table.id
            ).select(
                tbl_law_benefit.id,
                where=((tbl_law_benefit.kind == 'accumulate') &
                       (tbl_law_benefit_type.code == law_benefit_type.code) &
                       (tbl_law_benefit.employee == payslip.employee.id) &
                       (table.state == 'done') &
                       (tbl_period.start_date >= start_date) &
                       (tbl_period.end_date <= end_date))
            )

        cursor.execute(*select_query)
        ids = [row['id'] for row in cursor_dict(cursor)]

        for law_benefit in LawBenefit.browse(ids):
            generator_payslip = law_benefit.generator_payslip
            if law_benefit.type_.code == 'xiv':
                # Check rounding errors less than a cent that may have occurred
                # due to the use of deprecated rounding methods
                total_rounding_error += cls.get_rounding_error_xiv_amount(
                    law_benefit)

                # Adjustment to XIV for change in the basic salary of the
                # current fiscal year compared to the previous fiscal year
                previous_base_salary = generator_payslip.fiscalyear.base_salary
                if current_base_salary != previous_base_salary:
                    total_adjustment += cls.get_adjust_xiv_amount(
                        law_benefit,
                        previous_base_salary,
                        current_base_salary)

            total_amount += law_benefit.amount
            law_benefit.payslip = payslip
            law_benefit.payslip_date = generator_payslip.period.end_date
            to_save_law_benefits.append(law_benefit)

        total_adjustment += total_rounding_error
        if law_benefit_type.code == 'xiv' and total_adjustment:
            # If there is an adjustment of XIV due to a change in basic salary
            # or due to a rounding error, the corresponding record of kind
            # "adjustment" is created
            law_benefit_adjust = cls.get_new_law_benefit(
                payslip,
                law_benefit_type,
                total_adjustment,
                law_benefit_kind='adjustment',
                generator_payslip=payslip)
            law_benefit_adjust.payslip = payslip
            law_benefit_adjust.payslip_date = payslip.period.end_date

            total_amount += total_adjustment
            to_save_law_benefits.append(law_benefit_adjust)

        benefit = None
        if to_save_law_benefits:
            # Here, a generic Law Benefit is created, it contains the
            # accumulated amount of the sum of law benefits amounts of this
            # employee
            benefit = LawBenefit()
            benefit.company = payslip.company
            benefit.employee = payslip.employee
            benefit.period = payslip.period
            benefit.amount = total_amount
            benefit.kind = 'accumulate'
            benefit.type_ = law_benefit_type

        return {
            'benefit': benefit,
            'to_save_law_benefits': to_save_law_benefits
        }

    @classmethod
    def get_adjust_xiv_amount(cls, law_benefit, previous_base_salary,
            current_base_salary):
        old_amount = law_benefit.amount
        new_amount = old_amount * current_base_salary / previous_base_salary
        adjustment = new_amount - old_amount
        return utils.quantize_currency(adjustment)

    @classmethod
    def get_rounding_error_xiv_amount(cls, law_benefit):
        months = Decimal('12.00')
        gen_payslip = law_benefit.generator_payslip
        base_salary = gen_payslip.fiscalyear.base_salary

        rounded_amount = law_benefit.amount
        complete_amount = base_salary / months

        error = abs(complete_amount - rounded_amount)
        error = utils.quantize_currency(error, exp=CENT_LB)
        if error <= Decimal('0.01'):
            return error
        return Decimal('0.00')

    @classmethod
    def get_calculated_law_benefits(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: [])
        LawBenefit = pool.get('payslip.retired.law.benefit')
        Period = pool.get('account.period')
        law_benefit = LawBenefit.__table__()
        table = cls.__table__()
        tbl_period = Period.__table__()

        ids_payslips_accum = []
        ids_payslips_month = []
        for p in payslips:
            if p.template.accumulated_law_benefits:
                ids_payslips_accum.append(p.id)
            else:
                ids_payslips_month.append(p.id)

        # PAYSLIPS FOR NO ACCUMULATED LAW BENEFITS
        for sub_ids in grouped_slice(ids_payslips_month):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(law_benefit,
                condition=(table.id == law_benefit.generator_payslip)
            ).select(
                table.id,
                law_benefit.id.as_('law_benefit'),
                where=red_sql & (law_benefit.period == table.period)
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['law_benefit'])

        # PAYSLIPS FOR ACCUMULATED LAW BENEFITS
        for sub_ids in grouped_slice(ids_payslips_accum):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(law_benefit,
                condition=(table.id == law_benefit.payslip)
            ).join(tbl_period,
                condition=(tbl_period.id == law_benefit.period)
            ).select(
                table.id,
                law_benefit.id.as_('law_benefit'),
                where=red_sql,
                order_by=[tbl_period.end_date.desc]
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['law_benefit'])

        return {
            'law_benefits': result,
        }

    @classmethod
    def get_all_entries(cls, payslips, names=None, is_recalculate=False):
        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])
        # If you have not requested to re-calculate the payslip, the system
        # obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            cursor = Transaction().connection.cursor()
            Entry = pool.get('payslip.entry')
            PayslipRetiredManualEntry = pool.get('payslip.retired.manual.entry')
            entry = Entry.__table__()
            payslip_manual_entry = PayslipRetiredManualEntry.__table__()

            for payslip in payslips:
                # Get possible entries to consider in this payslip
                table_A = entry.select(
                    entry.employee,
                    entry.id.as_('entry'),
                    entry.kind,
                    entry.entry_date,
                    where=((entry.employee == payslip.employee.id) &
                           (entry.state == 'done') &
                           (entry.entry_date <= payslip.period.end_date)
                           & (entry.entry_date >= payslip.period.start_date)
                    )
                )
                # Get entries considered in other payslips
                table_B = payslip_manual_entry.join(entry,
                    condition=(entry.id == payslip_manual_entry.entry)
                ).select(
                    payslip_manual_entry.entry.as_('entry_aux'),
                    payslip_manual_entry.payslip,
                    where=((payslip_manual_entry.was_considered == 't') &
                           (entry.entry_date <= payslip.period.end_date)
                    )
                )
                # Get entries to consider in this payslip
                query = table_A.join(table_B, type_='LEFT OUTER',
                    condition=(table_A.entry == table_B.entry_aux)
                ).select(
                    table_A.entry,
                    table_A.kind,
                    where=((table_B.payslip == payslip.id) |
                           (table_B.entry_aux == Null)
                    )
                )
                # Execute
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    if row['kind'] == 'income':
                        incomes[payslip.id].append(row['entry'])
                    elif row['kind'] == 'deduction':
                        deductions[payslip.id].append(row['entry'])
        else:
            # Else, the system works with the information manipulated by the
            # user in the form.
            for payslip in payslips:
                for income_entry in payslip.income_entries:
                    incomes[payslip.id].append(income_entry.id)
                for deduction_entry in payslip.deduction_entries:
                    deductions[payslip.id].append(deduction_entry.id)

        return {
            'income_entries': incomes,
            'deduction_entries': deductions
        }

    @classmethod
    def get_considered_entries(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipRetiredManualEntry = pool.get('payslip.retired.manual.entry')
        table = cls.__table__()
        payslip_manual_entry = PayslipRetiredManualEntry.__table__()

        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_manual_entry,
                condition=(table.id == payslip_manual_entry.payslip)
            ).select(
                table.id,
                payslip_manual_entry.entry,
                payslip_manual_entry.kind,
                where=red_sql & (payslip_manual_entry.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['kind'] == 'income':
                    incomes[row['id']].append(row['entry'])
                elif row['kind'] == 'deduction':
                    deductions[row['id']].append(row['entry'])

        return {
            'income_entries': incomes,
            'deduction_entries': deductions,
        }

    @classmethod
    def get_manual_entries_amounts(cls, payslips, income_entries,
                deduction_entries):
        pool = Pool()
        Entry = pool.get('payslip.entry')
        EntryType = pool.get('payslip.entry.type')

        manual_entries_amounts = defaultdict(lambda: [])
        for payslip in payslips:
            entries = defaultdict(lambda: 0)
            # Union entries
            list_entries = []
            if income_entries:
                list_entries += income_entries[payslip.id]
            if deduction_entries:
                list_entries += deduction_entries[payslip.id]
            # Manual entries
            if list_entries:
                fields = get_fields_values(
                    Entry, list_entries, ['type_', 'real_amount', 'amount'])
                for field in fields:
                    code_entry = get_single_field_value(
                        EntryType, field['type_'], 'code')
                    amount = field['real_amount']
                    if code_entry not in entries:
                        entries[code_entry] = amount
                    else:
                        entries[code_entry] += amount
            # Results
            manual_entries_amounts[payslip.id] = entries
        return manual_entries_amounts

    @classmethod
    def set_deduction_entries(cls, payslips, name, value):
        pool = Pool()
        PayslipEntry = pool.get('payslip.entry')
        PayslipRetiredManualEntry = pool.get('payslip.retired.manual.entry')
        to_save = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    entry = PayslipEntry.search([('id', '=', id)])[0]
                    payslip_manual_entries = PayslipRetiredManualEntry.search([
                        ('payslip', '=', payslip),
                        ('entry', '=', entry)
                    ])
                    if operation == 'remove':
                        if entry in payslip.deduction_entries:
                            payslip_manual_entries[0].was_considered = False
                            to_save.append(payslip_manual_entries[0])
                    elif operation == 'add':
                        if len(payslip_manual_entries) > 0:
                            payslip_manual_entries[0].was_considered = True
                            to_save.append(payslip_manual_entries[0])
                        elif entry not in payslip.deduction_entries:
                            payslip_manual_entry = PayslipRetiredManualEntry()
                            payslip_manual_entry.payslip = payslip
                            payslip_manual_entry.entry = entry
                            payslip_manual_entry.kind = entry.kind
                            payslip_manual_entry.was_considered = True
                            to_save.append(payslip_manual_entry)
        PayslipRetiredManualEntry.save(to_save)

    @classmethod
    def manage_payslip_manual_entries(cls, payslip, income_entries,
            deduction_entries, considered_entries_codes):
        pool = Pool()
        PayslipRetiredManualEntry = pool.get('payslip.retired.manual.entry')
        Entry = pool.get('payslip.entry')

        list_entries = []
        if income_entries:
            list_entries += income_entries
        if deduction_entries:
            list_entries += deduction_entries

        # Create or update considered payslips manual entries
        to_save = []
        for entry in Entry.browse(list_entries):
            if entry.type_.code in considered_entries_codes:
                payslip_manual_entries = PayslipRetiredManualEntry.search([
                    ('entry', '=', entry)
                ])
                if payslip_manual_entries:
                    payslip_manual_entries[0].payslip = payslip
                    payslip_manual_entries[0].was_considered = True
                    to_save.append(payslip_manual_entries[0])
                else:
                    payslip_manual_entries = PayslipRetiredManualEntry()
                    payslip_manual_entries.payslip = payslip
                    payslip_manual_entries.entry = entry
                    payslip_manual_entries.kind = entry.kind
                    payslip_manual_entries.was_considered = True
                    to_save.append(payslip_manual_entries)

        # Set payslip deduction entries to not considered
        payslip_manual_entries = PayslipRetiredManualEntry.search([
            ('payslip', '=', payslip)
        ])
        for payslip_manual_entry in payslip_manual_entries:
            if payslip_manual_entry not in to_save:
                payslip_manual_entry.was_considered = False
                to_save.append(payslip_manual_entry)
        return {
            'to_save': to_save
        }

    @classmethod
    def get_new_law_benefit(cls, payslip, law_benefit_type, amount,
            law_benefit_kind=None, generator_payslip=None):
        LawBenefit = Pool().get('payslip.retired.law.benefit')
        benefit = LawBenefit()
        benefit.company = payslip.company
        benefit.employee = payslip.employee
        benefit.period = payslip.period
        benefit.amount = amount
        benefit.type_ = law_benefit_type
        if generator_payslip:
            benefit.generator_payslip = generator_payslip
        else:
            benefit.generator_payslip = payslip
        if law_benefit_kind:
            benefit.kind = law_benefit_kind
        else:
            benefit.kind = payslip.get_law_benefit_payment_form_by_field(
                law_benefit_type.code)
        return benefit

    @classmethod
    def get_new_payslip_line(cls, payslip, rule, type_, amount):
        Line = Pool().get('payslip.retired.line')
        pline = Line()
        pline.payslip = payslip
        pline.rule = rule
        pline.type_ = type_
        pline.amount = utils.quantize_currency(amount)
        return pline

    @classmethod
    def get_new_payslip_dict_code(cls, payslip, names):
        PayslipRetiredDictCodes = Pool().get('payslip.retired.dict.codes')
        payslip_dict_code = PayslipRetiredDictCodes()
        payslip_dict_code.payslip = payslip
        payslip_dict_code.description = str(
            PayslipRetiredDictCodes.get_dict_codes_instances(payslip, names))
        return payslip_dict_code

    @classmethod
    def get_manual_revision_color(cls, payslips, name=None):
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        result = {}

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.select(
                table.id,
                Case(
                    (table.manual_revision == 't', 'lightcoral'),
                    else_='lightgreen'),
                where=red_sql,
            )
            cursor.execute(*query)
            result.update(dict(cursor.fetchall()))
        return result

    def get_rec_name(self, name):
        return 'ROL %s: %s - %s' % (
            self.period.rec_name, self.template.rec_name,
            self.employee.party.name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('employee',) + tuple(clause[1:]),
            ('retired',) + tuple(clause[1:]),
            ('period',) + tuple(clause[1:]),
            ('template',) + tuple(clause[1:]),
            ]
        return domain


class PayslipRetiredLine(sequence_ordered(), ModelSQL, ModelView):
    'Payslip Retired Line'
    __name__ = 'payslip.retired.line'
    _history = True

    payslip = fields.Many2One('payslip.retired', 'Rol de pagos',
        required=True, ondelete='CASCADE',
        states={
            'readonly': True
        })
    rule = fields.Many2One('payslip.rule', 'Regla', required=True,
        domain=[
            ('company', '=', Eval('_parent_payslip', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        })
    type_ = fields.Selection(
        _PAYSLIP_ITEM_TYPES, 'Tipo', required=True, sort=False,
        states={
            'readonly': True
        })
    type_translated = type_.translated('type_')
    amount = fields.Numeric('Valor', required=True, digits=(16, 2),
        states={
            'readonly': True
        })
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    budget = fields.Many2One('public.budget', 'Partida')
    # Some extra fields of payslips for reports
    payslip_template = fields.Function(fields.Many2One('payslip.template',
        'Plantilla de rol'), 'get_payslip_field',
        searcher='search_payslip_field')
    payslip_employee = fields.Function(fields.Many2One('company.employee',
        'Empleado'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_retired = fields.Function(fields.Many2One('company.retired',
        'Jubilado'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_fiscalyear = fields.Function(fields.Many2One('account.fiscalyear',
        'Año fiscal'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_period = fields.Function(fields.Many2One('account.period',
        'Periodo'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_start_date = fields.Function(fields.Date('Fecha de inicio'),
        'get_payslip_field', searcher='search_payslip_field')
    payslip_end_date = fields.Function(fields.Date('Fecha de fin'),
        'get_payslip_field', searcher='search_payslip_field')
    payslip_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('confirm', 'Confirmado'),
            ('done', 'Realizado'),
            ('cancel', 'Cancelado'),
        ], 'Estado de rol'), 'get_payslip_field',
        searcher='search_payslip_field')
    payslip_state_translated = payslip_state.translated('payslip_state')

    @classmethod
    def __setup__(cls):
        super(PayslipRetiredLine, cls).__setup__()
        cls.order_payslip_template = cls._order_payslip_field('template')
        cls.order_payslip_retired = cls._order_payslip_field('retired')
        cls.order_payslip_fiscalyear = cls._order_payslip_field('fiscalyear')
        cls.order_payslip_period = cls._order_payslip_field('period')
        cls.order_payslip_start_date = cls._order_payslip_field('start_date')
        cls.order_payslip_end_date = cls._order_payslip_field('end_date')
        cls.order_payslip_state = cls._order_payslip_field('state')

    @staticmethod
    def default_amount():
        return ZERO

    def get_payslip_field(self, name):
        field = getattr(self.__class__, name)
        if name.startswith('payslip_'):
            name = name[8:]
        value = getattr(self.payslip, name)
        if isinstance(value, ModelSQL):
            if field._type == 'reference':
                return str(value)
            return value.id
        return value

    @classmethod
    def search_payslip_field(cls, name, clause):
        nested = clause[0].lstrip(name)
        if name.startswith('payslip_'):
            name = name[8:]
        return [('payslip.' + name + nested,) + tuple(clause[1:])]

    def _order_payslip_field(name):
        def order_field(tables):
            pool = Pool()
            PayslipRetired = pool.get('payslip.retired')
            field = PayslipRetired._fields[name]
            table, _ = tables[None]
            payslip_tables = tables.get('payslip')
            if payslip_tables is None:
                payslip = PayslipRetired.__table__()
                payslip_tables = {
                    None: (payslip, payslip.id == table.payslip),
                    }
                tables['payslip'] = payslip_tables
            return field.convert_order(name, payslip_tables, PayslipRetired)
        return staticmethod(order_field)

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('payslip_retired.rec_name',) + tuple(clause[1:]),
                ('payslip_period.rec_name',) + tuple(clause[1:]),
                ('payslip_template.rec_name',) + tuple(clause[1:]),
                ('rule',) + tuple(clause[1:]),
                ]


class PayslipRetiredLawBenefit(ModelSQL, ModelView):
    'Payslip Law Benefit'
    __name__ = 'payslip.retired.law.benefit'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], readonly=True, required=True)
    period = fields.Many2One('account.period', 'Periodo de rol generador',
        required=True, readonly=True)
    employee = fields.Many2One(
        'company.employee', 'Empleado jubilado', required=True,
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], readonly=True)
    payslip = fields.Many2One('payslip.retired', 'Rol en el que se pagó',
        readonly=True,
        domain=[
            ('employee', '=', Eval('employee')),
        ], depends=['employee'], ondelete='CASCADE')
    payslip_date = fields.Function(fields.Date('Fecha de rol de pagos'),
        'on_change_with_payslip_date')
    amount = fields.Numeric(
        'Valor', required=True, readonly=True, digits=(16, 10))
    kind = fields.Selection(
        [
            ('monthlyse', 'Mensualizado'),
            ('accumulate', 'Acumulado'),
            ('adjustment', 'Ajuste XIV'),
        ], 'Tipo', required=True, readonly=True)
    kind_translated = kind.translated('kind')
    type_ = fields.Many2One('payslip.law.benefit.type', 'Tipo', required=True,
        readonly=True, ondelete='RESTRICT')
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    budget = fields.Many2One('public.budget', 'Partida')
    # Payslip in which this law benefit was generated
    generator_payslip = fields.Many2One('payslip.retired',
        'Rol en el que se generó', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(PayslipRetiredLawBenefit, cls).__setup__()
        cls._error_messages.update({
            'no_delete': ('No puedes eliminar el "Beneficio de ley" debido a'
                ' que está siendo usado en la tabla de "Beneficios de ley" de '
                ' un "Rol de pagos de jubilación patronal".'),
            'duplicate_law_benefits_error': ('Los beneficios de ley no deben'
                ' duplicarse.')
        })
        cls._order.insert(0, ('generator_payslip.period.start_date', 'DESC'))
        cls._order.insert(1, ('employee.party.name', 'ASC'))
        cls._order.insert(2, ('type_.name', 'ASC'))

    @classmethod
    def delete(cls, benefits):
        if Transaction().context.get('from_payslip'):
            super(PayslipRetiredLawBenefit, cls).delete(benefits)
        else:
            cls.raise_user_error('no_delete')

    @classmethod
    def copy(cls, benefits, default=None):
        # The law benefits should not be duplicated
        for benefit in benefits:
            benefit.raise_user_error('duplicate_law_benefits_error')
        return []

    @fields.depends('payslip', 'period')
    def on_change_with_payslip_date(self, name=None):
        if self.payslip and self.period:
            return self.period.end_date
        return None

    def get_rec_name(self, name):
        str_payslip = "-"
        if self.payslip:
            str_payslip = self.payslip.rec_name
        return 'TIPO: %s ROL: %s' % (self.type_, str_payslip)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('type_',) + tuple(clause[1:]),
            ('payslip',) + tuple(clause[1:])
            ]
        return domain


class PayslipRetiredManualEntry(ModelSQL, ModelView):
    'Intermediate class between PayslipRetired and Manual Entries'
    __name__ = 'payslip.retired.manual.entry'
    _history = True

    payslip = fields.Many2One('payslip.retired', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    entry = fields.Many2One('payslip.entry', 'Entrada manual', required=True)
    kind = fields.Selection(_KIND, 'Tipo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipRetiredDictCodes(ModelView, ModelSQL):
    'PayslipRetired Dict Codes'
    __name__ = 'payslip.retired.dict.codes'

    payslip = fields.Many2One('payslip.retired', 'Rol individual',
        states={
            'readonly': True,
        }, ondelete='CASCADE')
    description = fields.Char('Descripción', readonly=True)

    @classmethod
    def __setup__(cls):
        super(PayslipRetiredDictCodes, cls).__setup__()

    @classmethod
    def get_dict_codes_instances(cls, payslip, dict):
        result = []
        if dict:
            for key, value in dict.items():
                dic_payslip_codes = []
                dic_payslip_codes.append(Null)
                dic_payslip_codes.append(0)
                dic_payslip_codes.append(Null)
                dic_payslip_codes.append(0)
                dic_payslip_codes.append(Null)

                # set payslip
                dic_payslip_codes.append(payslip.id)

                # set key
                dic_payslip_codes.append(key)

                # Set Value
                if isinstance(value, defaultdict):
                    dic_payslip_codes.append(cls.format_dictionaries(value))
                else:
                    dic_payslip_codes.append(str(value))

                # Set Type
                dic_payslip_codes.append(cls.format_type(dict[key]))

                result.append(dic_payslip_codes)
        return result

    @classmethod
    def format_type(cls, object):
        _str = (str(type(object)).replace("<class '", "")).replace("'>", "")
        return _str

    @classmethod
    def format_dictionaries(cls, dict):
        _str = ''
        for key, value in dict.items():
            _str = (f"{_str} '{key}': {value}, ")
        if len(_str) > 0:
            _str = _str[:-2]
        return f"{'{'}{_str}{'}'}"


class TemplatePayslipRetiredDictCodes(ModelView, ModelSQL):
    "Template - PayslipRetiredDictCodes"
    __name__ = 'template.payslip.retired.dict.codes'

    payslip = fields.Many2One('payslip.retired', 'Rol individual')

    code = fields.Char('Código', readonly=True)
    type = fields.Char('Tipo', readonly=True)
    value = fields.Char('Valor', readonly=True)

    @classmethod
    def __setup__(cls):
        super(TemplatePayslipRetiredDictCodes, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('type', 'ASC'))
        cls._order.insert(2, ('value', 'ASC'))

    @classmethod
    def table_query(cls):
        context = Transaction().context
        pool = Pool()
        PayslipRetiredDictCodes = pool.get('payslip.retired.dict.codes')

        columns = ['id', 'create_uid', 'create_date', 'write_uid', 'write_date',
            'payslip', 'code', 'value', 'type']

        # values = [[1, 0, Null, 0, Null, 1179, 'code', 'value', 'tipo'],
        #           [2, 0, Null, 0, Null, 1180, 'code1', 'value2', 'tipo2'],
        #           [3, 0, Null, 0, Null, 1179, 'code3', 'value3', 'tipo3']]
        values = []
        cont = 0
        if context.get('payslip'):
            payslip_dict_codes = PayslipRetiredDictCodes.search([
                ('payslip', '=', context['payslip'])])

            for payslipcode in payslip_dict_codes:
                array = ast.literal_eval(payslipcode.description)
                for fila, info in enumerate(array):
                    array[fila][0] = cont
                    cont += 1
                values += array

        if len(values) == 0:
            values = [[0, 0, Null, 0, Null, None, '', '', '']]

        data = With()
        data.columns = columns
        data.query = Values(values)
        query = data.select(with_=[data], order_by=data.code)

        return query
