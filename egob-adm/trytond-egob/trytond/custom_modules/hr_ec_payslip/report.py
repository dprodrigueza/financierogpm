from collections import OrderedDict, defaultdict
from decimal import Decimal
from calendar import different_locale, month_name, monthrange
from numbertoletters import number_to_letters

from sql.aggregate import Sum
from sql import Literal, Union, Window, Null

import unicodedata
from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.config import config as trytond_config
from trytond.tools import grouped_slice, reduce_ids, cursor_dict
from trytond.modules.hr_ec_payslip import utils

import pandas as pd
import numpy as np
import datetime 
from datetime import date
from unicodedata import normalize


__all__ = [
    'Contract', 'ContractLaborCertificate', 'Entry', 'Payslip', 'PayslipSummary',
    'PayslipGeneralDetail', 'PayslipPayslipGeneral',
    'PayslipPayslipGeneralSummary', 'GeneralEntry', 'ExtraIncomeExport',
    'ReserveFundsExport', 'OutputNoveltiesExport', 'DayNotWorkedExport',
    'NoticeInputExport', 'RetroactiveDifferenceExport', 'NoticeNewSalaryExport',
    'TransparencyMonthlyRemuneration', 'PayslipRuleCodes',
    'TransparencyMonthlyRemuneration', 'PayslipEntryExport',
    'RemunerationHistory', 'PayslipLineDetail',
    'PayslipPayslipGeneralSummaryEmail', 'EmailContentPayslipPaymentNotice',
    'EmailContentPayslipAdvancePaymentNotice',
    'EmailContentPayslipRetiredPaymentNotice', 'LaborMinistry',
    'LaborMinistryXIV',  'ReportContractLiquidation', 'LaborMinistryXIII',
    'PayslipGeneralResume', 'PayslipGeneralAdvanceDetail',
    'PayslipOvertimeLinesExport', 'PayslipAdvanceSummary',
    'PayslipGeneralAdvanceSummary', 'PayslipGeneralAdvanceResume',
    'PayslipGeneralTotalizedRules', 'ReportRulesTypePayslip',
    'PayslipIndividualTotalizedRulesAccounts',
    'PayslipIndividualTotalizedRules', 'PayslipIndividualProvisions',
    'LawBenefit', 'PayslipGeneralDetailRulesAdjustments', 'PayslipRetired',
    'PayslipRetiredSummary', 'PayslipGeneralRetired',
    'PayslipGeneralRetiredSummary', 'PayslipGeneralRetiredDetail',
    'PayslipRetiredIndividualTotalizedRules',
    'PayslipRetiredIndividualTotalizedRulesAccounts',
    'PayslipRetiredGeneralTotalizedRules',
    'PayslipRetiredGeneralTotalToPayEmployees',
    'PayslipRetiredIndividualProvisions',
    'PayslipGeneralRetiredDetailRulesAdjustments',
    'PayslipMedicalCertificateDiscountExport', 'PayslipSubsidiesExport',
    'ReportSettlementCertificate'
]


_CENT = Decimal('0.01')

GENERIC_DEPARTMENT_NAME = 'NO DEFINIDO'


def get_month_name(month_no, locale):
    with different_locale(locale) as encoding:
        s = month_name[month_no]
        if encoding is not None:
            s = s.decode(encoding)
        return s


def normalize_text(text):
    trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
    result = normalize('NFKC', normalize('NFKD', text).translate(trans_tab))
    return result.lower()


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode('utf-8')


def get_disease_detail(payslip):
    text_disease_detail = '-'
    med_certs_discount = []
    if payslip and payslip.total_days_disease and payslip.total_days_disease > 0:
        if not payslip.template_for_accumulated_law_benefits:
            med_certs_discount += list(
                payslip.medical_certificates_discount)
        else:
            law_benefits = payslip.law_benefits
            for lb in law_benefits:
                med_certs_discount += list(
                    lb.generator_payslip.medical_certificates_discount)

        if med_certs_discount:
            dict_med_cert = defaultdict(lambda: 0)
            for med_cert in med_certs_discount:
                certificate = med_cert.certificate_type.name
                percentage = med_cert.percentage_payslip * 100
                days = med_cert.number_days_month
                key = certificate + ' (' + str(percentage) + '%)'
                value = days
                if key not in dict_med_cert:
                    dict_med_cert[key] = value
                else:
                    dict_med_cert[key] += value

            list_med_cert = []
            for key, value in dict_med_cert.items():
                list_med_cert.append(key + ': ' + str(value) + '    ')
            if list_med_cert:
                list_med_cert.sort()
                text_disease_detail = ', '.join(list_med_cert)
    return text_disease_detail


def get_comm_subs_info_from_contract(contracts, start_date, end_date):
    pool = Pool()
    cursor = Transaction().connection.cursor()
    CommissionSubrogation = pool.get('company.commission.subrogation')
    tbl_comm_sub = CommissionSubrogation.__table__()
    dict_comm_subs = defaultdict(lambda: None)
    for contract in contracts:
        query = tbl_comm_sub.select(
            tbl_comm_sub.id,
            where=((tbl_comm_sub.contract_commission == contract.id)
                   & (tbl_comm_sub.start_date <= start_date)
                   & (tbl_comm_sub.end_date >= end_date)
                   & (tbl_comm_sub.state == 'done'))
        )
        cursor.execute(*query)
        ids_comm_subs = [row['id'] for row in cursor_dict(cursor)]
        result = get_comm_subs_detail(ids_comm_subs)
        if result:
            dict_comm_subs.update(result)
    return dict_comm_subs


def get_comm_subs_info(payslips):
    pool = Pool()
    cursor = Transaction().connection.cursor()
    CommissionSubrogation = pool.get('company.commission.subrogation')
    tbl_comm_sub = CommissionSubrogation.__table__()
    dict_comm_subs = defaultdict(lambda: None)
    for payslip in payslips:
        contract = payslip.contract
        consultation_date = payslip.period.end_date
        query = tbl_comm_sub.select(
            tbl_comm_sub.id,
            where=((tbl_comm_sub.contract_commission == contract.id)
                   & (tbl_comm_sub.start_date <= consultation_date)
                   & (tbl_comm_sub.end_date >= consultation_date)
                   & (tbl_comm_sub.state == 'done'))
        )
        cursor.execute(*query)
        ids_comm_subs = [row['id'] for row in cursor_dict(cursor)]
        result = get_comm_subs_detail(ids_comm_subs)
        if result:
            dict_comm_subs.update(result)
    return dict_comm_subs


def get_comm_subs_detail(ids_comm_subs):
    pool = Pool()
    CommissionSubrogation = pool.get('company.commission.subrogation')
    dict_comm_subs = defaultdict(lambda: None)
    for comm_sub in CommissionSubrogation.browse(ids_comm_subs):
        id_contract = comm_sub.contract_commission.id
        if id_contract not in dict_comm_subs:
            comm_sub_type = None
            comm_sub_name = comm_sub.position.name
            type_ = normalize_text(comm_sub.type_.name)

            # Name and type
            if (type_ in 'subrogacion') or ('subrogacion' in type_):
                comm_sub_name += ' (S)'
                comm_sub_type = 'subrogation'
            elif (type_ in 'encargo') or ('encargo' in type_):
                comm_sub_name += ' (E)'
                comm_sub_type = 'commission'

            # Situation actual and proposed
            action_staff = comm_sub.action_staff
            actual_situation = None
            proposed_situation = None
            if action_staff:
                if action_staff.actual_situation:
                    actual_situation = action_staff.actual_situation[0]
                if action_staff.proposed_situation:
                    proposed_situation = action_staff.proposed_situation[0]

            dict_comm_subs[id_contract] = {
                'comm_sub': comm_sub,
                'comm_sub_name': comm_sub_name,
                'comm_sub_type': comm_sub_type,
                'salary': comm_sub.salary_subrogation,
                'actual_situation': actual_situation,
                'proposed_situation': proposed_situation
            }
    return dict_comm_subs


def get_position_name(comm_sub_info, consider_commission=True,
        consider_subrogation=True):
    if comm_sub_info:
        contract = comm_sub_info['comm_sub'].contract_commission
        comm_sub_type = comm_sub_info['comm_sub_type']
        # Encargo
        if comm_sub_type == 'commission':
            if consider_commission:
                return comm_sub_info['comm_sub_name']
            else:
                return contract.position.name
        # Subrogacion
        elif comm_sub_type == 'subrogation':
            if consider_subrogation:
                return comm_sub_info['comm_sub_name']
            else:
                return contract.position.name
        else:
            return contract.position.name
    return None


def get_positions_info(payslips):
    # In the event that the employee has an commission, the job that will
    # be shown in the role report is the commissions and not the contract.
    # It is not the same for subrogations.
    positions = defaultdict(lambda: '')
    comm_sub_info = get_comm_subs_info(payslips)
    for payslip in payslips:
        contract = payslip.contract
        position_name = ''
        if payslip.position:
            position_name = payslip.position.name
        if comm_sub_info[contract.id]:
            item = comm_sub_info[contract.id]
            if item['comm_sub_type'] == 'commission':
                position_name = item['comm_sub_name']
        positions[payslip.id] = position_name
    return positions


def get_department(comm_sub_info, consider_commission=True,
        consider_subrogation=True):
    if comm_sub_info:
        contract = comm_sub_info['comm_sub'].contract_commission
        comm_sub_type = comm_sub_info['comm_sub_type']
        proposed_situation = comm_sub_info['proposed_situation']
        # Encargo
        if comm_sub_type == 'commission':
            if consider_commission and proposed_situation:
                return proposed_situation.department
            else:
                return contract.department
        # Subrogacion
        elif comm_sub_type == 'subrogation':
            if consider_subrogation and proposed_situation:
                return proposed_situation.department
            else:
                return contract.department
        else:
            return contract.department
    return None


def get_bank_accounts_info(payslips):
    bank_accounts = defaultdict(lambda: {
        'bank': '',
        'type': '',
        'number': '',
    })
    for payslip in payslips:
        party = payslip.employee.party
        bank_name = ''
        type = ''
        number = ''
        for bank_account in party.bank_accounts:
            bank_name = bank_account.bank.party.name
            type = bank_account.account_type_translated
            for account_number in bank_account.numbers:
                number = account_number.number
        bank_accounts[payslip.id].update({
            'bank': bank_name,
            'type': type,
            'number': number,
        })
    return bank_accounts


def get_template_account_codes(contracts):
    TemplateAccountLine = Pool().get('payslip.rule.account.budget')

    result = defaultdict(lambda: '-')
    templates_dict = defaultdict(lambda: [])

    for contract in contracts:
        template_account_budget = contract.payslip_template_account_budget
        if not templates_dict[template_account_budget]:
            templates_dict[template_account_budget] = [contract]
        else:
            templates_dict[template_account_budget].append(contract)

    for template_account_budget, contracts_values in templates_dict.items():
        if template_account_budget:
            lines = TemplateAccountLine.search([
                ('template_account_budget', '=', template_account_budget),
                ('payslip_rule.code', 'in', ['rmu', 'smu',
                                             'rmu_servicios_personales',
                                             'smu_servicios_personales'])
            ])
            if lines:
                for contract in contracts_values:
                    result[contract.id] = lines[0].budget.code
    return result


def normalize_rule_code(rule_code):
    rule_code = utils.normalize_text(rule_code)
    replacements = (
        (".", "_"),
        ("%", "_"),
        ("$", "_"),
        ("#", "_"),
        ("@", "_"),
        ("&", "_"),
        ("(", "_"),
        (")", "_"),
        ("[", "_"),
        ("]", "_"),
        ("{", "_"),
        ("}", "_"),
    )
    for a, b in replacements:
        rule_code = rule_code.replace(a, b)
    return rule_code


def _get_departments(payslip_model, general):
    pool = Pool()
    PayslipModel = pool.get(payslip_model)
    Department = pool.get('company.department')
    departments_data = PayslipModel.search_read([
            ('general', '=', general.id)
        ],
        fields_names=['department'],
        order=[('department.name', 'ASC')])
    departments = []
    departments_ids = []
    for department_ in departments_data:
        d_id = department_['department']
        if d_id not in departments_ids:
            departments_ids.append(d_id)
            if d_id == None:
                # Generic department to show it in the report
                department = Department()
                department.name = GENERIC_DEPARTMENT_NAME
                department.rec_name = GENERIC_DEPARTMENT_NAME
            else:
                department = Department(d_id)
            departments.append(department)
    return departments


def get_report_header(general, internal_rules=False, accounts=False,
        rule_adjustments=False, include_template_rules=False):
    # Can be used on monthly payslips, biweekly payslips and payslip retired
    # reports
    header = OrderedDict()
    rules = []
    income_rules = []
    deduction_rules = []
    internal_lines = []
    template = general.template

    # Get all rules from payslip
    for payslip in general.lines:
        for line in payslip.lines:
            if line.rule not in rules:
                rules.append(line.rule)

    # Get all rules from template
    if include_template_rules:
        for line in template.lines:
            if line.rule not in rules:
                rules.append(line.rule)

    # Sort according to template
    rules = get_sorted_rules_according_template(rules, template)

    # Separate rules
    for rule in rules:
        if rule.type_ == 'income':
            if rule not in income_rules:
                income_rules.append(rule)
        elif rule.type_ == 'deduction':
            if rule not in deduction_rules:
                deduction_rules.append(rule)
        elif rule.type_ == 'internal':
            if internal_rules:
                if rule not in internal_lines:
                    internal_lines.append(rule)

    # FIRST INCOME RULES
    for rule in income_rules:
        rule_code = normalize_rule_code(rule.code)
        rule_name = (rule.abbreviation or rule.name)
        header[rule_code] = rule_name
        if rule_adjustments:
            header[f'adjust_rule_{rule_code}'] = rule_name
            header[f'adjust_income_{rule_code}'] = 'AJUSTES DE INGRESO'
            header[f'adjust_deduction_{rule_code}'] = 'AJUSTES DE DEDUCCION'
        if accounts:
            header[f'{rule_code}_payable'] = 'CUENTA POR PAGAR'
            header[f'{rule_code}_expense'] = 'CUENTA DE GASTO'
    header['total_income'] = 'TOTAL INGRESOS'

    # SECOND DEDUCTION RULES
    for rule in deduction_rules:
        rule_code = normalize_rule_code(rule.code)
        rule_name = (rule.abbreviation or rule.name)
        header[rule_code] = rule_name
        if rule_adjustments:
            header[f'adjust_rule_{rule_code}'] = rule_name
            header[f'adjust_income_{rule_code}'] = 'AJUSTES DE INGRESO'
            header[f'adjust_deduction_{rule_code}'] = 'AJUSTES DE DEDUCCION'
        if accounts:
            header[f'{rule_code}_payable'] = 'CUENTA POR PAGAR'
            header[f'{rule_code}_expense'] = 'CUENTA DE GASTO'
    header['total_deduction'] = 'TOTAL EGRESOS'

    # THIRD INTERNAL RULES
    if internal_rules:
        for rule in internal_lines:
            rule_code = normalize_rule_code(rule.code)
            rule_name = (rule.abbreviation or rule.name)
            header[rule_code] = rule_name
            if rule_adjustments:
                header[f'adjust_rule_{rule_code}'] = rule_name
                header[f'adjust_income_{rule_code}'] = 'AJUSTES DE INGRESO'
                header[f'adjust_deduction_{rule_code}'] = 'AJUSTES DE DEDUCCION'
            if accounts:
                header[f'{rule_code}_payable'] = 'CUENTA POR PAGAR'
                header[f'{rule_code}_expense'] = 'CUENTA DE GASTO'
        header['total_internal'] = 'TOTAL INTERNAS'

    # LAST IS THE TOTAL
    header['total_value'] = 'TOTAL'
    return header



def get_report_lines(general, department, is_retired_payslip=False,
        accounts=False, rule_adjustments=False):
    # Can be used on monthly payslips and payslip retired reports
    payslips = []
    lines = []
    totals = defaultdict(lambda: Decimal('0.0'))
    totals['name'] = 'TOTALES'

    payslips = group_payslips_by_department(general.lines, department)
    bank_accounts = get_bank_accounts_info(payslips)
    if not is_retired_payslip:
        positions = get_positions_info(payslips)

    for payslip in payslips:
        items = defaultdict(lambda: '')
        # General data
        items['name'] = (f"{payslip.employee.party.name}")
        items['identifier'] = payslip.employee.identifier
        items['bank_account_bank'] = bank_accounts[payslip.id]['bank']
        items['bank_account_type'] = bank_accounts[payslip.id]['type']
        items['bank_account_number'] = bank_accounts[payslip.id]['number']
        items['total_internal'] = Decimal('0.00')

        if is_retired_payslip:
            # Data for payslip.retired instances
            items['management_start_date'] = payslip.management_start_date
            items['management_end_date'] = payslip.management_end_date
            items['service_years'] = payslip.param_service_years
        else:
            # Data for payslip.payslip instances
            items['position_name'] = positions[payslip.id]
            items['contract_type_name'] = (f"{payslip.contract_type.name}")
            items['worked_days_with_normative'] = (
                payslip.worked_days_with_normative)

        for line in payslip.lines:
            # Rule amount
            rule_code = normalize_rule_code(line.rule.code)
            items[rule_code] = line.amount

            # Total
            totals[rule_code] += line.amount

            # Rule adjustments information
            if rule_adjustments:
                set_rule_adjustments_info_on_report_lines(
                    line, rule_code, items, totals)

            # Accounts information
            if accounts:
                set_accounts_info_on_report_lines(
                    line, rule_code, items)

            # Total internal
            if line.type_ == 'internal':
                items['total_internal'] += line.amount

        items['total_income'] = payslip.total_income
        items['total_deduction'] = payslip.total_deduction
        items['total_value'] = payslip.total_value
        lines.append(items)
        totals['total_income'] += payslip.total_income
        totals['total_deduction'] += payslip.total_deduction
        totals['total_internal'] += items['total_internal']
        totals['total_value'] += payslip.total_value
        totals['identifier'] = ''
    lines.append(totals)
    return lines


def set_rule_adjustments_info_on_report_lines(p_line, rule_code, items, totals):
    payslip = p_line.payslip
    # Adjustment income
    adjust_income = 0
    for entry in payslip.income_entries:
        entry_type = entry.type_
        if entry_type.is_adjustment_entry and entry_type.rule_to_adjust:
            if entry_type.rule_to_adjust == p_line.rule:
                adjust_income += entry.real_amount

    # Adjustment deduction
    adjust_deduction = 0
    for entry in payslip.deduction_entries:
        entry_type = entry.type_
        if entry_type.is_adjustment_entry and entry_type.rule_to_adjust:
            if entry_type.rule_to_adjust == p_line.rule:
                adjust_deduction += entry.real_amount

    # Rule and adjustment values
    adjust_line_amount = p_line.amount - adjust_income + adjust_deduction
    items[f'adjust_rule_{rule_code}'] = adjust_line_amount
    items[f'adjust_income_{rule_code}'] = adjust_income
    items[f'adjust_deduction_{rule_code}'] = adjust_deduction

    # Totals
    totals[f'adjust_rule_{rule_code}'] += adjust_line_amount
    totals[f'adjust_income_{rule_code}'] += adjust_income
    totals[f'adjust_deduction_{rule_code}'] += adjust_deduction



def set_accounts_info_on_report_lines(p_line, rule_code, items):
    # Payable account
    payable = p_line.payable_account
    str_payable = payable.code if payable else ''
    items[f'{rule_code}_payable'] = str_payable

    # Expense account
    expense = p_line.expense_account
    str_expense = expense.code if expense else ''
    items[f'{rule_code}_expense'] = str_expense



def get_report_general_totals(departments, _lines):
    # Can be used on monthly payslips, biweekly payslips and retired payslips
    # reports
    totals = defaultdict(lambda: Decimal('0.00'))
    for department in departments:
        lines = _lines[department.id]
        if lines:
            # The following for crosses the lines except the last one that
            # corresponds to the line of TOTALS
            for row in (lines[0:len(lines) - 1]):
                for code, value in row.items():
                    if (isinstance(value, Decimal) or
                            isinstance(value, float) or
                                isinstance(value, int)):
                        amount = Decimal(str(value))
                        if code not in totals:
                            totals[code] = amount
                        else:
                            totals[code] += amount
    return totals


# PAYSLIP ADVANCE
def get_payslip_advance_report_lines(general, department):
    lines = []
    totals = defaultdict(lambda: Decimal('0.0'))
    totals['name'] = 'TOTALES'
    totals['position_name'] = ''

    payslips = group_payslips_by_department(general.lines, department)
    positions = get_positions_info(payslips)

    for payslip in payslips:
        items = defaultdict(lambda: '')
        items['name'] = (f"{payslip.employee.party.name}")
        items['position_name'] = positions[payslip.id]

        for line in payslip.lines:
            rule_code = normalize_rule_code(line.rule.code)
            items[rule_code] = line.amount
            totals[rule_code] += line.amount

        items['total_income'] = payslip.total_income
        items['total_deduction'] = payslip.total_deduction
        items['total_value'] = payslip.total_value
        items['identifier'] = payslip.employee.identifier
        lines.append(items)
        totals['total_income'] += payslip.total_income
        totals['total_deduction'] += payslip.total_deduction
        totals['total_value'] += payslip.total_value
        totals['identifier'] = ''
    lines.append(totals)
    return lines


# TOTALIZED RULES VALUES
def get_report_header_totalized_rules(generals):
    header = OrderedDict()
    rules = []
    income_rules = []
    deduction_rules = []
    internal_rules = []

    template = None
    if generals:
        template = generals[0].template

    for general in generals:
        for payslip in general.lines:
            for line in payslip.lines:
                if line.rule not in rules:
                    rules.append(line.rule)

    # Sort according to template
    rules = get_sorted_rules_according_template(rules, template)

    for rule in rules:
        if rule.type_ == 'income':
            income_rules.append(rule)
        elif rule.type_ == 'deduction':
            deduction_rules.append(rule)
        elif rule.type_ == 'internal':
            internal_rules.append(rule)

    # FIRST INCOME RULES
    for rule in income_rules:
        rule_code = normalize_rule_code(rule.code)
        header[rule_code] = rule.name
    header['total_income'] = 'TOTAL INGRESOS'

    # SECOND DEDUCTION RULES
    for rule in deduction_rules:
        rule_code = normalize_rule_code(rule.code)
        header[rule_code] = (rule.abbreviation or rule.name)
    header['total_deduction'] = 'TOTAL EGRESOS'

    # THIRD INTERNAL RULES
    for rule in internal_rules:
        rule_code = normalize_rule_code(rule.code)
        header[rule_code] = (rule.abbreviation or rule.name)
    header['total_internal'] = 'TOTAL INTERNAS'

    # LAST IS THE TOTAL
    header['total_value'] = 'TOTAL'
    return header


def get_report_totals_totalized_rules(generals):
    result = defaultdict(lambda: None)
    for general in generals:
        totals = defaultdict(lambda: Decimal('0.00'))
        totals['total_income'] = Decimal('0.00')
        totals['total_deduction'] = Decimal('0.00')
        totals['total_internal'] = Decimal('0.00')
        totals['total_value'] = Decimal('0.00')
        for payslip in general.lines:
            for payslip_line in payslip.lines:
                rule = payslip_line.rule
                rule_code = normalize_rule_code(rule.code)
                amount = payslip_line.amount
                if rule_code not in totals:
                    totals[rule_code] = amount
                else:
                    totals[rule_code] += amount
                if rule.type_ == 'income':
                    totals['total_income'] += amount
                elif rule.type_ == 'deduction':
                    totals['total_deduction'] += amount
                elif rule.type_ == 'internal':
                    totals['total_internal'] += amount
        totals['total_value'] = (
                totals['total_income'] - totals['total_deduction'])
        result[general.id] = totals
    return result


def get_report_general_totals_totalized_rules(generals, header, totals):
    general_totals = defaultdict(lambda: Decimal('0.00'))
    if generals and header and totals:
        for rule_code, rule_name in header.items():
            general_totals[rule_code] = Decimal('0.00')
            for general in generals:
                amount = totals[general.id][rule_code]
                general_totals[rule_code] += amount
    return general_totals


# PROVISIONS REPORT
def get_report_header_provisions(general):
    # Can be used on monthly payslips and retired payslips reports
    pool = Pool()
    LawBenefitType = pool.get('payslip.law.benefit.type')

    header = OrderedDict()
    rules = []
    template = general.template

    # Get all internal rules from payslip
    for payslip in general.lines:
        for line in payslip.lines:
            if line.type_ == 'internal':
                if (line.rule.is_law_benefit and
                        line.rule.law_benefit_type and
                            line.rule.law_benefit_type.code == 'fr'):
                    continue
                else:
                    if line.rule not in rules:
                        rules.append(line.rule)

    # Get all rules from template
    for line in template.lines:
        if line.rule.type_ == 'internal':
            if (line.rule.is_law_benefit and
                    line.rule.law_benefit_type and
                        line.rule.law_benefit_type.code == 'fr'):
                continue
            else:
                if line.rule not in rules:
                    rules.append(line.rule)

    # Sort according to template
    internal_rules = get_sorted_rules_according_template(rules, template)

    # Get all law benefit types
    law_benefit_types = LawBenefitType.search([])

    # FIRST LAW BENEFIT TYPES
    for law_benefit_type in law_benefit_types:
        lb_type_code = normalize_rule_code(law_benefit_type.code)
        header[lb_type_code] = f'{law_benefit_type.name.upper()} ACUMULADO'
    header['total_law_benefits'] = 'TOTAL BENEFICIOS SOCIALES'

    # SECOND INTERNAL RULES
    for rule in internal_rules:
        rule_code = normalize_rule_code(rule.code)
        header[rule_code] = (rule.abbreviation or rule.name)
    header['total_internal'] = 'TOTAL INTERNAS'

    # LAST IS THE TOTAL
    header['total_value'] = 'TOTAL'
    return header


def get_report_lines_provisions(general, department, is_retired_payslip=False):
    # Can be used on monthly payslips and retired payslips reports
    payslips = []
    lines = []
    totals = defaultdict(lambda: Decimal('0.0'))
    positions = defaultdict(lambda: '')
    totals['name'] = 'TOTALES'

    payslips = group_payslips_by_department(general.lines, department)
    if not is_retired_payslip:
        positions = get_positions_info(payslips)

    for payslip in payslips:
        items = defaultdict(lambda: '')
        # General data
        items['name'] = (f"{payslip.employee.party.name}")
        items['identifier'] = payslip.employee.identifier
        items['total_internal'] = Decimal('0.00')
        items['total_law_benefits'] = Decimal('0.00')

        if is_retired_payslip:
            # Data for payslip.retired instances
            items['management_start_date'] = payslip.management_start_date
            items['management_end_date'] = payslip.management_end_date
            items['service_years'] = payslip.param_service_years
        else:
            # Data for payslip.payslip instances
            items['position_name'] = positions[payslip.id]
            items['contract_type_name'] = (f"{payslip.contract_type.name}")

        # Internal Lines
        for line in payslip.lines:
            if line.rule.type_ == 'internal':
                if (line.rule.is_law_benefit and
                        line.rule.law_benefit_type and
                            line.rule.law_benefit_type.code == 'fr'):
                    continue
                else:
                    # Rule amount
                    rule_code = normalize_rule_code(line.rule.code)
                    items[rule_code] = line.amount

                    # Total by rule
                    totals[rule_code] += line.amount

                    # Total general internal
                    items['total_internal'] += line.amount

        # Law Benefit Lines
        for line in payslip.law_benefits:
            if line.kind == 'accumulate':
                # Law benefit amount
                lb_code = normalize_rule_code(line.type_.code)
                items[lb_code] = line.amount

                # Total by rule
                totals[lb_code] += line.amount

                # Total general internal
                items['total_law_benefits'] += line.amount

        # Totals by lines
        items['total_value'] = (
                items['total_internal'] + items['total_law_benefits'])
        lines.append(items)

        # Total generals
        totals['total_internal'] += items['total_internal']
        totals['total_law_benefits'] += items['total_law_benefits']
        totals['total_value'] += (
                items['total_internal'] + items['total_law_benefits'])
    lines.append(totals)
    return lines


def get_payslip_dict_code_value(payslip, code):
    DictCodes = Pool().get('template.payslip.dict.codes')
    if payslip and code:
        with Transaction().set_context(payslip=payslip.id):
            dict_codes = DictCodes.search([
                ('payslip', '=', payslip),
                ('code', '=', code)
            ])
            if dict_codes:
                return dict_codes[0].value
    return None


def get_sorted_rules_according_template(rules, template):
    if template:
        max_1 = 100000
        max_2 = 1000000
        rules_tuple = []
        template_rules = [l.rule for l in template.lines]
        template_sequence_rules = [(l.sequence, l.rule) for l in template.lines]
        for rule in rules:
            try:
                pos = template_rules.index(rule)
                _seq, _rule = template_sequence_rules[pos]
                if not _seq:
                    _seq = max_1
                    max_1 += 1
                rules_tuple.append((_seq, _rule))
            except:
                rules_tuple.append((max_2, rule))
                max_2 += 1
        # Sort
        rules_tuple.sort(key=lambda x: x[0])
        sorted_rules = [rt[1] for rt in rules_tuple]
        return sorted_rules
    return rules


def group_payslips_by_department(payslips, department):
    grouped_payslips = []
    if payslips:
        for payslip in payslips:
            if department.id == None:
                if payslip.department == None:
                    grouped_payslips.append(payslip)
            else:
                if payslip.department == department:
                    grouped_payslips.append(payslip)
    return grouped_payslips


class Contract(CompanyReportSignature):
    __name__ = 'company.contract'


class ContractLaborCertificate(CompanyReportSignature):
    __name__ = 'company.contract.labor.certificate'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            ContractLaborCertificate, cls).get_context(records, data)

        # Today info
        now = datetime.date.today()
        moth_name = get_month_name(now.month, 'es_EC.UTF-8')
        str_today = f"{now.day} de {moth_name} del {now.year}"

        # Commission/subrogation info
        comm_subs = cls.get_comm_sub_info(records, now)

        # Deprecated context variable but used on some GADs yet
        dict_department_commission = cls.get_dict_department_commission(records)

        letters = number_to_letters(records[0].salary)
        month_start_date = get_month_name(
            records[0].start_date.month, 'es_EC.UTF-8')
        month_end_date = ''
        if records[0].end_date:
            month_end_date = get_month_name(
                records[0].end_date.month, 'es_EC.UTF-8')

        report_context['letters'] = letters
        report_context['month_start_date'] = month_start_date
        report_context['month_end_date'] = month_end_date
        report_context['today'] = str_today
        report_context['dict_department_commission'] = dict_department_commission #noqa
        report_context['comm_sub'] = comm_subs
        report_context['department_name'] = cls._get_department_name
        report_context['position_name'] = cls._get_position_name
        return report_context

    @classmethod
    def get_dict_department_commission(cls, records):
        CommissionSubrogation = Pool().get('company.commission.subrogation')
        now = datetime.date.today()
        dict_department_commission = defaultdict(lambda: 0)
        for contract in records:
            commi_sub = CommissionSubrogation.search([
                ('start_date', '<=', now),
                ('end_date', '>=', now),
                ['OR',
                    [('contract_commission', '=', contract.id)]
                ],
            ])
            if commi_sub:
                dict_department_commission[contract.employee.identifier] = (
                    commi_sub[0])
        return dict_department_commission

    @classmethod
    def get_comm_sub_info(cls, records, on_date, consider_commission=True,
            consider_subrogation=False):
        result = defaultdict(lambda: None)
        comm_subs = get_comm_subs_info_from_contract(records, on_date, on_date)
        for contract_id, values in comm_subs.items():
            comm_sub_type = values['comm_sub_type']
            if comm_sub_type == 'commission':
                if consider_commission:
                    result[contract_id] = values
            elif comm_sub_type == 'subrogation':
                if consider_subrogation:
                    result[contract_id] = values
        return result

    @classmethod
    def _get_department_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        department = get_department(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        department_name = department.name if department else ''
        return department_name

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        position_name = get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        position_name = position_name if position_name else ''
        return position_name


class Entry(CompanyReportSignature):
    __name__ = 'payslip.entry'


class Payslip(CompanyReportSignature):
    'Rol de pago detallado (Rol individual)'
    __name__ = 'payslip.payslip'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(Payslip, cls).get_context(records, data)

        dates = cls.get_dates(records)
        positions = get_positions_info(records)

        report_context['positions'] = positions
        report_context['start_date'] = dates['start_date']
        report_context['end_date'] = dates['end_date']
        report_context['overtimes'] = dates['overtimes']
        report_context['effective_worked_days'] = cls._get_effective_worked_days
        report_context['dict_code_value'] = cls._get_dict_code_value
        report_context['expedition_date'] = datetime.date.today()

        return report_context

    @classmethod
    def _get_effective_worked_days(cls, payslip):
        value = get_payslip_dict_code_value(payslip, 'effective_worked_days')
        return f'{value}d' if value else '-'

    @classmethod
    def _get_dict_code_value(cls, payslip, code):
        value = get_payslip_dict_code_value(payslip, code)
        return f'{value}' if value else ''

    @classmethod
    def get_dates(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        overtimes = []
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        for payslip_id in payslips:
            payslip = Payslip.search([
                ('id', '=', payslip_id.id)
            ])[0]
            if payslip.template_for_accumulated_law_benefits:
                start_date[payslip.id] = payslip.start_date
                end_date[payslip.id] = payslip.end_date
            else:
                start_date[payslip.id] = payslip.period.start_date
                end_date[payslip.id] = payslip.period.end_date
            if payslip.overtimes:
                OvertimeLine = Pool().get('hr_ec.overtime.line')
                lines = OvertimeLine.search([
                    ('id', 'in', [l.id for l in payslip.overtimes]),
                ], order=[('overtime_date', 'ASC')])
                for line in lines:
                    overtimes.append(line)
        return {
            'start_date': start_date,
            'end_date': end_date,
            'overtimes': overtimes
        }


class PayslipSummary(CompanyReportSignature):
    'Aviso de pago (Rol individual)'
    __name__ = 'payslip.payslip.summary'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipSummary, cls).get_context(records, data)

        dates = cls.get_dates(records)
        positions = get_positions_info(records)

        report_context['positions'] = positions
        report_context['start_date'] = dates['start_date']
        report_context['end_date'] = dates['end_date']
        report_context['accumulated_fr'] = dates['accumulated_fr']
        report_context['effective_worked_days'] = cls._get_effective_worked_days
        report_context['disease_detail'] = cls._get_disease_detail
        report_context['dict_code_value'] = cls._get_dict_code_value
        report_context['expedition_date'] = datetime.date.today()

        return report_context

    @classmethod
    def _get_disease_detail(cls, payslip):
        return get_disease_detail(payslip)

    @classmethod
    def _get_effective_worked_days(cls, payslip):
        value = get_payslip_dict_code_value(payslip, 'effective_worked_days')
        return f'{value}d' if value else '-'

    @classmethod
    def _get_dict_code_value(cls, payslip, code):
        value = get_payslip_dict_code_value(payslip, code)
        return f'{value}' if value else '-'

    @classmethod
    def get_dates(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        accumulated_fr = defaultdict(lambda: None)
        for payslip_id in payslips:
            payslip = Payslip.search([
                ('id', '=', payslip_id.id)
            ])[0]
            if payslip.template_for_accumulated_law_benefits:
                start_date[payslip.id] = payslip.start_date
                end_date[payslip.id] = payslip.end_date
            else:
                start_date[payslip.id] = payslip.period.start_date
                end_date[payslip.id] = payslip.period.end_date

            for line in payslip.internal_lines:
                if line.rule.is_law_benefit and (
                        line.rule.law_benefit_type.code in ['fr']):
                    accumulated_fr[payslip.id] = line
                    break
        return {
            'accumulated_fr': accumulated_fr,
            'start_date': start_date,
            'end_date': end_date
        }


class PayslipGeneralResume(CompanyReportSignature):
    __name__ = 'payslip.general.resume'


class PayslipGeneralDetail(CompanyReportSignature):
    'Sábana (Rol general)'
    __name__ = 'payslip.general'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralDetail, cls).get_context(
            records, data)

        # HEADER TYPES
        # Includes only rules (income, deduction) that are part of payslips and
        # they are not necessarily part of the payslip template
        header = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # and they are not necessarily part of the payslip template
        header_with_internals = defaultdict(lambda: {})
        # Includes rules (income, deduction) that are part of payslips plus
        # those that are in the payslip template
        header_all_rules = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # plus those that are in the payslip template
        header_all_rules_with_internals = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            header_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True)
            header_all_rules[record.id] = cls._get_report_header(
                record,
                include_template_rules=True)
            header_all_rules_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True,
                include_template_rules=True)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['header_with_internals'] = header_with_internals
        report_context['header_all_rules'] = header_all_rules
        report_context['header_all_rules_with_internals'] = (
            header_all_rules_with_internals)
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        report_context['expedition_date'] = datetime.date.today()
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general, internal_rules=False, accounts=False,
            include_template_rules=False):
        return get_report_header(
            general,
            internal_rules=internal_rules,
            accounts=accounts,
            include_template_rules=include_template_rules)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class PayslipGeneralDetailRulesAdjustments(CompanyReportSignature):
    'Sábana con rubros y ajustes'
    __name__ = 'payslip.general.rules.adjustments'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipGeneralDetailRulesAdjustments, cls).get_context(
                records, data)

        # HEADER TYPES
        # Includes only rules (income, deduction) that are part of payslips
        header = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        header_with_internals = defaultdict(lambda: {})
        # Includes rules (income, deduction) that are part of payslips plus
        # those that are in the payslip template
        header_all_rules = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # plus those that are in the payslip template
        header_all_rules_with_internals = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            header_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True)
            header_all_rules[record.id] = cls._get_report_header(
                record,
                include_template_rules=True)
            header_all_rules_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True,
                include_template_rules=True)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['header_with_internals'] = header_with_internals
        report_context['header_all_rules'] = header_all_rules
        report_context['header_all_rules_with_internals'] = (
            header_all_rules_with_internals)
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        report_context['expedition_date'] = datetime.date.today()
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general, internal_rules=False, accounts=False,
            include_template_rules=False):
        return get_report_header(
            general,
            internal_rules=internal_rules,
            accounts=accounts,
            rule_adjustments=True,
            include_template_rules=include_template_rules)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, rule_adjustments=True)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class PayslipPayslipGeneral(CompanyReportSignature):
    'Rol de pagos: Detallado (Rol general)'
    __name__ = 'payslip.payslip.general'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipPayslipGeneral, cls).get_context(
            records[0].lines, data)
        dates = cls.get_dates(records[0].lines)
        positions = get_positions_info(records[0].lines)

        report_context['positions'] = positions
        report_context['start_date'] = dates['start_date']
        report_context['end_date'] = dates['end_date']
        report_context['effective_worked_days'] = cls._get_effective_worked_days
        report_context['dict_code_value'] = cls._get_dict_code_value
        report_context['expedition_date'] = datetime.date.today()

        return report_context

    @classmethod
    def _get_effective_worked_days(cls, payslip):
        value = get_payslip_dict_code_value(payslip, 'effective_worked_days')
        return f'{value}d' if value else '-'

    @classmethod
    def _get_dict_code_value(cls, payslip, code):
        value = get_payslip_dict_code_value(payslip, code)
        return f'{value}' if value else ''

    @classmethod
    def get_dates(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        for payslip_id in payslips:
            payslip = Payslip.search([
                ('id', '=', payslip_id.id)
            ])[0]
            if payslip.template_for_accumulated_law_benefits:
                start_date[payslip.id] = payslip.start_date
                end_date[payslip.id] = payslip.end_date
            else:
                start_date[payslip.id] = payslip.period.start_date
                end_date[payslip.id] = payslip.period.end_date
        return {
            'start_date': start_date,
            'end_date': end_date
        }


class PayslipPayslipGeneralSummary(CompanyReportSignature):
    'Rol de pagos: Aviso de pago (Rol general)'
    __name__ = 'payslip.payslip.general.summary'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        lines_to_print = Payslip.search([
            ('general', '=', records[0].id),
        ], order=[
            ('contract.department.name', 'ASC'),
            ('employee.party.name', 'ASC')])
        report_context = super(PayslipPayslipGeneralSummary, cls).get_context(
            lines_to_print, data)

        dates = cls.get_dates(lines_to_print)
        positions = get_positions_info(lines_to_print)

        report_context['positions'] = positions
        report_context['start_date'] = dates['start_date']
        report_context['end_date'] = dates['end_date']
        report_context['accumulated_fr'] = dates['accumulated_fr']
        report_context['effective_worked_days'] = cls._get_effective_worked_days
        report_context['disease_detail'] = cls._get_disease_detail
        report_context['dict_code_value'] = cls._get_dict_code_value
        report_context['expedition_date'] = datetime.date.today()
        return report_context

    @classmethod
    def _get_disease_detail(cls, payslip):
        return get_disease_detail(payslip)

    @classmethod
    def _get_effective_worked_days(cls, payslip):
        value = get_payslip_dict_code_value(payslip, 'effective_worked_days')
        return f'{value}d' if value else '-'

    @classmethod
    def _get_dict_code_value(cls, payslip, code):
        value = get_payslip_dict_code_value(payslip, code)
        return f'{value}' if value else ''

    @classmethod
    def get_dates(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        start_date = defaultdict(lambda: None)
        end_date = defaultdict(lambda: None)
        accumulated_fr = defaultdict(lambda: None)
        for payslip_id in payslips:
            payslip = Payslip.search([
                ('id', '=', payslip_id.id)
            ])[0]
            if payslip.template_for_accumulated_law_benefits:
                start_date[payslip.id] = payslip.start_date
                end_date[payslip.id] = payslip.end_date
            else:
                start_date[payslip.id] = payslip.period.start_date
                end_date[payslip.id] = payslip.period.end_date

            for line in payslip.internal_lines:
                if line.rule.is_law_benefit and (
                        line.rule.law_benefit_type.code in ['fr']):
                    accumulated_fr[payslip.id] = line
                    break
        return {
            'accumulated_fr': accumulated_fr,
            'start_date': start_date,
            'end_date': end_date
        }


class GeneralEntry(CompanyReportSignature):
    __name__ = 'payslip.general.entry'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(GeneralEntry, cls).get_context(records, data)
        report_context['totals'] = cls._get_totals
        return report_context

    @classmethod
    def _get_totals(cls, general_lines):
        total = 0
        for l in general_lines:
            total += l.amount
        return total


class ExtraIncomeExport(CompanyReportSignature):
    __name__ = 'extra.income.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ExtraIncomeExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')

        company = Company.search([('id', '=', data['company'])])[0]
        company_ruc = company.party.identifiers[0].code

        report_context['company_ruc'] = company_ruc
        report_context['extra_incomes'] = data['extra_incomes']

        cls.set_context_config_fields(report_context, data['config'])

    @classmethod
    def set_context_config_fields(cls, report_context, config):
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['movement_type'] = config.get('movement_type', '')
        report_context['cause'] = config.get('cause', '')


class ReserveFundsExport(CompanyReportSignature):
    __name__ = 'reserve.funds.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReserveFundsExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]

        company_ruc = company.party.identifiers[0].code

        current_year = str(fiscalyear.start_date.year)
        current_month = str(period.start_date.month).rjust(2, '0')

        start_date = data.get('start_date', None)
        end_date = data.get('end_date', None)

        start_year, start_month = '', ''
        if start_date:
            start_year = str(start_date.year)
            start_month = str(start_date.month).rjust(2, '0')

        end_year, end_month = '', ''
        if end_date:
            end_year = str(end_date.year)
            end_month = str(end_date.month).rjust(2, '0')

        period_start = start_year + '-' + start_month
        period_end = end_year + '-' + end_month

        for employee, fr_data in data['reserve_funds'].items():
            fr_data['months'] = str(fr_data['months']).rjust(2, '0')

        report_context['company_ruc'] = company_ruc
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['period_start'] = period_start
        report_context['period_end'] = period_end
        report_context['reserve_funds'] = data['reserve_funds']

        cls.set_context_config_fields(report_context, data['config'])

    @classmethod
    def set_context_config_fields(cls, report_context, config):
        report_context['type'] = config.get('type', '')
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['movement_type'] = config.get('movement_type', '')
        report_context['period_type'] = config.get('period_type', '')


class OutputNoveltiesExport(CompanyReportSignature):
    __name__ = 'output.novelties.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(OutputNoveltiesExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]

        company_ruc = company.party.identifiers[0].code

        current_year = str(fiscalyear.start_date.year)
        current_month = str(period.start_date.month).rjust(2, '0')

        config = data['config']

        report_context['company_ruc'] = company_ruc
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['output_novelties'] = data['output_novelties']
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['movement_type'] = config.get('movement_type', '')


class DayNotWorkedExport(CompanyReportSignature):
    __name__ = 'days.not.worked.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(DayNotWorkedExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]

        company_ruc = company.party.identifiers[0].code

        current_year = str(fiscalyear.start_date.year)
        current_month = str(period.start_date.month).rjust(2, '0')

        config = data['config']

        report_context['company_ruc'] = company_ruc
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['movement_type'] = config.get('movement_type', '')

        report_context['days_not_workeds'] = data['days_not_workeds']


class NoticeInputExport(CompanyReportSignature):
    __name__ = 'notice.input.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(NoticeInputExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]

        company_ruc = company.party.identifiers[0].code

        current_year = str(fiscalyear.start_date.year)
        current_month = str(period.start_date.month).rjust(2, '0')

        config = data['config']

        report_context['company_ruc'] = company_ruc
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['movement_type'] = config.get('movement_type', '')

        report_context['notice_input'] = data['notice_input']


class RetroactiveDifferenceExport(CompanyReportSignature):
    __name__ = 'retroactive.difference.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            RetroactiveDifferenceExport, cls).get_context(records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]

        company_ruc = company.party.identifiers[0].code

        current_year = str(fiscalyear.start_date.year)
        current_month = str(period.start_date.month).rjust(2, '0')

        config = data['config']
        report_context['company_ruc'] = company_ruc
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['retroactive_differences'] = (
            data['retroactive_differences'])

        report_context['type'] = config.get('type', '')
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['movement_type'] = config.get('movement_type', '')


class NoticeNewSalaryExport(CompanyReportSignature):
    __name__ = 'notice.new.salary.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(NoticeNewSalaryExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')

        company = Company.search([('id', '=', data['company'])])[0]
        date = data.get('date', '')

        company_ruc = company.party.identifiers[0].code
        current_year = str(date.year)
        current_month = str(date.month).rjust(2, '0')

        report_context['company_ruc'] = company_ruc
        report_context['current_year'] = current_year
        report_context['current_month'] = current_month
        report_context['salary_employees'] = data['salary_employees']

        cls.set_context_config_fields(report_context, data['config'])

    @classmethod
    def set_context_config_fields(cls, report_context, config):
        report_context['branch_office_code'] = config.get(
            'branch_office_code', '')
        report_context['movement_type'] = config.get('movement_type', '')


class TransparencyMonthlyRemuneration(CompanyReportSignature):
    __name__ = 'transparency.monthly.remuneration'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            TransparencyMonthlyRemuneration, cls).get_context(records, data)
        cls.set_context_fields(report_context, data)
        report_context['position_name'] = cls._get_position_name
        report_context['department_parent_name'] = cls._get_department_parent_name #noqa
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Employee = Pool().get('company.employee')
        Department = pool.get('company.department')
        Contract = pool.get('company.contract')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]
        responsable = Employee.search([('id', '=', data['responsable'])])[0]
        department = Department.search([('id', '=', data['department'])])[0]
        phone = data.get('phone', '-')
        email = data.get('email', '-')

        remunerations = data['remunerations']

        contracts_dict = defaultdict(lambda: None)
        contract_ids = [v['contract_id'] for v in remunerations.values()]
        contracts_instances = []
        for contract in Contract.browse(contract_ids):
            contracts_instances.append(contract)
            contracts_dict[contract.id] = contract

        comm_subs = get_comm_subs_info_from_contract(
            contracts=list(contracts_dict.values()),
            start_date=data['period_start_date'],
            end_date=data['period_end_date'])

        template_account_codes = get_template_account_codes(contracts_instances)

        for cont, values in remunerations.items():
            contract = contracts_dict[values['contract_id']]
            values['number'] = cont
            values['contract'] = contract
            values['comm_sub'] = comm_subs[contract.id]
            values['template_account'] = template_account_codes[contract.id]

        headers = data['headers']

        report_context['company'] = company
        report_context['fiscalyear'] = fiscalyear
        report_context['period'] = period
        report_context['responsable'] = responsable
        report_context['department'] = department
        report_context['phone'] = phone
        report_context['email'] = email
        report_context['remunerations'] = remunerations
        report_context['headers'] = headers
        report_context['start_date'] = data['period_start_date']
        report_context['end_date'] = data['period_end_date']

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=True):
        return get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)

    @classmethod
    def _get_department_parent_name(cls, department, level):
        Department = Pool().get('company.department')
        result = None
        if department:
            # Get levels until the level parameter
            levels, l = [], int(level)
            while l > 0:
                levels.append(str(l))
                l -= 1

            # Get structure
            structure = Department.get_level_structure_data(
                departments=[department], levels=levels)
            result = structure[department][level]
            if not result:
                # Find another greater level parent than the needed level
                for department, values in structure.items():
                    for vlevel, vparent in values.items():
                        if int(vlevel) < int(level) and vparent:
                            result = vparent
                            break
                if not result:
                    result = department
        result_name = result.name if result else '-'
        return result_name


class PayslipRuleCodes(CompanyReportSignature):
    __name__ = 'payslip.rule.codes'


class PayslipEntryExport(CompanyReportSignature):
    'Payslip Entry Export'
    __name__ = 'payslip.entry.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipEntryExport, cls).get_context(records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Fiscalyear = pool.get('account.fiscalyear')
        Period = pool.get('account.period')
        PayslipEntry = pool.get('payslip.entry')
        PayslipEntryType = pool.get('payslip.entry.type')

        company = Company.search([('id', '=', data['company'])])[0]
        fiscalyear = Fiscalyear.search([('id', '=', data['fiscalyear'])])[0]
        period = Period.search([('id', '=', data['period'])])[0]
        type = PayslipEntryType.search([('id', '=', data['type'])])[0]
        records = PayslipEntry.search([
                ('state', '=', 'done'),
                ('type_', '=', type),
                ('entry_date', '>=', period.start_date),
                ('entry_date', '<=', period.end_date),
        ])

        report_context['company'] = company
        report_context['fiscalyear'] = fiscalyear
        report_context['period'] = period
        report_context['type'] = type
        report_context['records'] = records


class RemunerationHistory(CompanyReportSignature):
    'Payslip Remuneration History'
    __name__ = 'remuneration.history.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RemunerationHistory, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Employee = pool.get('company.employee')
        Payslip = pool.get('payslip.payslip')

        company = Company.search([('id', '=', data['company'])])[0]
        employee = Employee.search([('id', '=', data['employee'])])[0]
        payslips = [p for p in Payslip.browse(data['payslips'])]
        if payslips:
            payslips.sort(key=lambda x: x.period)

        report_context['company'] = company
        report_context['employee'] = employee
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['records'] = payslips


class PayslipLineDetail(CompanyReportSignature):
    'Payslip Line Detail'
    __name__ = 'payslip.line.detail'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipLineDetail, cls).get_context(records, data)
        records_order = [record for record in records]
        records_order.sort(key=lambda x: x.payslip.employee.party.name)
        cls.set_context_fields(report_context, records_order)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, payslip_lines):
        # Get payslips
        payslips = list(set([line.payslip for line in payslip_lines]))

        # Get commissions subrogations info
        dict_comm_subs = cls.get_comm_sub_info(payslips)
        dict_comm_subs_departments = cls.get_comm_sub_department_info(
            dict_comm_subs)

        # Get departments
        departments = cls.get_department_list(payslips,
            dict_comm_subs_departments)
        department_levels = cls.get_department_structure(departments, levels=[])

        report_context['records_order'] = payslip_lines
        report_context['departments'] = department_levels
        report_context['comm_subs'] = dict_comm_subs
        report_context['comm_subs_departments'] = dict_comm_subs_departments
        report_context['position_name'] = cls._get_position_name

    @classmethod
    def get_department_structure(cls, departments, levels=[]):
        pool = Pool()
        Department = pool.get('company.department')
        structure = Department.get_level_structure_data(
            departments=departments, levels=levels)
        dict_levels = {d.id:s for d, s in structure.items()}
        return dict_levels

    @classmethod
    def get_comm_sub_info(cls, payslips):
        result = defaultdict(lambda: None)
        comm_sub_info = get_comm_subs_info(payslips)
        for payslip in payslips:
            contract_id = payslip.contract.id
            if comm_sub_info[contract_id]:
                result[payslip.id] = comm_sub_info[contract_id]
        return result

    @classmethod
    def get_comm_sub_department_info(cls, comm_subs_info):
        result = defaultdict(lambda: None)
        for payslip_id, comm_sub_info in comm_subs_info.items():
             department = get_department(
                comm_sub_info,
                consider_commission=True,
                consider_subrogation=True)
             if department:
                result[payslip_id] = department
        return result

    @classmethod
    def get_department_list(cls, payslips, comm_subs_departments):
        departments = [p.department for p in payslips]
        departments += [d for d in comm_subs_departments.values() if d]
        departments = list(set(departments))
        return departments

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        position_name = get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        position_name = position_name if position_name else ''
        return position_name


class PayslipPayslipGeneralSummaryEmail(CompanyReportSignature):
    __name__ = 'payslip.payslip.general.summary.email'


class EmailContentPayslipPaymentNotice(CompanyReportSignature):
    __name__ = 'email.content.payslip.payment.notice'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            EmailContentPayslipPaymentNotice, cls).get_context(records, data)
        report_context['month_name'] = cls._get_month_name
        return report_context

    @classmethod
    def _get_month_name(cls, period, locale):
        month_no = period.end_date.month
        try:
            month = get_month_name(month_no, locale)
        except:
            month = period.name
        return month.capitalize()


class EmailContentPayslipRetiredPaymentNotice(CompanyReportSignature):
    __name__ = 'email.content.payslip.retired.payment.notice'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            EmailContentPayslipRetiredPaymentNotice, cls).get_context(
                records, data)
        report_context['month_name'] = cls._get_month_name
        return report_context

    @classmethod
    def _get_month_name(cls, period, locale):
        month_no = period.end_date.month
        try:
            month = get_month_name(month_no, locale)
        except:
            month = period.name
        return month.capitalize()


class EmailContentPayslipAdvancePaymentNotice(CompanyReportSignature):
    __name__ = 'email.content.payslip.advance.payment.notice'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(EmailContentPayslipAdvancePaymentNotice,
            cls).get_context(records, data)
        report_context['month_name'] = cls._get_month_name
        return report_context

    @classmethod
    def _get_month_name(cls, period, locale):
        month_no = period.end_date.month
        try:
            month = get_month_name(month_no, locale)
        except:
            month = period.name
        return month.capitalize()


class LaborMinistry(CompanyReportSignature):
    'LABOR MINISTRY'
    __name__ = 'payslip.labor.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(LaborMinistry, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context
    
    @classmethod
    def _get_month_name(cls, month, locale):
        return get_month_name(month, locale)

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        Company = pool.get('company.company')
        company = Company.search([('id', '=', data['company'])])[0]
        company_ruc = company.party.identifiers[0].code

        payslips = [p for p in Payslip.browse(data['payslips'])]
        comm_subs = get_comm_subs_info(payslips)

        dict_records = {}
        for cont, payslip in enumerate(payslips):
            contract = payslip.contract
            comm_sub = comm_subs[contract.id]

            dict_payslip = {}
            # General info
            month = str(cls._get_month_name(payslips[0].period.start_date.month,
                'es_EC.UTF-8')).upper()
            dict_payslip['month'] = month
            dict_payslip['fiscalyear'] = payslips[0].period.fiscalyear.name
            dict_payslip['company_ruc'] = company_ruc
            dict_payslip['number'] = (cont + 1)
            dict_payslip['unit'] = 'Matriz'
            dict_payslip['process_type'] = ' '

            # Department info
            if comm_sub:
                department = cls._get_department(comm_sub,
                    consider_commission=True, consider_subrogation=False)
            else:
                department = payslip.department
            nivel3 = department
            third_level_unit = nivel3.name
            nivel2 = nivel3.parent

            if nivel2 and nivel2.parent:
                second_level_unit = nivel2.name
                nivel1 = nivel2.parent
                if nivel1 and nivel1.parent:
                    first_level_unit = nivel1.name
                else:
                    first_level_unit = nivel2.name
            else:
                second_level_unit = nivel3.name
                first_level_unit = nivel3.name

            dict_payslip['first_level_unit'] = remove_accents(first_level_unit)
            dict_payslip['second_level_unit'] = remove_accents(
                second_level_unit)
            dict_payslip['third_level_unit'] = remove_accents(third_level_unit)

            # Identifier info
            if payslip.employee.party.tax_identifier.type == "ec_ci":
                identifier_type = "CEDULA"
            else:
                identifier_type = "RUC"
            dict_payslip['identifier_type'] = identifier_type
            dict_payslip['identifier'] = (
                payslip.employee.party.tax_identifier.code)

            # Names info
            first_names = payslip.employee.party.first_name
            last_names = payslip.employee.party.last_name
            dict_payslip['first_names'] = remove_accents(first_names)
            dict_payslip['last_names'] = remove_accents(last_names)
            dict_payslip['civil_state'] = (
                payslip.employee.civil_state_translated)
            dict_payslip['gender'] = payslip.employee.gender_translated
            dict_payslip['education_level'] = remove_accents(
                payslip.employee.education_level_translated)

            # Degree info
            degree = 'NO APLICA'
            if payslip.employee.educations:
                if payslip.employee.education_level=='superior':
                    degree = payslip.employee.educations[0].name

            dict_payslip['degree'] = remove_accents(degree)
            dict_payslip['country'] = payslip.employee.country
            dict_payslip['authorization_number'] = ' '
            dict_payslip['authorization_number_date'] = ' '
            if payslip.employee.ethnic_group:
                ethnic = payslip.employee.ethnic_group.name
            else:
                ethnic = ""
            dict_payslip['ethnic'] = ethnic
            dict_payslip['birthdate'] = payslip.employee.birthdate.strftime(
                "%d/%m/%Y")
            dict_payslip['status_woman'] = ' '
            dict_payslip['start_date_woman'] = ' '
            dict_payslip['end_date_woman'] = ' '

            # Disability info
            is_disability = "no"
            disabil_name = ""
            disabil_percentage = ""
            kinship = ""
            if payslip.employee.disabilities:
                is_disability = "si"
                disabil_name = payslip.employee.disabilities[0].type.name
                disabil_percentage = payslip.employee.disabilities[0].percentage
            else:
                for burden in payslip.employee.burdens:
                    if burden.is_sustitute:
                        is_disability = "SUSTITUTO"
                        disabil_name = burden.disabilities[0].type.name
                        disabil_percentage = burden.disabilities[0].percentage
                        kinship = burden.consanguinity_translated

            dict_payslip['is_disability'] = is_disability
            dict_payslip['labor_inclusion'] = ' '
            dict_payslip['disabilities_name'] = remove_accents(disabil_name)
            dict_payslip['disabilities_percentage'] = disabil_percentage
            dict_payslip['kinship'] = kinship
            dict_payslip['catastrophic_disease'] = (
                'SI' if payslip.employee.catastrophic_disease else 'NO')
            dict_payslip['catastrophic_disease_cie'] = (
                payslip.employee.disease.name
                if payslip.employee.catastrophic_disease
                else ''
            )

            # Contract info
            dict_payslip['start_date'] = payslip.contract.start_date.strftime(
                "%d/%m/%Y")
            dict_payslip['end_date'] = payslip.contract.end_date
            dict_payslip['work_relationship'] = (
                payslip.work_relationship.name)
            dict_payslip['contract_type'] = remove_accents(
                payslip.contract_type.name)
            dict_payslip['state_position'] = 'OCUPADO'
            dict_payslip['vacancy_type'] = 'ACTIVA'
            dict_payslip['hierarchical_level'] = ' '
            dict_payslip['service_time'] = ' '
            dict_payslip['region'] = 'SIERRA'
            dict_payslip['province'] = (
                payslip.employee.company.party.addresses[0].subdivision.name)
            dict_payslip['canton'] = 'CUENCA'
            dict_payslip['city'] = (
                payslip.employee.company.party.addresses[0].city)
            dict_payslip['expense_type' ] = ' '
            dict_payslip['project'] = 'NO APLICA'
            dict_payslip['scale'] = 'NO APLICA'
            dict_payslip['salary'] = payslip.salary

            # Pisition info
            if comm_sub:
                position_name = cls._get_position_name(comm_sub,
                    consider_commission=True, consider_subrogation=False)
            else:
                position_name = payslip.position.name
            position_name = remove_accents(position_name)
            dict_payslip['position_name'] = position_name

            rmu = 0
            nocturne_surcharge = 0
            alimentation = 0
            transport = 0
            antique = 0
            family_subsidy = 0
            kindergarten = 0
            personal_contribution = 0
            patronal_contribution = 0
            for line in payslip.lines:
                if 'aporte_personal' in line.rule.code:
                    personal_contribution = line.amount
                if 'aporte_patronal' in line.rule.code:
                    patronal_contribution = line.amount
                if line.rule.code == 'recargo_nocturno':
                    nocturne_surcharge = line.amount
                if line.rule.code=='rmu':
                    rmu = line.amount
                if line.rule.code=='subsidio_alimentacion':
                    alimentation = line.amount
                if line.rule.code=='subsidio_familiar':
                    family_subsidy = line.amount
                if line.rule.code=='subsidio_transporte':
                    transport = line.amount
                if line.rule.code=='subsidio_antiguedad':
                    antique = line.amount
                if line.rule.code=='subsidio_guarderia':
                    kindergarten = line.amount
            dict_payslip['rmu'] = rmu
            dict_payslip['variable_remuneration'] = 0
            dict_payslip['nocturne_surcharge'] = (
                payslip.nocturne_surcharge_hours)

            supplementary_overtime = 0
            extraordinary_overtime = 0
            supplementary = 0
            extraordinary = 0
            for overtime in payslip.overtimes:
                if overtime.type_.description == 'Horas Suplementarias':
                    supplementary_overtime += overtime.total_with_salary
                    supplementary += overtime.time_round

                if overtime.type_.description == 'Horas Extraordinarias':
                    extraordinary_overtime += overtime.total_with_salary
                    extraordinary += overtime.time_round

            dict_payslip['extraordinary_overtime'] = extraordinary
            dict_payslip['supplementary_overtime'] = supplementary
            dict_payslip['nocturne_surcharge_paid'] = nocturne_surcharge
            dict_payslip['extraordinary_overtime_paid'] = extraordinary_overtime
            dict_payslip['supplementary_overtime_paid'] = supplementary_overtime
            dict_payslip['viatics'] = 0
            dict_payslip['alimentation'] = alimentation
            dict_payslip['transport'] = transport
            dict_payslip['antique'] = antique
            dict_payslip['family_subsidy'] = family_subsidy
            dict_payslip['kindergarten'] = kindergarten

            reserve_funds = 0
            xiii = 0
            xiv = 0
            for benefit in payslip.law_benefits:
                if benefit.type_.code == 'fr' and benefit.kind == 'monthlyse':
                    reserve_funds = benefit.amount
                if benefit.type_.code == 'xiii' and benefit.kind == 'monthlyse':
                    xiii = benefit.amount
                if benefit.type_.code == 'xiv' and benefit.kind == 'monthlyse':
                    xiv = benefit.amount

            dict_payslip['reserve_funds'] = reserve_funds

            commission_value = 0
            subrogation_value = 0
            if payslip.comms_subs:  # TODO: Cambiar
                # financiero.company_commission_subrogation_type no tiene codigo,
                if payslip.comms_subs[0].type_.id == 5:
                    commission_value = payslip.comms_subs[0].amount
                if payslip.comms_subs[0].type_.id == 6:
                    subrogation_value = payslip.comms_subs[0].amount

            # Quantize values
            commission_value = Decimal(str(commission_value)).quantize(_CENT)
            subrogation_value = Decimal(str(subrogation_value)).quantize(_CENT)

            dict_payslip['commission'] = commission_value
            dict_payslip['subrogation'] = subrogation_value
            dict_payslip['xiii'] = xiii
            dict_payslip['xiv'] = xiv
            dict_payslip['personal_contribution'] = personal_contribution
            dict_payslip['patronal_contribution'] = patronal_contribution
            dict_payslip['other_income'] = (alimentation + transport + antique +
                                            family_subsidy + kindergarten)
            dict_payslip['total_income'] = payslip.total_income
            if (payslip.total_income - payslip.salary - nocturne_surcharge -
                supplementary_overtime - extraordinary_overtime - xiii - xiv -
                reserve_funds - commission_value) == 0:
                other = "NO APLICA"
            else:
                other = ('Subsidio Alimentacion  Familiar  AntigUedad y/o '
                         'Desayuno negociados mediante Contrato Colectivo')
            dict_payslip['other'] = other
            dict_payslip['teleworking'] = 'NO'
            dict_payslip['start_date_teleworking'] = ' '
            dict_payslip['end_date_teleworking'] = ' '
            dict_payslip['total_days_teleworking'] = ' '
            dict_payslip['observation'] = 'NO APLICA'

            dict_records[payslip.employee.identifier+position_name] = dict_payslip

        report_context['records'] = dict_records

    @classmethod
    def _get_department(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        return get_department(comm_sub_info,
            consider_commission=consider_commission,
                consider_subrogation=consider_subrogation)

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
        consider_subrogation=False):
        return get_position_name(comm_sub_info,
            consider_commission=consider_commission,
                consider_subrogation=consider_subrogation)


class LaborMinistryXIV(CompanyReportSignature):
    'Labor XIV'
    __name__ = 'payslip.labor_xiv.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(LaborMinistryXIV, cls).get_context(
            records, data)
        Company = Pool().get('company.company')

        company = Company.search([('id', '=', data['company_id'])])[0]
        start_date = data['start_date']
        end_date = data['end_date']
        retention_rules_ids = data['retention_rules_ids']

        cls.set_context_values(
            report_context, company, start_date, end_date, retention_rules_ids)

        return report_context

    @classmethod
    def set_context_values(cls, report_context, company, start_date, end_date,
            retention_rules_ids):
        Employee = Pool().get('company.employee')

        employee_ids = []
        monthlyse = defaultdict(lambda: False)
        worked_days = defaultdict(lambda: 0)
        total_retained = defaultdict(lambda: 0)
        total_earned = defaultdict(lambda: 0)

        data = cls.get_data(company, start_date, end_date)

        for employee_id, records in data.items():
            employee_ids.append(employee_id)
            payslips_accum = records['payslips_accum']
            payslips_month = records['payslips_month']
            worked_days[employee_id] = cls.get_worked_days(payslips_month)
            total_retained[employee_id] = cls.get_total_retained(
                payslips_accum, retention_rules_ids)
            total_earned[employee_id] = cls.get_total_earned(employee_id,
                payslips_month, start_date, end_date)
            monthlyse[employee_id] = cls.get_monthlyse(payslips_month)

        employees = Employee.browse(employee_ids)
        employee_names = cls.get_employee_names(employees)
        departments = cls.get_departments(employees)

        employees.sort(key=lambda x: x.party.name)

        report_context['employees'] = employees
        report_context['monthlyse'] = monthlyse
        report_context['worked_days'] = worked_days
        report_context['employee_names'] = employee_names
        report_context['departments'] = departments
        report_context['total_retained'] = total_retained
        report_context['total_earned'] = total_earned

    @classmethod
    def get_data(cls, company, start_date, end_date):
        records = defaultdict(lambda: {
            'payslips_accum': [],
            'payslips_month': [],
        })

        data_xiv_accum_payslip = cls.get_data_xiv_accumulated_payslip(
            company, start_date, end_date)
        data_before_xiv_payslip = cls.get_data_payslips_before_xiv_period(
            company, start_date, end_date)

        for employee_id, payslips in data_xiv_accum_payslip.items():
            records[employee_id] = {
                'payslips_accum': payslips,
                'payslips_month': [],
            }

        for employee_id, payslips in data_before_xiv_payslip.items():
            if employee_id not in records:
                records[employee_id] = {
                    'payslips_accum': [],
                    'payslips_month': payslips,
                }
            else:
                records[employee_id]['payslips_month'] = payslips
        return records

    @classmethod
    def get_data_xiv_accumulated_payslip(cls, company, xiv_start_date,
            xiv_end_date):
        '''
        return: {employee_id: [accumulate individual payslips ids], ... }
        '''
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Company = pool.get('company.company')
        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        tbl_company = Company.__table__()
        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()

        data = defaultdict(lambda: [])

        query = tbl_payslip.join(tbl_company,
                condition=(
                    tbl_company.id == tbl_payslip.company)
            ).join(tbl_payslip_template,
                condition=(
                    tbl_payslip_template.id == tbl_payslip.template)
            ).join(tbl_law_benefit_type,
                condition=(
                    tbl_law_benefit_type.id ==
                        tbl_payslip_template.law_benefit_type)
            ).select(
                tbl_payslip.employee.as_('employee'),
                tbl_payslip.id.as_('payslip'),
                where=((tbl_company.id == company.id) &
                       (tbl_payslip.start_date >= xiv_start_date) &
                       (tbl_payslip.end_date <= xiv_end_date) &
                       (tbl_payslip_template.type ==
                            'accumulated_law_benefits') &
                       (tbl_law_benefit_type.code == 'xiv') &
                       ~(tbl_payslip.state.in_(['cancel']))),
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            if row['employee'] not in data:
                data[row['employee']] = [row['payslip']]
            else:
                data[row['employee']].append(row['payslip'])
        return data


    @classmethod
    def get_data_payslips_before_xiv_period(cls, company,
            xiv_start_date, xiv_end_date):
        '''
        return: {employee_id: [monthly individual payslips ids], ...}
        '''
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        Period = pool.get('account.period')
        PayslipTemplate = pool.get('payslip.template')
        Employee = pool.get('company.employee')
        Contract = pool.get('company.contract')

        tbl_payslip = Payslip.__table__()
        tbl_period = Period.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_employee = Employee.__table__()
        tbl_contract = Contract.__table__()

        data = defaultdict(lambda: [])

        employees_ids = cls.get_active_employees_before_xiv_period(
            company, xiv_end_date)

        for sub_ids in grouped_slice(employees_ids):
            red_sql = reduce_ids(tbl_employee.id, sub_ids)
            query = tbl_employee.join(tbl_payslip,
                    condition=(tbl_payslip.employee == tbl_employee.id)
                ).join(tbl_contract,
                    condition=(tbl_contract.id == tbl_payslip.contract)
                ).join(tbl_period,
                    condition=(tbl_period.id == tbl_payslip.period)
                ).join(tbl_payslip_template,
                    condition=(tbl_payslip_template.id == tbl_payslip.template)
                ).select(
                    tbl_employee.id.as_('employee'),
                    tbl_payslip.id.as_('payslip'),
                    where=(red_sql &
                           (tbl_period.start_date >= xiv_start_date) &
                           (tbl_period.end_date <= xiv_end_date) &
                           ((tbl_contract.end_date == Null) |
                            (tbl_contract.end_date >= xiv_end_date)) &
                           (tbl_payslip_template.type == 'payslip') &
                            ~(tbl_payslip.state.in_([
                                'draft', 'confirm', 'cancel']))),
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['employee'] not in data:
                    data[row['employee']] = [row['payslip']]
                else:
                    data[row['employee']].append(row['payslip'])
        return data

    @classmethod
    def get_active_employees_before_xiv_period(cls, company, xiv_end_date):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_employee = Employee.__table__()
        tbl_party = Party.__table__()
        tbl_period = Period.__table__()

        result = []

        year = xiv_end_date.year
        month = xiv_end_date.month
        last_day = monthrange(year, month)[1]
        period_start_date = datetime.date(year=year, month=month, day=1)
        period_end_date = datetime.date(year=year, month=month, day=last_day)

        query = tbl_payslip.join(tbl_employee,
                condition=(tbl_employee.id == tbl_payslip.employee)
            ).join(tbl_period,
                condition=(tbl_period.id == tbl_payslip.period)
            ).join(tbl_party,
                condition=(tbl_party.id == tbl_employee.party)
            ).join(tbl_payslip_template,
                condition=(tbl_payslip_template.id == tbl_payslip.template)
            ).select(
                tbl_payslip.employee,
                tbl_party.name,
                where=((tbl_payslip.company == company.id) &
                       ~(tbl_payslip.state.in_(['draft', 'confirm', 'cancel']))&
                       (tbl_payslip_template.type == 'payslip') &
                       (tbl_period.start_date >= period_start_date) &
                       (tbl_period.end_date <= period_end_date)),
                distinct_on=[tbl_payslip.employee, tbl_party.name],
                order_by=[tbl_party.name.asc, tbl_payslip.employee]
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['employee'])
        return result

    @classmethod
    def get_employee_names(cls, employees):
        employee_names = defaultdict(lambda: {
            'lastnames': '',
            'firstnames': ''
        })
        for employee in employees:
            employee_names[employee.id] = {
                'lastnames': employee.party.last_name,
                'firstnames': employee.party.first_name
            }
        return employee_names

    @classmethod
    def get_worked_days(cls, payslip_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()

        worked_days = 0
        if payslip_ids:
            query = tbl_payslip.join(tbl_payslip_template,
                    condition=(tbl_payslip_template.id == tbl_payslip.template)
                ).select(
                Sum(tbl_payslip.worked_days_with_normative).as_(
                    'days_with_norm'),
                Sum(tbl_payslip.total_days_disease).as_('days_disease'),
                where=((tbl_payslip.id.in_(payslip_ids)) &
                       ~(tbl_payslip_template.code.like('%rol_ajuste%'))),
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                worked_days += (row['days_with_norm'] + row['days_disease'])
        return worked_days

    @classmethod
    def get_departments(cls, employees):
        Contract = Pool().get('company.contract')
        result = defaultdict(lambda: '')
        for employee in employees:
            if employee.contract:
                result[employee.id] = employee.contract.department.name
            else:
                contracts = Contract.search([
                    ('employee', '=', employee),
                    ('state', '!=', 'cancel')
                ], order=[('start_date', 'DESC')])
                if contracts:
                    result[employee.id] = contracts[0].department.name
        return result

    @classmethod
    def get_total_retained(cls, payslips_ids, retention_rules_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipLine = pool.get('payslip.line')
        PayslipRule = pool.get('payslip.rule')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_line = PayslipLine.__table__()
        tbl_payslip_rule = PayslipRule.__table__()

        total_retained = 0
        if payslips_ids and retention_rules_ids:
            query = tbl_payslip_line.join(tbl_payslip,
                    condition=(tbl_payslip.id == tbl_payslip_line.payslip)
                ).join(tbl_payslip_rule,
                    condition=(tbl_payslip_rule.id == tbl_payslip_line.rule)
                ).select(
                    Sum(tbl_payslip_line.amount).as_('amount'),
                    where=((tbl_payslip.id.in_(payslips_ids))) &
                          (tbl_payslip_rule.id.in_(retention_rules_ids)))
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['amount']:
                    total_retained += row['amount']
        return total_retained

    @classmethod
    def get_total_earned(cls, employee_id, payslips_ids, xiv_start_date,
            xiv_end_date):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')
        PayslipLawBenefit = pool.get('payslip.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')
        Period = pool.get('account.period')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_law_benefit = PayslipLawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()
        tbl_period = Period.__table__()

        total_earned = Decimal('0.00')
        if payslips_ids:
            # Sum law benefits amounts
            query = tbl_payslip_law_benefit.join(tbl_payslip,
                condition=(tbl_payslip.id == tbl_payslip_law_benefit.payslip) |
                    (tbl_payslip.id ==
                        tbl_payslip_law_benefit.generator_payslip)
                ).join(tbl_law_benefit_type,
                    condition=(tbl_law_benefit_type.id ==
                        tbl_payslip_law_benefit.type_)
                ).select(
                    Sum(tbl_payslip_law_benefit.amount).as_('amount'),
                    where=((tbl_payslip.id.in_(payslips_ids)) &
                           (tbl_law_benefit_type.code == 'xiv'))
                )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['amount']:
                    total_earned += row['amount']

            # Sum adjustments amounts
            query = tbl_payslip_law_benefit.join(tbl_law_benefit_type,
                condition=(tbl_law_benefit_type.id ==
                    tbl_payslip_law_benefit.type_)
            ).join(tbl_period,
                condition=(tbl_period.id == tbl_payslip_law_benefit.period)
            ).select(
                Sum(tbl_payslip_law_benefit.amount).as_('amount'),
                where=((tbl_payslip_law_benefit.employee == employee_id) &
                       (tbl_payslip_law_benefit.kind == 'adjustment') &
                       (tbl_law_benefit_type.code == 'xiv') &
                       (tbl_period.start_date >= xiv_start_date) &
                       (tbl_period.end_date <= xiv_end_date))
                )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['amount']:
                    total_earned += row['amount']
        return total_earned.quantize(_CENT)

    @classmethod
    def get_monthlyse(cls, payslips_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')

        tbl_payslip = Payslip.__table__()

        if payslips_ids:
            query = tbl_payslip.select(tbl_payslip.xiv,
                where=(tbl_payslip.id.in_(payslips_ids)))

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['xiv'] and row['xiv'] == 'accumulate':
                    return False
        return True


class ReportSettlementCertificate(CompanyReportSignature):
    __name__ = 'contract.settlement.certificate'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportSettlementCertificate, cls).get_context(
            records, data)

        letters = number_to_letters(records[0].total_value)
        month_start_date = get_month_name(
            records[0].contract.start_date.month, 'es_EC.UTF-8')
        month_end_date = get_month_name(
            records[0].contract.end_date.month, 'es_EC.UTF-8')
        report_context['_get_totals_liquidation'] = cls._get_totals_liquidation
        report_context['letters'] = letters
        report_context['month_start_date'] = month_start_date
        report_context['month_end_date'] = month_end_date

        return report_context

    @classmethod
    def _get_totals_liquidation(cls, records, type='Ingresos'):
        totals_liquidation = {}

        for liquidation in records:
            if type == 'Ingresos':
                for rule_line in liquidation.income_lines:
                    totals_liquidation[rule_line.rule.code] = {
                        'header': rule_line.rule.name,
                        'amount': rule_line.amount
                    }
            else:
                for rule_line in liquidation.deduction_lines:
                    totals_liquidation[rule_line.rule.code] = {
                        'header': rule_line.rule.name,
                        'amount': rule_line.amount
                    }
        return totals_liquidation


class ReportContractLiquidation(CompanyReportSignature):
    __name__ = 'contract.liquidation'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportContractLiquidation, cls).get_context(
            records, data)

        report_context['_get_totals_liquidation'] = cls._get_totals_liquidation

        return report_context

    @classmethod
    def _get_totals_liquidation(cls, records):
        totals_liquidation = {}

        for liquidation in records:
            for rule_line in liquidation.income_lines:
                totals_liquidation[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
            totals_liquidation['total_income'] = {
                'header': 'Total Ingresos',
                'amount': liquidation.total_income
            }
            for rule_line in liquidation.deduction_lines:
                totals_liquidation[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
            totals_liquidation['total_deduction'] = {
                'header': 'Total Egresos',
                'amount': liquidation.total_deduction
            }
            # for rule_line in liquidation.internal_lines:
            #     totals_liquidation[rule_line.rule.code] = {
            #         'header': rule_line.rule.name,
            #         'amount': rule_line.amount
            #     }
            # totals_liquidation['total_internal'] = {
            #     'header': 'Total Internos',
            #     'amount': liquidation.total_internal
            # }

        return totals_liquidation


class LaborMinistryXIII(CompanyReportSignature):
    'Labor Ministry XIII'
    __name__ = 'payslip.labor_xiii.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(LaborMinistryXIII, cls).get_context(
            records, data)
        Company = Pool().get('company.company')

        company = Company.search([('id', '=', data['company_id'])])[0]
        start_date = data['start_date']
        end_date = data['end_date']
        retention_rules_ids = data['retention_rules_ids']

        cls.set_context_values(
            report_context, company, start_date, end_date, retention_rules_ids)

        return report_context

    @classmethod
    def set_context_values(cls, report_context, company, start_date, end_date,
            retention_rules_ids):
        Employee = Pool().get('company.employee')

        employee_ids = []
        monthlyse = defaultdict(lambda: False)
        worked_days = defaultdict(lambda: 0)
        total_retained = defaultdict(lambda: 0)
        total_earned = defaultdict(lambda: 0)

        data = cls.get_data(company, start_date, end_date)

        for employee_id, records in data.items():
            employee_ids.append(employee_id)
            payslips_accum = records['payslips_accum']
            payslips_month = records['payslips_month']
            worked_days[employee_id] = cls.get_worked_days(payslips_month)
            total_retained[employee_id] = cls.get_total_retained(
                payslips_accum, retention_rules_ids)
            total_earned[employee_id] = cls.get_total_earned(payslips_month)
            monthlyse[employee_id] = cls.get_monthlyse(payslips_month)

        employees = Employee.browse(employee_ids)
        employee_names = cls.get_employee_names(employees)
        departments = cls.get_departments(employees)

        employees.sort(key=lambda x: x.party.name)

        report_context['employees'] = employees
        report_context['monthlyse'] = monthlyse
        report_context['worked_days'] = worked_days
        report_context['employee_names'] = employee_names
        report_context['departments'] = departments
        report_context['total_retained'] = total_retained
        report_context['total_earned'] = total_earned

    @classmethod
    def get_data(cls, company, start_date, end_date):
        records = defaultdict(lambda: {
            'payslips_accum': [],
            'payslips_month': [],
        })

        data_xiii_accum_payslip = cls.get_data_xiii_accumulated_payslip(
            company, start_date, end_date)
        data_before_xiii_payslip = cls.get_data_payslips_before_xiii_period(
            company, start_date, end_date)

        for employee_id, payslips in data_xiii_accum_payslip.items():
            records[employee_id] = {
                'payslips_accum': payslips,
                'payslips_month': [],
            }

        for employee_id, payslips in data_before_xiii_payslip.items():
            if employee_id not in records:
                records[employee_id] = {
                    'payslips_accum': [],
                    'payslips_month': payslips,
                }
            else:
                records[employee_id]['payslips_month'] = payslips
        return records

    @classmethod
    def get_data_xiii_accumulated_payslip(cls, company,
            xiii_start_date, xiii_end_date):
        '''
        return: {employee_id: [accumulate individual payslips ids], ... }
        '''
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Company = pool.get('company.company')
        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        tbl_company = Company.__table__()
        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()

        data = defaultdict(lambda: [])

        query = tbl_payslip.join(tbl_company,
                condition=(
                    tbl_company.id == tbl_payslip.company)
            ).join(tbl_payslip_template,
                condition=(
                    tbl_payslip_template.id == tbl_payslip.template)
            ).join(tbl_law_benefit_type,
                condition=(
                    tbl_law_benefit_type.id ==
                        tbl_payslip_template.law_benefit_type)
            ).select(
                tbl_payslip.employee.as_('employee'),
                tbl_payslip.id.as_('payslip'),
                where=((tbl_company.id == company.id) &
                       (tbl_payslip.start_date >= xiii_start_date) &
                       (tbl_payslip.end_date <= xiii_end_date) &
                       (tbl_payslip_template.type ==
                            'accumulated_law_benefits') &
                       (tbl_law_benefit_type.code == 'xiii') &
                       ~(tbl_payslip.state.in_([
                           'draft', 'confirm', 'cancel']))),
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            if row['employee'] not in data:
                data[row['employee']] = [row['payslip']]
            else:
                data[row['employee']].append(row['payslip'])
        return data


    @classmethod
    def get_data_payslips_before_xiii_period(cls, company,
            xiii_start_date, xiii_end_date):
        '''
        return: {employee_id: [monthly individual payslips ids], ...}
        '''
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        Period = pool.get('account.period')
        PayslipTemplate = pool.get('payslip.template')
        Employee = pool.get('company.employee')
        Contract = pool.get('company.contract')

        tbl_payslip = Payslip.__table__()
        tbl_period = Period.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_employee = Employee.__table__()
        tbl_contract = Contract.__table__()

        data = defaultdict(lambda: [])

        employees_ids = cls.get_active_employees_before_xiii_period(
            company, xiii_end_date)

        for sub_ids in grouped_slice(employees_ids):
            red_sql = reduce_ids(tbl_employee.id, sub_ids)
            query = tbl_employee.join(tbl_payslip,
                    condition=(tbl_payslip.employee == tbl_employee.id)
                ).join(tbl_contract,
                    condition=(tbl_contract.id == tbl_payslip.contract)
                ).join(tbl_period,
                    condition=(tbl_period.id == tbl_payslip.period)
                ).join(tbl_payslip_template,
                    condition=(tbl_payslip_template.id == tbl_payslip.template)
                ).select(
                    tbl_employee.id.as_('employee'),
                    tbl_payslip.id.as_('payslip'),
                    where=(red_sql &
                           (tbl_period.start_date >= xiii_start_date) &
                           (tbl_period.end_date <= xiii_end_date) &
                           ((tbl_contract.end_date == Null) |
                            (tbl_contract.end_date >= xiii_end_date)) &
                           (tbl_payslip_template.type == 'payslip') &
                            ~(tbl_payslip.state.in_([
                                'draft', 'confirm', 'cancel']))),
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['employee'] not in data:
                    data[row['employee']] = [row['payslip']]
                else:
                    data[row['employee']].append(row['payslip'])
        return data

    @classmethod
    def get_active_employees_before_xiii_period(cls, company, xiii_end_date):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()
        tbl_employee = Employee.__table__()
        tbl_party = Party.__table__()
        tbl_period = Period.__table__()

        result = []

        year = xiii_end_date.year
        month = xiii_end_date.month
        last_day = monthrange(year, month)[1]
        period_start_date = datetime.date(year=year, month=month, day=1)
        period_end_date = datetime.date(year=year, month=month, day=last_day)

        query = tbl_payslip.join(tbl_employee,
                condition=(tbl_employee.id == tbl_payslip.employee)
            ).join(tbl_period,
                condition=(tbl_period.id == tbl_payslip.period)
            ).join(tbl_party,
                condition=(tbl_party.id == tbl_employee.party)
            ).join(tbl_payslip_template,
                condition=(tbl_payslip_template.id == tbl_payslip.template)
            ).select(
                tbl_payslip.employee,
                tbl_party.name,
                where=((tbl_payslip.company == company.id) &
                       ~(tbl_payslip.state.in_(['draft', 'confirm', 'cancel']))&
                       (tbl_payslip_template.type == 'payslip') &
                       (tbl_period.start_date >= period_start_date) &
                       (tbl_period.end_date <= period_end_date)),
                distinct_on=[tbl_payslip.employee, tbl_party.name],
                order_by=[tbl_party.name.asc, tbl_payslip.employee]
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['employee'])
        return result

    @classmethod
    def get_employee_names(cls, employees):
        employee_names = defaultdict(lambda: {
            'lastnames': '',
            'firstnames': ''
        })
        for employee in employees:
            employee_names[employee.id] = {
                'lastnames': employee.party.last_name,
                'firstnames': employee.party.first_name
            }
        return employee_names

    @classmethod
    def get_worked_days(cls, payslip_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipTemplate = pool.get('payslip.template')
        tbl_payslip = Payslip.__table__()
        tbl_payslip_template = PayslipTemplate.__table__()

        worked_days = 0
        if payslip_ids:
            query = tbl_payslip.join(tbl_payslip_template,
                    condition=(tbl_payslip_template.id == tbl_payslip.template)
                ).select(
                Sum(tbl_payslip.worked_days_with_normative).as_(
                    'days_with_norm'),
                Sum(tbl_payslip.total_days_disease).as_('days_disease'),
                where=((tbl_payslip.id.in_(payslip_ids)) &
                       ~(tbl_payslip_template.code.like('%rol_ajuste%'))),
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                worked_days += (row['days_with_norm'] + row['days_disease'])
        return worked_days

    @classmethod
    def get_departments(cls, employees):
        Contract = Pool().get('company.contract')
        result = defaultdict(lambda: '')
        for employee in employees:
            if employee.contract:
                result[employee.id] = employee.contract.department.name
            else:
                contracts = Contract.search([
                    ('employee', '=', employee),
                    ('state', '!=', 'cancel')
                ], order=[('start_date', 'DESC')])
                if contracts:
                    result[employee.id] = contracts[0].department.name
        return result

    @classmethod
    def get_total_retained(cls, payslips_ids, retention_rules_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Payslip = pool.get('payslip.payslip')
        PayslipLine = pool.get('payslip.line')
        PayslipRule = pool.get('payslip.rule')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_line = PayslipLine.__table__()
        tbl_payslip_rule = PayslipRule.__table__()

        total_retained = 0
        if payslips_ids and retention_rules_ids:
            query = tbl_payslip_line.join(tbl_payslip,
                    condition=(tbl_payslip.id == tbl_payslip_line.payslip)
                ).join(tbl_payslip_rule,
                    condition=(tbl_payslip_rule.id == tbl_payslip_line.rule)
                ).select(
                    Sum(tbl_payslip_line.amount).as_('amount'),
                    where=((tbl_payslip.id.in_(payslips_ids))) &
                          (tbl_payslip_rule.id.in_(retention_rules_ids)))
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['amount']:
                    total_retained += row['amount']
        return total_retained

    @classmethod
    def get_total_earned(cls, payslips_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')
        PayslipLawBenefit = pool.get('payslip.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        tbl_payslip = Payslip.__table__()
        tbl_payslip_law_benefit = PayslipLawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()

        total_earned = Decimal('0.00')
        if payslips_ids:
            query = tbl_payslip_law_benefit.join(tbl_payslip,
                condition=(tbl_payslip.id == tbl_payslip_law_benefit.payslip) |
                          (tbl_payslip.id ==
                           tbl_payslip_law_benefit.generator_payslip)
                ).join(tbl_law_benefit_type,
                    condition=(
                        tbl_law_benefit_type.id ==
                        tbl_payslip_law_benefit.type_)
                ).select(
                    Sum(tbl_payslip_law_benefit.amount).as_('amount'),
                    where=(tbl_payslip.id.in_(payslips_ids)) &
                          (tbl_law_benefit_type.code == 'xiii'))
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['amount']:
                    total_earned += row['amount']
            # total_earned = (total_earned * 12)
        return total_earned.quantize(_CENT)

    @classmethod
    def get_monthlyse(cls, payslips_ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')

        tbl_payslip = Payslip.__table__()

        query = tbl_payslip.select(tbl_payslip.xiii,
            where=(tbl_payslip.id.in_(payslips_ids)))

        cursor.execute(*query)
        for row in cursor_dict(cursor):
            if row['xiii'] and row['xiii'] == 'accumulate':
                return False
        return True


class PayslipOvertimeLinesExport(CompanyReportSignature):
    'Payslip Overtime Lines Export'
    __name__ = 'payslip.overtime.line.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipOvertimeLinesExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()

        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        PayslipGeneral = pool.get('payslip.general')

        ids_generals = data['ids_generals']
        ids_pols = data['ids_payslip_overtime_lines']
        payslip_overtime_lines = [
            pol for pol in PayslipOvertimeLine.browse(ids_pols)]
        generals = [g for g in PayslipGeneral.browse(ids_generals)]

        data_info = cls.get_data(payslip_overtime_lines)
        data = data_info['data']
        types = data_info['types']
        contracts = data_info['contracts']

        totals_info = cls.get_totals(data)
        total_time = totals_info['total_time']
        total_amount = totals_info['total_amount']

        report_context['generals'] = generals
        report_context['data'] = data
        report_context['types'] = types
        report_context['contracts'] = contracts
        report_context['total_amount'] = total_time
        report_context['total_amount'] = total_amount
        report_context['overtime_type_value'] = cls.get_overtime_type_value
        report_context['overtime_time_value'] = cls.get_overtime_time_value
        report_context['overtime_amount_value'] = cls.get_overtime_amount_value


    @classmethod
    def get_overtime_type_value(cls, overtime_values, type):
        value = ''
        for ov in overtime_values:
            if ov['type'] == type:
                value = type
                break
        return value


    @classmethod
    def get_overtime_time_value(cls, overtime_values, type):
        value = ''
        for ov in overtime_values:
            if ov['type'] == type:
                value = str(ov['time'])
                break
        return value


    @classmethod
    def get_overtime_amount_value(cls, overtime_values, type):
        value = ''
        for ov in overtime_values:
            if ov['type'] == type:
                value = str(ov['amount'])
                break
        return value


    @classmethod
    def get_data(cls, payslip_overtime_lines):
        dict_data = defaultdict(lambda: [
            {
                'type': '',
                'time': 0,
                'amount': 0
            }
        ])
        list_data = []
        overtime_types = []
        contracts = defaultdict(lambda: None)
        for pol in payslip_overtime_lines:
            payslip = pol.payslip
            contract = pol.payslip.contract
            overtime_line = pol.overtime_line
            overtime_type = overtime_line.type_.description
            overtime_time = overtime_line.time_round
            overtime_amount = overtime_line.total_with_salary
            item = None

            if contract.id not in dict_data:
                dict_data[contract.id] = []

            if overtime_type not in overtime_types:
                overtime_types.append(overtime_type)

            if contract.id not in contracts:
                contracts[contract.id] = contract

            for o in dict_data[contract.id]:
                if o['type'] == overtime_type:
                    item = o
                    break

            if not item:
                item = {
                    'type': overtime_type,
                    'time': overtime_time,
                    'amount': overtime_amount
                }
                dict_data[contract.id].append(item)
            else:
                item['time'] += overtime_time
                item['amount'] += overtime_amount
        for contract_id, values in dict_data.items():
            list_data.append({
                'contract_id': contract_id,
                'contract': contracts[contract_id],
                'values': values
            })
        return {
            'data': list_data,
            'types': overtime_types,
            'contracts': contracts
        }


    @classmethod
    def get_totals(cls, data):
        total_time = defaultdict(lambda: 0)
        total_amount = defaultdict(lambda: 0)
        for d in data:
            for v in d['values']:
                if v['type'] not in total_time:
                    total_time[v['type']] = v['time']
                else:
                    total_time[v['type']] += v['time']

                if v['type'] not in total_amount:
                    total_amount[v['type']] = v['amount']
                else:
                    total_amount[v['type']] += v['amount']
        return {
            'total_time': total_time,
            'total_amount': total_amount
        }


class DynamicReportRulesPayslip(CompanyReportSignature):
    __name__ = 'dynamic.report.rules.payslip'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            DynamicReportRulesPayslip, cls).get_context(
                records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        Payslip = pool.get('payslip.payslip')

        company = Company.search([('id', '=', data['company'])])[0]
        dict_rules = data['dict_rules']

        # Get payslips
        payslip_ids = list(set([v['payslip_id'] for v in dict_rules.values()]))
        payslips = Payslip.search([('id', 'in', payslip_ids)])
        dict_payslips = {p.id: p for p in payslips}

        # Get commissions subrogations info
        dict_comm_subs = cls.get_comm_sub_info(payslips)
        dict_comm_subs_departments = cls.get_comm_sub_department_info(
            dict_comm_subs)

        # Get departments
        departments = cls.get_department_list(payslips,
            dict_comm_subs_departments)
        department_levels = cls.get_department_structure(departments, levels=[])

        for cont, values in dict_rules.items():
            payslip_id = values['payslip_id']
            payslip = dict_payslips.get(payslip_id, None)
            values['payslip'] = payslip
            values['department'] = payslip.department
            values['number'] = cont

        headers = data['headers']

        report_context['company'] = company
        report_context['departments'] = department_levels
        report_context['dict_rules'] = dict_rules
        report_context['headers'] = headers
        report_context['comm_subs'] = dict_comm_subs
        report_context['comm_subs_departments'] = dict_comm_subs_departments
        report_context['position_name'] = cls._get_position_name

    @classmethod
    def get_department_structure(cls, departments, levels=[]):
        pool = Pool()
        Department = pool.get('company.department')
        structure = Department.get_level_structure_data(
            departments=departments, levels=levels)
        dict_levels = {d.id:s for d, s in structure.items()}
        return dict_levels

    @classmethod
    def get_comm_sub_info(cls, payslips):
        result = defaultdict(lambda: None)
        comm_sub_info = get_comm_subs_info(payslips)
        for payslip in payslips:
            contract_id = payslip.contract.id
            if comm_sub_info[contract_id]:
                result[payslip.id] = comm_sub_info[contract_id]
        return result

    @classmethod
    def get_comm_sub_department_info(cls, comm_subs_info):
        result = defaultdict(lambda: None)
        for payslip_id, comm_sub_info in comm_subs_info.items():
             department = get_department(
                comm_sub_info,
                consider_commission=True,
                consider_subrogation=True)
             if department:
                result[payslip_id] = department
        return result

    @classmethod
    def get_department_list(cls, payslips, comm_subs_departments):
        departments = [p.department for p in payslips]
        departments += [d for d in comm_subs_departments.values() if d]
        departments = list(set(departments))
        return departments

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        position_name = get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        position_name = position_name if position_name else ''
        return position_name


class PayslipGeneralAdvanceDetail(CompanyReportSignature):
    'Anticipo de rol: Reporte general (Sábana)'
    __name__ = 'payslip.general.advance.detail'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralAdvanceDetail, cls).get_context(
            records, data)

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.advance', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_payslip_advance_report_lines(general, department)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class ConglomerateGeneralRolExport(CompanyReportSignature):
    __name__ = 'conglomerate.general.rol.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            ConglomerateGeneralRolExport, cls).get_context(records, data)
        cls.set_context_fields(report_context, data)

        pool = Pool()
        PayslipGeneral = pool.get('payslip.general')
        general = PayslipGeneral.search([('id', '=', data['general_id'])])[0]
        report_context['general'] = general

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})

        header[general.id] = cls._get_report_header(general)
        departments[general.id] = cls._get_departments(general)
        for department in departments[general.id]:
            lines[general.id].update({
                department.id: cls._get_report_lines(general, department)
            })

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines

        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        PayslipGeneral = pool.get('payslip.general')
        general = PayslipGeneral.search([('id', '=', data['general_id'])])[0]
        report_context['general'] = general

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department)


class PayslipAdvanceSummary(CompanyReportSignature):
    'Aviso de pago (Quincena individual)'
    __name__ = 'payslip.advance.summary'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipAdvanceSummary, cls).get_context(records, data)
        positions = get_positions_info(records)
        report_context['positions'] = positions
        return report_context


class PayslipGeneralAdvanceSummary(CompanyReportSignature):
    'Aviso de pago (Quincena general)'
    __name__ = 'payslip.general.advance.summary'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Payslip = pool.get('payslip.advance')
        lines_to_print = Payslip.search([
            ('general', '=', records[0].id),
        ], order=[
            ('contract.department.name', 'ASC'),
            ('employee.party.name', 'ASC')])
        report_context = super(PayslipGeneralAdvanceSummary, cls).get_context(
            lines_to_print, data)

        positions = get_positions_info(lines_to_print)

        report_context['positions'] = positions
        return report_context


class PayslipGeneralAdvanceResume(CompanyReportSignature):
    __name__ = 'payslip.general.advance.resume'


class PayslipGeneralTotalizedRules(CompanyReportSignature):
    'Resumen general: Totales por rubros'
    __name__ = 'payslip.general.totalized.rules'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralTotalizedRules, cls).get_context(
            records, data)

        header = cls._get_report_header(records)
        totals = cls._get_report_totals(records)
        general_totals = cls._get_report_general_totals_totalized_rules(
            records, header, totals)

        report_context['header'] = header
        report_context['totals'] = totals
        report_context['general_totals'] = general_totals
        return report_context

    @classmethod
    def _get_report_header(cls, generals):
        return get_report_header_totalized_rules(generals)

    @classmethod
    def _get_report_totals(cls, generals):
        return get_report_totals_totalized_rules(generals)

    @classmethod
    def _get_report_general_totals_totalized_rules(cls, generals, header,
            totals):
        return get_report_general_totals_totalized_rules(
            generals, header, totals)


class ReportRulesTypePayslip(CompanyReportSignature):
    __name__ = 'report.rules.type.payslip'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportRulesTypePayslip, cls).get_context(
                records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        Company = pool.get('company.company')
        PayslipTemplate = pool.get('payslip.template')
        Period = pool.get('account.period')
        Rule = pool.get('payslip.rule')

        company = Company.search([('id', '=', data['company'])])[0]
        period = Period.search([('id', '=', data['period_id'])])[0]
        template = PayslipTemplate.search([('id', '=', data['template_id'])])[0]

        report_context['company'] = company

        rules = []
        for id_rule in data['rules_id']:
            rule, = Rule.search([('id', '=', id_rule)])
            rules.append(rule)

        report_context['rules'] = rules
        report_context['period'] = period
        report_context['template'] = template
        report_context['get_payslips'] = cls._get_payslips

    @classmethod
    def _get_payslips(cls, rule, period, template):
        pool = Pool()
        if template.type != 'employer_retirement':
            PayslipLine = pool.get('payslip.line')
            payslip_lines = PayslipLine.search([
                ('rule', '=', rule.id),
                ('payslip.period.id', '=', period.id),
                ('payslip.template.id', '=', template.id),
                ('payslip.state', 'not in', ['cancel']),
            ])
            temp = []
            if template.type == 'accumulated_law_benefits':
                PayslipRetiredLine = pool.get('payslip.retired.line')
                temp = PayslipRetiredLine.search([
                    ('rule', '=', rule.id),
                    ('payslip.period.id', '=', period.id),
                    ('payslip.template.id', '=', template.id),
                    ('payslip.state', 'not in', ['cancel']),
                ])
            if len(temp) > 0:
                payslip_lines += temp
        else:
            PayslipRetiredLine = pool.get('payslip.retired.line')
            payslip_lines = PayslipRetiredLine.search([
                ('rule', '=', rule.id),
                ('payslip.period.id', '=', period.id),
                ('payslip.template.id', '=', template.id),
                ('payslip.state', 'not in', ['cancel']),
            ])
        dict_payslips = {}
        payslips_line = []
        for payslip_line in payslip_lines:
            if not dict_payslips.get(payslip_line.payslip.id):
                dict_payslips[payslip_line.payslip.id] = payslip_line.payslip
                payslips_line.append(payslip_line)
        return payslips_line


class PayslipIndividualTotalizedRulesAccounts(CompanyReportSignature):
    'Resumen individual: Totales por rubros y cuentas'
    __name__ = 'payslip.individual.totalized.rules.accounts'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipIndividualTotalizedRulesAccounts, cls).get_context(
                records, data)

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general, internal_rules=True, accounts=True)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, accounts=True)


class PayslipIndividualTotalizedRules(CompanyReportSignature):
    'Resumen individual: Totales por rubros'
    __name__ = 'payslip.individual.totalized.rules'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipIndividualTotalizedRules, cls).get_context(
                records, data)

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general, internal_rules=True)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department)


class PayslipGeneralTotalToPayEmployees(CompanyReportSignature):
    __name__ = 'payslip.general.total.to.pay.employees'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipGeneralTotalToPayEmployees, cls).get_context(
                records, data)

        payslips = []
        for record in records:
            for payslip in record.lines:
                payslips.append(payslip)

        payslips.sort(key=lambda x: x.employee.party.name)
        positions = get_positions_info(payslips)

        report_context['payslips'] = payslips
        report_context['positions'] = positions
        return report_context


class PayslipIndividualProvisions(CompanyReportSignature):
    'Rol de pagos individual: Provisiones'
    __name__ = 'payslip.individual.provisions'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipIndividualProvisions, cls).get_context(
            records, data)

        # HEADER TYPES
        header = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.payslip', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header_provisions(general)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines_provisions(general, department)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class LawBenefit(CompanyReportSignature):
    __name__ = 'payslip.law.benefit'


class PayslipRetired(CompanyReportSignature):
    'Rol de pago detallado (Rol jubilados individual)'
    __name__ = 'payslip.retired'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipRetired, cls).get_context(records, data)
        report_context['expedition_date'] = datetime.date.today()
        return report_context


class PayslipRetiredSummary(CompanyReportSignature):
    'Aviso de pago (Rol jubilados individual)'
    __name__ = 'payslip.retired.summary'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredSummary, cls).get_context(records, data)
        report_context['expedition_date'] = datetime.date.today()
        return report_context


class PayslipGeneralRetired(CompanyReportSignature):
    'Rol de pagos: Detallado (Rol jubilados general)'
    __name__ = 'payslip.general.retired'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralRetired, cls).get_context(
            records[0].lines, data)
        report_context['expedition_date'] = datetime.date.today()
        return report_context


class PayslipGeneralRetiredSummary(CompanyReportSignature):
    'Rol de pagos: Aviso de pago (Rol jubilados general)'
    __name__ = 'payslip.general.retired.summary'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralRetiredSummary, cls).get_context(
            records[0].lines, data)
        report_context['expedition_date'] = datetime.date.today()
        return report_context


class PayslipGeneralRetiredDetail(CompanyReportSignature):
    'Sábana (Rol jubilados general)'
    __name__ = 'payslip.general.retired.detail'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipGeneralRetiredDetail, cls).get_context(
            records, data)

        # HEADER TYPES
        # Includes only rules (income, deduction) that are part of payslips and
        # they are not necessarily part of the payslip template
        header = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # and they are not necessarily part of the payslip template
        header_with_internals = defaultdict(lambda: {})
        # Includes rules (income, deduction) that are part of payslips plus
        # those that are in the payslip template
        header_all_rules = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # plus those that are in the payslip template
        header_all_rules_with_internals = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            header_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True)
            header_all_rules[record.id] = cls._get_report_header(
                record,
                include_template_rules=True)
            header_all_rules_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True,
                include_template_rules=True)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                key = department.id
                value = cls._get_report_lines(record, department)
                lines[record.id].update({key: value})
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['header_with_internals'] = header_with_internals
        report_context['header_all_rules'] = header_all_rules
        report_context['header_all_rules_with_internals'] = (
            header_all_rules_with_internals)
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        report_context['expedition_date'] = datetime.date.today()
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.retired', general)

    @classmethod
    def _get_report_header(cls, general, internal_rules=False, accounts=False,
            include_template_rules=False):
        return get_report_header(
            general,
            internal_rules=internal_rules,
            accounts=accounts,
            include_template_rules=include_template_rules)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, is_retired_payslip=True)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class PayslipGeneralRetiredDetailRulesAdjustments(CompanyReportSignature):
    'Rol de pagos: Reporte general (Sábana con rubros y ajustes)(Rol jubilados)'
    __name__ = 'payslip.general.retired.rules.adjustments'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipGeneralRetiredDetailRulesAdjustments, cls).get_context(
                records, data)

        # HEADER TYPES
        # Includes only rules (income, deduction) that are part of payslips
        header = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        header_with_internals = defaultdict(lambda: {})
        # Includes rules (income, deduction) that are part of payslips plus
        # those that are in the payslip template
        header_all_rules = defaultdict(lambda: {})
        # Includes rules (income, deduction, internal) that are part of payslips
        # plus those that are in the payslip template
        header_all_rules_with_internals = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            header_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True)
            header_all_rules[record.id] = cls._get_report_header(
                record,
                include_template_rules=True)
            header_all_rules_with_internals[record.id] = cls._get_report_header(
                record,
                internal_rules=True,
                include_template_rules=True)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['header_with_internals'] = header_with_internals
        report_context['header_all_rules'] = header_all_rules
        report_context['header_all_rules_with_internals'] = (
            header_all_rules_with_internals)
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        report_context['expedition_date'] = datetime.date.today()
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.retired', general)

    @classmethod
    def _get_report_header(cls, general, internal_rules=False, accounts=False,
            include_template_rules=False):
        return get_report_header(
            general,
            internal_rules=internal_rules,
            accounts=accounts,
            rule_adjustments=True,
            include_template_rules=include_template_rules)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, is_retired_payslip=True,
            rule_adjustments=True)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class PayslipRetiredIndividualTotalizedRules(CompanyReportSignature):
    'Resumen individual: Totales por rubros (Rol jubilados)'
    __name__ = 'payslip.retired.individual.totalized.rules'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredIndividualTotalizedRules, cls).get_context(
                records, data)

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.retired', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general, internal_rules=True)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, is_retired_payslip=True)


class PayslipRetiredIndividualTotalizedRulesAccounts(CompanyReportSignature):
    'Resumen individual: Totales por rubros y cuentas (Rol jubilados)'
    __name__ = 'payslip.retired.individual.totalized.rules.accounts'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredIndividualTotalizedRulesAccounts, cls).get_context(
                records, data)

        header = defaultdict(lambda: {})
        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.retired', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header(general, internal_rules=True, accounts=True)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines(general, department, is_retired_payslip=True)


class PayslipRetiredGeneralTotalizedRules(CompanyReportSignature):
    'Resumen general: Totales por rubros (rol jubilados)'
    __name__ = 'payslip.retired.general.totalized.rules'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredGeneralTotalizedRules, cls).get_context(
                records, data)

        header = cls._get_report_header(records)
        totals = cls._get_report_totals(records)
        general_totals = cls._get_report_general_totals_totalized_rules(
            records, header, totals)

        report_context['header'] = header
        report_context['totals'] = totals
        report_context['general_totals'] = general_totals
        return report_context

    @classmethod
    def _get_report_header(cls, generals):
        return get_report_header_totalized_rules(generals)

    @classmethod
    def _get_report_totals(cls, generals):
        return get_report_totals_totalized_rules(generals)

    @classmethod
    def _get_report_general_totals_totalized_rules(cls, generals, header,
            totals):
        return get_report_general_totals_totalized_rules(
            generals, header, totals)


class PayslipRetiredGeneralTotalToPayEmployees(CompanyReportSignature):
    __name__ = 'payslip.retired.general.total.to.pay.employees'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredGeneralTotalToPayEmployees, cls).get_context(
                records, data)

        payslips = []
        for record in records:
            for payslip in record.lines:
                payslips.append(payslip)

        payslips.sort(key=lambda x: x.employee.party.name)

        report_context['payslips'] = payslips
        return report_context


class PayslipRetiredIndividualProvisions(CompanyReportSignature):
    'Rol de pagos individual: Provisiones (Rol jubilados)'
    __name__ = 'payslip.retired.individual.provisions'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipRetiredIndividualProvisions, cls).get_context(
                records, data)

        # HEADER TYPES
        header = defaultdict(lambda: {})

        departments = defaultdict(lambda: [])
        lines = defaultdict(lambda: {})
        totals = defaultdict(lambda: {})
        for record in records:
            header[record.id] = cls._get_report_header(record)
            departments[record.id] = cls._get_departments(record)
            for department in departments[record.id]:
                lines[record.id].update({
                    department.id: cls._get_report_lines(record, department)
                })
            totals[record.id] = cls._get_report_totals(
                departments[record.id], lines[record.id])

        report_context['header'] = header
        report_context['departments'] = departments
        report_context['lines'] = lines
        report_context['totals'] = totals
        return report_context

    @classmethod
    def _get_departments(cls, general):
        return _get_departments('payslip.retired', general)

    @classmethod
    def _get_report_header(cls, general):
        return get_report_header_provisions(general)

    @classmethod
    def _get_report_lines(cls, general, department):
        return get_report_lines_provisions(
            general, department, is_retired_payslip=True)

    @classmethod
    def _get_report_totals(cls, departments, _lines):
        return get_report_general_totals(departments, _lines)


class PayslipMedicalCertificateDiscountExport(CompanyReportSignature):
    'Payslip Medical Certificate Discount Export'
    __name__ = 'payslip.medical.certificate.discount.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PayslipMedicalCertificateDiscountExport, cls).get_context(
                records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        PayslipGeneral = pool.get('payslip.general')

        # Payslip generals
        ids_generals = data['ids_generals']
        generals = [g for g in PayslipGeneral.browse(ids_generals)]

        # Payslip individuals
        payslips = []
        for g in generals:
            payslips += [l for l in g.lines]
        payslips.sort(key=lambda x: x.employee)

        dict_percentages = defaultdict(lambda: {})
        for payslip in payslips:
            employee = payslip.employee
            for per in payslip.medical_certificates_discount:
                key = (f'{employee.id}-{per.certificate_type.id}-'
                       f'{per.percentage_payslip}')
                if key not in dict_percentages:
                    dict_percentages.update({
                        key: {
                            'employee': employee,
                            'percentage': per.percentage_payslip,
                            'percentage_iess': per.percentage_payslip_iess,
                            'certificate_type': per.certificate_type,
                            'total_days': per.number_days_month
                        }
                    })
                else:
                    dict_percentages[key]['total_days'] += (
                        per.number_days_month)

        # Last payslips
        dict_payslips = defaultdict(lambda: None)
        for payslip in payslips:
            employee = payslip.employee
            last = dict_payslips[employee.id]
            if not last:
                last = payslip
            elif last.period.end_date <= payslip.period.end_date:
                last = payslip

            if last != dict_payslips[employee.id]:
                dict_payslips[employee.id] = last

        # Commission subrogation info
        dict_comm_subs = cls.get_comm_sub_info(list(dict_payslips.values()))

        report_context['generals'] = generals
        report_context['percentages'] = dict_percentages
        report_context['payslips'] = dict_payslips
        report_context['comm_subs'] = dict_comm_subs
        report_context['department_name'] = cls._get_department_name
        report_context['position_name'] = cls._get_position_name

    @classmethod
    def get_comm_sub_info(cls, payslips):
        result = defaultdict(lambda: None)
        comm_sub_info = get_comm_subs_info(payslips)
        for payslip in payslips:
            contract_id = payslip.contract.id
            if comm_sub_info[contract_id]:
                result[payslip.id] = comm_sub_info[contract_id]
        return result

    @classmethod
    def _get_department_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        department = get_department(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        department_name = department.name if department else ''
        return department_name

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        position_name = get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        position_name = position_name if position_name else ''
        return position_name


class PayslipSubsidiesExport(CompanyReportSignature):
    'Payslip Subsidies Export'
    __name__ = 'payslip.subsidies.export'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PayslipSubsidiesExport, cls).get_context(
            records, data)
        cls.set_context_fields(report_context, data)
        return report_context

    @classmethod
    def set_context_fields(cls, report_context, data):
        pool = Pool()
        PayslipGeneral = pool.get('payslip.general')
        PayslipRule = pool.get('payslip.rule')

        # Payslip generals
        ids_generals = data['ids_generals']
        generals = [g for g in PayslipGeneral.browse(ids_generals)]
        dict_generals = {g.id: g for g in generals}

        # Payslip rules
        ids_rules = data['ids_rules']
        rules = [r for r in PayslipRule.browse(ids_rules)]

        # Rules headers
        dict_headers = {r.code: r.name for r in rules}

        # Individual payslips
        payslips = []
        for general in generals:
            payslips += list(general.lines)
        dict_payslips = {p.id: p for p in payslips}

        # Rules amounts
        dict_rules = cls.get_dict_rules(payslips, rules)

        # Commission subrogation info
        dict_comm_subs = cls.get_comm_sub_info(generals)

        # Additional info
        dict_effective_worked_days = cls.get_dict_from_code_values(
            payslips, 'effective_worked_days')
        dict_antiquity_date = cls.get_dict_from_code_values(
            payslips, 'employee_antiquity_date', default='')
        dict_burdens_subsidy = cls.get_dict_from_code_values(
            payslips, 'total_burdens_with_subsidy')
        dict_burdens_kindergarten = cls.get_dict_from_code_values(
            payslips, 'total_burdens_with_kindergarten')

        # Set context
        report_context['headers'] = dict_headers
        report_context['generals'] = dict_generals
        report_context['payslips'] = dict_payslips
        report_context['rules'] = dict_rules
        report_context['comm_subs'] = dict_comm_subs
        report_context['effective_worked_days'] = dict_effective_worked_days
        report_context['antiquity_date'] = dict_antiquity_date
        report_context['burdens_subsidy'] = dict_burdens_subsidy
        report_context['burdens_kindergarten'] = dict_burdens_kindergarten
        report_context['department_name'] = cls._get_department_name
        report_context['position_name'] = cls._get_position_name

    @classmethod
    def get_dict_rules(cls, payslips, rules):
        result = defaultdict(lambda: 0)
        for payslip in payslips:
            dict_amounts = defaultdict(lambda: Decimal('0.00'))
            for line in payslip.lines:
                if line.rule in rules:
                    dict_amounts[line.rule.code] = line.amount
            result[payslip.id] = dict_amounts
        return result

    @classmethod
    def get_dict_from_code_values(cls, payslips, code, default=0):
        result = defaultdict(lambda: default)
        for payslip in payslips:
            value = get_payslip_dict_code_value(payslip, code)
            value = None if value == 'None' else value
            result[payslip.id] = value
        return result

    @classmethod
    def get_comm_sub_info(cls, generals):
        result = defaultdict(lambda: None)
        for general in generals:
            payslips = general.lines
            comm_sub_info = get_comm_subs_info(payslips)
            for payslip in payslips:
                contract_id = payslip.contract.id
                if comm_sub_info[contract_id]:
                    result[payslip.id] = comm_sub_info[contract_id]
        return result

    @classmethod
    def _get_department_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        department = get_department(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        department_name = department.name if department else ''
        return department_name

    @classmethod
    def _get_position_name(cls, comm_sub_info, consider_commission=True,
            consider_subrogation=False):
        position_name = get_position_name(
            comm_sub_info,
            consider_commission=consider_commission,
            consider_subrogation=consider_subrogation)
        position_name = position_name if position_name else ''
        return position_name