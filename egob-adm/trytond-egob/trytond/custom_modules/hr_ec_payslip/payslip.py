from datetime import date, datetime, timedelta, time

from dateutil.relativedelta import relativedelta
from decimal import Decimal, ROUND_HALF_UP
from collections import defaultdict
from io import BytesIO
from calendar import monthrange, isleap
from operator import attrgetter
import pendulum
from functools import reduce

from sql.operators import Concat, Exists, Abs
from sql.aggregate import Sum, Max
from sql.conditionals import Case, Coalesce
from sql import Null, With, Values, Union
from trytond import backend
import ast

from trytond.model import (ModelSQL, ModelView, Workflow, fields, Unique,
    sequence_ordered)
# from trytond.modules.public_ec.account import show_moves
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.pyson import Eval, If, Bool, Greater, Len, Or
from trytond.transaction import Transaction
from trytond.tools import (grouped_slice, reduce_ids, cursor_dict, Literal,
    file_open)
from trytond.bus import notify

from . import evaluator

import pandas as pd
import numpy as np

from trytond.modules.hr_ec_payslip.hr import _LAW_BENEFITS_PAYMENT_FORMS
from trytond.modules.hr_ec.hr_ec import hr_digits
from trytond.modules.hr_ec.attendance_calculations import get_day
from trytond.modules.hr_ec_payslip import utils

__all__ = ['PayslipEntryType', 'PayslipGeneralEntry', 'PayslipEntry',
    'PayslipLawBenefitType', 'PayslipLawBenefit', 'PayslipTemplate',
    'PayslipTemplateLine', 'PayslipRuleTemplate', 'PayslipRule',
    'PayslipGeneral', 'Payslip', 'PayslipLine', 'CreateRulesStart',
    'CreateRulesSucceed', 'CreateRules', 'PayslipDeductionLoanLine',
    'PayslipFixedIncomeDeduction', 'PayslipManualEntry', 'PayslipOvertimeLine',
    'PayslipCommSubDetail', 'PayslipAbsence', 'PayslipPermission',
    'PayslipDictCodes', 'TemplatePayslipDictCodes',
    'PayslipMedicalCertificateDiscount', 'PayslipWorkshiftDialing',
    'PayslipPermissionLicense', 'PayslipWorkshiftDelayLine']

_STATES = {
    'readonly': Eval('state') != 'draft',
}

_DEPENDS = ['state']

_PAYSLIP_ITEM_TYPES = [
    ('income', 'Ingreso'),
    ('deduction', 'Deducción'),
    ('internal', 'Interno')
]

_PAYSLIP_TEMPLATE_TYPES = [
    ('payslip', 'ROL DE PAGOS MENSUAL'),
    ('accumulated_law_benefits', 'BENEFICIOS DE LEY ACUMULADOS'),
    ('biweekly_advance', 'ANTICIPO QUINCENAL'),
    ('employer_retirement', 'JUBILACIÓN PATRONAL'),
]

_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS = _LAW_BENEFITS_PAYMENT_FORMS + [
    ('not_considered', 'No considerado')
]

_KIND = [
    ('income', 'INGRESO'),
    ('deduction', 'DEDUCCCIÓN'),
]

ZERO = Decimal('0.0')
CENT = Decimal('0.01')
CENT_LB = Decimal('0.0000000001')

DEFAULTDICT_EXTRA_AMOUNT = defaultdict(lambda: defaultdict(lambda: 0))


def normalize_string(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ü", "u"),
        ("Á", "A"),
        ("É", "E"),
        ("Í", "I"),
        ("Ó", "O"),
        ("Ú", "U"),
        ("ü", "u"),
        ("Ü", "U"),
        ("ñ", "n"),
        ("Ñ", "N"),
    )
    for a, b in replacements:
        s = s.replace(a, b)
    return s


def get_default_period_days_info():
    SeveraConfiguratios = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguratios.search([])[0]
    if several_configuration.consider_periods:
        custom_period_days = {
            'type': 'fixed',
            'value': int(several_configuration.days_period)
        }
    else:
        custom_period_days = {
            'type': 'not_fixed',
            'value': None
        }
    return custom_period_days


def get_permit_totals_negative():
    SeveraConfiguratios = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguratios.search([])[0]
    return several_configuration.totals_negative


def get_notification_email_payslip_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.notification_email


def get_rules_set_codes_from_names_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.rules_set_codes_from_names


def get_entry_type_set_codes_from_names_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.entry_type_set_codes_from_names


def get_payslip_template_set_codes_from_names_conf():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration.payslip_template_set_codes_from_names


def get_payslips_configuration():
    SeveraConfiguration = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguration(1)
    return several_configuration


def get_records_considered_in_payslips(model, items, search_field, states=[]):
    search_field = str(search_field)
    result = defaultdict(lambda: None)
    for item in items:
        recovered_records = model.search([
            (search_field, '=', item),
            ('was_considered', '=', True),
            ('payslip.state', 'in', states)
        ])
        if recovered_records:
            result[item.id] = recovered_records[0].payslip.id
    return result


def get_fields_values(model, ids, fields_names):
    fields_values = []
    if ids and fields_names:
        domain = [('id', 'in', ids)]
        fields_values = model.search_read(domain, fields_names=fields_names)
    return fields_values


def get_single_field_value(model, id, field_name):
    value = None
    if id and field_name:
        domain = [('id', '=', id)]
        fields_values = model.search_read(domain, fields_names=[field_name])
        if fields_values:
            value = fields_values[0][field_name]
    return value


def get_allowed_payslip_templates_for_attendance_detail():
    pool = Pool()
    TemplateToTransparencyExports = pool.get('transparency.exports.template')
    codes_templates = TemplateToTransparencyExports.get_code_templates()
    PayslipTemplate = pool.get('payslip.template')
    field_name = 'code'
    fields_values = PayslipTemplate.search_read([
        ('type', 'in', ['payslip'])
    ], fields_names=[field_name])
    result = [field_value[field_name] for field_value in fields_values]
    result = list(set(result))
    # Adjustment payslip templates should not add to the days worked
    is_temp = True
    for a in codes_templates:
        if a in result:
            is_temp = False
    if is_temp:
        result.remove(a)
    return result


def apply_contract_data(Model, payslips):
    for payslip in payslips:
        payslip.department = None
        payslip.position = None
        payslip.work_relationship = None
        payslip.contract_type = None
        payslip.payslip_template_account_budget = None
        if payslip.contract:
            payslip.department = payslip.contract.department
            payslip.position = payslip.contract.position
            payslip.work_relationship = payslip.contract.work_relationship
            payslip.contract_type = payslip.contract.contract_type
            if payslip.contract.payslip_template_account_budget:
                payslip.payslip_template_account_budget = (
                    payslip.contract.payslip_template_account_budget)
    Model.save(payslips)


def get_contracts_with_wrong_domain(generals):
    wrong_records = []
    for general in generals:
        for payslip in general.lines:
            if payslip.contract and payslip.contract.state in ['draft']:
                wrong_records.append(payslip.contract)
    if wrong_records:
        message = utils.get_wrong_records_domain_message(wrong_records)
        return message
    return None


class PayslipEntryType(ModelView, ModelSQL):
    'Payslip Entry type'
    __name__ = 'payslip.entry.type'
    _history = True

    rule = fields.One2Many('payslip.rule', 'entry_type', 'Regla')
    name = fields.Char('Nombre', required=True)
    code = fields.Char('Código', required=True)
    kind = fields.Selection(_KIND, 'Tipo', required=True)
    max_value = fields.Numeric('Valor máximo posible',
        digits=(16, 2))
    is_adjustment_entry = fields.Boolean(
        'Es una entrada de ajuste de regla de rol?',
        help='Indican que este tipo de entrada manual será usada para realizar '
             'ajustes que suman (AJUSTES DE INGRESO) o restan (AJUSTES DE '
             'DEDUCCIÓN) cantidades directamente al valor calculado en una '
             'regla de rolespecífica. Note que estos ajustes no son un nuevo '
             'rubro en el rol de pagos.\n\nAl activarlo, usted debe '
             'seleccionar la regla de rol que será ajustada en el campo que se '
             'habilita para tal efecto. Finalmente, debe serciorarse que la '
             'expresión de la regla seleccionada considera en su fórmula dicho '
             'ajuste.')
    rule_to_adjust = fields.Many2One('payslip.rule', 'Regla que será ajustada',
        states={
            'required': Bool(Eval('is_adjustment_entry')),
            'readonly': ~Bool(Eval('is_adjustment_entry')),
            'invisible': ~Bool(Eval('is_adjustment_entry'))
        }, depends=['is_adjustment_entry'])

    @classmethod
    def __setup__(cls):
        super(PayslipEntryType, cls).__setup__()
        cls._order.insert(0, ('rule_to_adjust', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))
        cls._order.insert(2, ('code', 'ASC'))
        cls._order.insert(3, ('kind', 'ASC'))
        cls._error_messages.update({
            'type_code_uniq': ('El código "%(code)s" que intenta asignar al '
                'tipo de entrada %(name)s ya ha sido usado en otro tipo de '
                'entrada cuyo nombre es "%(name_conflict)s".\n\nRecuerde que '
                'los códigos deben ser únicos.'),
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
        })

    @classmethod
    def copy(cls, entry_types, default=None):
        PayslipEntryType = Pool().get('payslip.entry.type')
        if default is None:
            default = {}
        default = default.copy()
        new_entry_types = []
        i = 1
        for entry_type in entry_types:
            default['code'] = entry_type.code + ' (DUPLICADO ' + str(i) + ')'
            default['name'] = entry_type.name + ' (DUPLICADO ' + str(i) + ')'
            default['rule'] = None
            new_entry_types.extend(super(PayslipEntryType, cls).copy(
                [entry_type], default=default))
            i += 1
        return new_entry_types

    @classmethod
    def validate(cls, entry_types):
        PayslipEntryType = Pool().get('payslip.entry.type')
        for entry_type in entry_types:
            list_types = PayslipEntryType.search([
                ('id', '!=', entry_type.id),
                ('code', '=', entry_type.code),
            ])
            if list_types:
                cls.raise_user_error('type_code_uniq', {
                    'code': entry_type.code,
                    'name': entry_type.name,
                    'name_conflict': list_types[0].name,
                })
        super(PayslipEntryType, cls).validate(entry_types)

    @classmethod
    def write(cls, payslip_entry_types, vals, *args):
        if payslip_entry_types:
            # Search if the code is used in any payslip rule or law benefit type
            utils.set_text_value_on_rules(
                cls, payslip_entry_types, vals, 'code')
        super(PayslipEntryType, cls).write(payslip_entry_types, vals, *args)

    @staticmethod
    def default_is_adjustment_entry():
        return False

    @fields.depends('name', 'code')
    def on_change_with_code(self, name=None):
        set_codes_from_names = get_entry_type_set_codes_from_names_conf()
        if set_codes_from_names:
            if self.name:
                code = normalize_string(self.name).lower().replace(' ', '_')
                return code
            else:
                return None
        return self.code

    @fields.depends('is_adjustment_entry', 'rule_to_adjust')
    def on_change_is_adjustment_entry(self, name=None):
        if not self.is_adjustment_entry:
            self.rule_to_adjust = None

    @fields.depends('kind', 'code', 'name', 'is_adjustment_entry',
        'rule_to_adjust')
    def on_change_rule_to_adjust(self, names=None):
        if self.kind and self.is_adjustment_entry and self.rule_to_adjust:
            r = self.__class__.set_name_and_code(self.rule_to_adjust, self.kind)
            self.name = r['name']
            self.code = r['code']
        else:
            self.name = None
            self.code = None

    @fields.depends('kind', 'code', 'name', 'is_adjustment_entry',
        'rule_to_adjust')
    def on_change_kind(self, names=None):
        if self.kind and self.is_adjustment_entry and self.rule_to_adjust:
            r = self.__class__.set_name_and_code(self.rule_to_adjust, self.kind)
            self.name = r['name']
            self.code = r['code']

    @classmethod
    def set_name_and_code(cls, rule_to_adjust, entry_kind):
        new_code = None
        new_name = None
        is_changed = False
        if entry_kind and rule_to_adjust:
            if rule_to_adjust.code and rule_to_adjust.name:
                new_code = rule_to_adjust.code
                new_name = rule_to_adjust.name
                if entry_kind == 'income':
                    new_name = f'AJUSTE INGRESO {new_name}'
                    new_code = f'ajuste_ingreso_{new_code}'
                elif entry_kind == 'deduction':
                    new_name = f'AJUSTE DEDUCCION {new_name}'
                    new_code = f'ajuste_deduccion_{new_code}'
                else:
                    new_name = f'AJUSTE {new_name}'
                    new_code = f'ajuste_{new_code}'
                is_changed = True

        if not is_changed:
            new_code = None
            new_name = None

        return {
            'code': new_code,
            'name': new_name
        }

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('name',) + tuple(clause[1:]),
            ('code',) + tuple(clause[1:]),
            ('kind',) + tuple(clause[1:]),
            ]
        return domain

    def get_rec_name(self, name):
        return "%s (%s)" % (self.name, self.code)


class PayslipGeneralEntry(Workflow, ModelSQL, ModelView):
    'Payslip General Entry'
    __name__ = 'payslip.general.entry'
    _history = True

    _states = {
        'readonly': Bool(Eval('lines')),
    }

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], states=_STATES, depends=_DEPENDS, readonly=True, required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año Fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_states, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], states=_states, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio periodo'),
        'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin periodo'),
        'on_change_with_period_end_date')
    kind = fields.Selection(_KIND, 'Tipo carga', required=True,
        states=_states, depends=_DEPENDS)
    kind_translated = kind.translated('kind')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado')
    ], 'State', readonly=True, required=True)
    state_translated = state.translated('state')
    type_ = fields.Many2One(
        'payslip.entry.type', 'Tipo entrada', required=True,
        domain=[
            ('kind', '=', Eval('kind')),
        ], states=_states, depends=_DEPENDS + ['kind', 'company'],
        ondelete='RESTRICT')
    excel_file = fields.Binary(
        'Archivo Excel', filename='filename', file_id='file_id', required=True,
        states=_states)
    filename = fields.Char('Nombre de archivo',
        states={
            'invisible': True,
        })
    file_id = fields.Char('ID de archivo')
    lines = fields.One2Many('payslip.entry', 'general', 'Lineas',
        domain=[
            ('kind', '=', Eval('kind')),
            ('type_', '=', Eval('type_')),
            ('company', '=', Eval('company')),
        ], depends=['kind', 'type_', 'company', 'period_start_date',
            'period_end_date'], states=_STATES)
    template = fields.Binary('Plantilla de archivo',
        filename='template_filename', file_id='template_path', readonly=True)
    template_filename = fields.Char('Nombre plantilla',
        states={
            'invisible': True,
        })
    template_path = fields.Char('Path location', readonly=True)
    total_amount = fields.Function(fields.Numeric(
        'Total valor cargado', digits=(16, 2)), 'get_totals_general')
    total_real_amount = fields.Function(fields.Numeric(
        'Total valor aplicado', digits=(16, 2)), 'get_totals_general')
    total_restant_amount = fields.Function(fields.Numeric(
        'Total valor pendiente', digits=(16, 2)), 'get_totals_general')
    entry_name = fields.Function(fields.Char('Nombre de entrada'),
        'get_entry_name')

    def get_rec_name(self, name):
        return '%s / %s (%s)' % (
            self.period.rec_name, self.type_.rec_name, self.kind)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @classmethod
    def __setup__(cls):
        super(PayslipGeneralEntry, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
            ('done', 'draft'),
            ('done', 'cancel')
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel']),
                'depends': ['state'],
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancel', 'draft']),
                'depends': ['state'],
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'cancel']),
                'depends': ['state'],
            },
            'load_lines': {
                'invisible': Eval('state').in_(['done', 'cancel']),
                'depends': ['state'],
            }
        })
        cls._error_messages.update({
            'error_identifier': ('No se ha encontrado en el sistema un '
                'empleado con identificador "%(identifier)s".\n\nPor favor, en '
                'el archivo que está cargando revise la línea del empleado con '
                'el identificador mencionado y corrígalo.'),
            'error_max_value': ('El valor $%(excel_amount)s supera el valor '
                'máximo de carga establecido para el tipo de entrada actual '
                '"%(entry_type)s" fijado en $%(max_value)s.\n\nPor favor, en '
                'el archivo que está cargando revise la línea del empleado con '
                'identificador "%(identifier)s".'),
            'error_no_lines': ('Debe tener mínimo una linea.'),
            'delete_no_draft': ('Debe eliminar únicamente entradas generales '
                'que estén en estado borrador. Por favor revise la entrada de '
                'tipo "%(entry_type)s" del período %(entry_period)s.'),
            'warning_no_delete_lines_used_on_payslips': ('Existen %(cont)s '
                'líneas de entradas manuales que ya han sido agregadas a un '
                'rol de pagos, por lo cual, estas %(cont)s no serán '
                'eliminadas.'),
            'zero_value': ('No es posible cargar registros con valores menores '
                'o iguales a 0.\n\nPor favor, en el archivo que está cargando '
                'revise la línea del empleado con identificador '
                '"%(identifier)s".'),
            'duplicated_registers': ('No es posible cargar dos registros para '
                'el mismo empleado.\n\nPor favor, en el archivo que está '
                'cargando revise las lineas del empleado con identificador '
                '"%(identifier)s" y unifíquelas o elimine una de ellas.'),
            'unavailable_employees': ('Está intentado cargar registros de '
                'empleados que no están activos ya que sus contratos están en '
                'estado finalizado, o inhabilitado, o están con licencia sin '
                'remuneración, o no han sido asociados a un registro de '
                'jubilación patronal:\n\n%(message)s\n\nSi no desea cargar '
                'estos registros, pulse el botón NO de este mensaje, diríjase '
                'a la plantilla, elimine los registros y vuelva a subir el '
                'archivo.\nSi desea cargar estos registros, pulse el botón SI '
                'de este mensaje.')
        })
        cls._order.insert(0, ('period', 'DESC'))
        cls._order.insert(1, ('type_', 'ASC'))

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def delete(cls, generals):
        PayslipEntry = Pool().get('payslip.entry')
        to_delete_lines = []
        cont = 0
        for general in generals:
            if general.state not in ['draft']:
                cls.raise_user_error('delete_no_draft', {
                    'entry_type': general.type_.name,
                    'entry_period': general.period.name
                })
            for line in general.lines:
                if line.payslip:
                    cont += 1
                else:
                    to_delete_lines.append(line)
        if cont > 0:
            cls.raise_user_warning('warning_no_delete_lines_used_on_payslips',
                'warning_no_delete_lines_used_on_payslips', {
                    'cont': cont})
        PayslipEntry.delete(to_delete_lines)
        super(PayslipGeneralEntry, cls).delete(generals)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, generals):
        for general in generals:
            general.change_lines_states('draft')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, generals):
        for general in generals:
            if general.lines:
                general.change_lines_states('done')
            else:
                cls.raise_user_error('error_no_lines')

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, generals):
        for general in generals:
            general.change_lines_states('cancel')

    def change_lines_states(self, new_state):
        pool = Pool()
        Line = pool.get('payslip.entry')
        if new_state == 'draft':
            Line.draft(self.lines)
        elif new_state == 'done':
            Line.done(self.lines)
        elif new_state == 'cancel':
            Line.cancel(self.lines)

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @staticmethod
    def default_template_path():
        return './data/plantilla_entradas_manuales.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('plantilla_entradas_manuales', 'xlsx')

    @staticmethod
    def default_template():
        path = 'hr_ec_payslip/data/plantilla_entradas_manuales.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @classmethod
    @ModelView.button
    def load_lines(cls, generals):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        Entry = pool.get('payslip.entry')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        PartyIdentifier = pool.get('party.identifier')

        employee_table = Employee.__table__()
        party_table = Party.__table__()
        identifier_table = PartyIdentifier.__table__()

        employees = {}

        query_employees = employee_table.join(party_table,
            condition=employee_table.party == party_table.id
        ).join(identifier_table,
            condition=identifier_table.party == party_table.id
        ).select(
            employee_table.id,
            identifier_table.code,
        )

        cursor.execute(*query_employees)
        for row in cursor.fetchall():
            employees[row[1]] = row[0]

        field_names = ['CEDULA', 'APELLIDOS_NOMBRES', 'RAZON', 'FECHA', 'VALOR']
        to_save = []
        to_delete = []

        for general in generals:
            to_delete += general.lines
            considered_identifiers = []

            file_data = fields.Binary.cast(general.excel_file)
            df = pd.read_excel(BytesIO(file_data), names=field_names,
                dtype='object', header=0)
            df = df.fillna(False)

            cls.check_employees_availability(general, df, employees)

            for row in df.itertuples():
                ci = str(row.CEDULA)
                ci = ci if len(ci) == 10 else f"{0}{ci}"
                if employees.get(ci):
                    excel_amount = utils.quantize_currency(row.VALOR)
                    if ci not in considered_identifiers:
                        if excel_amount > 0:
                            max_value = general.type_.max_value
                            if (not max_value) or (excel_amount <= max_value):
                                employee = Employee(employees.get(ci))
                                entry = Entry()
                                entry.general = general
                                entry.company = general.company
                                entry.kind = general.kind
                                entry.type_ = general.type_
                                entry.employee = employee
                                entry.reason = row.RAZON
                                entry.entry_date = row.FECHA
                                entry.amount = excel_amount
                                entry.real_amount = excel_amount

                                considered_identifiers.append(ci)
                                to_save.append(entry)
                            else:
                                cls.raise_user_error('error_max_value', {
                                    'max_value': general.type_.max_value,
                                    'excel_amount': excel_amount,
                                    'identifier': ci,
                                    'entry_type': general.type_.name
                                })
                        else:
                            cls.raise_user_error('zero_value', {
                                'identifier': ci,
                            })
                    else:
                        if excel_amount > 0:
                            t_employee = Employee(employees.get(ci))
                            for obj in to_save:
                                if obj.employee == t_employee:
                                    obj.amount += excel_amount
                                    obj.real_amount += excel_amount
                        # cls.raise_user_error('duplicated_registers', {
                        #     'identifier': ci,
                        # })
                else:
                    cls.raise_user_error('error_identifier', {
                        'identifier': ci,
                    })
        Entry.delete(to_delete)
        Entry.save(to_save)

    @classmethod
    def check_employees_availability(cls, general, excel_data, employee_ids):
        pool = Pool()
        Contract = pool.get('company.contract')
        Retired = pool.get('company.retired')
        PayslipGeneral = pool.get('payslip.general')

        payslips_conf = get_payslips_configuration()
        consider_finalized_contracts_payslip_month = (payslips_conf.
            consider_finalized_contracts_during_payslip_period)

        unavailables = []

        # Available contracts
        _domain = [
            ('state', 'not in', ['cancel'])
        ]
        if consider_finalized_contracts_payslip_month:
            _domain.append(['OR',
                ('end_date', '=', None),
                ('end_date', '>=', general.period.start_date)
            ])
        else:
            _domain.append(['OR',
                ('end_date', '=', None),
                ('end_date', '>=', general.period.end_date)
            ])

        contracts = Contract.search(_domain)
        contracts = PayslipGeneral.remove_contracts_with_unpaid_license(
            general.period, contracts)
        contracts = PayslipGeneral.remove_disabled_contracts(
            general.period, contracts)

        # Available retireds
        retireds = Retired.search([])

        # Available employees
        employees_available = []
        employees_available += [c.employee.id for c in contracts]
        employees_available += [r.employee.id for r in retireds]
        employees_available = list(set(employees_available))

        for row in excel_data.itertuples():
            ci = str(row.CEDULA)
            ci = ci if len(ci) == 10 else f"{0}{ci}"
            employee_id = employee_ids.get(ci)
            if employee_id:
                if employee_id not in employees_available:
                    names = str(row.APELLIDOS_NOMBRES)
                    unavailables.append(f'{names} / {ci}')
        if unavailables:
            message, count = '', 1
            for unavailable in unavailables:
                message += f'{count}) {unavailable}\n'
                count += 1
            cls.raise_user_warning('unavailable_employees',
                'unavailable_employees', {'message': message})

    @classmethod
    def get_totals_general(cls, generals, names=None):
        result_total_amount = defaultdict(lambda: 0)
        result_total_real_amount = defaultdict(lambda: 0)
        result_total_restant_amount = defaultdict(lambda: 0)
        for general in generals:
            result_total_amount[general.id] = 0
            result_total_real_amount[general.id] = 0
            result_total_restant_amount[general.id] = 0
            for line in general.lines:
                if line.state not in ['cancel']:
                    if line.amount:
                        result_total_amount[general.id] += line.amount
                    if line.real_amount:
                        result_total_real_amount[general.id] += line.real_amount
                    if line.restant_amount:
                        result_total_restant_amount[general.id] += (
                            line.restant_amount)
        return {
            'total_amount': result_total_amount,
            'total_real_amount': result_total_real_amount,
            'total_restant_amount': result_total_restant_amount
        }

    @classmethod
    def get_entry_name(cls, generals, names=None):
        result = defaultdict(lambda: None)
        for general in generals:
            if general.type_ and general.kind:
                result[general.id] = f'{general.type_.name}'
        return {
            'entry_name': result
        }

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('period',) + tuple(clause[1:]),
            ('kind',) + tuple(clause[1:]),
            ('type_',) + tuple(clause[1:]),
        ]
        return domain


class PayslipEntry(Workflow, ModelSQL, ModelView):
    'Payslip Entry'
    __name__ = 'payslip.entry'
    _history = True

    _states = {
        'readonly': Bool(Eval('state') != 'draft'),
    }

    company = fields.Many2One('company.company', 'Empresa',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS, readonly=True, required=True)
    general = fields.Many2One('payslip.general.entry', 'Entrada general',
        domain=[
            ('company', '=', Eval('company')),
            ('kind', '=', Eval('kind')),
            ('type_', '=', Eval('type_')),
            ('period.start_date', '<=', Eval('entry_date')),
            ('period.end_date', '>=', Eval('entry_date')),
            ('state', 'not in', ['cancel'])
        ], states=_STATES, depends=(
            _DEPENDS + ['company', 'kind', 'type_', 'entry_date']),
        ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Empleado', required=True,
        domain=[
            ('company', '=', Eval('company')),
        ], states=_states, depends=_DEPENDS + ['company'])
    kind = fields.Selection(_KIND, 'Tipo carga', required=True, states=_states,
        depends=_DEPENDS)
    kind_translated = kind.translated('kind')
    reason = fields.Char('Razón', required=True, states=_states,
        depends=_DEPENDS)
    entry_date = fields.Date('Fecha de entrada', required=True, states=_states,
        domain=[
            If(Bool(Eval('_parent_general')),
               ('entry_date', '>=',
                Eval('_parent_general', {}).get('period_start_date')),
               ()),
            If(Bool(Eval('_parent_general')),
               ('entry_date', '<=',
                Eval('_parent_general', {}).get('period_end_date')),
               ()),
        ], depends=_DEPENDS + ['general'])
    amount = fields.Numeric(
        'Valor cargado', help='Cantidad cargada inicialmente.',
        required=True, states=_states, depends=_DEPENDS,
        digits=(16, 2), domain=[
            ('amount', '>', 0),
        ])
    real_amount = fields.Numeric(
        'Valor aplicado', help='Cantidad real de ingreso o deducción a aplicar',
        required=True, digits=(16, 2),
        domain=[
            ('real_amount', '>=', 0),
        ],
        states={
            'readonly': ~Bool(Eval('payslip_state').in_([None, 'draft']))
        }, depends=['amount', 'payslip', 'payslip_state'])
    restant_amount = fields.Function(fields.Numeric(
        'Valor pendiente', help='Valor cargado - Valor aplicado',
        required=True, readonly=True,
        digits=(16, 2)), 'get_restant_amount')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado')
    ], 'State', readonly=True, required=True)
    state_translated = state.translated('state')
    type_ = fields.Many2One(
        'payslip.entry.type', 'Tipo de entrada', required=True,
        domain=[
            ('kind', '=', Eval('kind')),
        ], states=_states, depends=_DEPENDS + ['kind', 'company'],
        ondelete='RESTRICT')
    retroactive_difference = fields.Boolean('Retroactivo o Diferencia?')

    # INFO Paylips in which it was used
    payslip = fields.Function(fields.Many2One('payslip.payslip',
        'Rol individual', readonly=True), 'get_payslip',
        searcher='search_payslip')
    payslip_state = fields.Function(fields.Selection([
        (None, ''),
        ('draft', 'Borrador'),
        ('confirm', 'Confirmado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado')
    ], 'Estado de rol'), 'get_payslip_state')
    payslip_manual_entries = fields.One2Many('payslip.payslip.manual.entry',
        'entry', 'Entrada en roles')

    @classmethod
    def __setup__(cls):
        super(PayslipEntry, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('payslip_entry_uniq', Unique(t, t.employee, t.type_, t.general),
             'Code must be unique'),
        ]
        cls._error_messages.update({
            'error_max_value': (
                'El registro %(id)s, no puede exceder el Valor Máximo de Carga '
                'en este Tipo de Entrada: %(max_value)s.'),
            'no_cancel': ('No puede cancelar la entrada "%(entry)s" por que ya '
                'ha sido agregada al rol de pagos "%(payslip)s".'),
            'used_on_payslip': ('No puede "Cancelar" este registro ya que se '
                'usa en "%(payslip)s".'),
            'no_draft_used_on_payslip': ('No puede pasar a "Borrador" el '
                'registro de la entrada manual debido a que ya fue agregado a '
                '"%(payslip)s".'),
            'delete_no_draft': ('Debe eliminar únicamente entradas manuales '
                'que estén en estado borrador. Por favor revise la entrada del '
                'empleado %(employee)s de tipo "%(entry_type)s" del período '
                '%(entry_date)s.'),
            'warning_delete_info': ('Usted desea eliminar %(total_selected)s '
                'registros; sin embargo, se han presentado los siguientes '
                'detalles que debe considerar antes de eliminarlos: '
                '%(message)s')
        })

        cls._transitions |= set((
            ('draft', 'done'),
            ('done', 'draft'),
            ('done', 'cancel')
        ))
        cls._buttons.update({
            'draft': {
                'invisible': (Eval('state').in_(['draft', 'cancel'])) | (
                    ~Bool(Eval('payslip_state').in_([None, 'draft']))),
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancel', 'draft']) | (
                    ~Bool(Eval('payslip_state').in_([None, 'draft']))),
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'cancel']),
            }
        })
        cls._order.insert(0, ('entry_date', 'DESC'))
        cls._order.insert(1, ('state', 'ASC'))
        cls._order.insert(2, ('employee', 'ASC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def delete(cls, entries):
        PayslipManualEntry = Pool().get('payslip.payslip.manual.entry')

        del_payslip_entries_draft_payslip = []
        del_entries_draft_payslip = []
        del_payslip_entries_cancel_payslip = []
        del_entries_cancel_payslip = []
        del_payslip_entries = []
        del_entries = []

        preserve_payslip_entries = []
        preserve_entries = []

        to_delete_payslip_entries = []
        to_delete_entries = []

        for entry in entries:
            if entry.state not in ['draft', 'cancel']:
                cls.raise_user_error('delete_no_draft', {
                    'employee': entry.employee.rec_name,
                    'entry_type': entry.type_.name,
                    'entry_date': entry.entry_date
                })
            else:
                if not entry.payslip_manual_entries:
                    del_entries.append(entry)
                else:
                    for ppe in entry.payslip_manual_entries:
                        if not ppe.was_considered:
                            del_payslip_entries.append(ppe)
                            del_entries.append(ppe.entry)
                        else:
                            payslip = ppe.payslip
                            if payslip.state == 'cancel':
                                del_payslip_entries_cancel_payslip.append(ppe)
                                del_entries_cancel_payslip.append(ppe.entry)
                            elif payslip.state == 'draft':
                                del_payslip_entries_draft_payslip.append(ppe)
                                del_entries_draft_payslip.append(ppe.entry)
                            else:
                                preserve_payslip_entries.append(ppe)
                                preserve_entries.append(ppe.entry)

        if (del_payslip_entries_cancel_payslip or
                del_payslip_entries_draft_payslip or preserve_payslip_entries):
            del_entries_cancel_payslip = list(set(del_entries_cancel_payslip))
            del_entries_draft_payslip = list(set(del_entries_draft_payslip))
            del_entries = list(set(del_entries))
            preserve_entries = list(set(preserve_entries))

            warning_message = cls.get_warning_deletion_message(
                len(del_entries_cancel_payslip),
                len(del_entries_draft_payslip),
                len(preserve_entries),
                len(del_entries))

            cls.raise_user_warning('warning_delete_info', 'warning_delete_info',
                {'total_selected': len(entries), 'message': warning_message})

            # TO DELETE RECORDS OF INTERMEDIATE TABLE
            to_delete_payslip_entries += (
                del_payslip_entries +
                del_payslip_entries_cancel_payslip +
                del_payslip_entries_draft_payslip)

            # TO DELETE ENTRIES
            to_delete_entries += (
                del_entries +
                del_entries_cancel_payslip +
                del_entries_draft_payslip)

            # DELETE RECORDS OF INTERMEDIATE TABLE AND ENTRY TABLE
            to_delete_payslip_entries = list(set(to_delete_payslip_entries))
            to_delete_entries = list(set(to_delete_entries))
            PayslipManualEntry.delete(to_delete_payslip_entries)
            super(PayslipEntry, cls).delete(to_delete_entries)
        else:
            to_delete_payslip_entries = list(set(del_payslip_entries))
            to_delete_entries = list(set(del_entries))
            PayslipManualEntry.delete(to_delete_payslip_entries)
            super(PayslipEntry, cls).delete(to_delete_entries)

    @classmethod
    def get_warning_deletion_message(cls, total_entries_cancel_payslip,
        total_entries_draft_payslip, total_entries_to_preserve,
            total_entries):
        warning_message = ""
        if total_entries_cancel_payslip > 0:
            warning_message += (
                f'\n\nENTRADAS USADAS EN UN ROL EN ESTADO "CANCELADO": '
                f'{total_entries_cancel_payslip}\n*Luego de '
                f'eliminarlas, estas desapareceran de los registros del '
                f'rol cancelado.')

        if total_entries_draft_payslip > 0:
            warning_message += (
                f'\n\nENTRADAS USADAS EN UN ROL EN ESTADO "BORRADOR": '
                f'{total_entries_draft_payslip}\n*Luego de '
                f'eliminarlas, usted deberá volver a generar los valores '
                f'de los roles involucrados.')

        if total_entries_to_preserve > 0:
            warning_message += (
                f'\n\nENTRADAS QUE NO SERÁN ELIMINADAS: '
                f'{total_entries_to_preserve}.\n*Estas entradas no se '
                f'eliminarán debido a que forman parte de un rol de pagos '
                f'que no está en estado borrador o cancelado.')

        if total_entries > 0:
            warning_message += (
                f'\n\nLas demás entradas ({total_entries}) no presentan '
                f'inconvenientes.')
        return warning_message

    @classmethod
    def validate(cls, types):
        for type in types:
            if type.type_.max_value:
                if type.type_.max_value < type.amount:
                    cls.raise_user_error('error_max_value', {
                        'max_value': type.type_.max_value,
                        'id': type.id,
                    })

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, items):
        for item in items:
            if item.payslip:
                cls.raise_user_error('no_draft_used_on_payslip', {
                    'payslip': item.payslip.rec_name
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, items):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, items):
        for item in items:
            if item.payslip:
                cls.raise_user_error('used_on_payslip', {
                    'payslip': item.payslip.rec_name
                })

    @classmethod
    def get_payslip(cls, items, names):
        PayslipManualEntry = Pool().get('payslip.payslip.manual.entry')
        result = get_records_considered_in_payslips(
            PayslipManualEntry, items, 'entry', states=['confirm', 'done'])
        return {
            'payslip': result
        }

    @classmethod
    def get_payslip_state(cls, items, names):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        result = defaultdict(lambda: None)
        payslips = get_records_considered_in_payslips(
            PayslipManualEntry, items, 'entry', states=['confirm', 'done'])
        for item_id, payslip_id in payslips.items():
            p = Payslip.search([
                'id', '=', payslip_id
            ])
            result[item_id] = p[0].state if p else None
        return {
            'payslip_state': result
        }

    @fields.depends('real_amount', 'amount')
    def on_change_with_restant_amount(self, name=None):
        restant = Decimal("0")
        if self.amount:
            if self.real_amount:
                restant = self.amount - self.real_amount
            else:
                restant = self.amount
        restant = restant if restant >= 0 else 0
        restant = utils.quantize_currency(restant)
        return restant

    @fields.depends('amount', 'real_amount', 'restant_amount')
    def on_change_amount(self, name=None):
        real = Decimal('0.0')
        restant = Decimal('0.0')
        if self.amount:
            amount = utils.quantize_currency(self.amount)
            real = amount
            restant = amount - real
            restant = restant if restant >= 0 else 0
        self.real_amount = real
        self.restant_amount = restant

    @classmethod
    def get_restant_amount(cls, manual_entries, names=None):
        result = defaultdict(lambda: 0)
        for entry in manual_entries:
            result[entry.id] = entry.amount - entry.real_amount
        return {
            'restant_amount': result
        }

    def get_rec_name(self, name):
        return '%s: %s (%s)' % (
            self.type_.rec_name, self.amount, self.entry_date)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('employee',) + tuple(clause[1:]),
            ('type_',) + tuple(clause[1:]),
            ('kind',) + tuple(clause[1:]),
        ]
        return domain

    @classmethod
    def search_payslip(cls, name, clause):
        domain = []
        if clause[1] == '=' and clause[2] == None:
            domain = ['OR',
                ('payslip_manual_entries', '=', None),
                ('payslip_manual_entries.was_considered', '=', False),
                ('payslip_manual_entries.payslip.state', 'in',
                    ['draft', 'cancel']),
            ]
        else:
            domain = ['OR',
                ('payslip_manual_entries.payslip.period.name',) +
                      tuple(clause[1:]),
                ('payslip_manual_entries.payslip.template.name',) +
                      tuple(clause[1:]),
                ('payslip_manual_entries.was_considered', '=', True),
                ('payslip_manual_entries.payslip.state', 'not in',
                    ['draft', 'cancel']),
            ]
        return domain

    def _order_payslip(name):
        def order_field(tables):
            pool = Pool()
            PayslipManualEntry = pool.get('payslip.payslip.manual.entry')

            field = PayslipManualEntry._fields[name]
            tbl_manual_entry, _ = tables[None]
            tbl_payslip_manual_entry = PayslipManualEntry.__table__()

            condition = (tbl_payslip_manual_entry.entry == tbl_manual_entry.id)
            payslip_tables = {
                None: (tbl_payslip_manual_entry, condition),
            }
            tables['payslip'] = payslip_tables
            return field.convert_order(name, payslip_tables, PayslipManualEntry)
        return staticmethod(order_field)
    order_payslip = _order_payslip('payslip')


class PayslipLawBenefitType(ModelSQL, ModelView):
    'Payslip Law Benefit'
    __name__ = 'payslip.law.benefit.type'
    _history = True

    rule = fields.One2Many('payslip.rule', 'law_benefit_type', 'Regla')
    code = fields.Char('Código',
        states={
            'readonly': (Bool(Eval('code').in_(['xiii', 'xiv', 'fr'])) &
                ~Bool(Eval('id').in_([None, -1])))
        }, depends=['id'], required=True)
    name = fields.Char('Nombre', required=True)
    expression = fields.Text('Expresión', required=True)

    @classmethod
    def __setup__(cls):
        super(PayslipLawBenefitType, cls).__setup__()
        t = cls.__table__()
        # TODO DELETE THIS CONTRAINT IN ORDER TO USE THE VALIDATE METHOD
        cls._sql_constraints += [
            ('benetif_type_code_uniq', Unique(t, t.code),
                'El código del beneficio de ley debe ser único.'),
        ]
        cls._error_messages.update({
            'no_delete_preloaded_law_benefits': ('No puede eliminar tipos de '
                'beneficios de ley que vienen precargados en el sistema '
                '(xiii: Décimo tercero, xiv: Décimo cuarto, fr: Fondos de '
                'reserva).'),
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
            'benetif_type_code_uniq': ('El código "%(lb_code)"s que intenta '
                'asignar al tipo de beneficio de ley %(lb_name)s ya ha sido '
                'usado en el tipo de beneficio de ley con nombre '
                '%(lb_name_conflict)s. Recuerde que los códigos deben ser '
                'únicos.')
        })

    @classmethod
    def validate(cls, law_benefit_types):
        PayslipLawBenefitType = Pool().get('payslip.law.benefit.type')
        for lb_type in law_benefit_types:
            list_types = PayslipLawBenefitType.search([
                ('id', '!=', lb_type.id),
                ('code', '=', lb_type.code),
            ])
            if list_types:
                cls.raise_user_error('benetif_type_code_uniq', {
                    'lb_code': lb_type.code,
                    'lb_name': lb_type.name,
                    'lb_name_conflict': list_types[0].name,
                })

    @classmethod
    def delete(cls, items):
        for item in items:
            if item.code in ['xiii', 'xiv', 'fr']:
                cls.raise_user_error('no_delete_preloaded_law_benefits')
        super(PayslipLawBenefitType, cls).delete(items)

    def get_rec_name(self, name):
        return '%s (%s)' % (self.name, self.code)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('name',) + tuple(clause[1:]),
            ('code',) + tuple(clause[1:])
            ]
        return domain

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    def write(cls, law_benefits, vals, *args):
        if law_benefits:
            # Search if the code is used in any payslip rule
            utils.set_text_value_on_rules(
                cls, law_benefits, vals, 'code')
        super(PayslipLawBenefitType, cls).write(law_benefits, vals, *args)


class PayslipLawBenefit(ModelSQL, ModelView):
    'Payslip Law Benefit'
    __name__ = 'payslip.law.benefit'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], readonly=True, required=True)
    period = fields.Many2One('account.period', 'Periodo generación',
        required=True, readonly=True)
    employee = fields.Many2One(
        'company.employee', 'Empleado', required=True,
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], readonly=True)
    payslip = fields.Many2One('payslip.payslip', 'Rol en el que se pagó',
        readonly=True,
        domain=[
            ('employee', '=', Eval('employee')),
        ], depends=['employee'], ondelete='CASCADE')
    payslip_date = fields.Function(fields.Date('Fecha de rol'),
        'on_change_with_payslip_date')
    amount = fields.Numeric(
        'Valor', required=True, readonly=True, digits=(16, 10))
    kind = fields.Selection(
        [
            ('monthlyse', 'Mensualizado'),
            ('accumulate', 'Acumulado'),
            ('liquidation', 'Liquidacion'),
            ('adjustment', 'Ajuste XIV'),
        ], 'Tipo', required=True, readonly=True)
    kind_translated = kind.translated('kind')
    type_ = fields.Many2One('payslip.law.benefit.type', 'Tipo', required=True,
        readonly=True, ondelete='RESTRICT')
    is_paid_in_liquidation = fields.Boolean('Fué pagado en liquidación ?',
        states={'invisible': True})

    # Payslip in which this law benefit was generated
    generator_payslip = fields.Many2One('payslip.payslip',
        'Rol que generó', states={'readonly': True})
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    budget = fields.Many2One('public.budget', 'Partida')

    # Liquidatión in which this law benefit was generated
    generator_liquidation = fields.Many2One('contract.liquidation',
        'liquidación que generó', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(PayslipLawBenefit, cls).__setup__()
        cls._error_messages.update({
            'no_delete': ('No puedes eliminar el "Beneficio de ley" debido a'
                ' que está siendo usado en la tabla de "Beneficios de ley" de '
                ' un "Rol de pagos".'),
            'duplicate_law_benefits_error': ('Los beneficios de ley no deben'
                ' duplicarse.')
        })
        cls._order.insert(0, ('period.end_date', 'DESC'))
        cls._order.insert(1, ('employee.party.name', 'ASC'))
        cls._order.insert(2, ('type_.name', 'ASC'))

    @staticmethod
    def default_is_paid_in_liquidation():
        return False

    @classmethod
    def delete(cls, benefits):
        if Transaction().context.get('from_payslip'):
            super(PayslipLawBenefit, cls).delete(benefits)
        else:
            cls.raise_user_error('no_delete')

    @classmethod
    def copy(cls, benefits, default=None):
        # The law benefits should not be duplicated
        for benefit in benefits:
            benefit.raise_user_error('duplicate_law_benefits_error')
        return []

    @fields.depends('payslip', 'period', 'kind')
    def on_change_with_payslip_date(self, name=None):
        payslip_date = None
        if self.payslip and self.period and self.kind:
            if (self.payslip.template_for_accumulated_law_benefits and
                    self.kind == 'adjustment'):
                payslip_date = self.payslip.end_date
            else:
                payslip_date = self.period.end_date
        return payslip_date

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('type_',) + tuple(clause[1:]),
            ('payslip',) + tuple(clause[1:])
            ]
        return domain

    def get_rec_name(self, name):
        return (f"{self.period.name} - {self.type_.name} - "
            f"{self.kind_translated} - {self.amount}")


class PayslipTemplate(ModelSQL, ModelView):
    'Payslip Template'
    __name__ = 'payslip.template'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        }, required=True)
    code = fields.Char('Código', required=True)
    name = fields.Char('Nombre', required=True)
    lines = fields.One2Many('payslip.template.line', 'template', 'Líneas')
    accumulated_law_benefits = fields.Boolean(
        'Para calcular los beneficios de ley acumulados',
        help='Indica si la objetivo de este rol es calcular todos los '
             'beneficios de ley acumulados de un tipo específico o no.',
        states={
            'invisible': True,
        })
    law_benefit_type = fields.Many2One('payslip.law.benefit.type',
        'Beneficio de ley a acumular',
        domain=[
            ('code', 'in', ['xiii', 'xiv'])
        ],
        states={
            'required': Bool(Eval('accumulated_law_benefits')),
            'invisible': ~Bool(Eval('accumulated_law_benefits')),
        }, depends=['accumulated_law_benefits'])
    type = fields.Selection(_PAYSLIP_TEMPLATE_TYPES, 'Tipo',
        states={
            'required': True
        })

    @classmethod
    def __setup__(cls):
        super(PayslipTemplate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('template_code_uniq', Unique(t, t.code), 'El código de la '
                'plantilla de rol de pagos debe ser único.'),
        ]
        cls._order.insert(0, ('name', 'ASC'))
        cls._order.insert(1, ('code', 'ASC'))

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        cursor = Transaction().connection.cursor()
        table = cls.__table__()

        if TableHandler.table_exist(cls._table):
            table_h = cls.__table_handler__(module_name)
            exists_type = table_h.column_exist('type')

        # FIRST: Update the new fields of the model
        super(PayslipTemplate, cls).__register__(module_name)

        # SECOND: If the 'type' column did not exist previously (but at this
        # point it was already created), its values are updated according to
        # the conditions of the 'where'
        if not exists_type:
            cursor.execute(
                *table.update(
                    [table.type], ['accumulated_law_benefits'],
                    where=(table.accumulated_law_benefits == True)
                )
            )

    @classmethod
    def copy(cls, templates, default=None):
        if default is None:
            default = {}
        default = default.copy()
        new_reports = []
        i = 1
        for report in templates:
            default['code'] = report.code + ' (DUPLICADO ' + str(i) + ')'
            default['name'] = report.name + ' (DUPLICADO ' + str(i) + ')'
            new_reports.extend(super(PayslipTemplate, cls).copy([report],
                default=default))
            i += 1
        return new_reports

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'payslip'

    @classmethod
    def validate(cls, templates):
        super(PayslipTemplate, cls).validate(templates)

    @fields.depends('accumulated_law_benefits')
    def on_change_with_law_benefit_type(self, fieldnames=None):
        if not self.accumulated_law_benefits:
            return None

    @fields.depends('type', 'accumulated_law_benefits', 'law_benefit_type')
    def on_change_type(self, name=None):
        if self.type and self.type == 'accumulated_law_benefits':
            self.accumulated_law_benefits = True
        else:
            self.accumulated_law_benefits = False
            self.law_benefit_type = None

    @fields.depends('name', 'code')
    def on_change_with_code(self, name=None):
        set_codes_from_names = get_payslip_template_set_codes_from_names_conf()
        if set_codes_from_names:
            if self.name:
                code = normalize_string(self.name).lower().replace(' ', '_')
                return code
            else:
                return None
        return self.code


class PayslipTemplateLine(sequence_ordered(), ModelSQL, ModelView):
    'Payslip Template Line'
    __name__ = 'payslip.template.line'
    _history = True

    template = fields.Many2One(
        'payslip.template', 'Plantilla', required=True, ondelete='CASCADE')
    rule = fields.Many2One('payslip.rule', 'Reglas', required=True,
        domain=[
            ('company', '=', Eval('_parent_template', {}).get('company', -1)),
        ], depends=['template'])


class PayslipRuleTemplate(ModelSQL, ModelView):
    'Payslip Rule Template'
    __name__ = 'payslip.rule.template'
    _history = True

    code = fields.Char('Código', required=True)
    name = fields.Char('Nombre', required=True)
    type_ = fields.Selection(_PAYSLIP_ITEM_TYPES, 'Tipo de regla',
        required=True)
    payslip_position = fields.Selection(
        [
            ('left', 'Izquierda'),
            ('right', 'Derecha'),
            ('general', 'General'),
            ('other', 'Otro'),
        ], 'Posición en rol de pagos',
        states={
            'invisible': True
        }, required=True, sort=False)
    condition = fields.Text('Condición', required=True)
    expression = fields.Text('Expresión', required=True)
    is_law_benefit = fields.Boolean("¿Es un beneficio de ley (xii, xiv, fr)?",
        states={
            'readonly': (Bool(Eval('is_manual_entry')) |
                Bool(Eval('is_fixed_income_deduction')))
        }, depends=['is_manual_entry', 'is_fixed_income_deduction'])
    is_manual_entry = fields.Boolean("¿Es una entrada manual (Excel)?",
        states={
            'readonly': (Bool(Eval('is_law_benefit')) |
                Bool(Eval('is_fixed_income_deduction')))
        }, depends=['is_law_benefit', 'is_fixed_income_deduction'])
    is_fixed_income_deduction = fields.Boolean("¿Es un ingreso/deducción fijo?",
       states={
           'readonly': (Bool(Eval('is_law_benefit')) |
                Bool(Eval('is_manual_entry')))
       }, depends=['is_law_benefit', 'is_manual_entry'])
    entry_type = fields.Many2One('payslip.entry.type', 'Tipo de entrada',
        states={
            'required': Bool(Eval('is_manual_entry')),
            'invisible': ~Bool(Eval('is_manual_entry'))
        }, depends=['is_manual_entry'], ondelete='RESTRICT')
    fixed_income_deduction_type = fields.Many2One(
        'company.contract.type.income.deduction', 'Tipo de I/D fijo',
        states={
            'required': Bool(Eval('is_fixed_income_deduction')),
            'invisible': ~Bool(Eval('is_fixed_income_deduction'))
        }, depends=['is_fixed_income_deduction'], ondelete='RESTRICT')
    law_benefit_type = fields.Many2One('payslip.law.benefit.type',
        'Tipo de beneficio',
        states={
            'required': Bool(Eval('is_law_benefit')),
            'invisible': ~Bool(Eval('is_law_benefit'))
        }, depends=['is_law_benefit'], ondelete='RESTRICT')
    notes = fields.Text('Notas')

    @classmethod
    def __setup__(cls):
        super(PayslipRuleTemplate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('rule_code_uniq', Unique(t, t.code), 'No pueden existir dos '
                'reglas de rol de pagos con el mismo código'),
        ]
        cls._order.insert(0, ('name', 'ASC'))
        cls._order.insert(1, ('code', 'ASC'))

    @staticmethod
    def default_condition():
        return 'True'

    @staticmethod
    def default_is_law_benefit():
        return False

    @staticmethod
    def default_is_manual_entry():
        return False

    @staticmethod
    def default_is_fixed_income_deduction():
        return False

    def _get_rule_value(self, rule=None):
        """
        # Set the values for rule creation.
        """
        res = {}
        if not rule or rule.name != self.name:
            res['name'] = self.name
        if not rule or rule.code != self.code:
            res['code'] = self.code
        if not rule or rule.type_ != self.type_:
            res['type_'] = self.type_
        if not rule or rule.template != self:
            res['template'] = self.id
        if not rule or rule.payslip_position != self.payslip_position:
            res['payslip_position'] = self.payslip_position
        if not rule or rule.condition != self.condition:
            res['condition'] = self.condition
        if not rule or rule.expression != self.expression:
            res['expression'] = self.expression
        if not rule or rule.is_law_benefit != self.is_law_benefit:
            res['is_law_benefit'] = self.is_law_benefit
        if not rule or rule.law_benefit_type != self.law_benefit_type:
            res['law_benefit_type'] = self.law_benefit_type
        if not rule or rule.is_manual_entry != self.is_manual_entry:
            res['is_manual_entry'] = self.is_manual_entry
        if not rule or rule.entry_type != self.entry_type:
            res['entry_type'] = self.entry_type
        if not rule or (rule.is_fixed_income_deduction !=
                self.is_fixed_income_deduction):
            res['is_fixed_income_deduction'] = self.is_fixed_income_deduction
        if not rule or (rule.fixed_income_deduction_type !=
                self.fixed_income_deduction_type):
            res['fixed_income_deduction_type'] = (
                self.fixed_income_deduction_type)
        if not rule or rule.notes != self.notes:
            res['notes'] = self.notes
        return res

    @classmethod
    def create_rules(cls, templates, company_id, template2rule=None):
        '''
        Create recursively rules based on template.
        template2rule is a dictionary with template id as key and rule id
        as value, used to convert template id into rule. The dictionary is
        filled with new rules
        '''
        pool = Pool()
        Rule = pool.get('payslip.rule')

        if template2rule is None:
            template2rule = {}

        values = []
        created = []
        for template in templates:
            if template.id not in template2rule:
                vals = template._get_rule_value()
                vals['company'] = company_id
                vals['active'] = True
                values.append(vals)
                created.append(template)

        rules = []
        for val in values:
            rule = Rule.search([('code', '=', val['code'])])
            if not rule:
                rule, = Rule.create([val])
            else:
                rule, = rule
                Rule.write([rule], val)
            rules.append(rule)
        for template, rule in zip(created, rules):
            template2rule[template.id] = rule.id

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ]
        return domain


class PayslipRule(ModelSQL, ModelView):
    'Payslip Rule'
    __name__ = 'payslip.rule'
    _history = True

    template = fields.Many2One('payslip.rule.template', 'Plantilla',
        readonly=True)
    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        }, required=True)
    active = fields.Boolean('Activo')
    code = fields.Char('Código', required=True)
    name = fields.Char('Nombre', required=True)
    abbreviation = fields.Char('Abreviatura')
    type_ = fields.Selection(_PAYSLIP_ITEM_TYPES, 'Tipo de regla',
        required=True, sort=False)
    payslip_position = fields.Selection(
        [
            ('left', 'Izquierda'),
            ('right', 'Derecha'),
            ('general', 'General'),
            ('other', 'Otro'),
        ], 'Posición de rol de pagos',
        states={
            'invisible': True
        }, required=True, sort=False)
    condition = fields.Text('Condición', required=True)
    expression = fields.Text('Expresión', required=True)
    is_law_benefit = fields.Boolean("¿Es un beneficio de ley (XIII, XIV, FR)?",
        states={
            'readonly': (Bool(Eval('is_manual_entry')) |
                Bool(Eval('is_fixed_income_deduction')))
        }, depends=['is_manual_entry', 'is_fixed_income_deduction'])
    is_manual_entry = fields.Boolean("¿Es una entrada manual (Excel)?",
        states={
            'readonly': (Bool(Eval('is_law_benefit')) |
                Bool(Eval('is_fixed_income_deduction')))
        }, depends=['is_law_benefit', 'is_fixed_income_deduction'])
    is_fixed_income_deduction = fields.Boolean("¿Es un ingreso/deducción fijo?",
        states={
            'readonly': (Bool(Eval('is_law_benefit')) |
                Bool(Eval('is_manual_entry')))
        }, depends=['is_law_benefit', 'is_manual_entry'])
    entry_type = fields.Many2One('payslip.entry.type', 'Tipo de entrada',
        states={
            'required': Bool(Eval('is_manual_entry')),
            'invisible': ~Bool(Eval('is_manual_entry'))
        }, depends=['is_manual_entry'], ondelete='RESTRICT')
    fixed_income_deduction_type = fields.Many2One(
        'company.contract.type.income.deduction', 'Tipo de I/D fijo',
        states={
            'required': Bool(Eval('is_fixed_income_deduction')),
            'invisible': ~Bool(Eval('is_fixed_income_deduction'))
        }, depends=['is_fixed_income_deduction'], ondelete='RESTRICT')
    law_benefit_type = fields.Many2One('payslip.law.benefit.type',
        'Tipo de beneficio',
        states={
            'required': Bool(Eval('is_law_benefit')),
            'invisible': ~Bool(Eval('is_law_benefit'))
        }, depends=['is_law_benefit'], ondelete='RESTRICT')

    # AFFECTIONS
    affects_xiii = fields.Boolean('Afecta XIII',
        states={
            'invisible': True
        })
    affects_xiv = fields.Boolean('Afecta XIV',
        states={
            'invisible': True
        })
    affects_fr = fields.Boolean('Afecta FR',
        states={
            'invisible': True
        })
    party = fields.Many2One('party.party', 'Tercero', required=False)
    additional_value = fields.Numeric('Valor adicional', required=True,
        domain=[
            ('additional_value', '>=', Decimal('0')),
            ('additional_value', '<=', Decimal('1')),
        ])

    # NOTES
    notes = fields.Text('Notas')

    @classmethod
    def __setup__(cls):
        super(PayslipRule, cls).__setup__()
        t = cls.__table__()
        # TODO DELETE THIS CONTRAINT USING REGITSER METHOD IN ORDER TO USE THE
        # TODO VALIDATE METHOD
        cls._sql_constraints += [
            ('rule_code_uniq', Unique(t, t.code), 'El código ingresado para '
                'esta regla ya existe!'),
        ]
        cls._error_messages.update({
            'rule_code_uniq': ('El código "%(code)s" que intenta asignar a la '
                'regla %(name)s ya ha sido usado en otra regla cuyo nombre es'
                '"%(name_conflict)s".\n\nRecuerde que los códigos deben ser '
                'únicos.'),
            'rules_update': ('Se ha encontrado que el código de este registro '
                'es usado en las fórmulas de cálculo de los siguientes '
                'registros:\n\n%(names)s\nPara garantizar el funcionamiento '
                'correcto, se reemplazará el código antigüo por el nuevo en '
                'las reglas mencionadas (RECOMENDADO).'),
        })
        cls._order.insert(0, ('name', 'ASC'))
        cls._order.insert(1, ('code', 'ASC'))

    @classmethod
    def validate(cls, rules):
        PayslipRule = Pool().get('payslip.rule')
        for rule in rules:
            list_types = PayslipRule.search([
                ('id', '!=', rule.id),
                ('code', '=', rule.code),
            ])
            if list_types:
                cls.raise_user_error('rule_code_uniq', {
                    'code': rule.code,
                    'name': rule.name,
                    'name_conflict': list_types[0].name,
                })
        super(PayslipRule, cls).validate(rules)

    @classmethod
    def write(cls, rules, vals, *args):
        if rules:
            # Search if the code is used in any payslip rule or law benefit type
            utils.set_text_value_on_rules(cls, rules, vals, 'code')
        super(PayslipRule, cls).write(rules, vals, *args)

    @classmethod
    def copy(cls, rules, default=None):
        if default is None:
            default = {}
        default = default.copy()
        new_rules = []
        i = 1
        for rule in rules:
            default['code'] = rule.code + ' (DUPLICADO ' + str(i) + ')'
            default['name'] = rule.name + ' (DUPLICADO ' + str(i) + ')'
            new_rules.extend(super(PayslipRule, cls).copy(
                [rule], default=default))
            i += 1
        return new_rules

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_condition():
        return 'True'

    @staticmethod
    def default_is_law_benefit():
        return False

    @staticmethod
    def default_is_manual_entry():
        return False

    @staticmethod
    def default_is_fixed_income_deduction():
        return False

    @staticmethod
    def default_additional_value():
        return Decimal(0)

    def get_rec_name(self, name):
        return "%s (%s)" % (self.name, self.code)

    @fields.depends('name', 'code')
    def on_change_with_code(self, name=None):
        set_codes_from_names = get_rules_set_codes_from_names_conf()
        if set_codes_from_names:
            if self.name:
                code = normalize_string(self.name).lower().replace(' ', '_')
                return code
            else:
                return None
        return self.code

    @fields.depends('type_', 'payslip_position')
    def on_change_type_(self):
        if self.type_ == 'income':
            self.payslip_position = 'left'
        elif self.type_ == 'deduction':
            self.payslip_position = 'right'
        elif self.type_ == 'internal':
            self.payslip_position = 'other'

    @fields.depends('is_manual_entry', 'entry_type', 'code', 'name')
    def on_change_is_manual_entry(self, names=None):
        if not self.is_manual_entry:
            self.code = None
            self.name = None
            self.entry_type = None
            self.condition = None
            self.expression = None

    @fields.depends('is_fixed_income_deduction', 'fixed_income_deduction_type',
        'code', 'name')
    def on_change_is_fixed_income_deduction(self, names=None):
        if not self.is_fixed_income_deduction:
            self.code = None
            self.name = None
            self.fixed_income_deduction_type = None
            self.condition = None
            self.expression = None

    @fields.depends('is_law_benefit', 'law_benefit_type', 'code', 'name')
    def on_change_is_law_benefit(self, names=None):
        if not self.is_law_benefit:
            self.code = None
            self.name = None
            self.law_benefit_type = None
            self.condition = None
            self.expression = None

    @fields.depends('entry_type', 'type_', 'payslip_position', 'condition',
        'expression', 'code', 'name')
    def on_change_entry_type(self, names=None):
        if self.entry_type and self.entry_type.kind and self.entry_type.code:
            self.type_ = self.entry_type.kind
            self.code = self.entry_type.code
            self.name = self.entry_type.name
            self.on_change_type_()
            self.condition = 'True'
            self.expression = (
                f"result = {'{'}\n"
                f"    'value': manual_entry_amounts['{self.entry_type.code}']\n"
                f"{'}'}\n"
                f"result"
            )
        else:
            self.code = None
            self.name = None
            self.entry_type = None
            self.condition = None
            self.expression = None

    @fields.depends('fixed_income_deduction_type', 'type_', 'payslip_position',
        'condition', 'expression', 'code', 'name')
    def on_change_fixed_income_deduction_type(self, names=None):
        fixed_type = self.fixed_income_deduction_type
        if fixed_type and fixed_type.kind and fixed_type.code:
            code = fixed_type.code
            self.type_ = fixed_type.kind
            self.code = fixed_type.code
            self.name = fixed_type.description
            self.on_change_type_()
            self.condition = 'True'
            if fixed_type.kind == 'income':
                self.expression = (
                    f"result = {'{'}\n"
                    f"    'value': fixed_income_amounts['{code}']\n"
                    f"{'}'}\n"
                    f"result"
                )
            elif fixed_type.kind == 'deduction':
                self.expression = (
                    f"result = {'{'}\n"
                    f"    'value': fixed_deduction_amounts['{code}']\n"
                    f"{'}'}\n"
                    f"result"
                )
        else:
            self.code = None
            self.name = None
            self.fixed_income_deduction_type = None
            self.condition = None
            self.expression = None

    @fields.depends('type_', 'expression')
    def on_change_with_expression(self, names=None):

        if self.type_:
            if self.type_ == 'income':
                expression = str(self.expression)
                expression = expression.replace('fixed_deduction_amounts',
                                                'fixed_income_amounts')
                return expression
            elif self.type_ == 'deduction':
                expression = str(self.expression)
                expression = expression.replace('fixed_income_amounts',
                                                'fixed_deduction_amounts')
                return expression
            else:
                return self.expression

    @fields.depends('law_benefit_type', 'type_', 'payslip_position',
        'condition', 'expression', 'code', 'name')
    def on_change_law_benefit_type(self, names=None):
        if self.law_benefit_type and self.law_benefit_type.code:
            self.type_ = 'income'
            self.code = self.law_benefit_type.code
            self.name = self.law_benefit_type.name
            self.on_change_type_()
            if self.law_benefit_type.code in ['fr']:
                self.condition = (
                    f"# Si es un xiii, xiv, o fr acumulado sustituya "
                    f"'monthlyse' por 'accumulate'\n"
                    f"result = False\n"
                    f"if have_fr and payment_form_{self.law_benefit_type.code} "
                    f"== 'monthlyse':\n"
                    f"    result = True\n"
                    f"result"
                )
            else:
                self.condition = (
                    f"# Si es un xiii, xiv, o fr acumulado sustituya "
                    f"'monthlyse' por 'accumulate'\n"
                    f"result = False\n"
                    f"if payment_form_{self.law_benefit_type.code} == "
                    f"'monthlyse':\n"
                    f"    result = True\n"
                    f"result"
                )
            self.expression = (
                f"result = {'{'}\n"
                f"    'value': law_benefit_types["
                f"'{self.law_benefit_type.code}']\n"
                f"{'}'}\n"
                f"result"
            )
        else:
            self.code = None
            self.name = None
            self.entry_type = None
            self.condition = None
            self.expression = None

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ]
        return domain

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
            query=False):
        context = Transaction().context
        type = context.get('type')
        if type == 'payslip.payslip' and context.get('payslip_general'):
            pool = Pool()
            General = pool.get('payslip.general')
            Payslip = pool.get('payslip.payslip')
            Line = pool.get('payslip.line')
            Rule = pool.get('payslip.rule')
            general = General.__table__()
            payslip = Payslip.__table__()
            rule = Rule.__table__()
            line = Line.__table__()
            query_ = general.join(payslip,
                condition=general.id == payslip.general
            ).join(line,
                condition=payslip.id == line.payslip
            ).join(rule,
                condition=rule.id == line.rule
            ).select(
                rule.id,
                where=((general.id.in_(context['payslip_general'])) & (
                    rule.type_ == 'deduction')),
                group_by=[rule.id])
            domain += [('id', 'in', query_)]
        elif type == 'payslip.retired' and context.get('payslip_retired'):
            pool = Pool()
            General = pool.get('payslip.general.retired')
            Payslip = pool.get('payslip.retired')
            Line = pool.get('payslip.retired.line')
            Rule = pool.get('payslip.rule')
            general = General.__table__()
            payslip = Payslip.__table__()
            rule = Rule.__table__()
            line = Line.__table__()
            query_ = general.join(
                payslip, condition=general.id == payslip.general
                ).join(
                line, condition=payslip.id == line.payslip
                ).join(
                rule, condition=rule.id == line.rule
                ).select(
                rule.id,
                where=((general.id.in_(context['payslip_retired'])) & (
                        rule.type_ == 'deduction')),
                group_by=[rule.id])
            domain += [('id', 'in', query_)]
        return super(PayslipRule, cls).search(domain, offset=offset,
            limit=limit, order=order, count=count, query=query)


class PayslipGeneral(Workflow, ModelSQL, ModelView):
    'Payslip General'
    __name__ = 'payslip.general'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], states=_STATES, depends=_DEPENDS, readonly=True, required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ],
        states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio del periodo'),
        'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin del periodo'),
        'on_change_with_period_end_date')
    start_date = fields.Date('Fecha de inicio', required=True,
        states=_STATES, depends=_DEPENDS + ['end_date', 'period_start_date',
            'template_for_accumulated_law_benefits'])
    end_date = fields.Date('Fecha de fin', required=True,
        states=_STATES, depends=_DEPENDS + ['start_date', 'period_end_date',
            'template_for_accumulated_law_benefits'])
    template = fields.Many2One('payslip.template', 'Plantilla de rol de pagos',
        domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['payslip', 'accumulated_law_benefits'])
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    template_for_accumulated_law_benefits = fields.Function(fields.Boolean(
        'Es una plantilla de beneficios de ley acumulados?'),
        'on_change_with_template_for_accumulated_law_benefits',
        searcher='search_template_for_accumulated_law_benefits')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirm', 'Confirmado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado'),
        ('post', 'Contabilizar'),
    ], 'State', readonly=True)
    state_translated = state.translated('state')
    lines = fields.One2Many('payslip.payslip', 'general', 'Líneas',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'])
    total_general = fields.Function(fields.Numeric('Total general'),
        'get_total_general')
    total_income = fields.Function(fields.Numeric('Total ingresos'),
        'get_total_general')
    total_deduction = fields.Function(fields.Numeric('Total deducciones'),
        'get_total_general')
    move = fields.Many2One('account.move', 'Asiento de Rol',
        readonly=True, domain=[('company', '=', Eval('company', -1)), ],
        depends=['company'])
    move_lb = fields.Many2One('account.move', 'Asiento de Provisiones',
        readonly=True, domain=[('company', '=', Eval('company', -1)), ],
        depends=['company'])
    journal = fields.Many2One('account.journal', 'Diario',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
            'required': Eval('state').in_(
                ['post']
            ),
        },
        depends=['state'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso Rol',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
            'readonly': Bool(Eval('budget_impact_direct')),
            'required': Eval('state').in_(
                ['post']
            ),
        }, ondelete='RESTRICT')
    compromise_lb = fields.Many2One('public.budget.compromise',
        'Compromiso Beneficios',
        states={
            'invisible': (~Eval('state').in_(['done', 'post'])
                | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
            'readonly': Bool(Eval('budget_impact_direct')),
            'required': Eval('state').in_(
                ['post']
            ),
        }, ondelete='RESTRICT')
    budget_impact_direct = fields.Boolean('Impacto de Partida Directo',
        states={
            'invisible': (~Eval('state').in_(
                ['done', 'post']
            ) | Bool(Eval('template_for_accumulated_law_benefits')))
        })
    move_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('posted', 'Contabilizado'),
        ], 'Estado asiento de rol', states={
            'invisible': (~Eval('state').in_(
                ['done', 'post']
            ) | Bool(Eval('template_for_accumulated_law_benefits'))
            ),
        }), 'get_move_state',
        searcher='search_move_state')

    @classmethod
    def get_total_general(cls, generals, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: Decimal("0"))
        result_income = defaultdict(lambda: Decimal("0"))
        result_deduction = defaultdict(lambda: Decimal("0"))
        Payslip = pool.get('payslip.payslip')
        payslip = Payslip.__table__()
        query = payslip.select(
            payslip.general,
            Sum(payslip.total_value).as_('total_general'),
            Sum(payslip.total_income).as_('total_income'),
            Sum(payslip.total_deduction).as_('total_deduction'),
            group_by=[payslip.general]
        )
        cursor.execute(*query)
        for (general, total_general, total_income,
                total_deduction) in cursor.fetchall():
            result[general] = total_general
            result_income[general] = total_income
            result_deduction[general] = total_deduction

        return {
            'total_general': result,
            'total_income': result_income,
            'total_deduction': result_deduction,
        }

    @classmethod
    def __setup__(cls):
        super(PayslipGeneral, cls).__setup__()
        cls._error_messages.update({
            'loaded_payslips_error': 'Los roles no se han cargado.',
            'generated_values_error': ('Los valores de algunos roles no han '
                'sido generados.'),
            'no_delete': 'Solamente es posible eliminar registros que estén en '
                'estado "Borrador".',
            'payslip_move_already_posted': 'El asiento ya fue contabilizado',
            'error_unique_constraint': ('Ya existe un rol de pagos general con '
                'la plantilla "%(template)s" para el período "%(period)s".'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para roles de '
                'pago. Por favor, diríjase a Talento Humano / Roles de Pagos / '
                'Configuración / Configuraciones varias y defina la '
                'configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
            'employees_without_email': ('No se han encontrado direcciones de '
                'correos electrónicos para los siguientes empleados:\n'
                '%(employees)s\n\nLos avisos de pago no serán enviados a estas '
                'personas.'),
            'account_error': 'La cuenta %(account)s es de vista.',
            'posted_move': 'El asiento ya esta contabilizado.',
            'set_journal': 'Debe asignar el diario.',
            'set_compromise': 'Seleccione el compromiso o marque '
                'impacto directo.',
            'rules_without_budget': ('No se han encontrado configuraciones\n'
                'para las siguientes reglas:\n'
                '%(rules)s.'),
            'rule_without_configuration': ('Regla sin configuracion para rol:\n'
                '%(rule)s.'),
            'account_without_budget': ('Cuenta: %(account)s sin partida \n'
                'especifica para %(budget)s.'),
            'budget_not_exist': 'Partida no codificada %(description)s.',
            'payslip_template_ab_not_exist': ('Contrato sin plantilla de '
                'cuentas y partidas %(description)s.'),
            'wrong_records_domain': ('No se puede ejecutar la acción '
                'solicitada ya que los siguientes contratos están en estado '
                'BORRADOR:\n\n%(message)s\n\nPor favor, revise el estado de '
                'dichos contratos o elimínelos de este rol de pagos.'),
            'move_generated_successfully':('Los asientos se generaron con exito, '
                                           ' por favor, reviselos en el eslabon'),
        })
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': True,
                # 'invisible': Eval('state').in_(['cancel', 'draft', 'post']),
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'post': {
                'invisible': Or(Eval('state').in_(['confirm', 'draft',
                    'cancel']), Eval('move_state').in_(['posted']),
                    Bool(Eval('template_for_accumulated_law_benefits'))),
                'depends': ['state'],
            },
            'load_lines': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'tryton-import'
            },
            'generate_values': {
                'invisible': Eval('state').in_(['done', 'confirm', 'cancel',
                    'post']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'send_payment_notice_to_emails': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._order.insert(0, ('end_date', 'DESC'))
        cls._order.insert(1, ('template', 'ASC'))
        cls._order.insert(2, ('start_date', 'DESC'))

    @classmethod
    def validate(cls, generals):
        cls.check_general_payslips_with_same_template_and_period(generals)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @classmethod
    def write(cls, payslips_generals, vals, *args):
        # Change start and end date of each individual payslip in order to
        # match with start and end date of general payslip.
        super(PayslipGeneral, cls).write(payslips_generals, vals, *args)
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        to_save = []
        if payslips_generals:
            for payslip_general in payslips_generals:
                for line in payslip_general.lines:
                    line.start_date = payslip_general.start_date
                    line.end_date = payslip_general.end_date
                    to_save.append(line)
        Payslip.save(to_save)

    @classmethod
    def delete(cls, generals):
        for general in generals:
            if not Transaction().context.get('general_payslip_cancel_state'):
                if general.state in ['done', 'confirm', 'cancel']:
                    cls.raise_user_error('no_delete')
        super(PayslipGeneral, cls).delete(generals)

    @classmethod
    @ModelView.button
    def send_payment_notice_to_emails(cls, generals):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_conf()
        payslips_without_emails = ''
        if notification_email:
            records_to_notify = []
            for general in generals:
                for payslip in general.lines:
                    if not payslip.employee.email:
                        payslips_without_emails += (
                            '\n[' + payslip.employee.rec_name + ']')
                    else:
                        records_to_notify.append(payslip)

            if payslips_without_emails != '':
                cls.raise_user_warning('employees_without_email',
                    'employees_without_email', {
                        'employees': payslips_without_emails
                    })

            if records_to_notify:
                NotificationEmail.trigger(
                    records_to_notify, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, payslips):
        for payslip in payslips:
            payslip.change_lines_states('draft')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, payslips):
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Compromise = pool.get('public.budget.compromise')
        Certificate = pool.get('public.budget.certificate')

        # Check contract domain
        cls.check_contract_domains(payslips)

        moves = []
        for payslip in payslips:
            payslip.change_lines_states('done')
            if payslip.move and payslip.move.state == 'posted':
                cls.raise_user_error('payslip_move_already_posted')
            if payslip.move:
                moves.append(payslip.move)
        if moves:
            for m in moves:
                MoveLine.delete(m.lines)

                certificates_to_delete = []
                compromises_to_delete = []

                for c in m.compromises:
                    if c.number == "#" and c.certificate.number == '#':
                        c.state = 'draft'
                        Compromise.save([c])
                        compromises_to_delete.append(c)
                        c.certificate.state = 'draft'
                        Certificate.save([c.certificate])
                        certificates_to_delete.append(c.certificate)

                m.compromises = []
                Move.save([m])
                Compromise.delete(compromises_to_delete)
                Certificate.delete(certificates_to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, payslips):
        PayslipGeneral = Pool().get('payslip.general')
        to_delete = []
        for general in payslips:
            # Change individual payslips to canceled status
            general.change_lines_states('cancel')

            # There can not be two general payslips with the same template,
            # period and state (In "cancel" state, that could happen)
            canceled_generals = PayslipGeneral.search([
                ('template', '=', general.template),
                ('period', '=', general.period),
                ('state', '=', 'cancel'),
            ])
            if canceled_generals:
                to_delete += canceled_generals

        # This ignores the restriction of not deleting records in cancel state
        with Transaction().set_context(general_payslip_cancel_state=True):
            PayslipGeneral.delete(to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, payslips):
        # Check contract domain
        cls.check_contract_domains(payslips)
        for payslip in payslips:
            payslip.check_lines_loaded()
            payslip.change_lines_states('confirm')

    @classmethod
    @ModelView.button
    # @Workflow.transition('post')
    def post(cls, payslips):
        pool = Pool()
        MoveLine = pool.get('account.move.line')

        # Check contract domain
        cls.check_contract_domains(payslips)

        for payslip in payslips:
            if not payslip.journal:
                cls.raise_user_error('set_journal')
            if ((not payslip.budget_impact_direct
                    or payslip.budget_impact_direct == False)
                    and not payslip.compromise):
                cls.raise_user_error('set_compromise')
            if payslip.move and payslip.move.state == 'posted':
                cls.raise_user_error('posted_move')
            if payslip.move and payslip.move.state == 'draft':
                if payslip.move.lines:
                    MoveLine.delete(payslip.move.lines)
            move = payslip.get_payslip_move()
            payslip.move = move

            if payslip.move_lb and payslip.move_lb.state == 'draft':
                if payslip.move_lb.lines:
                    MoveLine.delete(payslip.move_lb.lines)
            move_lb = payslip.get_payslip_move_lb()
            payslip.move_lb = move_lb

            payslip.check_lines_loaded()
            payslip.change_lines_states('post')

        cls.save(payslips)
        # cls.raise_user_error('move_generated_successfully', {
        #     'message': 'Error' #TODO estaba un nombre de una variable y me daba error
        # })

    def change_lines_states(self, new_state):
        pool = Pool()
        Line = pool.get('payslip.payslip')
        if new_state == 'draft':
            Line.draft(self.lines)
        elif new_state == 'confirm':
            Line.confirm(self.lines)
        elif new_state == 'done':
            Line.done(self.lines)
        elif new_state == 'cancel':
            Line.cancel(self.lines)

    @classmethod
    def check_contract_domains(cls, generals):
        wrong_records_message = get_contracts_with_wrong_domain(generals)
        if wrong_records_message:
            cls.raise_user_error('wrong_records_domain', {
                'message': wrong_records_message
            })

    def check_lines_loaded(self):
        if not self.lines:
            self.raise_user_error('loaded_payslips_error')
        else:
            for line in self.lines:
                if not (line.income_lines or line.deduction_lines):
                    self.raise_user_error('generated_values_error')

    @classmethod
    def check_general_payslips_with_same_template_and_period(cls, generals):
        payslips_conf = get_payslips_configuration()
        do_check = (payslips_conf.
            allow_multiple_general_payslips_with_same_template_and_period)
        if not do_check:
            PayslipGeneral = Pool().get('payslip.general')
            for general in generals:
                list_generals = PayslipGeneral.search([
                    ('id', '!=', general.id),
                    ('company', '=', general.company),
                    ('template', '=', general.template),
                    ('period', '=', general.period),
                    ('state', 'not in', ['cancel'])
                ])
                if list_generals:
                    cls.raise_user_error('error_unique_constraint', {
                        'template': general.template.rec_name,
                        'period': general.period.rec_name,
                    })

    @fields.depends('template')
    def on_change_with_template_for_accumulated_law_benefits(self, name=None):
        if self.template:
            return self.template.accumulated_law_benefits
        return None

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_start_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - relativedelta(months=12)
            else:
                if self.period.payslip_start_date:
                    return self.period.payslip_start_date
                else:
                    return self.period.start_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_end_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - timedelta(days=1)
            else:
                if self.period.payslip_end_date:
                    return self.period.payslip_end_date
                else:
                    return self.period.end_date
        return None

    @fields.depends('start_date', 'lines')
    def on_change_start_date(self, name=None):
        if self.start_date:
            for line in self.lines:
                line.start_date = self.start_date

    @classmethod
    def get_move_state(cls, payslip_generals, name):
        ids = [pg.id for pg in payslip_generals]

        cursor = Transaction().connection.cursor()
        Move = Pool().get('account.move')
        move = Move.__table__()
        payslip_general = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payslip_general.id, sub_ids)
            query = payslip_general.join(move, 'LEFT',
                condition=(move.id == payslip_general.move)
                ).select(
                payslip_general.id,
                move.state,
                where=red_sql
            )
            cursor.execute(*query)

            result.update(dict(cursor.fetchall()))

        return result

    @classmethod
    @ModelView.button
    def load_lines(cls, generals):
        pool = Pool()
        Contract = pool.get('company.contract')
        Payslip = pool.get('payslip.payslip')
        to_save = []

        payslips_conf = get_payslips_configuration()
        consider_finalized_contracts_payslip_month = (payslips_conf.
            consider_finalized_contracts_during_payslip_period)
        consider_finalized_contracts_payslip_accum = (payslips_conf.
            consider_finalized_contracts_during_payslip_period_accum)
        consider_disabled_contracts_payslip_accum = (payslips_conf.
            consider_disabled_contracts_during_payslip_period_accum)

        for general in generals:
            # First: Delete payslips created before for this general payslip
            payslips = Payslip.search([
                ('general', '=', general),
                ('state', '!=', 'cancel'),
            ])
            if payslips:
                Payslip.delete(payslips)

            # Second: Get contracts information
            contracts = []
            if general.template.accumulated_law_benefits:
                # To calculate accumulated XIII, XIV, the system searches
                # the database for those benefits of the law pending payment
                # belonging to the range of the current role
                law_benefit_type = general.template.law_benefit_type
                aux_contracts = (
                    cls.get_contracts_with_pending_law_benefits_to_pay(
                        general.start_date, general.end_date, law_benefit_type))
                for c in aux_contracts:
                    if c.state in ['done']:
                        contracts.append(c)
                    elif c.state in ['finalized']:
                        if consider_finalized_contracts_payslip_accum:
                            contracts.append(c)
                    elif c.state in ['disabled']:
                        if consider_disabled_contracts_payslip_accum:
                            contracts.append(c)
            else:
                _domain = [
                    ('payslip_templates', 'in', [general.template.id]),
                    ('state', 'in', ['done', 'finalized', 'disabled'])
                ]
                if consider_finalized_contracts_payslip_month:
                    _domain.append(['OR',
                        ('end_date', '=', None),
                        ('end_date', '>=', general.period.start_date)
                    ])
                else:
                    _domain.append(['OR',
                        ('end_date', '=', None),
                        ('end_date', '>=', general.period.end_date)
                    ])
                contracts = Contract.search(_domain)

                # Contratos con permisos de licencia sin sueldo que no se deben
                # considerar en el rol
                contracts = cls.remove_contracts_with_unpaid_license(
                    general.period, contracts)

                # Contratos en estado inhabilitado que no se deben considerar
                # en el rol
                contracts = cls.remove_disabled_contracts(
                    general.period, contracts)

            for contract in contracts:
                payslips = Payslip.search([
                    ('contract', '=', contract),
                    ('period', '=', general.period),
                    ('template', '=', general.template),
                    ('state', '!=', 'cancel'),
                ])
                # Do not create repeated payslips for the same contract
                if not payslips or 'extra' in general.template.code:
                    if contract.state in ['done', 'finalized', 'disabled']:
                        # It is evaluated that the contract is within the
                        # payslip period depending on whether it is a cumulative
                        # template or not
                        contract_end_date = contract.end_date
                        if not contract_end_date:
                            contract_end_date = datetime.max.date()

                        comp_start_date = None
                        comp_end_date = None
                        if not general.template.accumulated_law_benefits:
                            comp_start_date = general.period.start_date
                            comp_end_date = general.period.end_date
                        elif general.template.accumulated_law_benefits:
                            comp_start_date = general.start_date
                            comp_end_date = general.end_date

                        if not (comp_start_date and comp_start_date):
                            continue

                        if (contract_end_date >= comp_start_date
                                and contract.start_date < comp_end_date):
                            # The record is created
                            payslip = Payslip()
                            payslip.general = general
                            payslip.template = general.template
                            payslip.employee = contract.employee
                            payslip.contract = contract
                            payslip.email_recipient = contract.employee.party
                            payslip.fiscalyear = general.fiscalyear
                            payslip.period = general.period
                            # Define payslip start date
                            payslip.start_date = general.start_date
                            # Define payslip end date
                            payslip.end_date = contract_end_date
                            if contract_end_date >= general.end_date:
                                payslip.end_date = general.end_date
                            # Payslip law benefit information
                            payslip.xiii = contract.xiii
                            payslip.xiv = contract.xiv
                            payslip.fr = 'not_considered'
                            if contract.have_fr:
                                payslip.fr = contract.fr
                            to_save.append(payslip)
        Payslip.save(to_save)

    @classmethod
    def remove_disabled_contracts(cls, period, contracts):
        DisableContract = Pool().get('company.disable.contract')
        contracts_ids = [c.id for c in contracts]

        # Note: If the contract has an activation date, it means that it was
        # inactive until the day before the activation date
        readed_fields = DisableContract.search_read([
            ('contract', 'in', contracts_ids),
            ('disable_date', '<=', period.start_date),
            ['OR',
                ('enable_date', '>', period.end_date),
                ('enable_date', '=', None)],
            ('state', 'in', ['done', 'enable_contract']),
        ], fields_names=['contract'])

        if readed_fields:
            result = []
            # Remove disabled contracts
            contracts_ids = [rf['contract'] for rf in readed_fields]
            for contract in contracts:
                consider = True
                for contract_id in contracts_ids:
                    if contract.id == contract_id:
                        consider = False
                        break
                if consider:
                    result.append(contract)
        else:
            result = contracts
        return result

    @classmethod
    def remove_contracts_with_unpaid_license(cls, period, contracts):
        Permission = Pool().get('hr_ec.permission')
        contracts_ids = [c.id for c in contracts]

        readed_fields = Permission.search_read([
            ('contract', 'in', contracts_ids),
            ('start_date', '<=', period.start_date),
            ('end_date', '>=', period.end_date),
            ('discount_to', '=', 'license'),
            ('state', '=', 'done'),
        ], fields_names=['contract'])

        if readed_fields:
            result = []
            # Remove contracts with unpaid license
            contracts_ids = [rf['contract'] for rf in readed_fields]
            for contract in contracts:
                consider = True
                for contract_id in contracts_ids:
                    if contract.id == contract_id:
                        consider = False
                        break
                if consider:
                    result.append(contract)
        else:
            result = contracts
        return result

    @classmethod
    @ModelView.button
    def generate_values(cls, generals):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        Contract = pool.get('company.contract')

        # Check contract domain
        cls.check_contract_domains(generals)

        for general in generals:
            salaries, hour_costs = {}, {}
            p_ids = [(p.id, p.contract.id) for p in general.lines]
            with Transaction().set_context(salary_date=general.period.end_date):
                salaries_aux, hour_cost_aux = {}, {}
                # Get contracts
                c_ids = [p.contract.id for p in general.lines]
                contracts = Contract.browse(c_ids)
                # Get salary and hour cost by contract
                info_salaries = Contract.get_salary(contracts, 'salary')
                salaries_aux = info_salaries['salary']
                hour_cost_aux = info_salaries['hour_cost']
                # Set information
                for position in p_ids:
                    salaries[position[0]] = salaries_aux[position[1]]
                    hour_costs[position[0]] = hour_cost_aux[position[1]]
            info_salaries = {
                'salaries': salaries,
                'hour_costs': hour_costs
            }
            with Transaction().set_context(info_salaries=info_salaries):
                Payslip.load_lines(general.lines)
            cls.delete_wrong_payslips(general)

    def get_payslip_move(self):
        def prepare_move_line(account, debit, credit, party, budget, compromise):
            payable_budget = None
            line_account = account_values[account]
            if line_account.kind == 'payable' and credit > 0:
                payable_budget = budget
            compromise_to_line = None
            if compromise:
                compromise_to_line = compromise
            else:
                compromise_to_line = self.compromise
            return self.get_move_line(
                move,
                line_account,
                utils.quantize_currency(debit),
                utils.quantize_currency(credit), {
                    'compromise': compromise_to_line,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': payable_budget,
                    'payable_cost_center': default_cost_center
                },
                budget
            )

        self.update_payslip_line_account_budget()
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        Party = pool.get('party.party')

        cursor = Transaction().connection.cursor()

        move_lines = defaultdict(lambda: [])
        negative_party = []

        accounting_date = (self.period_start_date
                           if self.end_date < self.period_start_date
                           else self.end_date)

        period_id = Period.find(self.company.id, date=accounting_date)

        move = self.move if self.move else Move(number="#")
        # move = Move(move.id)
        move.type = 'financial'
        move.description = 'Rol de pagos del periodo %s' % self.period.name
        move.journal = self.journal
        move.period = period_id
        move.date = (self.period_start_date
                     if self.end_date < self.period_start_date
                     else self.end_date)
        move.origin = self
        move.company = self.company
        if not self.budget_impact_direct:
            move.compromises = [self.compromise]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        with Transaction().set_context(company=self.company.id,
                start_date=self.start_date,
                end_date=self.end_date,
                state='done',
                general=self.id):
            Account = pool.get('account.account')
            AccountMoveLine = pool.get('account.move.line')
            CostCenter = pool.get('public.cost.center')

            default_cost_center, = CostCenter.search([])

            query = self.payslip_query_get()
            cursor.execute(*query)
            result = cursor.fetchall()

            account_values = defaultdict(lambda: Decimal(0))
            account_ids = []
            parent_account_ids = []
            parent_lines = {}
            for (account, debit, credit, party, budget, parent, compromise) in result:
                if account is not None:
                    account_ids.append(account)
                parent_account_ids.append(parent)
            for account in Account.browse(account_ids):
                account_values[account.id] = account
            for (account, debit, credit, party, budget, parent, compromise) in result:
                c_aux = compromise if compromise else ""
                if account is not None:
                    if debit == 0 and credit == 0:
                        continue
                    if debit != 0 and credit != 0:
                        move_lines[party] += prepare_move_line(
                            account, 0, credit, party, budget, None)
                        move_lines[party] += prepare_move_line(
                            account, debit, 0, party, budget, compromise)
                        move_lines[party][-1].payable_move_line = (
                            move_lines[party][-2])
                        if parent:
                            move_lines[party][-2].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    else:
                        move_lines[party] += prepare_move_line(
                            account, debit, credit, party, budget, compromise)
                        if (debit != 0 and account in parent_account_ids
                                and not parent):
                            parent_lines[
                                f'{account}-{budget}-{c_aux}'] = move_lines[party][-1]
                        if parent:
                            move_lines[party][-1].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    if party and credit < 0:
                        negative_party.append(party)

        for np_ in negative_party:
            party = Party(np_)
            negative_val = {}
            negative_amount = 0
            negative_account = None
            neg_move_lines = move_lines.get(np_)
            for line in neg_move_lines:
                if line.debit == 0 and line.credit < 0 and line.parent:
                    negative_val[line.parent.budget.code] = line
                    # negative_amount = line.credit
                    # negative_account = line.parent.account
                    neg_move_lines.remove(line)

            # mismo budget y parent
            lines_to_remove1 = []
            keys_to_remove = []
            party_affected = defaultdict(lambda: Decimal('0.0'))
            party_account_affected = defaultdict(lambda: Decimal('0.0'))
            is_break = False
            #Busca un tercer al que se le vaya a pagar con la misma partida sobregirada y mismo parent,
            # para que a este tercero se le pague con otra partida y el valor negativo quede en 0
            for bud_neg in negative_val.keys():
                if is_break:
                    break
                line_neg = negative_val.get(bud_neg)
                #empieza a buscar el tercero al que se va a pagar y divide las lineas de devengado
                #distribuyendo de rubros del rol del empleado se obtiene para pagar a este otro tercero
                for party in move_lines.keys():
                    if is_break:
                        break
                    if not party :
                        continue
                    #Que no descuente de los empleados por que quiere descontar entre empleados del mismo departamento
                    Employee = pool.get('company.employee')
                    employee_found = Employee.search([('party.id', '=', party)])
                    if employee_found:
                        continue
                    lines_to_remove1 = []
                    lines_party = move_lines.get(party)
                    for l in lines_party:
                        if (not l.account.code.startswith('213')
                                or l.debit != 0 or l.parent is None):
                            continue
                        asume_tercero=False
                        if (l.parent.account.code == line_neg.parent.account.code and
                            l.parent.budget.code == line_neg.parent.budget.code and
                                l.party.id in (46797,46799)  and
                                  l.party.name.lower().find('sindicato')!=-1 ):
                            asume_tercero=True
                        else:
                            continue

                        if l.credit > abs(line_neg.credit) :
                            l.credit += line_neg.credit
                            l.credit = utils.quantize_currency(l.credit)
                            is_break = True
                            keys_to_remove.append(bud_neg)
                            party_affected[party] += abs(line_neg.credit)
                            party_account_affected[party] = l.account
                            break
                        else:
                            line_neg.credit += l.credit
                            line_neg.credit = utils.quantize_currency(line_neg.credit)
                            party_affected[party] += l.credit
                            party_account_affected[party] = l.account
                            lines_to_remove1.append(l)

                    for l in lines_to_remove1:
                        lines_party.remove(l)
                        keys_to_remove.append(bud_neg)
                    move_lines[party] = lines_party

            for k in keys_to_remove:
                if negative_val.get(k):
                    negative_val.pop(k)

            #una vez que ha dividido los devengados por pagar a otros terceros
            #genera las nuevas lineas que indican el origen del pago a estos otros terceros
            for pa in party_affected.keys():
                aditional_lines = []
                while party_affected.get(pa)>0 and len(move_lines.get(np_))>0:
                    original_lines = move_lines.get(np_)
                    cont = 0
                    for line_employee in move_lines.get(np_):
                        if (not line_employee.account.code.startswith('213')
                                or line_employee.debit != 0):
                            cont+=1
                            continue
                        if party_affected.get(pa) > 0 and line_employee.credit > 0:
                            if line_employee.credit > party_affected.get(pa):
                                line_aux, = prepare_move_line(
                                line_employee.account.id, 0, utils.quantize_currency(party_affected.get(pa)), pa, None, None)
                                line_aux.parent = line_employee.parent
                                line_aux.move = line_employee.move
                                line_aux.party=pa
                                line_employee.credit -= party_affected.get(pa)
                                line_employee.credit = utils.quantize_currency(line_employee.credit)
                                party_affected[pa] = 0
                                if line_aux.account.code!=party_account_affected.get(pa).code :
                                    if line_aux.account.code[0:9]!=party_account_affected.get(pa).code[0:9]:
                                        Account = Pool().get('account.account')
                                        naccount = Account.search([('code','like',line_aux.account.code[0:9]+'%'),
                                                                ('kind','=',line_aux.account.kind),
                                                                   ('default_party.id','=',pa)])
                                        if naccount !=None and len(naccount)>0:
                                            # print('cuenta: ' + line_employee.account.code +
                                            #       ' Por:' + naccount[0].code + ' Parent:' + line_employee.parent.account.code)
                                            line_aux.account=naccount[0]
                                        else:
                                            print('Sin configuracion de tercero por defecto: '+line_aux.account.code +'debiera ir:'+party_account_affected.get(pa).code)
                                    else:
                                        line_aux.account=party_account_affected.get(pa)
                                original_lines[cont] = line_employee
                                aditional_lines.append(line_aux)
                                #print('Linea queda: ', line_employee.credit, ' para', line_employee.party.name)
                                break
                            else:
                                line_employee.party = pa
                                party_affected[pa] -= line_employee.credit
                                original_lines.pop(cont)
                                if line_employee.account.code!=party_account_affected.get(pa).code :
                                    if line_employee.account.code[0:9]!=party_account_affected.get(pa).code[0:9]:
                                        Account = Pool().get('account.account')
                                        naccount = Account.search([('code','like',line_employee.account.code[0:9]+'%'),
                                                                ('kind','=',line_employee.account.kind),
                                                                   ('default_party.id','=',pa)])
                                        if naccount !=None and len(naccount)>0:
                                            # print('cuenta: ' + line_employee.account.code +
                                            #       ' Por:' + naccount[0].code + ' Parent:' + line_employee.parent.account.code)
                                            line_employee.account=naccount[0]
                                        else:
                                            print('Sin configuracion de tercero por defecto: '+line_employee.account.code +
                                                  'Tdebiera ir:'+party_account_affected.get(pa).code)
                                    else:
                                        line_employee.account=party_account_affected.get(pa)
                                move_lines.get(pa).append(line_employee)

                        cont+=1
                    #if len(move_lines)!=len(original_lines):
                    move_lines[np_]=original_lines
                for al in aditional_lines:
                    move_lines.get(pa).append(al)
                    #print ('Valor ',al.credit,' a', al.party.name)

            lines_to_remove = []

            if negative_val:
                negative_val_sin_partida=negative_val.copy()
                # Busca como descontar el valor dentro de ingresos de la misma partida
                for budget_negative in negative_val.keys():
                    for line in neg_move_lines:
                        if (not line.account.code.startswith('213')
                                or line.debit != 0 or line.parent.budget.code[:2] != budget_negative[:2]):
                            continue
                        if line.debit == 0 and line.credit > abs(negative_val.get(budget_negative).credit):
                            line.credit += negative_val.get(budget_negative).credit
                            negative_val_sin_partida.pop(budget_negative)
                            break
                        else:
                            negative_val[budget_negative].credit += line.credit
                            lines_to_remove.append(line)
                    for line in lines_to_remove:
                        neg_move_lines.remove(line)

                #Busca hacer las deducciones de otro codigo presupuestario
                for budget_negative in negative_val_sin_partida.keys():
                    for line in neg_move_lines:
                        if (not line.account.code.startswith('213')
                                or line.debit != 0):
                            continue
                        if line.debit == 0 and line.credit > abs(negative_val.get(budget_negative).credit):
                            line.credit += negative_val.get(budget_negative).credit
                            break
                        else:
                            negative_val[budget_negative].credit += line.credit
                            lines_to_remove.append(line)
                    for line in lines_to_remove:
                        neg_move_lines.remove(line)
            move_lines[np_] = neg_move_lines
        print("finalize additional adjustment")

        for p in move_lines.keys():
            if p is None:
                continue
            debit_accounts = defaultdict(lambda: None)
            credit_accounts = defaultdict(lambda: None)
            debit_accounts_to_add = []
            debit_accounts_to_remove = []
            for l in move_lines.get(p):
                if l.debit != 0:
                    id_b = l.budget.id if l.budget else -1
                    debit_accounts[f'{l.account.id}-{id_b}'] = l
                elif l.credit != 0:
                    id_b = l.payable_budget.id if l.payable_budget else -1
                    credit_accounts[f'{l.account.id}-{id_b}'] = l
            for a in debit_accounts.keys():
                party = Party(p)
                pending_debit = 0.0
                fixed_line = None
                # Sin Cuenta
                if a not in credit_accounts.keys():
                    debit = debit_accounts.get(a).debit
                    debit_accounts_to_remove.append(debit_accounts.get(a))
                    pending_debit = debit
                # Ajuste
                elif (debit_accounts.get(a).debit
                      > credit_accounts.get(a).credit):
                    debit = debit_accounts.get(a).debit
                    credit = credit_accounts.get(a).credit
                    pending_debit = utils.quantize_currency((debit - credit))
                    debit_accounts.get(a).debit = credit
                    fixed_line = credit_accounts.get(a)

                for lc in credit_accounts.values():
                    if (lc == fixed_line or lc.account.code.startswith('112')
                            or (not lc.party and not lc.account.kind == 'payable')):  # noqa
                        continue
                    if pending_debit == 0:
                        break
                    amount = 0.0
                    if lc.credit > pending_debit:
                        amount = pending_debit
                    else:
                        amount = lc.credit
                    lines = prepare_move_line(
                        lc.account.id, amount, 0.0, lc.party,
                        lc.parent.budget, lc.compromise)
                    for li in lines:
                        debit_accounts_to_add.append(li)
                        debit_accounts_to_add[-1].payable_move_line = lc
                    pending_debit = utils.quantize_currency(
                        (pending_debit - amount))

            if not debit_accounts_to_add:
                continue
            move_lines[p] = debit_accounts_to_add
            for d in debit_accounts.values():
                move_lines[p].append(d)
            move_lines[p] = [i for i in move_lines[p]
                if i not in debit_accounts_to_remove]
            for c in credit_accounts.values():
                move_lines[p].append(c)

        mld1 = ()
        mld2 = ()
        mlc = ()
        comp = []
        for ls in move_lines.values():
            for l in ls:
                if l.compromise:
                    comp.append(l.compromise)
                if l.debit != 0 and l.account.kind == 'expense':
                    mld1 += (l,)
                elif l.debit != 0:
                    mld2 += (l,)
                else:
                    mlc += (l,)

        move.compromises = comp
        move.save()

        AccountMoveLine.save(mld1)
        print("mld1")
        AccountMoveLine.save(mlc)
        print("mlc")
        AccountMoveLine.save(mld2)
        print("mld2")

        return move

    def get_payslip_move_lb(self):
        def prepare_move_line(account, debit, credit, party, budget, compromise):
            payable_budget = None
            line_account = account_values[account]
            if line_account.kind == 'payable' and credit > 0:
                payable_budget = budget
            compromise_to_line = None
            if compromise:
                compromise_to_line = compromise
            else:
                compromise_to_line = self.compromise
            return self.get_move_line(
                move,
                line_account,
                debit,
                credit, {
                    'compromise': compromise_to_line,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': payable_budget,
                    'payable_cost_center': default_cost_center
                },
                budget
            )

        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')

        cursor = Transaction().connection.cursor()

        move_lines = defaultdict(lambda: [])

        accounting_date = (self.period_start_date
                           if self.end_date < self.period_start_date
                           else self.end_date)

        period_id = Period.find(self.company.id, date=accounting_date)

        move = self.move_lb if self.move_lb else Move(number="#")
        move.type = 'financial'
        move.description = (
            'Rol de Beneficios Sociales del periodo %s' % self.period.name)
        move.journal = self.journal
        move.period = period_id
        move.date = (self.period_start_date
                     if self.end_date < self.period_start_date
                     else self.end_date)
        move.origin = self
        move.company = self.company
        if not self.budget_impact_direct:
            move.compromises = [self.compromise_lb]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        with Transaction().set_context(company=self.company.id,
                start_date=self.start_date, end_date=self.end_date,
                state='done', general=self.id):
            Account = pool.get('account.account')
            AccountMoveLine = pool.get('account.move.line')
            CostCenter = pool.get('public.cost.center')

            default_cost_center, = CostCenter.search([])

            query_lb = self.payslip_lb_query_get()
            cursor.execute(*query_lb)
            result = cursor.fetchall()

            account_values = defaultdict(lambda: Decimal(0))
            account_ids = []
            parent_account_ids = []
            parent_lines = {}
            for (account, debit, credit, party, budget, parent, compromise) in result:
                if account is not None:
                    account_ids.append(account)
                parent_account_ids.append(parent)
            for account in Account.browse(account_ids):
                account_values[account.id] = account
            for (account, debit, credit, party, budget, parent, compromise) in result:
                c_aux = compromise if compromise else ""
                if account is not None:
                    if debit == 0 and credit == 0:
                        continue
                    if debit != 0 and credit != 0:
                        move_lines[party] += prepare_move_line(
                            account, 0, credit, party, budget, None)
                        move_lines[party] += prepare_move_line(
                            account, debit, 0, party, budget, compromise)
                        move_lines[party][-1].payable_move_line = (
                            move_lines[party][-2])
                        if parent:
                            move_lines[party][-2].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))
                    else:
                        move_lines[party] += prepare_move_line(
                            account, debit, credit, party, budget, compromise)
                        if (debit != 0 and account in parent_account_ids
                                and not parent):
                            parent_lines[f'{account}-{budget}-{c_aux}'] = move_lines[party][-1]
                        if parent:
                            move_lines[party][-1].parent = (
                                parent_lines.get(f'{parent}-{budget}-{c_aux}'))

        mld1 = ()
        mld2 = ()
        mlc = ()
        comp = []
        for ls in move_lines.values():
            for l in ls:
                if l.compromise:
                    comp.append(l.compromise)
                if l.debit != 0 and l.account.kind == 'expense':
                    mld1 += (l,)
                elif l.debit != 0:
                    mld2 += (l,)
                else:
                    mlc += (l,)

        move.compromises = comp
        move.save()
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        return move

    def update_payslip_line_account_budget(self):
        # Account Budget Rules
        account_budget_rules = self.get_all_rules_account_budget()

        pool = Pool()
        PayslipLine = pool.get('payslip.line')
        PayslipLawBenefit = pool.get('payslip.law.benefit')
        transaction = Transaction()

        rules_without_budget = []
        line_to_save = []
        line_lb_to_save = []
        errors_msg = ""
        for p in self.lines:
            ls = []
            lbs = []
            for l in p.lines:
                if not p.payslip_template_account_budget:
                    # self.raise_user_error('payslip_template_ab_not_exist', {
                    #     'description': (" Contrato:%s - Departamento(%s):%s "
                    #         "- Partida: %s - Regla(%s): %s " % (
                    #             p.contract.id,
                    #             p.department.id,
                    #             p.department.name,
                    #             l.budget.rec_name,
                    #             l.rule.id,
                    #             l.rule.name))
                    # })
                    errors_msg += ("\n Contrato:%s - Departamento(%s):%s "
                            "- Partida: %s - Regla(%s): %s " % (
                                p.contract.id,
                                p.department.id,
                                p.department.name,
                                l.budget.rec_name,
                                l.rule.id,
                                l.rule.name))
                    continue
                config = account_budget_rules.get(
                    str(p.contract.id) + "-" + str(l.rule.id))
                if config is not None:
                    l.expense_account = config.get('expense_account')
                    l.payable_account = config.get('payable_account')
                    l.receivable_account = config.get('receivable_account')
                    l.revenue_account = config.get('revenue_account')
                    l.budget = config.get('budget')
                    if l.expense_account.kind == 'expense' and not l.budget:
                        rules_without_budget.append("%s - %s - %s - %s" % (
                            p.contract.id,
                            p.contract.department.name,
                            p.contract.work_relationship.name,
                            l.rule.name))
                    if l.budget and l.budget.kind == 'view':
                        # self.raise_user_error('budget_not_exist', {
                        #     'description': " Contrato:%s - Departamento(%s):%s"
                        #     " - Partida: %s - Regla(%s): %s "
                        #     "Plantilla(%s): %s" % (
                        #         p.contract.id,
                        #         p.department.id,
                        #         p.department.name,
                        #         l.budget.rec_name,
                        #         l.rule.id,
                        #         l.rule.name,
                        #         p.payslip_template_account_budget.id,
                        #         p.payslip_template_account_budget.name)
                        # })
                        errors_msg += ("\n Contrato:%s - Departamento(%s):%s"
                            " - Partida: %s - Regla(%s): %s "
                            "Plantilla(%s): %s" % (
                                p.contract.id,
                                p.department.id,
                                p.department.name,
                                l.budget.rec_name,
                                l.rule.id,
                                l.rule.name,
                                p.payslip_template_account_budget.id,
                                p.payslip_template_account_budget.name))
                        continue
                    ls.append(l)
                else:
                    # self.raise_user_error('rule_without_configuration', {
                    #     'rule': " Contrato:%s - Departamento(%s):%s"
                    #     " - Relación Laboral: %s - Regla(%s): %s "
                    #     "Plantilla(%s): %s" % (
                    #             p.contract.id,
                    #             p.department.id,
                    #             p.department.name,
                    #             p.work_relationship.name,
                    #             l.rule.id,
                    #             l.rule.name,
                    #             p.payslip_template_account_budget.id,
                    #             p.payslip_template_account_budget.name
                    #     )
                    # })
                    errors_msg += ("\n Contrato:%s - Departamento(%s):%s"
                        " - Relación Laboral: %s - Regla(%s): %s "
                        "Plantilla(%s): %s" % (
                            p.contract.id,
                            p.department.id,
                            p.department.name,
                            p.work_relationship.name,
                            l.rule.id,
                            l.rule.name,
                            p.payslip_template_account_budget.id,
                            p.payslip_template_account_budget.name
                    ))
                    continue
            for lb in p.law_benefits:
                if lb.kind != 'accumulate':
                    continue
                r = None
                for rl in lb.type_.rule:
                    if 'accumulate' in rl.code:
                        r = rl
                        break
                config = account_budget_rules.get(
                    str(p.contract.id) + "-" + str(r.id))
                if config is not None:
                    lb.expense_account = config.get('expense_account')
                    lb.payable_account = config.get('payable_account')
                    lb.budget = config.get('budget')
                    lbs.append(lb)
                else:
                    # self.raise_user_error('rule_without_configuration', {
                    #     'rule': " Contrato:%s - Departamento(%s):%s -"
                    #     " Relación Laboral: %s - Regla(%s): %s "
                    #     "Plantilla(%s): %s" % (
                    #             p.contract.id,
                    #             p.department.id,
                    #             p.department.name,
                    #             p.work_relationship.name,
                    #             r.id,
                    #             r.name,
                    #             p.payslip_template_account_budget.id,
                    #             p.payslip_template_account_budget.name
                    #         )
                    # })
                    errors_msg += ("\n Contrato:%s - Departamento(%s):%s -"
                        " Relación Laboral: %s - Regla(%s): %s "
                        "Plantilla(%s): %s" % (
                                p.contract.id,
                                p.department.id,
                                p.department.name,
                                p.work_relationship.name,
                                r.id,
                                r.name,
                                p.payslip_template_account_budget.id,
                                p.payslip_template_account_budget.name
                            ))
                    continue

            line_to_save.extend(ls)
            line_lb_to_save.extend(lbs)
        PayslipLine.save(line_to_save)
        PayslipLawBenefit.save(line_lb_to_save)
        transaction.commit()

        if errors_msg != '':
            self.raise_user_error('budget_not_exist', {
                'description': errors_msg
            })
        if rules_without_budget:
            error_rules = ""
            for e in rules_without_budget:
                error_rules += e + "\n"
            self.raise_user_error('rules_without_budget', {
                'rules': error_rules
            })

    def get_all_rules_account_budget(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')
        DepartmentBudget = pool.get('company.department.template.budget')
        PayslipTemplateAccountBudget = pool.get(
            'payslip.template.account.budget')
        PayslipRuleAccountBudget = pool.get('payslip.rule.account.budget')
        Budget = pool.get('public.budget')
        FiscalYear = pool.get('account.fiscalyear')
        payslip = Payslip.__table__()
        dep_budget = DepartmentBudget.__table__()
        budget = Budget.__table__()
        pt_acc_budget = (
            PayslipTemplateAccountBudget.__table__())
        # Payslip Rule Account Budget
        pr_acc_budget = PayslipRuleAccountBudget.__table__()
        fiscalyear = FiscalYear.__table__()

        result = defaultdict(lambda: {lambda: None})
        ids = [p.id for p in self.lines]
        # TODO: id_fiscalyear = self.fiscal_year
        id_fiscalyear = -1
        if self.lines:
            id_fiscalyear = self.lines[0].fiscalyear.id
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payslip.id, sub_ids)
            query = payslip.join(pt_acc_budget,
                condition=(
                    pt_acc_budget.id == payslip.payslip_template_account_budget)  # noqa
            ).join(pr_acc_budget,
                condition=(pr_acc_budget.template_account_budget == pt_acc_budget.id)  # noqa
            ).join(dep_budget, 'LEFT',
                condition=(
                    (dep_budget.department == payslip.department) &
                    (dep_budget.template_budget == pt_acc_budget.id) &
                    (dep_budget.work_relationship == payslip.work_relationship)
                )
            ).join(fiscalyear,
                condition=(
                    (fiscalyear.id == dep_budget.fiscalyear) &
                    (fiscalyear.id == id_fiscalyear))
            ).join(budget, 'LEFT',
                condition=(
                    (budget.parent == pr_acc_budget.budget) &
                    (budget.activity == dep_budget.activity)
                )
            ).select(
                pr_acc_budget.expense_account,
                pr_acc_budget.payable_account,
                pr_acc_budget.receivable_account,
                pr_acc_budget.revenue_account,
                Coalesce(budget.id, pr_acc_budget.budget).as_('budget'),
                pr_acc_budget.payslip_rule,
                payslip.contract,
                where=red_sql
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[f"{row['contract']}-{row['payslip_rule']}"] = {
                    'expense_account': row['expense_account'],
                    'payable_account': row['payable_account'],
                    'receivable_account': row['receivable_account'],
                    'revenue_account': row['revenue_account'],
                    'budget': row['budget'],
                }
        return result

    def get_move_line(self, move, account, debit=0, credit=0, expense_data={},
            budget=None):
        if debit == 0 and credit == 0:
            return []
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Compromise = pool.get('public.budget.compromise')
        Party = pool.get('party.party')
        if account.kind == 'view':
            self.raise_user_error('account_error', {
                'account': account.rec_name
            })

        def add_line(move, account, debit, credit, budget=None):
            line = MoveLine(cost_center=expense_data.get('cost_center'),
                payable_budget=None,
                payable_cost_center=None)
            line.parent = None
            line.move = move
            line.type = 'financial'
            line.account, line.debit, line.credit = account, debit, credit
            line.compromise = expense_data.get('compromise')
            if line.account.party_required:
                if expense_data['party'] is not None:
                    line.party = Party(expense_data['party'])
                elif account.default_party:
                    line.party = account.default_party
                elif line.compromise is not None:
                    line.party = line.compromise.party
            line.budget = budget
            if expense_data['cost_center']:
                line.cost_center = expense_data['cost_center']
            if line.budget and not line.cost_center:
                line.on_change_budget()
            if account.kind == 'payable' and credit > 0:
                line.payable_budget = expense_data.get('payable_budget')
                line.payable_cost_center = expense_data.get(
                    'payable_cost_center')

            return line

        def set_budget_data(line):
            move = line.move
            budget_required = line.on_change_with_budget_required()
            with Transaction().set_context(
                    start_date=move.period.fiscalyear.start_date,
                    end_date=move.date):
                if budget_required:
                    if expense_data['compromise']:
                        line.compromise = Compromise(expense_data['compromise'])
                    allowed_budgets = line.get_allowed_budgets()
                    if allowed_budgets and not line.budget:
                        line.budget = allowed_budgets[0]
                    if line.budget and line.budget.type == 'expense':
                        if expense_data['cost_center']:
                            line.cost_center = expense_data['cost_center']
                        else:
                            costs = line.get_allowed_cost_centers()
                            if costs and not line.cost_center:
                                line.cost_center = \
                                    line.get_allowed_cost_centers()[0]  # noqa
                    if line.budget is None:
                        budget = (line.account.budget_debit or
                                  line.account.budget_credit)
                        self.raise_user_error('account_without_budget', {
                            'account': line.account.rec_name,
                            'budget': budget.rec_name,
                        })
                    if not line.cost_center:
                        line.cost_center = expense_data.get(
                            'payable_cost_center')
                else:
                    line.budget = None
                    line.cost_center = None
                    line.compromise = None

        line = add_line(move, account, debit, credit, budget)
        set_budget_data(line)
        lines = [line]
        if expense_data.get('previous_year_account'):
            for pya in expense_data['previous_year_account'].keys():
                for pya_year in expense_data[
                        'previous_year_account'].get(pya).keys():
                    amount = expense_data[
                        'previous_year_account'].get(pya).get(pya_year)
                    new_line_2 = add_line(move,
                        pya, amount if line.debit > 0 else line.debit,
                        amount if line.credit > 0 else line.credit)
                    set_budget_data(new_line_2)
                    lines.append(new_line_2)

            new_line_1 = add_line(move, line.account, line.credit, line.debit)
            set_budget_data(new_line_1)
            lines.append(new_line_1)

        return lines

    @classmethod
    def payslip_lb_query_get(cls):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        PayslipRule = pool.get('payslip.rule')
        PayslipLawBenefit = pool.get('payslip.law.benefit')
        PayslipLawBenefitType = pool.get('payslip.law.benefit.type')
        Account = pool.get('account.account')
        Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')
        context = Transaction().context

        payslip = Payslip.__table__()
        payslip_rule = PayslipRule.__table__()
        payslip_lb = PayslipLawBenefit.__table__()
        payslip_lb_type = PayslipLawBenefitType.__table__()
        account = Account.__table__()
        contract = Contract.__table__()
        employee = Employee.__table__()

        debit_lb_columns = [
            payslip.contract.as_('contract'),
            Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None).as_('party'),
            payslip_lb.expense_account.as_('account'),
            payslip_lb.budget.as_('budget'),
            payslip_lb.amount.as_('debit'),
            Literal(0.0).as_('credit'),
            Literal(None).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        credit_lb_columns = [
            payslip.contract.as_('contract'),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None)).as_('party'),
            payslip_lb.payable_account.as_('account'),
            payslip_lb.budget.as_('budget'),
            Literal(0.0).as_('debit'),
            payslip_lb.amount.as_('credit'),
            Case((account.kind == 'payable',
            payslip_lb.expense_account),
                else_=Literal(None)).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        where = Literal(True)
        if context.get('general'):
            where &= payslip.general == context.get('general')

        if context.get('company'):
            where &= payslip.company == context.get('company')

        if context.get('state'):
            where &= payslip.state == context.get('state')

        debit_lb_query = payslip.join(payslip_lb,
            condition=payslip.id == payslip_lb.generator_payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_lb.expense_account
        ).join(payslip_lb_type,
            condition=payslip_lb_type.id == payslip_lb.type_
        ).join(payslip_rule,
            condition=((payslip_rule.law_benefit_type == payslip_lb_type.id)
                       & (payslip_rule.code.like('%accumulate')))
        ).select(*debit_lb_columns,
            where=where,
        )

        credit_lb_query = payslip.join(payslip_lb,
            condition=payslip.id == payslip_lb.generator_payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_lb.payable_account
        ).join(payslip_lb_type,
            condition=payslip_lb_type.id == payslip_lb.type_
        ).join(payslip_rule,
            condition=((payslip_rule.law_benefit_type == payslip_lb_type.id)
                       & (payslip_rule.code.like('%accumulate')))
        ).select(*credit_lb_columns,
            where=where,
        )

        union = Union(debit_lb_query, credit_lb_query, all_=True)

        query = union.select(
            union.account,
            Sum(union.debit).as_('debit'),
            Sum(union.credit).as_('credit'), union.party, union.budget,
            Max(union.parent),
            union.compromise,
            group_by=(union.account, union.budget, union.party, union.compromise),
            order_by=[Sum(union.credit) > 0])

        return query

    @classmethod
    def payslip_query_get(cls):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        PayslipLine = pool.get('payslip.line')
        PayslipRule = pool.get('payslip.rule')
        Account = pool.get('account.account')
        Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')
        RulePrincipal = pool.get('payslip.rule.principal')
        rules = RulePrincipal.search_read([], fields_names=['rule.code'])
        rule_principals = [x['rule.code'] for x in rules]
        context = Transaction().context

        payslip = Payslip.__table__()
        payslip_line = PayslipLine.__table__()
        payslip_rule = PayslipRule.__table__()
        account = Account.__table__()
        deduction_account = Account.__table__()
        contract = Contract.__table__()
        employee = Employee.__table__()

        debit_columns = [
            payslip.contract.as_('contract'),
            Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None).as_('party'),
            payslip_line.expense_account.as_('account'),
            payslip_line.budget.as_('budget'),
            Case(((payslip_line.type_ == 'deduction')
                  & (account.kind == 'expense'),
                Literal(0.0) * payslip_line.amount),
                else_=payslip_line.amount).as_('debit'),
            Literal(0.0).as_('credit'),
            Literal(None).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        credit_columns = [
            payslip.contract.as_('contract'),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None)).as_('party'),
            payslip_line.payable_account.as_('account'),
            payslip_line.budget.as_('budget'),
            Case((payslip_line.revenue_account != Null, payslip_line.amount),
                else_=Literal(0.0)).as_('debit'),
            payslip_line.amount.as_('credit'),
            Case((account.kind == 'payable',
                payslip_line.expense_account),
            else_=Literal(None)).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]
        payslip_line1 = PayslipLine.__table__()
        exist_rmu_query = (payslip_line1.select(
                payslip_line1.id!=Null,
                where=(payslip_line1.type_ == 'income' )&
                  (payslip_line1.expense_account == payslip_line.expense_account) &
                  (payslip.id == payslip_line1.payslip)
        ))
        select_avalible_parent = (payslip_line1.join(payslip_rule,
            condition=(payslip_line1.rule == payslip_rule.id)
        ).select(
            payslip_line1.expense_account.as_('deducible'),
                 where=(payslip_line1.type_ == 'income') &
                       (payslip_rule.code=='commission') &
                        (payslip.id == payslip_line1.payslip)
        ))
        cursor = Transaction().connection.cursor()
        deduction_credit_columns = [
            payslip.contract.as_('contract'),
            Case(((payslip_rule.code.in_(rule_principals)),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None))), else_=None).as_('party'),
            Case(((payslip_rule.code.in_(rule_principals)),
            payslip_line.payable_account),
                else_=None).as_('account'),
            Case(((payslip_rule.code.in_(rule_principals)),
            payslip_line.budget),
                else_=None).as_('budget'),
            Literal(0.0).as_('debit'),
            Case(((payslip_line.type_ == 'deduction')
                  & (deduction_account.kind == 'expense'),
            Literal(-1.0) * payslip_line.amount),
                else_=Literal(0.0)).as_('credit'),
            Case((((account.kind == 'payable' ) & (Exists(exist_rmu_query)) ),
            payslip_line.expense_account),
            (account.kind == 'payable',
             Abs(select_avalible_parent)),
            else_=Literal(None)).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        debit_income_columns = [
            payslip.contract.as_('contract'),
            Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None).as_('party'),
            payslip_line.receivable_account.as_('account'),
            Literal(None).as_('budget'),
            payslip_line.amount.as_('debit'),
            payslip_line.amount.as_('credit'),
            Literal(None).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        credit_income_columns = [
            payslip.contract.as_('contract'),
            Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None).as_('party'),
            payslip_line.revenue_account.as_('account'),
            Literal(None).as_('budget'),
            Literal(0.0).as_('debit'),
            payslip_line.amount.as_('credit'),
            Literal(None).as_('parent'),
            Case((contract.compromise != Null,
                  contract.compromise),
                 else_=Literal(0)).as_('compromise'),
        ]

        where = Literal(True)
        if context.get('general'):
            where &= payslip.general == context.get('general')

        if context.get('company'):
            where &= payslip.company == context.get('company')

        if context.get('state'):
            where &= payslip.state == context.get('state')

        exclude_lb = (~payslip_rule.is_law_benefit
                      | (payslip_rule.is_law_benefit
                          & ~payslip_rule.code.like('%accumulate')))

        debit_query = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_line.expense_account
        ).join(payslip_rule,
            condition=payslip_rule.id == payslip_line.rule
        ).select(*debit_columns,
            where=(where & exclude_lb),
        )

        credit_query = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_line.payable_account
        ).join(payslip_rule,
            condition=payslip_rule.id == payslip_line.rule
        ).select(*credit_columns,
            where=(where & exclude_lb),
        )

        deduction_credit_query = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_line.payable_account
        ).join(deduction_account,
            condition=deduction_account.id == payslip_line.expense_account
        ).join(payslip_rule,
            condition=payslip_rule.id == payslip_line.rule
        ).select(*deduction_credit_columns,
            where=(where & exclude_lb),
        )

        deduction_credit_query = deduction_credit_query.select(
            deduction_credit_query.contract.as_('contract'),
            Max(deduction_credit_query.party).as_('party'),
            Max(deduction_credit_query.account).as_('account'),
            Max(deduction_credit_query.budget).as_('budget'),
            Sum(deduction_credit_query.debit).as_('debit'),
            Sum(deduction_credit_query.credit).as_('credit'),
            deduction_credit_query.parent.as_('parent'),
            deduction_credit_query.compromise.as_('compromise'),
            group_by=[deduction_credit_query.contract,
                deduction_credit_query.parent,
                deduction_credit_query.compromise]
        )

        debit_income_query = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_line.receivable_account
        ).join(payslip_rule,
            condition=payslip_rule.id == payslip_line.rule
        ).select(*debit_income_columns,
            where=where
        )

        credit_income_query = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(contract,
            condition=contract.id == payslip.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == payslip_line.revenue_account
        ).join(payslip_rule,
            condition=payslip_rule.id == payslip_line.rule
        ).select(*credit_income_columns,
            where=where
        )

        union = Union(debit_query, credit_query, deduction_credit_query,
            debit_income_query, credit_income_query,
            all_=True)

        query = union.select(
            union.account,
            Sum(union.debit).as_('debit'),
            Sum(union.credit).as_('credit'), union.party, union.budget,
            Max(union.parent),
            union.compromise,
            group_by=(union.account, union.budget, union.party, union.compromise),
            order_by=[(Sum(union.credit) < 0).asc,
                ((Sum(union.debit) > 0) & (Sum(union.credit) != 0)).asc,
                ((Sum(union.debit) > 0) & (Sum(union.credit) == 0) & (union.party == None)).desc,
                ((Sum(union.debit) > 0) & (Sum(union.credit) == 0)).desc,
                (Sum(union.credit) > 0).desc])
            # order_by=[(Sum(union.credit) < 0).asc, (Sum(union.debit) > 0).desc, (Sum(union.credit) > 0).desc])

        return query

    @classmethod
    def delete_wrong_payslips(cls, general):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Payslip = pool.get('payslip.payslip')
        payslip = Payslip.__table__()
        PayslipLine = pool.get('payslip.line')
        payslip_line = PayslipLine.__table__()
        PayslipRule = pool.get('payslip.rule')
        payslip_rule = PayslipRule.__table__()

        # GET WRONG PAYSLIPS AND ITS LINES
        select_query_B = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(payslip_rule,
            condition=payslip_line.rule == payslip_rule.id
        ).select(
            payslip.id,
            where=((payslip.general == general.id)
                   & ((payslip_rule.payslip_position == 'left')
                   | (payslip_rule.payslip_position == 'right'))),
        )

        select_query_A = payslip.join(payslip_line,
            condition=payslip.id == payslip_line.payslip
        ).join(payslip_rule,
            condition=payslip_line.rule == payslip_rule.id
        ).select(
            payslip.id.as_('payslip'),
            payslip_line.id.as_('line'),
            where=(((payslip.general == general.id)
                & (payslip_rule.payslip_position == 'general'))
                & (~payslip.id.in_(select_query_B))),
        )

        cursor.execute(*select_query_A)
        to_delete_payslips = []
        to_delete_lines = []
        for id_payslip, id_line in cursor.fetchall():
            if not (id_payslip in to_delete_payslips):
                to_delete_payslips.append(id_payslip)
            if not (id_line in to_delete_lines):
                to_delete_lines.append(id_line)

        # DELETE DATA
        if to_delete_payslips:
            PayslipLine.delete(PayslipLine.browse(to_delete_lines))
            Payslip.delete(Payslip.browse(to_delete_payslips))
            notify('Information',
                'No se han encontrado líneas de ingresos y deducciones para '
                'algunos empleados en este rol de pagos general, así que los '
                'roles de pagos individuales para dichos empleados, han sido '
                'removidos.', priority=3)

    @classmethod
    def get_contracts_with_pending_law_benefits_to_pay(cls, start_date,
            end_date, law_benefit_type):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')
        Contract = pool.get('company.contract')
        Payslip = pool.get('payslip.payslip')
        tbl_payslip = Payslip.__table__()
        tbl_period = Period.__table__()
        tbl_law_benefit = LawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()
        tbl_contract = Contract.__table__()

        contracts = []

        if end_date.year == 2021:
            select_query = tbl_contract.select(
                tbl_contract.id,
                where=(tbl_contract.state == 'done'),
            )
        else:

            # Get contracts from previously saved law benefits
            select_query = tbl_law_benefit.join(tbl_law_benefit_type,
                condition=tbl_law_benefit.type_ == tbl_law_benefit_type.id
            ).join(tbl_period,
                condition=tbl_law_benefit.period == tbl_period.id
            ).join(tbl_payslip,
                condition=tbl_law_benefit.generator_payslip == tbl_payslip.id
            ).join(tbl_contract,
                condition=tbl_payslip.contract == tbl_contract.id
            ).select(
                tbl_contract.id,
                where=((tbl_law_benefit.kind == 'accumulate') &
                       (tbl_law_benefit.is_paid_in_liquidation == False) &
                       (tbl_law_benefit_type.code == law_benefit_type.code) &
                       (tbl_payslip.state == 'done') &
                       (tbl_period.start_date >= start_date) &
                       (tbl_period.end_date <= end_date)),
                distinct_on=tbl_contract.id
            )

        cursor.execute(*select_query)
        ids = [row['id'] for row in cursor_dict(cursor)]

        for contract in Contract.browse(ids):
            contracts.append(contract)

        return contracts

    def get_rec_name(self, name):
        return 'ROL GENERAL %s: %s' % (
            self.period.rec_name, self.template.rec_name)

    @classmethod
    def search_move_state(cls, name, clause):
        return [('move.state',) + tuple(clause[1:])]

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('period',) + tuple(clause[1:]),
                  ('template',) + tuple(clause[1:]),
                  ]
        return domain

    @classmethod
    def search_template_for_accumulated_law_benefits(cls, name, clause):
        return [('template.accumulated_law_benefits',) + tuple(clause[1:])]

class Payslip(Workflow, ModelSQL, ModelView):
    'Payslip'
    __name__ = 'payslip.payslip'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS, readonly=True, required=True)
    general = fields.Many2One('payslip.general', 'General',
        domain=[
            ('company', '=', Eval('company')),
            ('fiscalyear', '=', Eval('fiscalyear')),
            ('period', '=', Eval('period')),
        ],  states=_STATES, depends=_DEPENDS + [
            'company', 'fiscalyear', 'period'], ondelete='CASCADE')
    template = fields.Many2One('payslip.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
            ('type', 'in', ['payslip', 'accumulated_law_benefits'])
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    template_for_accumulated_law_benefits = fields.Function(fields.Boolean(
        'Es una plantilla de beneficios de ley acumulados?'),
        'on_change_with_template_for_accumulated_law_benefits')
    employee = fields.Many2One('company.employee', 'Empleado',
        states={
            'readonly': True
        }, required=True)
    contract = fields.Many2One('company.contract', 'Contrato',
        domain=[
            ('company', '=', Eval('company')),
            ('state', 'in', ['done', 'finalized', 'disabled'])
        ],
        context={
            'salary_date': Eval('end_date')
        },
        states=_STATES, depends=_DEPENDS + ['end_date', 'template', 'company'],
        required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], states=_STATES, depends=_DEPENDS + ['company'], required=True)
    period = fields.Many2One('account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ],
        states=_STATES, depends=_DEPENDS + ['fiscalyear'], required=True)
    period_start_date = fields.Function(fields.Date('Fecha inicio de periodo'),
        'on_change_with_period_start_date')
    period_end_date = fields.Function(fields.Date('Fecha fin de periodo'),
        'on_change_with_period_end_date')
    start_date = fields.Date('Inicio de corte', required=True,
        states=_STATES, depends=_DEPENDS + ['end_date', 'period_start_date',
            'template_for_accumulated_law_benefits'])
    end_date = fields.Date('Fin de corte', required=True,
        states=_STATES, depends=_DEPENDS + ['start_date', 'period_end_date',
            'template_for_accumulated_law_benefits'])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], 'State', readonly=True)
    lines = fields.One2Many('payslip.line', 'payslip', 'Líneas')
    income_lines = fields.One2Many('payslip.line', 'payslip', 'Ingresos',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'income')])
    deduction_lines = fields.One2Many('payslip.line', 'payslip', 'Deducciones',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'deduction')])
    internal_lines = fields.One2Many('payslip.line', 'payslip', 'Internas',
        states={
            'readonly': True
        }, filter=[('type_', '=', 'internal')])
    law_benefits = fields.Function(
        fields.One2Many('payslip.law.benefit', None, 'Beneficios de ley',
        states={
            'invisible': ~Greater(Len(Eval('law_benefits')), 1, True)
        }, order=[('period', 'DESC')]), 'get_calculated_law_benefits')
    overtimes = fields.Function(fields.One2Many('hr_ec.overtime.line', None,
        'Horas extras',
        states={
            'invisible': ~Greater(Len(Eval('overtimes')), 1, True) | (
                Bool(Eval('template_for_accumulated_law_benefits')))
        }), 'get_considered_overtime_lines')
    comms_subs = fields.Function(
        fields.One2Many('company.commission.subrogation.detail', None,
        'Encargos/Subrogaciones',
        states={
            'invisible': ~Greater(Len(Eval('comms_subs')), 1, True) | (
                Bool(Eval('template_for_accumulated_law_benefits')))
        }), 'get_considered_comms_subs_details')
    deduction_loans = fields.Function(
        fields.One2Many('company.employee.loan.line', None,
            'Préstamos/Anticipos',
            domain=[
                ('loan.employee', '=', Eval('employee')),
                ('loan.state', 'in', ['open', 'paid']),
                ('state', 'in', ['pending', 'added_to_payslip', 'paid']),
                ('start_date', '<=', Eval('period_end_date')),
            ],
            states={
                'readonly': _STATES['readonly'],
                'invisible': Bool(Eval('template_for_accumulated_law_benefits'))
            }, depends=_DEPENDS + [
                'employee',
                'period_end_date',
                'template_for_accumulated_law_benefits'
            ]),
        'get_considered_deduction_loans', setter='set_deduction_loans')
    income_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Ingresos manuales',
        states={
            'invisible': ~Greater(Len(Eval('income_entries')), 1, True)
        }), 'get_considered_entries')
    deduction_entries = fields.Function(
        fields.One2Many('payslip.entry', None, 'Deducciones manuales',
        domain=[
            ('employee', '=', Eval('employee')),
            ('state', '=', 'done'),
            ('entry_date', '<=', Eval('period_end_date'))
        ], states=_STATES, depends=_DEPENDS + ['employee', 'period_end_date']),
        'get_considered_entries', setter='set_deduction_entries')
    fixed_incomes = fields.Function(fields.One2Many(
        'payslip.fixed.income.deduction', None, 'Ingresos fijos',
        states={
            'invisible': ~Greater(Len(Eval('fixed_incomes')), 1, True)
        }), 'get_considered_payslip_fixed_incomes')
    fixed_deductions = fields.Function(fields.One2Many(
        'payslip.fixed.income.deduction', None, 'Deducciones fijas',
        domain=[
            ('payslip.id', '=', Eval('id'))
        ],
        states={
            'readonly': _STATES['readonly'],
            'invisible': Bool(Eval('template_for_accumulated_law_benefits'))
        }, depends=_DEPENDS + [
            'id',
            'template_for_accumulated_law_benefits'
        ]), 'get_considered_payslip_fixed_deductions',
        setter='set_fixed_deductions')
    permissions = fields.Function(fields.One2Many('hr_ec.permission', None,
        'Permisos con cargo a rol'), 'get_considered_permissions')
    permissions_license = fields.Function(fields.One2Many('hr_ec.permission',
        None, 'Permisos de licencia sin remuneración'),
        'get_considered_permissions_license')
    absences = fields.Function(fields.One2Many('hr_ec.absence', None,
        'Faltas'), 'get_considered_absences')
    workshift_dialings = fields.Function(fields.One2Many(
        'payslip.workshift.dialing', None,
        'Marcaciones en turno de trabajo',
        states={'readonly': True}),
        'get_considered_workshift_dialings',
        setter='set_payslip_workshift_dialings')
    workshift_delays = fields.Function(fields.One2Many(
        'hr_ec.workshift.delay.line', None, 'Atrasos'),
        'get_considered_workshift_delays')
    medical_certificates_discount = fields.Function(fields.One2Many(
        'payslip.medical.certificate.discount', None,
        'Información de descuento por certificados médicos',
        domain=[
            ('employee', '=', Eval('employee')),
        ], states=_STATES, depends=_DEPENDS + ['employee'],
        order=[('order_', 'ASC')]),
        'get_considered_medical_certificates_discount',
        setter='set_medical_certificates_discount')
    medical_certificates = fields.Function(fields.One2Many(
        'galeno.medical.certificate', None, 'Certificados médicos',
        order=[('start_date', 'DESC')]), 'get_considered_medical_certificates')
    period_days = fields.Numeric('Días de periodo', readonly=True,
        states={
            'invisible': Bool(Eval('template_for_accumulated_law_benefits'))
        }, depends=['template_for_accumulated_law_benefits'])
    worked_days = fields.Numeric('Días trabajados (sin normativa)',
        states={
            'readonly': True,
        }, digits=(16, 4))
    worked_days_with_normative = fields.Numeric(
        'Días trabajados (con normativa)',
        states={
            'readonly': True,
        }, digits=(16, 4),
        help='Total de días trabajados aplicando la normativa de faltas.')
    worked_period_days = fields.Numeric(
        'Días del período de trabajo',
        states={
            'readonly': True,
        }, digits=(16, 4),
        help='Total de días entre la fecha de inicio (real) de trabajo y la '
             'fecha fin (real) de trabajo. Por ejemplo, un empleado cuyo '
             'contrato finaliza el 10 de un mes especifico, no tendrá 30 días '
             'de período sino 10.')
    total_absences = fields.Numeric('Total faltas',
        states={
            'readonly': True,
        }, digits=(16, 4))
    total_absences_with_normative = fields.Numeric(
        'Total faltas aplicando normativa',
        states={
            'readonly': True,
        }, digits=(16, 4))
    total_permissions = fields.Numeric('Total permisos con cargo a rol',
        states={
            'invisible': ~Greater(Len(Eval('permissions')), 1, True),
            'readonly': True,
        }, depends=['permissions'], digits=(16, 4),
        help='Las horas que aquí se suman para CARGO AL SUELDO son '
             '(Duración sin fin de semana) y para LICENCIA SIN SUELDO son '
             '(Duración con fin de semana)')
    total_days_disease = fields.Numeric('Total días enfermedad/accidente',
        states={
            'readonly': True,
        }, digits=(16, 4))
    total_income = fields.Numeric('Total de ingresos', digits=(16, 2),
        states={'readonly': True})
    total_deduction = fields.Numeric('Total de deducciones', digits=(16, 2),
        states={'readonly': True})
    total_value = fields.Numeric('Total a pagar', digits=(16, 2),
        states={'readonly': True})
    manual_revision = fields.Boolean('Requiere revisión manual',
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('manual_revision'))
        })
    min_salary_to_receive = fields.Numeric('Valor mínimo a recibir',
        states={
            'readonly': True,
            'invisible': Bool(Eval('template_for_accumulated_law_benefits')) | (
                Bool(Eval('negative_payslip')))
        }, depends=['negative_payslip'], digits=(16, 2))
    payslip_manual_entries = fields.Many2Many('payslip.payslip.manual.entry',
        'payslip', 'entry', 'Entrada manual de rol de pagos')

    # Dict of codes used on a payslip
    dict_codes = fields.One2Many('payslip.dict.codes', 'payslip',
        'Diccionario de códigos',
        states={
            'readonly': True
        })
    dict_codes_template = fields.Function(fields.One2Many(
        'template.payslip.dict.codes', 'payslip',
        'Plantilla diccionario de códigos',
        context={
            'payslip': Eval('id')
        }, order=[('code', 'ASC')]), 'on_change_with_dict_codes_template')

    # Totals information
    detail_time_overtimes = fields.Function(fields.Char('Detalle horas',
        states={
            'invisible': ~Greater(Len(Eval('overtimes')), 1, True)
        }, depends=['overtimes']), 'on_change_with_detail_time_overtimes')
    total_days_with_dialing = fields.Numeric(
        'Días con registro de marcación en turno de trabajo',
        states={
            'readonly': True
        })
    total_delay_hours = fields.Numeric('Total de atrasos en horas',
        states={
            'readonly': True
        }, digits=hr_digits)
    nocturne_surcharge_hours = fields.Numeric(
        'Horas de recargo nocturno en turno de trabajo',
        context={
            'company': Eval('company'),
            'start_date': Eval('start_date'),
            'end_date': Eval('end_date')
        },
        states={
            'readonly': True
        }, depends=['company', 'start_date', 'end_date'])
    negative_payslip = fields.Function(fields.Boolean(
        'rol de pagos negativos',
        states={
            'invisible': True
        }), 'get_negative_payslip')

    # Information about migration
    is_migrated = fields.Boolean('Migración',
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('is_migrated')),
        }, depends=['is_migrated'])

    # EXTRA INFO
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': True
        }, datetime_field='done_date')
    position = fields.Many2One('company.position', 'Puesto',
        states={
            'readonly': True
        }, datetime_field='done_date')
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Relación laboral',
        states={
            'readonly': True
        }, datetime_field='done_date')
    contract_type = fields.Many2One(
        'company.contract_type', 'Tipo de contrato',
        states={
            'readonly': True
        }, datetime_field='done_date')
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol',
        states={
            'readonly': False,  # TODO: use a group to control access
        })
    salary = fields.Numeric('Sueldo', digits=(16, 2),
        states={
            'readonly': True
        })
    email_recipient = fields.Many2One('party.party', 'Destinatario e-mail',
        states={
            'readonly': True
        })
    xiii = fields.Selection(_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS, 'XIII',
        states={
            'readonly': True
        })
    xiv = fields.Selection(_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS, 'XIV',
        states={
            'readonly': True
        })
    fr = fields.Selection(_PAYSLIP_LAW_BENEFITS_PAYMENT_FORMS,
        'Fondos de Reserva',
        states={
            'readonly': True
        })
    xiii_translated = xiii.translated('xiii')
    xiv_translated = xiv.translated('xiv')
    fr_translated = fr.translated('fr')
    done_date = fields.DateTime(
        'Pasó a realizado', states={
            # 'invisible': ~Eval('done_date'),
            'invisible': False,
            'readonly': True,
        })

    # ICONS
    manual_revision_color = fields.Function(fields.Char('color'),
        'get_manual_revision_color')

    @classmethod
    def __setup__(cls):
        super(Payslip, cls).__setup__()
        t = cls.__table__()
        # cls._sql_constraints += [
        #     ('payslip_uniq', Unique(
        #         t, t.template, t.contract, t.period, t.state),
        #         'Rol individual debe ser único por contrato, plantilla, '
        #         'período y esado.'),
        # ]
        cls._error_messages.update({
            'no_delete': ('Sólo se pueden eliminar registros en estado '
                '"Borrador".'),
            'expression_error': ('Error de expresión en la regla "%(rule)s":\n'
                '"%(error_message)s"'),
            'condition_error': ('Error de condición en la regla "%(rule)s":\n'
                '"%(error_message)s"'),
            'payslip_state': 'El rol "%(payslip)s"  debe estar en borrador.',
            'contract_with_wrong_salary': ('El contrato "%(contract)s" tiene '
                'mal definido el salario "$%(salary)s".'),
            'contract_with_wrong_hour_cost': ('El contrato "%(contract)s" '
                'tiene mal definido el salario por hora "$%(hour_cost)s".'),
            'no_account': ('No existe cuenta por pagar y/o cuenta de gasto '
                'definidas para la regla "%(rule)s" y el empleado '
                '"%(employee)s."'),
            'invalid_result': ('Resultado inválido "%(result)s" en la regla '
                '"%(rule)s" y empleado "%(employee)s"'),
            'sri_form_107_not_found': ('No es posible calcular el ingreso de '
                '"%(employee)s" debido a que su Formulario SRI 107 del '
                'año fiscal "%(fiscalyear)s" no ha sido encontrado.'),
            'sri_conf_income_tax_error': ('No hay valores en la tabla de '
                'configuracion de impuestos de ingresos que concuerden con los'
                'ingresos proyectados definidos por el empleado en su '
                'formulario 107'),
            'sri_conf_basic_fraction_error': ('La fracción basica del impuesto '
                'de ingreso para el año fiscal vigente para el formulario 107 '
                ' no puede ser determinada.'),
            'total_value_less_than_minimum': ('El rol de pagos del periodo '
                '"%(period)s" para el empleado "%(employee)s" requiere '
                'una revision manual. El valor total ($%(total_value)s) a pagar'
                ' es menor al mínimo requerido por el empleado '
                '($%(minimum_required)s). Por favor elimine algunas '
                'deducciones.'),
            'law_benefit_xiv_not_found': ('El beneficio de ley correspondiente '
                'al Décimo Cuarto debe tener asignado el código "xiv". '
                'Porfavor, asigne el código indicado al registro '
                'correspondiente o bien, proceda a crearlo.'),
            'expression_error_updating_xiv': ('No se han podido actualizar los '
                'valores del Décimo Cuarto Sueldo por que ha ocurrido un error '
                'de expresión en la regla: "%(error_message)s".'),
            'header_error_wrong_template': ('La plantilla del rol '
                '"%(payslip)s" no coincide con la plantilla de su rol general '
                '"%(general)s". Para actualizar la información, diríjase al '
                'rol general, presione el botón "Cargar roles" y luego el '
                'botón "Generar valores".'),
            'header_error_wrong_period': ('El periodo del rol "%(payslip)s" no '
                'coincide con el periodo de su rol general "%(general)s". Para '
                'actualizar la información, diríjase al rol general, presione '
                'el botón "Cargar roles" y luego el botón "Generar valores".'),
            'error_unique_constraint': ('Ya existe un rol de pagos individual '
                'del empleado con contrato "%(contract)s" para el período '
                '"%(period)s", dicho rol usa la plantilla "%(template)s".'),
            'undefined_period': ('No se ha definido en el sistema un período '
                'que contenga la fecha "%(date)s" del historial de pagos del '
                'empleado "%(employee)s".'),
            'email_notification_not_configured': ('No se ha encontrado la '
                'configuraciónn de notificaciónes por email para roles de '
                'pago. Por favor, diríjase a Talento Humano / Roles de Pagos / '
                'Configuración / Configuraciones varias y defina la '
                'configuración solicitada.'),
            'employee_without_email': ('No se ha encontrado un email para el '
                'empleado %(employee)s. Usted puede configurar el email en el '
                'registro del Tercero asociado al empleado, en la sección '
                '"Métodos de contacto".'),
            'error_payslip_dates_overtime_dates': ('Error en el rol de pagos '
                'con plantilla %(template)s.\n\nPor razones de seguridad, '
                'usted no puede generar el rol de pagos si, en el período del '
                'rol (%(period)s), la fecha de inicio de aprobación de horas '
                'extra (%(overtime_start_date)s) no es igual a la fecha de '
                'inicio de corte del rol de pagos (%(payslip_start_date)s).\n\n'
                'Diríjase a "[..] / Ejercicios fiscales / Períodos", busque el '
                'período %(period)s y solucione el conflicto.'),
            'warning_fr_info': ('El sistema ha determinado que es necesario '
                'pagar fondos de reserva a los siguientes empleados:'
                '\n%(message)s\n\nSin embargo, sus contratos no tienen '
                'activado el check de fondos de reserva.\n\nSi Ud desea '
                'continuar, pulse en SI y el sistema procederá a realizar el '
                'pago y activará el check de Fondos de Reserva '
                'automáticamente.')
        })
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-undo'
            },
            'cancel': {
                'invisible': True,
                'depends': ['state'],
                'icon': 'tryton-clear'
            },
            'confirm': {
                'invisible': Eval('state').in_(['confirm', 'cancel', 'done']),
                'depends': ['state'],
                'icon': 'tryton-forward'
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-ok'
            },
            'load_lines': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'play-arrow'
            },
            'recalculate_payslip': {
                'invisible': Eval('state').in_(['confirm', 'done', 'cancel']),
                'depends': ['state'],
                'icon': 'tryton-refresh'
            },
            'send_payment_notice_to_email': {
                'invisible': ~Bool(Eval('state').in_(['done'])),
                'depends': ['state'],
                'icon': 'tryton-email'
            },
        })
        cls._order.insert(0, ('manual_revision', 'DESC'))
        cls._order.insert(1, ('period.end_date', 'DESC'))
        cls._order.insert(2, ('period.start_date', 'DESC'))
        cls._order.insert(3, ('employee.party.name', 'ASC'))

    @classmethod
    def __register__(cls, module_name):
        super(Payslip, cls).__register__(module_name)
        _table = cls.__table_handler__(module_name)
        _table.drop_constraint('payslip_uniq')

    @classmethod
    def view_attributes(cls):
        return super(Payslip, cls).view_attributes() + [
            ('//page[@id="join_payslip_general"]', 'states',
             {
                 'invisible': Bool(Eval('general'))
             }),
            ('//group[@id="grp_absences_info"]', 'states',
             {
                 'invisible': Bool(Eval(
                     'template_for_accumulated_law_benefits'))
             }),
            ('//group[@id="grp_permissions_info"]', 'states',
             {
                 'invisible': Bool(Eval(
                     'template_for_accumulated_law_benefits'))
             }),
            ('//group[@id="grp_dialings_info"]', 'states',
             {
                 'invisible': Bool(Eval(
                     'template_for_accumulated_law_benefits'))
             }),
            ('//group[@id="grp_delay_lines_info"]', 'states',
             {
                 'invisible': Bool(Eval(
                     'template_for_accumulated_law_benefits'))
             }),
            ('//group[@id="grp_medical_certificates_discount"]', 'states',
             {
                 'invisible': Bool(Eval(
                     'template_for_accumulated_law_benefits'))
             })
        ]

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        now = datetime.today().date()
        periods = Period.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if periods:
            return periods[0].id
        return None

    @classmethod
    def default_total_income(cls):
        return 0

    @classmethod
    def default_total_deduction(cls):
        return 0

    @classmethod
    def default_total_value(cls):
        return 0

    @classmethod
    def default_min_salary_to_receive(cls):
        return 0

    @classmethod
    def default_xiii(cls):
        return 'monthlyse'

    @classmethod
    def default_xiv(cls):
        return 'monthlyse'

    @classmethod
    def default_fr(cls):
        return 'monthlyse'

    @classmethod
    def default_total_days_with_dialing(cls):
        return 0

    @classmethod
    def default_nocturne_surcharge_hours(cls):
        return 0

    @classmethod
    def default_total_delay_hours(cls):
        return 0

    @staticmethod
    def order_employee(tables):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        table, _ = tables[None]
        if 'employee' not in tables:
            employee = Employee.__table__()
            tables['employee'] = {
                None: (employee, table.employee == employee.id),
            }
        else:
            employee = tables['employee']
        if 'party' not in tables:
            party = Party.__table__()
            tables['party'] = {
                None: (party, employee.party == party.id),
            }
        else:
            party = tables['party']
        return Party.name.convert_order('name', tables['party'], Party)

    @classmethod
    def validate(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        for payslip in payslips:
            # VALIDATE UNIQUE PAYSLIPS
            if not payslip.template.accumulated_law_benefits and (
                    not 'extra' in payslip.general.template.code):
                list_payslips = Payslip.search([
                    ('id', '!=', payslip.id),
                    ('company', '=', payslip.company),
                    ('contract', '=', payslip.contract),
                    ('template.accumulated_law_benefits', '=', False),
                    ('period', '=', payslip.period),
                    ('state', 'not in', ['cancel']),
                    ('template', '=', payslip.template)
                ])
                if list_payslips:
                    cls.raise_user_error('error_unique_constraint', {
                        'contract': payslip.contract.rec_name,
                        'period': payslip.period.rec_name,
                        'template': list_payslips[0].template.name,
                    })

            # VALIDATE PAYSLIP PERIOD CUTOFF DATES AND OVERTIMES APPROVAL DATES
            if payslip.state in ['draft']:
                payslip_start_date = payslip.period.payslip_start_date
                overtime_start_date = payslip.period.overtime_start_date
                if overtime_start_date != payslip_start_date:
                    cls.raise_user_error('error_payslip_dates_overtime_dates', {
                        'employee': payslip.employee.rec_name,
                        'period': payslip.period.rec_name,
                        'template': payslip.template.name,
                        'overtime_start_date': overtime_start_date,
                        'payslip_start_date': payslip_start_date
                    })

    @classmethod
    def delete(cls, payslips):
        pool = Pool()
        LawBenefit = pool.get('payslip.law.benefit')
        to_delete_lb = []
        to_save_lb = []
        for payslip in payslips:
            if not Transaction().context.get('payslip_cancel_state'):
                if payslip.state in ['done', 'confirm', 'cancel']:
                    cls.raise_user_error('no_delete')

            if payslip.template_for_accumulated_law_benefits:
                if payslip.law_benefits:
                    for benefit in payslip.law_benefits:
                        if benefit.generator_payslip == payslip:
                            to_delete_lb.append(benefit)
                        else:
                            benefit.payslip = None
                            to_save_lb.append(benefit)
            else:
                law_benefits = LawBenefit.search([
                    ('generator_payslip', '=', payslip)
                ])
                if law_benefits:
                    to_delete_lb += law_benefits

        LawBenefit.save(to_save_lb)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_lb)
        super(Payslip, cls).delete(payslips)

    @classmethod
    @ModelView.button
    def send_payment_notice_to_email(cls, payslips):
        pool = Pool()
        NotificationEmail = pool.get('notification.email')
        notification_email = get_notification_email_payslip_conf()
        if notification_email:
            for payslip in payslips:
                if not payslip.employee.email:
                    cls.raise_user_error('employee_without_email', {
                        'employee': payslip.employee.rec_name
                    })
            NotificationEmail.trigger(payslips, notification_email.triggers[0])
        else:
            cls.raise_user_error('email_notification_not_configured')

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, payslips):
        cls.change_loan_lines_states(payslips, 'pending', None)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, payslips):
        cls.check_header_information(payslips)
        cls.change_loan_lines_states(payslips, 'paid', date.today())
        cls.set_done_date(payslips)

    @classmethod
    def set_done_date(cls, payslips):
        Payslip = Pool().get('payslip.payslip')
        today = datetime.today()
        list_payslips = []
        for payslip in payslips:
            payslip.done_date = today
            list_payslips.append(payslip)

        Payslip.save(list_payslips)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, payslips):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        LawBenefit = pool.get('payslip.law.benefit')
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        PayslipDeductionLoanLine = pool.get(
            'payslip.payslip.deduction.loan.line')
        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')

        to_delete_lb = []
        to_delete_me = []
        to_delete_ll = []
        to_delete_ol = []
        to_delete_csd = []
        to_delete_ab = []
        to_delete_per = []
        to_delete_mcd = []

        # Change loans states
        cls.change_loan_lines_states(payslips, 'pending', None)

        for payslip in payslips:
            # Delete Law Benefits
            to_delete_lb += LawBenefit.search([
                ('generator_payslip', '=', payslip)
            ])

            # Delete register of considered models in this payslip
            considered_models = [
                (to_delete_me, PayslipManualEntry),
                (to_delete_ll, PayslipDeductionLoanLine),
                (to_delete_ol, PayslipOvertimeLine),
                (to_delete_csd, PayslipCommSubDetail),
                (to_delete_ab, PayslipAbsence),
                (to_delete_per, PayslipPermission),
                (to_delete_mcd, PayslipMedicalCertificateDiscount),
            ]
            for model in considered_models:
                to_delete_list = model[0]
                considered_model = model[1]
                instances = considered_model.search([
                    ('payslip', '=', payslip.id)
                ])
                if instances:
                    to_delete_list += instances

        # There can not be two individual payslips with the same template,
        # employee, period and state (In "cancel" state, that could happen)
        to_delete = []
        for payslip in payslips:
            canceled_payslips = Payslip.search([
                ('template', '=', payslip.template),
                ('employee', '=', payslip.employee),
                ('period', '=', payslip.period),
                ('state', '=', 'cancel'),
            ])
            if canceled_payslips:
                to_delete += canceled_payslips

        # Execute deletions
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_lb)
        PayslipManualEntry.delete(to_delete_me)
        PayslipDeductionLoanLine.delete(to_delete_ll)
        PayslipOvertimeLine.delete(to_delete_ol)
        PayslipCommSubDetail.delete(to_delete_csd)
        PayslipAbsence.delete(to_delete_ab)
        PayslipPermission.delete(to_delete_per)
        PayslipMedicalCertificateDiscount.delete(to_delete_mcd)

        # This ignores the restriction of not deleting records in cancel state
        with Transaction().set_context(payslip_cancel_state=True):
            Payslip.delete(to_delete)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, payslips):
        cls.check_header_information(payslips)
        cls.check_min_salary_to_receive(payslips)
        cls.change_loan_lines_states(payslips, 'added_to_payslip', date.today())

    @classmethod
    def check_header_information(cls, payslips):
        if payslips:
            for payslip in payslips:
                if payslip.general:
                    if payslip.template != payslip.general.template:
                        cls.raise_user_error('header_error_wrong_template', {
                            'payslip': payslip.rec_name,
                            'general': payslip.general.rec_name
                        })
                    elif payslip.period != payslip.general.period:
                        cls.raise_user_error('header_error_wrong_period', {
                            'payslip': payslip.rec_name,
                            'general': payslip.general.rec_name
                        })

    @classmethod
    def check_min_salary_to_receive(cls, payslips):
        for payslip in payslips:
            min_salary = payslip.min_salary_to_receive
            min_salary = min_salary if min_salary else 0
            if not get_permit_totals_negative() and (
                    payslip.total_value < min_salary):
                cls.raise_user_error('total_value_less_than_minimum', {
                    'period': payslip.period.rec_name,
                    'employee': payslip.employee.rec_name,
                    'total_value': payslip.total_value,
                    'minimum_required': payslip.min_salary_to_receive
                })

    @classmethod
    def change_loan_lines_states(cls, payslips, new_state, payment_date):
        pool = Pool()
        EmployeeLoan = pool.get('company.employee.loan')
        EmployeeLoanLine = pool.get('company.employee.loan.line')
        loans_to_analize = {}
        to_save_loan_lines = []
        to_save_loan_general = []
        # FIRST: The loan line is updated to its new state
        for payslip in payslips:
            if payslip.deduction_loans:
                for loan_line in payslip.deduction_loans:
                    loan_line.state = new_state
                    loan_line.effective_date = payment_date
                    loan_line.payment_date = payment_date
                    to_save_loan_lines.append(loan_line)
                    if loan_line.loan.id not in loans_to_analize:
                        loans_to_analize[loan_line.loan.id] = loan_line.loan
        # At this point, it is necessary to save the changes of the lines
        EmployeeLoanLine.save(to_save_loan_lines)

        # SECOND: Check if loans need to change their states to 'paid' or not
        for id, loan in loans_to_analize.items():
            total_loans = len(loan.lines)
            if total_loans > 0:
                search_loans = EmployeeLoanLine.search([
                    ('loan', '=', loan),
                    ('state', 'in', ['paid'])
                ])
                if len(search_loans) == total_loans:
                    loan.state = 'paid'
                else:
                    loan.state = 'open'
                to_save_loan_general.append(loan)
        EmployeeLoan.save(to_save_loan_general)

    @classmethod
    def clear_comms_subs_payslips(cls, payslips):
        for payslip in payslips:
            for comms_subs in payslip.comms_subs:
                comms_subs.payslip = None
                comms_subs.save()

    @fields.depends('contract')
    def on_change_with_email_recipient(self, name=None):
        if self.contract:
            return self.contract.employee.party.id
        return None

    @fields.depends('template')
    def on_change_with_template_for_accumulated_law_benefits(self, name=None):
        if self.template:
            return self.template.accumulated_law_benefits
        return None

    @fields.depends('income_lines')
    def on_change_with_total_income(self, name=None):
        total = 0
        if self.income_lines:
            for income in self.income_lines:
                if income.amount:
                    total += income.amount
        return total

    @fields.depends('deduction_lines')
    def on_change_with_total_deduction(self, name=None):
        total = 0
        if self.deduction_lines:
            for deduction in self.deduction_lines:
                if deduction.amount:
                    total += deduction.amount
        return total

    @fields.depends('total_income', 'total_deduction')
    def on_change_with_total_value(self, name=None):
        total = 0
        if self.total_income and self.total_deduction:
            total = self.total_income - self.total_deduction
        return total

    @fields.depends('period')
    def on_change_with_period_start_date(self, name=None):
        if self.period:
            return self.period.start_date
        return None

    @fields.depends('period')
    def on_change_with_period_end_date(self, name=None):
        if self.period:
            return self.period.end_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_start_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - relativedelta(months=12)
            else:
                if self.period.payslip_start_date:
                    return self.period.payslip_start_date
                else:
                    return self.period.start_date
        return None

    @fields.depends('period', 'template')
    def on_change_with_end_date(self, name=None):
        if self.period and self.period.start_date:
            if self.template and self.template.accumulated_law_benefits:
                # Accumulate law benefits are calculated in one-year periods
                # until a day before the start of the current period
                return self.period.start_date - timedelta(days=1)
            else:
                if self.period.payslip_end_date:
                    return self.period.payslip_end_date
                else:
                    return self.period.end_date
        return None

    @fields.depends('employee')
    def on_change_with_contract(self, name=None):
        if self.employee:
            return self.employee.contract.id
        return None

    @fields.depends('contract')
    def on_change_with_employee(self, name=None):
        if self.contract:
            return self.contract.employee.id
        return None

    @fields.depends('start_date', 'end_date', 'employee')
    def on_change_with_absences(self, name=None):
        pool = Pool()
        Absence = pool.get('hr_ec.absence')
        absences = Absence.search([
            ('employee', '=', self.employee),
            ('state', '=', 'done'),
            ('start_date', '>=', self.start_date),
            ('end_date', '<=', self.end_date),
            ('is_paid_in_liquidation', '!=', True),
            ])
        return [x.id for x in absences]

    @fields.depends('start_date', 'end_date', 'contract')
    def on_change_with_permissions(self, name=None):
        pool = Pool()
        Permission = pool.get('hr_ec.permission')
        permissions = Permission.search([
            ('employee', '=', self.contract),
            ('discount_to', 'in', ['payslip', 'license']),
            ('state', '=', 'done'),
            ('start_date', '>=', self.start_date),
            ('end_date', '<=', self.end_date),
            ])
        return [x.id for x in permissions]

    @fields.depends('overtimes')
    def on_change_with_detail_time_overtimes(self, name=None):
        str_total_hours = ""
        detail = self.get_detail_by_overtime_type()
        total_hours = detail['total_hours']
        total_money = detail['total_money']
        if total_hours:
            for key, time_ in total_hours.items():
                money = total_money[key]
                str_total_hours = (
                    f"{str_total_hours}Total {key}: {time_}h ${money},  ")
            if len(str_total_hours) > 0:
                str_total_hours = str_total_hours[:-3]
        return str_total_hours

    @fields.depends('id')
    def on_change_with_dict_codes_template(self, name=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        DictCodes = pool.get('template.payslip.dict.codes')

        dict_codes = None
        with Transaction().set_context(payslip=self.id):
            dict_codes = DictCodes.table_query()
        ids_dict_codes = []
        cursor.execute(*dict_codes)
        for dict_code in cursor.fetchall():
            ids_dict_codes.append(dict_code[0])

        return ids_dict_codes

    @fields.depends('total_value', 'min_salary_to_receive', 'period_days',
        'total_days_disease', 'template_for_accumulated_law_benefits',
        'is_migrated')
    def on_change_with_manual_revision(self, name=None):
        if not self.is_migrated:
            if self.total_value and self.min_salary_to_receive:
                return (self.total_value < self.min_salary_to_receive)
            if not self.min_salary_to_receive:
                if not self.template_for_accumulated_law_benefits:
                    if self.period_days and self.total_days_disease:
                        return (self.period_days < self.total_days_disease)
        return False

    # @fields.depends('medical_certificates_discount', 'total_days_disease',
    #     'manual_revision', 'template_for_accumulated_law_benefits',
    #     'is_migrated')
    # def on_change_medical_certificates_discount(self, name=None):
    #     if self.medical_certificates_discount:
    #         days = 0
    #         for medical in self.medical_certificates_discount:
    #             days += medical.number_days_month
    #         self.total_days_disease = days
    #     if not self.template_for_accumulated_law_benefits:
    #         self.manual_revision = self.on_change_with_manual_revision()

    def get_detail_by_overtime_type(self):
        total_hours = defaultdict(lambda: None)
        total_money = defaultdict(lambda: None)
        if self.overtimes:
            for line in self.overtimes:
                if line.type_.description not in total_hours:
                    total_hours[line.type_.description] = line.time_round
                    total_money[line.type_.description] = (
                        line.total_with_salary)
                else:
                    total_hours[line.type_.description] += line.time_round
                    total_money[line.type_.description] += (
                        line.total_with_salary)
        return {
            'total_hours': total_hours,
            'total_money': total_money
        }

    def get_total_money_by_overtime_type(self):
        total_hours = defaultdict(lambda: None)
        if self.overtimes:
            for line in self.overtimes:
                if line.type_.description not in total_hours:
                    total_hours[line.type_.description] = line.time_round
                else:
                    total_hours[line.type_.description] += line.time_round
        return total_hours

    @classmethod
    def apply_contract_data(cls, payslips):
        apply_contract_data(cls, payslips)

    @classmethod
    @ModelView.button
    def recalculate_payslip(cls, payslips):
        cls.load_lines(payslips, is_recalculate=True)

    @classmethod
    @ModelView.button
    def load_lines(cls, payslips, is_recalculate=False):
        # Verify headers of individual payslips match with general payslips
        cls.check_header_information(payslips)

        # Set departments in every individual payslips
        cls.apply_contract_data(payslips)

        accumulated, no_accumulated = [], []
        for payslip in payslips:
            if payslip.template.accumulated_law_benefits:
                accumulated.append(payslip)
            else:
                no_accumulated.append(payslip)

        if accumulated:
            cls.load_lines_accumulated_templates(accumulated,
                is_recalculate=is_recalculate)
        if no_accumulated:
            cls.load_lines_no_accumulated_templates(no_accumulated,
                is_recalculate=is_recalculate)

    @classmethod
    def load_lines_no_accumulated_templates(cls, payslips,
            is_recalculate=False):
        # Get models
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        Line = pool.get('payslip.line')
        LawBenefit = pool.get('payslip.law.benefit')
        PayslipManualEntry = pool.get(
            'payslip.payslip.manual.entry')
        PayslipDeductionLoadLine = pool.get(
            'payslip.payslip.deduction.loan.line')
        PayslipFixedIncomeDeduction = pool.get(
            'payslip.fixed.income.deduction')
        PayslipOvertimeLine = pool.get(
            'payslip.payslip.overtime.line')
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')
        PayslipPermissionLicense = pool.get(
            'payslip.payslip.hr_ec.permission.license')
        PayslipWorkshiftDelayLine = pool.get(
            'payslip.payslip.hr_ec.workshift.delay.line')
        PayslipDictCodes = Pool().get('payslip.dict.codes')
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')

        #Get code rules
        rules = {}
        for row in payslips[0].general.template.lines:
            rules[row.rule.code] = True

        # Reserve Funds information
        fr_info = cls.get_reserve_funds_info(payslips)

        # Overtimes
        overtimes = defaultdict(lambda: [])
        if rules.get('overtimes'):
            overtimes = cls.get_all_overtime_lines(payslips)['overtimes']

        # Subrogations
        comms_subs, comms_subs_work_days = cls.get_all_comms_subs_details(
            payslips, rules.get('commission'),
            rules.get('subrogation'), rules.get('commission_other'))

        # Dedduction loans
        deduction_loans = defaultdict(lambda: [])
        if rules.get('anticipos') or rules.get('prestamos'):
            deduction_loans = cls.get_all_deduction_loans(payslips,
                is_recalculate=is_recalculate)['deduction_loans']

        # Manual entries
        manual_entries = cls.get_all_entries(payslips,
            is_recalculate=is_recalculate)
        income_entries = manual_entries['income_entries']
        deduction_entries = manual_entries['deduction_entries']

        # Fixed incomes info
        fixed_incomes = cls.get_all_fixed_incomes(payslips,
            is_recalculate=is_recalculate)['fixed_incomes']

        # Fixed deductions info
        fixed_deductions = cls.get_all_fixed_deductions(payslips,
            is_recalculate=is_recalculate)['fixed_deductions']

        # Absences and permissions with discount to payslip and license
        # registers (ids only)
        absences = cls.get_all_absences(payslips)['absences']
        permissions = cls.get_all_permissions(payslips)['permissions']
        permissions_license = (
            cls.get_all_permissions_license(payslips)['permissions_license'])

        # Get all law benefits types
        law_benefit_types = cls.get_law_benefit_types()

        # Get work days information
        info_days = cls.get_days(payslips)
        period_days = info_days['period_days']
        worked_period_days = info_days['worked_period_days']

        # Get absences in total days
        info_absences = cls.get_absences(payslips)
        total_absences = info_absences['total_absences']
        total_absences_with_normative = (
            info_absences['total_absences_with_normative'])
        total_permissions = info_absences['total_permissions']
        total_permissions_all = info_absences['total_permissions_all']

        # Get assistance detail information
        assistance_detail = cls.get_assistance_detail(payslips)

        # Get total days with dialings
        total_days_with_dialing = (
            cls.get_total_days_with_dialing(
                payslips,
                assistance_detail=assistance_detail
            )['total_days_with_dialing'])

        # Get total days on overtimes outside the workshift
        total_days_on_overtimes = (
            cls.get_total_days_on_overtimes(
                payslips,
                assistance_detail=assistance_detail,
                overtimes=overtimes
            )['total_days_on_overtimes'])

        # Get nocturne surcharge hours
        nocturne_surcharge_hours = defaultdict(lambda: 0)
        if rules.get('recargo_nocturno'):
            nocturne_surcharge_hours = (
                cls.get_nocturne_surcharge_hours(
                    payslips,
                )['nocturne_surcharge_hours'])

        # Get delay lines info
        result_delay_lines = cls.get_delay_lines_detail(payslips)
        workshift_delay_lines = result_delay_lines['workshift_delay_lines']
        total_delay_hours = result_delay_lines['total_delay_hours']

        # Get non-dialings info
        result_non_dialings = cls.get_non_dialings_detail(payslips)
        total_non_dialings = result_non_dialings['total_non_dialings']
        total_non_dialings_days = result_non_dialings['total_non_dialings_days']

        # Medical certificates
        info_medical = cls.get_all_medical_certificates_info(payslips,
            is_recalculate=is_recalculate)
        medical_certificates_info = info_medical['medical_certificates_info']
        total_days_disease = info_medical['total_days_disease']

        # Get holidays info
        info_holidays = cls.get_holidays(payslips)
        holidays = info_holidays['holidays']
        holidays_dates = info_holidays['holidays_dates']

        # Get amounts
        overtimes_amounts = Payslip.get_overtimes_amounts(payslips, overtimes)
        comms_subs_amounts = Payslip.get_comms_subs_amounts(
            payslips, comms_subs)
        loans_amounts = Payslip.get_loans_amounts(payslips, deduction_loans)

        comms_subs_amounts_by_type = (
            Payslip.get_comms_subs_amounts_by_type(payslips, comms_subs))

        loans_amounts_by_type = (
            Payslip.get_loans_amounts_by_type(payslips, deduction_loans))

        overtimes_amounts_by_type = (
            Payslip.get_overtimes_amounts_by_type(payslips, overtimes))

        # Get amounts by mannual entry
        manual_entry_amounts = Payslip.get_manual_entries_amounts(payslips,
            income_entries, deduction_entries)

        # Get amounts by fixed income/deduction
        fixed_incomes_amounts = Payslip.get_fixed_incomes_amounts(
            payslips, fixed_incomes)
        fixed_deductions_amounts = Payslip.get_fixed_deductions_amounts(
            payslips, fixed_deductions)

        # Get sri configs
        sri_conf = Payslip.get_sri_conf_income_tax(payslips)

        # Get worked days info
        res_wd = Payslip.get_worked_days_info(
            payslips,
            worked_period_days=worked_period_days,
            absences=total_absences,
            absences_with_normative=total_absences_with_normative,
            permissions=total_permissions,
            days_disease=total_days_disease)
        worked_days = res_wd['worked_days']
        worked_days_with_normative = res_wd['worked_days_with_normative']

        to_save_payslip_fixed_values = []
        to_delete_payslip_fixed_values = []
        to_save_payslip_overtime_lines = []
        to_save_payslip_comm_subs_details = []
        to_save_payslip_manual_entries = []
        to_save_payslip_deduction_loans = []
        to_save_payslip_absences = []
        to_save_payslip_permissions = []
        to_save_payslip_permissions_license = []
        to_save_payslip_workshift_delay_lines = []
        to_update_payslips = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []
        to_delete_lines = []
        to_save_dict_codes = []
        to_delete_dict_codes = []
        to_save_managed_workshift_dialings = []
        to_delete_managed_workshift_dialings = []
        to_save_managed_med_cert_dis = []
        to_delete_managed_med_cert_dis = []

        totals = defaultdict(lambda: [])
        salaries = defaultdict(lambda: [])
        hour_costs = defaultdict(lambda: [])

        for payslip in payslips:
            with Transaction().set_context(salary_date=payslip.period.end_date):
                # Set information related to work days, absences, permissions
                # and diseases on payslip
                payslip.period_days = period_days[payslip.id]
                payslip.worked_period_days = worked_period_days[payslip.id]
                payslip.worked_days = worked_days[payslip.id]
                payslip.total_absences = total_absences[payslip.id]
                payslip.total_absences_with_normative = (
                    total_absences_with_normative[payslip.id])
                payslip.total_permissions = total_permissions[payslip.id]
                payslip.worked_days_with_normative = (
                    worked_days_with_normative[payslip.id])
                payslip.total_days_disease = total_days_disease[payslip.id]
                payslip.total_days_with_dialing = (
                    total_days_with_dialing[payslip.id])
                payslip.total_delay_hours = total_delay_hours[payslip.id]
                payslip.nocturne_surcharge_hours = (
                    nocturne_surcharge_hours[payslip.id])

                # Get employee salaries and hour_costs
                info_salaries = cls.get_info_salary(payslip)
                salaries = info_salaries['salaries']
                hour_costs = info_salaries['hour_costs']

                # Set salary on payslip
                payslip.salary = salaries[payslip.id]

                # Totals definition
                totals[payslip.id] = {
                    'total_income': ZERO,
                    'total_deduction': ZERO,
                    'total_income_law_benefits': ZERO
                }

                # Delete previous lines
                to_delete_lines += payslip.lines

                # Get form 107 info for income taxes calculation
                form107 = sri_conf[payslip.id]['form107']
                projected_expenses = cls.get_projected_expenses(form107)
                projected_incomes = cls.get_projected_incomes(
                    payslip, form107, salaries[payslip.id])

                # Get income tax configuration
                basic_fraction_annual = (
                    sri_conf[payslip.id]['basic_fraction_annual'])
                discount_third_age = (
                    sri_conf[payslip.id]['discount_third_age'])
                discount_disability = (
                    sri_conf[payslip.id]['discount_disability'])
                discount_disability_burden = (
                    sri_conf[payslip.id]['discount_disability_burden'])

                # Get number of family charges
                num_family_burdens = cls.get_number_family_burdens(
                    payslip.employee)

                # Get payment form XIII, XIV
                law_benefits_payment_form = (
                    cls.get_law_benefits_payment_form_by_payslip(payslip))

                # Update payment form XIII, XIV on payslip
                payslip.xiii = law_benefits_payment_form['xiii']
                payslip.xiv = law_benefits_payment_form['xiv']
                payslip.fr = law_benefits_payment_form['fr']

                # Get minimum of salary to receive for an employee
                min_salary_to_receive = (
                    payslip.get_min_salary_to_receive(salaries[payslip.id]))

                # Get contract work relationship
                work_relationship = ''
                if payslip.contract.work_relationship:
                    work_relationship = payslip.contract.work_relationship.name

                # Get contract type
                contract_type = ''
                if payslip.contract.contract_type:
                    contract_type = payslip.contract.contract_type.name

                occupational_group = ''
                if payslip.contract.occupational_group:
                    occupational_group = payslip.contract.occupational_group.name

                hierarchical_level = ''
                if payslip.contract.hierarchical_level:
                    hierarchical_level = payslip.contract.hierarchical_level.name


                # Effective worked days: obtained from days with dialing on
                # workshifts and days on overtimes
                effective_worked_days = (
                    total_days_with_dialing[payslip.id] +
                    total_days_on_overtimes[payslip.id]
                )

                temp_work_days = 30 if payslip.period.start_date.month==2 and (
                    comms_subs_work_days[payslip.id] >= 28) else (
                    comms_subs_work_days[payslip.id])

                # Dictionary of codes used in the definition of rules
                names = {
                    # INFO Libraries and Objects
                    'np': np,
                    'pendulum': pendulum,
                    'pandas': pd,
                    'utils': utils,
                    'date': date,
                    'timedelta': timedelta,
                    'relativedelta': relativedelta,
                    'monthrange': monthrange,
                    'Decimal': Decimal,
                    'Pool': Pool,
                    'Payslip': Payslip,
                    # INFO Payslip
                    'payslip': payslip,
                    'contract': payslip.contract,
                    'employee': payslip.employee,
                    'initial_cut_date': payslip.start_date,
                    'final_cut_date': payslip.end_date,
                    'period_start_date': payslip.period.start_date,
                    'period_end_date': payslip.period.end_date,
                    'contract_start_date': payslip.contract.start_date,
                    'contract_end_date': payslip.contract.end_date,
                    # INFO Holidays
                    'holidays': holidays[payslip.id],
                    'holidays_dates': holidays_dates[payslip.id],
                    # INFO Salary'
                    'base_salary': Decimal(payslip.fiscalyear.base_salary),
                    'salary': salaries[payslip.id],
                    'hour_cost': hour_costs[payslip.id],
                    'min_salary_to_receive': min_salary_to_receive,
                    # INFO Employee
                    'employee_antiquity_years':
                        payslip.employee.years_antiguaty,
                    'employee_antiquity_date': payslip.employee.antiquity_date,
                    'employee_antiquity_date_year': (
                        payslip.employee.antiquity_date.year
                        if payslip.employee.antiquity_date else 0),
                    'contract_type': contract_type,
                    'work_relationship': work_relationship,
                    # INFO Worked days and absences
                    'period_days': period_days[payslip.id],
                    'worked_period_days': worked_period_days[payslip.id],
                    'worked_days': worked_days[payslip.id],
                    'comms_subs_work_days': temp_work_days,
                    'absences': total_absences[payslip.id],
                    'permissions': total_permissions[payslip.id],
                    'permissions_all': total_permissions_all[payslip.id],
                    'absences_with_normative':
                        total_absences_with_normative[payslip.id],
                    'worked_days_with_normative':
                        worked_days_with_normative[payslip.id],
                    'nocturne_surcharge_hours':
                        nocturne_surcharge_hours[payslip.id],
                    'total_days_with_dialing':
                        total_days_with_dialing[payslip.id],
                    'total_days_on_overtimes':
                        total_days_on_overtimes[payslip.id],
                    'effective_worked_days':
                        effective_worked_days,
                    'total_delay_hours':
                        total_delay_hours[payslip.id],
                    'total_non_dialings':
                        total_non_dialings[payslip.id],
                    'total_non_dialings_days':
                        total_non_dialings_days[payslip.id],
                    # INFO Medical certificates
                    'days_disease': total_days_disease[payslip.id],
                    'medical_certificates_info':
                        medical_certificates_info[payslip.id],
                    # INFO Family burdens
                    'total_burdens': num_family_burdens['total'],
                    'total_burdens_under_age': num_family_burdens['under_age'],
                    'total_burdens_legal_age': num_family_burdens['legal_age'],
                    'total_burdens_with_subsidy':
                        num_family_burdens['with_subsidy'],
                    'total_burdens_with_kindergarten':
                        num_family_burdens['with_kindergarten'],
                    'total_burdens_with_disability':
                        num_family_burdens['with_disability'],
                    'total_burdens_with_spouse_extension':
                        num_family_burdens['with_spouse_extension'],
                    'have_spouse_extension':
                        (num_family_burdens['with_spouse_extension'] > 0),
                    # INFO Law benefits
                    'law_benefit_types': law_benefit_types,
                    'fr_have': fr_info[payslip.id]['fr_have'],
                    'occupational_group': occupational_group,
                    'hierarchical_level': hierarchical_level,
                    'fr_first_time': fr_info[payslip.id]['fr_first_time'],
                    'fr_days_to_pay': fr_info[payslip.id]['fr_days_to_pay'],
                    'payment_form_fr': law_benefits_payment_form['fr'],
                    'payment_form_xiii': law_benefits_payment_form['xiii'],
                    'payment_form_xiv': law_benefits_payment_form['xiv'],
                    # INFO Tax income
                    'form107': form107,
                    'projected_incomes': projected_incomes,
                    'projected_expenses': projected_expenses,
                    'basic_fraction_annual': basic_fraction_annual,
                    'discount_third_age': discount_third_age,
                    'discount_disability': discount_disability,
                    'discount_disability_burden': discount_disability_burden,
                    # INFO Manual entries
                    'manual_entry_amounts':
                        manual_entry_amounts[payslip.id],
                    # INFO Fixed income deductions
                    'fixed_deduction_amounts':
                        fixed_deductions_amounts[payslip.id],
                    'fixed_income_amounts':
                        fixed_incomes_amounts[payslip.id],
                    # INFO Totals separated amounts by type
                    'total_comms_subs_by_type':
                        comms_subs_amounts_by_type[payslip.id],
                    'total_loans_by_type':
                        loans_amounts_by_type[payslip.id],
                    'total_overtimes_by_type':
                        overtimes_amounts_by_type[payslip.id],
                    # INFO Totals
                    'total_overtimes': overtimes_amounts[payslip.id],
                    'total_comms_subs': comms_subs_amounts[payslip.id],
                    'total_loans': loans_amounts[payslip.id],
                    'total_income': 0,
                    'total_deduction': 0,
                    'total_law_benefits': 0,
                    'total_by_rule': defaultdict(lambda: 0),
                }

                parser = evaluator.RuleParser(names)

                # Generate result
                result = Payslip.calculate_rules_no_accumulated_templates(
                    payslip, parser, names)

                # Create or update considered payslip overtime lines
                to_save_payslip_overtime_lines += (
                    cls.manage_payslip_overtime_lines(
                        payslip,
                        overtimes[payslip.id])['to_save'])

                # Create or update considered payslip comm subs details
                to_save_payslip_comm_subs_details += (
                    cls.manage_payslip_comm_subs_details(
                        payslip,
                        comms_subs[payslip.id])['to_save'])

                # Create or update considered payslip deduction loans
                to_save_payslip_deduction_loans += (
                    cls.manage_payslip_deduction_loans(
                        payslip,
                        deduction_loans[payslip.id])['to_save'])

                # Create or update considered payslip absences
                to_save_payslip_absences += (
                    cls.manage_payslip_absences(
                        payslip,
                        absences[payslip.id])['to_save'])

                # Create or update considered payslip permissions
                to_save_payslip_permissions += (
                    cls.manage_payslip_permissions(
                        payslip,
                        permissions[payslip.id])['to_save'])

                # Create or update considered payslip permissions_license
                to_save_payslip_permissions_license += (
                    cls.manage_payslip_permissions_license(
                        payslip,
                        permissions_license[payslip.id])['to_save'])

                # Create or update considered payslip manual entries
                considered_entries_codes = (
                    result['considered_entries_codes'] if result else [])
                to_save_payslip_manual_entries += (
                    cls.manage_payslip_manual_entries(
                        payslip,
                        income_entries[payslip.id],
                        deduction_entries[payslip.id],
                        considered_entries_codes)['to_save'])

                # Create payslip workshift dialing lines
                managed_workshift_dialings = (
                    cls.manage_payslip_workshift_dialings(
                        payslip, assistance_detail[payslip.id]))
                to_save_managed_workshift_dialings += (
                    managed_workshift_dialings['to_save'])
                to_delete_managed_workshift_dialings += (
                    managed_workshift_dialings['to_delete'])

                # Create payslip medical certificate discount lines
                managed_med_cert_disc = (
                    cls.manage_payslip_medical_certificate_discounts(
                        payslip, medical_certificates_info[payslip.id]))
                to_save_managed_med_cert_dis += (
                    managed_med_cert_disc['to_save'])
                to_delete_managed_med_cert_dis += (
                    managed_med_cert_disc['to_delete'])

                # Create or update considered payslip workshift delay lines
                to_save_payslip_workshift_delay_lines += (
                    cls.manage_payslip_workshift_delay_lines(
                        payslip,
                        workshift_delay_lines[payslip.id])['to_save'])

                if result:
                    to_save_lines += result['to_save_lines']
                    to_save_law_benefits += result['to_save_law_benefits']
                    to_delete_law_benefits += result['to_delete_law_benefits']

                    # Get partial totals
                    totals[payslip.id] = {
                        'total_income': result['total_income'],
                        'total_deduction': result['total_deduction'],
                        'total_law_benefits': result['total_law_benefits']
                    }

                    # Get final totals
                    income = totals[payslip.id]['total_income']
                    deduction = totals[payslip.id]['total_deduction']
                    income_lb = totals[payslip.id]['total_law_benefits']

                    # Update totals in payslip
                    payslip.total_income = utils.quantize_currency(
                        (income + income_lb))
                    payslip.total_deduction = utils.quantize_currency(deduction)
                    payslip.total_value = utils.quantize_currency(
                        (payslip.total_income - payslip.total_deduction))

                    # Update dict codes with totals
                    names['total_law_benefits'] = income_lb
                    names['total_income'] = payslip.total_income
                    names['total_deduction'] = payslip.total_deduction
                    names['total_value'] = payslip.total_value

                    payslip_dict_code = PayslipDictCodes()
                    payslip_dict_code.payslip = payslip
                    payslip_dict_code.description = str(
                        PayslipDictCodes.get_dict_codes_instances(
                            payslip, names))
                    to_save_dict_codes.append(payslip_dict_code)

                    to_delete_dict_codes += payslip.dict_codes

                    # Was the minimum wage to be received exceeded? or maybe,
                    # are the days of illness/accident greater than the days of
                    # the period?
                    if not get_permit_totals_negative():
                        payslip.min_salary_to_receive = (
                            min_salary_to_receive)
                        payslip.manual_revision = (payslip.total_value <
                            payslip.min_salary_to_receive)
                    if not payslip.template.accumulated_law_benefits:
                        if not payslip.manual_revision:
                            payslip.manual_revision = (payslip.period_days <
                                payslip.total_days_disease)

                    # Update payslip
                    to_update_payslips.append(payslip)

        # Save changes
        Payslip.save(to_update_payslips)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_law_benefits)
        LawBenefit.save(to_save_law_benefits)
        Line.delete(to_delete_lines)
        Line.save(to_save_lines)
        PayslipManualEntry.save(to_save_payslip_manual_entries)
        PayslipDeductionLoadLine.save(to_save_payslip_deduction_loans)
        PayslipFixedIncomeDeduction.delete(to_delete_payslip_fixed_values)
        PayslipFixedIncomeDeduction.save(to_save_payslip_fixed_values)
        PayslipOvertimeLine.save(to_save_payslip_overtime_lines)
        PayslipCommSubDetail.save(to_save_payslip_comm_subs_details)
        PayslipAbsence.save(to_save_payslip_absences)
        PayslipPermission.save(to_save_payslip_permissions)
        PayslipPermissionLicense.save(to_save_payslip_permissions_license)
        PayslipWorkshiftDelayLine.save(to_save_payslip_workshift_delay_lines)
        PayslipDictCodes.delete(to_delete_dict_codes)
        PayslipDictCodes.save(to_save_dict_codes)
        PayslipWorkshiftDialing.delete(to_delete_managed_workshift_dialings)
        PayslipWorkshiftDialing.save(to_save_managed_workshift_dialings)
        PayslipMedicalCertificateDiscount.delete(to_delete_managed_med_cert_dis)
        PayslipMedicalCertificateDiscount.save(to_save_managed_med_cert_dis)

    @classmethod
    def load_lines_accumulated_templates(cls, payslips, is_recalculate=False):
        # Get models
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        Line = pool.get('payslip.line')
        LawBenefit = pool.get('payslip.law.benefit')
        PayslipManualEntry = pool.get(
            'payslip.payslip.manual.entry')
        PayslipDictCodes = Pool().get('payslip.dict.codes')

        # Manual entries
        manual_entries = cls.get_all_entries(payslips,
            is_recalculate=is_recalculate)
        income_entries = manual_entries['income_entries']
        deduction_entries = manual_entries['deduction_entries']

        # Get all law benefits types
        law_benefit_types = cls.get_law_benefit_types()

        # Get holidays info
        info_holidays = cls.get_holidays(payslips)
        holidays = info_holidays['holidays']
        holidays_dates = info_holidays['holidays_dates']

        # Get amounts by mannual entry
        manual_entry_amounts = Payslip.get_manual_entries_amounts(payslips,
            income_entries, deduction_entries)

        to_save_payslip_manual_entries = []
        to_update_payslips = []
        to_save_lines = []
        to_save_law_benefits = []
        to_update_law_benefits = []
        to_delete_law_benefits = []
        to_delete_lines = []
        to_save_dict_codes = []
        to_delete_dict_codes = []

        totals = defaultdict(lambda: [])
        salaries = defaultdict(lambda: [])
        hour_costs = defaultdict(lambda: [])

        result_lb = cls.get_law_benefits_to_update(payslips)
        to_update_law_benefits += result_lb['to_update']
        to_delete_law_benefits += result_lb['to_delete']

        allowed_templates_for_attendace_detail = (
            get_allowed_payslip_templates_for_attendance_detail())

        for payslip in payslips:
            with Transaction().set_context(salary_date=payslip.period.end_date):
                # Get employee salaries and hour_costs
                info_salaries = cls.get_info_salary(payslip)
                salaries = info_salaries['salaries']
                hour_costs = info_salaries['hour_costs']

                # Set salary on payslip
                payslip.salary = salaries[payslip.id]

                # Totals definition
                totals[payslip.id] = {
                    'total_income': ZERO,
                    'total_deduction': ZERO,
                    'total_income_law_benefits': ZERO
                }

                # Delete previous lines
                to_delete_lines += payslip.lines

                # Get payment form XIII, XIV
                law_benefits_payment_form = (
                    cls.get_law_benefits_payment_form_by_payslip(payslip))

                # Update payment form XIII, XIV on payslip
                payslip.xiii = law_benefits_payment_form['xiii']
                payslip.xiv = law_benefits_payment_form['xiv']
                payslip.fr = law_benefits_payment_form['fr']

                # Get minimum of salary to receive for an employee
                min_salary_to_receive = (
                    payslip.get_min_salary_to_receive(salaries[payslip.id]))

                # Get contract type
                work_relationship = ''
                if payslip.contract.work_relationship:
                    work_relationship = payslip.contract.work_relationship.name

                # Get contract type
                contract_type = ''
                if payslip.contract.contract_type:
                    contract_type = payslip.contract.contract_type.name

                # Dictionary of codes used in the definition of rules
                names = {
                    # INFO Libraries and Objects
                    'np': np,
                    'pendulum': pendulum,
                    'pandas': pd,
                    'utils': utils,
                    'date': date,
                    'timedelta': timedelta,
                    'relativedelta': relativedelta,
                    'monthrange': monthrange,
                    'Decimal': Decimal,
                    'Pool': Pool,
                    'Payslip': Payslip,
                    # INFO Payslip
                    'payslip': payslip,
                    'contract': payslip.contract,
                    'employee': payslip.employee,
                    'initial_cut_date': payslip.start_date,
                    'final_cut_date': payslip.end_date,
                    'period_start_date': payslip.period.start_date,
                    'period_end_date': payslip.period.end_date,
                    'contract_start_date': payslip.contract.start_date,
                    'contract_end_date': payslip.contract.end_date,
                    # INFO Holidays
                    'holidays': holidays[payslip.id],
                    'holidays_dates': holidays_dates[payslip.id],
                    # INFO Salary'
                    'base_salary': Decimal(payslip.fiscalyear.base_salary),
                    'salary': salaries[payslip.id],
                    'hour_cost': hour_costs[payslip.id],
                    'min_salary_to_receive': min_salary_to_receive,
                    # INFO Employee
                    'contract_type': contract_type,
                    'work_relationship': work_relationship,
                    # INFO Law benefits
                    'law_benefit_types': law_benefit_types,
                    'payment_form_fr': law_benefits_payment_form['fr'],
                    'payment_form_xiii': law_benefits_payment_form['xiii'],
                    'payment_form_xiv': law_benefits_payment_form['xiv'],
                    # INFO Manual entries
                    'manual_entry_amounts':
                        manual_entry_amounts[payslip.id],
                    # INFO Totals
                    'total_income': 0,
                    'total_deduction': 0,
                    'total_law_benefits': 0,
                    'total_by_rule': defaultdict(lambda: 0),
                }

                parser = evaluator.RuleParser(names)

                # Generate result
                result = Payslip.calculate_rules_accumulated_templates(
                    payslip, parser, names)

                # Create or update considered payslip manual entries
                considered_entries_codes = (
                    result['considered_entries_codes'] if result else [])
                to_save_payslip_manual_entries += (
                    cls.manage_payslip_manual_entries(
                        payslip,
                        income_entries[payslip.id],
                        deduction_entries[payslip.id],
                        considered_entries_codes)['to_save'])

                if result:
                    to_save_lines += result['to_save_lines']
                    to_save_law_benefits += result['to_save_law_benefits']
                    to_delete_law_benefits += result['to_delete_law_benefits']

                    # Update attendance
                    payslip.set_assistance_detail_for_accumulated_templates(
                        result['to_save_law_benefits'],
                        allowed_templates=allowed_templates_for_attendace_detail
                    )

                    # Get partial totals
                    totals[payslip.id] = {
                        'total_income': result['total_income'],
                        'total_deduction': result['total_deduction'],
                        'total_law_benefits': result['total_law_benefits']
                    }

                    # Get final totals
                    income = totals[payslip.id]['total_income']
                    deduction = totals[payslip.id]['total_deduction']
                    income_lb = totals[payslip.id]['total_law_benefits']

                    # Update totals in payslip
                    payslip.total_income = utils.quantize_currency(
                        (income + income_lb))
                    payslip.total_deduction = utils.quantize_currency(deduction)
                    payslip.total_value = utils.quantize_currency(
                        (payslip.total_income - payslip.total_deduction))

                    # Update dict codes with totals
                    names['total_law_benefits'] = income_lb
                    names['total_income'] = payslip.total_income
                    names['total_deduction'] = payslip.total_deduction
                    names['total_value'] = payslip.total_value

                    payslip_dict_code = PayslipDictCodes()
                    payslip_dict_code.payslip = payslip
                    payslip_dict_code.description = str(
                        PayslipDictCodes.get_dict_codes_instances(
                            payslip, names))
                    to_save_dict_codes.append(payslip_dict_code)

                    to_delete_dict_codes += payslip.dict_codes

                    # Was the minimum wage to be received exceeded? or maybe,
                    # are the days of illness/accident greater than the days of
                    # the period?
                    if not get_permit_totals_negative():
                        payslip.min_salary_to_receive = (
                            min_salary_to_receive)
                        payslip.manual_revision = (payslip.total_value <
                            payslip.min_salary_to_receive)
                    if not payslip.template.accumulated_law_benefits:
                        if not payslip.manual_revision:
                            payslip.manual_revision = (payslip.period_days <
                                payslip.total_days_disease)

                    # Update payslip
                    to_update_payslips.append(payslip)

        # Save changes
        Payslip.save(to_update_payslips)
        LawBenefit.save(to_update_law_benefits)
        with Transaction().set_context(from_payslip=True):
            LawBenefit.delete(to_delete_law_benefits)
        LawBenefit.save(to_save_law_benefits)
        Line.delete(to_delete_lines)
        Line.save(to_save_lines)
        PayslipManualEntry.save(to_save_payslip_manual_entries)
        PayslipDictCodes.delete(to_delete_dict_codes)
        PayslipDictCodes.save(to_save_dict_codes)

    @classmethod
    def calculate_rules_no_accumulated_templates(cls, payslip, parser, names):
        considered_entries_codes = []
        considered_fixed_values_codes = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []

        for line in payslip.template.lines:
            rule = line.rule
            if not rule.active:
                continue

            parser.aeval.symtable['rule'] = rule
            condition = parser.eval_expr(rule.condition)
            if parser.error:
                cls.raise_user_error('condition_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not condition:
                if rule.is_law_benefit:
                    # XIII and XIV are calculated and saved on data base every
                    # month regardless of whether the employee monthlyizes or
                    # accumulates, except FR because it must meet the condition
                    # to be calculated
                    if rule.law_benefit_type.code in ['fr']:
                        # The reserve fund records that may have been created
                        # previously are deleted, then continue with the next
                        # iteration of the loop
                        to_delete_law_benefits += (
                            cls.get_law_benefits_to_delete(
                                payslip, rule.law_benefit_type))
                        continue
                else:
                    continue

            result = parser.eval_expr(rule.expression)
            if parser.error:
                cls.raise_user_error('expression_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not isinstance(result, list):
                result = [result]
            parser.aeval.symtable[rule.code] = 0

            # Get mannual entry codes used in every expression
            considered_entries_codes += (
                utils.manual_entries_in_rule_expression(rule.expression))
            considered_fixed_values_codes += (
                utils.fixed_values_in_rule_expression(rule.expression))

            for res in result:
                if not isinstance(res, dict):
                    cls.raise_user_error('invalid_result', {
                        'result': res,
                        'rule': rule.rec_name,
                        'employee': payslip.employee.rec_name,
                    })

                amount = 0
                create_new_payslip_line = True

                if not rule.is_law_benefit:
                    value = Decimal(res['value']) if res else ZERO
                    amount = utils.quantize_currency(value)
                    parser.aeval.symtable[rule.code] += amount
                    if amount >= 0:
                        if rule.type_ == 'income':
                            parser.aeval.symtable['total_income'] += amount
                        elif rule.type_ == 'deduction':
                            parser.aeval.symtable['total_deduction'] += amount
                else:
                    value = res['value'] if res else None
                    if value:
                        # The value could be a type of law benefit
                        if isinstance(value, PayslipLawBenefitType):
                            # Get law benefit type and its expression
                            law_benefit_type = value
                            expression = law_benefit_type.expression
                            # Get mannual entry codes used in the expression
                            considered_entries_codes += (utils.
                                manual_entries_in_rule_expression(expression))
                            considered_fixed_values_codes += (utils.
                                fixed_values_in_rule_expression(expression))
                            # Get amount
                            amount = parser.eval_expr(expression)
                            if parser.error:
                                cls.raise_user_error('expression_error', {
                                    'rule': rule.rec_name,
                                    'error_message': parser.error,
                                })
                            amount = utils.quantize_currency(amount)
                        # Else, the value could be a expression that return a
                        # number directly
                        else:
                            law_benefit_type = rule.law_benefit_type
                            amount = utils.quantize_currency(value)

                        # Delete previous law benefits created for this
                        # period, employee and type_
                        to_delete_law_benefits += (
                            cls.get_law_benefits_to_delete(
                                payslip, law_benefit_type))

                        # Calculation and creation of a new law benefit for
                        # this period, employee and type_
                        if amount >= 0:
                            benefit = (cls.get_new_law_benefit(
                                payslip, law_benefit_type, amount))

                            # RESERVE FUNDS
                            if law_benefit_type.code == 'fr':
                                benefit.payslip = payslip
                                benefit.payslip_date = payslip.period.end_date
                                if benefit.kind == 'monthlyse':
                                    parser.aeval.symtable[
                                        'total_law_benefits'] += amount

                            # XIII, XIV
                            else:
                                if benefit.kind == 'monthlyse':
                                    parser.aeval.symtable[
                                        'total_law_benefits'] += amount
                                    benefit.payslip = payslip
                                    benefit.payslip_date = (
                                        payslip.period.end_date)
                                elif benefit.kind == 'accumulate':
                                    # The accumulated XIII, XIV are calculated
                                    # monthly and saved in the database but not
                                    # shown in the current payslip.
                                    create_new_payslip_line = False
                            to_save_law_benefits.append(benefit)

                # Save amount of rule in dict_names
                if rule.code not in names['total_by_rule']:
                    names['total_by_rule'].update({rule.code: amount})
                else:
                    names['total_by_rule'][rule.code] += amount

                # Create line
                if create_new_payslip_line and amount > 0:
                    pline = Payslip.get_new_payslip_line(payslip, rule,
                        rule.type_, amount)
                    to_save_lines.append(pline)

        t_income = parser.aeval.symtable['total_income']
        t_deduction = parser.aeval.symtable['total_deduction']
        t_law_benefit = parser.aeval.symtable['total_law_benefits']

        # Delete duplicates
        considered_entries_codes = list(set(considered_entries_codes))
        considered_fixed_values_codes = list(set(considered_fixed_values_codes))

        return {
            'total_income': utils.quantize_currency(t_income),
            'total_deduction': utils.quantize_currency(t_deduction),
            'total_law_benefits': utils.quantize_currency(t_law_benefit),
            'to_save_lines': to_save_lines,
            'to_save_law_benefits': to_save_law_benefits,
            'to_delete_law_benefits': to_delete_law_benefits,
            'considered_entries_codes': considered_entries_codes,
            'considered_fixed_values_codes': considered_fixed_values_codes
        }

    @classmethod
    def calculate_rules_accumulated_templates(cls, payslip, parser, names):
        considered_entries_codes = []
        to_save_lines = []
        to_save_law_benefits = []
        to_delete_law_benefits = []

        for line in payslip.template.lines:
            rule = line.rule
            if not rule.active:
                continue

            parser.aeval.symtable['rule'] = rule
            condition = parser.eval_expr(rule.condition)
            if parser.error:
                cls.raise_user_error('condition_error', {
                    'error_message': parser.error,
                })
            if not condition:
                continue

            result = parser.eval_expr(rule.expression)
            if parser.error:
                cls.raise_user_error('expression_error', {
                    'rule': rule.rec_name,
                    'error_message': parser.error,
                })
            if not isinstance(result, list):
                result = [result]
            parser.aeval.symtable[rule.code] = 0

            # Get mannual entry codes used in every expression
            considered_entries_codes += (
                utils.manual_entries_in_rule_expression(rule.expression))

            for res in result:
                if not isinstance(res, dict):
                    cls.raise_user_error('invalid_result', {
                        'result': res,
                        'rule': rule.rec_name,
                        'employee': payslip.employee.rec_name,
                    })

                amount = 0
                create_new_payslip_line = True

                if not rule.is_law_benefit:
                    value = Decimal(res['value']) if res else ZERO
                    amount = utils.quantize_currency(value)
                    parser.aeval.symtable[rule.code] += amount
                    if amount >= 0:
                        if rule.type_ == 'income':
                            parser.aeval.symtable['total_income'] += amount
                        elif rule.type_ == 'deduction':
                            parser.aeval.symtable['total_deduction'] += amount
                else:
                    value = res['value'] if res else None
                    # It is possible to send in the key 'value' the type of law
                    # benefit that is being calculated when you want the value
                    # to be calculated internally in the system based on the sum
                    # of all the accumulated records and that the benefits of
                    # law are automatically managed what to consider in the role
                    if isinstance(value, PayslipLawBenefitType):
                        if value:
                            law_benefit_type = value
                            calculation_result = (
                                cls.calculate_accumulated_law_benefits(
                                    payslip, law_benefit_type))

                            accumulated_benefit = calculation_result['benefit']
                            if accumulated_benefit:
                                to_save_law_benefits += (
                                    calculation_result['to_save_law_benefits'])
                                amount = accumulated_benefit.amount
                                parser.aeval.symtable[
                                    'total_law_benefits'] += amount
                    else:
                        # It is also possible to send in the 'value' key the
                        # value calculated by the rule, in this case, the
                        # automatic management of the benefits of law considered
                        # in this role must be explicitly specified.
                        value = Decimal(str(value)) if value else ZERO
                        amount = utils.quantize_currency(value)
                        parser.aeval.symtable[rule.code] += amount
                        if amount >= 0:
                            if rule.type_ == 'income':
                                parser.aeval.symtable['total_income'] += amount
                            elif rule.type_ == 'deduction':
                                parser.aeval.symtable[
                                    'total_deduction'] += amount
                        # The explicit way of indicating that you want the
                        # system to manage the benefits of law considered in the
                        # role is done by sending the type of law benefit in the
                        # key 'manage_law_benefits'
                        if 'manage_law_benefits' in res:
                            law_benefit_type = res['manage_law_benefits']
                            if (isinstance(law_benefit_type,
                                    PayslipLawBenefitType)):
                                calculation_result = (
                                    cls.calculate_accumulated_law_benefits(
                                        payslip, law_benefit_type))
                                accumulated_benefit = calculation_result[
                                    'benefit']
                                if accumulated_benefit:
                                    to_save_law_benefits += (
                                        calculation_result[
                                            'to_save_law_benefits'])

                # Save amount of rule in dict_names
                if rule.code not in names['total_by_rule']:
                    names['total_by_rule'].update({rule.code: amount})
                else:
                    names['total_by_rule'][rule.code] += amount

                # Create line
                if create_new_payslip_line and amount > 0:
                    pline = Payslip.get_new_payslip_line(payslip, rule,
                        rule.type_, amount)
                    to_save_lines.append(pline)

        t_income = parser.aeval.symtable['total_income']
        t_deduction = parser.aeval.symtable['total_deduction']
        t_law_benefit = parser.aeval.symtable['total_law_benefits']

        # Delete duplicates
        considered_entries_codes = list(set(considered_entries_codes))

        return {
            'total_income': utils.quantize_currency(t_income),
            'total_deduction': utils.quantize_currency(t_deduction),
            'total_law_benefits': utils.quantize_currency(t_law_benefit),
            'to_save_lines': to_save_lines,
            'to_save_law_benefits': to_save_law_benefits,
            'to_delete_law_benefits': to_delete_law_benefits,
            'considered_entries_codes': considered_entries_codes,
        }

    @classmethod
    def get_law_benefits_to_update(cls, payslips):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        LawBenefit = pool.get('payslip.law.benefit')
        tbl_law_benefit = LawBenefit.__table__()
        tbl_payslip = cls.__table__()

        to_update = []
        to_delete = []

        for payslip in payslips:
            to_update_ids = []
            to_delete_ids = []

            query = tbl_payslip.join(tbl_law_benefit,
                condition=((tbl_payslip.id == tbl_law_benefit.payslip) |
                    (tbl_payslip.id == tbl_law_benefit.generator_payslip))
            ).select(
                tbl_law_benefit.id,
                tbl_law_benefit.payslip,
                tbl_law_benefit.generator_payslip,
                tbl_law_benefit.kind,
                where=((tbl_payslip.id == payslip.id) &
                       (tbl_law_benefit.is_paid_in_liquidation == False))
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                lb_id = row['id']
                lb_payslip = row['payslip']
                lb_generator_payslip = row['generator_payslip']
                if lb_generator_payslip == payslip.id:
                    to_delete_ids.append(lb_id)
                elif lb_payslip == payslip.id:
                    to_update_ids.append(lb_id)

            for lb in LawBenefit.browse(to_update_ids):
                lb.payslip = None
                to_update.append(lb)

            to_delete += list(LawBenefit.browse(to_delete_ids))

        return {
            'to_update': to_update,
            'to_delete': to_delete
        }

    def set_assistance_detail_for_accumulated_templates(self, law_benefits,
            allowed_templates=[]):
        allowed_templates = allowed_templates if allowed_templates else []
        worked_days = 0
        total_absences = 0
        total_absences_with_normative = 0
        total_permissions = 0
        worked_days_with_normative = 0
        total_days_disease = 0
        for lb in law_benefits:
            payslip = lb.generator_payslip
            if payslip:
                # Payslip templates of accumulated benefits, biweekly roles and
                # some adjustment do not add to the attendance detail
                if (payslip.template.code not in allowed_templates):
                    continue
                if payslip.worked_days:
                    worked_days += payslip.worked_days
                if payslip.total_absences:
                    total_absences += payslip.total_absences
                if payslip.total_absences_with_normative:
                    total_absences_with_normative += (
                        payslip.total_absences_with_normative)
                if payslip.total_permissions:
                    total_permissions += payslip.total_permissions
                if payslip.worked_days_with_normative:
                    worked_days_with_normative += (
                        payslip.worked_days_with_normative)
                if payslip.total_days_disease:
                    total_days_disease += (
                        payslip.total_days_disease)
        self.worked_days = worked_days
        self.total_absences = total_absences
        self.total_absences_with_normative = total_absences_with_normative
        self.total_permissions = total_permissions
        self.worked_days_with_normative = worked_days_with_normative
        self.total_days_disease = total_days_disease

    @classmethod
    def get_days(cls, payslips, names=None):
        '''
        period_days: Total days of the payslip period (for example, in January
            with fixed periods of 30 days: period_days = 30, in January without
            fixed periods: period_days = 31)
        worked_period_days: Total days between the initial cut date and the
            final cut date of the payslip and considering start and end date of
            the employee contract, disbled and enabled contract dates and
            permissions with unpaid licences.
        '''
        period_days_info = get_default_period_days_info()
        result_pd = defaultdict(lambda: 0)
        result_wpd = defaultdict(lambda: 0)
        if payslips:
            disabled_contracts_data = (
                cls.get_disabled_contracts_data(payslips))
            contracts_with_unpaid_license_data = (
                cls.get_contracts_with_unpaid_license_data(payslips))
            for payslip in payslips:
                if payslip.start_date and payslip.end_date:
                    # Period days information
                    add_days = 1
                    period_days = 0
                    if period_days_info['type'] == 'fixed':
                        period_days = period_days_info['value']
                    else:
                        period_days = np.busday_count(
                            payslip.period.start_date,
                            (payslip.period.end_date + timedelta(days=1)),
                            weekmask='1111111')

                    # Worked days on current period considering start and
                    # end date of the employee contract
                    worked_period_days = period_days
                    start_date, end_date = None, None
                    if payslip.contract:
                        start_date = payslip.contract.start_date
                        if not start_date:
                            start_date = datetime.min.date()
                        if start_date < payslip.period.start_date:
                            start_date = payslip.period.start_date

                        end_date = payslip.contract.end_date
                        if not end_date:
                            end_date = datetime.max.date()
                        if end_date >= payslip.period.end_date:
                            if period_days_info['type'] == 'fixed':
                                last_day = monthrange(
                                    payslip.period.end_date.year,
                                    payslip.period.end_date.month)[1]
                                end_date = (datetime(
                                    payslip.period.end_date.year,
                                    payslip.period.end_date.month,
                                    last_day
                                )).date()
                                add_days += period_days_info['value'] - last_day
                            else:
                                end_date = payslip.period.end_date

                    if start_date and end_date:
                        worked_period_days = np.busday_count(
                            start_date,
                            (end_date + timedelta(days=add_days)),
                            weekmask='1111111')
                        if period_days_info['type'] == 'fixed':
                            d = datetime.combine(start_date,
                                    datetime.min.time())
                            if d.month == 2:
                                worked_period_days = (
                                    30 - (30 - worked_period_days))
                            if worked_period_days >= 30:
                                worked_period_days = period_days

                        # The days worked in the period are obtained considering
                        # the days in which the employee was on unpaid license
                        unpaid_dates = []
                        contract_id = payslip.contract.id
                        unpaid_dates += disabled_contracts_data[contract_id]
                        unpaid_dates += (
                            contracts_with_unpaid_license_data[contract_id])
                        if unpaid_dates:
                            worked_period_days = (cls.
                                get_worked_days_contract_with_unpaid_licenses(
                                    start_date,
                                    end_date,
                                    worked_period_days,
                                    unpaid_dates))
                    else:
                        worked_period_days = 0

                    # Results
                    result_pd[payslip.id] = Decimal(str(period_days))
                    result_wpd[payslip.id] = Decimal(str(worked_period_days))

        return {
            'period_days': result_pd,
            'worked_period_days': result_wpd,
        }

    @classmethod
    def get_worked_days_info(cls, payslips, worked_period_days,
            absences, absences_with_normative, permissions, days_disease):
        period_days_info = get_default_period_days_info()
        res_worked_days = defaultdict(lambda: 0)
        res_worked_days_with_normative = defaultdict(lambda: 0)

        for payslip in payslips:
            wd, wd_normative = 0, 0
            _is_disease_all_period = cls.is_disease_all_period(
                payslip,
                days_disease[payslip.id],
                period_days_info)

            if not _is_disease_all_period:
                wd = (
                    worked_period_days[payslip.id] -
                    absences[payslip.id] -
                    permissions[payslip.id] -
                    days_disease[payslip.id])

                wd_normative = (
                    worked_period_days[payslip.id] -
                    absences_with_normative[payslip.id] -
                    permissions[payslip.id] -
                    days_disease[payslip.id])

                wd = wd if wd >= 0 else 0
                wd_normative = wd_normative if wd_normative >= 0 else 0

            res_worked_days[payslip.id] = wd
            res_worked_days_with_normative[payslip.id] = wd_normative

        return {
            'worked_days': res_worked_days,
            'worked_days_with_normative': res_worked_days_with_normative,
        }

    @classmethod
    def is_disease_all_period(cls, payslip, days_disease, period_days_info):
        period_days_value = period_days_info['value']
        period_days_type = period_days_info['type']

        period = payslip.period
        _start_date = period.payslip_start_date
        p_start_date = _start_date if _start_date else period.start_date

        # The cut-off periods involving February are less than 30 days, so if
        # employee get sick the whole period (28 or 29 days) it means that
        # employee worked 0 days
        period_days = monthrange(p_start_date.year, p_start_date.month)[1]
        if p_start_date.month != 2:
            if period_days_type == 'fixed':
                period_days = period_days_value

        if days_disease >= period_days:
            return True
        return False

    @classmethod
    def get_worked_days_contract_with_unpaid_licenses(cls, start_date, end_date,
            worked_period_days, unpaid_dates):
        for d in unpaid_dates:
            unp_start_date, unp_end_date = d[0], d[1]
            sd, ed = None, None
            if start_date <= unp_start_date <= unp_end_date <= end_date:
                sd = unp_start_date
                ed = unp_end_date
            elif start_date <= unp_start_date <= end_date <= unp_end_date:
                sd = unp_start_date
                ed = end_date
            elif unp_start_date <= start_date <= unp_end_date <= end_date:
                sd = start_date
                ed = unp_end_date
            elif unp_start_date <= start_date <= end_date <= unp_end_date:
                sd = start_date
                ed = end_date

            if sd and ed:
                diff = np.busday_count(sd, ed, weekmask='1111111')
                worked_period_days -= (diff + 1)
                if worked_period_days < 0:
                    worked_period_days = 0
        return worked_period_days

    @classmethod
    def get_contracts_with_unpaid_license_data(cls, payslips):
        Permission = Pool().get('hr_ec.permission')
        contract_ids = [p.contract.id for p in payslips]
        data = defaultdict(lambda: [])

        readed_fields = Permission.search_read([
            ('contract', 'in', contract_ids),
            ('discount_to', '=', 'license'),
            ('state', '=', 'done')
        ], fields_names=['contract', 'start_date', 'end_date'])

        if readed_fields:
            unpaid_dates = []
            for rf in readed_fields:
                contract_id = rf['contract']
                perm_start_date = rf['start_date']
                perm_end_date = rf['end_date']
                unpaid_dates.append({
                    'contract': contract_id,
                    'start_date': perm_start_date,
                    'end_date': perm_end_date
                })
            data = cls.get_unpaid_dates_data(payslips, unpaid_dates)
        return data

    @classmethod
    def get_disabled_contracts_data(cls, payslips):
        DisableContract = Pool().get('company.disable.contract')
        contract_ids = [p.contract.id for p in payslips]
        data = defaultdict(lambda: [])

        readed_fields = DisableContract.search_read([
            ('contract', 'in', contract_ids),
            ('state', 'in', ['done', 'enable_contract']),
        ], fields_names=['contract', 'disable_date', 'enable_date'])

        if readed_fields:
            unpaid_dates = []
            for rf in readed_fields:
                contract_id = rf['contract']
                disable_start_date = rf['disable_date']
                enable_date = rf['enable_date']

                if enable_date:
                    # If the contract has an activation date, it means that it
                    # was inactive until the day before the activation date
                    disable_end_date = enable_date - timedelta(days=1)
                else:
                    disable_end_date = datetime.max.date()
                    
                unpaid_dates.append({
                    'contract': contract_id,
                    'start_date': disable_start_date,
                    'end_date': disable_end_date
                })
            data = cls.get_unpaid_dates_data(payslips, unpaid_dates)
        return data

    @classmethod
    def get_unpaid_dates_data(cls, payslips, unpaid_dates):
        data = defaultdict(lambda: [])
        for unpaid_date in unpaid_dates:
            contract_id = unpaid_date['contract']
            up_start_date = unpaid_date['start_date']
            up_end_date = unpaid_date['end_date']
            
            for payslip in payslips:
                if payslip.contract.id == contract_id:
                    ps_date = payslip.period.start_date
                    pe_date = payslip.period.end_date

                    is_unpaid = False
                    if ps_date <= up_start_date <= up_end_date <= pe_date:
                        is_unpaid = True
                    elif ps_date <= up_start_date <= pe_date <= up_end_date:
                        is_unpaid = True
                    elif up_start_date <= ps_date <= up_end_date <= pe_date:
                        is_unpaid = True
                    elif up_start_date <= ps_date <= pe_date <= up_end_date:
                        is_unpaid = True

                    if is_unpaid:
                        if contract_id not in data:
                            data.update({
                                contract_id: [(up_start_date, up_end_date)]
                            })
                        else:
                            data[contract_id].append(
                                (up_start_date, up_end_date))
        return data

    @classmethod
    def get_absences(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Absence = pool.get('hr_ec.absence')
        Permission = pool.get('hr_ec.permission')
        Contract = pool.get('company.contract')

        absence = Absence.__table__()
        permission = Permission.__table__()
        contract = Contract.__table__()
        table = cls.__table__()

        total_absences = defaultdict(lambda: 0)
        total_absences_with_normative = defaultdict(lambda: 0)
        # TODO Rename 'total_permissions' to 'total_payslip_permissions'
        total_permissions = defaultdict(lambda: 0)
        total_permissions_all = defaultdict(lambda: 0)

        # Absences that overlaps with medical certificates
        query_absences_overlaps_cerificates = (
            cls.get_query_absences_overlaps_with_medical_certificates())

        # Contracts without discount for absences
        query_contracts_without_absences_apply = (
            cls.get_query_contracts_without_absences_apply())

        # Permissions that overlaps with medical certificates
        query_permissions_overlaps_cerificates = (
            cls.get_query_permissions_overlaps_with_medical_certificates())

        # Get totals
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)

            # Absences
            query_absence = table.join(absence,
                    condition=table.employee == absence.employee
                ).select(
                    table.id,
                    Sum(absence.total_in_days).as_(
                        'total_absences'),
                    Sum(absence.total_with_normative).as_(
                        'total_absences_with_normative'),
                    where=(red_sql
                           & ~(absence.id.in_(
                                query_absences_overlaps_cerificates))
                           & ~(table.contract.in_(
                                query_contracts_without_absences_apply))
                           # TODO Adapt range verification to the way it was
                           # TODO done with medical certificates
                           & (absence.end_date >= table.start_date)
                           & (absence.end_date <= table.end_date)
                           & (absence.state == 'done')
                           & (absence.is_paid_in_liquidation == False)),
                    group_by=[table.id]
            )
            cursor.execute(*query_absence)
            for row in cursor_dict(cursor):
                total_absences[row['id']] = (
                    row['total_absences'])
                total_absences_with_normative[row['id']] = (
                    row['total_absences_with_normative'])

            # Permissions with discount to payslip
            query_permission = table.join(permission,
                condition=table.contract == permission.contract
            ).join(contract,
                condition=permission.contract == contract.id
            ).select(
                table.id,
                Sum(Case((permission.discount_to == 'payslip',
                          permission.duration_without_weekend),
                         else_=permission.duration_with_weekend)).as_(
                    'total_permissions'),
                where=(red_sql
                       & ~(permission.id.in_(
                            query_permissions_overlaps_cerificates))
                       & (contract.absence_apply == True)
                       & (permission.end_date >= table.start_date)
                       & (permission.end_date <= table.end_date)
                       & (permission.state == 'done')
                       & (permission.is_paid_in_liquidation == False)
                       & (permission.discount_to.in_(['payslip']))),
                group_by=[table.id]
            )
            cursor.execute(*query_permission)
            for row in cursor_dict(cursor):
                total_permissions[row['id']] = row['total_permissions']

            # All Permissions of all types
            # Note: A similar implementation of this query maybe be used on
            # 'get_permissions_all' function in some metaclass=PoolMeta on
            # another model (if you change this query, make sure it matches
            # with the query of the metaclass=PoolMeta)
            query_permission = table.join(permission,
                condition=table.contract == permission.contract
            ).join(contract,
                condition=permission.contract == contract.id
            ).select(
                table.id,
                Sum(permission.duration_without_weekend).as_(
                    'total_permissions_all'),
                where=(red_sql
                       & ~(permission.id.in_(
                            query_permissions_overlaps_cerificates))
                       & (contract.absence_apply == True)
                       & (permission.end_date >= table.start_date)
                       & (permission.end_date <= table.end_date)
                       & (permission.is_paid_in_liquidation == False)
                       & (permission.state == "done")),
                group_by=[table.id]
            )
            cursor.execute(*query_permission)
            for row in cursor_dict(cursor):
                total_permissions_all[row['id']] = row['total_permissions_all']

        return {
            'total_absences': total_absences,
            'total_absences_with_normative': total_absences_with_normative,
            'total_permissions': total_permissions,
            'total_permissions_all': total_permissions_all
        }

    @classmethod
    def get_total_days_with_dialing(cls, payslips, assistance_detail=None,
            names=None):
        # The total number of days with marking refers to those days that are
        # part of a work shift and in which the employee has registered their
        # entry marking
        result = defaultdict(lambda: 0)
        if not assistance_detail:
            assistance_detail = cls.get_assistance_detail(payslips)

        if assistance_detail:
            for payslip_id, dialings in assistance_detail.items():
                result[payslip_id] = len(dialings)
        return {
            'total_days_with_dialing': result
        }

    @classmethod
    def get_total_days_on_overtimes(cls, payslips, assistance_detail=None,
            overtimes=None, names=None):
        # The days worked in overtime are obtained from the days that are not
        # part of an employee's workshift and in which the employee showed up
        # for work
        pool = Pool()
        OvertimeLine = pool.get('hr_ec.overtime.line')
        result = defaultdict(lambda: 0)
        midnight = time(0, 0, 0)
        if overtimes:
            for payslip in payslips:
                overtime_lines = []
                used_overtime_dates = []
                used_attendance_dates = []
                used_overtimes_same_workshift = []
                for o in OvertimeLine.browse(overtimes[payslip.id]):
                    overtime_lines.append(o)
                if overtime_lines:
                    if assistance_detail:
                        for a in assistance_detail[payslip.id]:
                            used_attendance_dates.append(a.date)
                    overtime_lines = sorted(overtime_lines, key=(attrgetter(
                        'overtime_date', 'start_time', 'end_time')))
                    for ol in overtime_lines:
                        if ol.state == 'valid' and ol.overtime.state == 'done':
                            ol_date = ol.overtime_date
                            start_time = ol.start_time
                            end_time = ol.end_time
                            # Se evalua que la fecha no haya sido considerada
                            # previamente en los registros de asistencia dentro
                            # del turno de trabajo, ni en las horas que ya han
                            # sido evaluadas por este algoritmo, ni en las horas
                            # que forman parte de una misma jornada (unidas por
                            # la media noche)
                            if ol_date in used_attendance_dates:
                                continue
                            elif ol_date in used_overtime_dates:
                                continue
                            elif ol.id in used_overtimes_same_workshift:
                                continue
                            else:
                                # Se evalua si la hora está justo en la
                                # fecha de corte del rol y que el tiempo inicio
                                # sea igual a la media noche; de ser el caso,
                                # se busca una hora extra de la fecha anterior
                                # al corte del rol con tiempo fin igual a media
                                # noche, de existir, la fecha actual de
                                # evaluación ya no se considera
                                if ol_date == payslip.start_date:
                                    if ol.start_time == midnight:
                                        yesterday = (
                                            ol_date - timedelta(days=1))
                                        o_lines = OvertimeLine.search([
                                            ('overtime.employee', '=',
                                                payslip.employee),
                                            ('overtime.state', '=', 'done'),
                                            ('overtime_date', '=', yesterday),
                                            ('end_time', '=', midnight),
                                            ('state', '=', 'valid'),
                                        ])
                                        if o_lines:
                                            (used_overtimes_same_workshift.
                                                append(ol_date))
                                            continue

                                # Si el caso anterior no se cumple, se procede
                                # a verificar las medias noches
                                if (start_time != midnight and
                                        end_time != midnight):
                                    used_overtime_dates.append(ol_date)
                                else:
                                    overtimes_same_workshift = []
                                    if (start_time == midnight or
                                            end_time == midnight):
                                        # Se obtienen todas las horas extra
                                        # que pueden ser una sola jornada con
                                        # la hora extra que se esta evaluando
                                        # (unidas por la media noche)
                                        overtimes_same_workshift = (cls.
                                            get_overtimes_same_workshift(
                                                ol, overtime_lines))

                                    if overtimes_same_workshift:
                                        # Se evalua que las fechas de las
                                        # horas extra del mismo turno no hayan
                                        # sido ya considerados, de ser asi, se
                                        # considera la fecha de la hora extra
                                        # que se esta evaluando
                                        for osw in overtimes_same_workshift:
                                            (used_overtimes_same_workshift.
                                                append(osw.id))
                                            osw_date = osw.overtime_date
                                            if osw_date in used_overtime_dates:
                                                continue
                                            else:
                                                used_overtime_dates.append(
                                                    ol_date)
                                    else:
                                        used_overtime_dates.append(ol_date)
                total = len(list(set(used_overtime_dates)))
                result[payslip.id] = total
        return {
            'total_days_on_overtimes': result
        }

    @classmethod
    def is_overtime_inside_workshift(cls, overtime_line):
        ol = overtime_line
        work_day = get_day(ol.overtime_date.strftime('%A'))
        if work_day and ol.workshift:
            workingdays = ol.workshift.workingdays
            workshift_days = [wd.day for wd in workingdays]
            if work_day not in workshift_days:
                return False
        return True

    @classmethod
    def get_overtimes_same_workshift(cls, overtime_line,
            overtime_lines_to_evaluate):
        result = []
        midnight = time(0, 0, 0)
        ol = overtime_line
        for ol_aux in overtime_lines_to_evaluate:
            start_date = None
            end_date = None
            if ol.id != ol_aux.id:
                if (ol.start_time == midnight and
                        ol_aux.end_time == midnight and
                        ol_aux.overtime_date < ol.overtime_date):
                    start_date = ol_aux.overtime_date
                    end_date = ol.overtime_date
                elif (ol.end_time == midnight and
                        ol_aux.start_time == midnight and
                        ol_aux.overtime_date > ol.overtime_date):
                    start_date = ol.overtime_date
                    end_date = ol_aux.overtime_date
                if start_date and end_date:
                    diff_dates = np.busday_count(
                        start_date, end_date, weekmask='1111111', holidays=[])
                    if diff_dates == 1:
                        result.append(ol_aux)
        return result

    @classmethod
    def get_nocturne_surcharge_hours(cls, payslips, names=None):
        # The nocturne surcharge is obtained only from those days that are part
        # of a work shift and in which the employee has registered his entry
        # marking
        result = defaultdict(lambda: 0)
        assistance_detail = cls.get_surcharge_nocturne(payslips)

        if assistance_detail:
            for payslip_id, dialings in assistance_detail.items():
                hours = ZERO
                for dialing in dialings:
                    t = dialing.surcharge_nocturne
                    h = t.hour + (t.minute / 60) + (t.second / 3600)
                    if h > 0:
                        hours += Decimal(str(h)).quantize(
                            CENT, rounding=ROUND_HALF_UP)
                result[payslip_id] = hours

        return {
            'nocturne_surcharge_hours': result
        }

    @classmethod
    def get_surcharge_nocturne(cls, payslips):
        pool = Pool()
        SurchargeNocturne = pool.get('company.surcharge.nocturne')
        result = defaultdict(lambda: [])
        for payslip in payslips:
            start_date = payslip.period.payslip_overtime_start_date
            end_date = payslip.period.payslip_overtime_end_date
            with Transaction().set_context(
                    start_date=start_date,
                    end_date=end_date,
                    employee=payslip.employee.id,
                    company=payslip.company.id,
                    surcharge_nocturne=True):

                contract = payslip.contract
                if contract.start_date and contract.start_date > start_date:
                    start_date = contract.start_date
                if contract.end_date and contract.end_date < end_date:
                    end_date = contract.end_date

                items = SurchargeNocturne.search([
                    ('employee', '=', payslip.employee),
                    ('date', '>=', start_date),
                    ('date', '<=', end_date),
                ])
                if items:
                    result[payslip.id] = [item for item in items]
        return result

    @classmethod
    def get_assistance_detail(cls, payslips):
        pool = Pool()
        AssistanceConsultationDetail = pool.get(
            'company.assistance.consultation.detail')
        result = defaultdict(lambda: [])
        for payslip in payslips:
            start_date = payslip.period.payslip_overtime_start_date
            end_date = payslip.period.payslip_overtime_end_date
            with Transaction().set_context(
                    start_date=start_date,
                    end_date=end_date,
                    employee=payslip.employee.id,
                    company=payslip.company.id):

                # If a contract ends before the cut-off date, or begins after
                # the cut-off date, the markings are considered in the start or
                # end range of the contract dates.
                contract = payslip.contract
                if contract.start_date and contract.start_date > start_date:
                    start_date = contract.start_date
                if contract.end_date and contract.end_date < end_date:
                    end_date = contract.end_date

                items = AssistanceConsultationDetail.search([
                    ('company', '=', payslip.company),
                    ('employee', '=', payslip.employee),
                    ('date', '>=', start_date),
                    ('date', '<=', end_date),
                    ('workshift', '!=', None)
                ])
                if items:
                    result[payslip.id] = [item for item in items]
        return result

    @classmethod
    def get_delay_lines_detail(cls, payslips):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        WorkshiftDelayConsultingLines = pool.get(
            'workshift.delay.consulting.lines')

        result_delay_lines = defaultdict(lambda: [])
        result_delay_hours = defaultdict(lambda: ZERO)

        for payslip in payslips:
            start_date = payslip.period.payslip_overtime_start_date
            end_date = payslip.period.payslip_overtime_end_date
            contract = payslip.contract

            # If an employee is not required to register dialing, then their
            # delays are not considered
            if not contract.obligatory_dialing_register:
                continue

            # If a contract ends before the cut-off date, or begins after
            # the cut-off date, the delays are considered in the start or
            # end range of the contract dates.
            if contract.start_date and contract.start_date > start_date:
                start_date = contract.start_date
            if contract.end_date and contract.end_date < end_date:
                end_date = contract.end_date

            with Transaction().set_context(
                    company=payslip.company.id,
                    employee=payslip.employee.id,
                    start_date=start_date,
                    end_date=end_date,
                    filter_delays='no_justified',
                    department=None):

                query = WorkshiftDelayConsultingLines.table_query()
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    delay_line_id = row['workshift_delay_line']
                    delay_hours = row['delay_hours']
                    result_delay_lines[payslip.id].append(delay_line_id)
                    result_delay_hours[payslip.id] += delay_hours
        return {
            'workshift_delay_lines': result_delay_lines,
            'total_delay_hours': result_delay_hours
        }

    @classmethod
    def get_non_dialings_detail(cls, payslips):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        WorkshiftNonDialingConsultingTotalized = pool.get(
            'workshift.non_dialing.consulting.totalized')

        result_non_dialings_day = defaultdict(lambda: ZERO)
        result_non_dialings = defaultdict(lambda: ZERO)

        for payslip in payslips:
            start_date = payslip.period.payslip_overtime_start_date
            end_date = payslip.period.payslip_overtime_end_date
            contract = payslip.contract

            # If an employee is not required to register dialing, then their
            # non-dialings are not considered
            if not contract.obligatory_dialing_register:
                continue

            # If a contract ends before the cut-off date, or begins after
            # the cut-off date, the non-dialings are considered in the start or
            # end range of the contract dates.
            if contract.start_date and contract.start_date > start_date:
                start_date = contract.start_date
            if contract.end_date and contract.end_date < end_date:
                end_date = contract.end_date

            with Transaction().set_context(
                    company=payslip.company.id,
                    employee=payslip.employee.id,
                    start_date=start_date,
                    end_date=end_date):

                query = WorkshiftNonDialingConsultingTotalized.table_query()
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    total_by_day = row['total_by_day']
                    total = row['total']
                    result_non_dialings_day[payslip.id] += total_by_day
                    result_non_dialings[payslip.id] += total
        return {
            'total_non_dialings_days': result_non_dialings_day,
            'total_non_dialings': result_non_dialings
        }

    @classmethod
    def default_negative_payslip(self):
        SeveralConfiguration = Pool().get('payslip.several.configurations')
        several_configuration = SeveralConfiguration(1)

        return several_configuration.totals_negative

    @classmethod
    def get_negative_payslip(cls, lines, names):
        SeveralConfiguration = Pool().get('payslip.several.configurations')
        several_configuration = SeveralConfiguration(1)

        result = defaultdict(lambda: None)
        if several_configuration:
            for line in lines:
                result[line.id] = several_configuration.totals_negative

        return {'negative_payslip': result}

    def get_previous_information(self):
        # Get previous data of a same period. This method is used for
        # calculate law benefits on biweekly payslips.
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        table = Payslip.__table__()
        previous_total_income = 0
        previous_worked_days = 0
        query = table.select(
            Sum(table.total_income).as_('total_income'),
            Sum((table.end_date - table.start_date) + 1).as_('worked_days'),
            where=((table.period == self.period.id)
                   & (table.end_date < self.start_date)
                   & (table.employee == self.employee.id)),
            group_by=[
                table.employee
            ]
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            if row['total_income']:
                previous_total_income = row['total_income']
            if row['worked_days']:
                previous_worked_days = row['worked_days']

        # Get absences
        payslips = Payslip.search([
            ('employee', '=', self.employee),
            ('period', '=', self.period),
            ('start_date', '<', self.start_date)
        ])
        if payslips:
            absences = self.get_absences(payslips)
            for payslip_id, absence in absences.items():
                previous_worked_days -= absence

        return {
            'previous_total_income': previous_total_income,
            'previous_worked_days': previous_worked_days
        }

    @classmethod
    def get_number_family_burdens(cls, employee, under_legal_age_limit=18):
        result = {
            'total': 0,
            'under_age': 0,
            'legal_age': 0,
            'with_subsidy': 0,
            'with_kindergarten': 0,
            'with_disability': 0,
            'with_spouse_extension': 0,
        }
        if employee and employee.burdens:
            result['total'] = len(employee.burdens)
            for burden in employee.burdens:
                diff = pendulum.period(
                    burden.birthdate, datetime.today().date())
                if diff.years < under_legal_age_limit:
                    result['under_age'] += 1
                else:
                    result['legal_age'] += 1
                if burden.subsidy:
                    result['with_subsidy'] += 1
                if burden.kindergarten:
                    result['with_kindergarten'] += 1
                if burden.disability:
                    result['with_disability'] += 1
                if burden.spouse_extension:
                    result['with_spouse_extension'] += 1
        return result

    def get_min_salary_to_receive(self, salary):
        min_value = 0
        if not self.template.accumulated_law_benefits:
            if self.worked_period_days > 0:
                min_value = (salary * (
                    self.worked_days_with_normative /
                        self.worked_period_days)) * (
                            self.contract.min_salary_rate_to_receive / 100)
                min_value = utils.quantize_currency(min_value)
        return min_value

    def get_discount_on_tax_base_for_third_age(self, sri_conf):
        if (sri_conf and sri_conf.third_age_years_from and
                sri_conf.real_discount_third_age):
            if self.employee.age >= sri_conf.third_age_years_from:
                return sri_conf.real_discount_third_age
        return 0

    def get_discount_on_tax_base_for_disability(self, sri_conf):
        if sri_conf and sri_conf.discount_disabilities:
            if self.employee and self.employee.disabilities:
                # Get max disability percentage of employee
                max_percentage = 0
                for disability in self.employee.disabilities:
                    if disability.percentage > max_percentage:
                        max_percentage = disability.percentage

                # Get ranges of disability configurations
                ranges = sri_conf.discount_disabilities

                # Get disability discount
                for range in ranges:
                    init = range.initial_disability_percentage
                    final = range.final_disability_percentage
                    if init <= max_percentage <= final:
                        if range.real_discount:
                            return range.real_discount
                        else:
                            return 0
        return 0

    def get_discount_on_tax_base_for_disability_burden(self, sri_conf):
        if sri_conf and sri_conf.discount_disabilities:
            if self.employee and self.employee.burdens:
                # Get max disability percentage of burdens
                max_percentage = 0
                for burden in self.employee.burdens:
                    if burden.disability:
                        for disability in burden.disabilities:
                            if disability.percentage > max_percentage:
                                max_percentage = disability.percentage

                # Get ranges of disability configurations
                ranges = sri_conf.discount_disabilities

                # Get disability discount
                for range in ranges:
                    init = range.initial_disability_percentage
                    final = range.final_disability_percentage
                    if init <= max_percentage <= final:
                        if range.real_discount:
                            return range.real_discount
                        else:
                            return 0
        return 0

    @classmethod
    def get_sri_conf_income_tax(cls, payslips):
        pool = Pool()
        PersonalExpenseProjection = pool.get('sri.personal.expense.projection')
        SriConfig = pool.get('sri.configuration')
        result = defaultdict(lambda: [])
        for payslip in payslips:
            basic_fraction = 1000000
            discount_third_age = 0
            discount_disability = 0
            discount_disability_burden = 0
            peps = PersonalExpenseProjection.search([
                ('employee', '=', payslip.employee.id),
                ('company', '=', payslip.company.id),
                ('fiscalyear', '=', payslip.fiscalyear.id),
                ('state', 'in', ['confirmed', 'finalized'])
            ], order=[('form_date', 'DESC')])

            # Get SRI Configurations for income tax
            pep = peps[0] if peps else None
            eval_date = (
                pep.form_date if pep else payslip.period.end_date)
            sri_configs = SriConfig.search([
                ('start_date', '<=', eval_date),
                ('end_date', '>=', eval_date),
            ], order=[('start_date', 'DESC'), ('end_date', 'DESC')])

            if sri_configs:
                sri_config = sri_configs[0]
                try:
                    # Basic Fraction that enable the Income Taxes
                    # calculation
                    if not sri_config.basic_fraction_taxed:
                        cls.raise_user_error(
                            'sri_conf_basic_fraction_error')
                    basic_fraction = sri_config.basic_fraction_taxed

                    # Discount for third age
                    discount_third_age = (
                        payslip.get_discount_on_tax_base_for_third_age(
                            sri_config)
                    )

                    # Discount for disability
                    discount_disability = (
                        payslip.get_discount_on_tax_base_for_disability(
                            sri_config)
                    )

                    # Discount for disability burden
                    discount_disability_burden = (payslip.
                        get_discount_on_tax_base_for_disability_burden(
                            sri_config)
                    )
                except Exception:
                    pass

            result[payslip.id] = {
                'form107': pep,
                'basic_fraction_annual': basic_fraction,
                'discount_third_age': discount_third_age,
                'discount_disability': discount_disability,
                'discount_disability_burden': discount_disability_burden
            }
        return result

    def get_income_tax_line(self, tax_base, form107):
        pool = Pool()
        IncomeTax = pool.get('sri.configuration.income.tax')

        eval_date = form107.form_date if form107 else self.period.end_date
        income_taxes = IncomeTax.search([
            ('configuration.start_date', '<=', eval_date),
            ('configuration.end_date', '>=', eval_date),
            ('basic_fraction', '<=', tax_base),
            ('excess', '>=', tax_base),
        ])
        if not income_taxes:
            self.raise_user_error('sri_conf_income_tax_error')

        return income_taxes[0]

    @classmethod
    def get_projected_incomes(cls, payslip, form107, salary):
        projected_incomes = form107.total_105 if form107 else salary * 12
        return projected_incomes

    @classmethod
    def get_projected_expenses(cls, form107):
        projected = form107.total_111 if form107 else 0
        return projected

    @classmethod
    def get_law_benefit_types(cls):
        LawBenefitType = Pool().get('payslip.law.benefit.type')
        result = defaultdict(lambda: None)
        law_benefits_types = LawBenefitType.search(None)
        if law_benefits_types:
            for law_benefit_type in law_benefits_types:
                result.update({
                    law_benefit_type.code: law_benefit_type
                })
        return result

    @classmethod
    def get_law_benefits_to_delete(cls, payslip, law_benefit_type):
        cursor = Transaction().connection.cursor()
        LawBenefit = Pool().get('payslip.law.benefit')
        law_benefit = LawBenefit.__table__()
        to_delete = []
        delete_query = law_benefit.select(
            law_benefit.id,
            where=(
                (law_benefit.generator_payslip == payslip.id) &
                (law_benefit.period == payslip.period.id) &
                (law_benefit.employee == payslip.employee.id) &
                (law_benefit.type_ == law_benefit_type.id) &
                (law_benefit.is_paid_in_liquidation == False)
            )
        )
        cursor.execute(*delete_query)
        ids = [id for (id,) in cursor.fetchall()]
        for lb in LawBenefit.browse(ids):
            to_delete.append(lb)
        return to_delete

    @classmethod
    def get_law_benefits_payment_form_by_payslip(cls, payslip):
        result = defaultdict(lambda: None)
        result['xiii'] = payslip.contract.xiii
        result['xiv'] = payslip.contract.xiv
        result['fr'] = 'not_considered'
        if payslip.contract.have_fr:
            result['fr'] = payslip.contract.fr
        if payslip.template.accumulated_law_benefits:
            code = payslip.template.law_benefit_type.code
            result[code] = 'accumulate'
            result['fr'] = 'not_considered'
        return result

    def get_law_benefit_payment_form_by_field(self, field):
        return getattr(self.contract, field)

    @classmethod
    def get_holidays(cls, payslips):
        Holiday = Pool().get('hr_ec.holiday')
        holidays = defaultdict(lambda: [])
        holidays_dates = defaultdict(lambda: [])
        for payslip in payslips:
            start_date = payslip.start_date
            end_date = payslip.end_date
            holidays_info = Holiday.browse(
                Holiday.between_dates(start_date, end_date))
            h, hd = [], []
            for holiday in holidays_info:
                h.append(holiday)
                hd.append(holiday.date_discount)
            holidays[payslip.id] = h
            holidays_dates[payslip.id] = hd
        return {
            'holidays': holidays,
            'holidays_dates': holidays_dates
        }

    @classmethod
    def get_info_salary(cls, payslip):
        pool = Pool()
        Contract = pool.get('company.contract')
        context = Transaction().context
        salaries = defaultdict(lambda: [])
        hour_costs = defaultdict(lambda: [])
        with Transaction().set_context(salary_date=payslip.period.end_date):
            # Get employee salaries and hour_costs
            if context.get('info_salaries'):
                info_salaries = context['info_salaries']
                salaries[payslip.id] = info_salaries['salaries'][payslip.id]
                hour_costs[payslip.id] = utils.quantize_currency(
                    info_salaries['hour_costs'][payslip.id])
            else:
                # Get salary and hour cost by contract
                contract = payslip.contract
                info_salaries = Contract.get_salary([contract], 'salary')
                salaries_aux = info_salaries['salary']
                hour_cost_aux = info_salaries['hour_cost']

                # Set salary and hour cost information
                salaries[payslip.id] = salaries_aux[contract.id]
                hour_costs[payslip.id] = hour_cost_aux[contract.id]

            if salaries[payslip.id] <= 0:
                cls.raise_user_error('contract_with_wrong_salary', {
                    'contract': payslip.contract.rec_name,
                    'salary': salaries[payslip.id]
                })

            if hour_costs[payslip.id] <= 0:
                cls.raise_user_error('contract_with_wrong_hour_cost', {
                    'contract': payslip.contract.rec_name,
                    'hour_cost': hour_costs[payslip.id]
                })
        return {
            'salaries': salaries,
            'hour_costs': hour_costs
        }

    @classmethod
    def calculate_accumulated_law_benefits(cls, payslip, law_benefit_type):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.law.benefit')
        LawBenefitType = pool.get('payslip.law.benefit.type')

        tbl_payslip = cls.__table__()
        tbl_period = Period.__table__()
        tbl_law_benefit = LawBenefit.__table__()
        tbl_law_benefit_type = LawBenefitType.__table__()

        to_save_law_benefits = []
        current_base_salary = payslip.fiscalyear.base_salary
        total_amount = Decimal('0.00')
        total_adjustment = Decimal('0.00')
        total_rounding_error = Decimal('0.00')

        # Evaluate consulting dates
        contract_start_date = payslip.contract.start_date
        contract_end_date = payslip.contract.end_date
        if not contract_end_date:
            contract_end_date = datetime.max.date()

        # Start date
        if contract_start_date > payslip.start_date:
            start_date = contract_start_date
            start_date = start_date.replace(day=1)
        else:
            start_date = payslip.start_date

        # End date
        if contract_end_date < payslip.end_date:
            end_date = contract_end_date
            last_day = monthrange(end_date.year, end_date.month)[1]
            end_date = end_date.replace(day=last_day)
        else:
            end_date = payslip.end_date

        # Get law benefits generated using payslip functionalities on SIIM
        select_query = tbl_law_benefit.join(tbl_law_benefit_type,
                condition=tbl_law_benefit.type_ == tbl_law_benefit_type.id
            ).join(tbl_period,
                condition=tbl_law_benefit.period == tbl_period.id
            ).join(tbl_payslip,
                condition=tbl_law_benefit.generator_payslip == tbl_payslip.id
            ).select(
                tbl_law_benefit.id,
                where=((tbl_law_benefit.kind == 'accumulate') &
                       (tbl_law_benefit.is_paid_in_liquidation == False) &
                       (tbl_law_benefit_type.code == law_benefit_type.code) &
                       (tbl_law_benefit.employee == payslip.employee.id) &
                       (tbl_payslip.state == 'done') &
                       (tbl_period.start_date >= start_date) &
                       (tbl_period.end_date <= end_date))
            )

        cursor.execute(*select_query)
        ids = [row['id'] for row in cursor_dict(cursor)]

        for law_benefit in LawBenefit.browse(ids):
            generator_payslip = law_benefit.generator_payslip
            if law_benefit.type_.code == 'xiv':
                # Check rounding errors less than a cent that may have occurred
                # due to the use of deprecated rounding methods
                total_rounding_error += cls.get_rounding_error_xiv_amount(
                    law_benefit)

                # Adjustment to XIV for change in the basic salary of the
                # current fiscal year compared to the previous fiscal year
                previous_base_salary = generator_payslip.fiscalyear.base_salary
                if current_base_salary != previous_base_salary:
                    total_adjustment += cls.get_adjust_xiv_amount(
                        law_benefit,
                        previous_base_salary,
                        current_base_salary)

            total_amount += law_benefit.amount
            law_benefit.payslip = payslip
            law_benefit.payslip_date = generator_payslip.period.end_date
            to_save_law_benefits.append(law_benefit)

        total_adjustment += total_rounding_error
        if law_benefit_type.code == 'xiv' and total_adjustment:
            # If there is an adjustment of XIV due to a change in basic salary
            # or due to a rounding error, the corresponding record of kind
            # "adjustment" is created
            total_adjustment = utils.quantize_currency(total_adjustment)
            law_benefit_adjust = cls.get_new_law_benefit(
                payslip,
                law_benefit_type,
                total_adjustment,
                law_benefit_kind='adjustment',
                generator_payslip=payslip)
            law_benefit_adjust.payslip = payslip
            law_benefit_adjust.payslip_date = payslip.period.end_date

            # The period to which the adjustment law benefit belongs is the
            # period to which the end date of the accumulated XIV payment range
            # corresponds
            adjustment_periods = Period.search([
                ('start_date', '<=', payslip.end_date),
                ('end_date', '>=', payslip.end_date),
            ])
            if adjustment_periods:
                law_benefit_adjust.period = adjustment_periods[0]

            total_amount += total_adjustment
            to_save_law_benefits.append(law_benefit_adjust)

        benefit = None
        if to_save_law_benefits:
            # Here, a generic Law Benefit is created, it contains the
            # accumulated amount of the sum of law benefits amounts of this
            # employee
            benefit = LawBenefit()
            benefit.company = payslip.company
            benefit.employee = payslip.employee
            benefit.period = payslip.period
            benefit.amount = total_amount
            benefit.kind = 'accumulate'
            benefit.type_ = law_benefit_type

        return {
            'benefit': benefit,
            'to_save_law_benefits': to_save_law_benefits
        }

    @classmethod
    def get_adjust_xiv_amount(cls, law_benefit, previous_base_salary,
            current_base_salary):
        old_amount = law_benefit.amount
        new_amount = old_amount * current_base_salary / previous_base_salary
        adjustment = new_amount - old_amount
        return utils.quantize_currency(adjustment)

    @classmethod
    def get_rounding_error_xiv_amount(cls, law_benefit):
        months = Decimal('12.00')
        gen_payslip = law_benefit.generator_payslip
        worked_days_with_normative = gen_payslip.worked_days_with_normative
        days_disease = gen_payslip.total_days_disease
        period_days = gen_payslip.period_days
        base_salary = gen_payslip.fiscalyear.base_salary
        period_days = utils.quantize_currency(period_days)

        worked_days = worked_days_with_normative + days_disease
        worked_days = utils.quantize_currency(worked_days)

        rounded_amount = law_benefit.amount
        complete_amount = (base_salary * worked_days / period_days) / months

        error = abs(complete_amount - rounded_amount)
        error = utils.quantize_currency(error, exp=CENT_LB)
        if error <= Decimal('0.01'):
            return error
        return Decimal('0.00')

    @classmethod
    def get_calculated_law_benefits(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: [])
        LawBenefit = pool.get('payslip.law.benefit')
        Period = pool.get('account.period')
        law_benefit = LawBenefit.__table__()
        table = cls.__table__()
        tbl_period = Period.__table__()

        ids_payslips_accum = []
        ids_payslips_month = []
        for p in payslips:
            if p.template.accumulated_law_benefits:
                ids_payslips_accum.append(p.id)
            else:
                ids_payslips_month.append(p.id)

        # PAYSLIPS FOR NO ACCUMULATED LAW BENEFITS
        for sub_ids in grouped_slice(ids_payslips_month):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(law_benefit,
                condition=(table.id == law_benefit.generator_payslip)
            ).select(
                table.id,
                law_benefit.id.as_('law_benefit'),
                where=red_sql & (law_benefit.period == table.period)
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['law_benefit'])

        # PAYSLIPS FOR ACCUMULATED LAW BENEFITS
        for sub_ids in grouped_slice(ids_payslips_accum):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(law_benefit,
                condition=(table.id == law_benefit.payslip)
            ).join(tbl_period,
                condition=(tbl_period.id == law_benefit.period)
            ).select(
                table.id,
                law_benefit.id.as_('law_benefit'),
                where=red_sql,
                order_by=[tbl_period.end_date.desc]
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['law_benefit'])

        return {
            'law_benefits': result,
        }

    @classmethod
    def get_all_fixed_incomes(cls, payslips, names=None,
            is_recalculate=False):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: [])
        FixedIncome = pool.get('company.contract.fixed.income')
        fixed_income = FixedIncome.__table__()
        table = cls.__table__()

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(fixed_income,
                condition=table.contract == fixed_income.contract
            ).select(
                table.id,
                fixed_income.id.as_('fixed_income'),
                where=red_sql & (fixed_income.is_active == True)
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['fixed_income'])
        return {
            'fixed_incomes': result,
        }

    @classmethod
    def get_all_fixed_deductions(cls, payslips, names=None,
            is_recalculate=False):
        result = defaultdict(lambda: [])
        codes_to_consider = defaultdict(lambda: [])

        pool = Pool()
        cursor = Transaction().connection.cursor()
        FixedDeduction = pool.get('company.contract.fixed.deduction')
        TypeIncomeDeduction = pool.get('company.contract.type.income.deduction')
        fixed_deduction = FixedDeduction.__table__()
        type_income_deduction = TypeIncomeDeduction.__table__()
        table = cls.__table__()

        if is_recalculate:
            for payslip in payslips:
                for fixed in payslip.fixed_deductions:
                    code = fixed.type_income_deduction.code
                    codes_to_consider[payslip.id].append(code)

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(fixed_deduction,
                condition=(table.contract == fixed_deduction.contract)
            ).join(type_income_deduction,
                condition=(fixed_deduction.type_income_deduction ==
                           type_income_deduction.id)
            ).select(
                table.id,
                fixed_deduction.id.as_('fixed_deduction'),
                type_income_deduction.code,
                where=((red_sql) &
                       (fixed_deduction.is_active == True))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if is_recalculate:
                    if row['code'] in codes_to_consider[row['id']]:
                        result[row['id']].append(row['fixed_deduction'])
                else:
                    result[row['id']].append(row['fixed_deduction'])
        return {
            'fixed_deductions': result
        }

    @classmethod
    def get_all_comms_subs_details(
            cls, payslips, commission = False, subrogation = False,
            commission_other = False, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        CommSub = pool.get('company.commission.subrogation')
        CommSubType = pool.get('company.commission.subrogation.type')
        Detail = pool.get('company.commission.subrogation.detail')
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        ActionStaff = pool.get('company.action.staff')
        comm_sub = CommSub.__table__()
        comm_sub_type = CommSubType.__table__()
        detail = Detail.__table__()
        payslip_comm_sub_detail = PayslipCommSubDetail.__table__()
        action_staff = ActionStaff.__table__()

        where = (comm_sub.state == 'done')
        type_ = ['']
        if commission or subrogation or commission_other:
            if commission or commission_other:
                type_.append('Encargo')
            if subrogation:
                type_.append('Subrogación')
        where &= (comm_sub_type.name.in_(type_))

        result = defaultdict(lambda: [])
        result_work_days = defaultdict(lambda: [])
        for payslip in payslips:
            # If an commission/subrogation was registered at the end of the
            # month and did not enter the corresponding payslip, it is
            # considered in the current role if it has registered the specified
            # number of days (11) before the start of the current payslip
            payslip_period_end_date = payslip.period.end_date
            payslip_period_start_date = (
                    payslip.period.start_date - timedelta(days=11))

            # Get possible comm/sub details to consider in this payslip
            table_A = comm_sub.join(detail,
                condition=(comm_sub.id == detail.commission)
            ).join(comm_sub_type,
                condition=(comm_sub_type.id == comm_sub.type_),
            ).join(action_staff,
                condition=(action_staff.id == comm_sub.action_staff)
            ).select(
                action_staff.employee,
                detail.id.as_('comm_sub_detail'),
                detail.end_date,
                detail.work_days,
                comm_sub_type.name.as_('comm_sub_type'),
                where=(where &
                       (action_staff.employee == payslip.employee.id) &
                       (detail.end_date <= payslip_period_end_date) &
                       (detail.start_date >= payslip_period_start_date)
                )
            )
            # Get comm/sub details considered in other payslips
            table_B = payslip_comm_sub_detail.join(detail,
                condition=(detail.id == payslip_comm_sub_detail.comm_sub_detail)
            ).select(
                payslip_comm_sub_detail.comm_sub_detail.as_(
                    'comm_sub_detail_aux'),
                payslip_comm_sub_detail.payslip,
                where=((payslip_comm_sub_detail.was_considered == 't') &
                       (detail.end_date <= payslip.period.end_date)
                )
            )
            # Get comm/sub details to consider in this payslip
            query = table_A.join(table_B, type_='LEFT OUTER',
                condition=(
                    table_A.comm_sub_detail == table_B.comm_sub_detail_aux)
            ).select(
                table_A.comm_sub_detail,
                table_A.work_days,
                table_A.comm_sub_type,
                where=((table_B.payslip == payslip.id) |
                       (table_B.comm_sub_detail_aux == Null)
                )
            )
            # Execute
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[payslip.id].append(row['comm_sub_detail'])
                if row['comm_sub_type'] == 'Encargo':
                    result_work_days[payslip.id].append(row['work_days'])
        _work_days = defaultdict(lambda: 0)
        for row in result_work_days.items():
            _work_days[row[0]] = reduce(lambda a, b: a + b, [x for x in row[1]])
        return result, _work_days

    @classmethod
    def get_all_deduction_loans(cls, payslips, names=None,
            is_recalculate=False):
        result = defaultdict(lambda: [])

        # If you have not requested to re-calculate the payslip, the system
        # obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            cursor = Transaction().connection.cursor()
            Loan = pool.get('company.employee.loan')
            LoanLine = pool.get('company.employee.loan.line')
            PayslipDeductionLoanLine = pool.get(
                'payslip.payslip.deduction.loan.line')
            loan = Loan.__table__()
            loan_line = LoanLine.__table__()
            payslip_loan_line = PayslipDeductionLoanLine.__table__()

            for payslip in payslips:
                # Get possible loan lines to consider in this payslip
                table_A = loan.join(loan_line,
                    condition=(loan.id == loan_line.loan)
                ).select(
                    loan.employee,
                    loan_line.id.as_('loan_line'),
                    loan_line.start_date,
                    where=((loan.state == 'open') &
                           (loan.employee == payslip.employee.id) &
                           (loan_line.state == 'pending') &
                           (loan_line.start_date <= payslip.period.end_date)
                    )
                )
                # Get loan lines considered in other payslips
                table_B = payslip_loan_line.join(loan_line,
                    condition=(loan_line.id == payslip_loan_line.loan_line)
                ).select(
                    payslip_loan_line.loan_line.as_('loan_line_aux'),
                    payslip_loan_line.payslip,
                    where=((payslip_loan_line.was_considered == 't') &
                           (loan_line.start_date <= payslip.period.end_date)
                    )
                )
                # Get loan lines to consider in this payslip
                query = table_A.join(table_B, type_='LEFT OUTER',
                    condition=(table_A.loan_line == table_B.loan_line_aux)
                ).select(
                    table_A.loan_line,
                    where=((table_B.payslip == payslip.id) | (
                            (table_B.loan_line_aux == Null)
                            & (table_A.start_date <= payslip.period.end_date))
                           # Next condition indicates if is needed to consider
                           # only items from current period
                           # &(table_A.start_date >= payslip.period.start_date))
                    )
                )
                # Execute
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    result[payslip.id].append(row['loan_line'])
        # Else, the system works with the information manipulated by the
        # user in the form.
        else:
            for payslip in payslips:
                for loan_line in payslip.deduction_loans:
                    result[payslip.id].append(loan_line.id)
        return {
            'deduction_loans': result,
        }

    @classmethod
    def get_all_entries(cls, payslips, names=None, is_recalculate=False):
        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])
        # If you have not requested to re-calculate the payslip, the system
        # obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            cursor = Transaction().connection.cursor()
            Entry = pool.get('payslip.entry')
            PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
            LiquidationManualEntry = pool.get(
                'contract.liquidation.payslip.entry')
            entry = Entry.__table__()
            payslip_manual_entry = PayslipManualEntry.__table__()
            liquidation_manual_entry = LiquidationManualEntry.__table__()

            for payslip in payslips:
                # Get possible entries to consider in this payslip
                table_A = entry.select(
                    entry.employee,
                    entry.id.as_('entry'),
                    entry.kind,
                    entry.entry_date,
                    where=((entry.employee == payslip.employee.id) &
                           (entry.state == 'done') &
                           (entry.entry_date <= payslip.period.end_date)
                           & (entry.entry_date >= payslip.period.start_date)
                    )
                )
                # Get entries considered in other payslips
                table_B = payslip_manual_entry.join(entry,
                    condition=(entry.id == payslip_manual_entry.entry)
                ).select(
                    payslip_manual_entry.entry.as_('entry_aux'),
                    payslip_manual_entry.payslip,
                    where=((payslip_manual_entry.was_considered == 't') &
                           (entry.entry_date <= payslip.period.end_date)
                    )
                )
                # Get entries considered in other liquidations
                table_C = liquidation_manual_entry.join(entry,
                    condition=(entry.id == liquidation_manual_entry.entry)
                ).select(
                    liquidation_manual_entry.entry,
                    where=((liquidation_manual_entry.was_considered == 't') &
                           (entry.entry_date <= payslip.period.end_date)
                           )
                )
                # Get entries to consider in this payslip
                query1 = table_A.join(table_B, type_='LEFT OUTER',
                    condition=(table_A.entry == table_B.entry_aux)
                ).select(
                    table_A.entry,
                    table_A.kind,
                    where=((table_B.payslip == payslip.id) |
                           (table_B.entry_aux == Null)
                    )
                )
                query2 = query1.join(table_C, type_='LEFT OUTER',
                    condition=(query1.entry == table_C.entry)
                ).select(
                    query1.entry,
                    query1.kind,
                    where=((table_C.entry == Null)
                    )
                )

                # Execute
                cursor.execute(*query2)
                for row in cursor_dict(cursor):
                    if row['kind'] == 'income':
                        incomes[payslip.id].append(row['entry'])
                    elif row['kind'] == 'deduction':
                        deductions[payslip.id].append(row['entry'])
        else:
            # Else, the system works with the information manipulated by the
            # user in the form.
            for payslip in payslips:
                for income_entry in payslip.income_entries:
                    incomes[payslip.id].append(income_entry.id)
                for deduction_entry in payslip.deduction_entries:
                    deductions[payslip.id].append(deduction_entry.id)

        return {
            'income_entries': incomes,
            'deduction_entries': deductions
        }

    @classmethod
    def get_all_overtime_lines(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Overtime = pool.get('hr_ec.overtime')
        OvertimeLine = pool.get('hr_ec.overtime.line')
        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        overtime = Overtime.__table__()
        overtime_line = OvertimeLine.__table__()
        payslip_overtime_line = PayslipOvertimeLine.__table__()

        result = defaultdict(lambda: [])
        for payslip in payslips:
            # PREVIOUS QUERY
            # Get possible overtime lines to consider in this payslip
            # table_A = overtime.join(overtime_line,
            #         condition=(
            #         overtime.id == overtime_line.overtime)
            #         ).select(
            #     overtime.employee,
            #     overtime_line.id.as_('overtime_line'),
            #     where=((overtime.employee == payslip.employee.id) &
            #            (overtime.state == 'done') &
            #            (overtime.done_date <=
            #             payslip.period.overtime_end_date) &
            #            (overtime.done_date >=
            #             payslip.period.overtime_start_date) &
            #            (overtime_line.state == 'valid') &
            #            (overtime_line.overtime_date <= payslip.end_date)
            #            # & (overtime_line.overtime_date >= payslip.start_date)
            #            )
            # )
            # NEW QUERY
            table_A = overtime.join(overtime_line,
                    condition=(
                        overtime.id == overtime_line.overtime)
                    ).select(
                overtime.employee,
                overtime_line.id.as_('overtime_line'),
                where=((overtime.employee == payslip.employee.id) &
                       (overtime.state == 'done') &
                       (overtime_line.is_paid_in_liquidation == False) &
                       (overtime_line.state == 'valid') &
                       (overtime_line.overtime_date <=
                        payslip.period.payslip_overtime_end_date) &
                       ((overtime_line.overtime_date >=
                         payslip.period.payslip_overtime_start_date)
                        | (overtime_line.is_lagged == 't'))
                )
            )

            # Get overtime lines considered in other payslips
            table_B = payslip_overtime_line.join(overtime_line,
                condition=(
                    overtime_line.id == payslip_overtime_line.overtime_line)
            ).select(
                payslip_overtime_line.overtime_line.as_('overtime_line_aux'),
                payslip_overtime_line.payslip,
                where=((payslip_overtime_line.was_considered == 't') &
                       (overtime_line.overtime_date <=
                        payslip.period.payslip_overtime_end_date)
                )
            )
            # Get overtime lines to consider in this payslip
            query = table_A.join(table_B, type_='LEFT OUTER',
                condition=(table_A.overtime_line == table_B.overtime_line_aux)
            ).select(
                table_A.overtime_line,
                where=((table_B.payslip == payslip.id) |
                       (table_B.overtime_line_aux == Null)
                )
            )
            # Execute
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[payslip.id].append(row['overtime_line'])
        return {
            'overtimes': result,
        }

    @classmethod
    def get_all_absences(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Absence = pool.get('hr_ec.absence')
        Contract = pool.get('company.contract')

        table = cls.__table__()
        absence = Absence.__table__()
        contract = Contract.__table__()

        result = defaultdict(lambda: [])

        # Absences that overlaps with medical certificates
        query_absences_overlaps_cerificates = (
            cls.get_query_absences_overlaps_with_medical_certificates())

        # Contracts without discount for absences
        query_contracts_without_absences_apply = (
            cls.get_query_contracts_without_absences_apply())

        # Get absences to consider in this payslip
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(absence,
                condition=table.employee == absence.employee
            ).join(contract,
                condition=absence.employee == contract.employee
            ).select(
                table.id,
                absence.id.as_('absence'),
                where=(red_sql &
                    ~(table.contract.in_(
                        query_contracts_without_absences_apply)) &
                    ~(absence.id.in_(query_absences_overlaps_cerificates)) &
                    (absence.end_date >= table.start_date) &
                    (absence.end_date <= table.end_date) &
                    (absence.state == 'done') &
                    (absence.is_paid_in_liquidation == False)
                       )
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['absence'])
        return {
            'absences': result,
        }

    @classmethod
    def get_all_permissions(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Permission = pool.get('hr_ec.permission')
        Contract = pool.get('company.contract')

        table = cls.__table__()
        permission = Permission.__table__()
        contract = Contract.__table__()

        result = defaultdict(lambda: [])

        # Permissions that overlaps with medical certificates
        query_permissions_overlaps_cerificates = (
            cls.get_query_permissions_overlaps_with_medical_certificates())

        # Get permission to consider in this payslip
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(permission,
                condition=table.contract == permission.contract
            ).join(contract,
                condition=permission.contract == contract.id
            ).select(
                table.id,
                permission.id.as_('permission'),
                where=(red_sql
                       & ~(permission.id.in_(
                            query_permissions_overlaps_cerificates))
                       & (contract.absence_apply == True)
                       & (permission.end_date >= table.start_date)
                       & (permission.end_date <= table.end_date)
                       & (permission.state == 'done')
                       & (permission.is_paid_in_liquidation == False)
                       & (permission.discount_to.in_(['payslip']))
                )
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['permission'])
        return {
            'permissions': result,
        }

    @classmethod
    def get_all_permissions_license(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Permission = pool.get('hr_ec.permission')
        Period = pool.get('account.period')

        table = cls.__table__()
        permission = Permission.__table__()
        period = Period.__table__()

        result = defaultdict(lambda: [])

        where_dates = (((period.start_date <= permission.start_date) &
                       (permission.end_date <= period.end_date)) |
                      ((period.start_date <= permission.start_date) &
                       (permission.start_date <= period.end_date) &
                       (period.end_date <= permission.end_date)) |
                      ((permission.start_date <= period.start_date) &
                       (period.start_date <= permission.end_date) &
                       (permission.end_date <= period.end_date)) |
                      ((permission.start_date <= period.start_date) &
                       (period.end_date <= permission.end_date)))

        # Get permission license to consider in this payslip
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(permission,
                condition=table.contract == permission.contract
            ).join(period,
                condition=table.period == period.id
            ).select(
                table.id,
                permission.id.as_('permission'),
                where=(red_sql
                       & (permission.state == 'done')
                       & (permission.discount_to.in_(['payslip', 'license']))
                       & (permission.is_paid_in_liquidation == False)
                       & (where_dates)
                )
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['permission'])
        return {
            'permissions_license': result,
        }

    @classmethod
    def get_all_medical_certificates_info(cls, payslips, names=None,
            is_recalculate=False):
        medical_certificates = defaultdict(lambda: [])
        total_days_disease = defaultdict(lambda: 0)

        # If you have not requested to re-calculate the payslip, the system
        # obtains all the information from the database.
        if not is_recalculate:
            pool = Pool()
            MedicalCertificatePayslip = pool.get(
                'galeno.medical.certificate.payslip')

            period_days_info = get_default_period_days_info()
            period_days = period_days_info['value']

            for payslip in payslips:
                period = payslip.period
                _start_date = period.payslip_start_date
                _end_date = period.payslip_end_date
                p_start_date = _start_date if _start_date else period.start_date
                p_end_date = _end_date if _end_date else period.end_date

                with Transaction().set_context(start_date=p_start_date,
                        end_date=p_end_date):

                    if period_days_info['type'] != 'fixed':
                        period_days = monthrange(payslip.period.end_date.year,
                            payslip.period.end_date.month)[1]

                    medical_certificates[payslip.id] = []
                    total_days_disease[payslip.id] = 0
                    try:
                        medical_info = MedicalCertificatePayslip.search([
                            ('employee', '=', payslip.employee)
                        ])
                        if medical_info:
                            for med in medical_info:
                                medical_certificates[payslip.id].append({
                                    'certificate_type_id':
                                        med.certificate_type.id,
                                    'certificate_type':
                                        med.certificate_type.name,
                                    'total_days': med.number_days_month,
                                    'percentage': med.percentage_payslip,
                                    'order_': med.order_
                                })
                                total_days_disease[payslip.id] += (
                                    med.number_days_month)

                            # Verify that the days of illness or accident are
                            # greater than the days of the period; if not, an
                            # automatic adjustment is made
                            if total_days_disease[payslip.id] > period_days:
                                med = medical_certificates[payslip.id][-1]
                                # It should not be removed every day of illness
                                # or accident, when its reach 1 the user should
                                # edit the field according to administrative
                                # decitions
                                while((total_days_disease[payslip.id] >
                                        period_days) and (
                                        med['total_days'] > 1)):
                                    med['total_days'] -= 1
                                    total_days_disease[payslip.id] -= 1
                    except Exception:
                        pass
        # Else, the system works with the information manipulated by the
        # user in the form.
        else:
            for payslip in payslips:
                medical_certificates[payslip.id] = []
                total_days_disease[payslip.id] = 0
                for med in payslip.medical_certificates_discount:
                    medical_certificates[payslip.id].append({
                        'certificate_type_id': med.certificate_type.id,
                        'certificate_type': med.certificate_type.name,
                        'total_days': med.number_days_month,
                        'percentage': med.percentage_payslip,
                        'order_': med.order_
                    })
                    total_days_disease[payslip.id] += med.number_days_month

        return {
            'medical_certificates_info': medical_certificates,
            'total_days_disease': total_days_disease
        }

    @classmethod
    def get_considered_medical_certificates_discount(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')
        table = cls.__table__()
        payslip_med_certificate_discount = (
            PayslipMedicalCertificateDiscount.__table__())

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_med_certificate_discount,
                condition=(table.id == payslip_med_certificate_discount.payslip)
            ).select(
                table.id,
                payslip_med_certificate_discount.id.as_('med_certificate_disc'),
                where=red_sql
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['med_certificate_disc'])

        return {
            'medical_certificates_discount': result
        }

    @classmethod
    def get_considered_medical_certificates(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        medical_certificate = MedicalCertificate.__table__()

        result = defaultdict(lambda: [])

        # Prepare column names
        cs = medical_certificate.start_date
        ce = medical_certificate.end_date

        for payslip in payslips:
            ps, pe = None, None
            if not payslip.template_for_accumulated_law_benefits:
                period = payslip.period
                _start_date = period.payslip_start_date
                _end_date = period.payslip_end_date
                ps = _start_date if _start_date else period.start_date
                pe = _end_date if _end_date else period.end_date
            else:
                # TODO Obtain medical certificates only from the payslips in
                # TODO which the law benefit accrued
                ps = payslip.start_date
                pe = payslip.end_date
            if ps and pe:
                if pe <= ps:
                    continue
                query = medical_certificate.select(
                    medical_certificate.id.as_('medical_certificate'),
                    where=(
                        (payslip.employee.id == medical_certificate.employee) &
                        (((cs <= ps) & (ce >= ps) & (ce <= pe)) |
                        ((cs >= ps) & (cs <= ce) & (ce <= pe)) |
                        ((cs >= ps) & (cs <= pe) & (ce >= pe)) |
                        ((cs <= ps) & (ce >= pe))) &
                        (medical_certificate.state == 'done') &
                        (medical_certificate.register_type == 'days')
                    ),
                    order_by=[medical_certificate.start_date.desc]
                )
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    result[payslip.id].append(row['medical_certificate'])
        return {
            'medical_certificates': result,
        }

    @classmethod
    def get_considered_comms_subs_details(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        table = cls.__table__()
        payslip_comm_sub_detail = PayslipCommSubDetail.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_comm_sub_detail,
                condition=(table.id == payslip_comm_sub_detail.payslip)
            ).select(
                table.id,
                payslip_comm_sub_detail.comm_sub_detail,
                where=red_sql & (payslip_comm_sub_detail.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['comm_sub_detail'])

        return {
            'comms_subs': result,
        }

    @classmethod
    def get_considered_deduction_loans(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        result = defaultdict(lambda: [])
        table = cls.__table__()
        PayslipDeductionLoanLine = pool.get(
            'payslip.payslip.deduction.loan.line')
        payslip_deduction_loan_line = PayslipDeductionLoanLine.__table__()
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_deduction_loan_line,
                condition=(table.id == payslip_deduction_loan_line.payslip)
            ).select(
                table.id,
                payslip_deduction_loan_line.loan_line,
                where=red_sql & (
                    payslip_deduction_loan_line.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['loan_line'])

        return {
            'deduction_loans': result,
        }

    @classmethod
    def get_considered_entries(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        table = cls.__table__()
        payslip_manual_entry = PayslipManualEntry.__table__()

        incomes = defaultdict(lambda: [])
        deductions = defaultdict(lambda: [])

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_manual_entry,
                condition=(table.id == payslip_manual_entry.payslip)
            ).select(
                table.id,
                payslip_manual_entry.entry,
                payslip_manual_entry.kind,
                where=red_sql & (payslip_manual_entry.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                if row['kind'] == 'income':
                    incomes[row['id']].append(row['entry'])
                elif row['kind'] == 'deduction':
                    deductions[row['id']].append(row['entry'])

        return {
            'income_entries': incomes,
            'deduction_entries': deductions,
        }

    @classmethod
    def get_considered_overtime_lines(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        OvertimeLine = pool.get('hr_ec.overtime.line')
        table = cls.__table__()
        tbl_overtime_line = OvertimeLine.__table__()
        payslip_overtime_line = PayslipOvertimeLine.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_overtime_line,
                condition=(table.id == payslip_overtime_line.payslip)
            ).join(tbl_overtime_line,
                condition=(
                    payslip_overtime_line.overtime_line == tbl_overtime_line.id)
            ).select(
                table.id,
                payslip_overtime_line.overtime_line,
                tbl_overtime_line.overtime_date,
                tbl_overtime_line.start_time,
                where=red_sql & (payslip_overtime_line.was_considered == 't'),
                order_by=[
                    tbl_overtime_line.overtime_date.desc,
                    tbl_overtime_line.start_time.desc]
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['overtime_line'])

        return {
            'overtimes': result,
        }

    @classmethod
    def get_considered_payslip_fixed_incomes(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipFixedIncomeDeduction = pool.get(
            'payslip.fixed.income.deduction')
        FixedTypeIncomeDeduction = pool.get(
            'company.contract.type.income.deduction')

        payslip_fixed_i_d = PayslipFixedIncomeDeduction.__table__()
        fixed_type_i_d = FixedTypeIncomeDeduction.__table__()
        table = cls.__table__()

        income = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_fixed_i_d,
                condition=(
                    table.id == payslip_fixed_i_d.payslip
                )
            ).join(fixed_type_i_d,
                condition=(
                    fixed_type_i_d.id == payslip_fixed_i_d.type_income_deduction
                )
            ).select(
                table.id,
                payslip_fixed_i_d.id.as_('payslip_fixed_income'),
                fixed_type_i_d.kind,
                where=red_sql & (fixed_type_i_d.kind == 'income')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                income[row['id']].append(row['payslip_fixed_income'])

        return {
            'fixed_incomes': income
        }

    @classmethod
    def get_considered_payslip_fixed_deductions(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipFixedIncomeDeduction = pool.get(
            'payslip.fixed.income.deduction')
        FixedTypeIncomeDeduction = pool.get(
            'company.contract.type.income.deduction')

        payslip_fixed_i_d = PayslipFixedIncomeDeduction.__table__()
        fixed_type_i_d = FixedTypeIncomeDeduction.__table__()
        table = cls.__table__()

        deduction = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_fixed_i_d,
                condition=table.id == payslip_fixed_i_d.payslip
            ).join(fixed_type_i_d,
                condition=(
                    fixed_type_i_d.id == payslip_fixed_i_d.type_income_deduction
                )
            ).select(
                table.id,
                payslip_fixed_i_d.id.as_('payslip_fixed_deduction'),
                fixed_type_i_d.kind,
                where=red_sql & (fixed_type_i_d.kind == 'deduction')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                deduction[row['id']].append(row['payslip_fixed_deduction'])

        return {
            'fixed_deductions': deduction
        }

    @classmethod
    def get_considered_absences(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')
        table = cls.__table__()
        payslip_absence = PayslipAbsence.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_absence,
                condition=(table.id == payslip_absence.payslip)
            ).select(
                table.id,
                payslip_absence.absence,
                where=red_sql & (payslip_absence.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['absence'])

        return {
            'absences': result,
        }

    @classmethod
    def get_considered_workshift_dialings(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipWorkshiftDialing = pool.get('payslip.workshift.dialing')
        table = cls.__table__()
        payslip_workshift_dialing = PayslipWorkshiftDialing.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_workshift_dialing,
                condition=(
                    table.id == payslip_workshift_dialing.payslip)
                ).select(
                    table.id,
                    payslip_workshift_dialing.id.as_(
                        'payslip_workshift_dialing'),
                where=red_sql
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['payslip_workshift_dialing'])

        return {
            'workshift_dialings': result
        }

    @classmethod
    def get_considered_workshift_delays(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipWorkshiftDelayLine = pool.get(
            'payslip.payslip.hr_ec.workshift.delay.line')
        table = cls.__table__()
        payslip_workshift_delay_line = PayslipWorkshiftDelayLine.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_workshift_delay_line,
                condition=(table.id == payslip_workshift_delay_line.payslip)
            ).select(
                table.id,
                payslip_workshift_delay_line.workshift_delay_line,
                where=((red_sql) &
                       (payslip_workshift_delay_line.was_considered == 't'))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['workshift_delay_line'])
        return {
            'workshift_delays': result,
        }

    @classmethod
    def get_medical_certificates(cls, payslips, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        medical_certificate = MedicalCertificate.__table__()

        result = defaultdict(lambda: [])
        for payslip in payslips:
            query = medical_certificate.select(
                medical_certificate.id.as_('medical_certificate'),
                where=((medical_certificate.start_date >= payslip.start_date) &
                       (medical_certificate.end_date <= payslip.end_date) &
                       (medical_certificate.state == 'done') &
                       (medical_certificate.register_type == 'days'))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[payslip.id].append(row['medical_certificate'])

        return {
            'medical_certificates': result,
        }

    @classmethod
    def get_considered_permissions(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')
        table = cls.__table__()
        payslip_permission = PayslipPermission.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_permission,
                condition=(table.id == payslip_permission.payslip)
            ).select(
                table.id,
                payslip_permission.permission,
                where=red_sql & (payslip_permission.was_considered == 't')
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['permission'])

        return {
            'permissions': result,
        }

    @classmethod
    def get_considered_permissions_license(cls, payslips, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipPermissionLicense = pool.get(
            'payslip.payslip.hr_ec.permission.license')
        table = cls.__table__()
        payslip_permission_license = PayslipPermissionLicense.__table__()

        result = defaultdict(lambda: [])
        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payslip_permission_license,
                condition=(table.id == payslip_permission_license.payslip)
            ).select(
                table.id,
                payslip_permission_license.permission,
                where=((red_sql) &
                       (payslip_permission_license.was_considered == 't'))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                result[row['id']].append(row['permission'])

        return {
            'permissions_license': result,
        }

    @classmethod
    def get_comms_subs_amounts(cls, payslips, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')

        field_name = 'amount'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            fields = get_fields_values(
                CommSubDetail, comms_subs_details[payslip.id], [field_name])
            amount = sum([field[field_name] for field in fields])
            result[payslip.id] = amount
        return result

    @classmethod
    def get_comms_subs_amounts_by_type(cls, payslips, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')
        CommSubType = pool.get('company.commission.subrogation.type')

        field_amount = 'amount'
        field_type = 'type_'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            items = defaultdict(lambda: 0)
            fields = get_fields_values(
                CommSubDetail, comms_subs_details[payslip.id], [
                    field_amount, field_type])
            for field in fields:
                name_type = get_single_field_value(
                    CommSubType, field[field_type], 'name')
                amount = field[field_amount]
                if name_type not in items:
                    items[name_type] = amount
                else:
                    items[name_type] += amount
            result[payslip.id] = items
        return result

    @classmethod
    def get_loans_amounts_by_type(cls, payslips, loans):
        pool = Pool()
        LoanLine = pool.get('company.employee.loan.line')
        LoanType = pool.get('company.employee.loan.type')

        field_amount = 'fee_residue'
        field_type = 'type'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            items = defaultdict(lambda: 0)
            fields = get_fields_values(
                LoanLine, loans[payslip.id], [field_amount, field_type])
            for field in fields:
                name_type = get_single_field_value(
                    LoanType, field[field_type], 'name')
                amount = field[field_amount]
                if name_type not in items:
                    items[name_type] = amount
                else:
                    items[name_type] += amount
            result[payslip.id] = items
        return result

    @classmethod
    def get_overtimes_amounts_by_type(cls, payslips, overtimes):
        pool = Pool()
        OvertimeLine = pool.get('hr_ec.overtime.line')
        OvertimeType = pool.get('hr_ec.overtime.type')

        field_amount = 'total_with_salary'
        field_type = 'type_'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            items = defaultdict(lambda: 0)
            fields = get_fields_values(
                OvertimeLine, overtimes[payslip.id], [field_amount, field_type])
            for field in fields:
                name_type = get_single_field_value(
                    OvertimeType, field[field_type], 'description')
                amount = field[field_amount] if field[field_amount] else 0
                if name_type not in items:
                    items[name_type] = amount
                else:
                    items[name_type] += amount
            result[payslip.id] = items
        return result

    @classmethod
    def get_overtimes_amounts(cls, payslips, overtime_lines):
        pool = Pool()
        OvertimeLine = pool.get('hr_ec.overtime.line')

        field_name = 'total_with_salary'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            fields = get_fields_values(
                OvertimeLine, overtime_lines[payslip.id], [field_name])
            # TODO Review
            amount = sum([field[field_name] for field in fields if (
                field and field[field_name])])
            result[payslip.id] = amount
        return result

    @classmethod
    def get_loans_amounts(cls, payslips, deductions_loans):
        pool = Pool()
        LoanLine = pool.get('company.employee.loan.line')

        field_name = 'fee_residue'
        result = defaultdict(lambda: [])
        for payslip in payslips:
            fields = get_fields_values(
                LoanLine, deductions_loans[payslip.id], [field_name])
            amount = sum([field[field_name] for field in fields])
            result[payslip.id] = amount
        return result

    @classmethod
    def get_manual_entries_amounts(cls, payslips, income_entries,
            deduction_entries):
        pool = Pool()
        Entry = pool.get('payslip.entry')
        EntryType = pool.get('payslip.entry.type')

        manual_entries_amounts = defaultdict(lambda: [])
        for payslip in payslips:
            entries = defaultdict(lambda: 0)
            # Union entries
            list_entries = []
            if income_entries:
                list_entries += income_entries[payslip.id]
            if deduction_entries:
                list_entries += deduction_entries[payslip.id]
            # Manual entries
            if list_entries:
                fields = get_fields_values(
                    Entry, list_entries, ['type_', 'real_amount', 'amount'])
                for field in fields:
                    code_entry = get_single_field_value(
                        EntryType, field['type_'], 'code')
                    amount = field['real_amount']
                    # amount = amount if amount is None else field['amount']
                    if code_entry not in entries:
                        entries[code_entry] = amount
                    else:
                        entries[code_entry] += amount
            # Results
            manual_entries_amounts[payslip.id] = entries
        return manual_entries_amounts

    @classmethod
    def get_fixed_incomes_amounts(cls, payslips, fixed_incomes):
        FixedIncome = Pool().get('company.contract.fixed.income')
        result = cls.get_fixed_amounts(
            FixedIncome, payslips, fixed_incomes)
        return result

    @classmethod
    def get_fixed_deductions_amounts(cls, payslips, fixed_deductions):
        FixedDeduction = Pool().get('company.contract.fixed.deduction')
        result = cls.get_fixed_amounts(
            FixedDeduction, payslips, fixed_deductions)
        return result

    @classmethod
    def get_fixed_amounts(cls, model, payslips, fixed_list):
        pool = Pool()
        TypeIncomeDeduction = pool.get('company.contract.type.income.deduction')
        result = defaultdict(lambda: [])
        for payslip in payslips:
            fixeds = defaultdict(lambda: 0)
            fields = get_fields_values(model, fixed_list[payslip.id],
                ['amount', 'type_income_deduction'])
            for field in fields:
                fixed_type_code = get_single_field_value(TypeIncomeDeduction,
                    field['type_income_deduction'], 'code')
                if fixed_type_code not in fixeds:
                    fixeds[fixed_type_code] = field['amount']
                else:
                    fixeds[fixed_type_code] += field['amount']
            result[payslip.id] = fixeds
        return result

    @classmethod
    def set_deduction_entries(cls, payslips, name, value):
        pool = Pool()
        PayslipEntry = pool.get('payslip.entry')
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        to_save = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    entry = PayslipEntry.search([('id', '=', id)])[0]
                    payslip_manual_entries = PayslipManualEntry.search([
                        ('payslip', '=', payslip),
                        ('entry', '=', entry)
                    ])
                    if operation == 'remove':
                        if entry in payslip.deduction_entries:
                            payslip_manual_entries[0].was_considered = False
                            to_save.append(payslip_manual_entries[0])
                    elif operation == 'add':
                        if len(payslip_manual_entries) > 0:
                            payslip_manual_entries[0].was_considered = True
                            to_save.append(payslip_manual_entries[0])
                        elif entry not in payslip.deduction_entries:
                            payslip_manual_entry = PayslipManualEntry()
                            payslip_manual_entry.payslip = payslip
                            payslip_manual_entry.entry = entry
                            payslip_manual_entry.kind = entry.kind
                            payslip_manual_entry.was_considered = True
                            to_save.append(payslip_manual_entry)
        PayslipManualEntry.save(to_save)

    @classmethod
    def set_deduction_loans(cls, payslips, name, value):
        pool = Pool()
        EmployeeLoanLine = pool.get('company.employee.loan.line')
        PayslipDeductionLoanLine = pool.get(
            'payslip.payslip.deduction.loan.line')
        to_save = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    loan = EmployeeLoanLine.search([('id', '=', id)])[0]
                    payslip_deduction_loans = PayslipDeductionLoanLine.search([
                        ('payslip', '=', payslip),
                        ('loan_line', '=', loan)
                    ])
                    if operation == 'remove':
                        if loan in payslip.deduction_loans:
                            payslip_deduction_loans[0].was_considered = False
                            to_save.append(payslip_deduction_loans[0])
                    elif operation == 'add':
                        if len(payslip_deduction_loans) > 0:
                            payslip_deduction_loans[0].was_considered = True
                            to_save.append(payslip_deduction_loans[0])
                        elif loan not in payslip.deduction_loans:
                            payslip_deduction_loan = PayslipDeductionLoanLine()
                            payslip_deduction_loan.payslip = payslip
                            payslip_deduction_loan.loan_line = loan
                            payslip_deduction_loan.was_considered = True
                            to_save.append(payslip_deduction_loan)
        PayslipDeductionLoanLine.save(to_save)

    @classmethod
    def set_fixed_deductions(cls, payslips, name, value):
        pool = Pool()
        PayslipFixedIncomeDeduction = pool.get('payslip.fixed.income.deduction')
        to_save = []
        to_delete = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                ids = v[1]
                for id in ids:
                    fixed = PayslipFixedIncomeDeduction.search([
                        ('id', '=', id)]
                    )[0]
                    if operation == 'remove':
                        if fixed in payslip.fixed_deductions:
                            to_delete.append(fixed)
                    elif operation == 'add':
                        to_save.append(fixed)
        PayslipFixedIncomeDeduction.delete(to_delete)
        PayslipFixedIncomeDeduction.save(to_save)

    @classmethod
    def set_medical_certificates_discount(cls, payslips, name, value):
        pool = Pool()
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')
        to_save = []
        to_delete = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                if operation == "remove":
                    # Form: v = ['remove', [id1, id2, id3]]
                    ids = v[1]
                    for id in ids:
                        med = PayslipMedicalCertificateDiscount.search([
                            ('id', '=', id)
                        ])[0]
                        if med in payslip.medical_certificates_discount:
                            to_delete.append(med)
                elif operation == "add":
                    # Form: v = ['add', [id1, id2, id3]]
                    ids = v[1]
                    for id in ids:
                        med = PayslipMedicalCertificateDiscount.search([
                            ('id', '=', id)
                        ])[0]
                        if med in payslip.medical_certificates_discount:
                            to_save.append(med)
                elif operation == "write":
                    # Form: v=['write',[id1],{changes id1},[id2],{changes id2}]
                    changes = v
                    i = 1
                    while i < (len(changes) - 1):
                        id = changes[i][0]
                        attrs = changes[i + 1]
                        med = PayslipMedicalCertificateDiscount.search([
                            ('id', '=', id)
                        ])[0]
                        for key, value in attrs.items():
                            setattr(med, key, value)
                        to_save.append(med)
                        i += 2
        PayslipMedicalCertificateDiscount.delete(to_delete)
        PayslipMedicalCertificateDiscount.save(to_save)

    @classmethod
    def set_payslip_workshift_dialings(cls, payslips, name, value):
        pool = Pool()
        PayslipWorkshiftDialing = pool.get('payslip.workshift.dialing')
        to_save = []
        to_delete = []
        for payslip in payslips:
            for v in value:
                operation = v[0]
                if operation == "remove":
                    # Form: v = ['remove', [id1, id2, id3]]
                    ids = v[1]
                    for id in ids:
                        dialing = PayslipWorkshiftDialing.search([
                            ('id', '=', id)
                        ])[0]
                        if dialing in payslip.workshift_dialings:
                            to_delete.append(dialing)
                elif operation == "add":
                    # Form: v = ['add', [id1, id2, id3]]
                    ids = v[1]
                    for id in ids:
                        dialing = PayslipWorkshiftDialing.search([
                            ('id', '=', id)
                        ])[0]
                        if dialing in payslip.workshift_dialings:
                            to_save.append(dialing)
                elif operation == "write":
                    # Form: v=['write',[id1],{changes id1},[id2],{changes id2}]
                    changes = v
                    i = 1
                    while i < (len(changes) - 1):
                        id = changes[i][0]
                        attrs = changes[i + 1]
                        dialing = PayslipWorkshiftDialing.search([
                            ('id', '=', id)
                        ])[0]
                        for key, value in attrs.items():
                            setattr(dialing, key, value)
                        to_save.append(dialing)
                        i += 2
        PayslipWorkshiftDialing.delete(to_delete)
        PayslipWorkshiftDialing.save(to_save)

    @classmethod
    def manage_payslip_comm_subs_details(cls, payslip, comm_subs_details):
        pool = Pool()
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')

        # Create or update considered payslips comm subs details
        to_save = []
        for detail in comm_subs_details:
            payslip_comm_subs_details = PayslipCommSubDetail.search([
                ('comm_sub_detail', '=', detail)
            ])
            if payslip_comm_subs_details:
                payslip_comm_subs_details[0].payslip = payslip
                payslip_comm_subs_details[0].was_considered = True
                to_save.append(payslip_comm_subs_details[0])
            else:
                payslip_comm_subs_detail = PayslipCommSubDetail()
                payslip_comm_subs_detail.payslip = payslip
                payslip_comm_subs_detail.comm_sub_detail = detail
                payslip_comm_subs_detail.was_considered = True
                to_save.append(payslip_comm_subs_detail)

        # Set payslip comm sub details to not considered
        payslip_comm_subs_details = PayslipCommSubDetail.search([
            ('payslip', '=', payslip)
        ])
        for payslip_comm_subs_detail in payslip_comm_subs_details:
            if payslip_comm_subs_detail not in to_save:
                payslip_comm_subs_detail.was_considered = False
                to_save.append(payslip_comm_subs_detail)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_manual_entries(cls, payslip, income_entries,
            deduction_entries, considered_entries_codes):
        pool = Pool()
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        Entry = pool.get('payslip.entry')

        list_entries = []
        if income_entries:
            list_entries += income_entries
        if deduction_entries:
            list_entries += deduction_entries

        # Create or update considered payslips manual entries
        to_save = []
        for entry in Entry.browse(list_entries):
            if entry.type_.code in considered_entries_codes:
                payslip_manual_entries = PayslipManualEntry.search([
                    ('entry', '=', entry)
                ])
                if payslip_manual_entries:
                    payslip_manual_entries[0].payslip = payslip
                    payslip_manual_entries[0].was_considered = True
                    to_save.append(payslip_manual_entries[0])
                else:
                    payslip_manual_entries = PayslipManualEntry()
                    payslip_manual_entries.payslip = payslip
                    payslip_manual_entries.entry = entry
                    payslip_manual_entries.kind = entry.kind
                    payslip_manual_entries.was_considered = True
                    to_save.append(payslip_manual_entries)

        # Set payslip deduction entries to not considered
        payslip_manual_entries = PayslipManualEntry.search([
            ('payslip', '=', payslip)
        ])
        for payslip_manual_entry in payslip_manual_entries:
            if payslip_manual_entry not in to_save:
                payslip_manual_entry.was_considered = False
                to_save.append(payslip_manual_entry)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_deduction_loans(cls, payslip, loans):
        pool = Pool()
        PayslipDeductionLoanLine = pool.get(
            'payslip.payslip.deduction.loan.line')

        # Create or update considered payslips deduction loan lines
        to_save = []
        for loan in loans:
            payslip_deduction_loans = PayslipDeductionLoanLine.search([
                ('loan_line', '=', loan)
            ])
            if payslip_deduction_loans:
                payslip_deduction_loans[0].payslip = payslip
                payslip_deduction_loans[0].was_considered = True
                to_save.append(payslip_deduction_loans[0])
            else:
                payslip_deduction_loan = PayslipDeductionLoanLine()
                payslip_deduction_loan.payslip = payslip
                payslip_deduction_loan.loan_line = loan
                payslip_deduction_loan.was_considered = True
                to_save.append(payslip_deduction_loan)

        # Set payslip deduction loans to not considered
        payslip_deduction_loans = PayslipDeductionLoanLine.search([
            ('payslip', '=', payslip)
        ])
        for payslip_deduction_loan in payslip_deduction_loans:
            if payslip_deduction_loan not in to_save:
                payslip_deduction_loan.was_considered = False
                to_save.append(payslip_deduction_loan)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_overtime_lines(cls, payslip, overtime_lines):
        pool = Pool()
        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')

        # Create or update considered payslips overtime lines
        to_save = []
        for line in overtime_lines:
            payslip_overtime_lines = PayslipOvertimeLine.search([
                ('overtime_line', '=', line)
            ])
            if payslip_overtime_lines:
                payslip_overtime_lines[0].payslip = payslip
                payslip_overtime_lines[0].was_considered = True
                to_save.append(payslip_overtime_lines[0])
            else:
                payslip_overtime_line = PayslipOvertimeLine()
                payslip_overtime_line.payslip = payslip
                payslip_overtime_line.overtime_line = line
                payslip_overtime_line.was_considered = True
                to_save.append(payslip_overtime_line)

        # Set payslip overtime lines to not considered
        payslip_overtime_lines = PayslipOvertimeLine.search([
            ('payslip', '=', payslip)
        ])
        for payslip_overtime_line in payslip_overtime_lines:
            if payslip_overtime_line not in to_save:
                payslip_overtime_line.was_considered = False
                to_save.append(payslip_overtime_line)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_fixed_incomes_deductions(cls, payslip, fixed_incomes,
            fixed_deductions, considered_fixed_values_codes):
        pool = Pool()
        FixedIncome = pool.get('company.contract.fixed.income')
        FixedDeduction = pool.get('company.contract.fixed.deduction')

        to_save, to_delete = [], []
        to_delete += payslip.fixed_incomes
        for fixed_income in FixedIncome.browse(fixed_incomes):
            fixed_type_code = fixed_income.type_income_deduction.code
            if fixed_type_code in considered_fixed_values_codes:
                p_fixed = cls.get_new_payslip_fixed_income_deduction(
                    payslip, fixed_income)
                to_save.append(p_fixed)

        to_delete += payslip.fixed_deductions
        for fixed_deduction in FixedDeduction.browse(fixed_deductions):
            fixed_type_code = fixed_deduction.type_income_deduction.code
            if fixed_type_code in considered_fixed_values_codes:
                p_fixed = cls.get_new_payslip_fixed_income_deduction(
                    payslip, fixed_deduction)
                to_save.append(p_fixed)
        return {
            'to_delete': to_delete,
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_absences(cls, payslip, absences):
        pool = Pool()
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')

        # Create or update considered payslips absences
        to_save = []
        for absence in absences:
            payslip_absences = PayslipAbsence.search([
                ('absence', '=', absence)
            ])
            if payslip_absences:
                payslip_absences[0].payslip = payslip
                payslip_absences[0].was_considered = True
                to_save.append(payslip_absences[0])
            else:
                payslip_absence = PayslipAbsence()
                payslip_absence.payslip = payslip
                payslip_absence.absence = absence
                payslip_absence.was_considered = True
                to_save.append(payslip_absence)

        # Set payslip absences to not considered
        payslip_absences = PayslipAbsence.search([
            ('payslip', '=', payslip)
        ])
        for payslip_absence in payslip_absences:
            if payslip_absence not in to_save:
                payslip_absence.was_considered = False
                to_save.append(payslip_absence)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_permissions(cls, payslip, permissions):
        pool = Pool()
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')

        # Create or update considered payslips permissions
        to_save = []
        for permission in permissions:
            payslip_permissions = PayslipPermission.search([
                ('permission', '=', permission)
            ])
            if payslip_permissions:
                payslip_permissions[0].payslip = payslip
                payslip_permissions[0].was_considered = True
                to_save.append(payslip_permissions[0])
            else:
                payslip_permission = PayslipPermission()
                payslip_permission.payslip = payslip
                payslip_permission.permission = permission
                payslip_permission.was_considered = True
                to_save.append(payslip_permission)

        # Set payslip permissions to not considered
        payslip_permissions = PayslipPermission.search([
            ('payslip', '=', payslip)
        ])
        for payslip_permission in payslip_permissions:
            if payslip_permission not in to_save:
                payslip_permission.was_considered = False
                to_save.append(payslip_permission)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_permissions_license(cls, payslip, permissions):
        pool = Pool()
        PayslipPermissionLicense = pool.get(
            'payslip.payslip.hr_ec.permission.license')

        # Create or update considered payslips permissions license
        to_save = []
        preserve_payslip_permissions = []
        for permission in permissions:
            payslip_permissions = PayslipPermissionLicense.search([
                ('permission', '=', permission)
            ])
            if not payslip_permissions:
                new_payslip_permission = PayslipPermissionLicense()
                new_payslip_permission.payslip = payslip
                new_payslip_permission.permission = permission
                new_payslip_permission.was_considered = True
                to_save.append(new_payslip_permission)
            else:
                payslip_permission = payslip_permissions[0]
                if payslip_permission.payslip != payslip:
                    new_payslip_permission = PayslipPermissionLicense()
                    new_payslip_permission.payslip = payslip
                    new_payslip_permission.permission = permission
                    new_payslip_permission.was_considered = True
                    to_save.append(new_payslip_permission)
                else:
                    if not payslip_permission.was_considered:
                        payslip_permission.was_considered = True
                        to_save.append(payslip_permission)
                    else:
                        preserve_payslip_permissions.append(payslip_permission)

        # Set payslip permissions to not considered
        payslip_permissions = PayslipPermissionLicense.search([
            ('payslip', '=', payslip)
        ])
        for payslip_permission in payslip_permissions:
            if payslip_permission not in to_save:
                if payslip_permission not in preserve_payslip_permissions:
                    payslip_permission.was_considered = False
                    to_save.append(payslip_permission)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_workshift_delay_lines(cls, payslip, delay_lines):
        pool = Pool()
        PayslipWorkshiftDelayLine = pool.get(
            'payslip.payslip.hr_ec.workshift.delay.line')

        # Create or update considered payslips workshift delay lines
        to_save = []
        for delay_line in delay_lines:
            payslip_delay_lines = PayslipWorkshiftDelayLine.search([
                ('workshift_delay_line', '=', delay_line)
            ])
            if payslip_delay_lines:
                payslip_delay_lines[0].payslip = payslip
                payslip_delay_lines[0].was_considered = True
                to_save.append(payslip_delay_lines[0])
            else:
                payslip_delay_line = PayslipWorkshiftDelayLine()
                payslip_delay_line.payslip = payslip
                payslip_delay_line.workshift_delay_line = delay_line
                payslip_delay_line.was_considered = True
                to_save.append(payslip_delay_line)

        # Set payslip permissions to not considered
        payslip_delay_lines = PayslipWorkshiftDelayLine.search([
            ('payslip', '=', payslip)
        ])
        for payslip_delay_line in payslip_delay_lines:
            if payslip_delay_line not in to_save:
                payslip_delay_line.was_considered = False
                to_save.append(payslip_delay_line)
        return {
            'to_save': to_save
        }

    @classmethod
    def manage_payslip_workshift_dialings(cls, payslip, dialings_detail):
        pool = Pool()
        PayslipWorkshiftDialing = pool.get('payslip.workshift.dialing')

        # Delete previous workshift dialing lines
        workshift_dialings = PayslipWorkshiftDialing.search([
            ('payslip', '=', payslip)
        ])
        to_delete = workshift_dialings if workshift_dialings else []

        # Create considered payslips workshift dialing lines
        to_save = []
        for dialing in dialings_detail:
            workshift_dialing = (
                cls.get_new_payslip_workshift_dialing(
                    payslip,
                    payslip.employee,
                    dialing.date,
                    dialing.hour,
                    dialing.workshift,
                    dialing.nocturne_surcharge
                ))
            to_save.append(workshift_dialing)
        return {
            'to_save': to_save,
            'to_delete': to_delete
        }

    @classmethod
    def manage_payslip_medical_certificate_discounts(cls, payslip,
            medical_certificates_info):
        pool = Pool()
        PayslipMedicalCertificateDiscount = pool.get(
            'payslip.medical.certificate.discount')

        # Delete previous medical information discount lines
        med_certificate_discs = PayslipMedicalCertificateDiscount.search([
            ('payslip', '=', payslip)
        ])
        to_delete = med_certificate_discs if med_certificate_discs else []

        # Create considered payslips medical certificate discounts lines
        to_save = []
        for med_info in medical_certificates_info:
            med_certificate_discount = (
                cls.get_new_payslip_medical_certificate_discount(
                    payslip,
                    payslip.employee,
                    med_info['certificate_type_id'],
                    med_info['total_days'],
                    med_info['percentage'],
                    med_info['order_']
                ))
            to_save.append(med_certificate_discount)
        return {
            'to_save': to_save,
            'to_delete': to_delete
        }

    @classmethod
    def get_new_payslip_fixed_income_deduction(cls, payslip, fixed_i_d):
        pool = Pool()
        PayslipFixedIncomeDeduction = pool.get('payslip.fixed.income.deduction')
        p_fixed = PayslipFixedIncomeDeduction()
        p_fixed.payslip = payslip
        p_fixed.type_income_deduction = fixed_i_d.type_income_deduction
        p_fixed.reason = fixed_i_d.reason
        p_fixed.amount = fixed_i_d.amount
        return p_fixed

    @classmethod
    def get_new_law_benefit(cls, payslip, law_benefit_type, amount,
            law_benefit_kind=None, generator_payslip=None):
        LawBenefit = Pool().get('payslip.law.benefit')
        benefit = LawBenefit()
        benefit.company = payslip.company
        benefit.employee = payslip.employee
        benefit.period = payslip.period
        benefit.amount = amount
        benefit.type_ = law_benefit_type
        if generator_payslip:
            benefit.generator_payslip = generator_payslip
        else:
            benefit.generator_payslip = payslip
        if law_benefit_kind:
            benefit.kind = law_benefit_kind
        else:
            benefit.kind = payslip.get_law_benefit_payment_form_by_field(
                law_benefit_type.code)
        return benefit

    @classmethod
    def get_new_payslip_line(cls, payslip, rule, type_, amount):
        Line = Pool().get('payslip.line')
        pline = Line()
        pline.payslip = payslip
        pline.rule = rule
        pline.type_ = type_
        pline.amount = utils.quantize_currency(amount)
        return pline

    @classmethod
    def get_new_payslip_workshift_dialing(cls, payslip, employee, date, hour,
            workshift, nocturne_surcharge):
        PayslipWorkshiftDialing = Pool().get('payslip.workshift.dialing')
        workshift_dialing = PayslipWorkshiftDialing()
        workshift_dialing.payslip = payslip
        workshift_dialing.employee = employee
        workshift_dialing.date = date
        workshift_dialing.hour = hour
        workshift_dialing.workshift = workshift
        workshift_dialing.nocturne_surcharge = nocturne_surcharge
        return workshift_dialing

    @classmethod
    def get_new_payslip_medical_certificate_discount(cls, payslip, employee,
            certificate_type, total_days, percentage, order_):
        PayslipMedicalCertificateDiscount = Pool().get(
            'payslip.medical.certificate.discount')
        med_certificate_discount = PayslipMedicalCertificateDiscount()
        med_certificate_discount.payslip = payslip
        med_certificate_discount.employee = employee
        med_certificate_discount.certificate_type = certificate_type
        med_certificate_discount.number_days_month = total_days
        med_certificate_discount.percentage_payslip = percentage
        med_certificate_discount.order_ = order_
        return med_certificate_discount

    @classmethod
    def get_query_absences_overlaps_with_medical_certificates(cls):
        pool = Pool()
        Absence = pool.get('hr_ec.absence')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        absence = Absence.__table__()

        query_medical = absence.join(medical_certificate,
                condition=absence.employee == medical_certificate.employee
            ).select(
            absence.id,
            where=((absence.start_date >= medical_certificate.start_date) &
                   (absence.end_date <= medical_certificate.end_date) &
                   (absence.state == 'done') &
                   (medical_certificate.state == 'done') &
                   (medical_certificate.register_type == 'days')
                   )
        )
        return query_medical

    @classmethod
    def get_query_contracts_without_absences_apply(cls):
        pool = Pool()
        Contract = pool.get('company.contract')
        tbl_contract = Contract.__table__()
        query = tbl_contract.select(
            tbl_contract.id,
            where=(tbl_contract.absence_apply == False)
        )
        return query

    @classmethod
    def get_query_permissions_overlaps_with_medical_certificates(cls):
        pool = Pool()
        Permission = pool.get('hr_ec.permission')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        permission = Permission.__table__()

        query_medical = permission.join(medical_certificate,
                condition=permission.employee == medical_certificate.employee
            ).select(
            permission.id,
            where=((permission.start_date >= medical_certificate.start_date) &
                   (permission.end_date <= medical_certificate.end_date) &
                   (permission.state == 'done') &
                   (medical_certificate.state == 'done') &
                   (medical_certificate.register_type == 'days')
                   )
        )
        return query_medical

    @classmethod
    def get_reserve_funds_info(cls, payslips):
        pool = Pool()
        Contract = pool.get('company.contract')
        LawBenefit = pool.get('payslip.law.benefit')
        warning_message = ''
        to_evaluate_contracts = []
        to_save_contracts = []
        result = defaultdict(lambda: {
            'fr_have': False,
            'fr_first_time': False,
            'fr_days_to_pay': 0
        })
        period_days_info = get_default_period_days_info()
        period_days = period_days_info['value']
        cont = 1
        for payslip in payslips:
            contract = payslip.contract
            employee = payslip.employee
            period = payslip.period
            period_days = period_days if period_days else period.end_date.day

            # Get FR information from contracts dates
            info = cls.get_reserve_funds_info_from_contracts_dates(
                employee, period, period_days)

            # Get FR information from work experience
            # info = get_reserve_funds_info_from_work_experience(
            #     contract.employee.work_experiences, period, period_days)

            if info:
                if info['fr_have']:
                    if info['fr_first_time']:
                        # Si la evaluación de fondos de reserva dice que hay que
                        # pagar al empleado su primer FR, debemos cerciorarnos
                        # que dicho empleado no haya cobrado antes ningun FR,
                        # por que en ese caso, no sería la primera vez que
                        # cobra. Eso significa que el empleado pudo venir de
                        # otro trabajo del sector público en el que percibía
                        # FRs, y cuando llegó a la empresa o GAD, se le continuó
                        # pagando.
                        law_benefits = LawBenefit.search([
                            ('type_.code', '=', 'fr'),
                            ('employee', '=', employee.id),
                            ('period.end_date', '<=', period.end_date),
                            ('generator_payslip.state', 'in', ['done']),
                        ])
                        if len(law_benefits) > 0:
                            info = {
                                'fr_have': True,
                                'fr_first_time': False,
                                'fr_days_to_pay': period_days
                            }
                    if not contract.have_fr:
                        # La evaluación de FR dice que hay que pagarle, pero en
                        # el contrato el check de FR está desactivado Por
                        # defecto, el sistema no debería pagarle (haciendo caso
                        # a lo que se indica en el contrato). Si se desea
                        # pagarle, el usuario debería activar el check de FR
                        # manualmente.
                        to_evaluate_contracts.append(contract)
                        warning_message += (
                            f"\n{cont}) [{contract.employee.rec_name} "
                            f"(Días: {info['fr_days_to_pay']})]")
                        cont += 1
                else:
                    # Si la evaluación de fondos de reserva dice que al empleado
                    # aún no hay que pagarle FR, pero en el contrato del
                    # empleado está especificado que si hay que pagarle
                    # (posiblemente por una carga previa del archivo del IESS),
                    # entonces lo que prima es la información especificada en el
                    # contrato.
                    if contract.have_fr:
                        info = {
                            'fr_have': True,
                            'fr_first_time': False,
                            'fr_days_to_pay': period_days
                        }
                result[payslip.id] = info
        if len(warning_message) > 0:
            cls.raise_user_warning('warning_fr_info', 'warning_fr_info', {
                'message': warning_message
            })
            # The check of reserve funds in each contract is activated
            for contract in to_evaluate_contracts:
                contract.have_fr = True
                contract.fr = 'monthlyse'
                to_save_contracts.append(contract)
            Contract.save(to_save_contracts)
        return result

    @classmethod
    def get_reserve_funds_info_evaluation(cls, intervals, period, period_days):
        info = None
        total_days = 0
        for interval in intervals:
            total_days += pendulum.period(interval['start_date'],
                interval['end_date']).days

        if total_days > 365:
            # When the current year is a leap year, it is necessary subtract 1
            # more day because the leap years have 1 more day compared to the
            # other years
            is_leap_year = isleap(period.end_date.year)
            if is_leap_year:
                diff_days = total_days - 365 - 1
            else:
                diff_days = total_days - 365

            if 0 < diff_days <= period_days:
                # First time receiving FR
                if period.end_date.month == 2:
                    last_day = monthrange(period.end_date.year,
                        period.end_date.month)[1]
                    days_to_pay = (
                        diff_days + (period_days - last_day))
                else:
                    if (len(intervals) == 1 and (
                            intervals[0]['start_date'].month ==
                                period.end_date.month) and (
                                    intervals[0]['start_date'].day == 30) and (
                                        period.end_date.day == 31)):
                        days_to_pay = 1
                    else:
                        days_to_pay = (
                            diff_days - (period.end_date.day - period_days))
                info = {
                    'fr_have': True,
                    'fr_first_time': True,
                    'fr_days_to_pay': days_to_pay
                }
            elif diff_days > period_days:
                # The employee has already collected their FR before
                info = {
                    'fr_have': True,
                    'fr_first_time': False,
                    'fr_days_to_pay': period_days
                }
            else:
                # TODO Confirm this
                # It means that in the payment roles with days of fixed periods
                # of 30 days, the employee meets 365 days of work on the 31st
                # of a specific month. Therefore, the employee is not paid FR
                info = {
                    'fr_have': False,
                    'fr_first_time': False,
                    'fr_days_to_pay': 0
                }
        else:
            # Based on work experience or based on contract dates, the employee
            # must not yet collect FR
            info = {
                'fr_have': False,
                'fr_first_time': False,
                'fr_days_to_pay': 0
            }
        return info

    @classmethod
    def get_reserve_funds_info_from_work_experience(cls, work_experiences,
            period, period_days):
        info = None
        intervals = []
        if work_experiences:
            # total_days = 0
            # TODO: review use of total_days
            for we in work_experiences:
                if we.type == 'public':
                    start_date = we.start_date
                    end_date = we.end_date
                    if end_date:
                        if end_date > period.end_date:
                            end_date = period.end_date
                    else:
                        end_date = period.end_date
                    intervals.append({
                        'start_date': start_date,
                        'end_date': end_date
                    })
            info = cls.get_reserve_funds_info_evaluation(intervals, period,
                period_days)
        else:
            info = {
                'fr_have': False,
                'fr_first_time': False,
                'fr_days_to_pay': 0
            }
        return info

    @classmethod
    def get_reserve_funds_info_from_contracts_dates(cls, employee, period,
            period_days):
        info = None
        intervals = []
        dates = cls.get_contracts_dates_for_reserve_funds_info(employee, period)
        if dates:
            for _date in dates:
                start_date = _date['start_date']
                end_date = _date['end_date']
                if end_date:
                    if end_date > period.end_date:
                        end_date = period.end_date
                else:
                    end_date = period.end_date
                intervals.append({
                    'start_date': start_date,
                    'end_date': end_date
                })
            info = cls.get_reserve_funds_info_evaluation(intervals, period,
                period_days)
        else:
            info = {
                'fr_have': False,
                'fr_first_time': False,
                'fr_days_to_pay': 0
            }
        return info

    @classmethod
    def get_contracts_dates_for_reserve_funds_info(cls, employee, period):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')

        tbl_contract = Contract.__table__()
        tbl_employee = Employee.__table__()

        dates = []

        query = tbl_employee.join(tbl_contract,
            condition=(tbl_contract.employee == tbl_employee.id)
        ).select(
            tbl_contract.id.as_('contract'),
            tbl_contract.start_date,
            Coalesce(tbl_contract.end_date, date.max).as_('end_date'),
            where=((tbl_employee.id == employee.id) &
                   (tbl_contract.state.in_(['done', 'disabled', 'finalized'])) &
                   (tbl_contract.start_date <= period.end_date)),
            order_by=tbl_contract.start_date.desc
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            dates.append({
                'start_date': row['start_date'],
                'end_date': row['end_date'],
            })
        return dates

    @classmethod
    def get_manual_revision_color(cls, payslips, name=None):
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        result = {}

        ids = [p.id for p in payslips]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.select(
                table.id,
                Case(
                    (table.manual_revision == 't', 'lightcoral'),
                    else_='lightgreen'),
                where=red_sql,
            )
            cursor.execute(*query)
            result.update(dict(cursor.fetchall()))
        return result

    def get_rec_name(self, name):
        return 'ROL %s: %s - %s' % (
            self.period.rec_name, self.template.rec_name,
            self.employee.party.name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('employee',) + tuple(clause[1:]),
            ('contract',) + tuple(clause[1:]),
            ('period',) + tuple(clause[1:]),
            ('template',) + tuple(clause[1:]),
            ]
        return domain


class PayslipManualEntry(ModelSQL, ModelView):
    'Intermediate class between Payslip and Manual Entries'
    __name__ = 'payslip.payslip.manual.entry'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    entry = fields.Many2One('payslip.entry', 'Entrada manual', required=True)
    kind = fields.Selection(_KIND, 'Tipo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipDeductionLoanLine(ModelSQL, ModelView):
    'Intermediate class between Payslip and EmployeeLoanLine'
    __name__ = 'payslip.payslip.deduction.loan.line'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    loan_line = fields.Many2One('company.employee.loan.line',
        'Línea de préstamo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipOvertimeLine(ModelSQL, ModelView):
    'Intermediate class between Payslip and OvertimeLine'
    __name__ = 'payslip.payslip.overtime.line'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    overtime_line = fields.Many2One('hr_ec.overtime.line',
        'Línea de horas extra', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipCommSubDetail(ModelSQL, ModelView):
    'Intermediate class between Payslip and CommissionSubrogationDetail'
    __name__ = 'payslip.payslip.commission.subrogation.detail'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    comm_sub_detail = fields.Many2One('company.commission.subrogation.detail',
        'Detalle de encargo/subrogación', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipAbsence(ModelSQL, ModelView):
    'Intermediate class between Payslip and Absence'
    __name__ = 'payslip.payslip.hr_ec.absence'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    absence = fields.Many2One('hr_ec.absence', 'Falta', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipPermission(ModelSQL, ModelView):
    'Intermediate class between Payslip and Permissions apply to payslips'
    __name__ = 'payslip.payslip.hr_ec.permission'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    permission = fields.Many2One('hr_ec.permission', 'Permiso', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipPermissionLicense(ModelSQL, ModelView):
    'Intermediate class between Payslip and Permissions apply to license'
    __name__ = 'payslip.payslip.hr_ec.permission.license'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    permission = fields.Many2One('hr_ec.permission', 'Permiso', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipWorkshiftDelayLine(ModelSQL, ModelView):
    'Intermediate class between Payslip and Workshift Delay Line'
    __name__ = 'payslip.payslip.hr_ec.workshift.delay.line'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos', required=True,
        ondelete='CASCADE')
    workshift_delay_line = fields.Many2One('hr_ec.workshift.delay.line',
        'Linea de registro de atrasos', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class PayslipLine(sequence_ordered(), ModelSQL, ModelView):
    'Payslip Line'
    __name__ = 'payslip.line'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos',
        required=True, ondelete='CASCADE',
        states={
            'readonly': True
        })
    rule = fields.Many2One('payslip.rule', 'Regla', required=True,
        domain=[
            ('company', '=', Eval('_parent_payslip', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        })
    type_ = fields.Selection(
        _PAYSLIP_ITEM_TYPES, 'Tipo', required=True, sort=False,
        states={
            'readonly': True
        })
    type_translated = type_.translated('type_')
    amount = fields.Numeric('Valor', required=True, digits=(16, 2),
        states={
            'readonly': True
        })
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    receivable_account = fields.Many2One('account.account', 'Cuenta por cobrar')
    revenue_account = fields.Many2One('account.account', 'Cuenta de ingreso')
    budget = fields.Many2One('public.budget', 'Partida')
    # Some extra fields of payslips for reports
    payslip_template = fields.Function(fields.Many2One('payslip.template',
        'Plantilla de rol'), 'get_payslip_field',
        searcher='search_payslip_field')
    payslip_employee = fields.Function(fields.Many2One('company.employee',
        'Empleado'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_contract = fields.Function(fields.Many2One('company.contract',
        'Contrato'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_department = fields.Function(fields.Many2One('company.department',
        'Departamento'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_fiscalyear = fields.Function(fields.Many2One('account.fiscalyear',
        'Año fiscal'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_period = fields.Function(fields.Many2One('account.period',
        'Periodo'), 'get_payslip_field', searcher='search_payslip_field')
    payslip_start_date = fields.Function(fields.Date('Fecha de inicio'),
        'get_payslip_field', searcher='search_payslip_field')
    payslip_end_date = fields.Function(fields.Date('Fecha de fin'),
        'get_payslip_field', searcher='search_payslip_field')
    payslip_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('confirm', 'Confirmado'),
            ('done', 'Realizado'),
            ('cancel', 'Cancelado'),
        ], 'Estado de rol'), 'get_payslip_field',
        searcher='search_payslip_field')
    payslip_state_translated = payslip_state.translated('payslip_state')
    account_crossing = fields.Boolean('Cruce de cuentas',
        help=('Marcar en caso de que la cuenta de débito deba cruzarse con la'
            'del crédito.\n  Ejemplo: anticipos'))

    @classmethod
    def __setup__(cls):
        super(PayslipLine, cls).__setup__()
        cls.order_payslip_template = cls._order_payslip_field('template')
        cls.order_payslip_contract = cls._order_payslip_field('contract')
        cls.order_payslip_department = cls._order_payslip_field('department')
        cls.order_payslip_fiscalyear = cls._order_payslip_field('fiscalyear')
        cls.order_payslip_period = cls._order_payslip_field('period')
        cls.order_payslip_start_date = cls._order_payslip_field('start_date')
        cls.order_payslip_end_date = cls._order_payslip_field('end_date')
        cls.order_payslip_state = cls._order_payslip_field('state')

    @staticmethod
    def default_amount():
        return ZERO

    def get_payslip_field(self, name):
        field = getattr(self.__class__, name)
        if name.startswith('payslip_'):
            name = name[8:]
        value = getattr(self.payslip, name)
        if isinstance(value, ModelSQL):
            if field._type == 'reference':
                return str(value)
            return value.id
        return value

    @classmethod
    def search_payslip_field(cls, name, clause):
        nested = clause[0].lstrip(name)
        if name.startswith('payslip_'):
            name = name[8:]
        return [('payslip.' + name + nested,) + tuple(clause[1:])]

    def _order_payslip_field(name):
        def order_field(tables):
            pool = Pool()
            Payslip = pool.get('payslip.payslip')
            field = Payslip._fields[name]
            table, _ = tables[None]
            payslip_tables = tables.get('payslip')
            if payslip_tables is None:
                payslip = Payslip.__table__()
                payslip_tables = {
                    None: (payslip, payslip.id == table.payslip),
                    }
                tables['payslip'] = payslip_tables
            return field.convert_order(name, payslip_tables, Payslip)
        return staticmethod(order_field)

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
                ('payslip_contract.rec_name',) + tuple(clause[1:]),
                ('payslip_department.rec_name',) + tuple(clause[1:]),
                ('payslip_period.rec_name',) + tuple(clause[1:]),
                ('payslip_template.rec_name',) + tuple(clause[1:]),
                ('rule',) + tuple(clause[1:]),
                ]


class CreateRulesStart(ModelView):
    'Create Rules Start'
    __name__ = 'payslip.create.rules.start'

    company = fields.Many2One('company.company', 'Empresa')
    templates = fields.Many2Many(
        'payslip.rule.template', None, None, 'Reglas', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class CreateRulesSucceed(ModelView):
    'Create Rules Succeed'
    __name__ = 'payslip.create.rules.succeed'


class CreateRules(Wizard):
    'Create Rules'
    __name__ = 'payslip.create.rules'

    start = StateView('payslip.create.rules.start',
        'hr_ec_payslip.create_rules_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'create_rules', 'tryton-ok'),
        ])
    create_rules = StateTransition()
    succeed = StateView('payslip.create.rules.succeed',
        'hr_ec_payslip.create_rules_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
            ])

    def transition_create_rules(self):
        pool = Pool()
        Template = pool.get('payslip.rule.template')
        template2rule = {}
        Template.create_rules(self.start.templates,
            self.start.company.id,
            template2rule=template2rule)
        return 'succeed'


class PayslipFixedIncomeDeduction(ModelSQL, ModelView):
    'Payslip Fixed Income Deduction'
    __name__ = 'payslip.fixed.income.deduction'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos',
        required=True, ondelete='CASCADE',
        states={
            'readonly': True
        })
    type_income_deduction = fields.Many2One(
        'company.contract.type.income.deduction', 'Tipo',
        states={
            'readonly': True,
            'required': True
        }, ondelete='RESTRICT')
    reason = fields.Char('Razón',
        states={
            'readonly': True,
            'required': True
        })
    amount = fields.Numeric('Valor', digits=(16, 2),
        domain=[
            ('amount', '>=', 0)
        ],
        states={
            'readonly': True,
            'required': True
        })


class PayslipDictCodes(ModelView, ModelSQL):
    'Payslip Dict Codes'
    __name__ = 'payslip.dict.codes'

    payslip = fields.Many2One('payslip.payslip', 'Rol individual',
        states={
            'readonly': True,
        }, ondelete='CASCADE')
    description = fields.Char('Descripción', readonly=True)

    @classmethod
    def __setup__(cls):
        super(PayslipDictCodes, cls).__setup__()

    @classmethod
    def __register__(cls, module_name):
        super(PayslipDictCodes, cls).__register__(module_name)
        TableHandler = backend.get('TableHandler')
        exist = TableHandler.table_exist(cls._table)
        if exist:
            # Se eliminan campos para cambiar estructura y poder obtimizar el
            # tiempo de ejecucion al crear los roles de forma general.
            table_dict_codes = cls.__table_handler__(module_name)

            if table_dict_codes.column_exist('code'):
                table_dict_codes.drop_column('code')

            if table_dict_codes.column_exist('type'):
                table_dict_codes.drop_column('type')

            if table_dict_codes.column_exist('value'):
                table_dict_codes.drop_column('value')

    @classmethod
    def get_dict_codes_instances(cls, payslip, dict):
        result = []
        if dict:
            for key, value in dict.items():
                dic_payslip_codes = []
                dic_payslip_codes.append(Null)
                dic_payslip_codes.append(0)
                dic_payslip_codes.append(Null)
                dic_payslip_codes.append(0)
                dic_payslip_codes.append(Null)

                # set payslip
                dic_payslip_codes.append(payslip.id)

                # set key
                dic_payslip_codes.append(key)

                # Set Value
                if isinstance(value, defaultdict):
                    dic_payslip_codes.append(cls.format_dictionaries(value))
                else:
                    dic_payslip_codes.append(str(value))

                # Set Type
                dic_payslip_codes.append(cls.format_type(dict[key]))

                result.append(dic_payslip_codes)
        return result

    @classmethod
    def format_type(cls, object):
        _str = (str(type(object)).replace("<class '", "")).replace("'>", "")
        return _str

    @classmethod
    def format_dictionaries(cls, dict):
        _str = ''
        for key, value in dict.items():
            _str = (f"{_str} '{key}': {value}, ")
        if len(_str) > 0:
            _str = _str[:-2]
        return f"{'{'}{_str}{'}'}"


class TemplatePayslipDictCodes(ModelView, ModelSQL):
    "Template - PayslipDictCodes"
    __name__ = 'template.payslip.dict.codes'

    payslip = fields.Many2One('payslip.payslip', 'Rol individual')

    code = fields.Char('Código', readonly=True)
    type = fields.Char('Tipo', readonly=True)
    value = fields.Char('Valor', readonly=True)

    @classmethod
    def __setup__(cls):
        super(TemplatePayslipDictCodes, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('type', 'ASC'))
        cls._order.insert(2, ('value', 'ASC'))

    @classmethod
    def table_query(cls):
        context = Transaction().context
        pool = Pool()
        PayslipDictCodes = pool.get('payslip.dict.codes')

        columns = ['id', 'create_uid', 'create_date', 'write_uid', 'write_date',
            'payslip', 'code', 'value', 'type']

        # values = [[1, 0, Null, 0, Null, 1179, 'code', 'value', 'tipo'],
        #           [2, 0, Null, 0, Null, 1180, 'code1', 'value2', 'tipo2'],
        #           [3, 0, Null, 0, Null, 1179, 'code3', 'value3', 'tipo3']]
        values = []
        cont = 0
        if context.get('payslip'):
            payslip_dict_codes = PayslipDictCodes.search([
                ('payslip', '=', context['payslip'])])

            for payslipcode in payslip_dict_codes:
                array = ast.literal_eval(payslipcode.description)
                for fila, info in enumerate(array):
                    array[fila][0] = cont
                    cont += 1
                values += array

        if len(values) == 0:
            values = [[0, 0, Null, 0, Null, None, '', '', '']]

        data = With()
        data.columns = columns
        data.query = Values(values)
        query = data.select(with_=[data], order_by=data.code)

        return query


class PayslipMedicalCertificateDiscount(ModelSQL, ModelView):
    'Payslip Medical Certificate Discount'
    __name__ = 'payslip.medical.certificate.discount'
    _history = True

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos',
        states={
            'required': True,
            'readonly': True
        }, ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Empleado', readonly=True)
    certificate_type = fields.Many2One(
        'galeno.medical.certificate.type', 'Tipo de Certificado', readonly=True)
    number_days_month = fields.Integer('Total días',
        domain=[
            ('number_days_month', '>=', 0)
        ])
    percentage_payslip = fields.Numeric('Porcentaje de pago (Empleador)',
        domain=[
            ('percentage_payslip', '>=', 0)
        ], digits=(16, 2))
    percentage_payslip_iess = fields.Function(fields.Numeric(
        'Porcentaje de pago (IESS)',
        domain=[
            ('percentage_payslip', '>=', 0)
        ], digits=(16, 2)), 'get_percentage_payslip_iess')
    order_ = fields.Integer('Orden', readonly=True)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('payslip',) + tuple(clause[1:]),
                  ('employee.party.name',) + tuple(clause[1:]),
                  ('certificate_type',) + tuple(clause[1:])
                  ]
        return domain

    @fields.depends('percentage_payslip')
    def on_change_with_percentage_payslip_iess(self, name=None):
        if self.percentage_payslip:
            return 1 - self.percentage_payslip
        return None

    @classmethod
    def get_percentage_payslip_iess(cls, items, names=None):
        result = defaultdict(lambda: None)
        for item in items:
            if item.percentage_payslip:
                result[item.id] = 1 - item.percentage_payslip
            else:
                result[item.id] = 1
        return {
            'percentage_payslip_iess': result
        }


class PayslipWorkshiftDialing(ModelSQL, ModelView):
    'Payslip Workshift Dialing'
    __name__ = 'payslip.workshift.dialing'
    _history = True

    _STATES = {
        'readonly': True
    }

    payslip = fields.Many2One('payslip.payslip', 'Rol de pagos',
        states={
            'required': True,
            'readonly': True
        }, ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Empleado', states=_STATES)
    date = fields.Date('Fecha', states=_STATES)
    hour = fields.Time('Hora', states=_STATES)
    workshift = fields.Many2One('company.workshift', 'Horario', states=_STATES)
    nocturne_surcharge = fields.Time('Recargo Nocturno', states=_STATES)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('payslip',) + tuple(clause[1:]),
                  ('employee.party.name',) + tuple(clause[1:]),
                  ('date',) + tuple(clause[1:])
                  ]
        return domain
