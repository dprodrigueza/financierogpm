{% for employee, data in reserve_funds.items() %}\
${company_ruc};\
${branch_office_code};\
${current_year};\
${current_month};\
${movement_type};\
${employee};\
${data['amount']};\
${period_start} A ${period_end};\
${data['months']}{% if type == 'monthlyse' %};${period_type}{% end %}${'\r'}
{% end %}