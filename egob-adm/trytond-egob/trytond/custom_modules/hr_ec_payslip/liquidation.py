from calendar import monthrange
from collections import defaultdict

import numpy as np
from datetime import timedelta, datetime

from dateutil.relativedelta import relativedelta
from sql import Null, Literal, Union
from sql.aggregate import Sum, Max
from sql.conditionals import Case, Coalesce
from sql.operators import NotIn
from trytond.model import (ModelSQL, ModelView, Workflow, fields, Unique,
    sequence_ordered)
from trytond.modules.hr_ec.loan import register_payment
from trytond.pool import Pool, PoolMeta
from decimal import Decimal
from trytond.pyson import Eval, If, Bool, Or
from trytond.transaction import Transaction
from . import evaluator
from trytond.config import config
from trytond.tools import reduce_ids, grouped_slice, cursor_dict
from trytond.modules.hr_ec_payslip import utils

__all__ = ['LiquidationTemplate', 'LiquidationTemplateLine', 'LiquidationRule',
    'ContractLiquidation', 'LiquidationOvertimeLine', 'LiquidationLoans',
    'LiquidationLawBenefitsXIII', 'LiquidationLawBenefitsXIV',
    'LiquidationCoomSub', 'LiquidationPermission', 'LiquidationAbsence',
    'LiquidationMedicalCertificate', 'LiquidationLine',
    'ContractLiquidationPayslipEntry', 'ContractLiquidationVacationLines',
    'ConfigurationRulePerceived']

STATES_ = {
    'readonly': Eval('state') != 'draft',
}

hr_digits = (16, config.getint('hr_ec', 'hr_decimal', default=4))

DICT_RULES_AMOUNT = defaultdict(lambda: 0)
NAMES = {}


def get_fields_values(model, ids, fields_names):
    fields_values = []
    if ids and fields_names:
        domain = [('id', 'in', ids)]
        fields_values = model.search_read(domain, fields_names=fields_names)
    return fields_values


def get_single_field_value(model, id, field_name):
    value = None
    if id and field_name:
        domain = [('id', '=', id)]
        fields_values = model.search_read(domain, fields_names=[field_name])
        if fields_values:
            value = fields_values[0][field_name]
    return value


def get_considered_in_liquidations(model, items, search_field, states=[]):
    search_field = str(search_field)
    result = defaultdict(lambda: None)
    for item in items:
        recovered_records = model.search([
            (search_field, '=', item),
            ('was_considered', '=', True),
            ('liquidation.state', 'in', states)
        ])
        if recovered_records:
            result[item.id] = recovered_records[0].liquidation.id
    return result


def get_default_period_days_info():
    SeveraConfiguratios = Pool().get('payslip.several.configurations')
    several_configuration = SeveraConfiguratios.search([])[0]
    if several_configuration.consider_periods:
        custom_period_days = {
            'type': 'fixed',
            'value': int(several_configuration.days_period)
        }
    else:
        custom_period_days = {
            'type': 'not_fixed',
            'value': None
        }
    if several_configuration.consider_finalized_contracts_during_payslip_period:
        custom_period_days['consider_finalized_contract'] = True
    else:
        custom_period_days['consider_finalized_contract'] = False

    return custom_period_days


def normalize_string(s):
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ü", "u"),
        ("Á", "A"),
        ("É", "E"),
        ("Í", "I"),
        ("Ó", "O"),
        ("Ú", "U"),
        ("ü", "u"),
        ("Ü", "U"),
        ("ñ", "n"),
        ("Ñ", "N"),
    )
    for a, b in replacements:
        s = s.replace(a, b)
    return s


class LiquidationTemplate(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.template'
    __doc__ = 'Liquidation Template'

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        }, required=True)
    code = fields.Char('Código', required=True)
    name = fields.Char('Nombre', required=True)
    liquidation_rules = fields.One2Many(
        'contract.liquidation.template.line', 'template', 'Reglas')

    @classmethod
    def __setup__(cls):
        super(LiquidationTemplate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('liquidation_temp_code_uniq', Unique(t, t.code), 'El código de la '
                'plantilla de liquidación debe ser único.'),
        ]
        cls._order.insert(0, ('name', 'ASC'))
        cls._order.insert(1, ('code', 'ASC'))

    @classmethod
    def copy(cls, templates, default=None):
        if default is None:
            default = {}
        default = default.copy()
        new_reports = []
        i = 1
        for report in templates:
            default['code'] = report.code + ' (DUPLICATED ' + str(i) + ')'
            default['name'] = report.name + ' (DUPLICATED ' + str(i) + ')'
            new_reports.extend(super(LiquidationTemplate, cls).copy([report],
                default=default))
            i += 1
        return new_reports

    @fields.depends('name')
    def on_change_with_code(self, name=None):
        if self.name:
            code = normalize_string(self.name).lower().replace(' ', '_')
            return code
        else:
            return None

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('name', 'code', 'liquidation_rules')
    def on_change_name(self, name=None):
        pool = Pool()
        LiquidationRule = Pool().get('contract.liquidation.rule')
        LiquidationTemplateLine = pool.get('contract.liquidation.template.line')
        liquidations_rules = LiquidationRule.search([()])
        if self.name and not self.liquidation_rules:
            for rule in liquidations_rules:
                if rule.code == 'rmu':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 10
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'overtimes':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 20
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'xiii':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 30
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'xiv':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 40
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'fr':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 50
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'fr_accumulate':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 60
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif (rule.code == 'prestamos' or rule.code == 'loans'):
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 70
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif (rule.code == 'anticipos'):
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 80
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif (rule.code == 'quincena_anticipos'):
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 90
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'comms_subs':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 100
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'eviction':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 150
                    self.liquidation_rules = self.liquidation_rules + (line,)
                elif rule.code == 'vacations':
                    line = LiquidationTemplateLine()
                    line.rule = rule
                    line.sequence = 160
                    self.liquidation_rules = self.liquidation_rules + (line,)

    @classmethod
    def validate(cls, templates):
        super(LiquidationTemplate, cls).validate(templates)


class LiquidationTemplateLine(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.template.line'

    template = fields.Many2One(
        'contract.liquidation.template', 'Plantilla liquidación', required=True,
        ondelete='CASCADE')
    rule = fields.Many2One('contract.liquidation.rule', 'Reglas liquidación',
        required=True,)


class LiquidationRule(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.rule'

    associated_rule = fields.Many2One('payslip.rule', 'Regla Asociada',
        states={
            'required': True,
        })
    is_manual_entry = fields.Boolean("¿Es una entrada manual?",
        states={
            'readonly': False,
        })
    condition = fields.Text('Condición',
        states={
            'required': True,
        })
    expression = fields.Text('Expresión',
        states={
            'required': True,
        })
    notes = fields.Text('Notas',
        states={
            'readonly': False,
        })
    # FUNTIONS CAMPS
    is_law_benefit = fields.Function(fields.Boolean(
        '¿Es un beneficio de ley (XIII, XIV, FR)?'),
        'on_change_with_is_law_benefit')
    code = fields.Function(fields.Char('Código', required=True,
        states={
            'readonly': True,
        }), 'on_change_with_code')
    name = fields.Function(fields.Char('Nombre', required=True,
        states={
            'readonly': True,
        }), 'on_change_with_name')
    abbreviation = fields.Function(fields.Char('Abreviatura',
        states={
            'readonly': True,
        }), 'on_change_with_abbreviation')
    type_ = fields.Function(fields.Selection([
        ('income', 'Ingreso'),
        ('deduction', 'Deducción'),
        ('internal', 'Interno')
    ], 'Tipo de regla', required=True, sort=False), 'on_change_with_type_')
    entry_type = fields.Function(fields.Many2One('payslip.entry.type',
        'Tipo de entrada'), 'on_change_with_entry_type')
    visible_date = fields.Boolean('Fechas en cálculos del rubro?',
        help='Este check ocaciona que se visualicen y se manipulen los rangos '
             'de las fechas del calculo para este rubro.')

    @classmethod
    def __setup__(cls):
        super(LiquidationRule, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('rule_code_uniq', Unique(t, t.associated_rule),
             'Ya existe una regla de liquidación asociada a esa regla de rol!'),
        ]
        cls._order.insert(0, ('associated_rule.name', 'ASC'))
        cls._order.insert(1, ('associated_rule.code', 'ASC'))

    @staticmethod
    def default_expression():
        exp = (f"result = {'{'}\n"
             f"    'value': 0\n"
             f"{'}'}\n"
             f"result"
            )
        return exp

    @staticmethod
    def default_visible_date():
        return False

    @fields.depends('entry_type', 'type_', 'condition', 'expression', 'code',
                    'name')
    def on_change_entry_type(self, names=None):
        if self.entry_type and self.entry_type.kind and self.entry_type.code:
            self.type_ = self.entry_type.kind
            self.code = self.entry_type.code
            self.name = self.entry_type.name
            self.condition = 'True'
            self.expression = (
                f"result = {'{'}\n"
                f"    'value': manual_entry_amounts['{self.entry_type.code}']\n"
                f"{'}'}\n"
                f"result"
            )
        else:
            self.code = None
            self.name = None
            self.entry_type = None
            self.condition = None
            self.expression = None

    @fields.depends('associated_rule')
    def on_change_with_is_law_benefit(self, name=None):
        if self.associated_rule:
            return self.associated_rule.is_law_benefit
        return False

    @fields.depends('associated_rule')
    def on_change_with_code(self, name=None):
        if self.associated_rule:
            return self.associated_rule.code
        return None

    @fields.depends('associated_rule')
    def on_change_with_name(self, name=None):
        if self.associated_rule:
            return self.associated_rule.name
        return None

    @fields.depends('associated_rule')
    def on_change_with_abbreviation(self, name=None):
        if self.associated_rule:
            return self.associated_rule.abbreviation
        return None

    @fields.depends('associated_rule')
    def on_change_with_type_(self, name=None):
        if self.associated_rule:
            return self.associated_rule.type_
        return None

    @fields.depends('associated_rule')
    def on_change_with_entry_type(self, name=None):
        if self.associated_rule and self.associated_rule.entry_type:
            return self.associated_rule.entry_type.id
        return None

    @classmethod
    def copy(cls, rules, default=None):
        if default is None:
            default = {}
        default = default.copy()
        new_rules = []
        i = 1
        for rule in rules:
            default['code'] = rule.code + ' (DUPLICATED ' + str(i) + ')'
            default['name'] = rule.name + ' (DUPLICATED ' + str(i) + ')'
            new_rules.extend(super(LiquidationRule, cls).copy(
                [rule], default=default))
            i += 1
        return new_rules

    @staticmethod
    def default_condition():
        return 'True'

    def get_rec_name(self, name):
        return "%s (%s)" % (self.associated_rule.name,
                            self.associated_rule.code)

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('associated_rule.code',) + tuple(clause[1:]),
                  ('associated_rule.name',) + tuple(clause[1:]),
                  ]
        return domain


class ContractLiquidation(metaclass=PoolMeta):
    __name__ = 'contract.liquidation'

    company = fields.Many2One('company.company', 'Empresa',
          domain=[
              ('id',
               If(Eval('context', {}).contains('company'),
                  '=', '!='),
               Eval('context', {}).get('company', -1)),
          ], required=True, states=STATES_)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
         domain=[
             ('company', '=', Eval('company')),
         ], states=STATES_,
         depends=['company'], required=True)
    contract_finalized = fields.Function(fields.One2Many(
        'company.contract', None, 'Contratos finalizados',
        states={'invisible': True}), 'on_change_with_contract_finalized')
    contract = fields.Many2One(
        'company.contract', 'Contrato a liquidar', required=True,
        domain=[If(Eval('state') == 'draft',
                   ('id', 'in', Eval('contract_finalized'),),
                   (),
                   ),
                ], depends=['contract_finalized', 'state'], states=STATES_)
    template = fields.Many2One('contract.liquidation.template', 'Plantilla',
        domain=[
            ('company', '=', Eval('company')),
        ], states={
            'readonly': (STATES_['readonly'] | ~Eval('reason_liquidation'))
        }, depends=['company', 'reason_liquidation'], required=True)
    contract_position = fields.Function(
        fields.Many2One('company.position', 'Puesto de trabajo'),
        'on_change_with_contract_position')
    contract_start_date = fields.Function(
        fields.Date('Fecha inició'), 'on_change_with_contract_start_date')
    contract_end_date = fields.Function(fields.Date('Fecha terminó',
        states=STATES_), 'on_change_with_contract_end_date')
    contract_work_relationship = fields.Function(
        fields.Many2One('company.work_relationship', 'Relación laboral'),
        'on_change_with_contract_work_relationship')
    total_work_days = fields.Function(fields.Char(
        'Total días trabajados'), 'on_change_with_total_work_days')
    reason_finalize = fields.Function(fields.Many2One(
        'company.finalize.contract.reason', 'Razón finalización',
        help='Razones de finalización de contrato normado por el IESS'),
        'on_change_with_reason_finalize')
    reason_liquidation = fields.Many2One('contract.liquidation.reason',
        'Razón liquidación', states={
            'required': True,
            'readonly': STATES_['readonly'],
        }, help='Razones de liquidación normado por el ministerio de relaciones'
        ' laborales.')
    zone = fields.Selection([
        ('', ''),
        ('coast', 'Costa'),
        ('sierra', 'Sierra'),
        ('east', 'Oriente'),
        ('galapagos', 'Galapagos'),
    ], 'Región', required=False, states=STATES_)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelar'),
    ], 'Estado', readonly=True, required=True)
    state_translated = state.translated('state')
    lines = fields.One2Many('contract.liquidation.line', 'liquidation',
        'Líneas')
    income_lines = fields.One2Many(
        'contract.liquidation.line', 'liquidation', 'Ingresos',
        states={
            'readonly': True,
        }, filter=[('type_', '=', 'income')])
    deduction_lines = fields.One2Many(
        'contract.liquidation.line', 'liquidation', 'Deducciones',
        states={
            'readonly': True,
        }, filter=[('type_', '=', 'deduction')])
    internal_lines = fields.One2Many(
        'contract.liquidation.line', 'liquidation', 'Internos',
        states={
            'readonly': True,
        }, filter=[('type_', '=', 'internal')])
    total_income = fields.Numeric('Total ingresos', digits=(16, 2),
        states={'readonly': True})
    aux_total_income = fields.Numeric('Total ingresos para calculos',
        digits=(16, 2),
        states={
            'readonly': True,
            'invisible': False
        })
    total_deduction = fields.Numeric('Total deducciones', digits=(16, 2),
        states={'readonly': True})
    total_value = fields.Numeric('Total a pagar', digits=(16, 2),
        states={'readonly': True})
    medical_certificates_discount = fields.One2Many(
        'contract.liquidation.certificates.lines', 'liquidation',
        'Información de descuento por certificados médicos',
        states={
            'readonly': True
        })
    permissions = fields.One2Many('contract.liquidation.permission.lines',
        'liquidation', 'Permisos con cargo a rol', states={'readonly': True})
    absences = fields.One2Many(
        'contract.liquidation.absence.lines', 'liquidation', 'Faltas',
        states={'readonly': True})
    loans = fields.One2Many('contract.liquidation.loans.lines',
        'liquidation', 'Detalle de Préstamos/Anticipo',
        states={
            'readonly': True
        })
    loans_pending = fields.Function(fields.Numeric('Deuda pendiente',
        states={
            'invisible': ~Bool(Eval('loans_pending')),
        }), 'on_change_with_loans_pending')
    loans_pending_description = fields.Function(fields.Text('Pendiente',
        states={
            'invisible': ~Bool(Eval('loans_pending')),
        }), 'on_change_with_loans_pending_description')
    total_vacation_pending = fields.Function(fields.Numeric(
        'Vacaciones pendientes', digits=(16, 2),),
        'on_change_with_total_vacation_pending')
    law_benefits_xiii = fields.One2Many(
        'contract.liquidation.law.benefits.xiii.lines', 'liquidation', 'XIII',
        states={'readonly': True})
    xiii_proportional = fields.Numeric('Proporcional XIII',
        states={
            'readonly': True,
        })
    law_benefits_xiv = fields.One2Many(
        'contract.liquidation.law.benefits.xiv.lines', 'liquidation', 'XIV',
        states={'readonly': True})
    xiv_proportional = fields.Numeric('Proporcional XIV',
        states={
            'readonly': True,
        })
    fr_proportional = fields.Numeric('Proporcional FR mensualizado',
        states={
            'readonly': True,
        })
    adjustment_xiv = fields.Numeric('Ajuste XIV',
        states={
            'readonly': True,
        })
    overtimes = fields.One2Many('contract.liquidation.overtimes.line',
        'liquidation', 'Detalle de Horas Extras', states={'readonly': True})
    income_manual_entries = fields.One2Many(
        'contract.liquidation.payslip.entry', 'liquidation',
        'Entradas manuales', states={'readonly': True},
        filter=[('kind', '=', 'income')])
    deduction_manual_entries = fields.One2Many(
        'contract.liquidation.payslip.entry', 'liquidation',
        'Deducciones manuales', states={'readonly': True},
        filter=[('kind', '=', 'deduction')])
    period = fields.Function(fields.Many2One('account.period',
        'Periodo salida', states={'invisible': False}), 'on_change_with_period')
    comms_subs = fields.One2Many('contract.liquidation.commsub.lines',
        'liquidation', 'Encargos/Subrogaciones', states={'readonly': True})
    assets_charge = fields.Function(fields.One2Many('asset', None,
        'Bienes a cargo',
        states={
            'readonly': True
        }), 'on_change_with_assets_charge')
    amount_dates = fields.One2Many('contract.liquidation.vacation.lines',
        'liquidation', 'Valores por Año', states={'readonly': True})
    # EXTRAS
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': True,
        })
    position = fields.Many2One('company.position', 'Puesto',
        states={
            'readonly': True,
        })
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Relación laboral',
        states={
            'readonly': True,
        })
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol',
        states={
            'readonly': True,
        })
    done_date = fields.DateTime(
        'Pasó a realizado', states={
            'invisible': False,
            'readonly': True,
        })
    last_full_payslip = fields.Many2One('payslip.payslip',
        'Ultimo rol íntegro', states={
            'readonly': True,
        })
    move = fields.Many2One('account.move', 'Asiento de Liquidacion',
        readonly=True, domain=[('company', '=', Eval('company', -1)), ],
        depends=['company'])
    journal = fields.Many2One('account.journal', 'Diario',
        states={
            'invisible': ~Eval('state').in_(['done']),
        },
        depends=['state'])
    compromise = fields.Many2One('public.budget.compromise',
        'Compromiso Liquidacion',
        states={
            'invisible': ~Eval('state').in_(['done']),
            'readonly': Bool(Eval('budget_impact_direct')),
        }, ondelete='RESTRICT')
    budget_impact_direct = fields.Boolean('Impacto de Partida Directo',
        states={
            'invisible': (~Eval('state').in_(['done']))
        })
    move_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('posted', 'Contabilizado'),
        ], 'Estado Asiento Liquidación', states={
            'invisible': ~Eval('state').in_(['done']
            ),
        }), 'get_move_state',
        searcher='search_move_state')

    @classmethod
    def __setup__(cls):
        super(ContractLiquidation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('unique_contract', Unique(t, t.contract),
             'Ya existe una liquidación de haberes creada para este contrato!'),
        ]
        cls._error_messages.update({
            'no_delete': ('Solamente es posible eliminar registros que estén en'
                ' estado "Borrador".'),
            'aseets_charge': 'La liquidación no puede continuar el proceso '
                'porque el empleado %(employee)s aún tiene bienes  a cargo '
                'pendientes de entregar.',
            'posted_move': 'El asiento ya esta contabilizado.',
            'set_journal': 'Debe asignar el diario.',
            'set_compromise': 'Seleccione el compromiso o marque '
                'impacto directo.',
            'rules_without_budget': ('No se han encontrado configuraciones\n'
                'para las siguientes reglas:\n %(rules)s.'),
            'rule_without_configuration': ('Regla sin configuracion para rol:\n'
                '%(rule)s.'),
            'account_without_budget': ('Cuenta: %(account)s sin partida \n'
                'especifica para %(budget)s.'),
            'budget_not_exist': 'Partida no codificada %(description)s.',
            'negative_total_pay': 'No se puede continuar con el proceso porque '
                ' el %(employee)s tiene una liquidación negativa. por favor '
                'revisar mencionados valores.',
            'contract_without_template': ('No se encontró la plantilla contable'
                ' relacionada al contrato a liquidar.'),
        })
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'done'),
            ('confirmed', 'cancel'),
        ))

        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done', 'cancel']),
                'depends': ['state'],
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'done', 'cancel']),
                'depends': ['state'],
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel']),
                'depends': ['state'],
            },
            'cancel': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done']),
                'depends': ['state'],
            },
            'post': {
                'invisible': Or(Eval('state').in_(['confirmed', 'draft',
                    'cancel']), Eval('move_state').in_(['posted']),
                    ),
                'depends': ['state'],
            },
        })


    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_xiii_proportional():
        return 0

    @staticmethod
    def default_xiv_proportional():
        return 0

    @staticmethod
    def default_fr_proportional():
        return 0

    @staticmethod
    def default_adjustment_xiv():
        return 0

    @staticmethod
    def default_contract_finalized():
        FinalizedContract = Pool().get('company.finalize.contract')
        result = []
        finalized_contract = FinalizedContract.search([('state', '=', 'done')])
        for finalized in finalized_contract:
            result.append(finalized.contract.id)

        return result

    @staticmethod
    def default_total_income():
        return 0.0

    @staticmethod
    def default_aux_total_income():
        return 0.0

    @staticmethod
    def default_total_deduction():
        return 0.0

    @classmethod
    def default_total_value(cls):
        return 0

    @classmethod
    def search_move_state(cls, name, clause):
        return [('move.state',) + tuple(clause[1:])]

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @classmethod
    def get_move_state(cls, liquidations, name):
        ids = [l.id for l in liquidations]

        cursor = Transaction().connection.cursor()
        Move = Pool().get('account.move')
        move = Move.__table__()
        liquidation = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(liquidation.id, sub_ids)
            query = liquidation.join(move, 'LEFT',
                condition=(move.id == liquidation.move)
            ).select(
                liquidation.id,
                move.state,
                where=red_sql
            )
            cursor.execute(*query)

            result.update(dict(cursor.fetchall()))

        return result

    @fields.depends('contract')
    def on_change_with_period(self, name=None):
        Period = Pool().get('account.period')
        if self.contract:
            periods = Period.search([
                ('start_date', '<=', self.contract.end_date),
                ('end_date', '>=', self.contract.end_date)
            ])
            if periods:
                return periods[0].id
        return None

    @fields.depends('contract')
    def on_change_with_last_full_payslip(self, name=None):
        pool = Pool()
        Payslip = pool.get('payslip.payslip')
        if self.contract:
            TemplateToTransparencyExports = pool.get('transparency.exports.template')
            codes_templates = TemplateToTransparencyExports.get_code_templates()
            payslips = Payslip.search([
                ('contract', '=', self.contract),
                ('state', '=', 'done'),
                ('template.accumulated_law_benefits', '=', False),
                ('template.code', 'in', codes_templates),
            ], order=[('period.end_date', 'DESC')])

            if payslips:
                for payslip in payslips:
                    if ((payslip.worked_days + payslip.total_absences +
                            payslip.total_days_disease + payslip.total_permissions) >=  # noqa
                            payslip.period_days):
                        return payslip.id
        return None

    @fields.depends('contract')
    def on_change_with_reason_finalize(self, name=None):
        CompanyFinalizeContract = Pool().get('company.finalize.contract')
        if self.contract:

            contract_finalize, = CompanyFinalizeContract.search([
                ('contract', '=', self.contract),
            ])

            return contract_finalize.reason_finalize.id
        return None

    @fields.depends('contract')
    def on_change_with_reason_liquidation(self, name=None):
        CompanyFinalizeContract = Pool().get('company.finalize.contract')
        if self.contract:

            contract_finalize, = CompanyFinalizeContract.search([
                ('contract', '=', self.contract),
                ('state', '=', 'done'),
            ])
            if contract_finalize.reason_finalize.liquidation_reason:
                return contract_finalize.reason_finalize.liquidation_reason.id
        return None

    @fields.depends('loans')
    def on_change_with_loans_pending(self, name=None):
        if self.loans:
            capital_amount = Decimal('0.00')
            total_applied = Decimal('0.00')
            for loan_pending in self.loans:
                capital_amount += loan_pending.capital_amount
                total_applied += loan_pending.value_applied

            total = (capital_amount - total_applied).quantize(Decimal('0.00'))
            if total:
                return total

        return None

    @fields.depends('loans', 'contract')
    def on_change_with_loans_pending_description(self, name=None):
        if self.loans and self.contract:
            total_capital_amount = Decimal('0.00')
            total_applied = Decimal('0.00')
            for loan_pending in self.loans:
                total_capital_amount += loan_pending.capital_amount
                total_applied += loan_pending.value_applied

            return (f"El empleado {self.contract.employee.party.name} mantiene "
                    f"una deuda de ${(total_capital_amount - total_applied)} "
                    f"con la empresa.")
        elif self.contract:
            return (f"El empleado {self.contract.employee.party.name} mantiene"
                    f" una deuda de $0.00 con la empresa.")
        else:
            return ""

    @fields.depends('contract', 'amount_dates', 'template', 'income_lines',
        'deduction_lines', 'internal_lines', 'reason_liquidation',
        'medical_certificates_discount', 'absences', 'permissions',
        'xiii_proportional', 'xiv_proportional', 'fr_proportional',
        'adjustment_xiv', 'income_manual_entries', 'deduction_manual_entries',
        'reason_finalize', 'loans',)
    def on_change_contract(self, name=None):
        ContractDate = Pool().get('contract.liquidation.vacation.lines')

        if self.contract:
            # period = Period.search([
            #     ('start_date', '<=', self.contract.end_date),
            #     ('end_date', '>=', self.contract.end_date)
            # ])[0]
            self.department = self.contract.department
            self.position = self.contract.position
            self.work_relationship = self.contract.work_relationship
            self.payslip_template_account_budget = (
                self.contract.payslip_template_account_budget)
            self.get_loans_considered()

            PermissionConsulting = Pool().get('hr_ec.permission.consulting')
            with Transaction().set_context(
                    contract=self.contract.id,
                    contract_end_date=self.contract.end_date,):
                permissions_consulting = PermissionConsulting.search([()])

                for pending in permissions_consulting:
                    contract_date = ContractDate()
                    contract_date.liquidation = self
                    contract_date.initial_date = pending.initial_date
                    contract_date.end_date = pending.end_date
                    contract_date.proportional_vacation = (
                        pending.proportional_vacation)
                    contract_date.additional = pending.additional
                    contract_date.vacation_in_year = pending.vacation_in_year
                    contract_date.amount = Decimal(str(
                        pending.amount)).quantize(Decimal('0.00'))
                    contract_date.pending_vacations = (Decimal(str(
                        pending.pending_vacations)).quantize(Decimal('0.00')))
                    contract_date.amount_pay = (
                        contract_date.on_change_with_amount_pay())
                    contract_date.liquidation = None
                    self.amount_dates = self.amount_dates + (contract_date,)

        else:
            self.amount_dates = []
            self.template = None
            self.income_lines = []
            self.deduction_lines = []
            self.internal_lines = []
            self.medical_certificates_discount = []
            self.absences = []
            self.permissions = []
            self.reason_liquidation = None
            self.reason_finalize = None
            self.law_benefits_xiii = []
            self.law_benefits_xiv = []
            self.loans = []
            self.overtimes = []
            self.comms_subs = []
            self.xiii_proportional = Decimal('0.00')
            self.xiv_proportional = Decimal('0.00')
            self.fr_proportional = Decimal('0.00')
            self.adjustment_xiv = Decimal('0.00')
            self.income_manual_entries = []
            self.deduction_manual_entries = []
            self.loans = []

    @fields.depends('contract')
    def on_change_with_assets_charge(self, name=None):
        Asset = Pool().get('asset')
        if self.contract:
            assets = Asset.search([
                ('employee', '=', self.contract.employee.id),
                ('state', '=', 'open'),
            ])
            result = []
            for asset in assets:
                from_date = asset.owners[0].from_date

                if from_date <= self.contract.end_date:
                    result.append(asset.id)
            return result
        else:
            return []

    @fields.depends('contract')
    def on_change_with_contract_finalized(self, name=None):
        FinalizedContract = Pool().get('company.finalize.contract')
        result = []
        finalized_contract = FinalizedContract.search([('state', '=', 'done')])
        for finalized in finalized_contract:
            result.append(finalized.contract.id)

        return result

    @fields.depends('income_lines')
    def on_change_with_total_income(self, name=None):
        total = Decimal('0.00')
        if self.income_lines:
            for income in self.income_lines:
                if income.amount:
                    total += income.amount
        return total

    @fields.depends('income_lines')
    def on_change_with_aux_total_income(self, name=None):
        total = Decimal('0.00')
        if self.income_lines:
            for income in self.income_lines:
                if income.amount and not income.rule.is_law_benefit:
                    total += income.amount
        return total

    @fields.depends('deduction_lines')
    def on_change_with_total_deduction(self, name=None):
        total = Decimal('0.00')
        if self.deduction_lines:
            for deduction in self.deduction_lines:
                if deduction.amount:
                    total += deduction.amount
        return total

    @fields.depends('income_lines', 'deduction_lines', 'amount_dates', 'loans')
    def on_change_with_total_value(self, name=None):

        return (self.on_change_with_total_income() -
                self.on_change_with_total_deduction())

    @fields.depends('contract')
    def on_change_with_contract_position(self, name=None):
        if self.contract and self.contract.position:
            return self.contract.position.id
        return None

    @fields.depends('template', 'income_lines', 'deduction_lines',
        'internal_lines', 'contract', 'period', 'fiscalyear', 'total_income',
        'aux_total_income', 'total_deduction', 'total_value', 'permissions',
        'medical_certificates_discount', 'absences', 'contract', 'amount_dates',
        'reason_liquidation', 'xiii_proportional', 'xiv_proportional',
        'fr_proportional', 'law_benefits_xiii', 'law_benefits_xiv', 'loans',
        'overtimes', 'comms_subs', 'adjustment_xiv', 'last_full_payslip',
        'reason_finalize', 'income_manual_entries', 'deduction_manual_entries',)
    def on_change_template(self, name=None):
        LiquidationLine = Pool().get('contract.liquidation.line')

        if self.template and self.contract:
            self.get_permissions_considered()
            self.get_absences_considered()
            self.get_medical_certificate_considered()
            self.get_manual_entries_considered(self.period)
            for rule in self.template.liquidation_rules:
                liquidation_line = LiquidationLine()
                liquidation_line.liquidation = self
                liquidation_line.rule = rule.rule

                if rule.rule.code == 'xiii' or rule.rule.code == 'xiv':
                    period = self.get_period_start(rule.rule.code, self.period)
                    liquidation_line.start_date = period.start_date
                else:
                    liquidation_line.start_date = self.period.payslip_start_date

                liquidation_line.end_date = self.contract.end_date
                liquidation_line.type_ = rule.rule.type_
                liquidation_line.is_manual_entry = rule.rule.is_manual_entry
                liquidation_line.is_visible_date = rule.rule.visible_date
                liquidation_line.amount = liquidation_line.get_amount_line_liquidation()  # noqa
                # liquidation_line.liquidation = None

                if liquidation_line.type_ == 'income':
                    if liquidation_line.rule.code == 'fr':
                        if (self.contract.have_fr and
                                self.contract.fr == 'monthlyse'):
                            self.income_lines += (liquidation_line,)
                    else:
                        self.income_lines += (liquidation_line,)

                elif liquidation_line.type_ == 'deduction':
                    self.deduction_lines += (liquidation_line,)
                elif liquidation_line.type_ == 'internal':
                    if liquidation_line.rule.code == 'fr_accumulate':
                        if (self.contract.have_fr and
                                self.contract.fr == 'accumulate'):
                            self.internal_lines += (liquidation_line,)
                    else:
                        self.internal_lines += (liquidation_line,)

                self.total_income = self.on_change_with_total_income()
                self.aux_total_income = self.on_change_with_aux_total_income()
                self.total_deduction = self.on_change_with_total_deduction()
                self.total_value = self.on_change_with_total_value()
            # Necesario para llegar pestañas de beneficios de ley, etc.
            self.on_change_income_lines()
            self.on_change_deduction_lines()
        else:
            self.income_lines = []
            self.deduction_lines = []
            self.internal_lines = []
            self.medical_certificates_discount = []
            self.absences = []
            self.permissions = []
            self.law_benefits_xiii = []
            self.law_benefits_xiv = []
            self.overtimes = []
            self.comms_subs = []
            self.xiii_proportional = Decimal('0.00')
            self.xiv_proportional = Decimal('0.00')
            self.fr_proportional = Decimal('0.00')
            self.adjustment_xiv = Decimal('0.00')
            self.income_manual_entries = []
            self.deduction_manual_entries = []

    @fields.depends('contract')
    def on_change_with_contract_start_date(self, name=None):
        if self.contract and self.contract.start_date:
            return self.contract.start_date
        return None

    @fields.depends('contract')
    def on_change_with_contract_work_relationship(self, name=None):
        if self.contract and self.contract.work_relationship:
            return self.contract.work_relationship.id
        return None

    @fields.depends('contract')
    def on_change_with_total_work_days(self, name=None):
        if self.contract and self.contract.end_date:
            rd = relativedelta(self.contract.end_date, self.contract.start_date)
            return "{0.years} años, {0.months} meses, {0.days} días".format(rd)
        return None

    @fields.depends('contract')
    def on_change_with_contract_end_date(self, name=None):
        if self.contract and self.contract.end_date:
            return self.contract.end_date
        return None

    @fields.depends('contract')
    def on_change_with_total_vacation_pending(self, name=None):
        if self.contract:
            return self.get_days_vacation()
        return 0

    @fields.depends('income_lines', 'deduction_lines', 'internal_lines',
        'template', 'aux_total_income', 'amount_dates', 'contract', 'fiscalyear',  # noqa
        'period', 'reason_liquidation', 'last_full_payslip', 'reason_finalize',
        'loans', )
    def on_change_amount_dates(self, name=None):
        total_lines = {}
        for line in (self.income_lines + self.deduction_lines +
                     self.internal_lines):
            total_lines[line.rule.code] = line

        self.update_lines(total_lines)

    @fields.depends('income_lines', 'deduction_lines', 'internal_lines',
        'template', 'aux_total_income', 'amount_dates', 'contract', 'fiscalyear',  # noqa
        'period', 'reason_liquidation', 'last_full_payslip', 'reason_finalize',
        'loans', )
    def on_change_loans(self, name=None):
        total_lines = {}
        for line in (self.income_lines + self.deduction_lines +
                     self.internal_lines):
            total_lines[line.rule.code] = line

        self.update_lines(total_lines)

    def get_period_start(self, code, actual_period):
        LawBenefit = Pool().get('payslip.law.benefit')
        Period = Pool().get('account.period')

        periods_law_benefit = LawBenefit.search_read([
            ('employee', '=', self.contract.employee),
            ('kind', '=', 'accumulate'),
            ('type_.code', '=', code),
            ('payslip', '=', None),
            ('is_paid_in_liquidation', '=', False),
            ('generator_payslip.is_migrated', '=', False),
            ('generator_payslip.state', '=', 'done'),
            ('generator_payslip.contract', '=', self.contract),
        ], fields_names=['period'])

        result = [field_value['period'] for field_value in periods_law_benefit]

        sort = sorted(result)
        period = False
        if sort:
            period = Period.search([('id', '=', sort[0])])
        if period:
            return period[0]
        else:
            return actual_period

    def get_overtimes_considered(self, line):

        self.overtimes = []
        info_overtimes = self.get_all_overtime_lines(self.contract,
                                                     line.start_date,
                                                     line.end_date)
        for id_line in info_overtimes['ids_overtimes_line']:
            liquidation_overtime = LiquidationOvertimeLine()
            liquidation_overtime.overtime_line = id_line
            self.overtimes = self.overtimes + (liquidation_overtime,)

    def get_all_overtime_lines(self, contract, start_date, end_date):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Overtime = pool.get('hr_ec.overtime')
        OvertimeLine = pool.get('hr_ec.overtime.line')
        PayslipOvertimeLine = pool.get('payslip.payslip.overtime.line')
        overtime = Overtime.__table__()
        overtime_line = OvertimeLine.__table__()
        payslip_overtime_line = PayslipOvertimeLine.__table__()

        result = Decimal('0.00')

        query = overtime.join(overtime_line,
            condition=(overtime.id == overtime_line.overtime)
        ).join(payslip_overtime_line, type_='LEFT',
            condition=((overtime_line.id == payslip_overtime_line.overtime_line)
                & (payslip_overtime_line.was_considered == True))
        ).select(
            overtime_line.id.as_('id_overtime_line'),
            overtime.employee,
            overtime_line.total_with_salary,
            where=((overtime.employee == contract.employee.id)
                   & (overtime.state == 'done')
                   & (overtime_line.is_paid_in_liquidation == False)
                   & (overtime_line.state == 'valid')
                   & (overtime_line.overtime_date <= end_date)
                   & ((overtime_line.overtime_date >= start_date)) &
                   (payslip_overtime_line.overtime_line == Null)
                   )
        )

        # Execute
        cursor.execute(*query)
        ids_overtime_line = []
        for row in cursor_dict(cursor):
            ids_overtime_line.append(row['id_overtime_line'])
            result += row['total_with_salary']

        return {
            'total_overtime': result,
            'ids_overtimes_line': ids_overtime_line,
        }

    def get_xiii_considered(self, line):
        LiquidationLawXIII = Pool().get(
            'contract.liquidation.law.benefits.xiii.lines')

        law_benefits = self.get_info_law_benefit_xiii(self.contract,
                                                      line.start_date,
                                                      line.end_date)

        self.law_benefits_xiii = []
        total_xiii_amount = Decimal('0.00')
        for law in law_benefits['law_benefits_xiii']:
            law_xiii = LiquidationLawXIII()
            law_xiii.xiii_line = law.id
            total_xiii_amount += law.amount
            self.law_benefits_xiii = self.law_benefits_xiii + (law_xiii,)

        if total_xiii_amount != line.amount:
            self.xiii_proportional = (line.amount - total_xiii_amount).quantize(
                Decimal('0.00'))
        else:
            self.xiii_proportional = Decimal('0.00')

    def get_info_law_benefit_xiii(self, contract, line_start_date,
                                  line_end_date):

        Period = Pool().get('account.period')
        LawBenefit = Pool().get('payslip.law.benefit')

        periods = Period.search([
            ['OR',
             (('start_date', '>=', line_start_date),
              ('end_date', '<=', line_end_date)),
             (('start_date', '<=', line_end_date),
              ('end_date', '>=', line_end_date))
             ]
        ])

        ids_periods = []
        for period in periods:
            ids_periods.append(period.id)

        law_benefits = LawBenefit.search([
            ('employee', '=', contract.employee),
            ('kind', '=', 'accumulate'),
            ('type_.code', '=', 'xiii'),
            ('payslip', '=', None),
            ('period', 'in', ids_periods),
            ('generator_payslip.state', '=', 'done'),
            ('generator_payslip.contract', '=', contract),
            ('is_paid_in_liquidation', '=', False),
        ])

        total_law_benefit_xiii = Decimal('0.00')
        for law in law_benefits:
            total_law_benefit_xiii += law.amount

        return {
            'total_amount_xiii': total_law_benefit_xiii,
            'law_benefits_xiii': law_benefits,
        }

    def get_xiv_considered(self, line):
        Period = Pool().get('account.period')
        LiquidationLawXIV = Pool().get(
            'contract.liquidation.law.benefits.xiv.lines')

        law_benefits = self.get_info_law_benefit_xiv(self.contract,
                                                     line.start_date,
                                                     line.end_date)
        self.law_benefits_xiv = []
        total_xiv_amount = Decimal('0.00')
        total_xiv_amount_aux = Decimal('0.00')
        for law in law_benefits['law_benefits_xiv']:
            law_xiv = LiquidationLawXIV()
            law_xiv.xiv_line = law.id
            previous_fiscalyear = Period.search([
                ('id', '=', law.period.id)])[0].fiscalyear
            total_xiv_amount_aux += law.amount
            total_xiv_amount += self.adjustment_xiv_amount(law.amount,
                                                           previous_fiscalyear)
            self.law_benefits_xiv = self.law_benefits_xiv + (law_xiv,)

        if total_xiv_amount != line.amount:
            self.xiv_proportional = (line.amount - total_xiv_amount).quantize(
                Decimal('0.00'))
        else:
            self.xiv_proportional = Decimal('0.00')

        self.adjustment_xiv = (total_xiv_amount - total_xiv_amount_aux
                               ).quantize(Decimal('0.00'))

    def get_info_law_benefit_xiv(self, contract, line_start_date,
            line_end_date):

        Period = Pool().get('account.period')
        LawBenefit = Pool().get('payslip.law.benefit')

        periods = Period.search([
            ['OR',
             (('start_date', '>=', line_start_date),
              ('end_date', '<=', line_end_date)),
             (('start_date', '<=', line_end_date),
              ('end_date', '>=', line_end_date))
             ]
        ])

        ids_periods = []
        for period in periods:
            ids_periods.append(period.id)

        law_benefits = LawBenefit.search([
            ('employee', '=', contract.employee),
            ('kind', '=', 'accumulate'),
            ('type_.code', '=', 'xiv'),
            ('payslip', '=', None),
            ('period', 'in', ids_periods),
            ('generator_payslip.state', '=', 'done'),
            ('generator_payslip.contract', '=', contract),
            ('is_paid_in_liquidation', '=', False),
        ])

        total_law_benefit_xiv = Decimal('0.00')
        for law in law_benefits:
            previous_fiscalyear = Period.search([
                ('id', '=', law.period.id)])[0].fiscalyear
            amount = self.adjustment_xiv_amount(law.amount, previous_fiscalyear)
            total_law_benefit_xiv += amount

        return {
            'total_amount_xiv': total_law_benefit_xiv,
            'law_benefits_xiv': law_benefits,
        }

    def adjustment_xiv_amount(self, law_amount, previous_fiscalyear):
        actual_base_salary = self.fiscalyear.base_salary
        previous_base_salary = previous_fiscalyear.base_salary

        new_amount = ((law_amount * actual_base_salary) / previous_base_salary)

        return new_amount.quantize(Decimal('0.00'))

    def get_comms_subs_considered(self, line):

        cursor = Transaction().connection.cursor()
        CommSub = Pool().get('company.commission.subrogation')
        Detail = Pool().get('company.commission.subrogation.detail')
        PayslipCommSubDetail = Pool().get(
            'payslip.payslip.commission.subrogation.detail')
        ActionStaff = Pool().get('company.action.staff')
        LiquidationCoomSub = Pool().get(
            'contract.liquidation.commsub.lines')
        comm_sub = CommSub.__table__()
        detail = Detail.__table__()
        payslip_comm_sub_detail = PayslipCommSubDetail.__table__()
        action_staff = ActionStaff.__table__()

        payslip_period_end_date = self.period.end_date
        payslip_period_start_date = self.period.start_date - timedelta(days=11)

        # Get possible comm/sub details to consider in this liquidation
        table_A = comm_sub.join(detail,
            condition=(comm_sub.id == detail.commission)
            ).join(action_staff,
                   condition=(action_staff.id == comm_sub.action_staff)
        ).select(
            action_staff.employee,
            detail.id.as_('comm_sub_detail'),
            detail.end_date,
            where=((comm_sub.state == 'done') &
               (action_staff.employee == self.contract.employee.id) &
               (detail.end_date <= payslip_period_end_date) &
               (detail.start_date >= payslip_period_start_date)
            )
        )
        # Get comm/sub details considered in other payslips
        table_B = payslip_comm_sub_detail.join(detail,
            condition=(detail.id == payslip_comm_sub_detail.comm_sub_detail)
        ).select(
            payslip_comm_sub_detail.comm_sub_detail.as_(
                'comm_sub_detail_aux'),
            payslip_comm_sub_detail.payslip,
            where=((payslip_comm_sub_detail.was_considered == 't') &
                   (detail.end_date <= self.period.end_date))
        )
        # Get comm/sub details to consider in this payslip
        query = table_A.join(table_B, type_='LEFT OUTER',
            condition=(table_A.comm_sub_detail == table_B.comm_sub_detail_aux)
        ).select(
            table_A.comm_sub_detail,
            where=((table_B.payslip == self.id) |
                   (table_B.comm_sub_detail_aux == Null)
                   )
        )
        self.comms_subs = []
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            comm_sub = LiquidationCoomSub()
            comm_sub.comm_sub_line = row['comm_sub_detail']
            self.comms_subs = self.comms_subs + (comm_sub,)

    def get_permissions_considered(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Permission = pool.get('hr_ec.permission')
        Contract = pool.get('company.contract')
        LiquidationPermission = pool.get('contract.liquidation.permission.lines')  # noqa
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')

        permission = Permission.__table__()
        contract = Contract.__table__()
        payslip_permission = PayslipPermission.__table__()

        if self.contract and self.period:
            # Permissions that overlaps with medical certificates
            query_permissions_overlaps_cerificates = (
                self.get_query_permissions_overlaps_with_medical_certificates())

            # Permissions with discount
            query_permission = permission.join(payslip_permission, type_='LEFT',
                condition=(permission.id == payslip_permission.permission) &
                          (payslip_permission.was_considered == True)
            ).join(contract,
                condition=permission.contract == contract.id
            ).select(
                permission.id,
                where=(~(permission.id.in_(
                    query_permissions_overlaps_cerificates))
                       & (contract.absence_apply == True)
                       & (permission.contract == self.contract.id)
                       & (permission.end_date >= self.period.payslip_start_date)
                       & (permission.end_date <= self.contract.end_date)
                       & (permission.state == 'done')
                       & (permission.is_paid_in_liquidation == False)
                       & (permission.discount_to.in_(('payslip', 'license')))
                       & (payslip_permission.permission == Null)),
                group_by=[permission.id]
            )
            self.permissions = []
            cursor.execute(*query_permission)
            for row in cursor_dict(cursor):
                liquidation_permission = LiquidationPermission()
                liquidation_permission.permission_line = row['id']
                self.permissions = self.permissions + (liquidation_permission,)

    def get_absences_considered(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Absence = pool.get('hr_ec.absence')
        Contract = pool.get('company.contract')
        LiquidationAbsence = pool.get('contract.liquidation.absence.lines')
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')
        absence = Absence.__table__()
        contract = Contract.__table__()
        payslip_absence = PayslipAbsence.__table__()

        if self.contract and self.period:
            # Absences that overlaps with medical certificates
            absences_overlaps_cerificates = (
                self.get_query_absences_overlaps_with_medical_certificates())

            # Absences
            query_absence = absence.join(payslip_absence, type_='LEFT',
                condition=(absence.id == payslip_absence.absence) &
                      (payslip_absence.was_considered == True)
            ).join(contract,
                condition=(absence.employee == contract.employee)
            ).select(absence.id,
                where=(~(absence.id.in_(absences_overlaps_cerificates))
                    & (contract.id == self.contract.id)
                    & (contract.absence_apply == True)
                    & (absence.end_date >= self.period.payslip_start_date)
                    & (absence.end_date <= self.contract.end_date)
                    & (absence.state == 'done')
                    & (absence.is_paid_in_liquidation == False)
                    & (payslip_absence.absence == Null)),
                    group_by=[absence.id]
                  )
            self.absences = []
            cursor.execute(*query_absence)
            for row in cursor_dict(cursor):
                liquidation_absence = LiquidationAbsence()
                liquidation_absence.absence_line = row['id']
                self.absences = self.absences + (liquidation_absence,)

    def get_medical_certificate_considered(self):
        pool = Pool()
        MedicalCertificate = pool.get('galeno.medical.certificate')
        LiquidationMedicalCertificate = pool.get(
            'contract.liquidation.certificates.lines')
        PayslipPayslip = pool.get('payslip.payslip')

        payslip = PayslipPayslip.search([
            ('contract', '=', self.contract.id),
            ('period', '=', self.period.id),
        ])

        if payslip:
            start_date_ = payslip[0].period.payslip_end_date + timedelta(days=1)
        else:
            start_date_ = self.period.payslip_start_date

        if self.contract and self.period:
            medical_info = MedicalCertificate.search([
                ('employee', '=', self.contract.employee),
                ('state', '=', 'done'),
                ('start_date', '>=', start_date_),
                ('end_date', '<=', self.contract.end_date),
            ])
            if medical_info:
                self.medical_certificates_discount = []
                for med in medical_info:
                    liquidation_medical = LiquidationMedicalCertificate()
                    liquidation_medical.medical_line = med.id
                    self.medical_certificates_discount = (
                        self.medical_certificates_discount +
                        (liquidation_medical,))

    def get_manual_entries_considered(self, period):
        LiquidationPayslipEntry = Pool().get('contract.liquidation.payslip.entry')  # noqa
        manual_entries = self.get_all_manual_entries(
            self, period.payslip_start_date, self.contract.end_date)

        for income_entry in manual_entries['income_entries']:
            new_income_entry = LiquidationPayslipEntry()
            new_income_entry.entry = income_entry
            new_income_entry.kind = 'income'
            new_income_entry.was_considered = False

            self.income_manual_entries += (new_income_entry,)

        for income_entry in manual_entries['deduction_entries']:
            new_deduction_entry = LiquidationPayslipEntry()
            new_deduction_entry.entry = income_entry
            new_deduction_entry.kind = 'deduction'
            new_deduction_entry.was_considered = False

            self.deduction_manual_entries += (new_deduction_entry,)

    @fields.depends('contract', 'deduction_lines', 'income_lines',
        'internal_lines', 'template', 'period', 'fiscalyear', 'total_income',
        'aux_total_income', 'amount_dates', 'overtimes', 'law_benefits_xiv',
        'law_benefits_xiii', 'comms_subs', 'reason_liquidation', 'xiii_proportional',  # noqa
        'xiv_proportional', 'medical_certificates_discount', 'absences',
        'permissions', 'fr_proportional', 'loans', 'adjustment_xiv',
        'last_full_payslip', 'reason_finalize', 'income_manual_entries',
        'deduction_manual_entries', )
    def on_change_income_lines(self, name=None):

        lines = (self.income_lines + self.deduction_lines + self.internal_lines)
        total_lines = {}
        for line in lines:
            total_lines[line.rule.code] = line

        self.update_lines(total_lines)

        lines = (self.income_lines + self.deduction_lines + self.internal_lines)
        for line in lines:
            if (self.contract and line.rule and line.rule.code == 'overtimes'
                    and line.start_date and line.end_date):
                self.get_overtimes_considered(line)
            elif (self.contract and line.rule and line.rule.code == 'xiii'
                    and line.start_date and line.end_date):
                self.get_xiii_considered(line)
            elif (self.contract and line.rule and line.rule.code == 'xiv'
                  and line.start_date and line.end_date):
                self.get_xiv_considered(line)
            elif (self.contract and line.rule and line.rule.code == 'fr'
                  and line.start_date and line.end_date):
                # self.get_fr_proportional(line)
                self.fr_proportional = (line.amount)
            elif (self.contract and line.rule and line.rule.code in
                  ['comms_subs', 'encargos', 'subrogaciones']
                  and line.start_date and line.end_date):
                self.get_comms_subs_considered(line)

    @fields.depends('contract', 'deduction_lines', 'loans', 'income_lines',
        'internal_lines', 'template', 'period', 'fiscalyear', 'total_income',
        'aux_total_income', 'total_deduction', 'amount_dates',
        'reason_liquidation', 'xiii_proportional', 'xiv_proportional',
        'fr_proportional', 'medical_certificates_discount', 'absences',
        'permissions', 'comms_subs', 'overtimes', 'law_benefits_xiv',
        'law_benefits_xiii', 'last_full_payslip', 'reason_finalize',
        'income_manual_entries', 'deduction_manual_entries',)
    def on_change_deduction_lines(self, name=None):
        lines = (self.deduction_lines + self.income_lines + self.internal_lines)
        total_lines = {}
        for line in lines:
            total_lines[line.rule.code] = line

        self.update_lines(total_lines)

    def get_loans_considered(self):
        LoanLine = Pool().get('company.employee.loan.line')
        lines = LoanLine.search([
            ('loan.contract', '=', self.contract.id),
            ('loan.state', '=', 'open'),
            # ('start_date', '>=', start_date),
            ('state', '!=', 'paid'),
        ])
        self.loans = []
        for line in lines:
            loan_line = LiquidationLoans()
            loan_line.loan_line = line.id
            loan_line.capital_amount = line.capital_amount
            loan_line.value_applied = line.capital_amount
            self.loans = self.loans + (loan_line,)

    @fields.depends('contract', 'deduction_lines', 'income_lines',
        'internal_lines', 'template', 'period', 'fiscalyear', 'total_income',
        'aux_total_income', 'amount_dates', 'reason_liquidation',
        'xiii_proportional', 'xiv_proportional', 'fr_proportional',
        'medical_certificates_discount', 'absences', 'permissions', 'comms_subs',  # noqa
        'overtimes', 'law_benefits_xiv', 'law_benefits_xiii', 'adjustment_xiv',
        'last_full_payslip', 'reason_finalize', 'income_manual_entries',
        'deduction_manual_entries', 'loans')
    def on_change_internal_lines(self, name=None):

        lines = (self.deduction_lines + self.income_lines + self.internal_lines)
        total_lines = {}
        for line in lines:
            total_lines[line.rule.code] = line

        self.update_lines(total_lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, items):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, liquidations):
        for liquidation in liquidations:
            if liquidation.assets_charge:
                cls.raise_user_warning('aseets_charge_%d' % liquidation.id,
                   'aseets_charge', {
                       'employee': liquidation.contract.employee.party.name
                   })

            if liquidation.total_value < 0:
                cls.raise_user_error('negative_total_pay', {
                    'employee': liquidation.contract.employee.party.name})

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, liquidations):

        cls.liquidate_law_benefits(liquidations)
        cls.liquidate_loans(liquidations)
        cls.liquidate_overtimes(liquidations)
        cls.liquidate_permissions(liquidations)
        cls.liquidate_absences(liquidations)
        cls.liquidate_manual_entries(liquidations)
        cls.set_done_date(liquidations)

        liquidations[0].state = 'done'
        super(ContractLiquidation, cls).save(liquidations)
        cls.save_law_benefits_adjustment_xiv(liquidations[0])

        cls.update_finalize_contract(liquidations)

    @classmethod
    def set_done_date(cls, liquidations):
        Liquidation = Pool().get('contract.liquidation')
        today = datetime.today()
        list_liquidations = []
        for liquidation in liquidations:
            liquidation.done_date = today
            list_liquidations.append(liquidation)

        Liquidation.save(list_liquidations)

    @classmethod
    def save_law_benefits_adjustment_xiv(cls, liquidation):
        LawBenefit = Pool().get('payslip.law.benefit')
        LiquidationLawXIII = Pool().get(
            'contract.liquidation.law.benefits.xiii.lines')
        LiquidationLawXIV = Pool().get(
            'contract.liquidation.law.benefits.xiv.lines')

        for line in liquidation.income_lines:
            new_law = LawBenefit()
            new_law.company = liquidation.company
            new_law.employee = liquidation.contract.employee
            new_law.period = liquidation.period
            new_law.kind = 'liquidation'
            new_law.generator_liquidation = liquidation

            if line.rule.code == 'xiii' and liquidation.xiii_proportional > 0:
                new_law.type_ = line.rule.associated_rule.law_benefit_type
                new_law.amount = liquidation.xiii_proportional
                LawBenefit.save([new_law])
                law_xiii = LiquidationLawXIII()
                law_xiii.liquidation = liquidation
                law_xiii.xiii_line = new_law
                LiquidationLawXIII.save([law_xiii])
            elif line.rule.code == 'xiv' and liquidation.xiv_proportional > 0:
                new_law.amount = liquidation.xiv_proportional
                new_law.type_ = line.rule.associated_rule.law_benefit_type
                LawBenefit.save([new_law])
                law_xiv = LiquidationLawXIV()
                law_xiv.liquidation = liquidation
                law_xiv.xiv_line = new_law
                LiquidationLawXIV.save([law_xiv])
            elif line.rule.code == 'fr' and liquidation.fr_proportional > 0:
                new_law.amount = liquidation.fr_proportional
                new_law.type_ = line.rule.associated_rule.law_benefit_type
                new_law.is_paid_in_liquidation = True
                LawBenefit.save([new_law])

            # SAVE AJUSTE DEL XIV SI EXISTE
            if line.rule.code == 'xiv' and liquidation.adjustment_xiv > 0:
                new_law = LawBenefit()
                new_law.company = liquidation.company
                new_law.employee = liquidation.contract.employee
                new_law.period = liquidation.period
                new_law.kind = 'adjustment'
                new_law.generator_liquidation = liquidation
                new_law.type_ = line.rule.associated_rule.law_benefit_type
                new_law.amount = liquidation.adjustment_xiv
                LawBenefit.save([new_law])
                law_xiv = LiquidationLawXIV()
                law_xiv.liquidation = liquidation
                law_xiv.xiv_line = new_law
                LiquidationLawXIV.save([law_xiv])

        for line in liquidation.internal_lines:

            if line.rule.code == 'fr_accumulate':
                new_law = LawBenefit()
                new_law.company = liquidation.company
                new_law.employee = liquidation.contract.employee
                new_law.period = liquidation.period
                new_law.kind = 'accumulate'
                new_law.generator_liquidation = liquidation
                new_law.amount = line.amount
                new_law.type_ = line.rule.associated_rule.law_benefit_type
                new_law.is_paid_in_liquidation = True
                LawBenefit.save([new_law])

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, items):
        pass

    @classmethod
    @ModelView.button
    def post(cls, liquidations):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        for liquidation in liquidations:
            if not liquidation.journal:
                cls.raise_user_error('set_journal')
            if ((not liquidation.budget_impact_direct
                    or liquidation.budget_impact_direct == False)
                    and not liquidation.compromise):
                cls.raise_user_error('set_compromise')
            if liquidation.move and liquidation.move.state == 'posted':
                cls.raise_user_error('posted_move')
            if liquidation.move and liquidation.move.state == 'draft':
                MoveLine.delete(liquidation.move.lines)
            move = liquidation.get_liquidation_move()
            liquidation.move = move

        cls.save(liquidations)

    @classmethod
    def liquidate_law_benefits(cls, liquidations):
        LawBenefit = Pool().get('payslip.law.benefit')

        for liquidation in liquidations:
            update_laws_banefits = []
            for line in liquidation.law_benefits_xiii:
                law = line.xiii_line
                # for law in law_benefit:
                law.is_paid_in_liquidation = True
                update_laws_banefits.append(law)

            for line in liquidation.law_benefits_xiv:
                law = line.xiv_line
                law.is_paid_in_liquidation = True
                update_laws_banefits.append(law)

            LawBenefit.save(update_laws_banefits)

    @classmethod
    def liquidate_loans(cls, liquidations):
        for liquidation in liquidations:
            for line in liquidation.loans:
                register_payment(line.value_applied, line.loan_line.loan.id)

    @classmethod
    def update_finalize_contract(cls, liquidations):
        FinalizeContract = Pool().get('company.finalize.contract')

        for liquidation in liquidations:
            finalize_contract, = FinalizeContract.search([
                ('contract', '=', liquidation.contract),
                ('state', '=', 'done')])

            finalize_contract.state = 'liquidated'

            FinalizeContract.save([finalize_contract])

    @classmethod
    def liquidate_overtimes(cls, liquidations):
        OvertimeLine = Pool().get('hr_ec.overtime.line')
        overtime_line = OvertimeLine.__table__()
        cursor = Transaction().connection.cursor()
        for liquidation in liquidations:
            for line in liquidation.overtimes:
                query_update = overtime_line.update(
                    [overtime_line.is_paid_in_liquidation], [True],
                    where=(overtime_line.id == line.id))
                cursor.execute(*query_update)

    @classmethod
    def liquidate_permissions(cls, liquidations):
        Permission = Pool().get('hr_ec.permission')
        cursor = Transaction().connection.cursor()
        permission = Permission.__table__()

        for liquidation in liquidations:
            for line in liquidation.permissions:
                permi = line.permission_line

                query_update = permission.update(
                    [permission.is_paid_in_liquidation], [True],
                    where=(permission.id == permi.id))
                cursor.execute(*query_update)

    @classmethod
    def liquidate_absences(cls, liquidations):
        Absence = Pool().get('hr_ec.absence')
        cursor = Transaction().connection.cursor()
        absence = Absence.__table__()

        for liquidation in liquidations:
            for line in liquidation.absences:
                absen = line.absence_line
                query_update = absence.update(
                    [absence.is_paid_in_liquidation], [True],
                    where=(absence.id == absen.id))
                cursor.execute(*query_update)

    @classmethod
    def liquidate_manual_entries(cls, liquidations):
        pool = Pool()
        LiquidationPayslipEntry = pool.get('contract.liquidation.payslip.entry')

        for liquidation in liquidations:
            update_manual_entries = []
            for line in liquidation.income_manual_entries:
                line.was_considered = True
                update_manual_entries.append(line)

            for line in liquidation.deduction_manual_entries:
                line.was_considered = True
                update_manual_entries.append(line)

            LiquidationPayslipEntry.save(update_manual_entries)

    def get_all_manual_entries(self, liquidation, start_date, end_date):
        incomes = []
        deductions = []

        pool = Pool()
        cursor = Transaction().connection.cursor()
        Entry = pool.get('payslip.entry')
        PayslipManualEntry = pool.get('payslip.payslip.manual.entry')
        LiquidationManualEntry = pool.get('contract.liquidation.payslip.entry')
        entry = Entry.__table__()
        payslip_manual_entry = PayslipManualEntry.__table__()
        liquidation_manual_entry = LiquidationManualEntry.__table__()

        # for liquidation in liquidations:
        # Get possible entries to consider in this payslip
        table_A = entry.select(
            entry.employee,
            entry.id,
            entry.kind,
            entry.entry_date,
            where=((entry.employee == liquidation.contract.employee.id) &
                   (entry.state == 'done') &
                   (entry.entry_date >= start_date) &
                   (entry.entry_date <= end_date)
                   )
        )
        # Get entries considered in other payslips
        table_B = payslip_manual_entry.join(entry,
            condition=(entry.id == payslip_manual_entry.entry)
            ).select(
            payslip_manual_entry.entry,
            where=((payslip_manual_entry.was_considered == 't') &
                   (entry.entry_date >= start_date) &
                   (entry.entry_date <= end_date)
                   )
        )
        # Get entries considered in other liquidations
        table_C = liquidation_manual_entry.join(entry,
            condition=(entry.id == liquidation_manual_entry.entry)
            ).select(
            liquidation_manual_entry.entry,
            where=((liquidation_manual_entry.was_considered == 't') &
                   (entry.entry_date >= start_date) &
                   (entry.entry_date <= end_date)
                   )
        )
        # Get entries to consider in this liquidation
        query = table_A.select(
            table_A.id,
            table_A.kind,
            where=(NotIn(table_A.id, table_B) & NotIn(table_A.id, table_C))
        )
        # Execute
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            if row['kind'] == 'income':
                incomes.append(row['id'])
            elif row['kind'] == 'deduction':
                deductions.append(row['id'])

        return {
            'income_entries': incomes,
            'deduction_entries': deductions
        }

    def get_days_vacation(self):

        Configuration = Pool().get('hr_ec.configuration')
        PermissionBalance = Pool().get('hr_ec.permission.balance')
        Date = Pool().get('ir.date')

        cursor = Transaction().connection.cursor()
        configuration = Configuration.search([])
        if configuration[0].have_cut:
            exist_cut_date = configuration[0].date_cut
        else:
            exist_cut_date = None
        if configuration[0].date_permissions:
            date_permissions = configuration[0].date_permissions
        else:
            date_permissions = None

        contract_end_date = None
        if self.contract.end_date:
            contract_end_date = self.contract.end_date
        elif self.contract_end_date:
            contract_end_date = self.contract_end_date
        else:
            contract_end_date = Date.today()

        with Transaction().set_context(employee=self.contract.employee.id,
                                       end_date=contract_end_date,
                                       exist_cut_date=exist_cut_date,
                                       date_permissions=date_permissions,
                                       department=None):
            query_vacation = PermissionBalance.table_query()

        cursor.execute(*query_vacation)

        row = cursor.fetchone()

        return row[16]

    def get_query_absences_overlaps_with_medical_certificates(self):
        pool = Pool()
        Absence = pool.get('hr_ec.absence')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        absence = Absence.__table__()

        query_medical = absence.join(medical_certificate,
                condition=(absence.employee == medical_certificate.employee)
        ).select(
            absence.id,
            where=((absence.start_date >= medical_certificate.start_date) &
                   (absence.end_date <= medical_certificate.end_date) &
                   (absence.state == 'done') &
                   (medical_certificate.state == 'done') &
                   (medical_certificate.register_type == 'days') &
                   (absence.employee == self.contract.employee.id) &
                   (absence.end_date >= self.period.payslip_start_date) &
                   (absence.end_date <= self.contract.end_date)
                   )
        )
        return query_medical

    def get_query_permissions_overlaps_with_medical_certificates(self):
        pool = Pool()
        Permission = pool.get('hr_ec.permission')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        permission = Permission.__table__()

        query_medical = permission.join(medical_certificate,
                condition=permission.employee == medical_certificate.employee
            ).select(
            permission.id,
            where=((permission.start_date >= medical_certificate.start_date) &
                (permission.end_date <= medical_certificate.end_date) &
                (permission.state == 'done') &
                (medical_certificate.state == 'done') &
                (medical_certificate.register_type == 'days') &
                (permission.employee == self.contract.employee.id) &
                (permission.end_date >= self.period.payslip_start_date) &
                (permission.end_date <= self.contract.end_date)
            )
        )
        return query_medical

    def update_lines(self, total_lines):
        if self.template:
            self.aux_total_income = Decimal('0.00')
            for template_rule in self.template.liquidation_rules:
                line = total_lines.get(template_rule.rule.code)
                if line:

                    if not line.rule.is_manual_entry:
                        line.liquidation = self
                        new_amount = line.get_amount_line_liquidation()
                        NAMES[line.rule.code] = new_amount
                        line.amount = new_amount

                    if (not line.rule.is_law_benefit and
                            line.rule.type_ == 'income' and line.amount):
                        self.aux_total_income += line.amount

                    self.total_income = self.on_change_with_total_income()
                    self.total_deduction = self.on_change_with_total_deduction()
                    line.liquidation = None
        else:
            self.income_lines = []
            self.deduction_lines = []
            self.internal_lines = []
            self.medical_certificates_discount = []
            self.absences = []
            self.permissions = []
            self.law_benefits_xiii = []
            self.law_benefits_xiv = []
            self.overtimes = []
            self.comms_subs = []
            self.xiii_proportional = Decimal('0.00')
            self.xiv_proportional = Decimal('0.00')
            self.fr_proportional = Decimal('0.00')
            self.adjustment_xiv = Decimal('0.00')
            self.income_manual_entries = []
            self.deduction_manual_entries = []

    def get_liquidation_move(self):
        def prepare_move_line(account, debit, credit, party, budget):
            payable_budget = None
            line_account = account_values[account]
            if line_account.kind == 'payable' and credit > 0:
                payable_budget = budget
            return self.get_move_line(
                move,
                line_account,
                debit,
                credit, {
                    'compromise': self.compromise,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': payable_budget,
                    'payable_cost_center': default_cost_center
                },
                budget
            )

        self.update_liquidation_line_account_budget()
        pool = Pool()
        Move = pool.get('account.move')
        Party = pool.get('party.party')
        Period = Pool().get('account.period')
        Date = pool.get('ir.date')
        cursor = Transaction().connection.cursor()

        move_lines = defaultdict(lambda: [])
        negative_party = []

        accounting_date = Date.today()

        period_id = Period.find(
            Transaction().context.get('company'), exception=False)

        move = self.move if self.move else Move(number="#")
        # move = Move(move.id)
        move.type = 'financial'
        move.description = 'Liquidacion de haberes correspondiente a %s' % \
            self.contract.employee.party.name
        move.journal = self.journal
        move.period = period_id
        move.date = accounting_date
        move.origin = self
        move.company = self.company
        if not self.budget_impact_direct:
            move.compromises = [self.compromise]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        with Transaction().set_context(company=self.company.id,
                state='done', contract_liquidation=self.id):

            Account = pool.get('account.account')
            AccountMoveLine = pool.get('account.move.line')
            CostCenter = pool.get('public.cost.center')

            default_cost_center, = CostCenter.search([])

            query = self.liquidation_query_get()
            cursor.execute(*query)
            result = cursor.fetchall()

            account_values = defaultdict(lambda: Decimal(0))
            account_ids = []
            parent_account_ids = []
            parent_lines = {}
            for (account, debit, credit, party, budget, parent) in result:

                if account is not None:
                    account_ids.append(account)
                parent_account_ids.append(parent)
            for account in Account.browse(account_ids):
                account_values[account.id] = account
            for (account, debit, credit, party, budget, parent) in result:
                if account is not None:
                    if debit == 0 and credit == 0:
                        continue
                    if debit != 0 and credit != 0:
                        move_lines[party] += prepare_move_line(
                            account, 0, credit, party, budget)
                        move_lines[party] += prepare_move_line(
                            account, debit, 0, party, budget)
                        move_lines[party][-1].payable_move_line = (
                            move_lines[party][-2])
                        if parent:
                            move_lines[party][-2].parent = (
                                parent_lines.get(f'{parent}-{budget}'))
                    else:
                        move_lines[party] += prepare_move_line(
                            account, debit, credit, party, budget)
                        if (debit != 0 and account in parent_account_ids
                                and not parent):
                            parent_lines[
                                f'{account}-{budget}'] = move_lines[party][-1]
                        if parent:
                            move_lines[party][-1].parent = (
                                parent_lines.get(f'{parent}-{budget}'))
                    if party and credit < 0:
                        negative_party.append(party)

        for np_ in negative_party:
            party = Party(np_)
            negative_amount = 0
            neg_move_lines = move_lines.get(np_)
            for line in neg_move_lines:
                if line.debit == 0 and line.credit < 0:
                    negative_amount = line.credit
                    neg_move_lines.remove(line)
                    break
            lines_to_remove = []
            if negative_amount < 0:
                for line in neg_move_lines:
                    if (not line.account.code.startswith('213')
                            or line.debit != 0):
                        continue
                    if line.debit == 0 and line.credit > abs(negative_amount):
                        line.credit += negative_amount
                        break
                    else:
                        negative_amount += line.credit
                        lines_to_remove.append(line)
                for line in lines_to_remove:
                    neg_move_lines.remove(line)
            move_lines[np] = neg_move_lines

        for p in move_lines.keys():
            if p is None:
                continue
            debit_accounts = defaultdict(lambda: None)
            credit_accounts = defaultdict(lambda: None)
            debit_accounts_to_add = []
            debit_accounts_to_remove = []
            for l in move_lines.get(p):
                if l.debit != 0:
                    id_b = l.budget.id if l.budget else -1
                    debit_accounts[f'{l.account.id}-{id_b}'] = l
                elif l.credit != 0:
                    id_b = l.payable_budget.id if l.payable_budget else -1
                    credit_accounts[f'{l.account.id}-{id_b}'] = l
            for a in debit_accounts.keys():
                party = Party(p)
                pending_debit = 0.0
                fixed_line = None
                # Sin Cuenta
                if a not in credit_accounts.keys():
                    debit = debit_accounts.get(a).debit
                    debit_accounts_to_remove.append(debit_accounts.get(a))
                    pending_debit = debit
                # Ajuste
                elif (debit_accounts.get(a).debit
                      > credit_accounts.get(a).credit):
                    debit = debit_accounts.get(a).debit
                    credit = credit_accounts.get(a).credit
                    pending_debit = utils.quantize_currency((debit - credit))
                    debit_accounts.get(a).debit = credit
                    fixed_line = credit_accounts.get(a)

                for lc in credit_accounts.values():
                    if (lc == fixed_line or lc.account.code.startswith('112')
                            or (not lc.party and not lc.account.kind == 'payable')):  # noqa
                        continue
                    if pending_debit == 0:
                        break
                    amount = 0.0
                    if lc.credit > pending_debit:
                        amount = pending_debit
                    else:
                        amount = lc.credit
                    lines = prepare_move_line(
                        lc.account.id, amount, 0.0, lc.party,
                        lc.parent.budget)
                    for li in lines:
                        debit_accounts_to_add.append(li)
                        debit_accounts_to_add[-1].payable_move_line = lc
                    pending_debit = utils.quantize_currency(
                        (pending_debit - amount))

            if not debit_accounts_to_add:
                continue
            move_lines[p] = debit_accounts_to_add
            for d in debit_accounts.values():
                move_lines[p].append(d)
            move_lines[p] = [i for i in move_lines[p]
                if i not in debit_accounts_to_remove]
            for c in credit_accounts.values():
                move_lines[p].append(c)

        mld1 = ()
        mld2 = ()
        mlc = ()
        for ls in move_lines.values():
            for l in ls:
                if l.debit != 0 and l.account.kind == 'expense':
                    mld1 += (l,)
                elif l.debit != 0:
                    mld2 += (l,)
                else:
                    mlc += (l,)

        move.save()

        query_accummulate = self.accummulate_query_get()
        cursor.execute(*query_accummulate)
        result = cursor.fetchall()

        # account_values_accummulate = defaultdict(lambda: Decimal(0))
        account_ids_accummulate = []

        for (rule, contract_id, xiii_line, fiscalyear, law_benefit_id,
             law_benefit_amount, law_benefit_kind, move_line_id,
             move_line_account, account_close, reclassification_account,
             template, activity, expense_account, payable_account) in result:
            if move_line_account is not None and move_line_account != -1:
                account_ids_accummulate.append(move_line_account)
            if account_close is not None and account_close != -1:
                account_ids_accummulate.append(account_close)
            if reclassification_account is not None and reclassification_account != -1:  # noqa
                account_ids_accummulate.append(reclassification_account)
            if expense_account is not None and expense_account != -1:
                account_ids_accummulate.append(expense_account)
            if payable_account is not None and payable_account != -1:
                account_ids_accummulate.append(payable_account)

        for account in Account.browse(account_ids_accummulate):
            account_values[account.id] = account

        for (rule, contract_id, xiii_line, fiscalyear, law_benefit_id,
             law_benefit_amount, law_benefit_kind, move_line_id,
             move_line_account, account_close, reclassification_account,
             template, activity, expense_account, payable_account) in result:
            new_lines = []
            party = self.contract.employee.party.id
            # Año anterior
            if self.fiscalyear.id != fiscalyear and fiscalyear is not None:
                account = account_close
                if account_close is None or reclassification_account is None:
                    continue
                new_lines += prepare_move_line(
                    account_close, law_benefit_amount, 0, party, None)

                new_lines += prepare_move_line(reclassification_account,
                    0, law_benefit_amount, party, None)
                new_lines[-1].parent = new_lines[-2]
                mld1 += (new_lines[-2],)
                mlc += (new_lines[-1],)
            # Ajuste de monto que no ha sido declarado en algun rol
            elif move_line_id is None:
                Budget = pool.get('public.budget')
                Account = pool.get('account.account')
                account = Account(expense_account)
                budgets = Budget.search([
                    ('activity', '=', activity),
                    ('parent', '=', account.budget_debit.id),
                    # search for draft moves
                ], limit=1)
                budget = None
                if len(budgets) > 0:
                    budget = budgets[0]
                new_lines += prepare_move_line(
                        expense_account, law_benefit_amount, 0, None, budget)
                new_lines += prepare_move_line(
                        payable_account, 0, law_benefit_amount, party, None)
                new_lines[-1].parent = new_lines[-2]
                mld1 += (new_lines[-2],)
                mlc += (new_lines[-1],)
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        return move

    def get_move_line(self, move, account, debit=0, credit=0, expense_data={},
            budget=None):
        if debit == 0 and credit == 0:
            return []
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Compromise = pool.get('public.budget.compromise')
        Party = pool.get('party.party')
        if account.kind == 'view':
            self.raise_user_error('account_error', {
                'account': account.rec_name
            })

        def add_line(move, account, debit, credit, budget=None):
            line = MoveLine(cost_center=expense_data.get('cost_center'),
                payable_budget=None,
                payable_cost_center=None)
            line.parent = None
            line.move = move
            line.type = 'financial'
            line.account, line.debit, line.credit = account, debit, credit
            if expense_data['party'] is not None:
                line.party = Party(expense_data['party'])
            line.compromise = expense_data.get('compromise')
            if line.account.party_required:
                if account.default_party:
                    line.party = account.default_party
                elif line.compromise is not None:
                    line.party = line.compromise.party
            line.budget = budget
            if expense_data['cost_center']:
                line.cost_center = expense_data['cost_center']
            if line.budget and not line.cost_center:
                line.on_change_budget()
            if account.kind == 'payable' and credit > 0:
                line.payable_budget = expense_data.get('payable_budget')
                line.payable_cost_center = expense_data.get(
                    'payable_cost_center')

            return line

        def set_budget_data(line):
            move = line.move
            budget_required = line.on_change_with_budget_required()
            with Transaction().set_context(
                    start_date=move.period.fiscalyear.start_date,
                    end_date=move.date):
                if budget_required:
                    if expense_data['compromise']:
                        line.compromise = Compromise(expense_data['compromise'])
                    allowed_budgets = line.get_allowed_budgets()
                    if allowed_budgets and not line.budget:
                        line.budget = allowed_budgets[0]
                    if line.budget and line.budget.type == 'expense':
                        if expense_data['cost_center']:
                            line.cost_center = expense_data['cost_center']
                        else:
                            costs = line.get_allowed_cost_centers()
                            if costs and not line.cost_center:
                                line.cost_center = \
                                    line.get_allowed_cost_centers()[0]  # noqa
                    if line.budget is None:
                        budget = (line.account.budget_debit or
                                  line.account.budget_credit)
                        self.raise_user_error('account_without_budget', {
                            'account': line.account.rec_name,
                            'budget': budget.rec_name,
                        })
                    if not line.cost_center:
                        line.cost_center = expense_data.get(
                            'payable_cost_center')
                else:
                    line.budget = None
                    line.cost_center = None
                    line.compromise = None

        line = add_line(move, account, debit, credit, budget)
        set_budget_data(line)
        lines = [line]
        if expense_data.get('previous_year_account'):
            for pya in expense_data['previous_year_account'].keys():
                for pya_year in expense_data[
                        'previous_year_account'].get(pya).keys():
                    amount = expense_data[
                        'previous_year_account'].get(pya).get(pya_year)
                    new_line_2 = add_line(move,
                        pya, amount if line.debit > 0 else line.debit,
                        amount if line.credit > 0 else line.credit)
                    set_budget_data(new_line_2)
                    lines.append(new_line_2)

            new_line_1 = add_line(move, line.account, line.credit, line.debit)
            set_budget_data(new_line_1)
            lines.append(new_line_1)

        return lines

    @classmethod
    def liquidation_query_get(cls):
        pool = Pool()
        # Liquidation = pool.get('contract.liquidation')
        LiquidationLine = pool.get('contract.liquidation.line')
        LiquidationRule = pool.get('contract.liquidation.rule')
        PayslipRule = pool.get('payslip.rule')
        Account = pool.get('account.account')
        Contract = pool.get('company.contract')
        Employee = pool.get('company.employee')
        context = Transaction().context

        liquidation = cls.__table__()
        liquidation_line = LiquidationLine.__table__()
        liquidation_rule = LiquidationRule.__table__()
        payslip_rule = PayslipRule.__table__()
        account = Account.__table__()
        deduction_account = Account.__table__()
        contract = Contract.__table__()
        employee = Employee.__table__()

        debit_columns = [
            liquidation.contract.as_('contract'),
            Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None).as_('party'),
            liquidation_line.expense_account.as_('account'),
            liquidation_line.budget.as_('budget'),
            Case(((liquidation_line.type_ == 'deduction')
                  & (account.kind == 'expense'),
            Literal(0.0) * liquidation_line.amount),
                else_=liquidation_line.amount).as_('debit'),
            Literal(0.0).as_('credit'),
            Literal(None).as_('parent'),
        ]

        credit_columns = [
            liquidation.contract.as_('contract'),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None)).as_('party'),
            liquidation_line.payable_account.as_('account'),
            liquidation_line.budget.as_('budget'),
            Literal(0.0).as_('debit'),
            liquidation_line.amount.as_('credit'),
            Case((account.kind == 'payable',
            liquidation_line.expense_account),
                else_=Literal(None)).as_('parent'),
        ]

        deduction_credit_columns = [
            liquidation.contract.as_('contract'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'liquidacion_decimo_tercero_cm')),
            Coalesce(payslip_rule.party, Case((account.party_required == True,
            (Coalesce(account.default_party, employee.party))),
                else_=None))), else_=None).as_('party'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'liquidacion_decimo_tercero_cm')),
            liquidation_line.payable_account),
                else_=None).as_('account'),
            Case((((payslip_rule.code == 'rmu') | (
                    payslip_rule.code == 'liquidacion_decimo_tercero_cm')),
            liquidation_line.budget),
                else_=None).as_('budget'),
            Literal(0.0).as_('debit'),
            Case(((liquidation_line.type_ == 'deduction')
                  & (deduction_account.kind == 'expense'),
            Literal(-1.0) * liquidation_line.amount),
                else_=Literal(0.0)).as_('credit'),
            Case((account.kind == 'payable',
            liquidation_line.expense_account),
                else_=Literal(None)).as_('parent'),
        ]

        where = Literal(True)
        if context.get('general'):
            where &= liquidation.general == context.get('general')

        if context.get('company'):
            where &= liquidation.company == context.get('company')

        if context.get('state'):
            where &= liquidation.state == context.get('state')
        if context.get('contract_liquidation'):
            where &= liquidation.id == context.get('contract_liquidation')

        exclude_lb = (~payslip_rule.is_law_benefit
                      | (payslip_rule.is_law_benefit
                         & ~payslip_rule.code.like('%accumulate'))
                      & (payslip_rule.code != 'xiii')
                      & (payslip_rule.code != 'xiv')
                      )

        debit_query = liquidation.join(liquidation_line,
            condition=liquidation.id == liquidation_line.liquidation
        ).join(contract,
            condition=contract.id == liquidation.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == liquidation_line.expense_account
        ).join(liquidation_rule,
            condition=liquidation_rule.id == liquidation_line.rule
        ).join(payslip_rule,
            condition=payslip_rule.id == liquidation_rule.associated_rule
        ).select(*debit_columns,
            where=(where & exclude_lb),
        )

        credit_query = liquidation.join(liquidation_line,
            condition=liquidation.id == liquidation_line.liquidation
        ).join(contract,
            condition=contract.id == liquidation.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == liquidation_line.payable_account
        ).join(liquidation_rule,
            condition=liquidation_rule.id == liquidation_line.rule
        ).join(payslip_rule,
            condition=payslip_rule.id == liquidation_rule.associated_rule
        ).select(*credit_columns,
            where=(where & exclude_lb),
        )

        deduction_credit_query = liquidation.join(liquidation_line,
            condition=liquidation.id == liquidation_line.liquidation
        ).join(contract,
            condition=contract.id == liquidation.contract
        ).join(employee,
            condition=employee.id == contract.employee
        ).join(account,
            condition=account.id == liquidation_line.payable_account
        ).join(deduction_account,
            condition=deduction_account.id == liquidation_line.expense_account
        ).join(liquidation_rule,
            condition=liquidation_rule.id == liquidation_line.rule
        ).join(payslip_rule,
            condition=payslip_rule.id == liquidation_rule.associated_rule
        ).select(*deduction_credit_columns,
            where=(where & exclude_lb),
        )

        deduction_credit_query = deduction_credit_query.select(
            deduction_credit_query.contract.as_('contract'),
            Max(deduction_credit_query.party).as_('party'),
            Max(deduction_credit_query.account).as_('account'),
            Max(deduction_credit_query.budget).as_('budget'),
            Sum(deduction_credit_query.debit).as_('debit'),
            Sum(deduction_credit_query.credit).as_('credit'),
            deduction_credit_query.parent.as_('parent'),
            group_by=[deduction_credit_query.contract,
                deduction_credit_query.parent]
        )

        union = Union(debit_query, credit_query, deduction_credit_query,
            all_=True)

        query = union.select(
            union.account,
            Sum(union.debit).as_('debit'),
            Sum(union.credit).as_('credit'), union.party, union.budget,
            Max(union.parent),
            group_by=(union.account, union.budget, union.party),
            order_by=[Sum(union.credit) > 0])

        return query

    def accummulate_query_get(cls):
        pool = Pool()
        context = Transaction().context
        Liquidation = pool.get('contract.liquidation')
        LawBenefitXIII = pool.get('contract.liquidation.law.benefits.xiii.lines')  # noqa
        LawBenefitXIV = pool.get('contract.liquidation.law.benefits.xiv.lines')
        LawBenefit = pool.get('payslip.law.benefit')

        PaysplipPayslip = pool.get('payslip.payslip')
        PaysplipGeneral = pool.get('payslip.general')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        # Account = pool.get('account.account')
        AccountClose = pool.get('account.account.close')
        # = pool.get('account.account')
        AccountReclassification = pool.get('account.account.reclassification')
        # = pool.get('account.account')

        Contract = pool.get('company.contract')
        PayslipTemplateAccountBudget = pool.get('payslip.template.account.budget')  # noqa
        PayslipRuleAccoutBudget = pool.get('payslip.rule.account.budget')
        PayslipRule = pool.get('payslip.rule')
        CompanyDepartmentTemplateBudget = pool.get('company.department.template.budget')  # noqa

        liquidation = Liquidation.__table__()
        law_benefit_xiii = LawBenefitXIII.__table__()
        law_benefit_xiv = LawBenefitXIV.__table__()
        law_benefit = LawBenefit .__table__()
        payslip_payslip = PaysplipPayslip.__table__()
        payslip_general = PaysplipGeneral.__table__()
        move = Move.__table__()
        move_line = MoveLine.__table__()

        account_close = AccountClose.__table__()
        account_reclassification = AccountReclassification.__table__()
        contract = Contract.__table__()
        payslip_template_account_budget = PayslipTemplateAccountBudget.__table__()  # noqa
        payslip_rule_account_budget = PayslipRuleAccoutBudget.__table__()

        payslip_rule = PayslipRule.__table__()
        company_department_template_budget = CompanyDepartmentTemplateBudget.__table__()  # noqa

        where = Literal(True)
        if context.get('general'):
            where &= liquidation.general == context.get('general')

        queryxiii = liquidation.join(law_benefit_xiii,
            condition=(liquidation.id == law_benefit_xiii.liquidation) &
                      (liquidation.id == cls.id)
        ).join(law_benefit,
            condition=law_benefit_xiii.xiii_line == law_benefit.id
        ).join(payslip_payslip, type_='LEFT',
            condition=law_benefit.generator_payslip == payslip_payslip.id
        ).join(payslip_general, type_='LEFT',
            condition=payslip_payslip.general == payslip_general.id
        ).join(move, type_='LEFT',
            condition=payslip_general.move_lb == move.id
        ).join(move_line, type_='LEFT',
            condition=move.id == move_line.move
        ).join(account_close, type_='LEFT',
            condition=(move_line.account == account_close.account) &
               (account_close.fiscalyear == payslip_payslip.fiscalyear)
        ).join(account_reclassification, type_='LEFT',
            condition=(account_close.to_account == account_reclassification.account) &  # noqa
               (account_reclassification.fiscalyear == cls.fiscalyear.id)
        ).select(
            Literal('xiii_accumulate').as_('rule'),
            liquidation.id.as_('contract_id'),
            law_benefit_xiii.xiii_line.as_('xiii_line'),
            payslip_payslip.fiscalyear.as_('fiscalyear'),
            law_benefit.id.as_('law_benefit_id'),
            law_benefit.amount.as_('law_benefit_amount'),
            law_benefit.kind.as_('law_benefit_kind'),
            move_line.id.as_('move_line_id'),
            move_line.account.as_('move_line_account'),
            account_close.to_account.as_('account_close'),
            account_reclassification.reclassification_account.as_('reclassification_account'),  # noqa
            where=(
                    (move_line.id == Null) |
                ((law_benefit.amount == move_line.credit) &
                (move_line.party == cls.contract.employee.party.id) &
                (payslip_payslip.fiscalyear != cls.fiscalyear.id))
            ),
        )
        queryxiv = liquidation.join(law_benefit_xiv,
            condition=(liquidation.id == law_benefit_xiv.liquidation) &
                      (liquidation.id == cls.id)
        ).join(law_benefit,
            condition=law_benefit_xiv.xiv_line == law_benefit.id
        ).join(payslip_payslip, type_='LEFT',
            condition=law_benefit.generator_payslip == payslip_payslip.id
        ).join(payslip_general, type_='LEFT',
            condition=payslip_payslip.general == payslip_general.id
        ).join(move, type_='LEFT',
            condition=payslip_general.move_lb == move.id
        ).join(move_line, type_='LEFT',
            condition=move.id == move_line.move
        ).join(account_close, type_='LEFT',
            condition=((
                move_line.account == account_close.account) & (
                account_close.fiscalyear == payslip_payslip.fiscalyear))
        ).join(account_reclassification, type_='LEFT',
            condition=(
                account_close.to_account == account_reclassification.account) &
            (account_reclassification.fiscalyear == cls.fiscalyear.id)
        ).select(
            Literal('xiv_accumulate').as_('rule'),
            liquidation.id.as_('contract_id'),
            law_benefit_xiv.xiv_line.as_('xiii_line'),
            payslip_payslip.fiscalyear.as_('fiscalyear'),
            law_benefit.id.as_('law_benefit_id'),
            law_benefit.amount.as_('law_benefit_amount'),
            law_benefit.kind.as_('law_benefit_kind'),
            move_line.id.as_('move_line_id'),
            move_line.account.as_('move_line_account'),
            account_close.to_account.as_('account_close'),
            account_reclassification.reclassification_account.as_('reclassification_account'),  # noqa
            where=(
                    (move_line.id == Null) |
                    ((law_benefit.amount == move_line.credit) &
                     (move_line.party == cls.contract.employee.party.id) &
                     (payslip_payslip.fiscalyear != cls.fiscalyear.id))
            ),
        )

        union_query = Union(queryxiii, queryxiv, all_=True)

        where = Literal(True)
        if context.get('general'):
            where &= contract.general == context.get('general')

        query_conf = contract.join(payslip_template_account_budget,
            condition=payslip_template_account_budget.id == contract.payslip_template_account_budget  # noqa
        ).join(payslip_rule_account_budget,
            condition=contract.payslip_template_account_budget == payslip_rule_account_budget.template_account_budget  # noqa
        ).join(payslip_rule,
            condition=payslip_rule_account_budget.payslip_rule == payslip_rule.id  # noqa
        ).join(company_department_template_budget, type_='LEFT',
            condition=(
                company_department_template_budget.department == contract.department) &  # noqa
                (
                company_department_template_budget.fiscalyear == cls.fiscalyear.id) &  # noqa
                (
                company_department_template_budget.template_budget == contract.payslip_template_account_budget)  # noqa
        ).select(
            contract.payslip_template_account_budget.as_('template'),
            payslip_rule.code.as_('rule'),
            contract.department.as_('department'),
            company_department_template_budget.activity.as_('activity'),
            payslip_rule_account_budget.expense_account.as_('expense_account'),
            payslip_rule_account_budget.payable_account.as_('payable_account'),
            company_department_template_budget.fiscalyear.as_('fiscalyear'),

            where=(
                (contract.id == cls.contract.id) &
                ((payslip_rule.code == 'xiii_accumulate') |
                    (payslip_rule.code == 'xiv_accumulate'))
            ),
        )

        final_query = union_query.join(query_conf, type_='LEFT',
            condition=query_conf.rule == union_query.rule
        ).select(
            union_query.rule.as_('rule'),
            union_query.contract_id.as_('contract_id'),
            union_query.xiii_line.as_('xiii_line'),
            union_query.fiscalyear.as_('fiscalyear'),
            union_query.law_benefit_id.as_('law_benefit_id'),
            union_query.law_benefit_amount.as_('law_benefit_amount'),
            union_query.law_benefit_kind.as_('law_benefit_kind'),
            union_query.move_line_id.as_('move_line_id'),
            union_query.move_line_account.as_('move_line_account'),
            union_query.account_close.as_('account_close'),
            union_query.reclassification_account.as_('reclassification_account'),  # noqa
            Case((union_query.fiscalyear == query_conf.fiscalyear,
                  query_conf.template),
                 else_=Literal(-1)).as_('template'),
            Case((union_query.fiscalyear == query_conf.fiscalyear,
                  query_conf.activity),
                 else_=Literal(-1)).as_('activity'),
            query_conf.expense_account.as_('expense_account'),
            query_conf.payable_account.as_('payable_account'),
            where=(union_query.contract_id == cls.id),
        )
        return final_query

    def missing_acummulate_values_query_get(cls):

        pool = Pool()
        context = Transaction().context
        Contract = pool.get('company.contract')
        PayslipTemplateAccountBudget = pool.get('payslip.template.account.budget')  # noqa
        PayslipRuleAccoutBudget = pool.get('payslip.rule.account.budget')
        PayslipRule = pool.get('payslip.rule')
        CompanyDepartmentTemplateBudget = pool.get('company.department.template.budget')  # noqa

        contract = Contract.__table__()
        payslip_template_account_budget = PayslipTemplateAccountBudget.__table__()  # noqa
        payslip_rule_account_budget = PayslipRuleAccoutBudget.__table__()
        # law_benefit = PayslipRuleAccoutBudget.__table__()
        payslip_rule = PayslipRule.__table__()
        company_department_template_budget = CompanyDepartmentTemplateBudget.__table__()  # noqa

        where = Literal(True)
        if context.get('general'):
            where &= contract.general == context.get('general')

        query_conf = contract.join(payslip_template_account_budget,
            condition=payslip_template_account_budget.id == contract.payslip_template_account_budget  # noqa
        ).join(payslip_rule_account_budget,
            condition=contract.payslip_template_account_budget == payslip_rule_account_budget.template_account_budget  # noqa
        ).join(payslip_rule,
            condition=payslip_rule_account_budget.payslip_rule == payslip_rule.id  # noqa
        ).join(company_department_template_budget,
            condition=(company_department_template_budget.department == contract.department) &  # noqa
                (company_department_template_budget.template_budget == contract.payslip_template_account_budget)  # noqa
        ).select(
            contract.payslip_template_account_budget.as_('template'),
            payslip_rule.code.as_('rule'),
            contract.department.as_('department'),
            company_department_template_budget.activity.as_('activity'),
            payslip_rule_account_budget.expense_account.as_('expense_account'),
            payslip_rule_account_budget.payable_account.as_('payable_account'),
            where=(
                (contract.id == cls.contract.id) &
                (company_department_template_budget.fiscalyear == cls.fiscalyear.id) &  # noqa
                ((payslip_rule.code == 'xiii_accumulate') |
                    (payslip_rule.code == 'xiv_accumulate'))
            ),
        )
        return query_conf

    def update_liquidation_line_account_budget(self):
        # Account Budget Rules
        account_budget_rules = self.get_all_rules_account_budget()

        pool = Pool()
        LiquidationLine = pool.get('contract.liquidation.line')
        transaction = Transaction()

        rules_without_budget = []
        line_to_save = []
        ls = []
        # lbs = []
        for l in self.lines:
            if l.amount == Decimal('0.0'):
                continue
            config = account_budget_rules.get(
                str(self.contract.id) + "-" + str(l.rule.associated_rule.id))
            if config is not None:
                l.expense_account = config.get('expense_account')
                l.payable_account = config.get('payable_account')
                l.budget = config.get('budget')
                if l.expense_account.kind == 'expense' and not l.budget:
                    rules_without_budget.append("%s - %s - %s - %s" % (
                        self.contract.id,
                        self.contract.department.name,
                        self.contract.work_relationship.name,
                        l.rule.name))
                if l.budget and l.budget.kind == 'view':
                    self.raise_user_error('budget_not_exist', {
                        'description': (" Contrato:%s - Departamento(%s):%s - Partida: %s - Regla(%s): %s "  # noqa
                            "Plantilla(%s): %s") % (
                                self.contract.id,
                                self.department.id,
                                self.department.name,
                                l.budget.rec_name,
                                l.rule.id,
                                l.rule.name,
                                self.payslip_template_account_budget.id,
                                self.payslip_template_account_budget.name)
                    })
                ls.append(l)
            else:
                if self.payslip_template_account_budget is None:
                    self.raise_user_error('contract_without_template')
                self.raise_user_error('rule_without_configuration', {
                    'rule': (" Contrato:%s - Departamento(%s):%s - Relación Laboral: %s - Regla(%s): %s "  # noqa
                        "Plantilla(%s): %s") % (
                            self.contract.id,
                            self.department.id,
                            self.department.name,
                            self.work_relationship.name,
                            l.rule.id,
                            l.rule.name,
                            self.payslip_template_account_budget.id,
                            self.payslip_template_account_budget.name
                        )
                })

        line_to_save.extend(ls)

        LiquidationLine.save(line_to_save)
        transaction.commit()

        if rules_without_budget:
            error_rules = ""
            for e in rules_without_budget:
                error_rules += e + "\n"
            self.raise_user_error('rules_without_budget', {
                'rules': error_rules
            })

    def get_all_rules_account_budget(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Liquidation = pool.get('contract.liquidation')
        DepartmentBudget = pool.get('company.department.template.budget')
        PayslipTemplateAccountBudget = pool.get(
            'payslip.template.account.budget')
        PayslipRuleAccountBudget = pool.get('payslip.rule.account.budget')
        Budget = pool.get('public.budget')
        FiscalYear = pool.get('account.fiscalyear')
        liquidation = Liquidation.__table__()
        dep_budget = DepartmentBudget.__table__()
        budget = Budget.__table__()
        pt_acc_budget = (
            PayslipTemplateAccountBudget.__table__())
        # Payslip Rule Account Budget
        pr_acc_budget = PayslipRuleAccountBudget.__table__()
        fiscalyear = FiscalYear.__table__()

        result = defaultdict(lambda: {lambda: None})

        query = liquidation.join(pt_acc_budget,
            condition=(
                pt_acc_budget.id == liquidation.payslip_template_account_budget)  # noqa
        ).join(pr_acc_budget,
            condition=(pr_acc_budget.template_account_budget == pt_acc_budget.id)  # noqa
        ).join(dep_budget, 'LEFT',
            condition=(
                (dep_budget.department == liquidation.department) &
                (dep_budget.template_budget == pt_acc_budget.id) &
                (dep_budget.work_relationship == liquidation.work_relationship)
            )
        ).join(fiscalyear,
            condition=(
                (fiscalyear.id == dep_budget.fiscalyear) &
                (fiscalyear.id == self.fiscalyear.id))
        ).join(budget, 'LEFT',
            condition=(
                (budget.parent == pr_acc_budget.budget) &
                (budget.activity == dep_budget.activity)  # &
            )
        ).select(
            pr_acc_budget.expense_account,
            pr_acc_budget.payable_account,
            Coalesce(budget.id, pr_acc_budget.budget).as_('budget'),
            pr_acc_budget.payslip_rule,
            liquidation.contract,
            order_by=[dep_budget.sequence.asc]
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result[f"{row['contract']}-{row['payslip_rule']}"] = {
                'expense_account': row['expense_account'],
                'payable_account': row['payable_account'],
                'budget': row['budget'],
            }
        return result

    @classmethod
    def delete(cls, liquidations):
        for liquidation in liquidations:
            # if not Transaction().context.get('general_payslip_cancel_state'):
            if liquidation.state in ['done', 'confirmed', 'cancel']:
                cls.raise_user_error('no_delete')
        super(ContractLiquidation, cls).delete(liquidations)

    def get_rec_name(self, name):
        if self.reason_finalize:
            return '%s' % (self.reason_finalize.name)
        else:
            return None


class LiquidationOvertimeLine(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.overtimes.line'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        states={
            'required': True,
        }, ondelete='CASCADE')
    overtime_line = fields.Many2One('hr_ec.overtime.line',
        'Línea de horas extra', states={
            'required': True,
            'readonly': True,
        }, ondelete='CASCADE')


class LiquidationLoans(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.loans.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    loan_line = fields.Many2One('company.employee.loan.line',
        'Línea de préstamo', required=True, states={'readonly': True})
    capital_amount = fields.Function(fields.Numeric('Cuota'),
        'on_change_with_capital_amount')
    value_applied = fields.Numeric('Total aplicado', digits=(16, 2), states={
        'required': True,
        'readonly': Eval('_parent_liquidation', {}).get('state') != 'draft',
    }, domain=[
        ('value_applied', '>=', 0),
        ('value_applied', '<=', Eval('capital_amount')),
    ])

    @fields.depends('loan_linea')
    def on_change_with_capital_amount(self, name=None):
        return self.loan_line.capital_amount


class LiquidationLawBenefitsXIII(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.law.benefits.xiii.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    xiii_line = fields.Many2One('payslip.law.benefit',
        'Líneas de XIII', required=True, states={
            'readonly': True,
        })

    @classmethod
    def __setup__(cls):
        super(LiquidationLawBenefitsXIII, cls).__setup__()
        cls._order.insert(0, ('xiii_line.period.name', 'DESC'))


class LiquidationLawBenefitsXIV(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.law.benefits.xiv.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    xiv_line = fields.Many2One('payslip.law.benefit',
        'Líneas de XIV', required=True, states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(LiquidationLawBenefitsXIV, cls).__setup__()
        cls._order.insert(0, ('xiv_line.period.name', 'DESC'))


class LiquidationCoomSub(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.commsub.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    comm_sub_line = fields.Many2One('company.commission.subrogation.detail',
        'Líneas de Encargos/Subrogaciones', required=True, states={
            'readonly': True})


class LiquidationPermission(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.permission.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    permission_line = fields.Many2One('hr_ec.permission',
        'Líneas de Permisos', required=True, states={'readonly': True})


class LiquidationAbsence(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.absence.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    absence_line = fields.Many2One('hr_ec.absence',
        'Líneas de Faltas', required=True, states={'readonly': True})


class LiquidationMedicalCertificate(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.certificates.lines'

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    medical_line = fields.Many2One('galeno.medical.certificate',
        'Líneas de Certicados médicos', required=True,
        states={'readonly': True})


class LiquidationLine(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.line'

    states_ = {
        'readonly': Eval('_parent_liquidation', {}).get('state') != 'draft'
    }

    liquidation = fields.Many2One('contract.liquidation', 'Liquidación',
        required=True, ondelete='CASCADE')
    rule = fields.Many2One('contract.liquidation.rule', 'Regla',
        states={
            'required': True,
            'readonly': True,
        })
    type_ = fields.Selection([
        ('income', 'Ingreso'),
        ('deduction', 'Deducción'),
        ('internal', 'Interno')
    ], 'Tipo de regla', required=True, sort=False)
    type_translated = type_.translated('type_')
    is_manual_entry = fields.Boolean("¿Es una entrada manual?",
        states={
            'readonly': True,
        }, help='Permitira que se manipule el valor generado en el rubro.')
    is_visible_date = fields.Boolean("Fechas en cálculos?",
        states={
            'readonly': True,
        })
    start_date = fields.Date('Fecha Inicio',
        states={
            'readonly': (Eval('_parent_liquidation', {}).get('state') != 'draft'),  # noqa
            'required': True,
            'invisible': ~Eval('is_visible_date'),
        }, depends=['is_visible_date'])
    end_date = fields.Date('Fecha Fin',
        states={
            'readonly': True,
            'invisible': ~Eval('is_visible_date'),
        }, required=True)
    amount = fields.Numeric('Valor', digits=(16, 2),
        states={
            'required': True,
            'readonly': (~Eval('is_manual_entry') |
                Bool(Eval('_parent_liquidation', {}).get('state') != 'draft')),
        }, depends=['is_manual_entry'])
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    budget = fields.Many2One('public.budget', 'Partida')

    @classmethod
    def __setup__(cls):
        super(LiquidationLine, cls).__setup__()

    @staticmethod
    def default_amount():
        return 0

    def get_amount_line_liquidation(self, name=None):
        if (self.liquidation and self.liquidation.contract and self.rule
                and self.start_date and self.end_date):
            contract = self.liquidation.contract
            NAMES['base_salary'] = Decimal(
                str(self.liquidation.fiscalyear.base_salary))
            NAMES['Decimal'] = Decimal
            NAMES['period'] = self.liquidation.period
            NAMES['salary'] = contract.salary
            NAMES['last_payslip'] = self.get_last_payslip(contract)
            NAMES['last_full_payslip'] = self.liquidation.last_full_payslip
            rd = relativedelta(contract.end_date, contract.start_date)
            NAMES['total_years_work'] = int("{0.years}".format(rd))

            NAMES['reason_liquidation'] = self.liquidation.reason_liquidation.name  # noqa

            NAMES['reason_finalize'] = self.liquidation.reason_finalize.name

            # Period days
            period_days = self.get_period_days()
            NAMES['period_days'] = period_days

            # Worked days
            worked_period_days = self.get_worked_days()
            NAMES['worked_period_days'] = worked_period_days

            # Absences
            # if self.get_worked_days():
            info_absences_permissions = self.get_all_absences()
            NAMES['permissions'] = info_absences_permissions['permissions']
            NAMES['absences_with_normative'] = (
                info_absences_permissions['total_absences_with_normative'])

            NAMES['permissions_all'] = (
                info_absences_permissions['total_permissions_all'])
            NAMES['absences'] = info_absences_permissions['total_absences']

            # Medical Certificate
            info_medical = self.get_all_medical_certificates_info()
            NAMES['medical_certificates_info'] = (
                info_medical['medical_certificates_info'])
            NAMES['days_disease'] = (
                info_medical['total_days_disease'])

            # Nocturne Surcharge
            nocturne_surcharge_hours = self.get_nocturne_surcharge_hours()
            NAMES['nocturne_surcharge_hours'] = nocturne_surcharge_hours

            # Commision Subrogation
            comms_subs = self.get_all_comms_subs_details()
            total_comms_subs = self.get_comms_subs_amounts(
                comms_subs['comms_subs'])
            NAMES['total_comms_subs'] = total_comms_subs

            comms_subs_amounts_by_type = (
                self.get_comms_subs_amounts_by_type(comms_subs['comms_subs']))
            NAMES['total_comms_subs_by_type'] = comms_subs_amounts_by_type

            # Overtimes
            overtimes = self.liquidation.get_all_overtime_lines(contract,
                                                                self.start_date,
                                                                self.end_date)
            NAMES['total_overtimes'] = overtimes['total_overtime']

            overtimes_amounts_by_type = (
                self.get_overtimes_amounts_by_type(
                    overtimes['ids_overtimes_line']))

            NAMES['total_overtimes_by_type'] = overtimes_amounts_by_type

            # FR
            get_info_law_benefits = self.get_info_law_benefits()
            NAMES['fr_have'] = get_info_law_benefits['have_fr']
            NAMES['payment_form_fr'] = get_info_law_benefits['payment_form_fr']

            if self.rule.code == 'xiii':
                # XIII
                NAMES['payment_form_xiii'] = get_info_law_benefits['payment_form_xiii']  # noqa
                # total_law_benefit_xiii = self.get_law_benefit_xiii()
                total_amount_xiii = (
                    self.liquidation.get_info_law_benefit_xiii(contract,
                        self.start_date, self.end_date))
                NAMES['xiii_accumulate'] = total_amount_xiii['total_amount_xiii']  # noqa

            if self.rule.code == 'xiv':
                # XIV
                NAMES['payment_form_xiv'] = get_info_law_benefits['payment_form_xiv']  # noqa
                # total_law_benefit_xiv = self.get_law_benefit_xiv()
                total_amount_xiv = (
                    self.liquidation.get_info_law_benefit_xiv(contract,
                        self.start_date, self.end_date))
                NAMES['xiv_accumulate'] = total_amount_xiv['total_amount_xiv']

            # Loans
            info_loans = self.get_total_loans(self.liquidation.loans)
            NAMES['total_loans'] = info_loans['total_loans']
            # LOANS Totals separated amounts by type
            total_loans_by_type = self.get_loans_amounts_by_type(
                self.liquidation.loans)
            NAMES['total_loans_by_type'] = total_loans_by_type

            # Vacations Pending
            total_vacation_pay = self.get_total_vacation_pay()
            NAMES['total_vacation_pay'] = total_vacation_pay

            # Manual entries
            manual_entries = self.liquidation.get_all_manual_entries(
                self.liquidation, self.liquidation.period.payslip_start_date,
                self.liquidation.contract.end_date)
            income_entries = manual_entries['income_entries']
            deduction_entries = manual_entries['deduction_entries']
            # Manual Entries Liquidation
            manual_entry_amounts = self.get_manual_entries_amounts_liquidation(
                [self.liquidation], (income_entries + deduction_entries))
            NAMES['manual_entry_amounts'] = (
                manual_entry_amounts[self.liquidation.id]
            )
            NAMES['total_income'] = self.liquidation.aux_total_income

            parser = evaluator.RuleParser(NAMES)
            condition = parser.eval_expr(self.rule.condition)

            result = Decimal('0.00')
            if condition:
                result = parser.eval_expr(self.rule.expression)

            if not result or result['value'] < 0:
                NAMES[self.rule.code] = Decimal('0.00')
                return 0

            NAMES[self.rule.code] = result['value']

            return Decimal(str(result['value'])).quantize(Decimal('0.00'))
        return None

    def get_period_days(self):
        period_days_info = get_default_period_days_info()

        if period_days_info['type'] == 'fixed':
            period_days = period_days_info['value']
        else:
            period_days = np.busday_count(
                self.liquidation.period.start_date,
                (self.liquidation.period.end_date + timedelta(days=1)),
                weekmask='1111111')
        return period_days

    def get_last_payslip(self, contract):
        pool = Pool()
        TemplateToTransparencyExports = pool.get('transparency.exports.template')
        codes_templates = TemplateToTransparencyExports.get_code_templates()
        Payslip = pool.get('payslip.payslip')

        payslips = Payslip.search([
            ('contract', '=', contract),
            ('state', '=', 'done'),
            ('template.accumulated_law_benefits', '=', False),
            ('template.code', 'in', codes_templates),
        ], order=[('period.end_date', 'DESC')])

        if payslips:
            return payslips[0]

        return None

    def get_worked_days(self):
        period_days = self.get_period_days()

        worked_days = ((self.end_date -
                        self.liquidation.period.start_date).days) + 1

        if self.liquidation.contract.end_date.month == 2:
            total_days_month = monthrange(
                self.liquidation.contract.end_date.year,
                self.liquidation.contract.end_date.month)[1]
            worked_days = round((worked_days * period_days) / total_days_month)

        if worked_days >= 30:
            worked_days = period_days

        return worked_days

    def get_total_vacation_pay(self):

        if self.liquidation.amount_dates:
            total_amount = Decimal('0.00')
            for date in self.liquidation.amount_dates:
                total_amount += date.amount_pay
            return total_amount
        return 0

    def get_rmu_in_year(self, contract_id, start_date, end_date):
        Payslip = Pool().get('payslip.payslip')

        payslips = Payslip.search_read([
            ('contract', '=', contract_id),
            ('period.start_date', '>=', start_date),
            ('period.end_date', '<=', end_date),
            ('state', '=', 'done'),
        ], fields_names=['total_value'])

        total_rmu = sum(value['total_value'] for value in payslips)

        return total_rmu

    def get_info_law_benefits(self):
        have_fr = False
        payment_form_fr = ''
        if self.liquidation.contract.have_fr:
            have_fr = True
            payment_form_fr = self.liquidation.contract.fr
        payment_form_xiii = self.liquidation.contract.xiii
        payment_form_xiv = self.liquidation.contract.xiv

        return {
            'have_fr': have_fr,
            'payment_form_fr': payment_form_fr,
            'payment_form_xiii': payment_form_xiii,
            'payment_form_xiv': payment_form_xiv,
        }

    def get_overtimes_amounts_by_type(self, overtimes):
        pool = Pool()
        OvertimeLine = pool.get('hr_ec.overtime.line')
        OvertimeType = pool.get('hr_ec.overtime.type')

        field_amount = 'total_with_salary'
        field_type = 'type_'
        # result = 0
        # for payslip in payslips:
        items = defaultdict(lambda: 0)
        fields = get_fields_values(
            OvertimeLine, overtimes, [field_amount, field_type])
        for field in fields:
            name_type = get_single_field_value(
                OvertimeType, field[field_type], 'description')
            amount = field[field_amount] if field[field_amount] else 0
            if name_type not in items:
                items[name_type] = amount
            else:
                items[name_type] += amount
        # result = items
        return items

    @classmethod
    def get_comms_subs_amounts_by_type(cls, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')
        CommSubType = pool.get('company.commission.subrogation.type')

        field_amount = 'amount'
        field_type = 'type_'
        # result = defaultdict(lambda: [])
        # for payslip in payslips:
        items = defaultdict(lambda: 0)
        fields = get_fields_values(
            CommSubDetail, comms_subs_details, [field_amount, field_type])
        for field in fields:
            name_type = get_single_field_value(
                CommSubType, field[field_type], 'name')
            amount = field[field_amount]
            if name_type not in items:
                items[name_type] = amount
            else:
                items[name_type] += amount
        # result[payslip.id] = items
        return items

    def get_all_comms_subs_details(self, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        CommSub = pool.get('company.commission.subrogation')
        Detail = pool.get('company.commission.subrogation.detail')
        PayslipCommSubDetail = pool.get(
            'payslip.payslip.commission.subrogation.detail')
        ActionStaff = pool.get('company.action.staff')
        comm_sub = CommSub.__table__()
        detail = Detail.__table__()
        payslip_comm_sub_detail = PayslipCommSubDetail.__table__()
        action_staff = ActionStaff.__table__()

        result = []
        payslip_period_end_date = self.liquidation.period.end_date
        payslip_period_start_date = (
                self.liquidation.period.start_date - timedelta(days=11))

        # Get possible comm/sub details to consider in this payslip
        table_A = comm_sub.join(detail,
            condition=(comm_sub.id == detail.commission)
        ).join(action_staff,
            condition=(action_staff.id == comm_sub.action_staff)
        ).select(
            action_staff.employee,
            detail.id.as_('comm_sub_detail'),
            detail.end_date,
            where=((comm_sub.state == 'done') &
                (action_staff.employee == self.liquidation.contract.employee.id) &  # noqa
                (detail.end_date <= payslip_period_end_date) &
                (detail.start_date >= payslip_period_start_date)
            )
        )
        # Get comm/sub details considered in other payslips
        table_B = payslip_comm_sub_detail.join(detail,
            condition=(detail.id == payslip_comm_sub_detail.comm_sub_detail)
        ).select(
            payslip_comm_sub_detail.comm_sub_detail.as_(
                'comm_sub_detail_aux'),
            payslip_comm_sub_detail.payslip,
            where=((payslip_comm_sub_detail.was_considered == 't') &
                   (detail.end_date <= self.liquidation.period.end_date)
            )
        )
        # Get comm/sub details to consider in this payslip
        query = table_A.join(table_B, type_='LEFT OUTER',
            condition=(
                table_A.comm_sub_detail == table_B.comm_sub_detail_aux)
        ).select(
            table_A.comm_sub_detail,
            where=((table_B.payslip == self.liquidation.id) |
                   (table_B.comm_sub_detail_aux == Null)
            )
        )
        # Execute
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result.append(row['comm_sub_detail'])
        return {
            'comms_subs': result,
        }

    def get_comms_subs_amounts(self, comms_subs_details):
        pool = Pool()
        CommSubDetail = pool.get('company.commission.subrogation.detail')

        field_name = 'amount'
        fields = get_fields_values(
            CommSubDetail, comms_subs_details, [field_name])
        amount = sum([field[field_name] for field in fields])
        return amount

    def get_all_absences(self, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Absence = pool.get('hr_ec.absence')
        Permission = pool.get('hr_ec.permission')
        Contract = pool.get('company.contract')
        PayslipPermission = pool.get('payslip.payslip.hr_ec.permission')
        PayslipAbsence = pool.get('payslip.payslip.hr_ec.absence')

        absence = Absence.__table__()
        permission = Permission.__table__()
        contract = Contract.__table__()
        payslip_permission = PayslipPermission.__table__()
        payslip_absence = PayslipAbsence.__table__()

        total_absences = 0
        total_absences_with_normative = 0
        permissions = 0
        total_permissions_all = 0

        # Absences that overlaps with medical certificates
        absences_overlaps_cerificates = (
            self.get_query_absences_overlaps_with_medical_certificates())

        # Permissions that overlaps with medical certificates
        query_permissions_overlaps_cerificates = (
            self.get_query_permissions_overlaps_with_medical_certificates())

        # Absences
        query_absence = absence.join(payslip_absence, type_='LEFT',
            condition=(absence.id == payslip_absence.absence) &
                      (payslip_absence.was_considered == True)
        ).join(contract,
            condition=(absence.employee == contract.employee)
        ).select(absence.id,
            Sum(absence.total_in_days).as_('total_absences'),
            Sum(absence.total_with_normative).as_(
                'total_absences_with_normative'),
            where=(~(absence.id.in_(absences_overlaps_cerificates))
                & (contract.id == self.liquidation.contract.id)
                & (contract.absence_apply == True)
                & (absence.end_date >= self.liquidation.period.payslip_start_date)  # noqa
                & (absence.end_date <= self.end_date)
                & (absence.is_paid_in_liquidation == False)
                & (absence.state == 'done')
                & (payslip_absence.absence == Null)),
            group_by=[absence.id]
        )
        cursor.execute(*query_absence)
        for row in cursor_dict(cursor):
            total_absences = (row['total_absences'])
            total_absences_with_normative = (
                row['total_absences_with_normative'])

        # Permissions with discount
        query_permission = permission.join(payslip_permission, type_='LEFT',
            condition=(permission.id == payslip_permission.permission) &
                      (payslip_permission.was_considered == True)
        ).join(contract,
            condition=permission.contract == contract.id
        ).select(
            permission.id,
            Sum(Case((permission.discount_to == 'payslip',
                      permission.duration_without_weekend),
                     else_=permission.duration_with_weekend)).as_(
                'permissions'),
            where=(~(permission.id.in_(
                        query_permissions_overlaps_cerificates))
                & (contract.absence_apply == True)
                & (permission.contract == self.liquidation.contract.id)
                & (permission.end_date >= self.liquidation.period.payslip_start_date)  # noqa
                & (permission.end_date <= self.end_date)
                & (permission.state == 'done')
                & (permission.is_paid_in_liquidation == False)
                & (permission.discount_to.in_(('payslip', 'license')))
                & (payslip_permission.permission == Null)),
            group_by=[permission.id]
        )
        cursor.execute(*query_permission)
        for row in cursor_dict(cursor):
            permissions = row['permissions']

        # All Permissions of all types
        query_permission = permission.join(payslip_permission, type_='LEFT',
            condition=(permission.id == payslip_permission.permission) &
                      (payslip_permission.was_considered == True)
        ).join(contract,
            condition=permission.contract == contract.id
        ).select(
            permission.id,
            Sum(permission.duration_without_weekend).as_(
                'total_permissions_all'),
            where=(~(permission.id.in_(query_permissions_overlaps_cerificates))
                & (contract.absence_apply == True)
                & (contract.id == self.liquidation.contract.id)
                & (permission.end_date >= self.liquidation.period.payslip_start_date)  # noqa
                & (permission.end_date <= self.end_date)
                & (permission.is_paid_in_liquidation == False)
                & (permission.state == "done")
                & (payslip_permission.permission == Null)),
            group_by=[permission.id]
        )
        cursor.execute(*query_permission)
        for row in cursor_dict(cursor):
            total_permissions_all = row['total_permissions_all']

        return {
            'total_absences': total_absences,
            'total_absences_with_normative': total_absences_with_normative,
            'permissions': permissions,
            'total_permissions_all': total_permissions_all
        }

    def get_all_medical_certificates_info(self, names=None):

        pool = Pool()
        MedicalCertificatePayslip = pool.get(
            'galeno.medical.certificate.payslip')
        PayslipPayslip = pool.get('payslip.payslip')

        period_days_info = get_default_period_days_info()
        period_days = period_days_info['value']

        period = self.liquidation.period

        payslip = PayslipPayslip.search([
            ('contract', '=', self.liquidation.contract.id),
            ('period', '=', self.liquidation.period.id),
        ])
        if payslip:
            _start_date = payslip[0].period.payslip_end_date + timedelta(days=1)
        else:
            _start_date = period.payslip_start_date
        _end_date = self.liquidation.contract.end_date
        p_start_date = _start_date if _start_date else period.start_date
        p_end_date = _end_date if _end_date else period.end_date

        with Transaction().set_context(start_date=p_start_date,
                                       end_date=p_end_date):

            if period_days_info['type'] != 'fixed':
                period_days = monthrange(period.end_date.year,
                                         period.end_date.month)[1]

            medical_certificates = []
            total_days_disease = 0
            try:
                medical_info = MedicalCertificatePayslip.search([
                    ('employee', '=', self.liquidation.contract.employee)
                ])
                if medical_info:
                    for med in medical_info:
                        medical_certificates.append({
                            'certificate_type_id':
                                med.certificate_type.id,
                            'certificate_type':
                                med.certificate_type.name,
                            'total_days': med.number_days_month,
                            'percentage': med.percentage_payslip,
                            'order_': med.order_
                        })
                        total_days_disease += (med.number_days_month)

                    # Verify that the days of illness or accident are
                    # greater than the days of the period; if not, an
                    # automatic adjustment is made
                    if total_days_disease > period_days:
                        med = medical_certificates[-1]
                        # It should not be removed every day of illness
                        # or accident, when its reach 1 the user should
                        # edit the field according to administrative
                        # decitions
                        while ((total_days_disease >
                                period_days) and (
                                       med['total_days'] > 1)):
                            med['total_days'] -= 1
                            total_days_disease -= 1
            except Exception:
                pass

        return {
            'medical_certificates_info': medical_certificates,
            'total_days_disease': total_days_disease
        }

    def get_query_absences_overlaps_with_medical_certificates(self):
        pool = Pool()
        Absence = pool.get('hr_ec.absence')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        absence = Absence.__table__()

        query_medical = absence.join(medical_certificate,
                condition=absence.employee == medical_certificate.employee
            ).select(
            absence.id,
            where=((absence.start_date >= medical_certificate.start_date) &
                   (absence.end_date <= medical_certificate.end_date) &
                   (absence.state == 'done') &
                   (medical_certificate.state == 'done') &
                   (medical_certificate.register_type == 'days') &
                   (absence.employee == self.liquidation.contract.employee.id) &
                   (absence.end_date >= self.start_date) &
                   (absence.end_date <= self.end_date)
                   )
        )
        return query_medical

    def get_query_permissions_overlaps_with_medical_certificates(self):
        pool = Pool()
        Permission = pool.get('hr_ec.permission')
        MedicalCertificate = pool.get('galeno.medical.certificate')

        medical_certificate = MedicalCertificate.__table__()
        permission = Permission.__table__()

        query_medical = permission.join(medical_certificate,
                condition=permission.employee == medical_certificate.employee
            ).select(
            permission.id,
            where=((permission.start_date >= medical_certificate.start_date) &
                (permission.end_date <= medical_certificate.end_date) &
                (permission.state == 'done') &
                (medical_certificate.state == 'done') &
                (medical_certificate.register_type == 'days') &
                (permission.employee ==
                self.liquidation.contract.employee.id) &
                (permission.end_date >= self.start_date) &
                (permission.end_date <= self.end_date)
            )
        )
        return query_medical

    def get_nocturne_surcharge_hours(self, names=None):
        # The nocturne surcharge is obtained only from those days that are part
        # of a work shift and in which the employee has registered his entry
        # marking
        result = Decimal('0.0')
        assistance_detail = self.get_assistance_detail()

        if assistance_detail:
            for dialing in assistance_detail:
                t = dialing.nocturne_surcharge
                h = t.hour + (t.minute / 60) + (t.second / 3600)
                if h > 0:
                    result += Decimal(str(h))

        return result

    def get_assistance_detail(self, names=None):
        pool = Pool()
        AssistanceConsultationDetail = pool.get(
            'company.assistance.consultation.detail')
        result = defaultdict(lambda: [])
        contract = self.liquidation.contract

        start_date = self.start_date
        end_date = self.end_date

        with Transaction().set_context(
                start_date=start_date,
                end_date=end_date,
                employee=contract.employee.id,
                company=contract.company.id):

            # If a contract ends before the cut-off date, or begins after
            # the cut-off date, the markings are considered in the start or
            # end range of the contract dates.
            if contract.start_date and contract.start_date > start_date:
                start_date = contract.start_date
            if contract.end_date and contract.end_date < end_date:
                end_date = contract.end_date

            items = AssistanceConsultationDetail.search([
                ('company', '=', contract.company),
                ('employee', '=', contract.employee),
                ('date', '>=', start_date),
                ('date', '<=', end_date),
                ('workshift', '!=', None)
            ])
            if items:
                result = [item for item in items]
        return result

    def get_total_loans(self, lines):
        result = Decimal('0.00')
        # ids_loans_line = []
        for line in lines:
            # ids_loans_line.append(line.loan_line.id)
            if line.value_applied:
                result += line.value_applied
        return {
            'total_loans': result,
            # 'ids_loans_line': ids_loans_line,
        }

    @classmethod
    def get_loans_amounts_by_type(cls, lines):
        items = defaultdict(lambda: 0)
        for line in lines:
            amount = line.value_applied
            if line.loan_line.type.name not in items and amount:
                items[line.loan_line.type.name] = amount
            elif amount:
                items[line.loan_line.type.name] += amount
        return items

    @classmethod
    def get_manual_entries_amounts_liquidation(cls, liquidations, list_entries):
        pool = Pool()
        Entry = pool.get('payslip.entry')
        EntryType = pool.get('payslip.entry.type')

        manual_entries_amounts = defaultdict(lambda: [])
        for liquidation in liquidations:
            entries = defaultdict(lambda: 0)

            # Manual entries
            if list_entries:
                fields = get_fields_values(
                    Entry, list_entries, ['type_', 'real_amount', 'amount'])
                for field in fields:
                    code_entry = get_single_field_value(
                        EntryType, field['type_'], 'code')
                    amount = field['real_amount']
                    # amount = amount if amount is None else field['amount']
                    if code_entry not in entries:
                        entries[code_entry] = amount
                    else:
                        entries[code_entry] += amount
            # Results
            manual_entries_amounts[liquidation.id] = entries
        return manual_entries_amounts


class ContractLiquidationPayslipEntry(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.payslip.entry'

    liquidation = fields.Many2One('contract.liquidation',
        'Liquidación Contrato', required=True, ondelete='CASCADE')
    entry = fields.Many2One('payslip.entry', 'Entrada manual', required=True,
        states={
            'readonly': True,
        })
    kind = fields.Selection([
        ('income', 'Ingreso'),
        ('deduction', 'Deducción'),
        ('internal', 'Interno')
    ], 'Tipo', required=True)
    was_considered = fields.Boolean('¿Fué considerado?')

    @staticmethod
    def default_was_considered():
        return True


class ContractLiquidationVacationLines(metaclass=PoolMeta):
    __name__ = 'contract.liquidation.vacation.lines'

    states_ = {
        'readonly': Eval('_parent_liquidation', {}).get('state') != 'draft'
    }

    liquidation = fields.Many2One('contract.liquidation',
        'Liquidación Contrato', required=True, ondelete='CASCADE')
    initial_date = fields.Date('Fecha inicial', required=True, states={
        'readonly': True,
    })
    end_date = fields.Date('Fecha fin', required=True, states={
        'readonly': True,
    })
    proportional_vacation = fields.Numeric('Vacaciones ganadas', digits=(16, 2),
        states={
            'readonly': True,
        }, required=True)
    additional = fields.Numeric('Días Adicionales', digits=(16, 2),
        states={
            'readonly': True,
        }, required=True)
    vacation_in_year = fields.Numeric('Total vacaciones (año)', states={
        'readonly': True,
    }, required=True)
    amount = fields.Numeric('Total ingresos', required=True, digits=(16, 2),
        states=states_, help='Si la relación laboral es CODIGO DE TRABAJO '
            'tomará el valor percibido durante todo el periodo anual, '
            'caso contrario tomará la ultima RMU percibida por el empleado.')
    pending_vacations = fields.Numeric('Vacaciones a pagar', required=True,
        digits=(16, 2), states=states_)
    amount_pay = fields.Function(fields.Numeric('Valor pagar', digits=(16, 2),
        required=True), 'on_change_with_amount_pay')

    @classmethod
    def __setup__(cls):
        super(ContractLiquidationVacationLines, cls).__setup__()
        # t = cls.__table__()
        # cls._sql_constraints += [
        #     ('before_year_uniq', Unique(t, t.liquidation, t.before_year),
        #      'Año inicial debe ser unico por liquidación'),
        #     ('after_year_uniq', Unique(t, t.liquidation, t.after_year),
        #      'Año fin debe ser unico por liquidación'),
        # ]
        pass

    # @fields.depends('liquidation',)
    # def on_change_amount(self, name=None):
    #     NAMES['line_rule_checked'] = 'vacations'
    #
    # @fields.depends('liquidation',)
    # def on_change_pending_vacations(self, name=None):
    #     NAMES['line_rule_checked'] = 'vacations'

    @fields.depends('liquidation', '_parent_liquidation.contract', 'amount',
                    'pending_vacations', 'additional', 'proportional_vacation')
    def on_change_with_amount_pay(self, name=None):
        if self.amount and self.pending_vacations and self.liquidation.contract:
            relationship_vacation = (
                self.liquidation.contract.work_relationship.vacation)
            months = int(365 / relationship_vacation)
            total_additional = (self.proportional_vacation + self.additional)
            if months != 24:
                months = 1
                total_additional = relationship_vacation

            result = (((self.amount / months) / total_additional) *
                      self.pending_vacations)

            return result
        else:
            return 0


class ConfigurationRulePerceived(metaclass=PoolMeta):
    __name__ = 'configuration.rule.perceived'

    rule = fields.Many2One('payslip.rule', 'Reglas de rol', required=True, )
