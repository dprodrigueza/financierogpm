import os
from jinja2 import Environment, FileSystemLoader
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import pandas as pd

PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_ENVIRONMENT = Environment(
    autoescape=False,
    loader=FileSystemLoader(os.path.join(PATH, 'templates')),
    trim_blocks=False)

def render_template(template_filename, context):
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)

def create_index_html(path_file, path_save):
    # fname = "insert_sectorial_activity_data.xml"
    param = []

    field_names = ['a', 'b', 'c', 'd', 'e']
    xls = pd.ExcelFile(path_file)


    for sheet_name in xls.sheet_names:

        data = pd.read_excel(path_file, names=field_names,
            dtype='object', header=0, skipfooter=0, sheetname=sheet_name )

        data = data.fillna(False)



        for row in data.itertuples():
            # print(row)
            if row.a != False and row.a != 'CARGO / ACTIVIDAD' and (
                    row.d != False and row.d != 'CÓDIGO IESS'):
                print(row.a)
                param.append(row)

    context = {
        'param': param
    }
    with open(path_save, 'w') as f:
        html = render_template('sectorial_template.xml', context)
        f.write(html)

if __name__ == "__main__":
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--path-file', dest='path_file', required=True,
                        help='ruta archivo')
    parser.add_argument('--path-save', dest='path_save', required=True,
                        help='ruta donde guardar')
    options = parser.parse_args()
    create_index_html(options.path_file, options.path_save)