import datetime

from trytond.model import ModelSQL, ModelView
from trytond.model import fields, sequence_ordered
from trytond.modules.hr_ec.company import ActionOfTheStaffSituationMixin
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, If, Bool
from trytond.transaction import Transaction

__all__ = ['CompanyDepartmentTemplateBudget', 'ActionOfTheStaffSituationActual',
    'ActionOfTheStaffSituationProposed', 'ActionOfTheStaff',
    'ActionOfTheStaffType']


class CompanyDepartmentTemplateBudget(sequence_ordered(), ModelSQL, ModelView):
    'Department Template Budget'
    __name__ = 'company.department.template.budget'
    _history = True

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
        ])
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año Fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    department = fields.Many2One('company.department', 'Departamento',
        domain=[
            ('company', '=', Eval('company'))
        ],
        depends=['company'],
        required=True, ondelete='RESTRICT')
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Relación laboral', required=True)
    template_budget = fields.Many2One('payslip.template.account.budget',
        'Plantilla Cuentas Partidas', required=True, ondelete='RESTRICT')
    activity = fields.Many2One('public.planning.unit', 'Actividad',
        required=True,
        domain=[
            ('type', '=', 'activity'),
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_sequence():
        return 1

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = datetime.datetime.today().date()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', now),
            ('end_date', '>=', now)
        ])
        if fiscalyears:
            return fiscalyears[0].id


class StaffSituationToBudgetMixin(ActionOfTheStaffSituationMixin):
    'Action of staff Situation to Budget'

    template_account_budget = fields.Many2One('payslip.template.account.budget',
        'Cuenta y Partida de Plantilla de Rol', states={
        'readonly': Bool(
            Eval('_parent_action_staff', {}).get('state') != 'draft'),
    },
        depends=['contract', 'department', 'work_relationship'])
    budget = fields.Function(fields.Many2One('public.budget', 'Partida',
        depends=['template_account_budget']),
        'on_change_with_budget')

    @classmethod
    def __setup__(cls):
        super(StaffSituationToBudgetMixin, cls).__setup__()

    @fields.depends('contract', 'department', 'work_relationship')
    def on_change_with_template_account_budget(self, name=None):
        return self.get_budget()

    @fields.depends('template_account_budget')
    def on_change_with_budget(self, name=None):
        if self.template_account_budget:
            for rule in self.template_account_budget.lines:
                if rule.payslip_rule and rule.payslip_rule.code in [
                    'rmu','smu','rmu_servicios_personales',
                    'smu_servicios_personales']:
                    return rule.budget.id
        return None

    def get_budget(self, from_contract=True):
        if self.contract:
            if from_contract and self.contract.payslip_template_account_budget:
                return self.contract.payslip_template_account_budget.id
            else:
                templates = self.department_budget(self.contract.department,
                    self.contract.work_relationship)
                if len(templates) == 1:
                    return templates[0].template_budget.id
                else:
                    return None
        elif not self.contract and self.department and self.work_relationship:
            templates = self.department_budget(self.department,
                self.work_relationship)
            if len(templates) == 1:
                return templates[0].template_budget.id
            else:
                return None
        else:
            return None

    def department_budget(self, department, work_relationship):
        pool = Pool()
        CD_Tem_Budget = pool.get('company.department.template.budget')
        templates = CD_Tem_Budget.search([
            ('department', '=', department.id),
            ('work_relationship', '=', work_relationship.id)
        ])
        return templates


class ActionOfTheStaffSituationActual(StaffSituationToBudgetMixin,
    metaclass=PoolMeta):
    __name__ = 'company.action.staff.situation.actual'


class ActionOfTheStaffSituationProposed(StaffSituationToBudgetMixin,
    metaclass=PoolMeta):
    __name__ = 'company.action.staff.situation.proposed'

    @fields.depends('contract', 'department', 'work_relationship')
    def on_change_with_template_account_budget(self, name=None):
        return self.get_budget(from_contract=(self.contract is not None))


class ActionOfTheStaff(metaclass=PoolMeta):
    __name__ = 'company.action.staff'

    @classmethod
    def __setup__(cls):
        super(ActionOfTheStaff, cls).__setup__()
        cls._error_messages.update({
            'not_template_account': (
                'Una de las situaciones (Actual, Propuesta) no tiene '
                'seleccionada una Cuenta y Partida de Plantilla de Rol, por '
                'ende afectará al campo partida.'),
        })

    @classmethod
    def done(cls, items):
        have_budget = False
        for item in items:
            if item.type_.require_budget:
                if item.actual_situation:
                    have_budget = cls.check_template_account_budget(
                        item.actual_situation)
                    if have_budget:
                        break
                if item.proposed_situation:
                    have_budget = cls.check_template_account_budget(
                        item.proposed_situation)
                    if have_budget:
                        break
        if have_budget:
            cls.raise_user_warning(
                'not_template_account', 'not_template_account', {})
        super(ActionOfTheStaff, cls).done(items)

    @classmethod
    def check_template_account_budget(cls, staff_situations):
        for situation in staff_situations:
            if not situation.template_account_budget:
                return True
        return False

    def get_new_situation(self, NewSituationModel, old_situation):
        new_situation = super(ActionOfTheStaff, self).get_new_situation(
            NewSituationModel, old_situation)
        new_situation.template_account_budget = (
            old_situation.template_account_budget)
        new_situation.budget = old_situation.budget
        return new_situation


class ActionOfTheStaffType(metaclass=PoolMeta):
    __name__ = 'company.action.staff.type'

    require_budget = fields.Boolean('Requiere definir partida?')

    @staticmethod
    def default_require_budget():
        return True
