from trytond.model import ModelSQL, ModelView, fields, ModelSingleton
from trytond.pyson import Eval, Bool


__all__ = ['PayslipSeveralConfiguration']


class PayslipSeveralConfiguration(ModelSingleton, ModelSQL, ModelView):
    'Payslip Several Configurations'
    __name__ = 'payslip.several.configurations'
    _history = True

    consider_periods = fields.Boolean('Considerar periodos fijos?')
    days_period = fields.Numeric('Dias por periodo',
        states={
            'readonly': ~Bool(Eval('consider_periods')),
            'required': Bool(Eval('consider_periods'))
        }, depends=['consider_periods'])
    totals_negative = fields.Boolean(
        'Permitir roles de pagos con totales negativos?')
    notification_email = fields.Many2One('notification.email',
        'Notificación de rol de pagos mensual')
    notification_email_payslip_advance = fields.Many2One('notification.email',
        'Notificación de anticipo de rol quincenal')
    notification_email_payslip_retired = fields.Many2One('notification.email',
        'Notificación de rol de pagos de jubilación patronal')
    rules_set_codes_from_names = fields.Boolean(
        'Generar el código a partir del nombre de la regla?')
    entry_type_set_codes_from_names = fields.Boolean(
        'Generar el código a partir del nombre del tipo de entrada?')
    payslip_template_set_codes_from_names = fields.Boolean(
        'Generar el código a partir del nombre de la plantilla de rol?')
    loan_fortnightly = fields.Boolean('Habilitar roles quincenales?')
    consider_finalized_contracts_during_payslip_period = fields.Boolean(
        'Considerar contratos FINALIZADOS durante el período de un rol '
        'mensual?',
        help='Si el check se activa, los contratos que hayan finalizado en el '
             'transcurso del período de un rol serán agregados al rol de pagos '
             'general y se pagarán hasta la fecha de culminación del contrato.'
             '\n\nSi el check no se activa, dichos contratos no se agregarán '
             'al rol de pagos general y, en la liquidación del contrato, '
             'deberán pagarse los valores correspondientes a los días '
             'laborados en su último período.')
    consider_finalized_contracts_during_payslip_period_accum = fields.Boolean(
        'Considerar contratos FINALIZADOS durante el período de un rol '
        'acumulado?',
        help='Si el check se activa, los contratos que hayan finalizado en el '
             'transcurso del período de un rol serán agregados al rol de pagos '
             'general y se pagarán los valores acumulados por concepto de '
             'décimos hasta la fecha de culminación del contrato.\n\nSi el '
             'check no se activa, dichos contratos no se agregarán al rol de '
             'pagos general y, en la liquidación del contrato, deberán pagarse '
             'los valores correspondientes.')
    consider_disabled_contracts_during_payslip_period_accum = fields.Boolean(
        'Considerar contratos INHABILITADOS durante el período de un rol '
        'acumulado?',
        help='Si el check se activa, los contratos que se hayan inhabilitado '
             'en el transcurso del período de un rol serán agregados al rol de '
             'pagos general y se pagarán los valores acumulados por concepto '
             'de décimos hasta la fecha de inhabilitación del contrato.\n\nSi '
             'el check no se activa, dichos contratos no se agregarán al rol '
             'de pagos general.')
    allow_multiple_general_payslips_with_same_template_and_period = (
        fields.Boolean('Permitir múltiples roles generales con la misma '
        'plantilla en el mismo periodo?',
        help='Si el check se activa, se podrán generar varios roles generales '
             'del mismo tipo en un mismo periodo.\n\nSi el check no se activa, '
             'el sistema mostrará un mensaje de alerta indicando que ya existe '
             'un rol general con la misma plantilla en el mismo período y no '
             'permitirá la creación del nuevo rol.'))

    @staticmethod
    def default_consider_periods():
        return True

    @staticmethod
    def default_days_period():
        return 30

    @staticmethod
    def default_totals_negative():
        return True

    @staticmethod
    def default_rules_set_codes_from_names():
        return True

    @staticmethod
    def default_entry_type_set_codes_from_names():
        return True

    @staticmethod
    def default_payslip_template_set_codes_from_names():
        return True

    @staticmethod
    def default_loan_fortnightly():
        return False

    @staticmethod
    def default_consider_finalized_contracts_during_payslip_period():
        return True

    @staticmethod
    def default_consider_finalized_contracts_during_payslip_period_accum():
        return True

    @staticmethod
    def default_consider_disabled_contracts_during_payslip_period_accum():
        return True

    @staticmethod
    def default_allow_multiple_general_payslips_with_same_template_and_period():
        return False

    @fields.depends('consider_periods', 'days_period')
    def on_change_consider_periods(self, names=None):
        if not self.consider_periods:
            self.days_period = None