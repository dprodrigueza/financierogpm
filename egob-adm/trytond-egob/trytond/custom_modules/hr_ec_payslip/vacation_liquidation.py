import functools
from collections import defaultdict
from datetime import datetime
from decimal import Decimal

from sql.aggregate import Sum, Max
from sql.conditionals import Coalesce, Case
from trytond.model import fields, ModelSQL, ModelView, Workflow, ModelSingleton
from trytond.modules.hr_ec_payslip import utils
from trytond.modules.public_ec.account import show_moves
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Or
from trytond.tools import grouped_slice, reduce_ids, Literal, cursor_dict
from trytond.transaction import Transaction

__all__ = ['VacationLiquidation', 'VacationLiquidationLines',
           'VacationLiquidationConfiguration']


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def date_field(string):
    return fields.Date(string,
        states={
            'readonly': True,
            })


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                cls.write(
                    [it for it in items
                        if not getattr(it, field)], {
                            field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class VacationLiquidation(Workflow, ModelSQL, ModelView):
    'Vacation Liquidation'
    __name__ = 'vacation.liquidation'
    _history = True

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True,
        })
    contract = fields.Many2One('company.contract', 'Contrato', domain=[
        ('state', '=', 'done'),
    ], states={
        'required': True,
        'readonly': Eval('state') != 'draft',
    })
    request_date = fields.Date('Fecha solicitud', states={
        'readonly': True,
    })
    vacation_pending = fields.Function(fields.Numeric(
        'Vacaciones pendientes', digits=(16, 2), states={
            'invisible': Eval('state') == 'done',
        }, depends=['state']),
        'on_change_with_vacation_pending')
    total_days = fields.Numeric('Total dias', digits=(16, 2), domain=[
        ('total_days', '>', 0),
    ], states={
        'required': True,
        'readonly': True,
    })
    amount_pay = fields.Numeric('Valor a pagar', digits=(16, 2), domain=[
        ('amount_pay', '>', 0),
    ], states={
        'required': True,
        'readonly': True,
    })
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('done', 'Aprobado'),
        ('cancel', 'Cancelar'),
    ], 'Estado', states={
        'required': True,
        'readonly': True,
    })
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], states={
            'readonly': True,
        })
    confirmed_by = employee_field('Confirmado por')
    confirmed_date = date_field('Fecha confirmada')
    done_by = employee_field('Aprobado por')
    done_date = date_field('Fecha aprobada')
    cancel_by = employee_field("Cancelado por")
    cancel_date = date_field("Fecha cancelación")
    reason_cancel = fields.Text('Razón cancelación', states={
        'required': False,
        'readonly': ~Eval('state').in_(['confirmed', 'done']) |
                    Eval('move_state').in_(['posted']),
    })
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': True,
        })
    position = fields.Many2One('company.position', 'Puesto',
        states={
            'readonly': True,
        })
    work_relationship = fields.Many2One('company.work_relationship',
        'Relación laboral', states={
            'readonly': True,
        })
    payslip_template_account_budget = fields.Many2One(
        'payslip.template.account.budget',
        'Cuentas y Partidas de Plantilla de Rol',
        states={
            'readonly': True,
        })

    lines_periods = fields.One2Many('vacation.liquidation.lines',
        'vacation_liquidation', 'Lineas periodos', states={
            'readonly': True,
        })
    rule_vacation_liquidation = fields.Many2One('payslip.rule', 'Regla',
        states={
            'readonly': True,
        }, help='Con este regla de rol se realizará el asiento.')

    move = fields.Many2One('account.move', 'Asiento liquidación vacaciones',
        states={
            'readonly': True,
        }, domain=[
            ('company', '=', Eval('company', -1)),
        ], depends=['company'])
    journal = fields.Many2One('account.journal', 'Diario',
        states={
            'invisible': ~Eval('state').in_(['done']),
            'readonly': Eval('move_state').in_(['posted']),
        }, depends=['state'])
    compromise = fields.Many2One('public.budget.compromise',
        'Compromiso Liquidacion', states={
            'invisible': ~Eval('state').in_(['done']),
            'readonly': Bool(Eval('budget_impact_direct')) |
                    Eval('move_state').in_(['posted']),
        }, ondelete='RESTRICT')
    budget_impact_direct = fields.Boolean('Impacto de Partida Directo',
        states={
            'invisible': (~Eval('state').in_(['done'])),
            'readonly': Eval('move_state').in_(['posted']),
        })
    move_state = fields.Function(
        fields.Selection([
            ('draft', 'Draft'),
            ('posted', 'Posted'),
        ], 'Estado Asiento Liquidación', states={
            'invisible': ~Eval('state').in_(['done']
            ),
        }), 'get_move_state',
        searcher='search_move_state')
    payable_account = fields.Many2One('account.account', 'Cuenta por pagar')
    expense_account = fields.Many2One('account.account', 'Cuenta de gasto')
    budget = fields.Many2One('public.budget', 'Partida')

    @classmethod
    def __setup__(cls):
        super(VacationLiquidation, cls).__setup__()
        # t = cls.__table__()
        # cls._sql_constraints += [
        #     ('liquidation_entry_uniq', Unique(t, t.contract, t.type_),
        #      'Debe ser único por Contrato y Tipo de entrada'),
        # ]
        cls._error_messages.update({
            'not_vacation': ('Al empleado %(employee)s no se pueden liquidar '
                '%(total_days)s dias porque solo tiene disponible '
                '%(vacation_pending)s dias de vacaciones. '),
            'need_reason': ('En el campo "Razón cancelación" ingrese el '
                'motivo por la cual desea cancelar este registro.'),
            'not_rule_config': ('Necesita configurar la regla para continuar '
                'con el proceso de liquidación de vacaciones.'),
            'posted_move': 'El asiento ya esta contabilizado.',
            'set_journal': 'Debe asignar el diario.',
            'set_compromise': 'Seleccione el compromiso o marque "Impacto '
                'de Partida Directo".',
            'rule_without_configuration': ('Regla sin configuracion para rol:\n'
                                           '%(rule)s.'),
        })

        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'done'),
            ('confirmed', 'cancel'),
            ('done', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': (Eval('state').in_(['draft', 'cancel', 'done'])),
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'cancel', 'done']),
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'cancel', 'draft']),
            },
            'cancel': {
                'invisible': Eval('state').in_(['cancel', 'draft']) |
                             Eval('move_state').in_(['posted']),
            },
            'post': {
                'invisible': Or(~Eval('state').in_(['done']),
                    Eval('move_state').in_(['posted']),
                ),
                'depends': ['state'],
            },
        })
        cls._order.insert(0, ('contract', 'DESC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_total_days():
        return 0

    @staticmethod
    def default_amount_pay():
        return 0

    @staticmethod
    def default_request_date():
        return datetime.today().date()

    @staticmethod
    def default_rule_vacation_liquidation():
        Configuration = Pool().get('vacation.liquidation.configuration')
        config = Configuration.search([()])
        if config:
            return config[0].rule_vacation_liquidation.id
        else:
            return None

    @classmethod
    def default_fiscalyear(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        return context.get(
            'fiscalyear',
            FiscalYear.find(context.get('company'), exception=False))

    @classmethod
    def search_move_state(cls, name, clause):
        return [('move.state',) + tuple(clause[1:])]

    @fields.depends('contract', 'lines_periods', 'request_date')
    def on_change_contract(self, name=None):
        if self.contract:
            self.department = self.contract.department
            self.position = self.contract.position
            self.work_relationship = self.contract.work_relationship
            self.payslip_template_account_budget = (
                self.contract.payslip_template_account_budget)
            PermissionConsulting = Pool().get('hr_ec.permission.consulting')
            VacationLiquidationLines = Pool().get('vacation.liquidation.lines')
            with Transaction().set_context(
                    contract=self.contract.id,
                    contract_end_date=self.request_date, ):
                permissions_consulting = PermissionConsulting.search([()])

                for pending in permissions_consulting:
                    new_line = VacationLiquidationLines()
                    new_line.vacation_liquidation = self
                    new_line.initial_date = pending.initial_date
                    new_line.end_date = pending.end_date
                    new_line.proportional_vacation = (
                        pending.proportional_vacation)
                    new_line.additional = pending.additional
                    # new_line.vacation_in_year = pending.vacation_in_year
                    new_line.amount = Decimal(str(
                        pending.amount)).quantize(Decimal('0.00'))
                    new_line.vacation_pay = Decimal('0.00')
                    new_line.amount_pay = (
                        new_line.on_change_with_amount_pay())
                    new_line.vacation_liquidation = None
                    self.lines_periods = self.lines_periods + (new_line,)
        else:
            self.lines_periods = []

    @fields.depends('lines_periods',)
    def on_change_with_amount_pay(self, name=None):
        if self.lines_periods:
            total_days = Decimal('0.00')
            for line in self.lines_periods:
                total_days += line.amount_pay
            return total_days
        else:
            return Decimal('0.00')

    @fields.depends('lines_periods',)
    def on_change_with_total_days(self, name=None):
        if self.lines_periods:
            total_amount = Decimal('0.00')
            for line in self.lines_periods:
                total_amount += line.vacation_pay
            return total_amount
        else:
            return Decimal('0.00')

    @fields.depends('contract', 'request_date')
    def on_change_with_vacation_pending(self, name=None):
        if self.contract:
            return self.get_days_vacation()
        return 0

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, vacations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirmed(cls, vacations):
        pool = Pool()
        Date = pool.get('ir.date')
        for vacation in vacations:
            vacation.confirmed_date = Date.today()
            vacation.check_total_days()
        cls.save(vacations)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def done(cls, vacations):
        pool = Pool()
        Date = pool.get('ir.date')
        for vacation in vacations:
            vacation.done_date = Date.today()
            vacation.check_total_days()
        cls.save(vacations)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    @set_employee('cancel_by')
    def cancel(cls, vacations):
        Date = Pool().get('ir.date')
        for vacation in vacations:
            if not vacation.reason_cancel:
                cls.raise_user_error('need_reason')
            vacation.cancel_date = Date.today()
        cls.save(vacations)

    @classmethod
    @ModelView.button
    def post(cls, liquidations):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        for liquidation in liquidations:
            if not liquidation.journal:
                cls.raise_user_error('set_journal')
            if ((not liquidation.budget_impact_direct
                 or liquidation.budget_impact_direct == False)
                and not liquidation.compromise):
                cls.raise_user_error('set_compromise')
            if liquidation.move and liquidation.move.state == 'posted':
                cls.raise_user_error('posted_move')
            if liquidation.move and liquidation.move.state == 'draft':
                MoveLine.delete(liquidation.move.lines)
            move = liquidation.get_liquidation_move()
            liquidation.move = move

        cls.save(liquidations)

    @classmethod
    def validate(cls, vacations):
        super(VacationLiquidation, cls).validate(vacations)
        for vacation in vacations:
            vacation.check_rule_liquidation()

    def check_total_days(self):
        if self.total_days > self.vacation_pending:
            self.raise_user_error('not_vacation', {
                'employee': self.contract.employee.rec_name,
                'total_days': self.total_days,
                'vacation_pending': self.vacation_pending,
            })

    def check_rule_liquidation(self):
        if not self.rule_vacation_liquidation:
            self.raise_user_error('not_rule_config', {})


    def get_days_vacation(self):

        Configuration = Pool().get('hr_ec.configuration')
        PermissionBalance = Pool().get('hr_ec.permission.balance')

        cursor = Transaction().connection.cursor()
        configuration = Configuration.search([])
        if configuration[0].have_cut:
            exist_cut_date = configuration[0].date_cut
        else:
            exist_cut_date = None
        if configuration[0].date_permissions:
            date_permissions = configuration[0].date_permissions
        else:
            date_permissions = None

        with Transaction().set_context(employee=self.contract.employee.id,
                                       end_date=self.request_date,
                                       exist_cut_date=exist_cut_date,
                                       date_permissions=date_permissions,
                                       department=None):
            query_vacation = PermissionBalance.table_query()

        cursor.execute(*query_vacation)

        row = cursor.fetchone()

        return row[16]

    @classmethod
    def get_move_state(cls, liquidations, name):
        ids = [l.id for l in liquidations]

        cursor = Transaction().connection.cursor()
        Move = Pool().get('account.move')
        move = Move.__table__()
        liquidation = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(liquidation.id, sub_ids)
            query = liquidation.join(move, 'LEFT',
                condition=(move.id == liquidation.move)
            ).select(
                liquidation.id,
                move.state,
                where=red_sql
            )
            cursor.execute(*query)

            result.update(dict(cursor.fetchall()))

        return result

    def get_liquidation_move(self):
        pool = Pool()
        Period = pool.get('account.period')
        CostCenter = pool.get('public.cost.center')
        LineMove = pool.get('account.move.line')

        self.update_vacation_liquidation_account_budget()

        Move = pool.get('account.move')

        default_cost_center, = CostCenter.search([])

        accounting_date = self.done_date

        period_id = Period.search([
            ('start_date', '<=', accounting_date),
            ('end_date', '>=', accounting_date)
        ])[0].id

        move = self.move if self.move else Move(number="#")
        move.type = 'financial'
        move.description = ('Liquidación de vacaciones del empleado %s' %
                            self.contract.employee.rec_name)
        move.journal = self.journal
        move.period = period_id
        move.date = accounting_date
        move.origin = self
        move.company = self.contract.company

        if not self.budget_impact_direct:
            move.compromises = [self.compromise]
            move.budget_impact_direct = False
        else:
            move.budget_impact_direct = True

        lines_debit = []
        lines_credit = []

        lines_debit += self.get_move_line(
            move,
            self.expense_account,
            self.amount_pay,
            0, {
                'compromise': self.compromise,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            }, self.budget, default_cost_center
        )
        lines_credit += self.get_move_line(
            move,
            self.payable_account,
            0,
            self.amount_pay, {
                'compromise': None,
                'cost_center': None,
                'party': self.contract.employee.party,
                'payable_budget': None,
                'payable_cost_center': None
            }, None,
        )

        lines_credit[-1].parent = lines_debit[-1]

        LineMove.save(lines_debit)
        LineMove.save(lines_credit)
        # show_moves([move])
        move.save()
        return move

    def get_move_line(self, move, account, debit=0, credit=0, expense_data={},
                      budget=None, cost_center=None, payable_move_line=None):
        if debit == 0 and credit == 0:
            return []
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        # Compromise = pool.get('public.budget.compromise')
        # CREAR ACCOUNT_ERROR!!!!
        Party = pool.get('party.party')
        if account.kind == 'view':
            self.raise_user_error('account_error', {
                'account': account.rec_name
            })

        def add_line(move, account, debit, credit, budget=None,
                     cost_center=None, payable_move_line=None):
            line = MoveLine()
            line.move = move
            line.type = 'financial'
            line.account, line.debit, line.credit = account, debit, credit
            if expense_data['party'] is not None:
                line.party = Party(expense_data['party'])
            line.compromise = expense_data.get('compromise')
            if line.account.party_required:
                if account.default_party:
                    line.party = account.default_party
                elif line.compromise is not None:
                    line.party = line.compromise.party
            line.budget = budget
            line.cost_center = cost_center
            line.payable_move_line = payable_move_line
            if account.kind == 'payable' and credit > 0:
                line.payable_budget = expense_data.get('payable_budget')
                line.payable_cost_center = expense_data.get(
                    'payable_cost_center')

            return line

        line = add_line(move, account, debit, credit, budget, cost_center, payable_move_line)
        lines = [line]

        return lines


    def update_vacation_liquidation_account_budget(self):
        # Account Budget Rules
        account_budget_rules = self.get_all_rules_account_budget()

        VacationLiquidation = Pool().get('vacation.liquidation')
        # transaction = Transaction()

        rules_without_budget = []

        rule = self.rule_vacation_liquidation
        config = account_budget_rules.get(f"{self.contract.id}-{rule.id}")

        if config is not None:
            self.expense_account = config.get('expense_account')
            self.payable_account = config.get('payable_account')
            self.budget = config.get('budget')
            if self.expense_account.kind == 'expense' and not self.budget:
                rules_without_budget.append("%s - %s - %s - %s" % (
                    self.contract.id,
                    self.contract.department.name,
                    self.contract.work_relationship.name,
                    rule.name))
            if self.budget and self.budget.kind == 'view':
                self.raise_user_error('budget_not_exist', {
                    'description': " Contrato:%s - Departamento(%s):%s - Partida: %s - Regla(%s): %s "
                                   "Plantilla(%s): %s"% (
                        self.contract.id,
                        self.department.id,
                        self.department.name,
                        self.budget.rec_name,
                        rule.id,
                        rule.name,
                        self.payslip_template_account_budget.id,
                        self.payslip_template_account_budget.name)
                })
            VacationLiquidation.save([self])
        else:
            self.raise_user_error('rule_without_configuration', {
                'rule': " Contrato:%s - Departamento(%s):%s - Relación Laboral: %s - Regla(%s): %s "
                        "Plantilla(%s): %s" % (
                            self.contract.id,
                            self.department.id,
                            self.department.name,
                            self.work_relationship.name,
                            rule.id,
                            rule.name,
                            self.payslip_template_account_budget.id,
                            self.payslip_template_account_budget.name
                        )
            })

        # transaction.commit()

        if rules_without_budget:
            error_rules = ""
            for e in rules_without_budget:
                error_rules += e + "\n"
            self.raise_user_error('rules_without_budget', {
                'rules': error_rules
            })

    def get_all_rules_account_budget(self):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        VacationLiquidation = pool.get('vacation.liquidation')
        DepartmentBudget = pool.get('company.department.template.budget')
        PayslipTemplateAccountBudget = pool.get(
            'payslip.template.account.budget')
        PayslipRuleAccountBudget = pool.get('payslip.rule.account.budget')
        Budget = pool.get('public.budget')
        FiscalYear = pool.get('account.fiscalyear')
        vacation_liquidation = VacationLiquidation.__table__()
        dep_budget = DepartmentBudget.__table__()
        budget = Budget.__table__()
        pt_acc_budget = (
            PayslipTemplateAccountBudget.__table__())
        # Payslip Rule Account Budget
        pr_acc_budget = PayslipRuleAccountBudget.__table__()
        fiscalyear = FiscalYear.__table__()

        result = defaultdict(lambda: {lambda: None})

        query = vacation_liquidation.join(pt_acc_budget,
            condition=(
                pt_acc_budget.id == vacation_liquidation.payslip_template_account_budget)  # noqa
        ).join(pr_acc_budget,
            condition=(pr_acc_budget.template_account_budget == pt_acc_budget.id)  # noqa
        ).join(dep_budget, 'LEFT',
            condition=(
                # (dep_budget.department == contract.department) &
                (dep_budget.template_budget == pt_acc_budget.id) &
                (dep_budget.work_relationship == vacation_liquidation.work_relationship)
            )
        ).join(fiscalyear,
            condition=(
                (fiscalyear.id == dep_budget.fiscalyear) &
                (fiscalyear.id == self.fiscalyear.id))
        ).join(budget, 'LEFT',
            condition=(
                (budget.parent == pr_acc_budget.budget) &
                (budget.activity == dep_budget.activity) #&
            )
        ).select(
            pr_acc_budget.expense_account,
            pr_acc_budget.payable_account,
            Coalesce(budget.id, pr_acc_budget.budget).as_('budget'),
            pr_acc_budget.payslip_rule,
            vacation_liquidation.contract
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            result[f"{row['contract']}-{row['payslip_rule']}"] = {
                'expense_account': row['expense_account'],
                'payable_account': row['payable_account'],
                'budget': row['budget'],
            }
        return result


class VacationLiquidationLines(ModelSQL, ModelView):
    'Vacation Liquidation Lines'
    __name__ = 'vacation.liquidation.lines'
    _history = True

    states_ = {
        'readonly': Eval('_parent_vacation_liquidation', {}).get('state') != 'draft'
    }

    vacation_liquidation = fields.Many2One('vacation.liquidation',
        'Liquidación vacaciones', required=True, ondelete='CASCADE')
    initial_date = fields.Date('Fecha inicial', required=True, states={
        'readonly': True,
    })
    end_date = fields.Date('Fecha fin', required=True, states={
        'readonly': True,
    })
    proportional_vacation = fields.Numeric('Vacaciones ganadas', digits=(16, 2),
        states={
            'readonly': True,
        }, required=True)
    additional = fields.Numeric('Días Adicionales', digits=(16, 2),
        states={
            'readonly': True,
        }, required=True)
    amount = fields.Numeric('Total ingresos', required=True, digits=(16, 2),
        states=states_, help='Si la relación laboral es CODIGO DE TRABAJO '
             'tomará el valor percibido durante todo el periodo anual, '
             'caso contrario tomará la ultima RMU percibida por el empleado.')
    vacation_pay = fields.Numeric('Vacaciones a pagar (dias)', required=True,
        digits=(16, 2), states=states_)
    amount_pay = fields.Function(fields.Numeric('Valor pagar', digits=(16, 2),
        required=True), 'on_change_with_amount_pay')


    @fields.depends('_parent_vacation_liquidation.contract', 'vacation_pay',
                    'proportional_vacation', 'additional', 'amount')
    def on_change_with_amount_pay(self, name=None):
        if self.vacation_liquidation.contract and self.vacation_pay:
            relationship_vacation = (
                self.vacation_liquidation.contract.work_relationship.vacation)
            months = int(365 / relationship_vacation)
            total_additional = (self.proportional_vacation + self.additional)
            if months != 24:
                months = 1
                total_additional = relationship_vacation

            result = (((self.amount / months) / total_additional) *
                      self.vacation_pay)

            return result
        else:
            return 0


class VacationLiquidationConfiguration(ModelSingleton, ModelSQL, ModelView):
    'Vacation Liquidation Configuration '
    __name__ = 'vacation.liquidation.configuration'
    _history = True

    rule_vacation_liquidation = fields.Many2One('payslip.rule', 'Regla de rol',
        states={
            'required': True,
        }, ondelete='CASCADE', help='Seleccionar la regla de rol que va '
        'afectar en el asiento de la liquidación de vacaciones.')


    def get_rec_name(self, name):
        return '%s' % (self.rule_vacation_liquidation.name)
