from collections import defaultdict

from trytond.model import fields, Workflow, ModelSQL, ModelView, Unique
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.tools import reduce_ids, grouped_slice, datetime
from trytond.transaction import Transaction

_STATES = {
    'readonly': Eval('state') != 'draft',
    'required': Eval('state') == 'draft',
}
_DEPENDS = ['state']


class Complain(Workflow, ModelSQL, ModelView):
    'Complain'
    __name__ = 'performance_evaluation.complain'

    employee = fields.Many2One('company.employee', 'Employee', states=_STATES,
        depends=_DEPENDS)
    code = fields.Char('Form Number', readonly=True, select=True)
    complainant = fields.Char('Complainant', states=_STATES, depends=_DEPENDS)
    description = fields.Char('Description', states=_STATES, depends=_DEPENDS)
    date = fields.Date('Date', states=_STATES, depends=_DEPENDS)
    in_evaluation = fields.Function(fields.Boolean('Include in Evaluation'),
        'get_in_evaluation', searcher='search_in_evaluation')
    evaluation = fields.Function(
        fields.Many2One('performance_evaluation.evaluation', 'Evaluation'),
        'get_evaluation', searcher='search_evaluation')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
    ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Complain, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
        ))
        cls._buttons.update(
            {
                'draft': {
                    'invisible':
                        Eval('state').in_(['draft', 'done'])
                },
                'done': {
                    'invisible': Eval('state').in_(['done'])
                },
            }
        )

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_date(cls):
        Date = Pool().get('ir.date')
        return Transaction().context.get('date') or Date.today()

    def get_rec_name(self, name):
        if self.code and self.date:
            return '%s (%s)' % (self.code, self.date)
        else:
            return (self.code or str(self.id))

    def get_complain_lines(self, complain):
        pool = Pool()
        ComplainLine = pool.get('performance_evaluation.complain_line')
        complainline = ComplainLine()
        complainline.complain = complain
        return complainline

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, complains):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, complains):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('performance_evaluation.configuration')
        config = Config(1)
        for complain in complains:
            if complain.code:
                continue
            complain.code = Sequence.get_id(config.complain_sequence.id)
        cls.save(complains)

    @classmethod
    def get_in_evaluation(cls, complains, name):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        ComplainLine = pool.get('performance_evaluation.complain_line')
        Evaluation = pool.get('performance_evaluation.evaluation')
        eval_table = cls.__table__()
        cl = ComplainLine.__table__()
        evaluation = Evaluation.__table__()
        result = defaultdict(lambda: False)
        ids = [c.id for c in complains]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(eval_table.id, sub_ids)
            query = eval_table.join(cl,
                condition=eval_table.id == cl.complain
            ).join(evaluation,
                condition=evaluation.id == cl.evaluation
            ).select(
                cl.complain,
                where=red_sql)
            cursor.execute(*query)
            for id, in cursor.fetchall():
                result[id] = True
        return result

    @classmethod
    def search_in_evaluation(cls, name, clause):
        pool = Pool()
        ComplainLine  = pool.get('performance_evaluation.complain_line')
        Evaluation = pool.get('performance_evaluation.evaluation')
        eval_table = cls.__table__()
        cl = ComplainLine.__table__()
        evaluation = Evaluation.__table__()
        query = eval_table.join(cl,
            condition=eval_table.id == cl.complain
        ).join(evaluation,
            condition=evaluation.id == cl.evaluation
        ).select(
            cl.complain)
        operator = 'in' if clause[2] else 'not in'
        return [('id', operator, query)]

    @classmethod
    def get_evaluation(cls, complains, name):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        ComplainLine  = pool.get('performance_evaluation.complain_line')
        Evaluation = pool.get('performance_evaluation.evaluation')
        eval_table = cls.__table__()
        cl = ComplainLine.__table__()
        evaluation = Evaluation.__table__()
        result = defaultdict(lambda: None)
        ids = [b.id for b in complains]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(eval_table.id, sub_ids)
            query = eval_table.join(cl,
                condition=eval_table.id == cl.complain
            ).join(evaluation,
                condition=evaluation.id == cl.evaluation
            ).select(
                cl.complain,
                cl.evaluation,
                where=red_sql)
            cursor.execute(*query)
            for id, eval_id in cursor.fetchall():
                result[id] = eval_id
        return result

    @classmethod
    def search_evaluation(cls, name, clause):
        # TODO Change Return type to Evaluation code or date
        pool = Pool()
        ComplainLine = pool.get('performance_evaluation.complain_line')
        _, operator, value = clause
        lines = ComplainLine.search([
            ('evaluation.code', operator, value),
        ])
        complains = [l.complain.id for l in lines]
        return [
            ('id', 'in', complains)
            ]