# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from . import performance_evaluation
from . import template
from . import configuration
from . import complain
from . import report

from trytond.pool import Pool

__all__ = ['register']


def register():
    Pool.register(
        performance_evaluation.JobActivity,
        performance_evaluation.Knowledge,
        performance_evaluation.Competency,
        performance_evaluation.CompetencyRelevance,
        performance_evaluation.UniversalCompetency,
        performance_evaluation.UniversalCompetencyRelevance,
        performance_evaluation.AdditionalSkill,
        performance_evaluation.AdditionalSkillRelevance,
        performance_evaluation.Period,
        performance_evaluation.Evaluation,
        performance_evaluation.JobActivityLine,
        performance_evaluation.KnowledgeLine,
        performance_evaluation.CompetencyLine,
        performance_evaluation.UniversalCompetencyLine,
        performance_evaluation.AdditionalSkillLine,
        performance_evaluation.ComplainLine,
        performance_evaluation.Rating,
        performance_evaluation.RatingScale,
        performance_evaluation.RatingAdditionalSkill,
        performance_evaluation.RatingScaleAdditionalSkill,
        performance_evaluation.RatingResult,
        performance_evaluation.RatingScaleResult,
        template.EvaluationTemplate,
        template.UniversalCompetencyLineTemplate,
        template.AdditionalSkillLineTemplate,
        template.CreateEvaluationSucceed,
        template.CreateEvaluationStart,
        complain.Complain,
        configuration.Configuration,
        configuration.ConfigurationSequence,
        report.EvaluationReportStart,
        report.EvaluationFinalReportStart,
        report.EvaluationListReportStart,
        module='performance_evaluation', type_='model')
    Pool.register(
        template.CreateEvaluationWizard,
        report.EvaluationReportWizard,
        report.EvaluationFinalReportWizard,
        report.EvaluationListReportWizard,
        module='performance_evaluation', type_='wizard')
    Pool.register(
        report.EvaluationReport,
        report.EvaluationFinalReport,
        report.EvaluationListReport,
        module='performance_evaluation', type_='report')
