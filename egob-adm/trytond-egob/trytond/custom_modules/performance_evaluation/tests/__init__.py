# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.performance_evaluation.tests.test_performance_evaluation import suite
except ImportError:
    from .test_performance_evaluation import suite

__all__ = ['suite']
