from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.modules.company.model import CompanyValueMixin, \
    CompanyMultiValueMixin
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['Configuration', 'ConfigurationSequence']

def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(
    ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Performance Evaluation Configuration'
    __name__ = 'performance_evaluation.configuration'
    complain_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Complain Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'performance_evaluation.complain'),
        ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'complain_sequence':
            return pool.get('performance_evaluation.configuration_sequence')
        return super(Configuration, cls).multivalue_model(field)

    default_complain_sequence = default_func('complain_sequence')


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    'Performance Evaluation Configuration Sequence'
    __name__ = 'performance_evaluation.configuration_sequence'
    complain_sequence = fields.Many2One(
        'ir.sequence', 'Complain Sequence', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'performance_evaluation.complain'),
        ],
        depends=['company'])

    @classmethod
    def default_complain_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('complain', 'sequence_complain')
        except KeyError:
            return None

