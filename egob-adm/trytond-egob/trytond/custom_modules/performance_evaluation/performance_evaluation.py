from decimal import Decimal

from sql import Null

from trytond.model import fields, Workflow, ModelSQL, ModelView, Unique
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, If
from trytond.tools import reduce_ids, grouped_slice, datetime

__all__ = ['JobActivity', 'Knowledge',
           'Competency', 'CompetencyRelevance', 'UniversalCompetency',
           'UniversalCompetencyRelevance', 'AdditionalSkill',
           'AdditionalSkillRelevance', 'Period', 'Evaluation',
           'JobActivityLine', 'KnowledgeLine', 'CompetencyLine',
           'UniversalCompetencyLine', 'AdditionalSkillLine',
           'Rating', 'RatingScale', 'RatingAdditionalSkill',
           'RatingScaleAdditionalSkill', 'RatingResult', 'RatingScaleResult',
           'ComplainLine']

_STATES = {
    'readonly': Eval('state') != 'draft',
    'required': Eval('state') == 'draft',
}
_DEPENDS = ['state']


class JobActivity(ModelSQL, ModelView):
    'Job Activity'
    __name__ = 'performance_evaluation.job_activity'

    name = fields.Char('Activity Description', required=True)
    type = fields.Selection([
        ('general', 'General'),
        ('specific', 'Specific'),
    ], 'Type', required=True)

    @classmethod
    def default_type(cls):
        return 'specific'


class Knowledge(ModelSQL, ModelView):
    'Knowledge'
    __name__ = 'performance_evaluation.knowledge'
    name = fields.Char('Knowledge', required=True)
    type = fields.Selection([
        ('general', 'General'),
        ('specific', 'Specific'),
    ], 'Type', required=True)

    @classmethod
    def default_type(cls):
        return 'specific'

class Competency(ModelSQL, ModelView):
    'Competency'
    __name__ = 'performance_evaluation.competency'
    name = fields.Char('Skill', required=True)
    relevancies = fields.One2Many('performance_evaluation.competency_relevance',
        'competency', 'Relevance Indicators')


class CompetencyRelevance(ModelSQL, ModelView):
    'Relevance'
    __name__ = 'performance_evaluation.competency_relevance'
    competency = fields.Many2One('performance_evaluation.competency',
        'Competency', ondelete='CASCADE')
    name = fields.Selection([
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', required=True, sort=False)
    description = fields.Text('Description', required=True)


class UniversalCompetency(ModelSQL, ModelView):
    'UniversalCompetency'
    __name__ = 'performance_evaluation.universal_competency'
    name = fields.Char('Skill', required=True)
    relevancies = fields.One2Many(
        'performance_evaluation.universal_competency_relevance',
        'universal_competency', 'Relevance Indicators', readonly=True)


class UniversalCompetencyRelevance(ModelSQL, ModelView):
    'UniversalCompetencyRelevance'
    __name__ = 'performance_evaluation.universal_competency_relevance'

    universal_competency = fields.Many2One(
        'performance_evaluation.universal_competency',
        'UniversalCompetency', ondelete='CASCADE')
    name = fields.Selection([
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', required=True, sort=False)
    description = fields.Text('Description', required=True)


class AdditionalSkill(ModelSQL, ModelView):
    'Additional Skill'
    __name__ = 'performance_evaluation.additional_skill'
    name = fields.Char('Description', required=True)
    relevancies = fields.One2Many(
        'performance_evaluation.additional_skill_relevance',
        'additional_skill', 'Relevance Indicators', readonly=True)


class AdditionalSkillRelevance(ModelSQL, ModelView):
    'AdditionalSkillRelevance'
    __name__ = 'performance_evaluation.additional_skill_relevance'
    additional_skill = fields.Many2One(
        'performance_evaluation.additional_skill', 'AdditionalSkill',
        ondelete='CASCADE')
    name = fields.Selection([
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', required=True, sort=False)
    description = fields.Text('Description', required=True)


class Period(Workflow, ModelSQL, ModelView):
    'Period'
    __name__ = 'performance_evaluation.period'
    state = fields.Selection([
        ('draft', 'Draft'),
        ('in_process', 'In process'),
        ('done', 'Done'),
    ], 'State', readonly=True)
    company = fields.Many2One('company.company', 'Company', states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
             Eval('context', {}).get('company', -1)),
        ], depends=_DEPENDS,)
    name = fields.Char('Period Name', states=_STATES, depends=_DEPENDS)
    start_date = fields.Date('Start Date', states=_STATES,
        domain=[
            ('start_date', '<', Eval('end_date'))
        ], depends=['end_date', 'state'])
    end_date = fields.Date('End Date', required=True, states=_STATES,
        domain=[
            ('end_date', '>', Eval('start_date'))
        ], depends=['start_date', 'state'])
    period_sequence = fields.Many2One('ir.sequence', 'Period Sequence',
        states=_STATES,
        domain=[
            ('code', '=', 'performance_evaluation.period'),
        ])
    evaluations = fields.One2Many('performance_evaluation.evaluation',
        'period', 'Evaluations', readonly=True)
    scale_job_activity = fields.Many2One('performance_evaluation.rating',
        'Job Activity Lines Scale',
        states={
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('type', '=', 'compliance_level'),
            ('period', '=', Eval('id')),
        ], depends=_DEPENDS)
    scale_knowledge = fields.Many2One('performance_evaluation.rating',
        'Knowledge Lines Scale',
        states={
          'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('type', '=', 'knowledge_level'),
            ('period', '=', Eval('id')),
        ], depends=_DEPENDS)
    scale_competency = fields.Many2One(
        'performance_evaluation.rating', 'Competency Lines Scale',
        states={
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('type', '=', 'development_level'),
            ('period', '=', Eval('id')),
        ], depends=_DEPENDS)
    scale_universal_competency = fields.Many2One(
        'performance_evaluation.rating', 'Universal Competency Lines Scale',
        states={
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('type', '=', 'frequency_competency'),
            ('period', '=', Eval('id')),
        ], depends=_DEPENDS)
    scale_additional_skill = fields.Many2One(
        'performance_evaluation.rating_additional_skill',
        'Scale Additional Skills',
        states={
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('period', '=', Eval('id'))
        ], depends=_DEPENDS)
    scale_result = fields.Many2One(
        'performance_evaluation.rating_result', 'Scale Results',
        states={
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('period', '=', Eval('id'))
        ], depends=_DEPENDS)
    percentage_increase = fields.Numeric('Percentage Increase (%)',
        states=_STATES, depends=_DEPENDS)
    discount_percentage = fields.Numeric('Discount Percentage (%)',
        states=_STATES, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'in_process'),
            ('in_process', 'done'),
        ))
        cls._error_messages.update({
            'empty_scales': ('You must complete all scales.'),
            'already_in_process': ('There is a period in process. '
                                   'End the period to create a new one'),
            'finish_evaluations': ('To Complete the evaluation period you must '
                                   'complete all evaluations')
        })
        cls._buttons.update(
            {
                'draft': {
                    'invisible':
                        Eval('state').in_(['draft', 'done', 'in_process'])
                },
                'in_process': {
                    'invisible': Eval('state').in_(['in_process', 'done'])
                },
                'done': {
                    'invisible': Eval('state').in_(['done'])
                },
                'load_scales': {
                    'invisible': Eval('state').in_(['done', 'in_process'])
                },
            }
        )

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_percentage_increase(cls):
        return 4

    @classmethod
    def default_discount_percentage(cls):
        return 4

    @staticmethod
    def default_start_date():
        return datetime.date.today()

    @staticmethod
    def default_end_date():
        return datetime.date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('in_process')
    def in_process(cls, periods):
        ids = [p.id for p in periods]
        for p in periods:
            if p.scale_job_activity is None or p.scale_knowledge is None or \
                    p.scale_competency is None or p.scale_result is None or \
                    p.scale_universal_competency is None or \
                    p.scale_additional_skill is None:
                cls.raise_user_error('empty_scales')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, periods):
        pool = Pool()
        Evaluation = pool.get('performance_evaluation.evaluation')
        for p in periods:
            evaluations = Evaluation.search([
                ('period', '=', p),
                ('state', '!=', 'done'),
            ])
            if evaluations:
                for e in evaluations:
                    cls.raise_user_error('finish_evaluations')

    @classmethod
    @ModelView.button
    def load_scales(cls, periods, default=None):
        pool = Pool()
        Rating = pool.get('performance_evaluation.rating')
        rating_job_activity = Rating.search([
            ('template', '=', True),
            ('type', '=', 'compliance_level')
        ])
        rating_knowledge = Rating.search([
            ('template', '=', True),
            ('type', '=', 'knowledge_level')
        ])
        rating_competency = Rating.search([
            ('template', '=', True),
            ('type', '=', 'development_level')
        ])
        rating_universal_competency = Rating.search([
            ('template', '=', True),
            ('type', '=', 'frequency_competency')
        ])
        RatingAdditionalSkill = pool.get(
            'performance_evaluation.rating_additional_skill')
        rads = RatingAdditionalSkill.search([
            ('template', '=', True)
        ])
        RatingResult = pool.get('performance_evaluation.rating_result')
        rating_result = RatingResult.search([
            ('template', '=', True)
        ])
        default = {
            'template': False
        }
        for p in periods:
            if p.scale_job_activity is None:
                new_job_activity_scale = \
                    Rating.copy(rating_job_activity, default=default)[0]
                new_job_activity_scale.period = p
                new_job_activity_scale.name += ' - ' + p.name
                new_job_activity_scale.save()
                p.scale_job_activity = new_job_activity_scale
            if p.scale_knowledge is None:
                new_knowledge_scale = \
                    Rating.copy(rating_knowledge, default=default)[0]
                new_knowledge_scale.period = p
                new_knowledge_scale.name += ' - ' + p.name
                new_knowledge_scale.save()
                p.scale_knowledge = new_knowledge_scale
            if p.scale_competency is None:
                new_competency_scale = \
                    Rating.copy(rating_competency, default=default)[0]
                new_competency_scale.period = p
                new_competency_scale.name += ' - ' + p.name
                new_competency_scale.save()
                p.scale_competency = new_competency_scale
            if p.scale_universal_competency is None:
                new_universal_competency_scale = \
                    Rating.copy(rating_universal_competency, default=default)[0]
                new_universal_competency_scale.period = p
                new_universal_competency_scale.name += ' - ' + p.name
                new_universal_competency_scale.save()
                p.scale_universal_competency = new_universal_competency_scale
            if p.scale_additional_skill is None:
                new_additional_skill_scale = \
                    RatingAdditionalSkill.copy(rads, default=default)[0]
                new_additional_skill_scale.period = p
                new_additional_skill_scale.name += ' - ' + p.name
                new_additional_skill_scale.save()
                p.scale_additional_skill = new_additional_skill_scale
            if p.scale_result is None:
                new_result_scale = \
                    RatingResult.copy(rating_result, default=default)[0]
                new_result_scale.period = p
                new_result_scale.name += ' - ' + p.name
                new_result_scale.save()
                p.scale_result = new_result_scale
        cls.save(periods)

    @classmethod
    def validate(cls, periods):
        # TODO Change validation, search periods in_process <=1
        super(Period, cls).validate(periods)
        count = 0
        for period in periods:
            exist = period.check_state(period.state)
            if exist:
                count += 1
        if count > 1:
            cls.raise_user_error('already_in_process')

    def check_state(self, state):
        if state == 'in_process':
            return True
        return False


class Evaluation(Workflow, ModelSQL, ModelView):
    'Evaluation'
    __name__ = 'performance_evaluation.evaluation'
    _history = True

    period = fields.Many2One('performance_evaluation.period',
        'Evaluation period', states=_STATES, ondelete='RESTRICT',
        domain=[
            ('state', '=', 'in_process')
        ], depends=_DEPENDS)
    employee = fields.Many2One('company.employee', 'Employee', states=_STATES,
       domain=[
           ('contract_state', '=', 'done')
       ], depends=_DEPENDS)
    code = fields.Char('Code', readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
    ], 'State', readonly=True)
    date = fields.Date('Date', states=_STATES, depends=_DEPENDS)
    position = fields.Char('Position', states=_STATES, depends=_DEPENDS)
    profession = fields.Char('Profession',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    boss = fields.Many2One('company.employee', 'Boss',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    observation = fields.Text('Observations',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    apply_increase = fields.Boolean('Apply Increase (%)',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    percentage_increase = fields.Function(fields.Numeric('Percentage Increase'),
        'on_change_with_percentage_increase')
    activity_lines = fields.One2Many('performance_evaluation.job_activity_line',
        'evaluation', 'Job Management Indicators',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    knowledge_lines = fields.One2Many('performance_evaluation.knowledge_line',
        'evaluation', 'Knowledge',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    competency_lines = fields.One2Many('performance_evaluation.competency_line',
        'evaluation', 'Technical Competencies of the position',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    universal_competency_lines = fields.One2Many(
        'performance_evaluation.universal_competency_line',
        'evaluation', 'Universal Competencies',
        states=_STATES, depends=_DEPENDS)
    additional_skill_lines = fields.One2Many(
        'performance_evaluation.additional_skill_line',
        'evaluation', 'Additional Skills', states=_STATES, depends=_DEPENDS)
    complain_lines = fields.One2Many('performance_evaluation.complain_line',
        'evaluation', 'Complains',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])
    total_job_activities = fields.Function(
        fields.Numeric('Total Essential Activities (%)', digits=(16, 1)),
        'on_change_with_total_job_activities')
    total_knowledge = fields.Function(
        fields.Numeric('Total Knowledge', digits=(16, 1)),
        'on_change_with_total_knowledge')
    total_competencies = fields.Function(
        fields.Numeric('Total Technical Competencies', digits=(16, 1)),
        'on_change_with_total_competencies')
    total_universal_competencies = fields.Function(
        fields.Numeric('Total Universal Competencies', digits=(16, 1)),
        'on_change_with_total_universal_competencies')
    total_additional_skills = fields.Function(
        fields.Numeric('Total Teamwork, Initiative and Leadership',
        digits=(16, 1)), 'on_change_with_total_additional_skills')
    total_complains = fields.Function(fields.Numeric('Total Complains',
        digits=(16, 1)),
        'on_change_with_total_complains')
    total = fields.Function(fields.Numeric('Total Complains', digits=(16, 1)),
        'on_change_with_total')
    result = fields.Function(fields.Char('Total Complains'),
        'on_change_with_result')

    @classmethod
    def __setup__(cls):
        super(Evaluation, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'done'),
        ))
        cls._error_messages.update({
            'empty_universal_competencies': ('You must complete all lines of '
                                             'Universal Competencies'),
            'empty_additional_skills': ('You must complete all lines of '
                                        'Teamwork, Initiative and Leadership'),
        })
        cls._buttons.update(
            {
                'draft': {
                    'invisible':
                        Eval('state').in_(['draft', 'done'])
                },
                'confirmed': {
                    'invisible': Eval('state').in_(['done', 'confirmed'])
                },
                'done': {
                    'invisible': Eval('state').in_(['done', 'draft']),
                },
            }
        )

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_date(cls):
        Date = Pool().get('ir.date')
        return Transaction().context.get('date') or Date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, evaluations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, evaluations):
        # TODO Create reusable function to check if lines are empty
        pool = Pool()
        cursor = Transaction().connection.cursor()
        eval_table = cls.__table__()
        UniversalCompetencyLine = pool.get(
            'performance_evaluation.universal_competency_line')
        AdditionalSkillLine = pool.get(
            'performance_evaluation.additional_skill_line')
        ucl = UniversalCompetencyLine.__table__()
        ads = AdditionalSkillLine.__table__()
        ids = [e.id for e in evaluations]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(eval_table.id, sub_ids)
            query = eval_table.join(ucl,
                                    condition=eval_table.id == ucl.evaluation
                                    ).select(
                ucl.evaluation,
                where=red_sql & (ucl.frequency == Null))
            cursor.execute(*query)
            for eval in cursor.fetchall():
                cls.raise_user_error('empty_universal_competencies')
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(eval_table.id, sub_ids)
            query = eval_table.join(ads,
                                    condition=eval_table.id == ads.evaluation
                                    ).select(
                ads.evaluation,
                where=red_sql & (ads.frequency == Null))
            cursor.execute(*query)
            for eval in cursor.fetchall():
                cls.raise_user_error('empty_additional_skills')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, evaluations):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for eval in evaluations:
            if eval.code is None:
                eval.code = Sequence.get_id(eval.period.period_sequence.id)
        cls.save(evaluations)

    @fields.depends('employee')
    def on_change_with_position(self, number=None):
        if self.employee and self.employee.contract and \
                self.employee.contract.position:
            return self.employee.contract.position.name
        else:
            return None

    @fields.depends('employee')
    def on_change_with_profession(self, number=None):
        if self.employee and self.employee.educations:
            return self.employee.educations[0].name
        else:
            return None

    @fields.depends('employee')
    def on_change_with_boss(self, number=None):
        if self.employee and self.employee.contract:
            if self.employee == self.employee.contract.department.leader:
                if self.employee.contract.department.parent and \
                        self.employee.contract.department.parent.leader:
                    return self.employee.contract.department.parent.leader.id
                else:
                    return None
            else:
                if self.employee.contract.department and \
                        self.employee.contract.department.leader:
                    return self.employee.contract.department.leader.id
                else:
                    return None
        return None

    @fields.depends('activity_lines', 'apply_increase', 'period')
    def on_change_with_percentage_increase(self, number=None):
        if self.activity_lines and self.period:
            count = Decimal(len(self.activity_lines))
            count_more_or_equals = 0
            count_more = 0
            pool = Pool()
            RatingScale = pool.get('performance_evaluation.rating_scale')
            job_scale = RatingScale.search([
                ('rating.type', '=', 'compliance_level'),
                ('type', '=', 'range'),
                ('rating.period', '=', self.period),
            ])
            for line in self.activity_lines:
                if line.level and line.level != 0 and \
                        line.activity and line.indicator:
                    # Calculate values to decide if a increase is applied or not
                    if line.complete >= line.period_goal:
                        count_more_or_equals += 1
                    if line.complete > line.period_goal:
                        count_more += 1
            if self.apply_increase and count_more_or_equals == count and \
                    count_more >= 1:
                return self.period.percentage_increase
            else:
                return 0
        return 0

    @fields.depends('activity_lines', 'apply_increase', 'period')
    def on_change_with_total_job_activities(self, number=None):
        if self.activity_lines and self.period:
            count = Decimal(len(self.activity_lines))
            sum = Decimal(0.0)
            count_more_or_equals = 0
            count_more = 0
            pool = Pool()
            RatingScale = pool.get('performance_evaluation.rating_scale')
            job_scale = RatingScale.search([
                ('rating.type', '=', 'compliance_level'),
                ('type', '=', 'range'),
                ('rating.period', '=', self.period),
            ])
            for line in self.activity_lines:
                if line.level and line.level != 0 and \
                        line.activity and line.indicator:
                    for scale in job_scale:
                        if line.level == scale.number:
                            factor_one = scale.value
                    factor_two = factor_one / count
                    sum += factor_two
                    # Calculate values to decide if a increase is applied or not
                    if line.complete >= line.period_goal:
                        count_more_or_equals += 1
                    if line.complete > line.period_goal:
                        count_more += 1
            if self.apply_increase and count_more_or_equals == count and \
                    count_more >= 1:
                return sum + self.period.percentage_increase
            else:
                return sum
        return 0

    @fields.depends('knowledge_lines', 'period')
    def on_change_with_total_knowledge(self, number=None):
        if self.knowledge_lines and self.period:
            count = Decimal(len(self.knowledge_lines))
            sum = Decimal(0.0)
            for line in self.knowledge_lines:
                if line.level and line.knowledge:
                    factor_one = line.level.value / count
                    sum += factor_one
            return sum
        return 0

    @fields.depends('competency_lines', 'period')
    def on_change_with_total_competencies(self, number=None):
        if self.competency_lines and self.period:
            count = Decimal(len(self.competency_lines))
            sum = Decimal(0.0)
            for line in self.competency_lines:
                if line.competency and line.relevance and line.level:
                    factor_one = line.level.value / count
                    sum += factor_one
            return sum
        return 0

    @fields.depends('universal_competency_lines', 'period')
    def on_change_with_total_universal_competencies(self, number=None):
        if self.universal_competency_lines and self.period:
            sum = Decimal(0.0)
            for line in self.universal_competency_lines:
                if line.universal_competency and \
                        line.relevance != 'None' and line.frequency:
                    factor_one = line.frequency.value
                    sum += factor_one
            factor_two = Decimal(round(sum, 2))
            return Decimal(round(factor_two, 1))
        return 0

    @fields.depends('additional_skill_lines', 'period', 'employee')
    def on_change_with_total_additional_skills(self, number=None):
        if self.additional_skill_lines and self.period and self.employee:
            count = len(self.additional_skill_lines)
            sum = Decimal(0.0)
            for line in self.additional_skill_lines:
                if line.additional_skill and line.relevance != 'None' and \
                        line.frequency:
                    if count == 2 and self.employee != \
                            self.employee.contract.department.leader:
                        sum += line.frequency.two_items_value
                    else:
                        sum += line.frequency.three_items_value
            return Decimal(round(sum, 1))
        return 0

    @fields.depends('complain_lines', 'period')
    def on_change_with_total_complains(self, number=None):
        if self.complain_lines and self.period:
            sum = Decimal(0.0)
            for line in self.complain_lines:
                if line.complain and line.reduction_percentage:
                    sum += line.reduction_percentage
            return sum
        return 0

    # TODO Migrate to version 5.0
    @fields.depends(
        'total_job_activities', 'total_knowledge', 'total_competencies',
        'total_universal_competencies', 'total_additional_skills',
        'total_complains')
    def on_change_with_total(self, number=None):
        if self.total_job_activities and self.total_knowledge and \
                self.total_competencies and \
                self.total_universal_competencies and \
                self.total_additional_skills:
            sum = self.total_additional_skills + self.total_competencies + \
                   self.total_universal_competencies + self.total_knowledge + \
                   self.total_job_activities
            if self.total_complains:
                return sum - self.total_complains
            else:
                return sum
        return 0

    # TODO Migrate to version 5.0
    @fields.depends('total')
    def on_change_with_result(self, number=None):
        if self.total and self.total != 0:
            pool = Pool()
            RatingScaleResult = pool.get(
                'performance_evaluation.rating_scale_result'
            )
            result_scale = RatingScaleResult.search([
                ('rating.type', '=', 'Result'),
                ('rating.period', '=', self.period),
            ])
            for scale in result_scale:
                if scale.min_range <= self.total <= scale.max_range:
                    return scale.name + ': ' + scale.description
        return ''

    def get_rec_name(self, name):
        if self.code:
            return self.code
        return self.id

class JobActivityLine(ModelSQL, ModelView):
    'JobActivityLine'
    __name__ = 'performance_evaluation.job_activity_line'

    evaluation = fields.Many2One('performance_evaluation.evaluation',
        'Evaluation', required=True, ondelete='CASCADE')
    activity = fields.Many2One('performance_evaluation.job_activity',
        'Job Activity', required=True)
    indicator = fields.Char('Indicator', required=True)
    period_goal = fields.Integer('Period goal', required=True)
    complete = fields.Numeric('Complete', required=True, digits=(16, 0))
    complete_percentage = fields.Function(
        fields.Numeric('Complete Percentage(%)', digits=(16, 0)),
        'on_change_with_complete_percentage')
    level = fields.Function(fields.Integer('Complete level'),
        'on_change_with_level')

    @fields.depends('period_goal', 'complete', '_parent_evaluation.period')
    def on_change_with_complete_percentage(self, name=None):
        if self.period_goal and self.complete and self.period_goal > 0 and \
                self.evaluation.period:
            if self.complete > self.period_goal:
                return 100
            else:
                return (self.complete * 100) / self.period_goal
        return 0

    @fields.depends('period_goal', 'complete', '_parent_evaluation.period')
    def on_change_with_level(self, name=None):
        if self.period_goal and self.complete and self.evaluation.period:
            pool = Pool()
            RatingScale = pool.get('performance_evaluation.rating_scale')
            job_scale = RatingScale.search([
                ('rating.type', '=', 'compliance_level'),
                ('type', '=', 'range'),
                ('rating.period', '=', self.evaluation.period)
            ])
            complete_percentage = (self.complete * 100) / self.period_goal
            # Search max number that will use if the complete percentage is more
            # than 100
            max_number = 0
            for a in job_scale:
                if a.number > max_number:
                    max_number = a.number
            if complete_percentage > 0:
                for scale in job_scale:
                    if complete_percentage > 100:
                        return max_number
                    else:
                        if scale.min_value <= complete_percentage <= scale.max_value:
                            return scale.number
        return 0


class KnowledgeLine(ModelSQL, ModelView):
    'KnowledgeLine'
    __name__ = 'performance_evaluation.knowledge_line'

    evaluation = fields.Many2One('performance_evaluation.evaluation',
        'Evaluation', required=True, ondelete='CASCADE')
    knowledge = fields.Many2One('performance_evaluation.knowledge', 'Knowledge',
        required=True)
    level = fields.Many2One('performance_evaluation.rating_scale',
        'Knowledge Level', required=True,
        domain=[
            ('rating.type', '=', 'knowledge_level'),
            ('rating.period', '=',
            Eval('_parent_evaluation', {}).get('period')),
        ])


class CompetencyLine(ModelSQL, ModelView):
    'CompetencyLine'
    __name__ = 'performance_evaluation.competency_line'

    evaluation = fields.Many2One('performance_evaluation.evaluation',
        'Evaluation', required=True, ondelete='CASCADE')
    competency = fields.Many2One('performance_evaluation.competency',
        'Competency', required=True)
    relevance = fields.Selection([
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', required=True, sort=False)
    behavior = fields.Function(
        fields.Text('Observable behavior'), 'on_change_with_behavior')
    level = fields.Many2One('performance_evaluation.rating_scale',
        'Development Level', required=True,
        domain=[
            ('rating.type', '=', 'development_level'),
            ('rating.period', '=',
             Eval('_parent_evaluation', {}).get('period')),
        ])

    @fields.depends('relevance', 'competency')
    def on_change_with_behavior(self, name=None):
        if self.relevance and self.competency and self.competency.relevancies:
            for relevance in self.competency.relevancies:
                if self.relevance == relevance.name:
                    return relevance.description
        return None


class UniversalCompetencyLine(ModelSQL, ModelView):
    'UniversalCompetencyLine'
    __name__ = 'performance_evaluation.universal_competency_line'

    evaluation = fields.Many2One(
        'performance_evaluation.evaluation', 'Evaluation',
        required=True, ondelete='CASCADE')
    universal_competency = fields.Many2One(
        'performance_evaluation.universal_competency',
        'Competency')
    relevance = fields.Selection([
        ('None', ''),
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', sort=False)
    behavior = fields.Function(
        fields.Text('Observable behavior'), 'on_change_with_behavior')
    frequency = fields.Many2One('performance_evaluation.rating_scale',
        'Aplication Frequency',
        domain=[
            ('rating.type', '=', 'frequency_competency'),
            ('rating.period', '=', Eval('_parent_evaluation', {}).get('period'))
        ])

    @classmethod
    def default_relevance(cls):
        return 'None'

    @fields.depends('relevance', 'universal_competency')
    def on_change_with_behavior(self, name=None):
        if self.relevance and self.universal_competency and \
                self.universal_competency.relevancies:
            for relevance in self.universal_competency.relevancies:
                if self.relevance == relevance.name:
                    return relevance.description
        return None

    @fields.depends('evaluation', '_parent_evaluation.state')
    def on_change_with_evaluation_state(self, name=None):
        if self.evaluation:
            return self.evaluation.state
        return None


class AdditionalSkillLine(ModelSQL, ModelView):
    'AdditionalSkillLine'
    __name__ = 'performance_evaluation.additional_skill_line'

    evaluation = fields.Many2One('performance_evaluation.evaluation',
        'Evaluation', required=True, ondelete='CASCADE')
    additional_skill = fields.Many2One(
        'performance_evaluation.additional_skill', 'Description')
    relevance = fields.Selection([
        ('None', ''),
        ('high', 'High'),
        ('medium', 'Medium'),
        ('low', 'Low'),
    ], 'Relevance', sort=False)
    behavior = fields.Function(fields.Text('Observable behavior'),
        'on_change_with_behavior')
    frequency = fields.Many2One(
        'performance_evaluation.rating_scale_additional_skill',
        'Aplication Frequency',
        domain=[
            ('rating.type', '=', 'Additional Skills Scale'),
            ('rating.period', '=', Eval('_parent_evaluation', {}).get('period'))
        ])

    @classmethod
    def default_relevance(cls):
        return 'None'

    @fields.depends('relevance', 'additional_skill')
    def on_change_with_behavior(self, name=None):
        if self.relevance and self.additional_skill and \
                self.additional_skill.relevancies:
            for relevance in self.additional_skill.relevancies:
                if self.relevance == relevance.name:
                    return relevance.description
        return None


class ComplainLine(ModelSQL, ModelView):
    'Complain Line'
    __name__ = 'performance_evaluation.complain_line'

    evaluation = fields.Many2One('performance_evaluation.evaluation',
        'Evaluation', ondelete='CASCADE')
    period_start_date = fields.Function(
        fields.Date('Period start date'), 'on_change_with_period_start_date')
    period_end_date = fields.Function(
        fields.Date('Period end date'), 'on_change_with_period_end_date')
    complain = fields.Many2One('performance_evaluation.complain', 'Complain',
        domain=[
            ('state', '=', 'done'),
            ('employee', '=', Eval('_parent_evaluation', {}).get('employee')),
            ('date', '>=', Eval('period_start_date')),
            ('date', '<=', Eval('period_end_date')),
        ], depends=['period_start_date', 'period_end_date'])
    complainant = fields.Function(
        fields.Char('Complainant', readonly=True), 'on_change_with_complainant')
    description = fields.Function(
        fields.Char('Description', readonly=True), 'on_change_with_description')
    apply_discount = fields.Boolean('Apply discount')
    reduction_percentage = fields.Function(
        fields.Numeric('Percentage Reduction', readonly=True),
        'on_change_with_reduction_percentage')

    @classmethod
    def __setup__(cls):
        super(ComplainLine, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('complain_unique', Unique(table, table.complain),
             'The complain has already been used'),
        ]

    @fields.depends('complain')
    def on_change_with_complainant(self, name=None):
        if self.complain:
            return self.complain.complainant
        return None

    @fields.depends('complain')
    def on_change_with_description(self, name=None):
        if self.complain:
            return self.complain.description
        return None

    @fields.depends('complain', 'apply_discount', '_parent_evaluation.period')
    def on_change_with_reduction_percentage(self, name=None):
        if self.complain and self.apply_discount and self.evaluation.period:
            return self.evaluation.period.discount_percentage
        return 0

    @fields.depends('evaluation', '_parent_evaluation.period')
    def on_change_with_period_start_date(self, name=None):
        if self.evaluation and self.evaluation.period:
            return self.evaluation.period.start_date
        return None

    @fields.depends('evaluation', '_parent_evaluation.period')
    def on_change_with_period_end_date(self, name=None):
        if self.evaluation and self.evaluation.period:
            return self.evaluation.period.end_date
        return None


class Rating(ModelSQL, ModelView):
    'Rating'
    __name__ = 'performance_evaluation.rating'

    period = fields.Many2One('performance_evaluation.period', 'Period',
        readonly=True, ondelete='CASCADE')
    template = fields.Boolean('Template', readonly=True)
    name = fields.Char('Rating Name', required=True)
    type = fields.Selection([
        ('compliance_level', 'Job Activities Scale'),
        ('knowledge_level', 'Knowledge Scale'),
        ('development_level', 'Competencies Scale'),
        ('frequency_competency', 'Frequency Universal Competencies Scale'),
    ], 'Type', required=True)
    weight = fields.Numeric('Weight (%)', required=True)
    rating_value = fields.One2Many('performance_evaluation.rating_scale',
        'rating', 'Rating Scales')

    @classmethod
    def default_template(cls):
        return False

    @classmethod
    def __setup__(cls):
        super(Rating, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('type_unique', Unique(table, table.type, table.period),
             'You can create only one type of scale per period.'),
        ]


class RatingScale(ModelSQL, ModelView):
    'Rating Scale'
    __name__ = 'performance_evaluation.rating_scale'
    rating = fields.Many2One('performance_evaluation.rating', 'Rating',
        ondelete='CASCADE')
    type = fields.Selection([
        ('unique', 'Unique'),
        ('range', 'Range')
    ], 'Type')
    name = fields.Char('Scale Name', required=True)
    number = fields.Integer('Integer Number', required=True)
    value = fields.Numeric('Value', required=True)
    min_value = fields.Numeric('Low Value',
        states={
            'required': (Eval('type') == 'range'),
            'invisible': (Eval('type') != 'range'),
        })
    max_value = fields.Numeric('Max Value',
        states={
            'required': (Eval('type') == 'range'),
            'invisible': (Eval('type') != 'range'),
        })

    @classmethod
    def default_type(cls):
        return 'unique'


class RatingAdditionalSkill(ModelSQL, ModelView):
    'Rating'
    __name__ = 'performance_evaluation.rating_additional_skill'

    period = fields.Many2One('performance_evaluation.period', 'Period',
        readonly=True, ondelete='CASCADE')
    template = fields.Boolean('Template', readonly=True)
    name = fields.Char('Rating Name', required=True)
    type = fields.Char('Type', readonly=True, required=True)
    weight = fields.Numeric('Weight (%)', required=True)
    rating_value = fields.One2Many(
        'performance_evaluation.rating_scale_additional_skill',
        'rating', 'Rating Scales')

    @classmethod
    def default_type(cls):
        return 'Additional Skills Scale'

    @classmethod
    def default_template(cls):
        return False

    @classmethod
    def __setup__(cls):
        super(RatingAdditionalSkill, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('type_unique', Unique(table, table.type, table.period),
             'You can create only one type of scale per period'),
        ]


class RatingScaleAdditionalSkill(ModelSQL, ModelView):
    'Rating Scale'
    __name__ = 'performance_evaluation.rating_scale_additional_skill'
    rating = fields.Many2One('performance_evaluation.rating_additional_skill',
        'Rating', ondelete='CASCADE')
    name = fields.Char('Scale Name', required=True)
    number = fields.Integer('Integer Number', required=True)
    two_items_value = fields.Numeric('Two Items Value', required=True)
    three_items_value = fields.Numeric('Three Items Value', required=True)


class RatingResult(ModelSQL, ModelView):
    'Rating Result'
    __name__ = 'performance_evaluation.rating_result'
    period = fields.Many2One('performance_evaluation.period', 'Period',
        readonly=True, ondelete='CASCADE')
    template = fields.Boolean('Template', readonly=True)
    name = fields.Char('Rating Name', required=True)
    type = fields.Char('Type', readonly=True, required=True)
    rating_value = fields.One2Many('performance_evaluation.rating_scale_result',
        'rating', 'Rating Scales')

    @classmethod
    def default_type(cls):
        return 'Result'

    @classmethod
    def default_template(cls):
        return False

    @classmethod
    def __setup__(cls):
        super(RatingResult, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('type_unique', Unique(table, table.type, table.period),
             'You can create only one type of scale per period'),
        ]


class RatingScaleResult(ModelSQL, ModelView):
    'Rating Result Scale'
    __name__ = 'performance_evaluation.rating_scale_result'
    rating = fields.Many2One('performance_evaluation.rating_result',
        'Rating', ondelete='CASCADE')
    name = fields.Char('Name', required=True)
    description = fields.Char('Description', required=True)
    min_range = fields.Numeric('Minimum Range', required=True)
    max_range = fields.Numeric('Maximum Range', required=True)


