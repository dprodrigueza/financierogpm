from sql import Null

from trytond.model import fields, Workflow, ModelSQL, ModelView, Unique
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button

_all__ = ['EvaluationTemplate', 'UniversalCompetencyLineTemplate',
          'AdditionalSkillLineTemplate', 'CreateEvaluationStart',
          'CreateEvaluationSucceed', 'CreateEvaluationWizard', ]


class EvaluationTemplate(ModelSQL, ModelView):
    'EvaluationTemplate'
    __name__ = 'performance_evaluation.evaluation_template'

    name = fields.Char('Template name', required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
    ], 'State', readonly=True)
    active = fields.Boolean('Active', select=True)
    universal_competency_lines = fields.One2Many(
        'performance_evaluation.u_competency_line_template',
        'evaluation_template', 'Universal Competencies')
    additional_skill_lines = fields.One2Many(
        'performance_evaluation.additional_skill_line_template',
        'evaluation_template', 'Additional Skills')

    @classmethod
    def default_state(cls):
        return 'draft'

    def get_evaluation(self, employee):
        'Return a evaluation'
        pool = Pool()
        Evaluation = pool.get('performance_evaluation.evaluation')
        evaluation = Evaluation()
        evaluation.state = self.state
        evaluation.universal_competency_lines = \
            [l.get_line() for l in self.universal_competency_lines]
        evaluation.additional_skill_lines = \
            [l.get_line(employee) for l in self.additional_skill_lines
             if l.get_line(employee) is not None]
        return evaluation


class UniversalCompetencyLineTemplate(ModelSQL, ModelView):
    'UniversalCompetencyLine'
    __name__ = 'performance_evaluation.u_competency_line_template'

    universal_competency = fields.Many2One(
        'performance_evaluation.universal_competency', 'Competency',
        required=True)
    evaluation_template = fields.Many2One(
        'performance_evaluation.evaluation_template', 'Evaluation',
        required=True, ondelete='CASCADE')

    def get_line(self):
        pool = Pool()
        Line = pool.get('performance_evaluation.universal_competency_line')
        line = Line()
        line.universal_competency = self.universal_competency
        return line


class AdditionalSkillLineTemplate(ModelSQL, ModelView):
    'AdditionalSkillLine'
    __name__ = 'performance_evaluation.additional_skill_line_template'

    evaluation_template = fields.Many2One(
        'performance_evaluation.evaluation_template', 'Evaluation',
        required=True, ondelete='CASCADE')
    additional_skill = fields.Many2One(
        'performance_evaluation.additional_skill',
        'Description', required=True)
    type = fields.Selection([
        ('required', 'Required'),
        ('optional', 'Optional'),
    ], 'Type Skill')

    def get_line(self, employee):
        pool = Pool()
        Line = pool.get('performance_evaluation.additional_skill_line')
        line = Line()
        if employee != employee.contract.department.leader:
            if self.type == 'required':
                line.additional_skill = self.additional_skill
            else:
                line = None
        else:
            line.additional_skill = self.additional_skill
        return line


class CreateEvaluationSucceed(ModelView):
    'Create Evaluation Succeed'
    __name__ = 'performance_evaluation.create_evaluation_succeed'


class CreateEvaluationStart(ModelSQL, ModelView):
    'Create Evaluation Start'
    __name__ = 'performance_evaluation.create_evaluation_start'

    employee = fields.Many2Many('company.employee', None, None, 'Employees',
        required=True,
        domain=[
            ('contract_state', '=', 'done')
        ])
    period = fields.Many2One(
        'performance_evaluation.period', 'Evaluation Period', required=True,
        domain=[
             ('state', '=', 'in_process')
        ])
    template = fields.Many2One('performance_evaluation.evaluation_template',
        'Evaluation Template', required=True)


class CreateEvaluationWizard(Wizard):
    'Create Evaluation Wizard'
    __name__ = 'performance_evaluation.create_evaluation_wizard'

    start = StateView(
        'performance_evaluation.create_evaluation_start',
        'performance_evaluation.create_evaluation_wizard_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'done', 'tryton-ok', default=True),
        ])
    done = StateTransition()
    succeed = StateView(
        'performance_evaluation.create_evaluation_succeed',
        'performance_evaluation.create_evaluation_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
        ])

    def transition_done(self):
        pool = Pool()
        Evaluation = pool.get('performance_evaluation.evaluation')
        Complain = pool.get('performance_evaluation.complain')
        employees = self.start.employee
        period = self.start.period
        template = self.start.template
        for employee in employees:
            evaluation = Evaluation.search([
                ('employee', '=', employee),
                ('period', '=', period),
            ])
            complains = Complain.search([
                ('employee', '=', employee),
                ('in_evaluation', '=', False),
            ])
            if not evaluation:
                evaluation = template.get_evaluation(employee)
            else:
                evaluation, = evaluation
            evaluation.employee = employee
            if complains:
                evaluation.complain_lines = \
                    [c.get_complain_lines(c) for c in complains]
            if employee.educations:
                evaluation.profession = employee.educations[0].name
            else:
                evaluation.profession = ''
            if employee == employee.contract.department.leader:
                if employee.contract.department.parent and \
                        employee.contract.department.parent.leader:
                    evaluation.boss = \
                        employee.contract.department.parent.leader.id
                else:
                    evaluation.boss = None
            else:
                if employee.contract.department and \
                        employee.contract.department.leader:
                    evaluation.boss = employee.contract.department.leader.id
                else:
                    evaluation.boss = None
            evaluation.position = employee.contract.position.name
            evaluation.period = period
            evaluation.percentage_increase = period.percentage_increase
            evaluation.save()
        return 'succeed'
