import datetime

from trytond.model import ModelSQL, ModelView, fields
from trytond.modules.company import CompanyReport
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateReport

__all__ = ['EvaluationReport', 'EvaluationReportStart',
           'EvaluationReportWizard', 'EvaluationFinalReport',
           'EvaluationFinalReportStart', 'EvaluationFinalReportWizard',
           'EvaluationListReport', 'EvaluationListReportStart',
           'EvaluationListReportWizard']


class EvaluationReport(CompanyReport):
    'Evaluation Report'
    __name__ = 'performance_evaluation.evaluation_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(EvaluationReport, cls).get_context(records, data)
        return context


class EvaluationReportStart(ModelSQL, ModelView):
    'EvaluationReportStart'

    __name__ = 'performance_evaluation.evaluation_report_start'
    period = fields.Many2One(
        'performance_evaluation.period', 'Evaluation Period', required=True)
    evaluations = fields.Many2Many(
        'performance_evaluation.evaluation', None, None, 'Evaluations',
        domain=[
            ('period', '=', Eval('period'))
        ], depends=['period'], required=True)


class EvaluationReportWizard(Wizard):
    'EvaluationReportWizard'

    __name__ = 'performance_evaluation_report_wizard'
    start = StateView(
        'performance_evaluation.evaluation_report_start',
        'performance_evaluation.evaluation_report_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('performance_evaluation.evaluation_report')

    def do_print_(self, action):
        data = {
            'ids': [e.id for e in self.start.evaluations]
        }
        return action, data


class EvaluationFinalReport(CompanyReport):
    'Evaluation Final Report'
    __name__ = 'performance_evaluation.evaluation_final_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(EvaluationFinalReport, cls).get_context(records, data)
        return context


class EvaluationFinalReportStart(ModelSQL, ModelView):
    'EvaluationFinalReportStart'

    __name__ = 'performance_evaluation.evaluation_final_report_start'
    period = fields.Many2One('performance_evaluation.period',
        'Evaluation Period', required=True)
    date = fields.Date('Print Date', required=True)

    @classmethod
    def default_date(cls):
        Date = Pool().get('ir.date')
        return Transaction().context.get('date') or Date.today()


class EvaluationFinalReportWizard(Wizard):
    'EvaluationFinalReportWizard'

    __name__ = 'performance_evaluation_final_report_wizard'
    start = StateView(
        'performance_evaluation.evaluation_final_report_start',
        'performance_evaluation.evaluation_final_report_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('performance_evaluation.evaluation_final_report')

    def do_print_(self, action):
        pool = Pool()
        Evaluation = pool.get('performance_evaluation.evaluation')
        evaluations =Evaluation.search([
            ('period', '=', self.start.period)
        ])
        data = {
            'ids': [e.id for e in evaluations],
            'period_name': self.start.period.name,
            'period_start_date': self.start.period.start_date,
            'period_end_date': self.start.period.end_date,
            'print_date': self.start.date,
        }
        return action, data


class EvaluationListReport(CompanyReport):
    'Evaluation List Report'
    __name__ = 'performance_evaluation.evaluation_list_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(EvaluationListReport, cls).get_context(records, data)
        context['boss_evaluations'] = cls.get_boss_evaluations
        context['subordinate_evaluations'] = cls.get_subordinate_evaluations
        context['departments'] = cls.get_departments
        return context

    @classmethod
    def get_boss_evaluations(cls, period_id):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Evaluation = pool.get('performance_evaluation.evaluation')
        Department = pool.get('company.department')
        Period = pool.get('performance_evaluation.period')
        evaluations = Evaluation.search([
            'period.id', '=', period_id
        ])
        evaluation = Evaluation.__table__()
        department = Department.__table__()
        ids = [e.id for e in evaluations]
        boss_evaluations_ids = []
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(evaluation.id, sub_ids)
            query = evaluation.join(department,
                condition=evaluation.employee == department.leader
            ).select(
                evaluation.id,
                where=red_sql & (evaluation.period == period_id)
            )
            cursor.execute(*query)
            for eval_id in cursor.fetchall():
                boss_evaluations_ids.append(eval_id[0])
        return Evaluation.browse(boss_evaluations_ids)

    @classmethod
    def get_departments(cls):
        pool = Pool()
        Department = pool.get('company.department')
        context = Transaction().context
        departments = Department.search([('company', '=', context.get('company'))])
        return departments

    @classmethod
    def get_subordinate_evaluations(cls, period_id, boss_id):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Evaluation = pool.get('performance_evaluation.evaluation')
        Department = pool.get('company.department')
        Period = pool.get('performance_evaluation.period')
        evaluations = Evaluation.search([
            'period.id', '=', period_id
        ])
        evaluation = Evaluation.__table__()
        department = Department.__table__()
        ids = [e.id for e in evaluations]
        subordinate_evaluations_ids = []
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(evaluation.id, sub_ids)
            query = evaluation.join(department,
                condition=evaluation.boss == department.leader
            ).select(
                evaluation.id,
                where=red_sql & (evaluation.period == period_id) &
                      (department.leader == boss_id)
            )
            cursor.execute(*query)
            for eval_id in cursor.fetchall():
                subordinate_evaluations_ids.append(eval_id[0])
        return Evaluation.browse(subordinate_evaluations_ids)


class EvaluationListReportStart(ModelSQL, ModelView):
    'EvaluationListReportStart'

    __name__ = 'performance_evaluation.evaluation_list_report_start'
    period = fields.Many2One(
        'performance_evaluation.period', 'Evaluation Period', required=True)


class EvaluationListReportWizard(Wizard):
    'EvaluationListReportWizard'

    __name__ = 'performance_evaluation_list_report_wizard'
    start = StateView(
        'performance_evaluation.evaluation_list_report_start',
        'performance_evaluation.evaluation_list_report_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('performance_evaluation.evaluation_list_report')

    def do_print_(self, action):
        pool = Pool()
        Evaluation = pool.get('performance_evaluation.evaluation')
        evaluations =Evaluation.search([
            ('period', '=', self.start.period)
        ])
        data = {
            'ids': [e.id for e in evaluations],
            'period_id': self.start.period.id,
            'period_name': self.start.period.name,
            'period_start_date': self.start.period.start_date,
            'period_end_date': self.start.period.end_date,
        }
        return action, data
