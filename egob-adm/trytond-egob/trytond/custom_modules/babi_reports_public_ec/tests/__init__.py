# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.babi_reports_public_ec.tests.test_babi_reports_public_ec import suite
except ImportError:
    from .test_babi_reports_public_ec import suite

__all__ = ['suite']
