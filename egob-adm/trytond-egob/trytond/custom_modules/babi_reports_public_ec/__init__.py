# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import model

__all__ = ['register']


def register():
    Pool.register(
        model.Model,
        module='babi_reports_public_ec', type_='model')
    Pool.register(
        module='babi_reports_public_ec', type_='wizard')
    Pool.register(
        module='babi_reports_public_ec', type_='report')
