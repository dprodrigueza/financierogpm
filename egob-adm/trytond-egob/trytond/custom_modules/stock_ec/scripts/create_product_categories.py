from datetime import datetime
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from getpass import getpass

from proteus import config, Model

import xlrd

import pdb


def create_categories(host, port, database, filename):
    username = raw_input('Username: ')
    password = getpass('Password: ')

    config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        % (username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    pdb.set_trace()

    Category = Model.get('product.category')
    Tax = Model.get('account.tax')
    Sequence = Model.get('ir.sequence')

    tax_iva, = Tax.find([('name', '=', 'IVA 12')])
    category_all, = Category.find([('name', '=', 'TODO')])
    to_save = []
    categories = {}
    categories['TODO'] = category_all

    for row in range(sheet.nrows)[1:]:
        cont += 1
        code = str(sheet.cell(row, 0).value)
        name = sheet.cell(row, 1).value
        parent_code = str(sheet.cell(row, 2).value)
        # Crear categoria de producto
        sequence = Sequence()
        sequence.name = code
        sequence.code = 'product.product'
        sequence.active = True
        sequence.padding = 5
        sequence.save()
        category = Category()
        category.name = name
        category.code = code
        category.accounting = True
        category.parent = categories[parent_code]
        category.account_parent = True
        category.taxes_parent = True
        category.product_sequence = sequence
        to_save.append(category)
        Category.save([category])
        categories[code] = category
        print(cont, ' de ', total)
        print('Correcto --->', sheet.cell(row, 0).value)
    Category.save(to_save)

    print(datetime.now())


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host', default='localhost',
        help='localhost')
    parser.add_argument('--port', dest='port', default='8000',
        help='port')
    parser.add_argument('--database', dest='database', required=True,
        help='port')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_categories(options.host, options.port, options.database,
        options.filename)
