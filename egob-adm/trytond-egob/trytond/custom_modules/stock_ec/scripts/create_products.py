from datetime import datetime
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from getpass import getpass

from proteus import config, Model

import xlrd


def create_products(host, port, database, filename):
    username = raw_input('Username: ')
    password = getpass('Password: ')

    import pdb; pdb.set_trace()

    config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        % (username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    import pdb; pdb.set_trace()

    ProductUom = Model.get('product.uom')
    Category = Model.get('product.category')
    ProductTemplate = Model.get('product.template')
    Product = Model.get('product.product')

    category_all, = Category.find([('name', '=', 'TODO')])
    to_save = []
    products_to_save = []
    categories = {}
    units = {}

    for category in Category.find([]):
        categories[category.code] = category

    for unit in ProductUom.find([]):
        units[unit.name.lower()] = unit

    import pdb; pdb.set_trace()

    for row in range(sheet.nrows)[1:]:
        cont += 1
        category_code = str(sheet.cell(row, 0).value)
        category = categories[category_code]
        reference_code = str(sheet.cell(row, 1).value)
        name = sheet.cell(row, 2).value
        product_qty = sheet.cell(row, 3).value
        product_uom = sheet.cell(row, 4).value
        product_price = sheet.cell(row, 5).value
        install_price = sheet.cell(row, 6).value

        unit = units[product_uom.lower()]
        template = ProductTemplate()
        template.name = name
        template.default_uom = unit
        template.type = 'goods'
        template.purchasable = True
        template.account_category = category
        template.accounts_category = True
        template.taxes_category = True
        template.list_price = Decimal(0)
        template.install_price = Decimal(install_price)
        template.cost_price_method = 'average'
        product, = template.products
        product.external_code = reference_code
        to_save.append(template)
        products_to_save.append(product)
        print('Correcto ---> ', cont, ' de ', sheet.cell(row, 0).value)

    ProductTemplate.save(to_save)
    Product.save(products_to_save)

    print(datetime.now())


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host', default='localhost',
        help='localhost')
    parser.add_argument('--port', dest='port', default='8000',
        help='port')
    parser.add_argument('--database', dest='database', required=True,
        help='port')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_products(options.host, options.port, options.database,
        options.filename)
