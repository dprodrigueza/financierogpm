from proteus import config, Model
from datetime import datetime
import xlrd
import pdb
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

def create_parties(host, port, database, username, password, filename):
    pdb.set_trace()
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    Party = Model.get('party.party')
    PartyIdentifier = Model.get('party.identifier')
    Address = Model.get('party.address')
    Country = Model.get('country.country')
    #Subdivision = Model.get('country.subdivision')
    ContactMechanism = Model.get('party.contact_mechanism')


    for row in range(sheet.nrows)[1:]:
        cont += 1
        name = sheet.cell(row, 0).value
        id_type = sheet.cell(row, 1).value
        identifier = sheet.cell(row, 2).value
        street = sheet.cell(row, 3).value
        phone = sheet.cell(row, 4).value
        email = sheet.cell(row, 5).value
        city = sheet.cell(row, 6).value

        ec, = Country.find([('code', '=', 'EC')])
        #azuay, = Subdivision.find([('code', '=', subdivision)])

        try:
            try:
                party, = Party.find([('name', '=', name)])
                print("Party existe....", name)
            except:
                party = Party(name=name)
                party.addresses.pop()
                party.addresses.append(Address(street=street,
                    country=ec, city=city))
                party.contact_mechanisms.append(
                    ContactMechanism(type='phone', value=phone))
                if email:
                    party.contact_mechanisms.append(
                        ContactMechanism(type='email', value=email))
                party.identifiers.append(
                    PartyIdentifier(type=id_type, code=identifier))
                party.identifiers[0].code = identifier
                party.save()
        except Exception as e:
            print('Incorrecto --->',sheet.cell(row, 0).value, str(e))
    print(datetime.now())

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host',
        help='host', default='localhost')
    parser.add_argument('--port', dest='port',
        help='port', default='8000')
    parser.add_argument('--database', dest='database',
        help='database', required=True)
    parser.add_argument('--user', dest='user',
        help='user', required=True)
    parser.add_argument('--password', dest='password',
        help='password', required=True)
    parser.add_argument('--filename', dest='filename',
        help='filename', required=True)
    options = parser.parse_args()
    create_parties(options.host, options.port, options.database,
        options.user, options.password, options.filename)
    
    
    
