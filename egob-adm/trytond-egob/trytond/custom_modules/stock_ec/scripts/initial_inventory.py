from datetime import datetime
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from getpass import getpass

from proteus import config, Model

import xlrd

import pdb


def initial_inventory(config_file, database, effective_date, filename,
    by_warehouse=False, digits=8):
    digits = int(digits)

    username = raw_input('Username: ')
    password = getpass('Password: ')
    initial_date = datetime.strptime(effective_date, '%Y-%m-%d')

    #config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
    #    % (username, password, host, port, database))
    config.set_trytond(user=username, database=database,
        config_file=config_file)

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    pdb.set_trace()

    lines_warehouse = {}

    Purchase = Model.get('purchase.purchase')
    PurchaseLine = Model.get('purchase.line')
    Product = Model.get('product.product')
    ShipmentIn = Model.get('stock.shipment.in')
    Location = Model.get('stock.location')
    StockMove = Model.get('stock.move')
    Party = Model.get('party.party')
    Department = Model.get('company.department')
    party, = Party.find([('name', '=', 'Inventario Inicial')])
    department, = Department.find([])

    purchase = Purchase()
    purchase.party = party
    purchase.date = initial_date
    purchase.department = department
    purchase.save()
    products = {}
    lines = []
    for product in Product.find([]):
        products[product.account_category.code + product.name] = product
    pdb.set_trace()

    for row in range(sheet.nrows)[1:]:
        cont += 1
        category_code = str(sheet.cell(row, 0).value)
        name = sheet.cell(row, 2).value
        # product, = Product.find([
        #     ('account_category.code', '=', category_code),
        #     ('name', '=', name)
        # ])
        product = products[category_code + name]
        product_qty = sheet.cell(row, 3).value
        product_price = Decimal(
            sheet.cell(row, 5).value).quantize(Decimal(10) ** -digits)
        if by_warehouse:
            lines_warehouse[product.id] = sheet.cell(row, 7).value
        line = PurchaseLine()
        # purchase.lines.append(line)
        line.product = product
        line.quantity = product_qty
        line.unit_price = product_price
        line.purchase = purchase
        lines.append(line)
        print('Correcto ---> ', cont, ' de ', sheet.cell(row, 0).value)
    PurchaseLine.save(lines)
    pdb.set_trace()
    purchase.click('quote')
    purchase.click('confirm')
    purchase.click('process')
    shipment = ShipmentIn()
    shipment.supplier = party
    shipment.effective_date = initial_date
    moves = StockMove.find([('purchase', '=', purchase.id)])
    for move in moves:
        print(move.id)
        shipment.incoming_moves.append(move)
    shipment.save()
    print("Recibiendo...")
    shipment.click('receive')
    print("Recibido...")
    to_save = []
    if by_warehouse:
        for line in shipment.inventory_moves:
            code_dest = lines_warehouse[line.product.id]
            location_dest, = Location.find([('code', '=', code_dest)])
            line.to_location = location_dest
            to_save.append(line)
    StockMove.save(to_save)
    print(datetime.now())


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    # parser.add_argument('--host', dest='host', default='localhost',
    #     help='localhost')
    # parser.add_argument('--port', dest='port', default='8000',
    #     help='port')
    parser.add_argument('--config', dest='config_file', required=True,
        help="path to config file")
    parser.add_argument('--database', dest='database', required=True,
        help='port')
    parser.add_argument('--date', dest='effective_date', required=True,
        help='%Y-%m-%d  Ex:2018-01-31')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    parser.add_argument('--by_warehouse', dest='by_warehouse', required=True,
        help='By warehouse')
    parser.add_argument('--digits', dest='digits', default=6,
        help='digits')
    options = parser.parse_args()
    initial_inventory(options.config_file, options.database,
        options.effective_date, options.filename, options.by_warehouse,
        options.digits)
