from proteus import config, Model
from datetime import datetime
import xlrd
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import pdb


def create_locations(host, port, database, user, password, filename):
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(user, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    pdb.set_trace()

    Location = Model.get('stock.location')

    for row in range(sheet.nrows)[1:]:
        cont += 1
        name = sheet.cell(row, 0).value
        parent_name = sheet.cell(row, 1).value
        type_ = sheet.cell(row, 2).value

        location = Location()
        location.name = name
        location.type = type_
        if parent_name:
            parent, = Location.find([('name', '=', parent_name)])
            if parent:
                location.parent = parent
            else:
                print('parent no encontrado')
        location.save()
        print(cont, ' de ', total)
        print(
        'Correcto --->',sheet.cell(row, 0).value)

    print(datetime.now())

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host', default='localhost',
        help='localhost')
    parser.add_argument('--port', dest='port', default='8000',
        help='port')
    parser.add_argument('--database', dest='database', required=True,
        help='port')    
    parser.add_argument('--user', dest='user', required=True,
        help='user')
    parser.add_argument('--password', dest='password', required=True,
        help='password')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_locations(options.host, options.port, options.database,
        options.user, options.password, options.filename)
