from decimal import Decimal
import datetime
from collections import defaultdict

from sql import Union, Literal, Column, Window
from sql.aggregate import Sum
from sql.conditionals import Coalesce, Case

from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    StateReport, Button
from trytond.pyson import PYSONEncoder, Eval, Bool, If, Or

from trytond.modules.company import CompanyReport
from trytond.modules.company.model import CompanyValueMixin
from trytond.modules.product import price_digits, TemplateFunction
from trytond.modules.stock.move import _location_children
from trytond.modules.product.product import STATES
from trytond.modules.product.product import DEPENDS

__all__ = ['ProductCategory', 'ProductTemplate', 'ProductInstallPrice',
    'ProductBasePriceField', 'Product', 'ProductKardex', 'StockKardexContext',
    'ProductKardexContext', 'OpenProductKardex', 'PrintProductKardexStart',
    'PrintProductKardex', 'ProductKardexReport', 'ProductListReport',
    'ProductSummary', 'ProductSummaryReport', 'CategorySummary',
    'CategorySummaryReport', 'DepartmentSummary', 'DepartmentSummaryReport',
    'ProductsRecomputeStart', 'ProductsRecompute']


class ProductCategory:
    __metaclass__ = PoolMeta
    __name__ = "product.category"
    _history = True

    code = fields.Char('Code', required=True)
    product_sequence = fields.Many2One('ir.sequence', 'Product sequence',
        domain=[('code', '=', 'product.product')], required=True)
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    storage_location = fields.Many2One(
        'stock.location', 'Storage location', domain=[('type', '=', 'storage')])

    @classmethod
    def __setup__(cls):
        super(ProductCategory, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_name_uniq', Unique(t, t.code, t.name),
                'code_name_uniq'),
            ]
        cls.parent.left = 'left'
        cls.parent.right = 'right'
        cls._error_messages.update({
                'code_name_uniq': 'Name and code must be unique.',
                })

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    def get_rec_name(self, name):
        code = ''
        if self.code:
            code = self.code + ' / '
        return code + self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]


class ProductTemplate:
    __metaclass__ = PoolMeta
    __name__ = "product.template"
    _history = True

    public_price = fields.Function(
        fields.Numeric("Public Price", digits=price_digits), 'get_public_price')
    install_price = fields.MultiValue(
        fields.Numeric("Install Price", required=True, digits=price_digits,
        states=STATES, depends=DEPENDS))
    install_prices = fields.One2Many(
        'product.install_price', 'template', "Install Prices")
    base_price_field = fields.MultiValue(fields.Selection([
        ('cost_price', 'Cost price'),
        ('list_price', 'List price'),
    ], 'Public price based on', required=True))
    base_price_fields = fields.One2Many(
        'product.base_price_field', 'template', "Base Price Fields")

    @classmethod
    def __setup__(cls):
        super(ProductTemplate, cls).__setup__()
        cls.account_category.states['required'] = True
        t = cls.__table__()
        cls.account_category.required = True
        cls._sql_constraints = [
            ('name_uniq', Unique(t, t.name, t.account_category),
                'name_uniq'),
        ]
        cls._error_messages.update({
            'name_uniq': 'Name must be unique by category.',
        })
        cls.list_price.states['invisible'] = Or(
            cls.list_price.states.get('invisible', False),
            If(Bool(Eval('base_price_field')),
               (Eval('base_price_field') == 'cost_price'),
               (False))
        )

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'install_price':
            return pool.get('product.install_price')
        elif field == 'base_price_field':
            return pool.get('product.base_price_field')
        return super(ProductTemplate, cls).multivalue_model(field)

    @classmethod
    def default_base_price_field(cls, **pattern):
        return cls.multivalue_model(
            'base_price_field').default_base_price_field()

    @classmethod
    def get_public_price(cls, templates, names):
        pool = Pool()
        result = {}
        for name in names:
            result[name] = defaultdict(lambda: 0)
        Config = pool.get('product.configuration')
        config = Config(1)
        price_list = config.price_list
        for template in templates:
            if template.base_price_field:
                base_price_field = template.base_price_field
                base_price = getattr(template, template.base_price_field)
            else:
                base_price_field = config.base_price_field
                base_price = getattr(template, base_price_field)
            base_price = getattr(template, base_price_field)
            for name in names:
                if name == 'public_price':
                    if price_list:
                        result[name][template.id] = price_list.compute(
                            party=None,
                            product=None,
                            unit_price=base_price,
                            quantity=None,
                            uom=None)
                    else:
                        result[name][template.id] = base_price
        return result

    @fields.depends('base_price_field')
    def on_change_base_price_field(self):
        if self.base_price_field and self.base_price_field == 'cost_price':
            self.list_price = 0


class ProductInstallPrice(ModelSQL, CompanyValueMixin):
    "Product Install Price"
    __name__ = 'product.install_price'
    template = fields.Many2One(
        'product.template', "Template", ondelete='CASCADE', select=True)
    install_price = fields.Numeric("Install Price", digits=price_digits)


class ProductBasePriceField(ModelSQL, CompanyValueMixin):
    "Product Base Price Field"
    __name__ = 'product.base_price_field'
    template = fields.Many2One(
        'product.template', "Template", ondelete='CASCADE', select=True)
    base_price_field = fields.Selection([
        ('cost_price', 'Cost price'),
        ('list_price', 'List price')
    ], 'Public price based on')

    @classmethod
    def default_base_price_field(cls):
        return 'cost_price'


class Product:
    __metaclass__ = PoolMeta
    __name__ = "product.product"
    _history = True

    external_code = fields.Char('External code')

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uniq', Unique(t, t.code),
                'code_uniq'),
        ]
        cls.code.states['readonly'] = True
        cls._error_messages.update({
            'code_uniq': 'Code must be unique.',
            'without_category': ('You can not create the product "%(name)s" '
                'without category.'),

        })
        cls.cost_price.states['readonly'] = True
        cls.list_price.states['invisible'] = Or(
            cls.list_price.states.get('invisible', False),
            If(Bool(Eval('base_price_field')),
               (Eval('base_price_field') == 'cost_price'),
               (False))
        )

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Template = pool.get('product.template')

        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            template = Template(vals['template'])
            if template.account_category:
                category = template.account_category
                vals['code'] = category.code + '-' + Sequence.get_id(
                    template.account_category.product_sequence.id)
            else:
                cls.raise_user_error('without_category', {
                    'name': template.name,
                })
        return super(Product, cls).create(vlist)

    def recompute_cost_price_average(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Currency = pool.get('currency.currency')
        Product = pool.get('product.product')
        Uom = pool.get('product.uom')
        digits = Product.cost_price.digits

        context = Transaction().context

        if not isinstance(self.__class__.cost_price, TemplateFunction):
            product_clause = ('product', '=', self.id)
        else:
            product_clause = ('product.template', '=', self.template.id)

        moves = Move.search([
                product_clause,
                ('state', '=', 'done'),
                ('company', '=', context.get('company')),
                ['OR',
                    [
                        ('to_location.type', '=', 'storage'),
                        ('from_location.type', '!=', 'storage'),
                        ],
                    [
                        ('from_location.type', '=', 'storage'),
                        ('to_location.type', '!=', 'storage'),
                        ],
                    ],
                ], order=[('effective_date', 'ASC'), ('id', 'ASC')])

        cost_price = Decimal(0)
        quantity = 0
        to_save = []
        for move in moves:
            qty = Uom.compute_qty(move.uom, move.quantity, self.default_uom)
            qty = Decimal(str(qty))
            if move.from_location.type == 'storage':
                qty *= -1
            if (move.from_location.type in ['supplier', 'production']
                or move.to_location.type == 'supplier'):
                with Transaction().set_context(date=move.effective_date):
                    unit_price = Currency.compute(
                        move.currency, move.unit_price,
                        move.company.currency, round=False)
                unit_price = Uom.compute_price(move.uom, unit_price,
                    self.default_uom)
                if quantity + qty > 0 and quantity >= 0:
                    cost_price = (
                        (cost_price * quantity) + (unit_price * qty)
                        ) / (quantity + qty)
                elif qty > 0:
                    cost_price = unit_price
            quantity += qty
            if move.cost_price != cost_price:
                cost = cost_price.quantize(Decimal(str(10.0 ** -digits[1])))
                move.cost_price = cost
                to_save.append(move)
        Move.save(to_save)
        return cost_price

    @staticmethod
    def get_sale_price(products, quantity=0):
        prices = {}
        for product in products:
            if product.base_price_field == 'cost_price':
                prices[product.id] = product.public_price + \
                    product.install_price
            else:
                prices[product.id] = product.list_price
        return prices


class StockKardexContext(ModelView):
    'Stock Kardex Context'
    __name__ = "stock.kardex.context"
    location = fields.Many2One('stock.location', 'Location', required=True)
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    with_childs = fields.Boolean('With childs')

    @classmethod
    def default_location(cls):
        Location = Pool().get('stock.location')
        warehouse, = Location.search([('code', '=', 'STO')])
        return warehouse.id

    @classmethod
    def default_with_childs(cls):
        return True

    @classmethod
    def get_fiscalyear_date(cls, field):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        Date = pool.get('ir.date')
        today = Date.today()
        fiscalyear = FiscalYear.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today)
        ])
        if fiscalyear:
            fiscalyear, = fiscalyear
            return getattr(fiscalyear, field)
        return None

    @classmethod
    def default_start_date(cls):
        return cls.get_fiscalyear_date('start_date')

    @classmethod
    def default_end_date(cls):
        return cls.get_fiscalyear_date('end_date')


class ProductKardex(ModelSQL, ModelView):
    'Product Kardex'
    __name__ = 'stock.product.kardex'

    move = fields.Many2One('stock.move', 'Move')
    shipment = fields.Reference('Shipment', selection='get_shipment',)
    product = fields.Many2One('product.product', 'Product')
    internal_quantity = fields.Float('Internal Quantity', digits=(16, 2))
    move_uom = fields.Many2One('product.uom', 'Move Uom')
    quantity = fields.Float('Quantity', digits=(16, 2))
    input_qty = fields.Float('Input Qty', digits=(16, 2))
    output_qty = fields.Float('Output Qty', digits=(16, 2))
    input_value = fields.Float('Input Value', digits=(16, 4))
    output_value = fields.Float('Output Value', digits=(16, 4))
    move_price = fields.Float('Move price', digits=(16, 4))
    average_price = fields.Float('Average price', digits=(16, 4))
    effective_date = fields.Date('Effective date')
    account_category = fields.Many2One('product.category', 'Category')
    department = fields.Many2One('company.department', 'Departament')
    employee = fields.Many2One('company.employee', 'Employee')
    from_location = fields.Many2One('stock.location', 'From location')
    to_location = fields.Many2One('stock.location', 'To location')
    subtotal = fields.Float('Subtotal', digits=(16, 4))
    balance_qty = fields.Float('Balance Qty', digits=(16, 2))
    balance_uom = fields.Many2One('product.uom', 'Balance Uom')
    valuation = fields.Float('Valuation', digits=(16, 4))

    @classmethod
    def __setup__(cls):
        super(ProductKardex, cls).__setup__()
        cls._order.insert(0, ('effective_date', 'ASC'))
        cls._order.insert(1, ('id', 'ASC'))

    @classmethod
    def table_query(cls):
        pool = Pool()
        Location = pool.get('stock.location')
        Move = pool.get('stock.move')
        Product = pool.get('product.product')
        Template = pool.get('product.template')
        Category = pool.get('product.category')
        Department = pool.get('company.department')
        move_t = Move.__table__()
        product_t = Product.__table__()
        template_t = Template.__table__()
        category_t = Category.__table__()
        department_t = Department.__table__()
        context = {}
        if Transaction().context:
            context.update(Transaction().context)
        start_date = context.get('start_date')
        end_date = context.get('end_date')
        product = context.get('product')
        location = context.get('location')
        if not location:
            return
        location_ids = [location]
        location = Location(location)
        if location.code == 'STO':
            input_zone, = Location.search([('code', '=', 'IN')])
            location_ids.append(input_zone.id)
        with_childs = context.get('with_childs')

        if with_childs:
            location_query = _location_children(location_ids, query=True)
        else:
            location_query = location_ids[:]
        where_dates = Literal(True)
        if start_date:
            where_dates = where_dates & (move_t.effective_date >= start_date)
        if end_date:
            where_dates = where_dates & (move_t.effective_date <= end_date)
        where_product = Literal(True)
        if product:
            where_product = (move_t.product == product)
        where_state = (move_t.state == 'done')
        where_in = (move_t.to_location.in_(location_query) &
            ~move_t.from_location.in_(location_query))
        where_out = (move_t.from_location.in_(location_query) &
            ~move_t.to_location.in_(location_query))
        if context.get('stock_destinations'):
            destinations = context.get('stock_destinations')
            dest_clause_from = move_t.from_location.in_(destinations)
            dest_clause_to = move_t.to_location.in_(destinations)
        else:
            dest_clause_from = dest_clause_to = Literal(True)
        columns_in = [
            move_t.id,
            move_t.create_uid,
            move_t.create_date,
            move_t.write_uid,
            move_t.write_date,
            move_t.id.as_('move'),
            move_t.uom.as_('move_uom'),
            move_t.shipment,
            move_t.product,
            move_t.internal_quantity,
            move_t.quantity,
            move_t.internal_quantity.as_('input_qty'),
            Literal(0).as_('output_qty'),
            Case((move_t.unit_price != 0, move_t.unit_price),
                 else_=move_t.cost_price).as_('move_price'),
            move_t.cost_price.as_('average_price'),
            move_t.effective_date,
            template_t.account_category,
            template_t.default_uom.as_('balance_uom'),
            move_t.department,
            move_t.employee,
            move_t.from_location,
            move_t.to_location,
        ]
        columns_out = columns_in[:]
        columns_out[9] = (move_t.internal_quantity * -1).as_(
            'internal_quantity')
        columns_out[10] = (move_t.quantity * -1).as_(
            'quantity')
        columns_out[11] = Literal(0).as_('input_qty')
        columns_out[12] = (move_t.internal_quantity * -1).as_('output_qty')

        def make_join(columns, where_direction, dest_clause):
            query = move_t.join(product_t,
                condition=move_t.product == product_t.id
            ).join(template_t,
                   condition=product_t.template == template_t.id
            ).join(category_t, type_='LEFT',
                   condition=template_t.account_category == category_t.id
            ).join(department_t, type_='LEFT',
                   condition=move_t.department == department_t.id
            ).select(
                *columns,
                where=where_state & where_product & where_direction &
                where_dates & dest_clause
            )
            return query

        query_in = make_join(columns_in, where_in, dest_clause_from)
        query_out = make_join(columns_out, where_out, dest_clause_to)
        query_temp = Union(query_in, query_out)
        rfields = [str(c).replace('"', '') for c in query_in._columns]
        result_columns = [Column(query_temp, c).as_(c) for c in rfields] + [
            (query_temp.move_price * query_temp.internal_quantity).as_(
                'subtotal'),
            Case((query_temp.internal_quantity > 0,
                 query_temp.quantity * query_temp.move_price),
                 else_=0).as_('input_value'),
            Case((query_temp.internal_quantity < 0,
                 query_temp.quantity * query_temp.move_price),
                 else_=0).as_('output_value'),
            Sum(query_temp.internal_quantity,
                window=Window([query_temp.product], order_by=[
                    query_temp.effective_date, query_temp.id
                ])).as_('balance_qty')
        ]
        query_window = query_temp.select(
            *result_columns,
            order_by=[query_temp.effective_date, query_temp.id])
        rfields = [str(c).replace('"', '') for c in query_window._columns]
        final_columns = [Column(query_window, c).as_(c) for c in rfields] + [
            (query_window.balance_qty * query_window.average_price).as_(
                'valuation')
        ]
        return query_window.select(*final_columns)

    @classmethod
    def get_shipment(cls):
        Move = Pool().get('stock.move')
        return Move.get_shipment()


class ProductKardexContext(StockKardexContext):
    'Product Kardex Context'
    __name__ = 'stock.product.kardex.context'
    product = fields.Many2One('product.product', 'Product', required=True)

    @classmethod
    def default_product(cls):
        return Transaction().context.get('product')


class OpenProductKardex(Wizard):
    'Open Product Kardex'
    __name__ = 'open.product.kardex'
    start_state = 'open_'
    open_ = StateAction('stock_ec.act_product_kardex')

    def do_open_(self, action):
        Location = Pool().get('stock.location')
        location, = Location.search([('code', '=', 'STO')])
        action['pyson_context'] = PYSONEncoder().encode({
                'product': Transaction().context['active_id'],
                'location': location.id,
                })
        return action, {}


class PrintProductKardexStart(ModelView):
    'Print Product Kardex Start'
    __name__ = 'print.product.kardex.start'
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')

    @classmethod
    def get_fiscalyear_date(cls, field):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        Date = pool.get('ir.date')
        today = Date.today()
        fiscalyear = FiscalYear.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today)
        ])
        if fiscalyear:
            fiscalyear, = fiscalyear
            return getattr(fiscalyear, field)
        return None

    @classmethod
    def default_start_date(cls):
        return cls.get_fiscalyear_date('start_date')

    @classmethod
    def default_end_date(cls):
        return cls.get_fiscalyear_date('end_date')


class PrintProductKardex(Wizard):
    'Print Product Kardex'
    __name__ = 'print.product.kardex'
    start = StateView('print.product.kardex.start',
        'stock_ec.print_product_kardex_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'print_', 'tryton-print', default=True),
            ])
    print_ = StateReport('stock.product.kardex.report')

    def do_print_(self, action):
        Location = Pool().get('stock.location')
        location, = Location.search([('code', '=', 'STO')])
        action['pyson_context'] = PYSONEncoder().encode({
            'product': Transaction().context.get('product'),
            'location': location.id,
        })
        data = {
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            }
        return action, data


class ProductListReport(CompanyReport):
    __name__ = 'stock.product.list.report'

    @classmethod
    def get_context(cls, records, data):
        Location = Pool().get('stock.location')
        report_context = super(ProductListReport, cls).get_context(
            records, data)
        context = Transaction().context
        locations = context.get('locations', [])
        if locations:
            locations = Location.search([('id', 'in', locations)])
        report_context['locations'] = locations
        report_context['price_digits'] = price_digits[1]
        report_context['start_date'] = context.get('start_date')
        report_context['to_date'] = context.get('end_date')
        report_context['Decimal'] = Decimal
        return report_context


class ProductKardexReport(CompanyReport):
    __name__ = 'stock.product.kardex.report'

    @classmethod
    def _get_records(cls, ids, model, data):
        Kardex = Pool().get(model)
        start_date = data.get('start_date')
        end_date = data.get('end_date')
        with Transaction().set_context(start_date=None, end_date=None):
            domain = []
            if start_date:
                domain += [('effective_date', '>=', start_date)]
            if end_date:
                domain += [('effective_date', '<=', end_date)]
            return Kardex.search(domain)

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Product = pool.get('product.product')
        Location = pool.get('stock.location')
        report_context = super(ProductKardexReport, cls).get_context(
            records, data)
        context = Transaction().context
        product = Product(context['product'])
        location = Location(context['location'])
        report_context['product'] = product
        report_context['location'] = location
        report_context['price_digits'] = price_digits[1]
        report_context['from_date'] = data['start_date']
        report_context['to_date'] = data['end_date']
        report_context['get_summary'] = cls._get_summary
        return report_context

    @classmethod
    def _get_summary(cls):
        context = Transaction().context
        start_date = context.get('start_date')
        end_date = context.get('end_date')
        with Transaction().set_context(start_date=start_date,
                end_date=end_date, product=context.get('product')):
            summary = ProductSummary.search([])
        return summary


class ProductSummary(ModelSQL, ModelView):
    'ProductSummary'
    __name__ = 'stock.product.summary'

    product = fields.Many2One('product.product', 'Product')
    account_category = fields.Many2One('product.category', 'Category')
    initial_qty = fields.Float('Initial qty', digits=(16, 6))
    initial_value = fields.Float('Initial', digits=(16, 6))
    input_qty = fields.Float('Input Qty', digits=(16, 2))
    input_value = fields.Float('Input value', digits=(16, 6))
    output_qty = fields.Float('Output Qty', digits=(16, 2))
    output_value = fields.Float('Output value', digits=(16, 6))
    balance_qty = fields.Float('Balance Qty', digits=(16, 6))
    balance_uom = fields.Many2One('product.uom', 'Balance Uom')
    valuation = fields.Float('Valuation', digits=(16, 6))
    average_price = fields.Float('Average price', digits=(16, 6))

    @classmethod
    def __setup__(cls):
        super(ProductSummary, cls).__setup__()
        cls._order.insert(0, ('product', 'ASC'))

    @classmethod
    def search_rec_name(self, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('product.rec_name',) + tuple(clause[1:]),
            ]

    @classmethod
    def run_kardex(cls, type_='initial'):
        '''
        Run kardex in order to get initial data or data between dates
        type: 'initial': To get initial values
              'between': To get inputs and outputs on period
        '''
        pool = Pool()
        Kardex = pool.get('stock.product.kardex')
        query = Kardex.table_query()
        if query and type_ == 'initial':
            query = query.select(
                query.product,
                Sum(query.internal_quantity).as_('initial_qty'),
                Sum(query.subtotal).as_('initial_value'),
                group_by=[query.product]
            )
        elif query and type_ == 'between':
            query = query.select(
                query.product,
                Sum(query.input_qty).as_('input_qty'),
                Sum(query.input_value).as_('input_value'),
                Sum(query.output_qty).as_('output_qty'),
                Sum(query.output_value).as_('output_value'),
                group_by=[query.product]
            )
        return query

    @classmethod
    def table_query(cls):
        pool = Pool()
        Product = pool.get('product.product')
        Template = pool.get('product.template')
        product_table = Product.__table__()
        template_table = Template.__table__()
        context = Transaction().context.copy()
        if context.get('product'):
            where_product = product_table.id == context.get('product')
        else:
            where_product = Literal(True)
        if context.get('start_date'):
            end_date = context['start_date'] - datetime.timedelta(days=1)
            with Transaction().set_context(start_date=None, end_date=end_date):
                query_initial = cls.run_kardex(type_='initial')
        else:
            query_initial = product_table.select(
                product_table.id.as_('product'),
                Literal(0).as_('initial_qty'),
                Literal(0).as_('initial_value'),
            )
        query_between = cls.run_kardex(type_='between')
        query_temp = None
        if query_initial and query_between:
            query_temp = product_table.join(template_table,
                condition=product_table.template == template_table.id
            ).join(query_initial, type_='LEFT',
                   condition=product_table.id == query_initial.product
            ).join(query_between, type_='LEFT',
                   condition=product_table.id == query_between.product
            ).select(
                product_table.id,
                product_table.create_uid,
                product_table.create_date,
                product_table.write_uid,
                product_table.write_date,
                product_table.id.as_('product'),
                template_table.account_category,
                template_table.default_uom.as_('balance_uom'),
                Coalesce(query_initial.initial_qty, 0).as_('initial_qty'),
                Coalesce(query_initial.initial_value, 0).as_('initial_value'),
                Coalesce(query_between.input_qty, 0).as_('input_qty'),
                Coalesce(query_between.input_value, 0).as_('input_value'),
                Coalesce(query_between.output_qty, 0).as_('output_qty'),
                Coalesce(query_between.output_value, 0).as_('output_value'),
                where=where_product
            )
            rfields = [str(c).replace('"', '') for c in query_temp._columns]
            columns = [Column(query_temp, c).as_(c) for c in rfields]
            balance_columns = columns + [
                (query_temp.initial_qty + query_temp.input_qty + \
                 query_temp.output_qty).as_('balance_qty'),
                (query_temp.initial_value + query_temp.input_value + \
                 query_temp.output_value).as_('valuation'),
            ]
            query_temp = query_temp.select(*balance_columns)
            rfields = [str(c).replace('"', '') for c in query_temp._columns]
            columns = [Column(query_temp, c).as_(c) for c in rfields]
            final_columns = columns + [
                Case((query_temp.balance_qty > 0,
                      query_temp.valuation / query_temp.balance_qty),
                     else_=0).as_('average_price')
            ]
            query_temp = query_temp.select(*final_columns)
        return query_temp


class KardexSummaryReport(CompanyReport):

    @classmethod
    def get_context(cls, records, data):
        Location = Pool().get('stock.location')
        report_context = super(KardexSummaryReport, cls).get_context(
            records, data)
        context = Transaction().context
        location = context.get('location')
        location = Location(location)
        report_context['location'] = location
        report_context['price_digits'] = price_digits[1]
        report_context['from_date'] = context.get('start_date')
        report_context['to_date'] = context.get('end_date')
        return report_context


class ProductSummaryReport(KardexSummaryReport):
    __name__ = 'stock.product.summary.report'


class KardexSummary(ModelView, ModelSQL):
    initial_value = fields.Float('Initial', digits=(16, 6))
    input_value = fields.Float('Input value', digits=(16, 6))
    output_value = fields.Float('Output value', digits=(16, 6))
    valuation = fields.Float('Valuation', digits=(16, 6))

    @classmethod
    def run_kardex(cls, type_, field):
        '''
        Run kardex in order to get initial data or data between dates
        type: 'initial': To get initial values
              'between': To get inputs and outputs on period
        '''
        pool = Pool()
        Kardex = pool.get('stock.product.kardex')
        query = Kardex.table_query()
        if query:
            column = getattr(query, field)
            if type_ == 'initial':
                query = query.select(
                    column,
                    Sum(query.subtotal).as_('initial_value'),
                    where=(column != None),
                    group_by=[column])
            elif type_ == 'between':
                column = getattr(query, field)
                query = query.select(
                    column,
                    Sum(query.input_value).as_('input_value'),
                    Sum(query.output_value).as_('output_value'),
                    group_by=[column]
                )
        return query

    @classmethod
    def master_query(cls, table_name, field_name):
        pool = Pool()
        Table = pool.get(table_name)
        table = Table.__table__()
        context = Transaction().context.copy()

        if context.get('start_date'):
            end_date = context['start_date'] - datetime.timedelta(days=1)
            with Transaction().set_context(start_date=None, end_date=end_date):
                query_initial = cls.run_kardex(
                    type_='initial', field=field_name)
        else:
            query_initial = table.select(
                table.id.as_(field_name),
                Literal(0).as_('initial_value'),
            )
        query_between = cls.run_kardex(type_='between', field=field_name)
        query_temp = None
        if query_initial and query_between:
            query_temp = table.join(query_initial, type_='LEFT',
                condition=table.id == getattr(query_initial, field_name)
            ).join(query_between, type_='LEFT',
                   condition=table.id == getattr(query_between, field_name)
            ).select(
                table.id,
                table.create_uid,
                table.create_date,
                table.write_uid,
                table.write_date,
                table.id.as_(field_name),
                Coalesce(query_initial.initial_value, 0).as_('initial_value'),
                Coalesce(query_between.input_value, 0).as_('input_value'),
                Coalesce(query_between.output_value, 0).as_('output_value'),
            )
            rfields = [str(c).replace('"', '') for c in query_temp._columns]
            columns = [Column(query_temp, c).as_(c) for c in rfields]
            final_columns = columns + [
                (query_temp.initial_value + query_temp.input_value + \
                 query_temp.output_value).as_('valuation'),
            ]
            query_temp = query_temp.select(*final_columns)
        return query_temp


class CategorySummary(KardexSummary):
    'Category Summary'
    __name__ = 'stock.category.summary'

    account_category = fields.Many2One('product.category', 'Category')

    @classmethod
    def __setup__(cls):
        super(CategorySummary, cls).__setup__()
        cls._order.insert(0, ('account_category.code', 'ASC'))

    @classmethod
    def table_query(cls):
        query = super(CategorySummary, cls).master_query(
            'product.category', 'account_category')
        if query:
            pool = Pool()
            Category = pool.get('product.category')
            table_a = Category.__table__()
            table_c = Category.__table__()
            table_f = Category.__table__()
            result = table_a.join(table_c,
                condition=(table_c.left >= table_a.left)
                & (table_c.right <= table_a.right)
            ).join(query, 'LEFT', condition=query.id == table_c.id
            ).select(
                table_a.id,
                Sum(query.initial_value).as_('initial_value'),
                Sum(query.input_value).as_('input_value'),
                Sum(query.output_value).as_('output_value'),
                Sum(query.valuation).as_('valuation'),
                group_by=table_a.id)
            query_final = result.join(table_f, condition=result.id == table_f.id
            ).select(
                table_f.id,
                table_f.create_uid,
                table_f.create_date,
                table_f.write_uid,
                table_f.write_date,
                table_f.id.as_('account_category'),
                result.initial_value,
                result.input_value,
                result.output_value,
                result.valuation,
            )
            return query_final
        return None


class CategorySummaryReport(KardexSummaryReport):
    __name__ = 'stock.category.summary.report'


class DepartmentSummary(KardexSummary):
    'Department Summary'
    __name__ = 'stock.department.summary'

    department = fields.Many2One('company.department', 'Department')

    @classmethod
    def __setup__(cls):
        super(DepartmentSummary, cls).__setup__()
        cls._order.insert(0, ('department', 'ASC'))

    @classmethod
    def table_query(cls):
        query = super(DepartmentSummary, cls).master_query(
            'company.department', 'department')
        return query


class DepartmentSummaryReport(KardexSummaryReport):
    __name__ = 'stock.department.summary.report'


class ProductsRecomputeStart(ModelView):
    'Products Recompute Price Start'
    __name__ = 'products.recompute.start'
    products = fields.Many2Many('product.product', None, None, 'Products',
        required=True)


class ProductsRecompute(Wizard):
    'Products Recompute Wizard'
    __name__ = 'products.recompute'
    start = StateView('products.recompute.start',
        'stock_ec.products_recompute_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Recompute', 'recompute', 'tryton-ok', default=True),
            ])
    recompute = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        ShipmentIn = pool.get('stock.shipment.in')
        if (Transaction().context.get('active_model', '') == 'stock.shipment.in'
            and Transaction().context.get('active_id')):
            shipment = ShipmentIn(Transaction().context['active_id'])
            products = []
            for move in shipment.moves:
                products.append(move.product.id)
            return {
                'products': products,
                }
        return {}

    def transition_recompute(self):
        pool = Pool()
        Product = pool.get('product.product')
        Product.recompute_cost_price(self.start.products)
        return 'end'
