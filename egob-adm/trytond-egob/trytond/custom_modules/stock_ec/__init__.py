# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import product
from . import purchase
from . import configuration
from . import stock
from . import account
from . import party
from . import public_siim


def register():
    Pool.register(
        product.ProductCategory,
        product.ProductTemplate,
        product.ProductInstallPrice,
        product.ProductBasePriceField,
        product.Product,
        product.ProductKardex,
        product.StockKardexContext,
        product.ProductKardexContext,
        product.PrintProductKardexStart,
        product.ProductSummary,
        product.CategorySummary,
        product.DepartmentSummary,
        configuration.ProductConfiguration,
        configuration.ProductConfigurationPriceList,
        purchase.Purchase,
        purchase.PurchaseLine,
        configuration.PurchaseConfiguration,
        configuration.PurchaseConfigurationTax,
        stock.Location,
        stock.Period,
        stock.PeriodCache,
        stock.ShipmentIn,
        stock.ShipmentInReturn,
        stock.ShipmentInternal,
        stock.Move,
        stock.ShipmentReason,
        stock.ShipmentReasonCategory,
        stock.ShipmentTemplate,
        stock.ShipmentTemplateLine,
        stock.CreateShipmentFromTemplateStart,
        stock.CreateShipmentFromReferenceStart,
        stock.UnsetShipmentFromReferenceStart,
        stock.AssignToLocationStart,
        account.Period,
        configuration.StockConfiguration,
        configuration.StockConfigurationEmployeeDomain,
        configuration.StockConfigurationStockUser,
        configuration.StockConfigurationStockFromLocationProductItem,
        configuration.StockConfigurationStockToLocationProductItem,
        party.Party,
        product.ProductsRecomputeStart,
        public_siim.PublicSIIMItem,
        module='stock_ec', type_='model')
    Pool.register(
        product.OpenProductKardex,
        product.PrintProductKardex,
        product.ProductsRecompute,
        stock.AssignShipmentInternal,
        stock.AssignShipmentInReturn,
        stock.CreateShipmentFromTemplate,
        stock.CreateShipmentFromReference,
        stock.UnsetShipmentFromReference,
        stock.AssignToLocation,
        module='stock_ec', type_='wizard')
    Pool.register(
        product.ProductListReport,
        product.ProductKardexReport,
        product.ProductSummaryReport,
        product.CategorySummaryReport,
        product.DepartmentSummaryReport,
        stock.InteralShipmentReport,
        stock.OutShipmentReport,
        stock.MoveListReport,
        module='stock_ec', type_='report')
