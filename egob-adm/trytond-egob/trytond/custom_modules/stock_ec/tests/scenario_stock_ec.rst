========================
Stock Ec Tests
========================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)
    >>> before_yesterday = today - relativedelta(days=2)

Install stock Module::

    >>> config = activate_modules('stock_ec')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create before fiscal year::

    >>> oyb = today - relativedelta(years=1)
    >>> before_fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company, oyb))
    >>> before_fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.credit_account = cash
    >>> cash_journal.debit_account = cash
    >>> cash_journal.save()

Create tax::

    >>> tax = create_tax(Decimal('.12'))
    >>> tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create product category sequence::
    >>> Sequence = Model.get('ir.sequence')
    >>> sequence = Sequence()
    >>> sequence.name = 'Product Category'
    >>> sequence.code = 'product.product'
    >>> sequence.company = company
    >>> sequence.save()

Create product category::

    >>> ProductCategory = Model.get('product.category')
    >>> product_category = ProductCategory()
    >>> product_category.name = "Category"
    >>> product_category.code = "001"
    >>> product_category.product_sequence = sequence
    >>> product_category.accounting = True
    >>> product_category.save()

Create shipment reason::

    >>> ShipmentReason = Model.get('stock.shipment.reason')
    >>> shipment_reason = ShipmentReason()
    >>> shipment_reason.sequence = 1
    >>> shipment_reason.name = "Reason 1"
    >>> shipment_reason.categories.append(product_category)
    >>> shipment_reason.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Meter')])

    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.account_expense = expense
    >>> template.account_revenue = revenue
    >>> template.supplier_taxes.append(tax)
    >>> template.list_price = Decimal('2.0')
    >>> template.install_price = Decimal('1.0')
    >>> template.cost_price_method = 'average'
    >>> product, = template.products
    >>> product.cost_price = Decimal('1.5')
    >>> template.account_category = product_category
    >>> template.save()
    >>> product, = template.products

Create price list::

    >>> PriceList = Model.get('product.price_list')
    >>> PriceListLine = Model.get('product.price_list.line')
    >>> price_list = PriceList(name='30%')
    >>> price_list_line = PriceListLine(formula='unit_price*1.30')
    >>> price_list.lines.append(price_list_line)
    >>> price_list.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> storage_sub_loc = Location(
    ...     name="Storage Sub", type='storage', parent=storage_loc)
    >>> storage_sub_loc.save()
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])

Create stock department::

    >>> Department = Model.get('company.department')
    >>> department = Department()
    >>> department.name = 'Stock Department'
    >>> department.save()

Create HR position::

    >>> Position = Model.get('company.position')
    >>> position = Position(name='Sample position')
    >>> position.save()

Create scenario user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> scenario_user = User()
    >>> scenario_user.name = 'Stock'
    >>> scenario_user.login = 'stock'
    >>> scenario_user.main_company = company
    >>> stock_group, = Group.find([('name', '=', 'Stock')])
    >>> purchase_group, = Group.find([('name', '=', 'Purchase')])
    >>> scenario_user.groups.append(stock_group)
    >>> scenario_user.groups.append(purchase_group)
    >>> employee_party = Party(name="Employee")
    >>> employee_party.save()
    >>> employee = Employee(
    ...     party=employee_party, department=department, position=position)
    >>> employee.save()
    >>> scenario_user.employees.append(employee)
    >>> scenario_user.employee = employee
    >>> scenario_user.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Purchase 200 products::

    >>> set_user(1)
    >>> Purchase = Model.get('purchase.purchase')
    >>> PurchaseLine = Model.get('purchase.line')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.payment_term = payment_term
    >>> purchase.department = department
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 200.0
    >>> purchase_line.unit_price = Decimal('1.5000')
    >>> purchase.click('quote')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('confirm')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('process')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.state
    u'processing'
    >>> purchase.shipment_state
    u'waiting'
    >>> purchase.invoice_state
    u'waiting'
    >>> len(purchase.moves), len(purchase.shipment_returns), len(purchase.invoices)
    (1, 0, 1)
    >>> invoice, = purchase.invoices
    >>> invoice.origins == purchase.rec_name
    True

Validate Shipments::

    >>> Move = Model.get('stock.move')
    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment = ShipmentIn()
    >>> shipment.supplier = supplier
    >>> for move in purchase.moves:
    ...     incoming_move = Move(id=move.id)
    ...     shipment.incoming_moves.append(incoming_move)
    >>> shipment.save()
    >>> shipment.origins == purchase.rec_name
    True
    >>> shipment.effective_date = before_yesterday
    >>> shipment.click('receive')
    >>> shipment.click('done')
    >>> purchase.reload()
    >>> purchase.shipment_state
    u'received'
    >>> len(purchase.shipments), len(purchase.shipment_returns)
    (1, 0)

Check Cost Price is 1.50::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.500000')

Check Public Price is 1.50::

    >>> product.reload()
    >>> product.public_price.quantize(Decimal(str(10**-6)))
    Decimal('1.500000')

Change product configuration for price_list::

    >>> ProductConfig = Model.get('product.configuration')
    >>> product_config = ProductConfig(1)
    >>> product_config.price_list = price_list
    >>> product_config.save()

Check Public Price is 1.95::

    >>> product.reload()
    >>> product.public_price.quantize(Decimal(str(10**-6)))
    Decimal('1.950000')

Remove 50 units @ 1::

    >>> Shipment = Model.get('stock.shipment.internal')
    >>> StockMove = Model.get('stock.move')
    >>> shipment = Shipment()
    >>> shipment.planned_date = before_yesterday
    >>> shipment.effective_date = before_yesterday
    >>> shipment.from_location = storage_loc
    >>> shipment.to_location = lost_found_loc
    >>> shipment.department = department
    >>> shipment.employee = employee
    >>> shipment.reason = shipment_reason
    >>> move = shipment.moves.new()
    >>> move.product = product
    >>> move.quantity = 50
    >>> move.from_location = storage_loc
    >>> move.to_location = lost_found_loc
    >>> move.currency = company.currency
    >>> move.department = department
    >>> move.employee = employee
    >>> shipment.save()
    >>> shipment.assigned_by
    >>> shipment.done_by

    >>> shipment.click('wait')
    >>> shipment.state
    u'waiting'
    >>> shipment.click('assign_try')
    True
    >>> shipment.state
    u'assigned'
    >>> shipment.click('done')
    >>> shipment.state
    u'done'

Make 100 unit of the product available @ 2 ::

    >>> Purchase = Model.get('purchase.purchase')
    >>> PurchaseLine = Model.get('purchase.line')

    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.department = department
    >>> purchase.payment_term = payment_term
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product
    >>> unit_cm, = ProductUom.find([('name', '=', 'centimeter')])
    >>> purchase_line.unit = unit_cm
    >>> purchase_line.quantity = 10000
    >>> purchase_line.unit_price = Decimal('0.017')
    >>> purchase.click('quote')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('170.00'), Decimal('20.40'), Decimal('190.40'))
    >>> purchase.click('confirm')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('170.00'), Decimal('20.40'), Decimal('190.40'))
    >>> purchase.click('process')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('170.00'), Decimal('20.40'), Decimal('190.40'))
    >>> purchase.state
    u'processing'
    >>> purchase.shipment_state
    u'waiting'
    >>> purchase.invoice_state
    u'waiting'
    >>> len(purchase.moves), len(purchase.shipment_returns), len(purchase.invoices)
    (1, 0, 1)
    >>> invoice, = purchase.invoices
    >>> invoice.origins == purchase.rec_name
    True

Validate Shipments::

    >>> Move = Model.get('stock.move')
    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment = ShipmentIn()
    >>> shipment.supplier = supplier
    >>> for move in purchase.moves:
    ...     incoming_move = Move(id=move.id)
    ...     shipment.incoming_moves.append(incoming_move)
    >>> shipment.save()
    >>> shipment.origins == purchase.rec_name
    True
    >>> shipment.effective_date = yesterday
    >>> shipment.click('receive')
    >>> shipment.click('done')
    >>> purchase.reload()
    >>> purchase.shipment_state
    u'received'
    >>> len(purchase.shipments), len(purchase.shipment_returns)
    (1, 0)

Check Cost Price Average is 1.58::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.580000')

Remove 30 more unit @ 1::

    >>> Shipment = Model.get('stock.shipment.internal')
    >>> StockMove = Model.get('stock.move')
    >>> shipment1 = Shipment()
    >>> shipment1.planned_date = today
    >>> shipment1.from_location = storage_loc
    >>> shipment1.department = department
    >>> shipment1.employee = employee
    >>> shipment1.to_location = lost_found_loc
    >>> shipment1.reason = shipment_reason
    >>> move = shipment1.moves.new()
    >>> move.product = product
    >>> move.quantity = 30
    >>> move.from_location = storage_loc
    >>> move.to_location = lost_found_loc
    >>> move.department = department
    >>> move.employee = employee
    >>> move.currency = company.currency
    >>> shipment1.save()
    >>> shipment1.assigned_by
    >>> shipment1.done_by

    >>> shipment1.click('wait')
    >>> shipment1.state
    u'waiting'
    >>> shipment1.click('assign_try')
    True
    >>> shipment1.state
    u'assigned'
    >>> shipment1.click('done')
    >>> shipment1.state
    u'done'

Remove 20 more unit @ 1::

    >>> Shipment = Model.get('stock.shipment.internal')
    >>> StockMove = Model.get('stock.move')
    >>> shipment2 = Shipment()
    >>> shipment2.planned_date = today
    >>> shipment2.from_location = storage_loc
    >>> shipment2.to_location = lost_found_loc
    >>> shipment2.department = department
    >>> shipment2.employee = employee
    >>> shipment2.reason = shipment_reason
    >>> move = shipment2.moves.new()
    >>> move.product = product
    >>> move.quantity = 20
    >>> move.from_location = storage_loc
    >>> move.to_location = lost_found_loc
    >>> move.department = department
    >>> move.employee = employee
    >>> move.currency = company.currency
    >>> shipment2.save()
    >>> shipment2.assigned_by
    >>> shipment2.done_by

    >>> shipment2.click('wait')
    >>> shipment2.state
    u'waiting'
    >>> shipment2.click('assign_try')
    True
    >>> shipment2.state
    u'assigned'
    >>> shipment2.click('done')
    >>> shipment2.state
    u'done'

Check Product Summary::

    >>> ProductSummary = Model.get('stock.product.summary')
    >>> context = {
    ...     'location': storage_loc.id,
    ...     'with_childs': True,
    ...     'product': product.id,
    ...     }
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].balance_qty
    200.0
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].valuation
    316.0

Cancel purchase::

    >>> shipment_purchase, = purchase.shipments
    >>> shipment_purchase.click('cancel')
    >>> shipment_purchase.state
    u'cancel'

Check Cost Price is 1.50::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.500000')

Check Product Summary::

    >>> ProductSummary = Model.get('stock.product.summary')
    >>> context = {
    ...     'location': storage_loc.id,
    ...     'with_childs': True,
    ...     'product': product.id,
    ...     }
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].balance_qty
    100.0
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].valuation
    150.0

Cancel shipment2::

    >>> shipment2.click('cancel')
    >>> shipment2.state
    u'cancel'

Check Product Summary::

    >>> ProductSummary = Model.get('stock.product.summary')
    >>> context = {
    ...     'location': storage_loc.id,
    ...     'with_childs': True,
    ...     'product': product.id,
    ...     }
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].balance_qty
    120.0
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].valuation
    180.0

Check Cost Price is 1.5::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.500000')

Change purchase configuration for unit_price_with_price::

    >>> PurchaseConfig = Model.get('purchase.configuration')
    >>> purchase_config = PurchaseConfig(1)
    >>> purchase_config.unit_price_with_tax = True
    >>> purchase_config.save()

Purchase 200 products with tax in price::

    >>> Purchase = Model.get('purchase.purchase')
    >>> PurchaseLine = Model.get('purchase.line')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.payment_term = payment_term
    >>> purchase.department = department
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 200.0
    >>> purchase_line.unit_price = Decimal('1.5000')
    >>> purchase.click('quote')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('confirm')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('process')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.state
    u'processing'
    >>> purchase.shipment_state
    u'waiting'
    >>> purchase.invoice_state
    u'waiting'
    >>> len(purchase.moves), len(purchase.shipment_returns), len(purchase.invoices)
    (1, 0, 1)
    >>> invoice, = purchase.invoices
    >>> invoice.origins == purchase.rec_name
    True

Validate Shipments::

    >>> Move = Model.get('stock.move')
    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment = ShipmentIn()
    >>> shipment.supplier = supplier
    >>> for move in purchase.moves:
    ...     incoming_move = Move(id=move.id)
    ...     shipment.incoming_moves.append(incoming_move)
    >>> shipment.save()
    >>> shipment.origins == purchase.rec_name
    True
    >>> shipment.effective_date = today
    >>> shipment.click('receive')
    >>> shipment.click('done')
    >>> purchase.reload()
    >>> purchase.shipment_state
    u'received'
    >>> len(purchase.shipments), len(purchase.shipment_returns)
    (1, 0)

Check Product Summary::

    >>> ProductSummary = Model.get('stock.product.summary')
    >>> context = {
    ...     'location': storage_loc.id,
    ...     'with_childs': True,
    ...     'product': product.id,
    ...     }
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].balance_qty
    320.0
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].valuation
    516.0

Check Cost Price is 1.6125::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.612500')


Purchase 100 products with tax in price on previous date::

    >>> Purchase = Model.get('purchase.purchase')
    >>> PurchaseLine = Model.get('purchase.line')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.payment_term = payment_term
    >>> purchase.department = department
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 100.0
    >>> purchase_line.unit_price = Decimal('3.0000')
    >>> purchase.click('quote')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('confirm')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.click('process')
    >>> purchase.untaxed_amount, purchase.tax_amount, purchase.total_amount
    (Decimal('300.00'), Decimal('36.00'), Decimal('336.00'))
    >>> purchase.state
    u'processing'
    >>> purchase.shipment_state
    u'waiting'
    >>> purchase.invoice_state
    u'waiting'
    >>> len(purchase.moves), len(purchase.shipment_returns), len(purchase.invoices)
    (1, 0, 1)
    >>> invoice, = purchase.invoices
    >>> invoice.origins == purchase.rec_name
    True

Validate Shipments on previous date::

    >>> Move = Model.get('stock.move')
    >>> ShipmentIn = Model.get('stock.shipment.in')
    >>> shipment = ShipmentIn()
    >>> shipment.supplier = supplier
    >>> for move in purchase.moves:
    ...     incoming_move = Move(id=move.id)
    ...     shipment.incoming_moves.append(incoming_move)
    >>> shipment.save()
    >>> shipment.origins == purchase.rec_name
    True
    >>> shipment.effective_date = yesterday
    >>> shipment.click('receive')
    >>> shipment.click('done')
    >>> purchase.reload()
    >>> purchase.shipment_state
    u'received'
    >>> len(purchase.shipments), len(purchase.shipment_returns)
    (1, 0)

Check Product Summary on previous date::

    >>> ProductSummary = Model.get('stock.product.summary')
    >>> context = {
    ...     'location': storage_loc.id,
    ...     'with_childs': True,
    ...     'product': product.id,
    ...     }
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].balance_qty
    420.0
    >>> with config.set_context(context):
    ...     ProductSummary.find([])[0].valuation
    829.68

Check Cost Price is 1.975429 on previous date::

    >>> product.reload()
    >>> product.cost_price.quantize(Decimal(str(10**-6)))
    Decimal('1.975429')

Create shipment on closed period::

    >>> StockPeriod = Model.get('stock.period')
    >>> omb = today - relativedelta(months=1)
    >>> stock_period, = StockPeriod.find([
    ...    ('start_date', '<=', omb),
    ...    ('date', '>=', omb)])
    >>> stock_period.click('close')
    >>> stock_period.save()

    >>> shipment3 = Shipment()
    >>> shipment3.planned_date = omb
    >>> shipment3.from_location = storage_loc
    >>> shipment3.to_location = lost_found_loc
    >>> shipment3.department = department
    >>> shipment3.employee = employee
    >>> shipment3.reason = shipment_reason
    >>> move = shipment3.moves.new()
    >>> move.product = product
    >>> move.quantity = 20
    >>> move.from_location = storage_loc
    >>> move.to_location = lost_found_loc
    >>> move.department = department
    >>> move.employee = employee
    >>> move.currency = company.currency
    >>> shipment3.save()
    >>> shipment3.assigned_by
    >>> shipment3.done_by

    >>> shipment3.click('wait') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    UserError: ...
