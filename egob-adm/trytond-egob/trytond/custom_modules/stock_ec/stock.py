import datetime
from decimal import Decimal
from collections import defaultdict

import logging
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import Workflow, ModelView, fields, ModelSQL, \
    sequence_ordered, Unique
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.tools import reduce_ids, grouped_slice
from trytond.rpc import RPC
from trytond.config import config as trytond_config

from trytond.modules.company import CompanyReport
from trytond.modules.product import price_digits

import requests
import json


__all__ = ['Location', 'Period', 'PeriodCache', 'ShipmentIn',
    'ShipmentInReturn', 'ShipmentInternal', 'Move', 'AssignShipmentInternal',
    'AssignShipmentInReturn', 'InteralShipmentReport', 'OutShipmentReport',
    'MoveListReport', 'ShipmentReason', 'ShipmentReasonCategory',
    'ShipmentTemplate', 'ShipmentTemplateLine',
    'CreateShipmentFromTemplateStart', 'CreateShipmentFromTemplate',
    'CreateShipmentFromReferenceStart', 'CreateShipmentFromReference',
    'UnsetShipmentFromReferenceStart', 'UnsetShipmentFromReference',
    'AssignToLocationStart', 'AssignToLocation']

logger = logging.getLogger(__name__)


class Location:
    __metaclass__ = PoolMeta
    __name__ = "stock.location"
    _history = True

    @classmethod
    def __setup__(cls):
        super(Location, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uniq', Unique(t, t.code),
                'code_uniq'),
            ('name_uniq', Unique(t, t.name),
                'name_uniq'),
            ]
        cls._error_messages.update({
                'code_uniq': 'Code must be unique.',
                'name_uniq': 'Name must be unique.',
                })

    def get_rec_name(self, name):
        code = ''
        if self.code:
            code = self.code + ' / '
        return code + self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
        ]


class Period:
    __metaclass__ = PoolMeta
    __name__ = 'stock.period'
    _history = True

    start_date = fields.Date('Start date', required=True,
        states={
            'readonly': Eval('state') == 'closed',
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super(Period, cls).__setup__()
        cls._error_messages.update({
            'account_period': ('There is no account period corresponding to '
            'this stock period'),
        })

    @classmethod
    def validate(cls, periods):
        super(Period, cls).validate(periods)
        for period in periods:
            period.check_account_period()

    def check_account_period(self):
        pool = Pool()
        AccountPeriod = pool.get('account.period')
        account_period = AccountPeriod.search([
            ('start_date', '=', self.start_date),
            ('end_date', '=', self.date)])
        if not account_period:
            self.__class__.raise_user_error('account_period')


class PeriodCache:
    __metaclass__ = PoolMeta
    __name__ = 'stock.period.cache'

    @classmethod
    def create(cls, vlist):
        pass


class ShipmentIn:
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.in'
    _history = True

    returned = fields.Boolean('Returned', readonly=True)
    total_amount = fields.Function(fields.Numeric('Total',
        digits=price_digits), 'on_change_with_total_amount')

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        cls.moves.context.update({
            'move_in': True,
            })
        cls._transitions |= set((('done', 'cancel'),))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state').in_(
                    ['cancel', 'shipped']),
            },
        })

    @staticmethod
    def default_returned():
        return False

    @fields.depends('incoming_moves')
    def on_change_with_total_amount(self, name=None):
        amount = 0
        for move in self.incoming_moves:
            if move.unit_price and move.quantity:
                amount += move.unit_price * Decimal(move.quantity)
        return amount

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, shipments):
        pool = Pool()
        Product = pool.get('product.product')
        Date = pool.get('ir.date')
        super(ShipmentIn, cls).done(shipments)
        products = []
        for shipment in shipments:
            for move in shipment.moves:
                if shipment.effective_date < Date.today():
                    products.append(move.product.id)
        products = list(set(products))
        Product.recompute_cost_price(Product.browse(products))

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, shipments):
        pool = Pool()
        Product = pool.get('product.product')
        super(ShipmentIn, cls).cancel(shipments)
        products = []
        for shipment in shipments:
            for move in shipment.moves:
                products.append(move.product.id)
        products = list(set(products))
        Product.recompute_cost_price(Product.browse(products))

    @classmethod
    def _get_inventory_moves(cls, incoming_move):
        move = super(ShipmentIn, cls)._get_inventory_moves(incoming_move)
        if move:
            if move.product.account_category.storage_location:
                move.to_location = \
                    move.product.account_category.storage_location
        return move


class ShipmentInReturn:
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.in.return'

    @classmethod
    def __setup__(cls):
        super(ShipmentInReturn, cls).__setup__()
        cls._error_messages.update({
            'create_from_wizard': ('Returns must be created from wizard.'),
        })

    @classmethod
    def create(cls, vlist):
        if Transaction().context.get('from_wizard'):
            return super(ShipmentInReturn, cls).create(vlist)
        else:
            cls.raise_user_error('create_from_wizard')


class ShipmentInternal:
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.internal'
    _history = True

    from_type = fields.Function(fields.Char('from_type'),
        'on_change_with_from_type')
    to_type = fields.Function(fields.Char('to_type'),
        'on_change_with_to_type')
    department = fields.Many2One('company.department', 'Department',
        states={
            'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
        }, required=True)
    employee = fields.Many2One('company.employee', 'Employee',
        states={
            'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
            'required': ((Eval('from_type') == 'storage') &
                (Eval('to_type') != 'storage')),
            },
        depends=['from_type', 'to_type'])
    siim_reference = fields.Char('Invoice Reference')
    reason = fields.Many2One('stock.shipment.reason', 'Reason',
        states={
            'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
        }, required=True)
    reason_categories = fields.Function(
        fields.Many2Many('product.category', None, None, 'Reason categories'),
        'on_change_with_reason_categories')
    observations = fields.Text('Observaciones')

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls.siim_reference.readonly = True
        cls.moves.domain.append(('department', '=', Eval('department', '')))
        cls.moves.depends.append('department')
        cls.moves.domain.append(
            ('product.account_category', 'in', Eval('reason_categories', []))
        )
        cls.moves.depends.append('reason_categories')
        cls.moves.context.update({
            'employee': Eval('employee', None),
            'locations': [Eval('from_location', -1)],
            'move_internal': True,
            })
        cls.moves.depends += ['employee', 'from_location']
        cls._transitions |= set((('done', 'cancel'),))
        cls._error_messages.update({
            'employee_domain': ('Employee must be part of department %s.'),
        })
        cls._buttons.update({
            'siim_reference_search': {
                'invisible': ~Eval('state').in_(['draft', 'request']),
            },
            'siim_reference_unset': {
                'invisible': ~Eval('state').in_(['draft', 'request']),
            },
            'cancel': {
                'invisible': Eval('state').in_(
                    ['cancel', 'shipped']),
            },
        })
        cls.__rpc__.update({
            'get_move_data': RPC(),
            })

    @fields.depends('from_location')
    def on_change_with_from_type(self, name=None):
        if self.from_location:
            return self.from_location.type
        return ''

    @fields.depends('to_location')
    def on_change_with_to_type(self, name=None):
        if self.to_location:
            return self.to_location.type
        return ''

    @fields.depends('reason')
    def on_change_with_reason_categories(self, name=None):
        pool = Pool()
        Category = pool.get('product.category')
        categories = []
        if self.reason and self.reason.categories:
            categories = [c.id for c in
                Category.search([
                    ('parent', 'child_of', self.reason.categories)]
                )]
        return categories

    @fields.depends('employee', 'department')
    def on_change_employee(self):
        pool = Pool()
        if self.employee and self.department:
            Config = pool.get('stock.configuration')
            config = Config(1)
            if config.employee_domain:
                if self.employee.department != self.department:
                    self.__class__.raise_user_error('employee_domain',
                        self.department.name)

    @classmethod
    @ModelView.button_action('stock_ec.act_shipment_reference_create')
    def siim_reference_search(cls, internals):
        pass

    @classmethod
    @ModelView.button_action('stock_ec.act_shipment_reference_unset')
    def siim_reference_unset(cls, internals):
        pass

    @classmethod
    def get_move_data(cls, reference_invoice, lines):
        pool = Pool()
        Company = pool.get('company.company')
        Item = pool.get('public.siim.item')
        Config = pool.get('stock.configuration')
        from_location = Config(1).get_multivalue('from_location_product_item')
        to_location = Config(1).get_multivalue('to_location_product_item')
        company = Company(Transaction().context.get('company'))
        result = []
        for line in lines:
            item, = Item.search([
                ('item_id', '=', (lines[line]['item']))
            ])
            result.append({
                'product': item.product.id,
                'uom': item.product.default_uom.id,
                'quantity': lines[line]['quantity'],
                'from_location': from_location.id,
                'to_location': to_location.id,
                'company': company.id,
                'currency': company.currency.id if company else None,
                'state': 'draft',
                'reference': reference_invoice,
            })
        return result


class CreateShipmentFromTemplateStart(ModelView):
    'Get Order Start'
    __name__ = 'stock.shipment.template.create.start'

    reason = fields.Many2One('stock.shipment.reason', 'Reason')
    template = fields.Many2One(
        'stock.shipment.template', 'Template', required=True,
        domain=[('reason', '=', Eval('reason'))], depends=['reason']
    )


class CreateShipmentFromTemplate(Wizard):
    'Create Shipment from Template'
    __name__ = 'stock.shipment.template.create'
    start = StateView('stock.shipment.template.create.start',
        'stock_ec.shipment_create_from_template_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def default_start(self, fields):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        shipment = Shipment(Transaction().context['active_id'])
        return {
            'reason': shipment.reason.id
        }

    def transition_accept(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')
        context = Transaction().context
        model = context.get('active_model')
        active_id = context.get('active_id')
        if model == 'stock.shipment.internal':
            shipment = Shipment(active_id)
            lines = []
            for line in self.start.template.lines:
                move = Move()
                move.shipment = shipment
                move.company = shipment.company
                move.from_location = shipment.from_location
                move.to_location = shipment.to_location
                move.department = shipment.department
                move.employee = shipment.employee
                move.product = line.product
                move.on_change_product()
                move.quantity = line.quantity
                lines.append(move)
            shipment.moves += tuple(lines)
            shipment.save()
        return 'end'


class CreateShipmentFromReferenceStart(ModelView):
    'Create Shipment from Reference Start'
    __name__ = 'stock.shipment.reference.create.start'

    reference = fields.Integer('Reference', required=True)


class CreateShipmentFromReference(Wizard):
    'Create Shipment from Reference'
    __name__ = 'stock.shipment.reference.create'
    start = StateView('stock.shipment.reference.create.start',
        'stock_ec.shipment_create_from_reference_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateShipmentFromReference, cls).__setup__()
        cls._error_messages.update({
            'siim_error': ('Error on SIIM communication'),
            'existing_shipment': (
                'There is already a shipment:"%(shipment)s" '
                'with reference:"%(reference)s"'),
        })

    def get_url_rest(self, reference):
        return 'itemFacturacion/%s' % reference

    def transition_accept(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')
        Item = pool.get('public.siim.item')
        context = Transaction().context
        model = context.get('active_model')
        active_id = context.get('active_id')
        url_item_callback = trytond_config.get('siim', 'url_callback')
        reference = str(self.start.reference)
        existing_shipment = Shipment.search([
            ('siim_reference', '=', reference),
            ('state', '=', 'done')
        ])
        if existing_shipment:
            self.raise_user_error('existing_shipment', {
                'shipment': existing_shipment[0].number,
                'reference': reference
            })
        url_item_callback += self.get_url_rest(reference)
        if model == 'stock.shipment.internal':
            try:
                rest_data = requests.get(url_item_callback)
                if rest_data.status_code != 200:
                    self.raise_user_error('siim_error')
                result = json.loads(rest_data.content)
            except (requests.HTTPError, requests.ConnectionError) as e:
                logger.error(e.message)
                self.raise_user_error('siim_error')
            shipment = Shipment(active_id)
            shipment_moves = []
            for line in result:
                item_id = line['idRubro']
                qty = line['cantidad']
                item = Item.search([('item_id', '=', item_id)])
                if item:
                    product = item[0].product
                    shipment_moves.append(Move(
                        from_location=shipment.from_location,
                        to_location=shipment.to_location,
                        department=shipment.department,
                        employee=shipment.employee,
                        product=product,
                        quantity=qty,
                        uom=product.default_uom,
                        company=shipment.company,
                    ))
            if shipment_moves:
                shipment.moves = shipment_moves
                shipment.siim_reference = reference
                shipment.save()
        return 'end'


class UnsetShipmentFromReferenceStart(ModelView):
    'Create Shipment from Reference Start'
    __name__ = 'stock.shipment.reference.unset.start'


class UnsetShipmentFromReference(Wizard):
    'Unset Shipment from Reference'
    __name__ = 'stock.shipment.reference.unset'
    start = StateView('stock.shipment.reference.unset.start',
        'stock_ec.shipment_unset_from_reference_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.internal')
        Move = pool.get('stock.move')
        context = Transaction().context
        model = context.get('active_model')
        active_id = context.get('active_id')
        if model == 'stock.shipment.internal':
            shipment = Shipment(active_id)
            if shipment.state == 'draft':
                reference = shipment.siim_reference
                shipment.siim_reference = None
            lines = Move.search([
                ('reference', '=', reference),
                ('state', '=', 'draft'),
                ('shipment', '=', 'stock.shipment.internal,%d' % shipment.id)
            ])
            lines_to_save = []
            for line in lines:
                line.shipment = None
                lines_to_save.append(line)
            Move.save(lines_to_save)
            shipment.save()

        return 'end'


class AssignToLocationStart(ModelView):
    'Assign to_location on stock.shipment.in start'
    __name__ = 'stock.shipment.in.assign.to_location.start'

    storage_location = fields.Many2One('stock.location', 'Storage Location',
        states={
            'invisible': True,
        })
    to_location = fields.Many2One('stock.location', 'Location', required=True,
        domain=[
            ('parent', 'child_of', Eval('storage_location'), 'parent')
        ], depends=['storage_location'])

    @classmethod
    def default_storage_location(self, name=None):
        pool = Pool()
        Location = pool.get('stock.location')
        storage, = Location.search([('code', '=', 'STO')])
        return storage.id


class AssignToLocation(Wizard):
    'Assign to_location on stock.shipment.in'
    __name__ = 'stock.shipment.in.assign.to_location'
    start = StateView('stock.shipment.in.assign.to_location.start',
        'stock_ec.assign_to_location_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Accept', 'accept', 'tryton-ok', default=True),
            ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Shipment = pool.get('stock.shipment.in')
        Move = pool.get('stock.move')
        context = Transaction().context
        model = context.get('active_model')
        active_id = context.get('active_id')
        if model == 'stock.shipment.in':
            shipment = Shipment(active_id)
            to_save = []
            for line in shipment.inventory_moves:
                line.to_location = self.start.to_location
                to_save.append(line)
            Move.save(to_save)
        return 'end'


class Move:
    __metaclass__ = PoolMeta
    __name__ = 'stock.move'
    _history = True

    readonly_unit_price = fields.Function(
        fields.Boolean('Readonly unit price'), 'get_readonly_unit_price')
    base_unit_price = fields.Numeric('Base Unit Price', digits=price_digits,
        states={
            'invisible': ~Eval('readonly_unit_price'),
            'readonly': Eval('state') != 'draft',
            },
        depends=['readonly_unit_price', 'state'])
    from_type = fields.Function(fields.Char('from_type'),
        'on_change_with_from_type')
    to_type = fields.Function(fields.Char('from_type'),
        'on_change_with_to_type')
    department = fields.Many2One('company.department', 'Department',
        states={
            'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
            'required': Eval('_parent_shipment', {}).get('department', False),
            })
    employee = fields.Many2One('company.employee', 'Employee',
        states={
            'readonly': Eval('state').in_(['cancel', 'assigned', 'done']),
            'required': ((Eval('from_type') == 'storage') &
                (Eval('to_type') == 'lost_found') & (Eval('state') != 'draft')),
            },
        depends=['from_type', 'to_type'])
    reference = fields.Char('Reference', states={'readonly': True})
    stock = fields.Function(fields.Numeric('Stock'), 'on_change_with_stock')

    @classmethod
    def __setup__(cls):
        super(Move, cls).__setup__()
        cls._deny_modify_done_cancel.remove('state')
        cls._deny_modify_done_cancel.remove('effective_date')
        cls.unit_price.states['readonly'] = Eval('readonly_unit_price')
        cls.unit_price.depends += ['readonly_unit_price']
        cls._transitions |= set((('done', 'cancel'),))
        cls._error_messages.update({
            'employee_domain': ('Employee must be part of department %s.'),
            'modify_on_close_period': ('Can not modify or create moves on '
                'close period'),
        })

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee', None)

    @classmethod
    def get_readonly_unit_price(cls, moves, name=None):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        config = Configuration(1)
        if config.unit_price_with_tax:
            result = defaultdict(lambda: True)
        else:
            result = defaultdict(lambda: False)
        return result

    @fields.depends('from_location')
    def on_change_with_from_type(self, name=None):
        if self.from_location:
            return self.from_location.type
        return ''

    @fields.depends('to_location')
    def on_change_with_to_type(self, name=None):
        if self.to_location:
            return self.to_location.type
        return ''

    @fields.depends('employee', 'department')
    def on_change_employee(self):
        pool = Pool()
        if self.employee and self.department:
            Config = pool.get('stock.configuration')
            config = Config(1)
            if config.employee_domain:
                if self.employee.department != self.department:
                    self.__class__.raise_user_error('employee_domain',
                        self.department.name)

    @classmethod
    def check_open_period(cls, moves):
        pool = Pool()
        StockPeriod = pool.get('stock.period')
        stock_period = StockPeriod.__table__()
        table = cls.__table__()
        cursor = Transaction().connection.cursor()
        for sub_move_ids in grouped_slice([m.id for m in moves]):
            red_sql = reduce_ids(table.id, sub_move_ids)
            query = table.join(stock_period,
                condition=(table.effective_date >= stock_period.start_date) &
                (table.effective_date <= stock_period.date)).select(
                table.id, where=red_sql & (stock_period.state == 'closed'))
            cursor.execute(*query)
            error_moves = cursor.fetchall()
            if error_moves:
                cls.raise_user_error('modify_on_close_period')

    @fields.depends('base_unit_price', 'origin')
    def on_change_base_unit_price(self):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        configuration = Configuration(1)
        if self.base_unit_price:
            if configuration.unit_price_with_tax:
                Tax = pool.get('account.tax')
                if self.origin:
                    taxes = Tax.compute(self.origin.taxes, self.base_unit_price,
                        self.quantity)
                    if taxes:
                        total = 0
                        for tax in taxes:
                            total += tax['amount'] + tax['base']
                        self.unit_price = Decimal(total /
                            Decimal(self.quantity)).quantize(
                            Decimal(10) ** -price_digits[1])

            else:
                self.unit_price = self.base_unit_price
        else:
            self.unit_price = 0

    @fields.depends('from_location', 'product', 'effective_date')
    def on_change_with_stock(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        stock_date_end = datetime.date.today()
        result = 0
        if self.product and self.from_location:
            products = [self.product]
            locations = [self.from_location.id]
            if self.effective_date:
                stock_date_end = self.effective_date
            with Transaction().set_context(locations=locations,
                    stock_date_end=stock_date_end):
                stock_data = Product.get_quantity(products, 'forecast_quantity')
                result = stock_data[self.product.id]
        return result

    @classmethod
    def validate(cls, moves):
        super(Move, cls).validate(moves)
        cls.check_open_period(moves)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, moves):
        super(Move, cls).do(moves)
        cls.check_open_period(moves)


class AssignShipmentInternal(Wizard):
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.internal.assign'

    @classmethod
    def __setup__(cls):
        super(AssignShipmentInternal, cls).__setup__()
        for button in cls.failed.buttons:
            if button.state == 'force':
                button.states.update({'invisible': True})


class AssignShipmentInReturn(Wizard):
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.in.return.assign'

    @classmethod
    def __setup__(cls):
        super(AssignShipmentInReturn, cls).__setup__()
        for button in cls.failed.buttons:
            if button.state == 'force':
                button.states.update({'invisible': True})


class InteralShipmentReport:
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.internal.report'

    @classmethod
    def get_context(cls, records, data):
        Config = Pool().get('stock.configuration')
        config = Config(1)
        report_context = super(InteralShipmentReport, cls).get_context(
            records, data)
        report_context['stock_owner'] = config.stock_user and \
            config.stock_user.name or ''
        return report_context


class OutShipmentReport:
    __metaclass__ = PoolMeta
    __name__ = 'stock.shipment.out.delivery_note'

    @classmethod
    def get_context(cls, records, data):
        Config = Pool().get('stock.configuration')
        config = Config(1)
        report_context = super(OutShipmentReport, cls).get_context(
            records, data)
        report_context['stock_owner'] = config.stock_user and \
            config.stock_user.name or ''
        return report_context


class MoveListReport(CompanyReport):
    __name__ = 'move.list.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MoveListReport, cls).get_context(
            records, data)
        report_context['price_digits'] = price_digits[1]
        return report_context


class ShipmentReason(sequence_ordered(), ModelSQL, ModelView):
    'Shipment Reason'
    __name__ = 'stock.shipment.reason'
    _history = True

    name = fields.Char('Name', required=True)
    categories = fields.Many2Many('stock.shipment.reason-product.category',
        'reason', 'category', 'Categories', required=True)


class ShipmentReasonCategory(ModelSQL):
    'Shipment Reason - Product Category'
    __name__ = 'stock.shipment.reason-product.category'
    reason = fields.Many2One('stock.shipment.reason', 'Reason',
        ondelete='CASCADE', select=True, required=True)
    category = fields.Many2One('product.category', 'Product category',
        ondelete='CASCADE', select=True, required=True)


class ShipmentTemplate(sequence_ordered(), ModelSQL, ModelView):
    'Shipment Template'
    __name__ = 'stock.shipment.template'
    _history = True

    name = fields.Char('Name', required=True)
    reason = fields.Many2One('stock.shipment.reason', 'Reason', required=True)
    reason_categories = fields.Function(
        fields.Many2Many('product.category', None, None, 'Reason categories'),
        'on_change_with_reason_categories')
    lines = fields.One2Many(
        'stock.shipment.template.line', 'template', 'Lines', required=True,
        domain=[
            ('product.account_category', 'in', Eval('reason_categories', []))
        ], depends=['reason_categories'])

    @classmethod
    def __setup__(cls):
        super(ShipmentTemplate, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name', Unique(t, t.name), 'name_uniq'),
        ]
        cls._error_messages.update({
            'name_uniq': 'Name must be unique.',
        })

    @fields.depends('reason')
    def on_change_with_reason_categories(self, name=None):
        pool = Pool()
        Category = pool.get('product.category')
        categories = []
        if self.reason and self.reason.categories:
            categories = [c.id for c in
                Category.search([
                    ('parent', 'child_of', self.reason.categories)]
                )]
        return categories


class ShipmentTemplateLine(sequence_ordered(), ModelSQL, ModelView):
    'Shipment Template Line'
    __name__ = 'stock.shipment.template.line'
    _history = True

    template = fields.Many2One('stock.shipment.template', 'Template',
        ondelete='CASCADE', select=True, required=True)
    product = fields.Many2One("product.product", "Product", required=True,
        select=True, domain=[('type', '=', 'goods')])
    uom = fields.Function(
        fields.Many2One('product.uom', 'Product Uom'), 'on_change_with_uom')
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
        'on_change_with_unit_digits')
    quantity = fields.Float("Quantity", required=True,
        digits=(16, Eval('unit_digits', 2)),
        depends=['unit_digits'])

    @fields.depends('product')
    def on_change_with_uom(self, name=None):
        if self.product:
            return self.product.default_uom.id

    @staticmethod
    def default_unit_digits():
        return 2

    @fields.depends('uom')
    def on_change_with_unit_digits(self, name=None):
        if self.uom:
            return self.uom.digits
        return 2
