from trytond.model import ModelSQL, ValueMixin, fields
from trytond.pool import Pool, PoolMeta

__all__ = ['StockConfiguration', 'StockConfigurationEmployeeDomain',
    'StockConfigurationStockUser',
    'StockConfigurationStockFromLocationProductItem',
    'StockConfigurationStockToLocationProductItem', 'PurchaseConfiguration',
    'PurchaseConfigurationTax', 'ProductConfiguration',
    'ProductConfigurationPriceList']


class StockConfiguration:
    __metaclass__ = PoolMeta
    __name__ = 'stock.configuration'

    employee_domain = fields.MultiValue(
        fields.Boolean('Employee domain per department?'))
    stock_user = fields.MultiValue(
        fields.Many2One('res.user', 'Inventory user', required=True))
    from_location_product_item = fields.MultiValue(
        fields.Many2One(
            'stock.location', 'From Location Product Item', required=True))
    to_location_product_item = fields.MultiValue(
        fields.Many2One(
            'stock.location', 'To Location Product Item', required=True))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'employee_domain':
            return pool.get('stock.configuration.employee_domain')
        elif field == 'stock_user':
            return pool.get('stock.configuration.stock_user')
        elif field == 'from_location_product_item':
            return pool.get('stock.configuration.from_location_product_item')
        elif field == 'to_location_product_item':
            return pool.get('stock.configuration.to_location_product_item')
        return super(StockConfiguration, cls).multivalue_model(field)

    @classmethod
    def default_employee_domain(cls, **pattern):
        return cls.multivalue_model(
            'employee_domain').default_employee_domain()


class StockConfigurationEmployeeDomain(ModelSQL, ValueMixin):
    "Stock Configuration Employee Domain"
    __name__ = 'stock.configuration.employee_domain'

    employee_domain = fields.Boolean('Employee domain per department?')

    @classmethod
    def default_employee_domain(cls):
        return False


class StockConfigurationStockUser(ModelSQL, ValueMixin):
    "Stock Configuration Employee Domain"
    __name__ = 'stock.configuration.stock_user'

    stock_user = fields.Many2One('res.user', 'Inventory user', required=True)


class StockConfigurationStockFromLocationProductItem(ModelSQL, ValueMixin):
    "Stock Configuration From Location Product Item"
    __name__ = 'stock.configuration.from_location_product_item'

    from_location_product_item = fields.Many2One('stock.location',
        'From Location Product Item', required=True)


class StockConfigurationStockToLocationProductItem(ModelSQL, ValueMixin):
    "Stock Configuration To Location Product Item"
    __name__ = 'stock.configuration.to_location_product_item'

    to_location_product_item = fields.Many2One('stock.location',
        'To Location Product Item', required=True)


class PurchaseConfiguration:
    __metaclass__ = PoolMeta
    __name__ = 'purchase.configuration'

    unit_price_with_tax = fields.MultiValue(
        fields.Boolean('Unit price with Tax'))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'unit_price_with_tax':
            return pool.get('purchase.configuration.tax')
        return super(PurchaseConfiguration, cls).multivalue_model(field)

    @classmethod
    def default_unit_price_with_tax(cls, **pattern):
        return cls.multivalue_model(
            'unit_price_with_tax').default_unit_price_with_tax()


class PurchaseConfigurationTax(ModelSQL, ValueMixin):
    "Purchases Configuration Tax"
    __name__ = 'purchase.configuration.tax'

    unit_price_with_tax = fields.Boolean('Unit price with Tax')

    @classmethod
    def default_unit_price_with_tax(cls):
        return False


class ProductConfiguration:
    __metaclass__ = PoolMeta
    __name__ = 'product.configuration'

    price_list = fields.MultiValue(fields.Many2One(
        'product.price_list', 'Price list'))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'price_list':
            return pool.get('product.configuration.price_list')
        return super(ProductConfiguration, cls).multivalue_model(field)


class ProductConfigurationPriceList(ModelSQL, ValueMixin):
    "Product Configuration Price List"
    __name__ = 'product.configuration.price_list'

    price_list = fields.Many2One('product.price_list', 'Price list')
