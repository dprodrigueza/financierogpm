from trytond.pool import PoolMeta

__all__ = ['Party']


class Party:
    __metaclass__ = PoolMeta
    __name__ = 'party.party'

    def get_rec_name(self, name):
        tax_code = ''
        for identifier in self.identifiers:
            if identifier.type in ['ec_ci', 'ec_ruc']:
                tax_code = identifier.code + ' / '
                break
        return tax_code + self.name
