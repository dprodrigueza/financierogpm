from trytond.pool import Pool, PoolMeta


__all__ = ['Period']


class Period:
    __metaclass__ = PoolMeta
    __name__ = 'account.period'

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        StockPeriod = pool.get('stock.period')
        FiscalYear = pool.get('account.fiscalyear')
        result = super(Period, cls).create(vlist)
        to_create = []
        for period in vlist:
            fiscalyear = FiscalYear(period['fiscalyear'])
            to_create.append({
                'start_date': period['start_date'],
                'date': period['end_date'],
                'company': fiscalyear.company,
            })
        StockPeriod.create(to_create)
        return result
