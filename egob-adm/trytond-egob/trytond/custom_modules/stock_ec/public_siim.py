import datetime
from decimal import Decimal
from collections import defaultdict

from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.pool import Pool, PoolMeta


__all__ = ['PublicSIIMItem']


class PublicSIIMItem:
    __metaclass__ = PoolMeta
    __name__ = "public.siim.item"

    stock_control = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No'),
        ], 'Stock control', sort=False, required=True)
    product = fields.Many2One('product.product', 'Product',
        domain=[
            ('salable', '=', True)
        ],
        states={
            'required': Eval('stock_control') == '1',  # Stock control
            'invisible': ~(Eval('stock_control') == '1'),
        }, depends=['stock_control'])
    quantity = fields.Function(fields.Numeric('Quantity',
        states={
            'invisible': ~Bool(Eval('product')),
        }, depends=['product']), 'get_quantity')
    price = fields.Function(fields.Numeric('Price',
        states={
            'invisible': ~Bool(Eval('product')),
        }, depends=['product']), 'get_price')

    @classmethod
    def __setup__(cls):
        super(PublicSIIMItem, cls).__setup__()
        cls._error_messages.update({
            'product_used': (
                'Product %(product)s is already used on item "%(name)s"'),
        })

    @staticmethod
    def default_stock_control():
        return '0'

    @classmethod
    def get_url_rest(cls):
        url = super(PublicSIIMItem, cls).get_url_rest()
        url += '/%s'
        return url

    @classmethod
    def get_url_rest_params(cls, values):
        url_params = super(PublicSIIMItem, cls).get_url_rest_params(values)
        url_params.append(values['stock_control'])
        return url_params

    @classmethod
    def to_int_fields(cls):
        fields = super(PublicSIIMItem, cls).to_int_fields()
        fields.append('stock_control')
        return fields

    @classmethod
    def validate(cls, items):
        super(PublicSIIMItem, cls).validate(items)
        for item in items:
            if item.product:
                used = cls.search([
                    ('id', '!=', item.id),
                    ('product', '=', item.product.id),
                ])
                if used:
                    cls.raise_user_error('product_used', {
                        'name': item.description,
                        'product': item.product.rec_name
                    })

    @fields.depends('stock_control')
    def on_change_stock_control(self):
        if self.stock_control == '0':
            self.product = None

    @classmethod
    def get_quantity(cls, items, name):
        pool = Pool()
        products = []
        result = defaultdict(lambda: Decimal(0))
        product_items = {}
        Location = pool.get('stock.location')
        Product = pool.get('product.product')
        locations = Location.search([
            ('type', '=', 'storage'),
        ])
        stock_date_end = datetime.date.today()
        location_ids = [l.id for l in locations]
        for item in items:
            if item.product:
                products.append(item.product)
                product_items[item.product.id] = item.id
        if products:
            with Transaction().set_context(locations=location_ids,
                    stock_date_end=stock_date_end):
                stock_data = Product.get_quantity(products, 'forecast_quantity')
                for line in stock_data:
                    result[product_items[line]] = Decimal(stock_data[line])
        return result

    @classmethod
    def get_price(cls, items, name):
        pool = Pool()
        products = []
        result = defaultdict(lambda: Decimal(0))
        product_items = {}
        Product = pool.get('product.product')
        for item in items:
            if item.product:
                products.append(item.product)
                product_items[item.product.id] = item.id
        if products:
            price_data = Product.get_sale_price(products)
            for line in price_data:
                result[product_items[line]] = Decimal(price_data[line])
        return result
