import datetime
from decimal import Decimal

from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import fields
from trytond.pyson import Eval

from trytond.modules.product import price_digits

__all__ = ['Purchase', 'PurchaseLine']

_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_DEPENDS = ['state']


class Purchase:
    __metaclass__ = PoolMeta
    __name__ = 'purchase.purchase'
    _history = True

    department = fields.Many2One('company.department', 'Department',
        states=_STATES, depends=_DEPENDS, required=True)

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._transitions |= set((('confirmed', 'quotation'),))
        cls._buttons['quote'].update({
            'invisible': ~Eval('state').in_(['draft', 'confirmed']),
        })


class PurchaseLine:
    __metaclass__ = PoolMeta
    __name__ = 'purchase.line'
    _history = True

    stock = fields.Function(fields.Numeric('Stock'), 'on_change_with_stock')

    def get_move(self, move_type):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        configuration = Configuration(1)
        move = super(PurchaseLine, self).get_move(move_type)
        if move:
            if self.purchase.department:
                move.department = self.purchase.department
            if configuration.unit_price_with_tax:
                if self.taxes:
                    Tax = pool.get('account.tax')
                    taxes = Tax.compute(self.taxes, move.unit_price,
                        self.quantity)
                    if taxes:
                        total = 0
                        for tax in taxes:
                            total += tax['amount'] + tax['base']
                        move.unit_price = Decimal(total /
                            Decimal(self.quantity)).quantize(
                                Decimal(10) ** -price_digits[1])
                move.base_unit_price = self.unit_price
        return move

    @fields.depends('product', '_parent_purchase.warehouse',
        '_parent_purchase.purchase_date')
    def on_change_with_stock(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        stock_date_end = datetime.date.today()
        result = 0
        if self.product:
            products = [self.product]
            if self.purchase.warehouse:
                locations = [self.purchase.warehouse.storage_location.id]
            else:
                locations = []
            if self.purchase.purchase_date:
                stock_date_end = self.purchase.purchase_date
            with Transaction().set_context(locations=locations,
                    stock_date_end=stock_date_end):
                stock_data = Product.get_quantity(products, 'forecast_quantity')
                result = stock_data[self.product.id]
        return result
