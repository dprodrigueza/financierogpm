from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.protocols.jsonrpc import JSONDecoder

import json

__all__ = ['FilterParameter', 'ReportExecution']


class FilterParameter:
    __metaclass__ = PoolMeta
    __name__ = 'babi.filter.parameter'

    code = fields.Char('Code', required=True)


class ReportExecution:
    __metaclass__ = PoolMeta
    __name__ = 'babi.report.execution'

    def replace_parameters(self, expression):
        if self.report.filter and self.report.filter.parameters:
            if not self.filter_values:
                self.raise_user_error('filter_parameters', self.rec_name)
            filter_data = json.loads(self.filter_values.encode('utf-8'),
                object_hook=JSONDecoder())
            parameters = dict((p.id, p.code) for p in
                self.report.filter.parameters)
            values = {}
            for key, value in filter_data.iteritems():
                filter_name = parameters[int(key.split('_')[-1:][0])]
                if not value or filter_name not in expression:
                    continue
                values[filter_name] = value
            expression = expression.format(**values)
        return expression
