from sql import Literal

from trytond.model import fields
from trytond.model import (Workflow, ModelView, ModelSQL, fields)
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If


__all__ = ['ShipmentIn', 'ShipmentInPendingProduct', 'ShipmentInternal']


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'
    pending_products = fields.One2Many('stock.shipment.in.pending.product',
        'shipment_in', 'Productos pendientes')

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        cls.state_translated = cls.state.translated('state')
        cls._error_messages.update({
                'pending_product': ('Recuerde que todas las líneas dentro de '
                    '"Productos pendientes" deben estar relacionadas a un '
                    '"Movimiento de entrada" que determinará el nuevo producto '
                    'creado o asignado.')
                })
    
    @classmethod
    @ModelView.button
    @Workflow.transition('received')
    def receive(cls, shipments):
        for shipment in shipments:
            for product in shipment.pending_products:
                if not product.incoming_move:
                    cls.raise_user_error('pending_product', {})
                else:
                    product.origin.spare = product.incoming_move.product
                    product.incoming_move.budget = product.budget
                    product.origin.save()
        super(ShipmentIn, cls).receive(shipments)


class ShipmentInPendingProduct(ModelView, ModelSQL):
    'Shipment In Pending Product'
    __name__ = 'stock.shipment.in.pending.product'
    description = fields.Char('Descripción', states={
        'required': True,
        'readonly': True
    })
    quantity = fields.Numeric('Cantidad', states={
        'required': True,
        'readonly': True
    })
    unit_price = fields.Numeric('Precio unitario', states={
        'required': True,
        'readonly': True
    })
    shipment_in = fields.Many2One('stock.shipment.in', 'Shipment In')
    incoming_move = fields.Many2One('stock.move', 'Moviniento de entrada',
        domain=[
            ('id', 'in', Eval('_parent_shipment_in', {}).get('incoming_moves'))
        ])
    budget = fields.Many2One('public.budget', 'Partida', states={
            'readonly': True
        })
    origin = fields.Reference('Origen', selection='get_origin', select=True, 
        states={
            'readonly': True
        })
    
    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['parking.vehicle.maintenance.line.external']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        models = cls._get_origin()
        models = Model.search([
                ('model', 'in', models),
                ])
        return [(None, '')] + [(m.model, m.name) for m in models]


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
    reason_name = fields.Function(fields.Char('reason',
            states={
                'invisible': True,
            }),
        'on_change_with_reason_name')
    party_barter = fields.Many2One('party.party', 'Cliente',
        states={
            'invisible': (Eval('reason_name') != 'TRUEQUE AMBIENTAL')
        })

    @fields.depends('reason')
    def on_change_with_reason_name(self, name=None):
        if self.reason:
            return self.reason.name
        return None
