from trytond.model import  fields
from trytond.pool import PoolMeta, Pool
from trytond import backend
from trytond.tools.multivalue import migrate_property
from trytond.pyson import Eval

from_location_ = fields.Many2One(
    'stock.location', "Desde la bodega de mantenimiento",
    states={
       # 'required': True
    },
    domain=[
        ('type', '=','storage'),
    ])

maintenance_tax = fields.Many2One(
    'account.tax', 'Impuesto para mantenimientos',
    states={
        'required': True
    })

class Configuration(metaclass=PoolMeta):
    __name__ = 'parking.place.configuration'

    from_location_ = fields.MultiValue(from_location_)
    # from_location_tires = fields.MultiValue(from_location_tires)
    maintenance_tax = fields.MultiValue(maintenance_tax)

    code_emac_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia Codigo Interno Emac', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'code_emac'),
        ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['code_emac_sequence',]:
            return pool.get('parking.place.configuration.sequence')
        if field in ['from_location_','maintenance_tax']:
            return pool.get('parking.place.configuration.location')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_code_emac_sequence(cls, **pattern):
        return cls.multivalue_model('code_emac_sequence'
                                    ).default_code_emac_sequence()

class ConfigurationSequence(metaclass=PoolMeta):
    'Parking Configuration Sequence'
    __name__ = 'parking.place.configuration.sequence'

    code_emac_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Codigo Interno Emac',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'code_emac'),
        ])

    @classmethod
    def default_code_emac_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_public_pac',
                                    'code_emac_sequence')
        except KeyError:
            return None

class ConfigurationLocation(metaclass=PoolMeta):
    __name__ = 'parking.place.configuration.location'

    from_location_ = fields.Many2One('stock.location', "Desde la Bodega JC",
        domain=[
            ('type', 'in',
             ['view', 'storage', 'lost_found']),
        ])

    maintenance_tax = fields.Many2One('account.tax',
        'Impuesto para mantenimientos')


    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):

        field_names.append('from_location_')
        value_names.append('from_location_')
        migrate_property(
            'parking.place.configuration', field_names, cls, value_names,
            fields=fields)
        return super(cls, field_names, value_names, fields)