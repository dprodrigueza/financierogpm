from decimal import Decimal
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import ModelView, fields, ModelSQL
from trytond.wizard import Wizard, StateView, StateTransition, StateReport, Button
from trytond.modules.hr_ec.company import CompanyReportSignature
from datetime import date
import numpy as np


__all__ = ['FuelOrderPaymentConsolidated']

class FuelOrderPaymentConsolidated(CompanyReportSignature):
    'Fuel Order Payment Consolidated'
    __name__ = 'parking.fuel.order.payment.consolidated.report'

    @classmethod
    def get_context(cls, records, data):
        context = super(FuelOrderPaymentConsolidated, cls).get_context(records, data)

        book = []
        for x in records:
            budgets = {}
            for y in x.total_lines:
                if not y.budget.rec_name in budgets.keys():
                    budgets[str(y.budget.rec_name)] = Decimal(0.0)
                budgets[str(y.budget.rec_name)] += y.total
            aux = []
            aux.append(x.number)
            aux.append(budgets)
            book.append(aux)
        context['book'] = book
        context['subtotal'] = sum(book[0][1].values())

        book2 = []
        for x in records:
            budgets2 = {}
            for y in x.total_lines:
                key = (str(y.budget.rec_name) + '/' + str(y.budget.activity.rec_name))
                if not key in budgets2.keys():
                    budgets2[key] = Decimal(0.0)
                budgets2[key] += y.total
            aux = []
            aux.append(x.number)
            aux.append(budgets2)
            book2.append(aux)
        context['book1'] = book2

        return context


class WorkshopReport(CompanyReportSignature):
    __name__ = 'parking.workshop.summary.report'



class WorkshopPurchaseReport(CompanyReportSignature):
    __name__ = 'parking.workshop.purchase.report'
