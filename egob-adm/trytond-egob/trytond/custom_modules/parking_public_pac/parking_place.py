import pandas as pd
from io import BytesIO
from decimal import Decimal, ROUND_HALF_UP
import functools
from datetime import datetime, date, timedelta
from trytond.model import (Workflow, ModelView, ModelSQL, fields)
from trytond.pool import PoolMeta
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Or,If
from trytond.wizard import (Wizard, Button, StateTransition, StateView)
from trytond.transaction import Transaction
from trytond.tools import reduce_ids, grouped_slice
from trytond.exceptions import UserError

from sql import Window, Literal, Union, Cast, Null
from sql.functions import RowNumber, CurrentTimestamp, ToNumber, ToChar
from sql.aggregate import Sum
from sql.operators import Concat
from sql.conditionals import Case, Coalesce

__all__ = ['Vehicle', 'Workshop', 'WorkshopContract', 'Maintenance',
    'MaintenancePendingLine']


def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        _dict.update({field_name: row[i]})
        i += 1
    return _dict


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class Vehicle(metaclass=PoolMeta):
    __name__ = 'parking.vehicle.vehicle'

    number_vehicle = fields.Numeric('Vehículo N:')
    usage_range_alert = fields.Numeric('Rango de alerta (uso)', 
        states={'required': True})
    mileage_range_alert = fields.Numeric('Rango de alerta (recorrido)', 
        states={'required': True})
    pending_lines = fields.One2Many(
        'parking.vehicle.maintenance.pending.line', 'vehicle',
        'Repuestos pendientes')


    def get_rec_name(self, name):
        if self.type == 'institutional':
            return ' # %s [%s] (%s / %s)' % (
                str(self.number_vehicle),
                self.plaque,
                self.mark.name, self.model.name,
                )
        elif self.type == 'machinery':
            if self.vehicle_name:
                return '%s - Maquinaria y equipo' % (self.vehicle_name)
            else:
                return self.id
        else:
            return 'VEHICULO CONTRATADO [%s]' % (self.plaque)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('vehicle_name',) + tuple(clause[1:]),
            ('plaque',) + tuple(clause[1:]),
            ('model',) + tuple(clause[1:]),
            ('mark',) + tuple(clause[1:]),
            ]
        return domain


    @staticmethod
    def default_mileage_range_alert():
        return Decimal(100.0)

    @staticmethod
    def default_usage_range_alert():
        return Decimal(100.0)


class Workshop(metaclass=PoolMeta):
    __name__ = 'parking.workshop'

    contracts = fields.Many2Many(
        'parking.workshop-purchase.contract.process',
        'workshop', 'contract', 'Contratos',
        states={
            'invisible': ~(Eval('type_').in_(['external']))
        },domain=[
            ('party', '=', Eval('party'))
        ],depends=['type_', 'party'])

    infinite_amounts = fields.Many2Many(
        'parking.workshop-infinite.amounts.process',
        'workshop', 'infinite_amount', 'Infimas Cuantias',
        states={
            'invisible': ~(Eval('type_').in_(['external']))
        },
         domain=[
             ('party', '=', Eval('party')),
             ('state', '=', 'done') ],
        depends=['type_', 'party'])

    @classmethod
    def view_attributes(cls):
        return super(Workshop, cls).view_attributes() + [
			('//group[@id="group_contracts"]', 'states',{
				'invisible': ~(Eval('type_').in_(
					['external'])),}),
		]


class WorkshopContract(ModelView, ModelSQL):
    "Workshop Contract"
    __name__ = 'parking.workshop-purchase.contract.process'

    workshop = fields.Many2One('parking.workshop', 'Taller',
        ondelete='CASCADE', select=True)
    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        ondelete='CASCADE', select=True)


class WorkshopInfiniteAmounts(ModelView, ModelSQL):
    "Workshop Infinite Amounts"
    __name__ = 'parking.workshop-infinite.amounts.process'

    workshop = fields.Many2One('parking.workshop', 'Taller',
                               ondelete='CASCADE', select=True)
    infinite_amount = fields.Many2One('purchase.purchase', 'Inifina Cuantia',
                               ondelete='CASCADE', select=True)


class FuelStation(ModelView, ModelSQL):
    "Fuel Station"
    __name__ = 'parking.fuel_station'

    name = fields.Char('Nombre', states={
        'required': True
        })
    contracts = fields.Many2Many(
        'parking.fuel_station-purchase.contract.process',
        'fuel_station', 'contract', 'Contratos')


class FuelStationContract(ModelView, ModelSQL):
    "Fuel Station Contract"
    __name__ = 'parking.fuel_station-purchase.contract.process'

    fuel_station = fields.Many2One('parking.fuel_station', 'Gasolinera',
        ondelete='CASCADE', select=True)
    contract = fields.Many2One('purchase.contract.process', 'Contato',
        ondelete='CASCADE', select=True)


class MaintenanceBudgetSummary(ModelSQL, ModelView):
    "Maintenance Budget Summary"
    __name__ = 'parking.vehicle.maintenance.budget.summary'

    maintenance = fields.Many2One('parking.vehicle.maintenance', 
        'Mantenimiento', readonly=True)
    budget = fields.Many2One('public.budget', 'Partida',
        readonly=True)
    actual_value = fields.Numeric('Pendiente en este mantenimiento (PA)', digits=(16, 2), 
        readonly=True)
    total_used = fields.Numeric('Total pagados (TP)', digits=(16, 2), 
        readonly=True)
    total_pendant = fields.Numeric('Total pendiente (P)', digits=(16, 2), 
        readonly=True, help="Mantenimientos en estado: Borrador, Enviados"
        ", Ingresando, y Finalizados que aun no hayan sido pagados")
    available_amount = fields.Numeric('Monto de partida (M)', digits=(16, 2), 
        readonly=True)
    balance = fields.Numeric('Saldo (M - TP)', digits=(16, 2), 
        readonly=True)
    balance_plannified = fields.Numeric('Saldo planificado (M - (TP + P + PA))', digits=(16, 2), 
        readonly=True)
    exceeded_color = fields.Function(fields.Char('color',
        readonly=True, depends=['balance']), 'on_change_with_exceeded_color')

    @fields.depends('balance')
    def on_change_with_exceeded_color(self, name=None):
        if self.balance > 0:
            return 'lightcoral'
        else:
            return 'lightgreen'

    @classmethod
    def table_query(cls):
        context = Transaction().context
        contract_id = context.get('contract')
        maintenance_id = context.get('maintenance')
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Contract = pool.get('purchase.contract.process')
        contract = Contract.__table__()
        TDRBudget = pool.get('purchase.contract.process.tdr.budget')
        tdr_budget = TDRBudget.__table__()
        Budget = pool.get('public.budget')
        budget = Budget.__table__()
        Certificate = pool.get('public.budget.certificate')
        certificate = Certificate.__table__()
        CertificateLine = pool.get('public.budget.certificate.line')
        certificate_line = CertificateLine.__table__()
        CertCard = pool.get('public.budget.certificate.line.card')
        cert_query = CertCard.__table__()
        Payment = pool.get('purchase.contract.process.payment')
        payment = Payment.__table__()
        PaymentLine = pool.get('purchase.contract.process.payment.line')
        payment_line = PaymentLine.__table__()
        where_actual = (contract.id == contract_id) & (
            maintenance.id == maintenance_id)
        # GET USED BUDGETS
        query_budgets = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            where=where_actual)

        where_no_draft = (maintenance.state != 'draft') & (
            maintenance.state != 'deny') & (
            maintenance.state != 'cancel') & (
            contract.id == contract_id) & (
            budget.id.in_(query_budgets)) & (
            maintenance.id != maintenance_id)

        # GET ACTUAL MAINTENANCES BUDGET VALUES
        query_actual = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_liquids + 
             maintenance.total_activities_estimated + 
             maintenance.tax_amount).as_(
                'actual_value'),
            Literal(0).as_('total_used'),
            Literal(0).as_('total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_actual 
            & ( maintenance.payments_generated == False) # NO DELETE PENDANT
        )

        # GET ACTUAL MAINTENANCES BUDGET VALUES IF IT'S PAID
        query_actual_paid = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            Literal(0).as_('actual_value'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_liquids + 
             maintenance.tax_amount).as_(
                'total_used'),
            Literal(0).as_('total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_actual 
            & ( maintenance.payments_generated == True) # NO DELETE PENDANT
        )

        # GET CONTRACT BUDGET VALUES ALREADY USED
        where_paid = (
            maintenance.payments_generated == True) & (
            budget.id.in_(query_budgets)) & (
            contract.id == contract_id) & (
            maintenance.id != maintenance_id)


        query_used = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            Literal(0).as_('actual_value'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_liquids + 
             maintenance.tax_amount).as_(
                'total_used'),
            Literal(0).as_('total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_paid
        )







        # GET CONTRACT BUDGET VALUES ALREADY USED
        where_pendant= (maintenance.state != 'deny') & (
            maintenance.state != 'cancel') & (
            budget.id.in_(query_budgets)) & (
            contract.id == contract_id) & (
            maintenance.payments_generated == False)

        query_pendant = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            Literal(0).as_('actual_value'),
            Literal(0).as_('total_used'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_liquids + 
             maintenance.total_activities_estimated +
             maintenance.tax_amount).as_(
                'total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_pendant
        )

        # GET TDR_BUDGET ASSIGNED VALUES
        where_contract = (contract.id == contract_id) & (
            certificate_line.budget.in_(query_budgets))

        line_ids = contract.join(certificate, type_='LEFT',
            condition=contract.tdr_certificate == certificate.id
        ).join(certificate_line, type_='LEFT',
            condition=certificate.id == certificate_line.certificate
        ).select(
            certificate_line.id.as_('id'),
            where=where_contract
        )
        queryCert = cert_query.select(
            cert_query.budget.as_('budget'),
            Literal(0).as_('actual_value'),
            Literal(0).as_('total_used'),
            Literal(0).as_('total_pendant'),
            cert_query.modified_amount.as_('available_amount'),
            where=cert_query.id.in_(line_ids))

        # GET TDR_BUDGET ASSIGNED VALUES
        query_tdr = contract.join(certificate, type_='LEFT',
            condition=contract.tdr_certificate == certificate.id
        ).join(certificate_line, type_='LEFT',
            condition=certificate.id == certificate_line.certificate
        ).select(
            certificate_line.budget.as_('budget'),
            Literal(0).as_('actual_value'),
            Literal(0).as_('total_used'),
            Literal(0).as_('total_pendant'),
            certificate_line.amount.as_('available_amount'),
            where=where_contract
        )

        query_final = Union(query_used, query_actual, all_=True)
        query_final = Union(query_final, query_tdr, all_=True)
        query_final = query_final.select(
            query_final.budget,
            Sum(query_final.actual_value).as_('actual_value'),
            Sum(query_final.total_used).as_('total_used'),
            Sum(query_final.total_pendant).as_('total_pendant'),
            Sum(query_final.available_amount).as_('available_amount'),
            group_by=[query_final.budget]
        )

        # GET ContractProcessPaymentLine USED VALUES
        # PENDANT BEFORE ASK IF SOME PAYMENTS ARE GENERATED DIRECT ON
        # CONTRACT
        # where_contract = (contract.id == contract_id) & (
        #     payment_line.budget.in_(query_budgets))

        # query_payment = contract.join(payment,
        #     condition=payment.process == contract.id
        # ).join(payment_line,
        #     condition=payment_line.payment == payment.id
        # ).select(
        #     payment_line.budget.as_('budget'),
        #     Literal(0).as_('actual_value'),
        #     (payment_line.amount * payment_line.unit_price).as_('total_used'),
        #     Literal(0).as_('available_amount'),
        #     where=where_contract
        # )
        query_final = Union(query_used, query_actual, all_=True)
        query_final = Union(query_final, query_pendant, all_=True)
        query_final = Union(query_final, query_tdr, all_=True)
        # query_final = Union(query_final, query_payment, all_=True)
        query_final = Union(query_final, query_actual_paid, all_=True)
        query_final = Union(query_final, queryCert, all_=True)
        query_final = query_final.select(
            query_final.budget,
            Sum(query_final.actual_value).as_('actual_value'),
            Sum(query_final.total_used).as_('total_used'),
            Sum(query_final.total_pendant).as_('total_pendant'),
            Sum(query_final.available_amount).as_('available_amount'),
            group_by=[query_final.budget]
        )

        # maintenance_number = ToNumber(str(maintenance_id), '99999')
        maintenance_number = Cast(maintenance_id, 'int')

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            maintenance_number.as_('maintenance'),
            query_final.budget,
            query_final.actual_value,
            query_final.total_used,
            query_final.total_pendant,
            query_final.available_amount,
            (query_final.available_amount - (query_final.actual_value + \
                query_final.total_used)).as_('balance'),
            (query_final.available_amount - (query_final.actual_value + \
                query_final.total_used + query_final.total_pendant)
                ).as_('balance_plannified')
        )


class MaintenanceInfiniteAmountSumary(ModelSQL, ModelView):
    "Maintenance Infinite Amount Summary"
    __name__ = 'parking.vehicle.maintenance.infinite.amount.summary'
    maintenance = fields.Many2One('parking.vehicle.maintenance',
                                  'Mantenimiento', readonly=True)
    infinite_amount = fields.Many2One('purchase.purchase', 'Infima Cuantía',
                             readonly=True)
    actual_value = fields.Numeric('En este mantenimiento', digits=(16, 2),
        readonly=True)
    total_used = fields.Numeric('Total usado', digits=(16, 2),
        readonly=True)
    available_amount = fields.Numeric('Monto de partida', digits=(16, 2),
        readonly=True)
    balance = fields.Numeric('Saldo', digits=(16, 2),
        readonly=True)

    @classmethod
    def table_query(cls):
        context = Transaction().context
        infinite_amount_id = context.get('infinite_amounts')
        maintenance_id = context.get('maintenance')
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        InfiniteAmounts = pool.get('purchase.purchase')
        infiniteAmounts = InfiniteAmounts.__table__()

        where_actual = (infiniteAmounts.id == infinite_amount_id) & (
            maintenance.id == maintenance_id)

        where_no_draft = (maintenance.state != 'draft') & (
            maintenance.state != 'cancel') & (
            infiniteAmounts.id == infinite_amount_id) & (
            maintenance.id != maintenance_id)

        query_actual = maintenance.join(infiniteAmounts,
            condition=maintenance.infinite_amounts == infiniteAmounts.id
            ).select(
                infiniteAmounts.id,
            (maintenance.total_activities+
                maintenance.total_external_spares +
                maintenance.total_liquids).as_('actual_value'),
                Literal(0).as_('total_used'),
                Literal(0).as_('available_amount'),
                where = where_actual
            )

        query_used = maintenance.join(infiniteAmounts,
        condition=maintenance.infinite_amounts == infiniteAmounts.id
         ).select(
            infiniteAmounts.id,
            Literal(0).as_('actual_value'),
            (maintenance.total_activities +
             maintenance.total_external_spares +
             maintenance.total_liquids).as_('total_used'),
            Literal(0).as_('available_amount'),
            where=where_no_draft

        )
        query_final = Union(query_used, query_actual, all_=True)
        query_final = query_final.select(
            Sum(query_final.actual_value).as_('actual_value'),
            Sum(query_final.total_used).as_('total_used'),
            Sum(query_final.available_amount).as_('available_amount'),
            group_by=[query_final.id]
        )
        maintenance_number = Cast(maintenance_id, 'int')

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            maintenance_number.as_('maintenance'),

            query_final.actual_value,
            query_final.total_used,
            query_final.available_amount,
            (query_final.available_amount - (query_final.actual_value + \
                                             query_final.total_used)).as_(
                'balance')
        )


class Maintenance(metaclass=PoolMeta):
    __name__ = 'parking.vehicle.maintenance'

    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        states={
            'readonly': (Eval('state') != 'draft'),
            'invisible': ~(Eval('workshop_type') == 'external') |
                         Bool(Eval('is_not_billable')),
            'required': ~Bool(Eval('is_not_billable')) & (Eval(
                 'workshop_type') == 'external'),
        },
        domain=[
            ('state', '=', 'open'),
            ('id', 'in', Eval('selectable_contracts'))
        ], depends=['workshop_type','selectable_contracts', 'state'])

    infinite_amounts = fields.Many2One('purchase.purchase', 'Infima Cuantía',
        states={
           'readonly': (Eval('state') != 'draft'),

           'invisible': ~(Eval('workshop_type') == 'external'),
           # 'required': ~Bool(Eval('is_not_billable')) &
           #             (Eval('workshop_type') == 'external'),
        },
        domain=[
           ('state', '=', 'done'),
            ('id', 'in', Eval('selectable_infinite_amounts'))
        ],  depends=['workshop_type','selectable_infinite_amounts', 'state']
                                       )
    purchase_lines_selectable = fields.Function(fields.One2Many(
        'purchase.line', None, 'Lineas Cuatías',
        depends=['infinite_amounts'],
    ), 'on_change_with_selectable_purchase_lines_selectable')

    selectable_contracts = fields.Function(fields.One2Many(
            'purchase.contract.process', None, 'Contratos disponibles',
            depends=['workshop'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_contracts')
    selectable_infinite_amounts = fields.Function(fields.One2Many(
        'purchase.purchase', None, 'Infimas cuantías',
        depends=['workshop'],
        states={
            'invisible': True
        }
    ), 'on_change_with_selectable_infinite_amounts')
    shipment_in = fields.Many2One('stock.shipment.in', 'Albarán de proveedor',
        states={
            'readonly': True,
            'invisible': ~(Eval('workshop_type') == 'external'),
        }, depends=['workshop_type'])
    selectable_budgets = fields.Function(fields.One2Many(
            'public.budget', None, 'Partidas disponibles',
            depends=['contract'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_budgets')
    pending_lines = fields.One2Many(
        'parking.vehicle.maintenance.pending.line', 'maintenance',
        'Repuestos pendientes', states={
            'readonly': ~(Eval('state').in_(['process','send']))
        }, depends=['state'])
    pending_observation = fields.Text('Observación de pendientes', states={
            'readonly': ~(Eval('state').in_(['process','send']))
        }, depends=['state'])
    budgets_summary = fields.Function(fields.One2Many(
        'parking.vehicle.maintenance.budget.summary',
        'maintenance', 'Resumen por partida', context={
            'contract': Eval('contract', -1),
            'maintenance': Eval('id', -1)
        }, depends=['contract']), 'on_change_with_budgets_summary')

    # infinite_amounts_summary = fields.Function(fields.One2Many(
    #     'parking.vehicle.maintenance.infinite.amount.summary',
    #     'maintenance','Resumen por Infima Cuantia',
    #     context={
    #         'infinite_amounts': Eval('infinite_amounts', -1),
    #         'maintenance': Eval('id', -1)
    #     }, depends=['infinite_amounts']),
    #     'on_change_with_infinite_amounts_summary')

    related_requirement = fields.Many2One(
        'purchase.requirement',
        'Solicitd de despacho', states={
            'readonly': True
        }, help='Solicitud de despacho que deberá estar en estado realizado \
        para poder continuar con el mantenimiento')
    service_to_pay = fields.Many2One('product.product', 'Servicio',
        states={
            'readonly': Eval('state') == 'finished',
            'required': (Eval('workshop_type') == 'external') & 
                ~Bool(Eval('separated_invoice')) & 
                ~(Eval('state') == 'draft') &
                ~Bool(Eval('is_not_billable')),
            'invisible': Bool(Eval('separated_invoice')) |
                Bool(Eval('is_not_billable')),
        }, depends=['state'],
        domain=[
            ('type', '=', 'service'),
            ]
        )
    budget = fields.Many2One('public.budget', 'Partida para el pago',
        states={
            'required': ~(Eval('state') == 'draft') &
                ~Bool(Eval('is_not_billable')) & (Eval('workshop_type') == 'external'),
            'invisible': Bool(Eval('is_not_billable')),
        }, domain=[
            ('id', 'in', Eval('selectable_budgets'))
        ],
        depends=['selectable_budgets'])
    separated_invoice = fields.Boolean('Facturar por separado?',
        states={
            'readonly': Eval('state') == 'finished',
            'invisible': ~(Eval('workshop_type') == 'external') |
                Bool(Eval('is_not_billable')),
        }, depends=['workshop_type', 'state'])
    payment_lines = fields.Many2Many(
        'parking.vehicle.maintenance-payment',
        'maintenance', 'payment', 'Lineas de pago', states={
            'readonly': True
        })
    invoice_generated = fields.Function(
        fields.Boolean('Pagos facturados?', depends=['payment_lines']),
        'on_change_with_invoice_generated'
        )
    is_not_billable = fields.Boolean('No se factura con contrato?',
        states={
            'readonly': ~(Eval('state') == 'draft'),
            'invisible': ~(Eval('workshop_type') == 'external'),
        }, depends=['workshop_type', 'state'])
    payments_generated = fields.Boolean('Pagos generados?',states={'readonly': True})
    multi_payment = fields.Many2One('parking.vehicle.maintenance.multi.payment',
        'Multipago de mantenimientos', states={'readonly': True})
    infinite_amount_pay = fields.Many2One('purchase.line', 'Linea de pago '
        'Infima', domain=[
            ('id', 'in',
            Eval(
                'purchase_lines_selectable'))
        ],
        depends=['purchase_lines_selectable']
        )
    includes_tax = fields.Boolean('Incluir IVA?', states={
        'invisible': ~(Eval('workshop_type') == 'external')
    }, depends=['workshop_type'])
    tax_amount = fields.Numeric('IVA', digits=(16,2), states={
        'readonly': True,
        'invisible': ~Bool(Eval('includes_tax')) &
             ~(Eval('workshop_type') == 'external')
    }, depends=['includes_tax', 'workshop_type'])
    total_with_tax = fields.Function(fields.Numeric('Total', digits=(16, 2),
        states={
            'invisible': ~(Eval('workshop_type') == 'external')
        },
        depends=['total_amount', 'tax_amount']),
        'on_change_with_total_with_tax')

    dissasembly_tires = fields.Boolean('Desmonaje Neumaticos?',
        states={
            'readonly': ~(Eval('state') == 'draft'),
            #'invisible': ~(Eval('workshop_type') == 'external'),
        }, depends=['workshop_type', 'state'])
    @classmethod
    def __setup__(cls):
        super(Maintenance, cls).__setup__()
        cls._error_messages.update({
            'no_budget': ('Alguna de las lineas de repuesto no tienen asignada'
            ' una partida, deberá asignar todas para poder continuar'),
            'no_spare': ('No todas las lineas de repuestos tienen su'
            ' producto asignado.'),
            'no_service': ('No todas las lineas de "Actividades" o '
            '"Combustibles y lubricates" tienen su servicio asignado.'),
            'no_requirement_done': ('La solicitud de despacho aun no ha sido '
            'notificada como despachada.'),
            'budget_balance': ('No se cuenta con saldo suficiente en la partida %s'
                ', por favor revise los valores de su mantenimiento'),
            'budget_balance_finish': ('No se cuenta con saldo suficiente en la partida '
                ', por favor revise los valores de su mantenimiento o realice una '
                'reforma a esa partida'),
            'infinite_amounts_balance': ('Saldo suficiente no puede realizar '
                                        'el mantenimiento'),
            'no_payment_related': ('Este multipago no tiene una línea de pago de'
            ' contrato relacionada'),
            'no_invalid_state': ('Para poder pasar a borrador nuevmante la línea'
            ' de pago debe estar en estado "Inválida".'),
            'is_multipayment': ('Este mantenimiento ha sido pagado desde'
            ' un multipago, por favor lo debe desvincular desde el mismo.'),
            'no_finished_return': ('No se puede volver a otro estado'
            ' los mantenimientos que ya han sido pagados.'),
            })
        cls._buttons.update({
            'generate_payment': {
                'invisible': ~(Eval('state').in_(['finished'])) |
                    Bool(Eval('payments_generated')) |
                    Bool(Eval('infinite_amounts')) |
                    ~(Eval('workshop_type').in_(['external'])),
            },
            'unlink_payments': {
                'invisible': ~(Eval('state').in_(['finished'])) |
                    ~Bool(Eval('payments_generated')) |
                    Bool(Eval('infinite_amounts')) |
                    ~(Eval('workshop_type').in_(['external'])),
            },
        })
    
    @staticmethod
    def default_tax_amount():
        return Decimal(0.0)
    
    @staticmethod
    def default_includes_tax():
        return True

    @fields.depends('vehicle', 'pending_lines', 'lines')
    def on_change_vehicle(self, name=None):
        pool = Pool()
        Line = pool.get('parking.vehicle.maintenance.line')
        if self.vehicle:
            if self.vehicle.pending_lines:
                for line in self.vehicle.pending_lines:
                    new_line = Line()
                    new_line.description = line.description
                    self.lines = self.lines + (new_line,)

    @fields.depends('workshop')
    def on_change_with_selectable_contracts(self, name=None):
        ids = []
        if self.workshop:
            for contract in self.workshop.contracts:
                ids.append(contract.id)
        return ids

    @fields.depends('contract')
    def on_change_with_selectable_budgets(self, name=None):
        ids = []
        if self.contract:
            for tdr_budget in self.contract.tdr_budgets:
                ids.append(tdr_budget.budget.id)
        # TODO sacar las partidas de un campo que FALTA crear que relacione 
        # con una infima cuantia que este relacionada al taller
        # C. Publicas > P. de adquisicion > Proceso de compra > S. compra
        # Modelo: purchase.header
        return ids

    @fields.depends('workshop')
    def on_change_with_selectable_infinite_amounts(self, name=None):
        ids = []
        if self.workshop:
            for infina in self.workshop.infinite_amounts:
                ids.append(infina.id)
        return ids

    @fields.depends('infinite_amounts')
    def on_change_with_selectable_purchase_lines_selectable(self, name=None):
        ids = []

        if self.infinite_amounts:
            for line in self.infinite_amounts.lines:
                ids.append(line.id)
        return ids
    
    @fields.depends('payment_lines')
    def on_change_with_invoice_generated(self, name=None):
        if not self.payment_lines:
            return False
        for line in self.payment_lines:
            if not line.is_invoice:
                return False
        return True
    
    @fields.depends('total_amount', 'tax_amount')
    def on_change_with_total_with_tax(self, name=None):
        if self.total_amount and self.tax_amount:
            return self.total_amount + self.tax_amount
        elif self.total_amount:
            return self.total_amount
        else:
            return Decimal("0.0")
    
    @classmethod
    def validate(cls, maintenances):
        for maintenance in maintenances:
            if maintenance.is_not_billable == False:
                if maintenance.contract:
                    if cls.get_budgets_summary(maintenance.id,
                        maintenance.contract.id) == False:
                        cls.raise_user_error('budget_balance',
                            maintenance.budget.code)
        super(Maintenance, cls).validate(maintenances)

    
    @classmethod
    def write(cls, maintenances, values, *args):
        pool = Pool()
        Config = pool.get('parking.place.configuration')
        maintenance_tax = Config(1).get_multivalue(
            'maintenance_tax').rate
        #super(Maintenance, cls).write(maintenances, values, *args)
        tax_amount = Decimal('0.0')
        aux = 0.0
        for maintenance in maintenances:
            if maintenance.includes_tax:
                aux = maintenance.total_amount
                if not aux:
                    aux = Decimal('0.0')
                tax_amount = aux * maintenance_tax
                tax_amount = tax_amount.quantize(Decimal('0.01'))
                values['includes_tax'] = True
            values['tax_amount'] = tax_amount
        super(Maintenance, cls).write(maintenances, values, *args)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    @set_employee('request_by')
    def request(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        #if not user.employee:
            #cls.raise_user_error('not_employee')
        for maintenance in maintenances:
            if maintenance.workshop_type == "own":
                cls.generate_requirement(maintenance)
                # cls.update_mileage_usage(maintenance)
        cls.set_number(maintenances)
        super(Maintenance, cls).save(maintenances)
        # cls.send_email(cls, maintenances, 'request')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('send')
    def send(cls, maintenances):
        for maintenance in maintenances:
            if maintenance.is_not_billable == False:
                if cls.get_budgets_summary(maintenance.id,
                    maintenance.contract.id) == False:
                    cls.raise_user_error('budget_balance',
                        maintenance.budget.code)
            if maintenance.mileage_arrival > 0 and maintenance.usage_arrival:
                cls.update_mileage_usage(maintenance)
            cls.set_number(maintenances)
            if maintenance.infinite_amounts:
                if cls.get_infinite_amounts_summary(maintenance.id,
                maintenance.infinite_amounts.id):
                    cls.raise_user_error('infinite_amounts_balance')
            if maintenance.lines:
                cls.generate_requirement(maintenance)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('entering')
    def entering(cls, maintenances):
        for maintenance in maintenances:
            if maintenance.related_requirement:
                if maintenance.related_requirement.state != 'done':
                    cls.raise_user_error('no_requirement_done')
        cls.save(maintenances)
        # for maintenance in maintenances:
        #     # COMENTADO A DECISION DE DPTO. FINANCIERO EMAC
        #     # cls.request_shipment_in_budgets(maintenance)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('handed')
    @set_employee('handed_by')
    def handed(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        Accident = pool.get('parking.vehicle.accident')
        accidents = []
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for maintenance in maintenances:
            if maintenance.related_requirement:
                if maintenance.related_requirement.state != 'done':
                    cls.raise_user_error('no_requirement_done')
            maintenance.handed_date = today
            if maintenance.related_accident:
                accident = maintenance.related_accident
                # to set accident end date
                accident.maintenance_date = maintenance.start_date
                accidents.append(accident)
        Accident.save(accidents)
        super(Maintenance, cls).save(maintenances)
        # cls.send_email(cls, maintenances, 'authorized')
        pass

    @classmethod
    @ModelView.button
    def generate_payment(cls, maintenances):
        cls.pay_external_maintenance(maintenances)
        pass

    @classmethod
    @ModelView.button
    def unlink_payments(cls, maintenances):
        pool = Pool()
        MaintenancePayment = pool.get('parking.vehicle.maintenance-payment')
        for maintenance in maintenances:
            if maintenance.multi_payment:
                cls.raise_user_error('is_multipayment')
                pass
            if not maintenance.payment_lines:
                cls.raise_user_error('no_payment_related')
                pass
            to_delete = []
            for payment in maintenance.payment_lines:
                if payment.state == 'invalid':
                    to_delete.append(payment)
                else:
                    cls.raise_user_error('no_invalid_state')
            if to_delete:
                maintenance.payments_generated = False
                # MaintenancePayment.delete(to_delete)
                cls.save([maintenance])
        pass

    @classmethod
    def request_shipment_in_budgets(cls, maintenance):
        pass
        # COMENTED CAUSE NO LONGER USE PRODUCTS
        # pool = Pool()
        # Config = pool.get('parking.place.configuration')
        # to_location_id = Config(1).get_multivalue(
        #     'to_location').id
        # ShipmentIn = pool.get('stock.shipment.in')
        # StockLocation = pool.get('stock.location')
        # StockMove = pool.get('stock.move')
        # PendingProduct = pool.get('stock.shipment.in.pending.product')
        # pool = Pool()
        # User = pool.get('res.user')
        # user = User(Transaction().user)
        # to_location_id = Config(1).get_multivalue(
        #     'to_location').id
        # from_location_id = Config(1).get_multivalue(
        #     'from_location').id
        # employee = user.employee
        # shipment = ShipmentIn()
        # shipment.effective_date = maintenance.request_date
        # shipment.planned_start_date = maintenance.request_date
        # shipment.company = maintenance.company
        # to_location = StockLocation.browse([1])[0]
        # # from_location = StockLocation.browse([from_location_id])[0]
        # # shipment.warehouse = to_location
        # shipment.reference = maintenance.description
        # shipment.supplier = maintenance.workshop.party
        # shipment.on_change_with_supplier_location()
        # shipment.certificate = maintenance.contract.tdr_certificate
        # shipment.save()
        # maintenance.shipment_in = shipment
        # maintenance.save()
        # moves = []
        # pending_lines = []
        # for line in maintenance.external_lines:
        #     if line.quantity_to_request and line.price:
        #         if line.budget:
        #             if line.spare:
        #                 move = StockMove()
        #                 move.shipment = shipment
        #                 move.product = line.spare
        #                 move.uom = line.spare.default_uom
        #                 move.quantity = float(line.quantity_to_request)
        #                 # move.from_location = shipment.warehouse
        #                 move.from_location = shipment.supplier_location
        #                 move.to_location = to_location
        #                 move.company = maintenance.company
        #                 move.planned_date = maintenance.request_date
        #                 move.currency = maintenance.company.currency
        #                 move.budget = line.budget
        #                 move.on_change_product()
        #                 # move.department = employee.contract_department
        #                 moves.append(move)
        #             else:
        #                 pending = PendingProduct()
        #                 pending.description = line.description
        #                 pending.quantity = line.quantity_to_request
        #                 pending.unit_price = line.price
        #                 pending.shipment_in = shipment
        #                 pending.budget = line.budget
        #                 # pending.incoming_move = line.
        #                 pending.origin = line
        #                 pending_lines.append(pending)
        #         else:
        #             cls.raise_user_error('no_budget')
        # StockMove.save(moves)
        # PendingProduct.save(pending_lines)
        # # shipment.get_incoming_moves()
        # shipment.save()

    @classmethod    
    def generate_requirement(cls, maintenance):
        generate = False
        for line in maintenance.lines:
            if line.to_request == True:
                generate = True
        if generate == True:
            pool = Pool()
            Requirement = pool.get('purchase.requirement')
            Line = pool.get('purchase.requirement.line')
            requirement = Requirement()
            requirement.company = maintenance.company
            requirement.employee = Transaction().context.get('employee')
            requirement.department = Transaction().context.get('department')
            requirement.date = date.today()
            requirement.description = maintenance.description
            requirement.locations = requirement.on_change_with_locations()
            requirement.location = requirement.on_change_with_location()
            requirement.save()
            for line in maintenance.lines:
                if line.quantity_to_request:
                    if line.to_request == True:
                        item = Line()
                        item.requirements = requirement
                        if line.spare:
                            item.product = line.spare
                            item.price_tag = line.spare.cost_price
                            item.type = line.spare.type
                        item.uom = line.uom
                        if line.description:
                            item.description = line.description
                        item.request_quantity = float(line.quantity_to_request)
                        item.stock = item.on_change_with_stock()
                        Line.save([item])
                        line.related_requirement_line = item
                        line.save()
                else:
                    cls.raise_user_error('no_quantity_request')
            maintenance.related_requirement = requirement
            maintenance.save()
            Requirement.save([requirement])
            # Comented because needs human interaction 
            # Requirement.request([requirement])
        # TODO add internal shipment from maintenance storage
        generate_internal_shipment = False
        for line in maintenance.lines:
            if line.quantity_to_request:
                if line.to_request == False:
                    generate_internal_shipment = True
        if generate_internal_shipment == True:        
            cls.request_maintenance_store_shipment(maintenance)

    @classmethod
    def request_maintenance_store_shipment(cls, maintenance):
        pool = Pool()
        Config = pool.get('parking.place.configuration')
        Maintenance = pool.get('parking.vehicle.maintenance')
        to_location_id = Config(1).get_multivalue('to_location').id
        reason_id =  Config(1).get_multivalue('shipment_reason').id
        from_location_id_ = Config(1).get_multivalue('from_location_').id
        ShipmentInternal = pool.get('stock.shipment.internal')
        ShipmentReason = pool.get('stock.shipment.reason')
        StockLocation = pool.get('stock.location')
        ProductTemplate = pool.get('product.template')
        StockMove = pool.get('stock.move')
        shipment = ShipmentInternal()
        shipment.planned_date = maintenance.request_date
        shipment.planned_start_date = maintenance.request_date
        shipment.company = maintenance.company
        to_location = \
            StockLocation.browse([to_location_id])[0]
        shipment.to_location = to_location
        shipment.reference = maintenance.description
        reason = ShipmentReason.browse([reason_id])[0]
        shipment.reason = reason
        shipment.on_change_with_reason_categories()
        from_location_ = \
            StockLocation.browse([from_location_id_])[0]
        shipment.from_location = from_location_

        # Setting employee
        User = pool.get('res.user')
        user = User(Transaction().user)
        employee = user.employee
        shipment.employee = employee
        shipment.on_change_employee()
        shipment.save()
        moves = []
        for line in maintenance.lines:
            if line.to_request == True:
                continue
            if line.spare and line.quantity_to_request:
                move = StockMove()
                move.shipment = shipment
                move.product = line.spare
                move.uom = line.spare.default_uom
                move.quantity = float(line.quantity_to_request)
                move.from_location = from_location_
                move.to_location = to_location
                move.company = maintenance.company
                move.planned_date = maintenance.request_date
                move.department = shipment.department
                moves.append(move)
            else:
                cls.raise_user_error('no_quantity_request')
        StockMove.save(moves)
        ShipmentInternal.draft([shipment])
        ShipmentInternal.wait([shipment])
        ShipmentInternal.assign([shipment])
        ShipmentInternal.ship([shipment])
        ShipmentInternal.done([shipment])
        maintenance.related_shipment = shipment
        Maintenance.save([maintenance])


    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, maintenances):
        super(Maintenance, cls).finish(maintenances)
        for maintenance in maintenances:
            if maintenance.workshop_type != 'own':
                if maintenance.contract:
                    if cls.get_budgets_summary(maintenance.id,
                        maintenance.contract.id) == False:
                        cls.raise_user_error('budget_balance',
                            maintenance.budget.code)
                if maintenance.infinite_amounts:
                    pass
                #     if cls.get_infinite_amounts_summary(maintenance.id,
                #         maintenance.infinite_amounts.id) == False:
                        #raise_user_error('budget_balance')


            for line in maintenance.lines:
                if not line.spare:
                    if line.related_requirement_line:
                        line.spare = line.related_requirement_line.product
                        line.save()
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('entering')
    def return_entering(cls, maintenances):
        for maintenance in maintenances:
            if maintenance.invoice_generated or maintenance.payments_generated:
                cls.raise_user_error('no_finished_return')
                pass
        pass
    
    @classmethod
    def pay_external_maintenance(cls, maintenances):
        pool = Pool()
        ContractProcessPayment = pool.get('purchase.contract.process.payment')
        ContractProcess = pool.get('purchase.contract.process')
        ContractProduct = pool.get('purchase.contract.process.product')
        PaymentLine =  pool.get('purchase.contract.process.payment.line')
        MaintenancePayment = pool.get('parking.vehicle.maintenance-payment')
        Date = pool.get('ir.date')
        for maintenance in maintenances:
            payments = []
            product_ids = [x.product.id for x in maintenance.contract.products]
            if maintenance.separated_invoice:
                # EXTERNAL SPARES PAYMENT
                if maintenance.external_lines:
                    new_payment = ContractProcessPayment(
                        process = maintenance.contract,
                        amount =  maintenance.total_external_spares,
                        planned_date = Date.today(),
                        type = 'payment',
                    )
                    ContractProcessPayment.save([new_payment])
                    new_lines = []
                    for line in maintenance.external_lines:
                        if line.item.id not in product_ids:
                            new = ContractProduct(
                                    process=maintenance.contract,
                                    product=line.item
                                )
                            product_ids.append(line.item.id)
                            ContractProduct.save([new])
                            # maintenance.contract.products = \
                            #     maintenance.contract.products + (line.item, )
                        # aqui agregar al listado de productos
                        new_line = PaymentLine()
                        new_line.payment = new_payment
                        new_line.product = line.item
                        new_line.budget = maintenance.budget
                        new_line.amount = line.quantity_to_request
                        new_line.unit_price = line.price
                        new_lines.append(new_line)
                    ContractProcess.save([maintenance.contract])
                    PaymentLine.save(new_lines)
                    payments.append(new_payment)
                # EXTERNAL SERVICES PAYMENT
                if maintenance.activities_maintenance:
                    new_payment = ContractProcessPayment(
                        process = maintenance.contract,
                        amount =  maintenance.total_activities,
                        planned_date = Date.today(),
                        type = 'payment',
                    )
                    ContractProcessPayment.save([new_payment])
                    new_lines = []
                    for line in maintenance.activities_maintenance:
                        if line.price > 0 and line.supplier_item:
                            if line.item.id not in product_ids:
                                # maintenance.contract.products = \
                                #     maintenance.contract.products + (line.item, )
                                new = ContractProduct(
                                    process=maintenance.contract,
                                    product=line.item
                                )
                                product_ids.append(line.item.id)
                                ContractProduct.save([new])
                            # aqui agregar al listado de productos
                            new_line = PaymentLine()
                            new_line.payment = new_payment
                            new_line.product = line.item
                            new_line.budget = maintenance.budget
                            new_line.amount = line.quantity_to_request
                            new_line.unit_price = line.price.quantize(
                                Decimal('0.01'))
                            new_lines.append(new_line)
                    ContractProcess.save([maintenance.contract])
                    PaymentLine.save(new_lines)
                    payments.append(new_payment)
                # EXTERNAL LIQUIDS PAYMENT
                if maintenance.liquid_lines:
                    new_payment = ContractProcessPayment(
                        process = maintenance.contract,
                        amount =  maintenance.total_liquids,
                        planned_date = Date.today(),
                        type = 'payment',
                    )
                    ContractProcessPayment.save([new_payment])
                    new_lines = []
                    for line in maintenance.liquid_lines:
                        if line.item.id not in product_ids:
                            new = ContractProduct(
                                process=maintenance.contract,
                                product=line.item
                            )
                            product_ids.append(line.item.id)
                            ContractProduct.save([new])
                            # maintenance.contract.products = \
                            #     maintenance.contract.products + (line.item, )
                        # aqui agregar al listado de productos
                        new_line = PaymentLine()
                        new_line.payment = new_payment
                        new_line.product = line.item
                        new_line.budget = maintenance.budget
                        new_line.amount = line.quantity
                        new_line.unit_price = line.price
                        new_lines.append(new_line)
                        
                    ContractProcess.save([maintenance.contract])
                    PaymentLine.save(new_lines)
                    payments.append(new_payment)
            else:
                new_payment = ContractProcessPayment(
                    process = maintenance.contract,
                    amount =  maintenance.total_amount,
                    planned_date = Date.today(),
                    type = 'payment',
                )
                ContractProcessPayment.save([new_payment])
                new_lines = []

                if maintenance.service_to_pay.id not in \
                        product_ids:
                    new = ContractProduct(
                        process=maintenance.contract,
                        product=maintenance.service_to_pay
                    )
                    product_ids.append(maintenance.service_to_pay.id)
                    ContractProduct.save([new])
                    # maintenance.contract.products = \
                    #     maintenance.contract.products + (
                    #         maintenance.service_to_pay, )
                # aqui agregar al listado de productos
                new_line = PaymentLine()
                new_line.payment = new_payment
                new_line.product = maintenance.service_to_pay
                new_line.budget = maintenance.budget
                new_line.amount = 1
                new_line.unit_price = maintenance.total_amount
                new_lines.append(new_line)
                ContractProcess.save([maintenance.contract])
                PaymentLine.save(new_lines)
                payments.append(new_payment)
            for payment in payments:
                maintenance_payment = MaintenancePayment()
                maintenance_payment.maintenance = maintenance.id
                maintenance_payment.payment = payment.id
                maintenance_payment.save()
                maintenance.payments_generated = True
                maintenance.save()
            # ContractProcessPayment.create_process(payments)
        pass

    # @fields.depends('id', 'infinite_amounts')
    # def on_change_with_infinite_amounts_summary(self,  name=None):
    #     if self.infinite_amounts:
    #         pool = Pool()
    #         cursor = Transaction().connection.cursor()
    #         AmountSumary = pool.get(
    #             'parking.vehicle.maintenance.infinite.amount.summary')
    #         amount_sumary = None
    #         with Transaction().set_context(maintenance=self.id,
    #             infinite_amounts=self.infinite_amounts.id):
    #             amount_sumary = AmountSumary.table_query()
    #         ids_summaries = []
    #         cursor.execute(*amount_sumary)
    #         for summary in cursor.fetchall():
    #             ids_summaries.append(summary[0])
    #             print(summary)
    #         return ids_summaries
    #         print('sale')
    #
    #     else:
    #         return []
    #
    # def get_infinite_amounts_summary(maintenance_id, infinite_amounts_id):
    #     pool = Pool()
    #     cursor = Transaction().connection.cursor()
    #
    #     AmountSumary = pool.get(
    #         'parking.vehicle.maintenance.infinite.amount.summary')
    #     amount_sumary = None
    #     with Transaction().set_context(maintenance=maintenance_id,
    #                                    infinite_amounts=infinite_amounts_id):
    #         amount_sumary = AmountSumary.table_query()
    #     ids_summaries = []
    #     cursor.execute(*amount_sumary)
    #     for summary in cursor.fetchall():
    #         if summary[len(summary) - 1] < 0:
    #             return False
    #     return True


    @fields.depends('id', 'contract')
    def on_change_with_budgets_summary(self, name=None):
        if self.contract:
            pool = Pool()
            cursor = Transaction().connection.cursor()

            BudgetSummary = pool.get(
                'parking.vehicle.maintenance.budget.summary')

            budget_summary = None
            with Transaction().set_context(maintenance=self.id,
                contract=self.contract.id):
                budget_summary = BudgetSummary.table_query()
            ids_summaries = []
            cursor.execute(*budget_summary)
            for summary in cursor.fetchall():
                ids_summaries.append(summary[0])
            return ids_summaries
        else:
            return []
        
    def get_budgets_summary(maintenance_id, contract_id):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        BudgetSummary = pool.get(
            'parking.vehicle.maintenance.budget.summary')

        budget_summary = None
        with Transaction().set_context(maintenance=maintenance_id,
            contract= contract_id):
            budget_summary = BudgetSummary.table_query()
        ids_summaries = []
        cursor.execute(*budget_summary)
        for summary in cursor.fetchall():
            if summary[len(summary) -1] < 0:
                return False
        return True

    def get_infinite_amounts_summary(maintenance_id, infinite_amounts_id):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        InfiniteAmountSummary = pool.get('parking.infinite.amounts.summary')
        Transaction().set_context(maintenance=maintenance_id,
            infinite_amounts= infinite_amounts_id)
        infinite_amount_summary = InfiniteAmountSummary.table_query()
        ids_summaries = []
        cursor.execute(*infinite_amount_summary)
        for summary in cursor.fetchall():
            return summary[len(summary) -1] < 0


    @classmethod
    def view_attributes(cls):
        return super(Maintenance, cls).view_attributes() + [(
            '/form/notebook/page[@id="mantainance_info"]/group\
                [@id="group_spares"]/label[@name="related_shipment"]', 
            'states', {
                'invisible': True,
            }),
            ('/form/notebook/page[@id="budgets_info"]', 
            'states', {
                'invisible': Eval('workshop_type').in_(['own']),
            })
        ]


class MaintenanceLine(ModelSQL, ModelView):
    "Maintenance Line"
    __name__ = 'parking.vehicle.maintenance.line'
    uom = fields.Many2One('product.uom', 'Udm',
         states={
        'readonly': Or(
            ~Eval('_parent_maintenance', {}).get('state').in_(
                ['draft', 'send']),
            (Bool(Eval('spare')))
        ), 'required': True
        }, depends=['maintenance', 'spare'])
    related_requirement_line = fields.Many2One('purchase.requirement.line',
        'Línea de requerimiento relacionada',
         states={
            'readonly': True
        })

    @fields.depends('spare', 'uom')
    def on_change_spare(self, name=None):
        if self.spare:
            self.uom = self.spare.default_uom
        else:
            self.uom = None


class MaintenancePaymentLine(ModelSQL, ModelView):
    "Maintenance - Contract Process Payment"
    __name__ = 'parking.vehicle.maintenance-payment'
    
    maintenance = fields.Many2One('parking.vehicle.maintenance', 'Mantenimiento',
        ondelete='CASCADE', select=True)
    payment = fields.Many2One('purchase.contract.process.payment', 'Línea de pago',
        ondelete='CASCADE', select=True)


class MaintenanceLineLiquid(metaclass=PoolMeta):
    __name__ = 'parking.vehicle.maintenance.line.liquid'

    _depends = ['maintenance']

    _states = {
        'readonly':
            Eval('_parent_maintenance', {}).get('state').in_(['finished']),
        'required':
            ~(Eval('_parent_maintenance', {}).get('workshop_type').in_(
                ['own']
            ))
    }

    item = fields.Many2One('product.product', 'Liquido',
        domain=[('type', '=', 'assets')],
        states=_states, depends=_depends
    )


class MaintenancePendingLine(ModelSQL, ModelView):
    "Maintenance Pending Line"
    __name__ = 'parking.vehicle.maintenance.pending.line'

    description = fields.Char('Descripción', required=True)
    quantity_pending = fields.Numeric( 'Cantidad pendiente')
    vehicle = fields.Many2One(
        'parking.vehicle.vehicle', 'Vehículo', states={
            'readonly': True
        })
    maintenance = fields.Many2One(
        'parking.vehicle.maintenance', 'Mantenimiento')

    @fields.depends('maintenance')
    def on_change_maintenance(self, name=None):
        if self.maintenance:
            if self.maintenance.vehicle:
                self.vehicle = self.maintenance.vehicle


class MaintenancePreventive(metaclass=PoolMeta):
    __name__ = 'parking.vehicle.maintenance.preventive'

    is_cycle = fields.Boolean('ciclo recorrido?')
    cycle_value = fields.Numeric('Valor del ciclo (recorrido)', states={
        'invisible': ~(Eval('is_cycle')),
        'required': Bool(Eval('is_cycle'))
    }, depends=['is_cycle'])
    is_time_cycle = fields.Boolean('ciclo tiempo?')
    cycle_value_time = fields.Numeric('Valor del ciclo (horas)', states={
        'invisible': ~(Eval('is_time_cycle')),
        'required': Bool(Eval('is_time_cycle'))
    }, depends=['is_cycle'])
    is_cycle_by_date = fields.Boolean('Es ciclico por fecha?')
    cycle_value_by_date = fields.Numeric('Valor del ciclo ', states={
        'invisible': ~(Eval('is_cycle_by_date')),
        'required': Bool(Eval('is_cycle_by_date'))
    }, depends=['is_cycle_by_date'])
    time_of_cicle = fields.Selection([
        (None, ""),
        ('days', 'Días'),
        ('weeks', 'Semanas'),
        ('months', 'Meses'),
        ('years', 'Años')],
        'Unidad de Ciclo', states={
            'invisible': ~(Eval('is_cycle_by_date')),
            'required': Bool(Eval('is_cycle_by_date'))},
        depends=['is_cycle_by_date'])
    start_date = fields.Date('Fecha inicio', states={
            'invisible': ~(Eval('is_cycle_by_date')),
            'required': Bool(Eval('is_cycle_by_date'))},
        depends=['is_cycle_by_date'])

    @classmethod
    def cron_check_preventive_maintenance(cls):
        """Cron add maintenance.
                """
        day = 1
        week = 7
        month = 30
        year = 365
        diff = 0
        pool = Pool()
        Vehicles = pool.get('parking.vehicle.vehicle')
        vehicles = Vehicles.search([])
        Maintenance = pool.get('parking.vehicle.maintenance')
        Maintenance_Preventive = pool.get(
            'parking.vehicle.maintenance.preventive')
        preventives_maintenances = Maintenance_Preventive.search([])
        today = date.today()
        for vehicle in vehicles:
            for preventive_maintenance in vehicle.maintenances:
                if preventive_maintenance.start_date: #and not \
                    aux = preventive_maintenance.start_date
                #     preventive_maintenance.maintenance:
                    if preventive_maintenance.is_cycle_by_date:
                        if preventive_maintenance.cycle_value_by_date:
                            if preventive_maintenance.time_of_cicle and \
                                not preventive_maintenance.maintenance_related:
                                if preventive_maintenance.time_of_cicle == 'days':
                                    diff = (preventive_maintenance.cycle_value_by_date * day)
                                    diff_date = (
                                        preventive_maintenance.start_date +
                                                 timedelta(days=round(diff)))
                                if preventive_maintenance.time_of_cicle == 'weeks':
                                    diff = (preventive_maintenance.
                                            cycle_value_by_date * week)
                                    diff_date = (
                                        preventive_maintenance.start_date +
                                                 timedelta(days=round(diff)))
                                if preventive_maintenance.time_of_cicle == 'months':
                                    diff = (
                                        preventive_maintenance.cycle_value_by_date
                                        * month)
                                    diff_date = (preventive_maintenance.start_date +
                                                 timedelta(days=round(diff)))
                                if preventive_maintenance.time_of_cicle == 'years':
                                    diff = (
                                        preventive_maintenance.cycle_value_by_date
                                        * year)
                                    diff_date = (preventive_maintenance.start_date +
                                                 timedelta(days=round(diff)))
                                if diff_date <= today:
                                    next_diff_date = (diff_date +
                                                 timedelta(days=round(diff)))
                                    new_line = Maintenance_Preventive()
                                    new_line.maintenance_description = \
                                        "Mantenimiento: " + str(next_diff_date) + " "
                                    new_line.date = next_diff_date
                                    new_line.template = preventive_maintenance.template
                                    new_line.is_cycle_by_date = True
                                    new_line.start_date = today
                                    new_line.cycle_value_by_date = \
                                        preventive_maintenance.cycle_value_by_date
                                    new_line.vehicle = vehicle.id
                                    new_line.time_of_cicle = \
                                        preventive_maintenance.time_of_cicle
                                    Maintenance_Preventive.save([new_line])
                                    maintenance = Maintenance()
                                    if preventive_maintenance.template:
                                        maintenance.template = \
                                            preventive_maintenance.template
                                    maintenance.vehicle = vehicle.id
                                    maintenance.type = 'preventive'
                                    maintenance.related_maintenance_preventive = preventive_maintenance
                                    maintenance.description = \
                                        preventive_maintenance.maintenance_description
                                    maintenance.state = 'draft'
                                    if preventive_maintenance.date:
                                        maintenance.plannified_start_date = preventive_maintenance.date
                                    maintenance.start_date = today
                                    maintenance.start_time = None
                                    maintenance.estimated_day = 0
                                    maintenance.estimated_time = 0
                                    maintenance.on_change_template()
                                    maintenance.company = Transaction().context.get(
                                        'company')
                                    Maintenance.save([maintenance])
                                    preventive_maintenance.maintenance_related = maintenance
                                    Maintenance_Preventive.save([preventive_maintenance])

            # cls.notify_maintenance([maintenance])

    @staticmethod
    def default_cycle_value_by_date():
        return 1

    @staticmethod
    def default_time_of_cicle():
        return 'months'

    def notify_maintenance(cls, maintenances):
        pool = Pool()
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Mantenimiento'),
            ('template', '=', True),
        ])
        alerts = []
        for maintenance in maintenances:
            if maintenance:
                employees = []
                Group = pool.get('res.group')
                search_group = Group.search([('name', '=',
                                            'Parque automotor / Mantenimientos')
                                             ])
                if search_group:
                    for user in search_group[0].users:
                        if user.employee:
                            employees.append(user.employee)
                mail_body = cls.generate_text(maintenance)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                         f"parking.vehicle.maintenance.preventive,"
                         f"{maintenance.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                    ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = maintenance
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) empleado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_text(cls, maintenance):
        return (f" Alerta realizar mantenimiento de  "
                f"{maintenance.description} "
                "para el vehículo " f"{maintenance.vehicle.rec_name}. \n"
                "Por favor revisar este mantenimiento creado en estado borrador "
                "dentro del sistema SIIM."
                )
        pass


class UpdateVehicleMileage(metaclass=PoolMeta):
    __name__ = 'parking.vehicle.update.mileage'
    
    def transition_change(self):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        Maintenance = pool.get('parking.vehicle.maintenance')
        MaintenancePreventive = pool.get(
            'parking.vehicle.maintenance.preventive')
        vehicle = Vehicle(Transaction().context['active_id'])
        MileageUpdate = pool.get('parking.vehicle.mileage.update')
        NewPreventiveMaintenance = pool.get(
            'parking.vehicle.maintenance.preventive')
        Date = pool.get('ir.date')
        if (self.start.new_mileage < vehicle.current_mileage) and \
                (self.start.new_mileage > 0):
            self.raise_user_error('lower_mileage', vehicle.rec_name)
        if (self.start.new_usage < vehicle.current_usage) and \
                (self.start.new_usage > 0):
            self.raise_user_error('lower_mileage', vehicle.rec_name)
        else:
            mileage_update = MileageUpdate()
            mileage_update.new_mileage = self.start.new_mileage
            mileage_update.new_usage = self.start.new_usage
            mileage_update.description = self.start.description
            mileage_update.vehicle = vehicle
            MileageUpdate.save([mileage_update])
            vehicle.current_mileage = self.start.new_mileage
            vehicle.current_usage = self.start.new_usage
            Vehicle.save([vehicle])

        new_preventive_maintenances = []
        for line in vehicle.maintenances:
            text = ''
            # Asking for create new preventive lines
            # if not line.maintenance_related:
            if not line.maintenance_related:
                print( str(line.id) + " > " + line.maintenance_description)
                if line.is_cycle or line.is_time_cycle:
                    if line.expected_use and self.start.new_mileage and \
                        line.expected_use != 0 and self.start.new_mileage != 0:
                        if (line.expected_use-vehicle.mileage_range_alert) <= \
                            self.start.new_mileage and line.cycle_value:
                            new_line = NewPreventiveMaintenance()
                            new_line.maintenance_description = \
                                "Mantenimiento por: "+str(line.expected_use + \
                                line.cycle_value) + " de recorrido"
                            if line.cycle_value:
                                new_line.expected_use = self.start.new_mileage + \
                                    line.cycle_value
                            else:
                                new_line.expected_use = 0
                            if line.cycle_value_time:
                                new_line.expected_time = self.start.new_usage + \
                                    line.cycle_value_time
                            else:
                                new_line.expected_time = 0
                            new_line.template = line.template
                            new_line.vehicle = vehicle
                            new_line.is_cycle = True
                            new_line.cycle_value = line.cycle_value
                            new_line.cycle_value_time = line.cycle_value_time
                            new_preventive_maintenances.append(new_line)
                    elif line.expected_time and self.start.new_usage and \
                        line.expected_time != 0 and self.start.new_usage != 0:
                        if (line.expected_time-vehicle.usage_range_alert) <= \
                            self.start.new_usage and line.cycle_value_time:
                            new_line = NewPreventiveMaintenance()
                            new_line.maintenance_description = \
                                "Mantenimiento por "+str(line.expected_time + \
                                line.cycle_value_time) + " de horas maquina"
                            if line.cycle_value:
                                new_line.expected_use = self.start.new_mileage + \
                                    line.cycle_value
                            else:
                                new_line.expected_use = 0
                            if  line.cycle_value_time:
                                new_line.expected_time = self.start.new_usage + \
                                    line.cycle_value_time
                            else:
                                new_line.expected_time = 0
                            new_line.template = line.template
                            new_line.vehicle = vehicle
                            new_line.is_time_cycle = True
                            new_line.cycle_value = line.cycle_value
                            new_line.cycle_value_time = line.cycle_value_time
                            new_preventive_maintenances.append(new_line)


            # Asking for pendant maintenances
            if not line.maintenance_related:
                if line.expected_use:
                    if (line.expected_use-vehicle.mileage_range_alert) <= \
                            self.start.new_mileage:
                        text = "Se sobrepasó el recorrido"
                if line.expected_time:
                    if (line.expected_time-vehicle.usage_range_alert) <= \
                            self.start.new_usage:
                        if text == '':
                            text = "Se sobrepasó el uso"
                        else:
                            text = text + ', y el uso'
                # if line.date:
                #     if line.date <= date.today():
                #         if text == '':
                #             text = "Se pasó de la fecha"
                #         else:
                #             text = text + ', y la fecha'
                if text != '':
                    text = text + ' previsto en el mantenimiento ' + \
                        'planificado con descripción: "' + \
                        line.maintenance_description + '", por lo tanto se' + \
                        'ha creado un mantenimiento en borrador.'         
                    Note = pool.get('ir.note')
                    ReadNote = pool.get('ir.note.read')
                    note = Note()
                    note.resource = vehicle
                    note.message = text
                    note.create_uid = 1
                    Note.save([note])
                    notes = Note.search([('resource', '=', str(vehicle))])
                    for item in notes:
                        read_notes = ReadNote.search([('note', '=', item.id)])
                        for read in read_notes:
                            read.user = 1
                            ReadNote.save([read])
                    maintenance = Maintenance()
                    if line.template:
                        maintenance.template = line.template
                    maintenance.vehicle = vehicle
                    maintenance.type = 'preventive'
                    maintenance.related_maintenance_preventive = line
                    maintenance.description = line.maintenance_description
                    maintenance.state = 'draft'
                    maintenance.plannified_start_date = Date.today()
                    maintenance.start_date = Date.today()
                    maintenance.start_time = None                    
                    maintenance.estimated_day = Decimal("0.0")
                    maintenance.estimated_time = Decimal("0.0")
                    maintenance.on_change_template()
                    maintenance.company = Transaction().context.get('company')
                    Maintenance.save([maintenance])
                    line.maintenance_related = maintenance
                    MaintenancePreventive.save([line])
                    self.notify_maintenance([maintenance])
        # Saving at the end, otherwise it will cause a bucle
        NewPreventiveMaintenance.save(new_preventive_maintenances)
        return 'end'

    def notify_maintenance(cls, maintenances):
        pool = Pool()
        SendEmail = pool.get('parking.place.maintenance.send.email')
        employees_ = SendEmail.search([])
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Mantenimiento'),
            ('template', '=', True),
        ])
        alerts = []
        for maintenance in maintenances:
            if maintenance:
                employees = []
                # Group = pool.get('res.group')
                # search_group = Group.search([('name', '=',
                # 'Parque automotor / Mantenimientos')
                # ])
                # if search_group:
                #     for user in search_group[0].users:
                #         if user.employee:
                #             employees.append(user.employee)
                for employee in employees_:
                    employees.append(employee.email)
                mail_body = cls.generate_text(maintenance)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                        f"parking.vehicle.maintenance,"
                        f"{maintenance.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                         ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = maintenance
                        # The mail body depends of transition state
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) empleado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_text(cls, maintenance):
        return (f"Se a creado un nuevo mantenimiento debido a que existia un "
            "mantenimiento planificado a razon de: " f"{maintenance.description} "
            "para el vehículo " f"{maintenance.vehicle.rec_name}. \n"
            "Por favor revisar este mantenimiento creado en estado borrador "
            "dentro del sistema SIIM."
            )
    # def spares_return(self):
    #     pool = Pool()
    #     Spares = pool.get('parking.vehicle.maintenance.line')
        pass


class FuelOrder(metaclass=PoolMeta):
    __name__ = 'parking.fuel_order'
	
    invoice_sequence = fields.Char('Secuencial de factura', required=True)
    
    @classmethod
    def __setup__(cls):
        super(FuelOrder, cls).__setup__()
        cls.vehicle.states.update({
            'required': False,
        })

    @fields.depends('vehicle')
    def on_change_with_department(self, name=None):
        if self.vehicle:
            if self.vehicle.asset:
                if self.vehicle.asset.owners:
                    return self.vehicle.asset.owners[0].employee.\
                        contract_department.id
        return None

    # TEMPORALY REMOVED TO PROVE TOTAL CALC
    # @fields.depends('gallons_number', 'price')
    # def on_change_with_fuel_cost(self, name=None):
    #     if self.gallons_number and self.price:
    #         return self.gallons_number * self.price


class FuelType(metaclass=PoolMeta):
    __name__ = 'parking.fuel_type'

    spare = fields.Many2One('product.product', 'Servicio',
        states={
            'required': True
        }, domain=[
            ('type', '=', 'goods'),
        ]
    )


class FuelOrderPayment(metaclass=PoolMeta):
    __name__ = 'parking.fuel_order.payment'

    fuel_station = fields.Many2One('parking.fuel_station', 'Gasolinera',
        states={
            'readonly': ~Eval('state').in_(['draft']),
            'required': True
        }, depends=['state'])
    invoice_sequence = fields.Char('Secuencial de factura', states={
            'readonly': ~Eval('state').in_(['draft']),
            'required': True
        }, depends=['state'])
    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        states={
            'readonly': ~Eval('state').in_(['draft']),
            'required': True
        },
        domain=[
            ('state', '=', 'open'),
            ('id', 'in', Eval('selectable_contracts'))
        ], depends=['selectable_contracts', 'state'])
    selectable_contracts = fields.Function(fields.One2Many(
            'purchase.contract.process', None, 'Contratos disponibles',
            depends=['fuel_station'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_contracts')
    selectable_budgets = fields.Function(fields.One2Many(
            'public.budget', None, 'Partidas disponibles',
            depends=['contract'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_budgets')
    total_lines = fields.One2Many(
        'parking.fuel_order.payment.line',
        'payment', 'Totales',
        states={
            'readonly': ~Eval('state').in_(['draft'])
        }, depends=['state'],)
    
    total = fields.Function(fields.Numeric("Total", digits=(16,3),
        depends=['total_lines']), 'on_change_with_total')


    @classmethod
    def __setup__(cls):
        super(FuelOrderPayment, cls).__setup__()
        cls.date.states.update({
            'readonly': True,
        })
        cls._buttons.update({
            'generate_summary': {
                'invisible': Eval('state').in_(['registered']),
            },
        })
        cls._error_messages.update({
            'no_budget': ('Alguna de las lineas de totales no tienen asignada'
            ' una partida, deberá asignar todas para poder registrar el pago'),
            'no_department': ('La orden de combustible # '
                '"%(number)s", no tiene departamento'),
            })

    @fields.depends('fuel_station')
    def on_change_with_selectable_contracts(self, name=None):
        ids = []
        if self.fuel_station:
            for contract in self.fuel_station.contracts:
                ids.append(contract.id)
        return ids
    
    @fields.depends('contract')
    def on_change_with_selectable_budgets(self, name=None):
        ids = []
        if self.contract:
            for tdr_budget in self.contract.tdr_budgets:
                ids.append(tdr_budget.budget.id)
        return ids

    @fields.depends('total_lines')
    def on_change_with_total(self, name=None):
        total = Decimal('0.0')
        for line in self.total_lines:
            if line.total:
                total += line.total
        return total

    @classmethod
    @ModelView.button
    def generate_summary(cls, payments):
        for payment in payments:
            for order in payment.fuel_orders:
                if not order.department:
                    cls.raise_user_error('no_department',
                    {'number': str(order.number)})
            pool = Pool()
            cursor = Transaction().connection.cursor()

            Query = pool.get('parking.fuel_order.payment.line.query')
            TotalLine = pool.get('parking.fuel_order.payment.line')

            total_lines = None
            with Transaction().set_context(payment=payment.id):
                total_lines = Query.table_query()
            items = []
            payment.total_lines = []
            cursor.execute(*total_lines)
                # 0 RowNumber(window=Window([])).as_('id'),
                # 1 Literal(0).as_('create_uid'),
                # 2 CurrentTimestamp().as_('create_date'),
                # 3 Literal(0).as_('write_uid'),
                # 4 CurrentTimestamp().as_('write_date'),
                # 5 query.name,
                # 6 (query.unit_price * query.gallons_number).as_('total'),
                # 7 query.payment,
                # 8 query.spare,
                # 9 query.unit_price,
                # 10 query.department,
                # 11 query.gallons_number,
                # 12 Literal(0).as_('budget'),
            for summary in cursor.fetchall():
                total_line = TotalLine()
                total_line.name = summary[5]
                total_line.department = summary[10]
                total_line.spare = summary[8]
                total_line.gallons_number = round(summary[6]/summary[9], 4)
                total_line.unit_price = summary[9]
                total_line.total = round(summary[6], 2)
                total_line.budget = None
                items.append(total_line)
                # payment.total_lines = payments.total_lines + (total_line, )
            payment.total_lines = items
            payment.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('registered')
    def registration(cls, payments):
        super(FuelOrderPayment, cls).registration(payments)
        pool = Pool()
        ContractProcessPayment = pool.get('purchase.contract.process.payment')
        ContractProcess = pool.get('purchase.contract.process')
        ContractProduct = pool.get('purchase.contract.process.product')
        PaymentLine =  pool.get('purchase.contract.process.payment.line')
        Date = pool.get('ir.date')
        for payment in payments:
            for line in payment.total_lines:
                if not line.budget:
                    cls.raise_user_error('no_budget')
            new_payment = ContractProcessPayment(
                process = payment.contract,
                amount =  payment.total,
                planned_date = Date.today(),
                type = 'payment',
            )
            product_ids = [x.product.id for x in payment.contract.products]
            payment.date = Date.today()
            payment.save()
            ContractProcessPayment.save([new_payment])
            new_lines = []
            for line in payment.total_lines:
                if line.spare.id not in product_ids:
                    new = ContractProduct(
                        process=payment.contract,
                        product=line.spare
                    )
                    product_ids.append(line.spare.id)
                    ContractProduct.save([new])
                # aqui agregar al listado de productos
                new_line = PaymentLine()
                new_line.payment = new_payment
                new_line.product = line.spare
                new_line.budget = line.budget
                # new_line.amount = line.gallons_number
                new_line.amount = round(line.total / line.unit_price, 4)
                new_line.unit_price = line.unit_price
                new_lines.append(new_line)
            ContractProcess.save([payment.contract])
            PaymentLine.save(new_lines)
            # ContractProcessPayment.create_process([new_payment])
        pass


class FuelOrderPaymentLineQuery(ModelSQL, ModelView):
    "Payment Total Line"
    __name__ = 'parking.fuel_order.payment.line.query'

    payment = fields.Many2One('parking.fuel_order.payment', 'Contrato',
        readonly=True)
    name = fields.Char('Tipo de combustible', readonly=True)
    gallons_number = fields.Numeric('Galones', digits=(16, 3), 
        readonly=True)
    unit_price = fields.Numeric('Precio unitario', digits=(16, 3), 
        readonly=True)
    total = fields.Numeric('Total', digits=(16, 3), readonly=True)
    spare = fields.Many2One('product.product', 'Servicio',
        readonly=True)
    department = fields.Many2One('company.department', 'Departamento',
        readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        payment_id = context.get('payment')
        pool = Pool()
        FuelOrder = pool.get('parking.fuel_order')
        fuel_order = FuelOrder.__table__()
        FuelType = pool.get('parking.fuel_type')
        fuel_type = FuelType.__table__()
        Payment = pool.get('parking.fuel_order.payment')
        payment = Payment.__table__()
        Department = pool.get('company.department')
        department = Department.__table__()
        where = ( payment_id == payment.id)


        query = fuel_order.join(payment,
            condition=fuel_order.payment == payment.id
        ).join(department,
            condition=fuel_order.department == department.id
        ).join(fuel_type,
            condition=fuel_order.fuel_type == fuel_type.id
        ).select(
            fuel_type.name.as_('name'),
            fuel_type.spare.as_('spare'),
            fuel_type.cost.as_('unit_price'),
            department.id.as_('department'),
            Sum(fuel_order.gallons_number).as_('gallons_number'),
            Sum(fuel_order.fuel_cost).as_('total'),
            payment.id.as_('payment'),
            where=where,
            group_by=[fuel_type.name, payment.id, fuel_type.spare,
                fuel_type.cost,department.id]
        )

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.name,
            query.total,
            query.payment,
            query.spare,
            query.unit_price,
            query.department,
            query.gallons_number,
        )


class FuelOrderPaymentLine(ModelSQL, ModelView):
    "Payment Total Line"
    __name__ = 'parking.fuel_order.payment.line'

    payment = fields.Many2One('parking.fuel_order.payment', 'Liquidación',
        readonly=True)
    name = fields.Char('Tipo de combustible', readonly=True)
    gallons_number = fields.Numeric('Galones', digits=(16, 4), 
        readonly=True)
    unit_price = fields.Numeric('Precio unitario', digits=(16, 3), 
        readonly=True)
    total = fields.Numeric('Total', digits=(16, 3), readonly=True)
    spare = fields.Many2One('product.product', 'Servicio',
        readonly=True)
    department = fields.Many2One('company.department', 'Departamento',
        readonly=True)
    budget = fields.Many2One('public.budget', 'Partida',
        states={
            'readonly': ~(Eval('_parent_payment',{}).get('state').in_(['draft']))
            # 'required': True
        }, domain=[
            ('id', 'in', Eval('_parent_payment',{}).get('selectable_budgets'))
        ],
        depends=['payment'])
        

class PaymentFuelOrdersPaymentStart(ModelView):
    'Payment Fuel Orders Date Range Start'
    __name__ = 'parking.vehicle.payment.fuel.orders.payment.start'

    invoice_sequence = fields.Char('Secuencial de factura')


class PaymentFuelOrdersPayment(Wizard):
    'Payment Fuel Orders Date Range'
    __name__ = 'parking.vehicle.payment.fuel.orders.payment'

    start = StateView(
        'parking.vehicle.payment.fuel.orders.payment.start',
        'parking_public_pac.vehicle_payment_fuel_orders_payment_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'charge', 'tryton-ok', default=True),
        ])
    charge = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PaymentFuelOrdersPayment, cls).__setup__()

    def transition_charge(self):
        pool = Pool()
        Payment = pool.get('parking.fuel_order.payment')
        FuelOrder = pool.get('parking.fuel_order')
        payment = Payment(Transaction().context['active_id'])
        search_orders = []
        search_orders = FuelOrder.search([
            ('invoice_sequence', '=', self.start.invoice_sequence)
        ], order=[('id', 'ASC')])
        for order in payment.fuel_orders:
            order.payment = None
        FuelOrder.save(payment.fuel_orders)
        payment.fuel_orders = search_orders
        Payment.save([payment])
        return 'end'


class UploadFuelOrdersSucceed(ModelView):
    'Upload Fuel Orders Succeed'
    __name__ = 'parking.fuel_order.upload.succeed'


class UploadFuelOrdersStart(ModelView):
    'Upload Fuel Orders'
    __name__ = 'parking.fuel_order.upload.start'

    is_machine = fields.Boolean('Maquinaria sin placa?')
    source_file = fields.Binary('Archivo a cargar', required=True)
    start_date = fields.Date('Fecha inicio', required=True)
    end_date = fields.Date('Fecha fin', required=True)
    # fuel_station = fields.Many2One('parking.fuel_station', 'Gasolinera',
    #     required=True)
    # contract = fields.Many2One('purchase.contract.process', 'Contrato',
    #     domain=[
    #         ('state', '=', 'open'),
    #         ('id', 'in', Eval('selectable_contracts'))
    #     ], depends=['selectable_contracts'], required=True)
    # selectable_contracts = fields.Function(fields.One2Many(
    #         'purchase.contract.process', None, 'Contratos disponibles',
    #         depends=['fuel_station'],
    #         states={
    #             'invisible': True
    #         }
    #     ), 'on_change_with_selectable_contracts')
    
    # @fields.depends('fuel_station')
    # def on_change_with_selectable_contracts(self, name=None):
    #     ids = []
    #     if self.fuel_station:
    #         for contract in self.fuel_station.contracts:
    #             ids.append(contract.id)
    #     return ids


class UploadFuelOrders(Wizard):
    'Upload Payslip Rule Start'
    __name__ = 'parking.fuel_order.upload'

    start = StateView('parking.fuel_order.upload.start',
        'parking_public_pac.upload_fuel_orders_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()
    succeed = StateView('parking.fuel_order.upload.succeed',
        'parking_public_pac.upload_fuel_orders_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
        ])

    @classmethod
    def __setup__(cls):
        super(UploadFuelOrders, cls).__setup__()
        cls._error_messages.update({
            'no_fuel_type': ('No existe tipo de combustible'
            ': %(fuel_type)s, por favor registrela e intentente nuevamente'),
            'no_identifier': ('No existe empleado con número de cédula'
            ': %(identifier)s'),
            'no_vehicle': ('No existe vehículo con placa'
            ': %(plaque)s'),
            'no_plaque': ('Orden sin placa, Orden #'
            ': %(number)s'),
            'out_date': ('Orden de combustible Nro'
            ': %(number)s se encuentra fuera del rango establecido'),
            'no_employee': ('Orden de combustible Nro'
            ': %(number)s se encuentra fuera del rango establecido'),
            # 'format_error': ('La linea %(field)s ,\
            #         tiene un problema de formato en uno de los campos.'),
            # 'index_error': ('No se ha encontrado registros con los datos \
            #         de la línea %(field)s'),
            })

    def transition_import_(self):
        pool = Pool()
        FuelOrder = pool.get('parking.fuel_order')
        FuelType = pool.get('parking.fuel_type')
        Identifier = pool.get('party.identifier')
        Employee = pool.get('company.employee')
        Vehicle = pool.get('parking.vehicle.vehicle')

        field_names = ['fecha_carga', 'nro_orden', 'cedula', 'fac_secuencial',
            'placa', 'tipo_combustible', 'galones', 'precio_unit',
            'subtotal', 'iva', 'total','recorrido', 'uso']
        file_data = fields.Binary.cast(self.start.source_file)
        try:
            df = pd.read_excel(
                BytesIO(file_data), names=field_names, dtype='object', header=2)
            df = df.fillna(False)
        except ValueError:
            raise UserError("El número de columnas del archivo seleccionado " +
                    "no concuerda con el de la plantilla establecida.")
        to_save_orders = []

        for cont, row in enumerate(df.itertuples()):
            items = get_dict_from_readed_row(field_names, row)
            fuel_search = FuelType.search([
                    ('name', '=', items['tipo_combustible'])
                ])
            fuel_order = FuelOrder()
            if fuel_search:
                fuel_order.fuel_type = FuelType.browse(fuel_search)[0]
                fuel_order.on_change_fuel_type()
            else:
                self.raise_user_error('no_fuel_type',
                    {'fuel_type': items['tipo_combustible']})
            try:
                identifier = str(items['cedula'])
                identifier_search = Identifier.search([('code', '=', identifier)])
                if identifier_search:
                    party_id = identifier_search[0].party.id
                    employee_search = Employee.search([('party', '=', party_id)])
                    if not employee_search:
                        self.raise_user_error('no_identifier',
                            {'identifier': items['cedula']})
                    employee = Employee.browse(employee_search)[0]
                    fuel_order.employee = employee
                    # fuel_order.department = employee.contract_department
                else:
                    self.raise_user_error('no_identifier',
                        {'identifier': items['cedula']})
            except AssertionError:
                raise UserError("El formato de la cédula en la línea %s \
                    es incorrecto" % (cont))
            if not self.start.is_machine:
                if items['placa']:
                    vehicle_search = Vehicle.search([
                            ('plaque', '=', str(items['placa']))
                        ])
                    if vehicle_search:
                        fuel_order.vehicle = Vehicle.browse(vehicle_search)[0]
                    else:
                        self.raise_user_error('no_vehicle',
                        {'plaque': items['placa']})
                else:
                    self.raise_user_error('no_plaque',
                    {'number': str(items['nro_orden'])})
            
            if not self.start.is_machine:
                if fuel_order.vehicle:
                    if fuel_order.vehicle.asset:
                        if fuel_order.vehicle.asset.owners:
                            fuel_order.department = \
                                fuel_order.vehicle.asset.owners[0].employee.\
                                contract_department
            date = None
            time = None
            if type(items['fecha_carga']) == str:
                date = datetime.strptime(items['fecha_carga'], '%d/%m/%Y').date()
                time = datetime.strptime(items['fecha_carga'], '%d/%m/%Y').time()
            else:
                date = items['fecha_carga'].date()
                time = items['fecha_carga'].time()
            if date < self.start.start_date or \
                date > self.start.end_date:
                self.raise_user_error('out_date',
                    {'number': str(items['nro_orden'])})
            fuel_order.date = date
            fuel_order.time = time  # < no info provided
            fuel_order.number = str(items['nro_orden'])
            fuel_order.gallons_number = Decimal(items['galones'])
            fuel_order.fuel_cost = Decimal(items['subtotal'])
            fuel_order.fuel_cost_iva = Decimal(items['total'])
            fuel_order.iva = Decimal(items['iva'])

            # fuel_order.on_change_fuel_cost()

            fuel_order.fuel_cost = "{0:.2f}".format(
                fuel_order.fuel_cost)
            fuel_order.gallons_number = "{0:.2f}".format(
                fuel_order.gallons_number)
            fuel_order.fuel_cost_iva = Decimal("{0:.2f}".format(
                fuel_order.fuel_cost_iva))
            fuel_order.iva = Decimal("{0:.2f}".format(
                fuel_order.iva))
            fuel_order.mileage = items['recorrido']
            fuel_order.usage = float(items['uso'])
            fuel_order.description = "Ordenes de combustible de secuencial" + \
                str(items['fac_secuencial'])
            fuel_order.invoice_sequence = str(items['fac_secuencial'])
            # fuel_order.contract = self.start.contract
            # fuel_order.fuel_station = self.start.fuel_station
            to_save_orders.append(fuel_order)
        FuelOrder.save(to_save_orders)
        return 'succeed'

    
class MaintenanceMultiPayment(Workflow, ModelSQL, ModelView):
    "Maintenance Multi Payment"
    __name__ = 'parking.vehicle.maintenance.multi.payment'

    _states = {
        'readonly':
            ~(Eval('state') == 'draft'),
        }
    _depends = ['state']

    state = fields.Selection([
            ('draft', 'Borrador'),
            ('payments_generated', 'Pagos generados'),
        ], 'Estado', states={'readonly': True})
    description = fields.Text(
        'Descripción del pago', states=_states, depends=_depends)
    date = fields.Date('Fecha', states=_states, depends=_depends)
    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        states={
            'readonly': (Eval('state') != 'draft'),
            'required': True
        },
        domain=[
            ('state', '=', 'open'),
            ('id', 'in', Eval('selectable_contracts'))
        ], depends=['selectable_contracts', 'state'])
    selectable_contracts = fields.Function(fields.One2Many(
            'purchase.contract.process', None, 'Contratos disponibles',
            depends=['workshop'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_contracts')
    maintenances = fields.One2Many('parking.vehicle.maintenance', 
        'multi_payment', 'Mantenimientos', 
        domain=[If(
                Eval('state') == 'draft',
                (
                    ('state', '=', 'finished'),
                    ('contract', '=', Eval('contract')),
                    ('payments_generated', '=', False),
                    ('workshop_type', '=', 'external')
                ),
                ()
            )
        ],
        states=_states, depends=_depends)
    workshop = fields.Many2One('parking.workshop', 'Taller',
        states = {
            'readonly': (Eval('state') != 'draft'),
            'required': True
        }, depends = ['state'])
    related_payment = fields.Many2One(
        'purchase.contract.process.payment',
        'Línea de pago relacionada', states={
            'readonly': True,
            'invisible':  ~(Eval('state') != 'draft')
        }, help='Línea de pago generada desde \
        este multipago')

    
    @classmethod
    def __setup__(cls):
        super(MaintenanceMultiPayment, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'payments_generated'),
            ('payments_generated', 'draft'),
        ))
        cls._buttons.update(
            {
                'generate_payments': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },
                'draft': {
                    'invisible':
                        ~(Eval('state').in_(['payments_generated']))
                },
            })
        cls._error_messages.update({
            'no_payment_related': ('Este multipago no tiene una línea de pago de'
            ' contrato relacionada'),
            'no_invalid_state': ('Para poder pasar a borrador nuevmante la línea'
            ' de pago debe estar en estado "Inválida".'),
            })

    @staticmethod
    def default_state():
        return 'draft'
    
    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('workshop')
    def on_change_with_selectable_contracts(self, name=None):
        ids = []
        if self.workshop:
            for contract in self.workshop.contracts:
                ids.append(contract.id)
        return ids

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, multipayments):
        pool = Pool()
        MaintenancePayment = pool.get('parking.vehicle.maintenance-payment')
        Maintenance = pool.get('parking.vehicle.maintenance')
        for payment in multipayments:
            if payment.related_payment:
                if payment.related_payment.state == 'invalid':
                    for maintenance in payment.maintenances:
                        MaintenancePayment.delete(maintenance.payment_lines)
                        maintenance.payments_generated = False
                        Maintenance.save([maintenance])
                    payment.related_payment = None
                    cls.save([payment])
                else:
                    cls.raise_user_error('no_invalid_state')
            else:
                cls.raise_user_error('no_payment_related')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('payments_generated')
    def generate_payments(cls, multipayments):
        pool = Pool()
        ContractProcessPayment = pool.get('purchase.contract.process.payment')
        ContractProcess = pool.get('purchase.contract.process')
        ContractProduct = pool.get('purchase.contract.process.product')
        PaymentLine =  pool.get('purchase.contract.process.payment.line')
        MaintenancePayment = pool.get('parking.vehicle.maintenance-payment')
        Date = pool.get('ir.date')
        payment_data = dict()
        for multipayment in multipayments:
            new_payment = ContractProcessPayment(
                process = multipayment.contract,
                amount =  0, # <------------------------------cambiar por el total total
                planned_date = Date.today(),
                type = 'payment',
            )
            new_lines = []
            product_ids = [x.product.id for x in multipayment.contract.products]
            for maintenance in multipayment.maintenances:
                if maintenance.separated_invoice:
                    for line in maintenance.external_lines:
                        if line.item.id not in product_ids:
                            new = ContractProduct(
                                process=multipayment.contract,
                                product=line.item
                            )
                            product_ids.append(line.item.id)
                            ContractProduct.save([new])
                            # multipayment.contract.products = \
                            #     multipayment.contract.products + (line.item, )
                        payment_data = cls.compute_payment_data(
                            payment_data, new_payment, line.item,
                            maintenance.budget, line.quantity_to_request, line.price)
                    for line in maintenance.activities_maintenance:
                        if line.price > 0 and line.supplier_item:
                            # TODO Consider line.supplier_item in IF statement
                            if line.item.id not in product_ids:
                                new = ContractProduct(
                                    process=multipayment.contract,
                                    product=line.item
                                )
                                product_ids.append(line.item.id)
                                ContractProduct.save([new])
                            payment_data = cls.compute_payment_data(
                                payment_data, new_payment, line.item,
                                maintenance.budget, line.quantity_to_request, line.price)
                    for line in maintenance.liquid_lines:
                        if line.item.id not in product_ids:
                            # multipayment.contract.products = \
                            #     multipayment.contract.products + (line.item, )
                            new = ContractProduct(
                                process=multipayment.contract,
                                product=line.item
                            )
                            product_ids.append(line.item.id)
                            ContractProduct.save([new])
                        payment_data = cls.compute_payment_data(
                            payment_data, new_payment, line.item,
                            maintenance.budget, line.quantity, line.price)
                else:
                    if maintenance.service_to_pay.id not in \
                            product_ids:
                        new = ContractProduct(
                            process=multipayment.contract,
                            product=maintenance.service_to_pay
                        )
                        product_ids.append(maintenance.service_to_pay.id)
                        ContractProduct.save([new])
                        # multipayment.contract.products = \
                        #     multipayment.contract.products + (
                        #         maintenance.service_to_pay, )
                    payment_data = cls.compute_payment_data(
                        payment_data, new_payment, maintenance.service_to_pay,
                            maintenance.budget, 1, maintenance.total_amount)
            total = Decimal(0.0)
            for item in payment_data:
                new_line = PaymentLine()
                new_line.payment = payment_data[item]["payment"]
                new_line.product = payment_data[item]["item"]
                new_line.budget = payment_data[item]["budget"]
                new_line.amount = payment_data[item]["amount"]
                new_line.unit_price = payment_data[item]["unit_price"].quantize(
                    Decimal('0.01'))
                new_lines.append(new_line)
                total += (payment_data[item]["amount"] * payment_data[item]["unit_price"]) 
            new_payment.amount = total.quantize(Decimal('0.01'))
            ContractProcess.save([multipayment.contract])
            ContractProcessPayment.save([new_payment])
            multipayment.related_payment = new_payment
            cls.save([multipayment])
            # necesary double save
            ContractProcess.save([multipayment.contract])
            PaymentLine.save(new_lines)
            for maintenance in multipayment.maintenances:
                maintenance_payment = MaintenancePayment()
                maintenance_payment.maintenance = maintenance.id
                maintenance_payment.payment = new_payment.id
                maintenance_payment.save()
                maintenance.payments_generated = True
                maintenance.save()
 
    def compute_payment_data(payment_data, payment, item, budget, quantity, price):
        key = str(item.id) + '-' + budget.code + '-' + str(price)
        if key in payment_data.keys():
            payment_data[key]["amount"] = quantity + payment_data[key]["amount"]
        else:
            payment_data[key] = dict()
            payment_data[key]["payment"] = payment
            payment_data[key]["item"] = item
            payment_data[key]["budget"] = budget
            payment_data[key]["amount"] = quantity
            payment_data[key]["unit_price"] = price
            
        return payment_data
        # new_line = PaymentLine()
        # new_line.payment = new_payment
        # new_line.product = line.item
        # new_line.budget = maintenance.budget
        # new_line.amount = line.quantity_to_request
        # new_line.unit_price = line.price


class ContractBudgetSummary(ModelSQL, ModelView):
    'Contract Budget Summary'
    __name__ = 'parking.contract.budget.summary'

    budget = fields.Many2One('public.budget', 'Partida',
        readonly=True)
    actual_value = fields.Numeric('En este mantenimiento', digits=(16, 2), 
        readonly=True)
    total_used = fields.Numeric('Total pagados (TP)', digits=(16, 2), 
        readonly=True, help="Mantenimientos finalizados y pagados")
    total_pendant = fields.Numeric('Total pendiente (P)', digits=(16, 2), 
        readonly=True, help="Mantenimientos en estado: Borrador, Enviados"
        ", Ingresando, y Finalizados que aun no hayan sido pagados")
    available_amount = fields.Numeric('Monto de partida (M)', digits=(16, 2), 
        readonly=True)
    balance = fields.Numeric('Saldo (M - TP)', digits=(16, 2), 
        readonly=True)
    balance_plannified = fields.Numeric('Saldo planificado (M - (TP + P))', digits=(16, 2), 
        readonly=True)

    @classmethod
    def table_query(cls):
        context = Transaction().context
        contract_id = context.get('contract')
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Contract = pool.get('purchase.contract.process')
        contract = Contract.__table__()
        Certificate = pool.get('public.budget.certificate')
        certificate = Certificate.__table__()
        CertificateLine = pool.get('public.budget.certificate.line')
        certificate_line = CertificateLine.__table__()
        CertCard = pool.get('public.budget.certificate.line.card')
        cert_query = CertCard.__table__()
        TDRBudget = pool.get('purchase.contract.process.tdr.budget')
        tdr_budget = TDRBudget.__table__()
        Budget = pool.get('public.budget')
        budget = Budget.__table__()
        Payment = pool.get('purchase.contract.process.payment')
        payment = Payment.__table__()
        PaymentLine = pool.get('purchase.contract.process.payment.line')
        payment_line = PaymentLine.__table__()

        where_paid = (maintenance.payments_generated == True) & (
            contract.id == contract_id)

        where_pendant= (maintenance.state != 'deny') & (
            maintenance.state != 'cancel') & (
            contract.id == contract_id) & (
            maintenance.payments_generated == False)


        # GET CONTRACT BUDGET VALUES ALREADY USED
        query_pendant = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            Literal(0).as_('actual_value'),
            Literal(0).as_('total_used'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_activities_estimated +
             maintenance.total_liquids + maintenance.tax_amount).as_(
                'total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_pendant
        )

        # GET CONTRACT BUDGET VALUES ALREADY USED
        query_paid = maintenance.join(contract,
            condition=maintenance.contract == contract.id
        ).join(budget,
            condition=maintenance.budget == budget.id
        ).select(
            budget.id.as_('budget'),
            Literal(0).as_('actual_value'),
            (maintenance.total_activities +
             maintenance.total_external_spares + 
             maintenance.total_liquids + maintenance.tax_amount).as_(
                'total_used'),
            Literal(0).as_('total_pendant'),
            Literal(0).as_('available_amount'),
            where=where_paid
        )

        # GET TDR_BUDGET MODIFICATIONS ASSIGNED VALUES
        where_contract = (contract.id == contract_id)

        line_ids = contract.join(certificate, type_='LEFT',
            condition=contract.tdr_certificate == certificate.id
        ).join(certificate_line, type_='LEFT',
            condition=certificate.id == certificate_line.certificate
        ).select(
            certificate_line.id.as_('id'),
            where=where_contract
        )
        queryCert = cert_query.select(
                cert_query.budget.as_('budget'),
                Literal(0).as_('actual_value'),
                Literal(0).as_('total_used'),
                Literal(0).as_('total_pendant'),
                cert_query.modified_amount.as_('available_amount'),
                where=cert_query.id.in_(line_ids))

        # GET TDR_BUDGET ASSIGNED VALUES
        query_tdr = contract.join(certificate, type_='LEFT',
            condition=contract.tdr_certificate == certificate.id
        ).join(certificate_line, type_='LEFT',
            condition=certificate.id == certificate_line.certificate
        ).select(
            certificate_line.budget.as_('budget'),
            Literal(0).as_('actual_value'),
            Literal(0).as_('total_used'),
            Literal(0).as_('total_pendant'),
            certificate_line.amount.as_('available_amount'),
            where=where_contract
        )

        # GET ContractProcessPaymentLine USED VALUES
        where_contract = (contract.id == contract_id)

        # query_payment = contract.join(payment,
        #     condition=payment.process == contract.id
        # ).join(payment_line,
        #     condition=payment_line.payment == payment.id
        # ).select(
        #     payment_line.budget.as_('budget'),
        #     Literal(0).as_('actual_value'),
        #     (payment_line.amount * payment_line.unit_price).as_('total_used'),
        #     Literal(0).as_('available_amount'),
        #     where=where_contract
        # )
        query_final = Union(query_pendant, query_tdr, all_=True)
        query_final = Union(query_final, query_paid, all_=True)
        # query_final = Union(query_final, query_payment, all_=True)
        query_final = Union(query_final, queryCert, all_=True)
        query_final = query_final.select(
            query_final.budget,
            Sum(query_final.actual_value).as_('actual_value'),
            Sum(query_final.total_used).as_('total_used'),
            Sum(query_final.total_pendant).as_('total_pendant'),
            Sum(query_final.available_amount).as_('available_amount'),
            group_by=[query_final.budget]
        )

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_final.budget,
            query_final.actual_value,
            query_final.total_used,
            query_final.total_pendant,
            query_final.available_amount,
            (query_final.available_amount - (query_final.actual_value + \
                query_final.total_used)).as_('balance'),
            (query_final.available_amount - (query_final.actual_value + \
                query_final.total_used + query_final.total_pendant)
                ).as_('balance_plannified')
        )


class ContractBudgetSummaryContext(ModelView):
    'Contract Budget Summary Context'
    __name__ = 'parking.contract.budget.summary.context'

    workshop = fields.Many2One('parking.workshop', 'Taller')
    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        domain=[
            ('state', '=', 'open'),
            ('id', 'in', Eval('selectable_contracts'))
        ], depends=['selectable_contracts'])
    selectable_contracts = fields.Function(fields.One2Many(
            'purchase.contract.process', None, 'Contratos disponibles',
            depends=['workshop'],
            states={
                'invisible': True
            }
        ), 'on_change_with_selectable_contracts')

    @fields.depends('workshop')
    def on_change_with_selectable_contracts(self, name=None):
        ids = []
        if self.workshop:
            for contract in self.workshop.contracts:
                ids.append(contract.id)
        return ids


class InfiniteAmountSummaryContext(ModelView):
    'Infinite Amounts Summary Context'
    __name__ = 'parking.infinite.amounts.summary.context'

    workshop = fields.Many2One('parking.workshop', 'Taller')
    infinite_amounts = fields.Many2One('purchase.purchase', 'Infimas Cuantías',
        domain=[
            ('state', '=', 'done'),
            ('id', 'in', Eval('selectable_infinite_amounts'))
        ],
        depends = ['selectable_infinite_amounts']
    )

    selectable_infinite_amounts = fields.Function(fields.One2Many(
        'purchase.purchase', None, 'Infimas cuantías',
        depends=['workshop'],
        states={
            'invisible': True
        }
    ), 'on_change_with_selectable_infinite_amounts')

    @fields.depends('workshop')
    def on_change_with_selectable_infinite_amounts(self, name=None):
        ids = []
        if self.workshop:
            for infina in self.workshop.infinite_amounts:
                ids.append(infina.id)
        return ids


class InfiniteAmountSummary(ModelSQL, ModelView):
    'Infinite Amounts Summary'
    __name__ = 'parking.infinite.amounts.summary'

    infinite_amounts = fields.Many2One('purchase.purchase', 'Infima Cuantia',
        readonly=True)
    total_used = fields.Numeric('Total usado', digits=(16, 2),
        readonly=True)
    available_amount = fields.Numeric('Monto Inicial', digits=(16, 2),
        readonly=True)
    balance = fields.Numeric('Saldo', digits=(16, 2),
        readonly=True)

    @classmethod
    def table_query(cls):
        context = Transaction().context


        infinite_amount_id = context.get('infinite_amounts')
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        InfiniteAmounts = pool.get('purchase.purchase')
        infiniteAmounts = InfiniteAmounts.__table__()

        where_no_draft = ( maintenance.state != 'draft') & (
            maintenance.state != 'cancel') & (
            infiniteAmounts.id == infinite_amount_id)

        query_used = maintenance.join(infiniteAmounts,
            condition=maintenance.infinite_amounts == infiniteAmounts.id
        ).select(
            infiniteAmounts.id.as_('infinite_amounts'),
            infiniteAmounts.total_amount_cache.as_('available_amount'),
            (maintenance.total_activities +
             maintenance.total_external_spares +
             maintenance.total_liquids + maintenance.tax_amount).as_(
                'total_used'),
            where=where_no_draft
        )

        query_final = query_used
        query_final = query_final.select(
            query_final.infinite_amounts,
            Sum(query_final.total_used).as_('total_used'),
            query_final.available_amount.as_('available_amount'),
            group_by=[query_final.infinite_amounts,query_final.available_amount]
        )

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_final.infinite_amounts,
            query_final.total_used,
            query_final.available_amount,
            (query_final.available_amount - (
                query_final.total_used)).as_('balance')
        )


class Tires(metaclass=PoolMeta):
    "Tires"
    __name__ = 'parking_place.tires'

    price = fields.Function(fields.Float('Precio', readonly=True),
        'on_change_with_price')
    code_asset = fields.Function(fields.Char('Código del Activo',
        readonly=True), 'on_change_with_code')
    code_emac = fields.Char('Código Emac',readonly=True)
    order_work = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento',domain=[('state', '=', 'entering')], required=True)
    product = fields.Many2One('product.product', 'Neumático')
    quantity = fields.Function(fields.Numeric('Cantidad'),
        'on_change_with_quantity')
    lines = fields.Many2One('parking.vehicle.maintenance.line','Neumaticos')
    related_requirement = fields.Many2One(
        'purchase.requirement',
        'Solicitud de despacho', states={
            'readonly': True
        })
    location = fields.Function(fields.Char('Ubicacion',  readonly=True),
         'on_change_with_location')
    state_tire = fields.Selection(
        [
            (None,''),
            ('new', 'Nueva'),
            ('tire_retreat', 'Reencauchada'),
        ],'Estado del neumatico'
    )

    def get_rec_name(self, name):
        return ' # %s [%s] (%s)' % (
            str(self.code_emac),
            self.mark.name, self.desing.name,
        )

    @fields.depends('product')
    def on_change_with_price(self, name=None):
        if self.product:
            if self.product.cost_price:
                return self.product.cost_price

    @fields.depends('product')
    def on_change_with_code(self, name=None):
        if self.product:
            if self.product.code:
                return self.product.code

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('code_emac',) + tuple(clause[1:]),
                  ('mark',) + tuple(clause[1:]),
                  ('desing',) + tuple(clause[1:]),
                  ('state',) + tuple(clause[1:]),
                  ]
        return domain

    @fields.depends('order_work')
    def on_change_with_quantity(self, name=None):
        if self.order_work:
            if self.order_work.lines:
                for line in self.order_work.lines:
                    aux = line.quantity_to_request
                    return aux

    @fields.depends('order_work')
    def on_change_order_work(self, name=None):
        if self.order_work:
            if self.order_work.lines:
                for line in self.order_work.lines:
                    self.product = line.spare.id

    @fields.depends('state','order_work')
    def on_change_with_location(self, name=None):
        pool = Pool()
        Config = pool.get('parking.place.configuration')
        available_location = Config(1).get_multivalue('from_location_')
        retread_location = Config(1).get_multivalue('to_location')
        if self.state:
            if self.state== 'retread':
                return available_location.name
            if self.state == 'available':
                return available_location.name
            if self.state== 'mount':
                return self.order_work.vehicle.rec_name
            if self.state== 'disassembly':
                return available_location.name

    @classmethod
    def validate(cls, tires):
        super(Tires, cls).validate(tires)
        cls.set_number(tires)

    @classmethod
    def set_number(cls, tires):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.code_emac_sequence:
            cls.raise_user_error('no_sequence')
        for tire in tires:
            if tire.code_emac:
                continue
            tire.code_emac = Sequence.get_id(
                config.code_emac_sequence.id)
        cls.save(tires)


class CreateTires(ModelSQL, ModelView):
    'Create Tires'
    __name__ = 'parking.place.create.tires.'
    tires = fields.Many2One('parking_place.tires', 'Neumático')
    order_work = fields.Many2One('parking.vehicle.maintenance',
         'Mantenimiento',
         domain=[('state', '=', 'entering')])
    quantity = fields.Function(fields.Numeric('Cantidad'),
                               'on_change_with_quantity')

    @classmethod
    def __setup__(cls):
        super(CreateTires, cls).__setup__()
        cls._order = [
            ('id', 'DESC')
        ]

    @fields.depends('order_work')
    def on_change_with_quantity(self, name=None):
        if self.order_work:
            if self.order_work.lines:
                for line in self.order_work.lines:
                    aux = line.quantity_to_request
                    return aux


class CreateTiresStart(ModelView):
    'Create Tires Start'
    __name__ = 'parking.place.create.tires.start'

    order_work = fields.Many2One('parking.vehicle.maintenance',
         'Mantenimiento',
         domain=[('state', '=', 'entering')])

    quantity = fields.Function(fields.Numeric('Cantidad'),
                               'on_change_with_quantity')
    @fields.depends('order_work')
    def on_change_with_quantity(self, name=None):
        if self.order_work:
            if self.order_work.lines:
                for line in self.order_work.lines:
                    aux = line.quantity_to_request
                    return aux


class CreateTiresWizard(Wizard):
    'Create Tires Wizard'
    __name__ = 'parking.place.create.tires.wizard'

    start = StateView('parking.place.create.tires.start',
                      'parking_public_pac.create_tires_view_form_2', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('OK', 'change', 'tryton-ok', default=True),
                      ])
    change = StateTransition()

    def transition_change(self):
        pool = Pool()
        Tires = pool.get('parking_place.tires')
        Mark = pool.get('parking_place.mark_tires')
        tires = Tires(Transaction().context['active_id'])
        number = self.start.quantity -1

        if(number):
            if (tires):
                to_save = []
                for i in range(int(number)):
                    tire = Tires()
                    tire.order_work= tires.order_work
                    tire.product = tires.product
                    tire.price = tires.price
                    tire.mark = tires.mark
                    tire.desing = tires.desing
                    tire.measurement = tires.measurement
                    tire.depth = tires.depth
                    tire.pressure = tires.pressure
                    tire.state_tire= tires.state_tire
                    to_save.append(tire)
        Tires.save(to_save)
        return 'end'


class TiresRetread(metaclass=PoolMeta):
    "Tires Retread"
    __name__ = 'parking.tires.retread'
    contract = fields.Many2One('purchase.contract.process', 'Contrato',
        states={
        'readonly': (Eval('state') != 'draft')},
        domain=[
        ('state', '=', 'open'),
        #('id', 'in', Eval('selectable_contracts'))
        ],
        # depends=[ 'selectable_contracts',
        # 'state']
        )

    infinite_amounts = fields.Many2One('purchase.purchase', 'Infima Cuantía',
        states={
           'readonly': (
                   Eval('state') != 'draft'),
        },
        domain=[
           ('state', '=', 'done'),
          # ('id', 'in', Eval('selectable_infinite_amounts'))
        ],
                   #  depends=['workshop_type',
                   # 'selectable_infinite_amounts',
                   # 'state']
        )


class DisassemblyTires(metaclass=PoolMeta):
    'Disassembly Tires'
    __name__ = 'parking_place.disassembly_tires'



    @classmethod
    def __setup__(cls):
        super(DisassemblyTires, cls).__setup__()

    @classmethod
    @ModelView.button
    @Workflow.transition('sent')
    def sent(cls, disassemblies):
        super(DisassemblyTires, cls).sent(disassemblies)
        for disassembly in disassemblies:
            cls.send_storage_tires(disassembly)

    @classmethod
    def send_storage_tires(cls, disassembly):
        pool = Pool()
        Config = pool.get('parking.place.configuration')
        DisassemblyTires = pool.get('parking_place.disassembly_tires')
        to_expenses_location_id = Config(1).get_multivalue('from_location_').id
        reason_id = Config(1).get_multivalue('shipment_reason').id
        from_reentry_location_id = Config(1).get_multivalue(
        'to_location').id
        ShipmentInternal = pool.get('stock.shipment.internal')
        ShipmentReason = pool.get('stock.shipment.reason')
        StockLocation = pool.get('stock.location')
        ProductTemplate = pool.get('product.template')
        Product = pool.get('product.product')
        StockMove = pool.get('stock.move')
        stock_date_end = date.today()
        shipment = ShipmentInternal()
        shipment.planned_date = disassembly.date
        shipment.planned_start_date = disassembly.date
        shipment.company = disassembly.order_work.company
        to_location = \
            StockLocation.browse([to_expenses_location_id])[0]
        shipment.to_location = to_location
        shipment.reference = disassembly.order_work.description
        reason = ShipmentReason.browse([reason_id])[0]
        shipment.reason = reason
        shipment.on_change_with_reason_categories()
        from_location_ = \
            StockLocation.browse([from_reentry_location_id])[0]
        shipment.from_location = from_location_
        # Setting employee
        User = pool.get('res.user')
        user = User(Transaction().user)
        employee = user.employee
        shipment.employee = employee
        shipment.on_change_employee()
        shipment.save()
        moves = []

        for line in disassembly.disassembly_lines_tires:
            if line.tires:

                with Transaction().set_context(
                    locations=[from_reentry_location_id],
                    stock_date_end=stock_date_end):
                    stock_in_location = Product.products_by_location(
                        [from_reentry_location_id],
                        grouping=('product',),
                        grouping_filter=([line.tires.product.id],)
                    )
                stock = [
                    value for key, value in stock_in_location.items()
                ][0]
                if stock > 0:
                    move = StockMove()
                    move.shipment = shipment
                    move.product = line.tires.product
                    move.uom = line.tires.product.template.default_uom
                    move.quantity = 1
                    move.from_location = from_location_
                    move.to_location = to_location
                    move.company = disassembly.order_work.company
                    move.planned_date = disassembly.date
                    move.department = shipment.department
                    moves.append(move)
                else:
                    cls.raise_user_error('no_quantity_request')
        StockMove.save(moves)
        ShipmentInternal.draft([shipment])
        ShipmentInternal.wait([shipment])
        ShipmentInternal.assign([shipment])
        ShipmentInternal.ship([shipment])
        ShipmentInternal.done([shipment])
        disassembly.related_shipment = shipment
        DisassemblyTires.save([disassembly])


class RequirementQuery(ModelSQL, ModelView):
    'Requirement Query'
    __name__ = 'parking.requirement.query'

    maintenance = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento', readonly=True)
    maintenance_date = fields.Date('F. mantenimiento', readonly=True)
    requirement = fields.Many2One('purchase.requirement', 
        'Solicitud de despacho', readonly=True)
    requirement_date = fields.Date('F. solicitud de despacho', readonly=True)
    material =  fields.Many2One('purchase.material.requirement',
        'Despacho de solicitud', readonly=True)
    material_date = fields.Date('F. despacho de solicitud', readonly=True)
    purchase = fields.Many2One('purchase.purchase', 'Compra',
        readonly=True)
    purchase_date = fields.Date('F. aprobación de compra', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Requirement = pool.get('purchase.requirement')
        requirement = Requirement.__table__()
        RequirementLine = pool.get('purchase.requirement.line')
        requirement_line = RequirementLine.__table__()
        MaterialLine = pool.get('purchase.material.requirement.line')
        material_line = MaterialLine.__table__()
        MaterialRequirement = pool.get('purchase.material.requirement')
        material_requirement = MaterialRequirement.__table__()
        ConsolidationDetail = pool.get('purchase.consolidation.detail')
        consolidation_detail = ConsolidationDetail.__table__()
        ConsolidationLineDetail = pool.get('purchase.consolidation.line.detail')
        consolidation_line_detail = ConsolidationLineDetail.__table__()
        ConsolidationLine = pool.get('purchase.consolidation.line')
        consolidation_line = ConsolidationLine.__table__()
        Consolidation = pool.get('purchase.consolidation')
        consolidation = Consolidation.__table__()
        Request = pool.get('purchase.request')
        request = Request.__table__()
        PurchaseLine = pool.get('purchase.line')
        purchase_line = PurchaseLine.__table__()
        Purchase = pool.get('purchase.purchase')
        purchase = Purchase.__table__()

        query = maintenance.join(
            requirement,
            condition=requirement.id == maintenance.related_requirement
        ).join(
            requirement_line, type_='LEFT',
            condition=requirement_line.requirements == requirement.id
        ).join(
            material_line, type_='LEFT',
            condition=material_line.requirement_line_origin == requirement_line.id
        ).join(
            material_requirement, type_='LEFT',
            condition=material_requirement.id == material_line.requirement
        ).join(
            consolidation_detail, type_='LEFT',
            condition=consolidation_detail.requirement_material_line == material_line.id #noqa
        ).join(
            consolidation_line_detail, type_='LEFT',
            condition=consolidation_line_detail.detail == consolidation_detail.id #noqa
        ).join(
            consolidation_line, type_='LEFT',
            condition=consolidation_line.id == consolidation_line_detail.line
        ).join(
            consolidation, type_='LEFT',
            condition=consolidation.id == consolidation_line.purchase_consolidation #noqa
        ).join(
            request, type_='LEFT',
            condition=request.consolidation_origin == consolidation_line.id
        ).join(
            purchase_line, type_='LEFT',
            condition=purchase_line.id == request.purchase_line
        ).join(
            purchase, type_='LEFT',
            condition=purchase.id == purchase_line.purchase
        ).select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            maintenance.id.as_('maintenance'),
            maintenance.start_date.as_('maintenance_date'),
            requirement.id.as_('requirement'),
            requirement.date.as_('requirement_date'),
            material_requirement.id.as_('material'),
            material_requirement.date.as_('material_date'),
            purchase.id.as_('purchase'),
            purchase.done_date.as_('purchase_date'),
            where = (Literal(True))
        )
        # import pdb; pdb.set_trace()
        return query

    @classmethod
    def __setup__(cls):
        super(RequirementQuery, cls).__setup__()
        cls._order = [
            ('maintenance_date', 'DESC')
        ]


# class WorkshopQueryContext(ModelView):
#     'Workshop Query Context'
#     __name__ = 'parking.workshop.query.context'

#     start_date = fields.Date('Fecha inicio')
#     end_date = fields.Date('Fecha fin')
#     vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')
#     workshop = fields.Many2One('parking.workshop', 'Taller')