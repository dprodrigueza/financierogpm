# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import parking_place
from . import configuration
from . import stock
from . import report

__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        configuration.ConfigurationLocation,
        parking_place.Vehicle,
        parking_place.Workshop,
        parking_place.WorkshopContract,
        parking_place.FuelStation,
        parking_place.FuelStationContract,
        parking_place.MaintenanceBudgetSummary,
        parking_place.Maintenance,
        parking_place.MaintenanceLine,
        parking_place.MaintenancePaymentLine,
        parking_place.MaintenanceLineLiquid,
        parking_place.MaintenancePendingLine,
        parking_place.MaintenancePreventive,
        parking_place.FuelOrder,
        parking_place.FuelType,
        parking_place.FuelOrderPayment,
        parking_place.FuelOrderPaymentLineQuery,
        parking_place.FuelOrderPaymentLine,
        parking_place.PaymentFuelOrdersPaymentStart,
        parking_place.UploadFuelOrdersSucceed,
        parking_place.UploadFuelOrdersStart,
        parking_place.MaintenanceMultiPayment,
        parking_place.ContractBudgetSummary,
        parking_place.ContractBudgetSummaryContext,
        #parking_place.MaintenanceInfiniteAmountSumary,
        parking_place.InfiniteAmountSummaryContext,
        parking_place.InfiniteAmountSummary,
        parking_place.WorkshopInfiniteAmounts,
        parking_place.TiresRetread,
        parking_place.Tires,
        parking_place.CreateTiresStart,
        parking_place.CreateTires,
        parking_place.DisassemblyTires,
        parking_place.RequirementQuery,
        stock.ShipmentIn,
        stock.ShipmentInPendingProduct,
        stock.ShipmentInternal,

        module='parking_public_pac', type_='model')
    Pool.register(
        parking_place.UpdateVehicleMileage,
        parking_place.PaymentFuelOrdersPayment,
        parking_place.UploadFuelOrders,
        parking_place.CreateTires,
        parking_place.CreateTiresWizard,
        module='parking_public_pac', type_='wizard')
    Pool.register(
        report.FuelOrderPaymentConsolidated,
        report.WorkshopReport,
        report.WorkshopPurchaseReport,
        module='parking_public_pac', type_='report')
