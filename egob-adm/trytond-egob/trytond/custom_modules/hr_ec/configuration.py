from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['Configuration']


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Human Resources Configuration'
    __name__ = 'hr_ec.configuration'
    hr_leader = fields.Many2One('company.employee', 'Human Resources Leader')
    holiday_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Holiday Sequence',
        domain=[
            ('code', '=', 'holiday.holiday'),
        ]))
    overtime_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Overtime Sequence',
        domain=[
            ('code', '=', 'overtime.overtime'),
        ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['holiday_sequence', 'overtime_sequence']:
            return pool.get('hr_ec.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_holiday_sequence(cls, **pattern):
        return cls.multivalue_model(
            'holiday_sequence').default_holiday_sequence()

    @classmethod
    def default_overtime_sequence(cls, **pattern):
        return cls.multivalue_model(
            'overtime_sequence').default_overtime_sequence()


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    "Holiday Configuration Sequence"
    __name__ = 'holiday.configuration.sequence'
    holiday_sequence = fields.Many2One(
        'ir.sequence', "Holiday Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'holiday.holiday'),
            ],
        depends=['company'])
    overtime_sequence = fields.Many2One(
        'ir.sequence', "Overtime Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'overtime.overtime'),
            ],
        depends=['company'])

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        exist = TableHandler.table_exist(cls._table)

        super(ConfigurationSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('holiday_sequence')
        value_names.append('holiday_sequence')
        field_names.append('overtime_sequence')
        value_names.append('overtime_sequence')
        fields.append('company')
        migrate_property(
            'holiday.configuration', field_names, cls, value_names,
            fields=fields)

    @classmethod
    def default_holiday_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('holiday', 'sequence_holiday')
        except KeyError:
            return None

    @classmethod
    def default_overtime_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('overtime', 'sequence_overtime')
        except KeyError:
            return None
