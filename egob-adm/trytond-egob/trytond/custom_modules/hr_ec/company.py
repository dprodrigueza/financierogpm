from sql.aggregate import Max

from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pool import PoolMeta
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, If, Bool
from trytond.modules.company import CompanyReport

__all__ = ['Employee', 'Department', 'Position', 'ContractType',
    'OccupationalGroup', 'WorkRelationship', 'Contract',
    'WorkCertificateReport', 'EmployeesReport', 'DepartmentsReport',
    'DepartmentsReportChart']


class Employee:
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'
    active = fields.Boolean('Active', select=True)
    department = fields.Many2One(
        'company.department', 'Department', required=True)
    position = fields.Many2One('company.position', 'Position', required=True)
    identifier = fields.Function(fields.Char('Identifier'),
        'get_identifier', searcher='search_identifier')
    birthdate = fields.Date('birthdate')
    gender = fields.Char('Gender')
    civil_state = fields.Char('Civil state')
    nationality = fields.Char('Nationality')
    blood_type = fields.Char('Blood type')
    handicapped = fields.Boolean('Handicapped', select=True)
    mobile = fields.Function(fields.Char('Mobile'), 'get_mechanism')
    email = fields.Function(fields.Char('E-Mail'), 'get_mechanism')
    personal_email = fields.Function(
        fields.Char('Personal E-Mail'), 'get_mechanism')
    emergency_call = fields.Char('Emergency call')
    contract = fields.Function(fields.Many2One('company.contract', 'Contract'),
        'get_contract', searcher='search_contract')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

        cls.active.states['readonly'] = True

        t = cls.__table__()
        cls._sql_constraints += [
            ('party_uniq', Unique(t, t.party),
                'There should be only one '
                'party by employee.'),
            ]

        cls._buttons.update({
            'activate': {'invisible': Bool(Eval('active')), },
            'deactivate': {'invisible': ~Bool(Eval('active')), },
        })

    @classmethod
    def default_active(cls):
        return True

    def get_identifier(self, name):
        for identifier in self.party.identifiers:
            if identifier.type == 'ec_ci':
                return identifier.code
        return ''

    @classmethod
    def search_identifier(cls, name, domain):
        return [
            ('party.identifiers.code',) + tuple(domain[1:]),
        ]

    def get_mechanism(self, name):
        return getattr(self.party, name)

    @classmethod
    def get_contract(cls, employees, names):
        pool = Pool()
        companyContract = pool.get('company.contract')

        contracts = {}
        for employee in employees:
            contracts[employee.id] = None
            domain = [('employee', '=', employee.id)]
            result = companyContract.search(
                domain, order=[('start_date', 'DESC')], limit=1)
            if result:
                contracts[employee.id] = result.pop().id
        results = {
            'contract': contracts,
            }
        return results

    @classmethod
    def search_contract(cls, name, clause):
        name, operator_, value = clause
        if name.startswith('contract_') or name.startswith('contract.'):
            name = name[9:]
        Contract = Pool().get('company.contract')
        contract = Contract.__table__()
        employee = cls.__table__()
        operator = fields.SQL_OPERATORS[operator_]
        query_contract_max = contract.select(
            contract.employee.as_('employee_max'),
            Max(contract.start_date).as_('start_date_max'),
            where=contract.state != 'cancel',
            group_by=contract.employee)
        query_contract_data = query_contract_max.join(contract,
            condition=(query_contract_max.employee_max == contract.employee) &
            (query_contract_max.start_date_max == contract.start_date)) \
            .select()
        contract_column = getattr(query_contract_data, name)
        query = employee \
            .join(query_contract_data, 'LEFT',
                  condition=query_contract_data.employee == employee.id) \
            .select(employee.id,
                    where=operator(contract_column, value)
            )
        return [('id', 'in', query)]

    @classmethod
    @ModelView.button
    def activate(cls, employees):
        for employee in employees:
            employee.active = True
        cls.save(employees)

    @classmethod
    @ModelView.button
    def deactivate(cls, employees):
        for employee in employees:
            employee.active = False
        cls.save(employees)


class EmployeesReport(CompanyReport):
    __name__ = 'company.employee.list.report'


class Department(ModelSQL, ModelView):
    'Department'
    __name__ = 'company.department'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active', select=True)
    company = fields.Many2One('company.company', 'Company', required=True,
            ondelete="RESTRICT")
    parent = fields.Many2One('company.department', 'Parent', select=True,
        left='left', right='right', ondelete="RESTRICT",
        domain=[('company', '=', Eval('company'))],
        depends=['company'])
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    childs = fields.One2Many('company.department', 'parent', 'Children')
    leader = fields.Many2One('company.employee', 'Leader')

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0


class DepartmentsReport(CompanyReport):
    __name__ = 'company.department.list.report'


class DepartmentsReportChart(CompanyReport):
    # departments_chart.odt file support up 5 level for childs
    __name__ = 'company.department.list.chart.report'


class Position(ModelSQL, ModelView):
    'Position'
    __name__ = 'company.position'
    name = fields.Char('Name', required=True)


class ContractType(ModelSQL, ModelView):
    'Contract - Type'
    __name__ = 'company.contract_type'
    name = fields.Char('Name', required=True)


class OccupationalGroup(ModelSQL, ModelView):
    'Occupational Group'
    __name__ = 'company.occupational_group'
    name = fields.Char('Name', required=True)


class WorkRelationship(ModelSQL, ModelView):
    'Work Relationship'
    __name__ = 'company.work_relationship'
    name = fields.Char('Name', required=True)


class Contract(Workflow, ModelSQL, ModelView):
    'Contract'
    __name__ = 'company.contract'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done'),
        ('expired', 'Expired'),
        ('cancel', 'Cancel')], 'State',
        readonly=True, required=True, select=True)
    state_string = state.translated('state')
    number = fields.Char('Contract Number', required=True)
    employee = fields.Many2One('company.employee', 'Employee', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    salary = fields.Numeric('Salary',
        digits=(16, Eval('company.currency.digits', 2)), required=True)
    work_relationship = fields.Many2One(
        'company.work_relationship', 'Work Relationship', required=True)
    contract_type = fields.Many2One(
        'company.contract_type', 'Contract Type', required=True)
    occupational_group = fields.Many2One(
        'company.occupational_group', 'Occupational Group')
    department = fields.Many2One(
        'company.department', 'Department', required=True)
    position = fields.Many2One('company.position', 'Position', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date')
    parent = fields.Many2One('company.contract', 'Parent',
        domain=[
            ('employee', '=', Eval('employee')),
            ('id', If(Eval('id', -1) != -1, '=', '!='), Eval('id', -1))])

    @classmethod
    def __setup__(cls):
        super(Contract, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
            ('done', 'expired'),
            ('done', 'cancel'),
        ))
        cls._buttons.update(
            {
                'done': {
                    'invisible': Eval('state').in_(['done', 'cancel']),
                },
                'cancel': {
                    'invisible': Eval('state').in_(['draft', 'cancel']),
                },
            })
        cls._error_messages.update({
            'other_contract_in_done': (
                'Employee %(name)s has other contract in done state'),
        })

    def get_rec_name(self, name):
        return ("%s - %s - %s" % (
            self.number, self.employee.party.name, self.state_string.upper()))

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_expired(cls):
        return False

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_start_date():
        pool = Pool()
        date = pool.get('ir.date')
        return date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, contracts):
        for contract in contracts:
            contracts_match = Contract.search(
                [('employee.id', '=', contract.employee.id),
                 ('state', '=', 'done')])
            if contracts_match:
                cls.raise_user_error('other_contract_in_done', {
                    'name': contract.employee.party.name,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, contracts):
        pass


class WorkCertificateReport(CompanyReport):
    __name__ = 'hr_ec.work_certificate.report'

    @classmethod
    def get_context(cls, records, data):
        Config = Pool().get('hr_ec.configuration')
        config = Config(1)

        report_context = super(WorkCertificateReport, cls).get_context(
            records, data)
        report_context['hr_leader'] = config.hr_leader.party.name + " - " + \
            config.hr_leader.position.name
        return report_context
