from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['ContactMechanism']


class ContactMechanism:
    __metaclass__ = PoolMeta
    __name__ = 'party.contact_mechanism'

    personal_email = fields.Function(fields.Char('Personal E-Mail',
        states={
            'invisible': Eval('type') != 'email',
            'required': Eval('type') == 'email',
            'readonly': ~Eval('active', True),
        }, depends=['value', 'type', 'active']),
        'get_value', setter='set_value')

    @classmethod
    def __setup__(cls):
        super(ContactMechanism, cls).__setup__()
        new_type = ('personal_email', 'Personal E-Mail')
        if new_type not in cls.type.selection:
            cls.type.selection.append(new_type)
