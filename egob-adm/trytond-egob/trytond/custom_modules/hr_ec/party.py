from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Party']


class Party:
    __metaclass__ = PoolMeta
    __name__ = 'party.party'

    personal_email = fields.Function(fields.Char('E-Mail'), 'get_mechanism')
