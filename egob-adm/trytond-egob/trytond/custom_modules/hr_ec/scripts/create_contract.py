from proteus import config, Model
from datetime import datetime
import xlrd
import pdb
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

def create_parties(host, port, database, username, password, filename):
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(2)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    Employee = Model.get('company.employee')
    WorkRelationship = Model.get('company.work_relationship')
    ContractType = Model.get('company.contract_type')
    Department = Model.get('company.department')
    Position = Model.get('company.position')
    Contract = Model.get('company.contract')
    Company = Model.get('company.company')

    company, = Company.find([('party.name', 'ilike', 'GAD Pueblo Viejo')])

    pdb.set_trace()

    for row in range(sheet.nrows)[1:]:
        #pdb.set_trace()
        cont += 1
        name = sheet.cell(row, 2).value.strip()
        identifier = sheet.cell(row, 4).value.strip()
        position_name = sheet.cell(row, 5).value.strip()
        tipo_de_contrato = sheet.cell(row, 6).value.strip()
        relacion_de_trabajo = sheet.cell(row, 7).value.strip()
        department_name = sheet.cell(row, 18).value.strip()

        salary = sheet.cell(row, 11).value

        contract = Contract()

        contract.number = str(identifier)
        employee, = Employee.find([('party.identifiers.code', '=', identifier)])
        if employee:
            contract.employee = employee
        else:
            print('empleado no encontrado')

        contract.company = company
        contract.salary = Decimal("{0:.2f}".format(salary))

        try:
            contract_type, = ContractType.find([('name', 'ilike', tipo_de_contrato)])
            contract.contract_type = contract_type
        except:
            contract_type = ContractType(name=tipo_de_contrato)
            contract_type.save()
            contract.contract_type = contract_type

        try:
            work_relationship, = WorkRelationship.find([('name', 'ilike', relacion_de_trabajo)])
            contract.work_relationship = work_relationship
        except:
            work_relationship = WorkRelationship(name=relacion_de_trabajo)
            work_relationship.save()
            contract.work_relationship = work_relationship

        department, = Department.find([('name', 'ilike', department_name)])
        if department:
            contract.department = department
        else:
            print('departamento no encontrado')

        try:
            position, = Position.find([('name', 'ilike', position_name)])
            contract.position = position
        except:
            position = Position(name=position_name)
            position.save()
            contract.position = position

        contract.save()
        print('contratto creado',name, cont)

    print(datetime.now())

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host',
        help='host', default='localhost')
    parser.add_argument('--port', dest='port',
        help='port', default='8000')
    parser.add_argument('--database', dest='database',
        help='database', required=True)
    parser.add_argument('--user', dest='user',
        help='user', required=True)
    parser.add_argument('--password', dest='password',
        help='password', required=True)
    parser.add_argument('--filename', dest='filename',
        help='filename', required=True)
    options = parser.parse_args()
    create_parties(options.host, options.port, options.database,
        options.user, options.password, options.filename)
