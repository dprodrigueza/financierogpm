from proteus import config, Model
from datetime import datetime
import xlrd
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import pdb


def create_departments(host, port, database, user, password, filename):
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(user, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(1)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    pdb.set_trace()

    Department = Model.get('company.department')

    for row in range(sheet.nrows)[1:]:
        cont += 1
        name = sheet.cell(row, 0).value
        parent_name = sheet.cell(row, 1).value
        department = Department()
        department.name = name
        if parent_name:
            parent, = Department.find([('name', '=', parent_name)])
            if parent:
                department.parent = parent
            else:
                print('parent no encontrado')
        department.save()
        print(cont, ' de ', total)
        print(
        'Correcto --->',sheet.cell(row, 0).value)

    print(datetime.now())

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host', default='localhost',
        help='localhost')
    parser.add_argument('--port', dest='port', default='8000',
        help='port')
    parser.add_argument('--database', dest='database', required=True,
        help='port')    
    parser.add_argument('--user', dest='user', required=True,
        help='user')
    parser.add_argument('--password', dest='password', required=True,
        help='password')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_departments(options.host, options.port, options.database,
        options.user, options.password, options.filename)
