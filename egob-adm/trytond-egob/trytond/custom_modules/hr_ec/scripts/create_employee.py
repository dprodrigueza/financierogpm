from proteus import config, Model
from datetime import datetime
import xlrd
import pdb
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

def create_parties(host, port, database, username, password, filename):
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(2)

    print(datetime.now())

    total = sheet.nrows
    cont = 0

    Party = Model.get('party.party')
    PartyIdentifier = Model.get('party.identifier')
    #Address = Model.get('party.address')
    #Country = Model.get('country.country')
    #Subdivision = Model.get('country.subdivision')
    #ContactMechanism = Model.get('party.contact_mechanism')
    Employee = Model.get('company.employee')
    Department = Model.get('company.department')
    Position = Model.get('company.position')
    Company = Model.get('company.company')

    pdb.set_trace()

    company, = Company.find([('party.name', 'ilike', 'GAD Pueblo Viejo')])

    for row in range(sheet.nrows)[1:]:

        cont += 1
        name = sheet.cell(row, 2).value.strip()
        id_type = sheet.cell(row, 3).value.strip()
        identifier = sheet.cell(row, 4).value.strip()
        position_name = sheet.cell(row, 5).value.strip()
        department_name = sheet.cell(row, 18).value.strip()
        #ec, = Country.find([('code', '=', 'EC')])

        employee = Employee()
        employee.company = company

        department, = Department.find([('name', 'ilike', department_name)])
        if department:
            employee.department = department
        else:
            print('parent no encontrado')

        try:
            position, = Position.find([('name', 'ilike', position_name)])
            employee.position = position
        except:
            position = Position(name=position_name)
            position.save()
            employee.position = position

        try:
            party, = Party.find([('identifiers.code', '=', identifier)])
        except:
            party = Party(name=name)
            party.identifiers.append(PartyIdentifier(type=id_type, code=identifier))
            party.identifiers[0].code = identifier
            party.save()

        employee.party = party
        employee.save()
        print('empleado creado',name, cont)

    print(datetime.now())

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host',
        help='host', default='localhost')
    parser.add_argument('--port', dest='port',
        help='port', default='8000')
    parser.add_argument('--database', dest='database',
        help='database', required=True)
    parser.add_argument('--user', dest='user',
        help='user', required=True)
    parser.add_argument('--password', dest='password',
        help='password', required=True)
    parser.add_argument('--filename', dest='filename',
        help='filename', required=True)
    options = parser.parse_args()
    create_parties(options.host, options.port, options.database,
        options.user, options.password, options.filename)
