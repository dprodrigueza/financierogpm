from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Id

from trytond.modules.company import CompanyReport

import pytz
import dateutil

__all__ = ['HolidayType', 'Holiday', 'Overtime', 'HolidayReport']

STATES_ = {
    'readonly': (Eval('state') != 'draft'),
}


class HRTimeMixin(Workflow, ModelSQL, ModelView):
    'HRTimeMixin'
    name = fields.Char('Name')
    employee = fields.Many2One('company.employee', 'Employee', required=True,
        states={
            'readonly': (~Eval('context', {}).get('groups', []).contains(
                Id('hr_ec', 'group_hr_admin')) | (Eval('state') != 'draft')),
        },
    )
    department = fields.Many2One('company.department', 'Department',
        required=True)
    description = fields.Char('Description', required=True, states=STATES_)
    date_start = fields.DateTime('Start Date/hour', required=True,
        states=STATES_)
    date_end = fields.DateTime('End Date/hour', required=True, states=STATES_)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('request', 'Request'),
        ('done', 'Done'),
        ('cancel', 'Canceled'),
        ], 'State', select=True, readonly=True)

    @classmethod
    def __setup__(cls):
        super(HRTimeMixin, cls).__setup__()
        cls._order[0] = ('name', 'DESC')
        cls.name.states['readonly'] = True
        cls.department.states['readonly'] = True
        t = cls.__table__()
        cls._transitions |= set((
                ('draft', 'request'),
                ('request', 'done'),
                ('request', 'draft'),
                ('done', 'cancel'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['done']),
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['request']),
                    },
                'request': {
                    'invisible': ~Eval('state').in_(['draft']),
                    },
                'done': {
                    'invisible': ~Eval('state').in_(['request']),
                    },
                })
        cls._error_messages.update({
            'dates_overlaps': ('"Dates %(first)s" and "%(second)s" overlap.'),
            'delete_no_draft': ('You can only delete records on draft state, '
                'please check %(name)s record.'),
            'no_sequence': ('You must to define a secuence in order to request '
                'a new records.'),
        })

    @classmethod
    def default_name(cls):
        return '/'

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee')

    @fields.depends('employee')
    def on_change_with_department(self, name=None):
        if self.employee:
            return self.employee.department.id

    @classmethod
    def validate(cls, items):
        super(HRTimeMixin, cls).validate(items)
        for item in items:
            item.check_dates()

    def check_dates(self):
        if self.date_end <= self.date_start:
            self.raise_user_error('dates_overlaps', {
                'first': str(self.date_start),
                'second': str(self.date_end)
            })
        domain = [
            ('date_start', '<=', self.date_end),
            ('date_end', '>=', self.date_start),
            ('employee', '=', self.employee.id),
            ('state', 'not in', ['cancel']),
            ('id', '!=', self.id),
        ]
        holidays = self.__class__.search(domain, count=True)
        if holidays > 0:
            self.raise_user_error('date_overlaps', {
                'first': str(self.date_start),
                'second': str(self.date_end)
            })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, items):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, items):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, items):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, items):
        pass

    @classmethod
    def delete(cls, items):
        for item in items:
            if item.name != '/':
                cls.raise_user_error('delete_no_draft', {'name': item.name})
        super(HRTimeMixin, cls).delete(items)


class HolidayType(ModelSQL, ModelView):
    'HR Holiday Type'
    __name__ = "hr_ec.holiday.type"

    name = fields.Char("Name", required=True)

    @classmethod
    def __setup__(cls):
        super(HolidayType, cls).__setup__()

        t = cls.__table__()
        cls._sql_constraints += [
            ('name_uniq', Unique(t, t.name),
                'Name must be unique '),
            ]


class Holiday(HRTimeMixin):
    'HR Holiday'
    __name__ = "hr_ec.holiday"

    type_ = fields.Many2One('hr_ec.holiday.type', 'Type', required=True,
        states=STATES_)
    date_return = fields.Date('Date return', required=True, states=STATES_)

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, holidays):
        super(Holiday, cls).request(holidays)
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('hr_ec.configuration')
        config = Config(1)
        for holiday in holidays:
            if holiday.name == '/':
                if config.holiday_sequence:
                    holiday.name = Sequence.get_id(config.holiday_sequence.id)
                else:
                    cls.raise_user_error('no_sequence')
        cls.save(holidays)

    def check_dates(self):
        super(Holiday, self).check_dates()
        if self.date_return < self.date_end.date():
            self.raise_user_error('dates_overlaps', {
                'first': str(self.date_end),
                'second': str(self.date_return)
            })


class Overtime(HRTimeMixin):
    'OverTime'
    __name__ = "hr_ec.overtime"

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, overtimes):
        super(Overtime, cls).request(overtimes)
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('hr_ec.configuration')
        config = Config(1)
        for overtime in overtimes:
            if overtime.name == '/':
                if config.overtime_sequence:
                    overtime.name = Sequence.get_id(config.overtime_sequence.id)
                else:
                    cls.raise_user_error('no_sequence')
        cls.save(overtimes)


class HolidayReport(CompanyReport):
    __name__ = 'hr_ec.holiday.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(HolidayReport, cls).get_context(
            records, data)
        report_context['get_time_tz'] = cls.get_time_tz
        return report_context

    @staticmethod
    def get_time_tz(dt):
        Company = Pool().get('company.company')
        company = Company(Transaction().context['company'])
        if company.timezone:
            czone = pytz.timezone(company.timezone)
            lzone = dateutil.tz.tzlocal()
            return dt.replace(tzinfo=lzone).astimezone(czone)
