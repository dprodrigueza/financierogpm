try:
    from trytond.modules.hr_ec.tests.test_hr_ec import suite
except ImportError:
    from .test_hr_ec import suite

__all__ = ['suite']
