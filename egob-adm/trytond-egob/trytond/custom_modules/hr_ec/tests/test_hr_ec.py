import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class HrEcTestCase(ModuleTestCase):
    'Test Hr Ec module'
    module = 'hr_ec'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            HrEcTestCase))
    return suite
