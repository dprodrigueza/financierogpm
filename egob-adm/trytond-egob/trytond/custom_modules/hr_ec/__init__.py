from trytond.pool import Pool

from company import Department
from company import Position
from company import ContractType
from company import OccupationalGroup
from company import WorkRelationship
from company import Contract
from company import Employee
from company import WorkCertificateReport
from company import EmployeesReport
from company import DepartmentsReport
from company import DepartmentsReportChart
from contact_mechanism import ContactMechanism
from party import Party
from configuration import Configuration
from hr_ec import HolidayType
from hr_ec import Holiday
from hr_ec import Overtime
from hr_ec import HolidayReport


def register():
    Pool.register(
        Department,
        Position,
        ContractType,
        OccupationalGroup,
        WorkRelationship,
        Contract,
        Employee,
        ContactMechanism,
        Party,
        Configuration,
        HolidayType,
        Holiday,
        Overtime,
        module='hr_ec', type_='model')
    Pool.register(
        module='hr_ec', type_='wizard')
    Pool.register(
        WorkCertificateReport,
        EmployeesReport,
        DepartmentsReport,
        DepartmentsReportChart,
        HolidayReport,
        module='hr_ec', type_='report')
