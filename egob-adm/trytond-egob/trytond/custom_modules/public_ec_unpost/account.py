from datetime import datetime

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Id
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction

__all__ = ['Move', 'MoveUnpost', 'UnpostStart', 'Unpost']


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    unpost_detail = fields.One2Many(
        'account.move.unpost', 'move', 'Detalle borrador', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Move, cls).__setup__()
        cls._error_messages.update({
            'closed_period': ('Periodo %(period)s, se encuentra cerrado.'),
            'closed_journal': ('Diario %(journal)s, se encuentra cerrado, '
                'para el periodo %(period)s.'),
            'reconciled_bank_statement': ('Asiento: %(move)s \n'
                'Cuenta: %(account)s \n '
                'Monto: %(amount)s \n'
                'Está conciliada en el extracto bancario: %(conciliation)s'
                'Para continuar con ésta operación desconcilie la línea '
                'en el extracto bancario.')
        })
        cls._buttons['unpost_'] = {
            'invisible': ~((Eval('state') == 'posted') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_account_unpost'))
            )),
            'depends': ['state'],
        }

    @classmethod
    def view_attributes(cls):
        return super(Move, cls).view_attributes() + [
            ('/form/notebook/page[@id="unpost_detail"]', 'states', {
                'invisible': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_account_unpost')))
            })
        ]

    @classmethod
    @ModelView.button_action('public_ec_unpost.act_account_move_unpost')
    def unpost_(cls, moves):
        pass

    @classmethod
    def unpost(cls, moves, description=None):
        pool = Pool()
        Line = pool.get('account.move.line')
        Invoice = pool.get('account.invoice')
        JournalPeriod = pool.get('account.journal.period')
        Reconciliation = pool.get('account.move.reconciliation')
        UnpostDetail = pool.get('account.move.unpost')
        User = pool.get('res.user')
        cls._force_state(moves, 'draft')
        reconciliation_to_remove = []
        bank_lines_to_update = []
        for move in moves:
            # Search for a close period or close journal by period
            if move.period.state == 'close':
                cls.raise_user_error('closed_period', {
                    'period': move.period.rec_name
                })
            journal_period = JournalPeriod.search([
                ('period', '=', move.period.id),
                ('journal', '=', move.journal.id)
            ])
            if journal_period:
                if journal_period[0].state == 'close':
                    cls.raise_user_error('closed_journal', {
                        'period': move.period.rec_name,
                        'journal': move.journal.rec_name
                    })
            move.check_rules()
            for line in move.lines:
                if line.reconciliation:
                    if line.reconciliation not in reconciliation_to_remove:
                        reconciliation_to_remove.append(line.reconciliation)
                if line.bank_reconciliation:
                    if line.bank_reconciliation.state == 'reconciled':
                        cls.raise_user_error('reconciled_bank_statement', {
                            'conciliation': (
                                line.bank_reconciliation.rec_name),
                            'move': line.move.number,
                            'account': line.account.rec_name,
                            'amount': line.debit - line.credit,
                        })
                    else:
                        line.bank_reconciliation = None
                        bank_lines_to_update.append(line)

            invoices_to_validate = Invoice.search([
                ('move', '=', move.id)
            ])
            lines_with_invoice = Line.search([
                ('move', '=', move),
                ('origin_', 'like', 'account.invoice%'),
            ])
            for m_line in lines_with_invoice:
                if m_line.origin_ not in invoices_to_validate:
                    invoices_to_validate.append(m_line.origin_)
            if invoices_to_validate:
                Invoice.validate_invoice(invoices_to_validate)

            unpost_detail = UnpostDetail()
            unpost_detail.move = move
            unpost_detail.user = User(Transaction().user)
            unpost_detail.date = datetime.now()
            if description:
                unpost_detail.description = description
            unpost_detail.save()

        if reconciliation_to_remove:
            Reconciliation.delete(reconciliation_to_remove)
        if bank_lines_to_update:
            Line.save(bank_lines_to_update)


class MoveUnpost(ModelSQL, ModelView):
    'Move Unpost'
    __name__ = 'account.move.unpost'

    move = fields.Many2One(
        'account.move', 'Asiento', required=True, readonly=True)
    user = fields.Many2One(
        'res.user', 'Usuario', required=True, readonly=True)
    description = fields.Char('Descripción', readonly=True)
    date = fields.DateTime('Fecha', required=True, readonly=True)


class UnpostStart(ModelView):
    'Unpost Move Start'
    __name__ = 'account.move.unpost.start'

    description = fields.Char('Razón')


class Unpost(Wizard):
    'Unpost Move'
    __name__ = 'account.move.unpost.wizard'
    start = StateView('account.move.unpost.start',
        'public_ec_unpost.account_move_unpost_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'unpost', 'tryton-ok', default=True),
            ])
    unpost = StateTransition()

    def transition_unpost(self):
        pool = Pool()
        Move = pool.get('account.move')
        context = Transaction().context
        ids = context['active_ids']
        moves = []
        for id_ in ids:
            moves.append(Move(id_))
        Move.unpost(moves, description=self.start.description)
        return 'end'
