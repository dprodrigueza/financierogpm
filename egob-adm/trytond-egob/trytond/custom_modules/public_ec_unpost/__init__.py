# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import account
from . import budget

__all__ = ['register']


def register():
    Pool.register(
        account.Move,
        account.MoveUnpost,
        account.UnpostStart,
        budget.Certificate,
        budget.CertificateUndone,
        budget.CertificateUndoneStart,
        budget.CertificateModifier,
        budget.CertificateModifierUndone,
        budget.CertificateModifierUndoneStart,
        budget.Compromise,
        budget.CompromiseUndone,
        budget.CompromiseUndoneStart,
        budget.CompromiseModifier,
        budget.CompromiseModifierUndone,
        budget.CompromiseModifierUndoneStart,
        module='public_ec_unpost', type_='model')
    Pool.register(
        account.Unpost,
        budget.CertificateUndoneWizard,
        budget.CertificateModifierUndoneWizard,
        budget.CompromiseUndoneWizard,
        budget.CompromiseModifierUndoneWizard,
        module='public_ec_unpost', type_='wizard')
    Pool.register(
        module='public_ec_unpost', type_='report')
