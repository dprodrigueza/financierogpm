from datetime import datetime

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Id
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction

__all__ = ['Certificate', 'CertificateUndone', 'CertificateUndoneStart',
    'CertificateUndoneWizard', 'CertificateModifier',
    'CertificateModifierUndone', 'CertificateModifierUndoneStart',
    'CertificateModifierUndoneWizard', 'Compromise', 'CompromiseUndone',
    'CompromiseUndoneStart', 'CompromiseUndoneWizard', 'CompromiseModifier',
    'CompromiseModifierUndone', 'CompromiseModifierUndoneStart',
    'CompromiseModifierUndoneWizard']


class Certificate(metaclass=PoolMeta):
    __name__ = 'public.budget.certificate'

    undone_detail = fields.One2Many('public.budget.certificate.undone',
        'certificate', 'Detalle borrador', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Certificate, cls).__setup__()
        cls._error_messages.update({
            'certificate_with_compromises': ('La certificación: '
                '%(certificate)s tiene compromisos aprobados.'),
            'certificate_with_modifiers': ('La certificación: '
                '%(certificate)s tiene modificaciones aprobados.'),
            'closed_period': ('Periodo %(period)s, se encuentra cerrado.'),
        })
        cls._buttons['undone_'] = {
            'invisible': ~((Eval('state') == 'done') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone'))
            )),
            'depends': ['state'],
        }

    @classmethod
    def view_attributes(cls):
        return super(Certificate, cls).view_attributes() + [
            ('/form/notebook/page[@id="undone_detail"]', 'states', {
                'invisible': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone')))
            })
        ]

    @classmethod
    @ModelView.button_action(
        'public_ec_unpost.act_public_budget_certificate_undone')
    def undone_(cls, certificates):
        pass

    @classmethod
    def undone(cls, certificates, description=None):
        pool = Pool()
        Compromise = pool.get('public.budget.compromise')
        Modifier = pool.get('public.budget.certificate.modifier')
        UndoneDetail = pool.get('public.budget.certificate.undone')
        User = pool.get('res.user')
        for certificate in certificates:
            # Search for a close period
            if certificate.period.state == 'close':
                cls.raise_user_error('closed_period', {
                    'period': certificate.period.rec_name
                })
            # Check there is no done compromises, needed when a done certificate
            # is passed to confirmed
            done_compromises = Compromise.search([
                ('state', '=', 'done'),
                ('certificate', '=', certificate.id)
            ])
            if done_compromises:
                cls.raise_user_error('certificate_with_compromises', {
                    'certificate': certificate.number,
                    })
            # Check there is no done modifiers
            done_modifiers = Modifier.search([
                ('state', '=', 'done'),
                ('certificate', '=', certificate.id),
            ])
            if done_modifiers:
                cls.raise_user_error('certificate_with_modifiers', {
                    'certificate': certificate.number,
                    })
            certificate.state = 'draft'
            certificate.save()
            undone_detail = UndoneDetail()
            undone_detail.certificate = certificate
            undone_detail.user = User(Transaction().user)
            undone_detail.date = datetime.now()
            if description:
                undone_detail.description = description
            undone_detail.save()


class CertificateUndone(ModelSQL, ModelView):
    'Certificate Undone'
    __name__ = 'public.budget.certificate.undone'

    certificate = fields.Many2One('public.budget.certificate', 'Certificación',
        required=True, readonly=True)
    user = fields.Many2One(
        'res.user', 'Usuario', required=True, readonly=True)
    description = fields.Char('Descripción', readonly=True)
    date = fields.DateTime('Fecha', required=True, readonly=True)


class CertificateUndoneStart(ModelView):
    'Undone Certificate Start'
    __name__ = 'public.budget.certificate.undone.start'

    description = fields.Char('Razón')


class CertificateUndoneWizard(Wizard):
    'Undone Certificate'
    __name__ = 'public.budget.certificate.undone.wizard'
    start = StateView('public.budget.certificate.undone.start',
        'public_ec_unpost.certificate_undone_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'undone', 'tryton-ok', default=True),
            ])
    undone = StateTransition()

    def transition_undone(self):
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        context = Transaction().context
        ids = context['active_ids']
        certificates = []
        for id_ in ids:
            certificates.append(Certificate(id_))
        Certificate.undone(certificates, description=self.start.description)
        return 'end'


class CertificateModifier(metaclass=PoolMeta):
    __name__ = 'public.budget.certificate.modifier'

    undone_detail = fields.One2Many('public.budget.certificate.modifier.undone',
        'modifier', 'Detalle borrador', readonly=True)

    @classmethod
    def __setup__(cls):
        super(CertificateModifier, cls).__setup__()
        cls._error_messages.update({
            'certificate_with_compromises': ('La certificación: '
                '%(certificate)s tiene compromisos aprobados.'),
            'closed_period': ('Periodo %(period)s, se encuentra cerrado.'),
        })
        cls._buttons['undone_'] = {
            'invisible': ~((Eval('state') == 'done') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone'))
            )),
            'depends': ['state'],
        }

    @classmethod
    def view_attributes(cls):
        return super(CertificateModifier, cls).view_attributes() + [
            ('/form/notebook/page[@id="undone_detail"]', 'states', {
                'invisible': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone')))
            })
        ]

    @classmethod
    @ModelView.button_action(
        'public_ec_unpost.act_public_budget_certificate_modifier_undone')
    def undone_(cls, modifiers):
        pass

    @classmethod
    def undone(cls, modifiers, description=None):
        pool = Pool()
        Compromise = pool.get('public.budget.compromise')
        UndoneDetail = pool.get('public.budget.certificate.modifier.undone')
        User = pool.get('res.user')
        for modifier in modifiers:
            # Search for a close period
            if modifier.period.state == 'close':
                cls.raise_user_error('closed_period', {
                    'period': modifier.certificate.period.rec_name
                })
            # Check there is no done compromises, needed when a done certificate
            # is passed to confirmed
            done_compromises = Compromise.search([
                ('state', '=', 'done'),
                ('certificate', '=', modifier.certificate.id)
            ])
            if done_compromises:
                cls.raise_user_error('certificate_with_compromises', {
                    'certificate': modifier.certificate.number,
                    })
            modifier.state = 'draft'
            modifier.save()
            undone_detail = UndoneDetail()
            undone_detail.modifier = modifier
            undone_detail.user = User(Transaction().user)
            undone_detail.date = datetime.now()
            if description:
                undone_detail.description = description
            undone_detail.save()


class CertificateModifierUndone(ModelSQL, ModelView):
    'Certificate Undone'
    __name__ = 'public.budget.certificate.modifier.undone'

    modifier = fields.Many2One('public.budget.certificate.modifier',
        'Certificación', required=True, readonly=True)
    user = fields.Many2One(
        'res.user', 'Usuario', required=True, readonly=True)
    description = fields.Char('Descripción', readonly=True)
    date = fields.DateTime('Fecha', required=True, readonly=True)


class CertificateModifierUndoneStart(ModelView):
    'Undone Certificate Start'
    __name__ = 'public.budget.certificate.modifier.undone.start'

    description = fields.Char('Razón')


class CertificateModifierUndoneWizard(Wizard):
    'Undone Certificate'
    __name__ = 'public.budget.certificate.modifier.undone.wizard'
    start = StateView('public.budget.certificate.modifier.undone.start',
        'public_ec_unpost.certificate_modifier_undone_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'undone', 'tryton-ok', default=True),
            ])
    undone = StateTransition()

    def transition_undone(self):
        pool = Pool()
        Modifier = pool.get('public.budget.certificate.modifier')
        context = Transaction().context
        ids = context['active_ids']
        modifiers = []
        for id_ in ids:
            modifiers.append(Modifier(id_))
        Modifier.undone(modifiers, description=self.start.description)
        return 'end'


class Compromise(metaclass=PoolMeta):
    __name__ = 'public.budget.compromise'

    undone_detail = fields.One2Many('public.budget.compromise.undone',
        'compromise', 'Detalle borrador', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Compromise, cls).__setup__()
        cls._error_messages.update({
            'compromise_with_moves': ('El compromiso: '
                '%(compromise)s tiene asientos contabilizados.'),
            'compromise_with_modifiers': ('El compromiso: '
                '%(compromise)s tiene modificaciones aprobadas.'),
            'closed_period': ('Periodo %(period)s, se encuentra cerrado.'),
        })
        cls._buttons['undone_'] = {
            'invisible': ~((Eval('state') == 'done') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone'))
            )),
            'depends': ['state'],
        }

    @classmethod
    def view_attributes(cls):
        return super(Compromise, cls).view_attributes() + [
            ('/form/notebook/page[@id="undone_detail"]', 'states', {
                'invisible': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone')))
            })
        ]

    @classmethod
    @ModelView.button_action(
        'public_ec_unpost.act_public_budget_compromise_undone')
    def undone_(cls, compromises):
        pass

    @classmethod
    def undone(cls, compromises, description=None):
        pool = Pool()
        Move = pool.get('account.move')
        Modifier = pool.get('public.budget.compromise.modifier')
        UndoneDetail = pool.get('public.budget.compromise.undone')
        User = pool.get('res.user')
        for compromise in compromises:
            # Search for a close period
            if compromise.period.state == 'close':
                cls.raise_user_error('closed_period', {
                    'period': compromise.period.rec_name
                })
            # Check there is no posted moves, needed when a done compromise
            # is passed to confirmed
            posted_moves = Move.search([
                ('state', '=', 'posted'),
                ('compromises', 'in', [compromise.id])
            ])
            if posted_moves:
                cls.raise_user_error('compromise_with_moves', {
                    'compromise': compromise.number,
                    })
            # Check there is no done modifiers
            done_modifiers = Modifier.search([
                ('state', '=', 'done'),
                ('compromise', '=', compromise.id),
            ])
            if done_modifiers:
                cls.raise_user_error('compromise_with_modifiers', {
                    'certificate': compromise.number,
                    })
            compromise.state = 'draft'
            compromise.save()
            undone_detail = UndoneDetail()
            undone_detail.compromise = compromise
            undone_detail.user = User(Transaction().user)
            undone_detail.date = datetime.now()
            if description:
                undone_detail.description = description
            undone_detail.save()


class CompromiseUndone(ModelSQL, ModelView):
    'Compromise Undone'
    __name__ = 'public.budget.compromise.undone'

    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        required=True, readonly=True)
    user = fields.Many2One(
        'res.user', 'Usuario', required=True, readonly=True)
    description = fields.Char('Descripción', readonly=True)
    date = fields.DateTime('Fecha', required=True, readonly=True)


class CompromiseUndoneStart(ModelView):
    'Undone Compromise Start'
    __name__ = 'public.budget.compromise.undone.start'

    description = fields.Char('Razón')


class CompromiseUndoneWizard(Wizard):
    'Undone Compromise'
    __name__ = 'public.budget.compromise.undone.wizard'
    start = StateView('public.budget.compromise.undone.start',
        'public_ec_unpost.compromise_undone_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'undone', 'tryton-ok', default=True),
            ])
    undone = StateTransition()

    def transition_undone(self):
        pool = Pool()
        Compromise = pool.get('public.budget.compromise')
        context = Transaction().context
        ids = context['active_ids']
        compromises = []
        for id_ in ids:
            compromises.append(Compromise(id_))
        Compromise.undone(compromises, description=self.start.description)
        return 'end'


class CompromiseModifier(metaclass=PoolMeta):
    __name__ = 'public.budget.compromise.modifier'

    undone_detail = fields.One2Many('public.budget.compromise.modifier.undone',
        'modifier', 'Detalle borrador', readonly=True)

    @classmethod
    def __setup__(cls):
        super(CompromiseModifier, cls).__setup__()
        cls._error_messages.update({
            'compromise_with_compromises': ('La certificación: '
                '%(compromise)s tiene compromisos aprobados.'),
            'closed_period': ('Periodo %(period)s, se encuentra cerrado.'),
        })
        cls._buttons['undone_'] = {
            'invisible': ~((Eval('state') == 'done') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone'))
            )),
            'depends': ['state'],
        }

    @classmethod
    def view_attributes(cls):
        return super(CompromiseModifier, cls).view_attributes() + [
            ('/form/notebook/page[@id="undone_detail"]', 'states', {
                'invisible': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_ec_unpost', 'group_budget_undone')))
            })
        ]

    @classmethod
    @ModelView.button_action(
        'public_ec_unpost.act_public_budget_compromise_modifier_undone')
    def undone_(cls, modifiers):
        pass

    @classmethod
    def undone(cls, modifiers, description=None):
        pool = Pool()
        Move = pool.get('account.move')
        UndoneDetail = pool.get('public.budget.compromise.modifier.undone')
        User = pool.get('res.user')
        for modifier in modifiers:
            # Search for a close period
            if modifier.period.state == 'close':
                cls.raise_user_error('closed_period', {
                    'period': modifier.compromise.period.rec_name
                })
            # Check there is no posted moves, needed when a done compromise
            # is passed to confirmed
            posted_moves = Move.search([
                ('state', '=', 'posted'),
                ('compromises', 'in', [modifier.compromise.id])
            ])
            if posted_moves:
                cls.raise_user_error('compromise_with_moves', {
                    'compromise': modifier.compromise.number,
                    })
            modifier.state = 'draft'
            modifier.save()
            undone_detail = UndoneDetail()
            undone_detail.modifier = modifier
            undone_detail.user = User(Transaction().user)
            undone_detail.date = datetime.now()
            if description:
                undone_detail.description = description
            undone_detail.save()


class CompromiseModifierUndone(ModelSQL, ModelView):
    'Compromise Undone'
    __name__ = 'public.budget.compromise.modifier.undone'

    modifier = fields.Many2One('public.budget.compromise.modifier',
        'Certificación', required=True, readonly=True)
    user = fields.Many2One(
        'res.user', 'Usuario', required=True, readonly=True)
    description = fields.Char('Descripción', readonly=True)
    date = fields.DateTime('Fecha', required=True, readonly=True)


class CompromiseModifierUndoneStart(ModelView):
    'Undone Compromise Start'
    __name__ = 'public.budget.compromise.modifier.undone.start'

    description = fields.Char('Razón')


class CompromiseModifierUndoneWizard(Wizard):
    'Undone Compromise'
    __name__ = 'public.budget.compromise.modifier.undone.wizard'
    start = StateView('public.budget.compromise.modifier.undone.start',
        'public_ec_unpost.compromise_modifier_undone_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'undone', 'tryton-ok', default=True),
            ])
    undone = StateTransition()

    def transition_undone(self):
        pool = Pool()
        Modifier = pool.get('public.budget.compromise.modifier')
        context = Transaction().context
        ids = context['active_ids']
        modifiers = []
        for id_ in ids:
            modifiers.append(Modifier(id_))
        Modifier.undone(modifiers, description=self.start.description)
        return 'end'
