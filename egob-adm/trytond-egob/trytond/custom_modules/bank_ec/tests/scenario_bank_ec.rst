=============
Bank Scenario
=============

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.public_ec.tests.tools import set_fiscalyear_data
    >>> from trytond.modules.account.tests.tools import create_fiscalyear
    >>> from trytond.modules.hr_ec.tests.tools import get_party

Install bank_ec::

    >>> config = activate_modules('bank_ec')

Create Company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_data(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create Default Variables::

    >>> Journal = Model.get('account.journal')
    >>> Bank = Model.get('bank')
    >>> BankAccount = Model.get('bank.account')
    >>> AccountBankTransfer = Modelget('account.bank.transfer')
    >>> Account = Model.get('account.account')

Create Main Journals::

    >>> cash, = Journal.find([('code', '=', 'CASH')])
    >>> stock, = Journal.find([('code', '=', 'STO')])
    >>> expense, = Journal.find([('code', '=', 'EXP')])
    >>> revenue, = Journal.find([('code', '=', 'REV')])

Create Bank 001::

    >>> bank_001 = Bank()
    >>> bank_001.party = get_party()
    >>> bank_001.save()

Create Bank 002::

    >>> bank_002 = Bank()
    >>> bank_002.party = get_party()
    >>> bank_002.save()

Create Bank 003::

    >>> bank_003 = Bank()
    >>> bank_003.party = get_party()
    >>> bank_003.save()

Create Bank 004::

    >>> bank_004 = Bank()
    >>> bank_004.party = get_party()
    >>> bank_004.save()

Create Account Fiscal Resources::

    >>> account_111_02_01_01 = Account()
    >>> account_111_02_01_01.name = 'Fiscal Resources'
    >>> account_111_02_01_01.code = '111.02.01.01'
    >>> parent, = Account.find([
    ...     ('code', '=', '111.02.01')])
    >>> account_111_02_01_01.parent = parent
    >>> account_111_02_01_01.kind = 'other'
    >>> account_111_02_01_01.save()

Create Account Collecting Boxes::

    >>> account_111_01_01 = Account()
    >>> account_111_01_01.name = 'Collecting Boxes'
    >>> account_111_01_01.code = '111.01.01'
    >>> parent, = Account.find([
    ...     ('code', '=', '111.01')])
    >>> account_111_01_01.parent = parent
    >>> account_111_01_01.kind = 'other'
    >>> account_111_01_01.save()

Create Account Self-Management Resources::

    >>> account_111_02_02_01 = Account()
    >>> account_111_02_02_01.name = 'Self-Management Resources'
    >>> account_111_02_02_01.code = '111.02.02.01'
    >>> parent, = Account.find([
    ...     ('code', '=', '111.02.02')])
    >>> account_111_02_02_01.parent = parent
    >>> account_111_02_02_01.kind = 'other'
    >>> account_111_02_02_01.save()

Create Account Third-Party Funds::

    >>> account_111_02_07_01 = Account()
    >>> account_111_02_07_01.name = 'Third-Party Funds'
    >>> account_111_02_07_01.code = '111.02.07.01'
    >>> parent, = Account.find([
    ...     ('code', '=', '111.02.07')])
    >>> account_111_02_07_01.parent = parent
    >>> account_111_02_07_01.kind = 'other'
    >>> account_111_02_07_01.save()

Create Account Bank 001::

    >>> bank_account_001 = BankAccount()
    >>> bank_account_001.bank = bank_001
    >>> bank_account_001.account = account_111_02_01_01

    >>> bank_account_001_number_001 = bank_account_001.numbers.new()
    >>> bank_account_001_number_001.type = 'other'
    >>> bank_account_001_number_001.number = '1234'

    >>> bank_account_001.save()

Create Account Bank 002::

    >>> bank_account_002 = BankAccount()
    >>> bank_account_002.bank = bank_001
    >>> bank_account_002.account = account_111_02_01_01

    >>> bank_account_002_number_001 = bank_account_002.numbers.new()

    >>> bank_account_002.save()

Create Account Bank 003::

    >>> bank_account_003 = BankAccount()
    >>> bank_account_003.bank = bank_001
    >>> bank_account_003.account = account_111_02_01_01

    >>> bank_account_003_number_001 = bank_account_003.numbers.new()

    >>> bank_account_003.save()

Create Account Bank 004::

    >>> bank_account_004 = BankAccount()
    >>> bank_account_004.bank = bank_001
    >>> bank_account_004.account = account_111_02_01_01

    >>> bank_account_004_number_001 = bank_account_004.numbers.new()
    >>> bank_account_004_number_001.type = 'other'
    >>> bank_account_004_number_001.number = '9876'

    >>> bank_account_004.save()

Create Account Bank Transfer 001::

    >>> account_bank_transfer_001 = AccountBankTransfer()
    >>> account_bank_transfer_001.company = company
    >>> account_bank_transfer_001.journal = cash
    >>> account_bank_transfer_001.fiscalyear = fiscalyear
    >>> account_bank_transfer_001.date = datetime.date.today()
    >>> account_bank_transfer_001.account_bank_from = bank_account_001
    >>> account_bank_transfer_001.account_bank_to = bank_account_002
    >>> account_bank_transfer_001.amount = Decimal('1234.50')

    >>> account_bank_transfer_001.save()
    >>> account_bank_transfer_001.click('confirm')
    >>> account_bank_transfer_001.clik('done')

Check Account Move of Account Bank Transfer 001::

    >>> account_move = account_bank_transfer_001.account_move
    >>> account_move.lines[0].debit
    Decimal('1234.50')
    >>> account_move.line[0].credit
    Decimal('0.00')
    >>> account_move.lines[1].debit
    Decimal('0.00')
    >>> account_move.line[1].credit
    Decimal('1234.50')

Create Account Bank Transfer 002::

    >>> account_bank_transfer_002 = AccountBankTransfer()
    >>> account_bank_transfer_002.company = company
    >>> account_bank_transfer_002.journal = cash
    >>> account_bank_transfer_002.fiscalyear = fiscalyear
    >>> account_bank_transfer_002.date = datetime.date.today()
    >>> account_bank_transfer_002.account_bank_from = bank_account_003
    >>> account_bank_transfer_002.account_bank_to = bank_account_004
    >>> account_bank_transfer_002.amount = Decimal('111.50')

    >>> account_bank_transfer_002.save()
    >>> account_bank_transfer_002.click('confirm')
    >>> account_bank_transfer_002.clik('done')

Check Account Move of Account Bank Transfer 002::

    >>> account_move = account_bank_transfer_002.account_move
    >>> account_move.lines[0].debit
    Decimal('111.50')
    >>> account_move.line[0].credit
    Decimal('0.00')
    >>> account_move.lines[1].debit
    Decimal('0.00')
    >>> account_move.line[1].credit
    Decimal('111.50')

Create Account Bank Transfer 003 Error Same Bank::

    >>> account_bank_transfer_001 = AccountBankTransfer()
    >>> account_bank_transfer_001.company = company
    >>> account_bank_transfer_001.journal = cash
    >>> account_bank_transfer_001.fiscalyear = fiscalyear
    >>> account_bank_transfer_001.date = datetime.date.today()
    >>> account_bank_transfer_001.account_bank_from = bank_account_001
    >>> account_bank_transfer_001.account_bank_to = bank_account_001
    >>> account_bank_transfer_001.amount = Decimal('1234.50')

    >>> account_bank_transfer_001.save()
    >>> account_bank_transfer_001.click('confirm')
    >>> account_bank_transfer_001.clik('done')

