# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import bank

__all__ = ['register']


def register():
    Pool.register(
        bank.Bank,
        bank.AccountBankTransfer,
        bank.Fiscalyear,
        bank.Move,
        module='bank_ec', type_='model')
    Pool.register(
        module='bank_ec', type_='wizard')
    Pool.register(
        module='bank_ec', type_='report')
