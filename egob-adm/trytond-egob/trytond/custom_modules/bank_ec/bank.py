from trytond.modules.public_ec.budget import (set_employee, employee_field,
    date_field)
from trytond.model import ModelView, fields, ModelSQL, Workflow
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

__all__ = ['Bank', 'AccountBankTransfer', 'Fiscalyear', 'Move']


class Bank:
    __metaclass__ = PoolMeta
    __name__ = 'bank.account'

    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[
            ('bank_reconcile', '=', True),
            ('kind', '!=', 'view'),
            #TODO: Add Domain for Bank Account
        ], ondelete='RESTRICT')


class AccountBankTransfer(Workflow, ModelSQL, ModelView):
    'Account Bank Transfer'
    __name__ = 'account.bank.transfer'

    _states = {
        'readonly': ~Eval('state').in_(['draft'])
    }
    _depends = ['state']

    company = fields.Many2One('company.company', 'Company', required=True,
        ondelete='RESTRICT', states=_states, depends=_depends)
    journal = fields.Many2One('account.journal', 'Journal', required=True,
        ondelete='RESTRICT', states=_states, depends=_depends)
    number = fields.Char('Number', readonly=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True, ondelete='RESTRICT', states=_states, depends=_depends)
    fiscalyear_start_date = fields.Function(
        fields.Date('Fiscalyear Start Date'),
        'on_change_with_fiscalyear_start_date')
    fiscalyear_end_date = fields.Function(
        fields.Date('Fiscalyear End Date'),
        'on_change_with_fiscalyear_end_date')
    date = fields.Date('Date', required=True, states=_states,
        domain=[('date', '>=', Eval('fiscalyear_start_date')),
            ('date', '<=', Eval('fiscalyear_end_date'))
        ], depends=_depends + ['fiscalyear_start_date', 'fiscalyear_end_date'])
    account_bank_from = fields.Many2One('bank.account', 'Bank Account From',
        states=_states, ondelete='RESTRICT',
        domain=[
            ('id', '!=', Eval('account_bank_to'))
        ], depends=_depends + ['account_bank_to'])
    account_bank_to = fields.Many2One('bank.account', 'Bank Account To',
        states=_states, ondelete='RESTRICT',
        domain=[
            ('id', '!=', Eval('account_bank_from'))
        ], depends=_depends + ['account_bank_from'])
    confirmed_date = date_field("Confirmed Date")
    confirmed_by = employee_field("Confirmed By")
    done_by = employee_field("Done By")
    done_date = date_field("Done Date")
    amount = fields.Numeric('Amount', digits=(16,2), required=True,
        states=_states, depends=_depends)
    account_move = fields.Many2One('account.move', 'Account Move',
        states=_states, ondelete='RESTRICT', readonly=True,
        domain=[
            ('company', '=', Eval('company', -1)),
        ], depends=['company'])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancel', 'Cancel'),
    ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(AccountBankTransfer, cls).__setup__()
        cls._error_messages.update({
            'no_transfer_sequence': 'No account bank transfer sequence for "%s".'
            })
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'done'),
            ('confirmed', 'draft'),
            ('confirmed', 'cancel')))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'icon': 'tryton-go-previous'
            },
            'confirm': {
                'invisible': ~Eval('state').in_(['draft']),
                'icon': 'tryton-go-next'
            },
            'done': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'icon': 'tryton-close'
            },
            'cancel': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'icon': 'tryton-cancel'}
        })

    @fields.depends('fiscalyear')
    def on_change_with_fiscalyear_start_date(self, name=None):
        if self.fiscalyear:
            return self.fiscalyear.start_date

    @fields.depends('fiscalyear')
    def on_change_with_fiscalyear_end_date(self, name=None):
        if self.fiscalyear:
            return self.fiscalyear.end_date

    def get_move_lines(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        lines =[]
        line = MoveLine()
        line.account = self.account_bank_from.account
        line.debit = self.amount
        lines.append(line)
        line = MoveLine()
        line.account = self.account_bank_to.account
        line.credit = self.amount
        lines.append(line)
        return lines

    def get_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        move = Move()
        move.type = 'financial'
        move.journal = self.journal
        move.origin = self
        move.lines = self.get_move_lines();
        return move

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, transfers):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirm(cls, transfers):
        pool = Pool()
        Date = pool.get('ir.date')
        for transfer in transfers:
            transfer.confirmed_date = Date.today()
        cls.save(transfers)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def done(cls, transfers):
        pool = Pool()
        Date = pool.get('ir.date')
        Sequence = pool.get('ir.sequence')
        for transfer in transfers:
            if transfer.fiscalyear.account_bank_transfer_sequence:
                transfer.done_date = Date.today()
                transfer.number = Sequence.get_id(
                    transfer.fiscalyear.account_bank_transfer_sequence.id)
                move = transfer.get_move()
                transfer.account_move = move
                move.save()
            else:
                cls.raise_user_error('no_transfer_sequence', (
                    transfer.fiscalyear.rec_name,))
        cls.save(transfers)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, transfers):
        pass


class Fiscalyear:
    __metaclass__ = PoolMeta
    __name__ = 'account.fiscalyear'

    account_bank_transfer_sequence = fields.Many2One('ir.sequence',
        'Account Bank Transfer Sequence', required=True,
        domain=[
            ('code', '=', 'account.bank.transfer'),
            ['OR',
            ('company', '=', Eval('company')),
            ('company', '=', None)
            ]],
        context={
            'code': 'account.bank.transfer',
            'company': Eval('company'),
        }, depends=['company'])


class Move:
    __metaclass__ = PoolMeta
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['account.bank.transfer']