from decimal import Decimal
from collections import defaultdict
from datetime import date

from sql import Literal, Window
from sql.functions import CurrentTimestamp, RowNumber

from trytond.model import ModelSQL, ModelView, fields, Workflow, Unique
from trytond.pyson import Eval, Bool, Id, Equal, If
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.modules.hr_ec.hr_ec import date_field, employee_field, set_employee
from trytond.bus import notify

from functools import reduce

__all__ = [
    'Requirement', 'RequirementLine', 'RequirementLineDetail',
    'EnterRequirementLineDetail', 'EnterRequirementLineDetailStart',
    'MaterialRequirement', 'MaterialRequirementLine', 'RequirementLocation',
    'MaterialRequirementLocationHistory',
    'MaterialRequirementLineProductStockPurhcase', 'EnterAsignToLotacion',
    'EnterAsignToLotacionStart', 'PurchaseBudget', 'EnterRequirementBudget',
    'EnterRequestBudgetWizard', 'RequirementLineTracking',
    'EnterLocationAllLines', 'EnterLocationAllLinesStart'
]


def domain_quarter():
    quarter = {
        '1': [1, 2, 3, 4],
        '2': [5, 6, 7, 8],
        '3': [9, 10, 11, 12]
    }
    month = date.today().month
    domain = []
    for item in quarter:
        if month in quarter[item]:
            domain += [('quarter'+item, '=', True)]
    return domain

def create_location_by_product(line):
    stock = Decimal("0")
    if line.from_location and line.product:
        Product = Pool().get('product.product')
        Date = Pool().get('ir.date')
        with Transaction().set_context(
                with_childs=True,
                stock_date_end=Date.today(),
                locations=[line.from_location.id]):
            product = Product(line.product.id)
            stock = Decimal(str(product.forecast_quantity)).quantize(
                Decimal("0.01"))
    return stock


class RequirementLocation(ModelSQL, ModelView):
    'Requirement Location'
    __name__ = 'purchase.requirement.location'

    requirement = fields.Many2One(
        'purchase.requirement', 'Requerimiento', required=True,
        ondelete='CASCADE')
    location = fields.Many2One(
        'stock.location', 'Ubicacion', required=True)


class Requirement(Workflow, ModelSQL, ModelView):
    'Purchase Requirement'
    __name__ = 'purchase.requirement'
    _history = True

    _STATES = {
        'readonly': Eval('state') != 'draft',
    }
    _DEPENDS = ['state']

    number = fields.Char('Número', states={'readonly': True})
    company = fields.Many2One(
        'company.company', 'Compañia', readonly=True, required=True)
    employee = fields.Many2One(
        'company.employee', 'Responsable',
        states={
            'readonly': ~((Eval('state') == 'draft') & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_pac','group_public_pac_edit'))))
        }, required=True, depends=_DEPENDS)
    department = fields.Many2One(
        'company.department', 'Departamento', states={'readonly': True,},
        required=True,depends=_DEPENDS + ['employee'])
    locations = fields.Many2Many(
        'purchase.requirement.location', 'requirement', 'location',
        'Ubicaciones', states={'readonly': True,})
    date = fields.Date('Fecha', readonly=True)
    description = fields.Text(
        'Observaciones',
        states={
            'readonly': ~(Eval('state').in_(['request']) & (
                Eval('context', {}).get('groups', []).contains(
                    Id('public_pac','group_requirements_done'))) & ~(
                Equal(Eval('context', {}).get('employee'),Eval('employee'))))
        },depends=_DEPENDS)
    line = fields.One2Many(
        'purchase.requirement.line', 'requirements', 'Requerimientos',
        states={
            'readonly': ~Eval('state').in_(['draft']),
        }, depends=_DEPENDS)
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('request', 'Solicitado'),
            ('cancel', 'Cancelado'),
            ('done', 'Realizado'),
        ], 'Estado', readonly=True)
    total = fields.Function(
        fields.Numeric('Total', digits=(6, 2)),
        'on_change_with_total'
    )
    request_date = date_field('Fecha solicitud')
    done_date = date_field('Fecha Aprobacion')

    request_by = employee_field('Solicitado por')
    done_by = employee_field('Aprobado por')

    location = fields.Function(
        fields.Many2One('stock.location', 'Ubicacion'),
        'on_change_with_location'
    )
    products = fields.Function(
        fields.Char('Productos'),
        'get_products', searcher='search_products'
    )
    tracking = fields.One2Many(
        'purchase.requirement.line.tracking', 'requirement', 'Seguimiento',
        states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(Requirement, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
        ]
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'done'),
            ('request', 'cancel'),
            ('request', 'draft'),))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['request']),
                'readony': ~(Eval('context', {}).get('groups', []).contains(
                    Id('public_pac', 'group_public_pac_edit')))
            },
            'request': {
                'invisible': ~Eval('state').in_(['draft']),
            },
            'cancel': {
                'invisible': ~((Eval('state') == 'request') & ~(Equal(
                    Eval('context', {}).get('employee'),Eval('employee')))),
                'depends': ['state', 'employee']
            },
            'done': {
                'invisible': ~((Eval('state') == 'request') &~(Equal(
                    Eval('context', {}).get('employee'),Eval('employee')))),
                'depends': ['state', 'employee']
            },
        })
        cls._error_messages.update({
            'cero_aproved_quantity': (
                'El requerimiento "%(requirement)s" tiene un valor de '
                'aprobación de cero.\n Los requerimientos con valores cero en '
                'aprobación no seran procesados.'),
            'require_observation': (
                'No se puede devolver el requerimiento con observaciones '
                'vacias.'),
            'no_sequence_defined': (
                'No existe secuencia definida para requerimientos.'
                'Por favor, diríjase a "Configuración de Compras" y '
                'configure una secuencia.'),
            'update_employee_department_warehouse': (
                'Actualizar la bodega a nivel del empleado o del departamento'),
            'cero_budgets': (
                'El requerimiento "%(requirement)s" debe tener al menos '
                'una partida presupuestaria.\n'),
            'changed_quantity': (
                'La cantidad requerida del requerimiento "%(requirement)s" se '
                'ha modificado por que los valores de las partidas tienen que '
                'ser actualizados.\n Ingresa a las partidas y verfica que los '
                'valores correspondan al monto total del requerimiento.\n'),
            'delete_no_draft': (
                'Debe eliminar solo registros en estado borrador.'),
            'no_have_configuration_to_location': (
                'No existe definida la bodega destino.'
                'Por favor, diríjase a "Configuración de Compras".'),
            'no_lines': 'La solicitud de despacho debe tener al menos un requerimiento.'
        })

    @classmethod
    def delete(cls, requirements):
        for requirement in requirements:
            if requirement.state not in ['draft']:
                cls.raise_user_error('delete_no_draft')
        super(Requirement, cls).delete(requirements)

    @classmethod
    def copy(cls, requirements, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('number', '/')
        default.setdefault('request_date', None)
        default.setdefault('done_date', None)
        default.setdefault('request_by', None)
        default.setdefault('done_by', None)
        default.setdefault('date', date.today())
        return super(Requirement, cls).copy(requirements, default=default)
                    
    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee')

    @classmethod
    def default_department(cls):
        employee = Pool().get('company.employee').search([
            ('id', '=', Transaction().context.get('employee'))
        ])
        if employee:
            return employee[0].contract_department.id

    @fields.depends('lines')
    def on_change_with_total(self, name=None):
        total = Decimal('0.00')
        for line in self.line:
            total += line.total
        return total


    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.department = self.employee.contract_department
        else:
            self.department = None

    @fields.depends('employee', 'department')
    def on_change_with_locations(self, name=None):
        locations = []
        if self.employee and self.department:
            if self.employee.locations:
                for location in self.employee.locations:
                    locations.append(location.location.id)
            elif self.department.locations:
                for location in self.department.locations:
                    locations.append(location.location.id)
        return locations

    @fields.depends('employee', 'department')
    def on_change_with_location(self, name=None):
        location = None
        if self.employee and self.department:
            if self.employee.locations:
                location = self.employee.locations[0].location
            elif self.department.locations:
                location = self.department.locations[0].location
        return None if location is None else location.id

    @classmethod
    def get_products(cls, requirements, names=None):
        result = defaultdict(lambda : None)
        for requirement in requirements:
            if requirement.line:
                description = reduce(lambda a, b:a+b, [
                    (x.product.template.name if x.product else x.description)
                    + ', ' for x in requirement.line])
                result[requirement.id] = description
        return {
            'products': result,
        }

    @classmethod
    def search_products(cls, name, clause):
        return [('line.description',) + tuple(clause[1:])]

    @staticmethod
    def default_date():
        return date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, requirements):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    @set_employee('request_by')
    def request(cls, requirements):
        pool = Pool()
        Date = pool.get('ir.date')
        RequirementBudget = pool.get('purchase.requirement.budget')
        RequirementLine = pool.get('purchase.requirement.line')
        Config = pool.get('purchase.configuration')
        config = Config(1)

        for row in requirements:
            row.request_date = Date.today()
            if len(row.line) <= 0:
                cls.raise_user_error('no_lines')
            for line in row.line:
                check_quantity = line.request_quantity > line.stock
                if len(line.requirement_budgets)==0 and check_quantity and (
                        line.process!='contract'):
                    if config.request_budgets_requirement: 
                        cls.raise_user_error('cero_budgets', {
                            'requirement': line.description,
                            'stock': line.stock,
                            'quantity': line.request_quantity,
                        })
                if line.pac_line:
                    if line.type == 'service':
                        if line.pac_line.purchase_type != 'property':
                            pass
                        else: 
                            cls.raise_user_error(RequirementLine._error_messages.get('pac_line_error', ''))
                    else:
                        if line.pac_line.purchase_type != 'property':
                            cls.raise_user_error(RequirementLine._error_messages.get('pac_line_error', ''))

        cls.save(requirements)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, requirements):
        pass

    @classmethod
    @ModelView.button
    @set_employee('done_by')
    @Workflow.transition('done')
    def done(cls, requirements):
        pool = Pool()
        Config = pool.get('purchase.configuration')
        Sequence = pool.get('ir.sequence')
        Date = pool.get('ir.date')
        config = Config(1)
        list_goods = []
        list_services = []
        for requirement in requirements:
            requirement.done_date = Date.today()
            if requirement.number == '/':
                if config.requirement_sequence:
                    requirement.number = Sequence.get_id(
                        config.requirement_sequence.id)
                else:
                    requirement.raise_user_error('no_sequence_defined')
            for line in requirement.line:
                if line.aproved_quantity == 0.0:
                    cls.raise_user_warning(
                        'cero_aproved_quantity', 'cero_aproved_quantity',
                        {'requirement': line.description})
                if ((line.type == 'service' or (
                        line.process and line.process == 'contract')) and
                        line.aproved_quantity > Decimal("0.0")):
                    list_services.append(line)
                elif line.aproved_quantity > Decimal("0.0"):
                    list_goods.append(line)
                #update budgets#TODO: borrar

                '''for budget in line.requirement_budgets:
                    budget.quantity = line.aproved_quantity
                    budget.save()'''

        if len(list_goods) > 0:
            cls.create_material_requirement(
                requirements[0], 'goods', list_goods)

        if len(list_services) > 0:
            cls.create_material_requirement(
                requirements[0], 'services', list_services)
        cls.save(requirements)

    def create_material_requirement(self, state, list):
        pool = Pool()
        Date = pool.get('ir.date')
        MaterialRequirement = pool.get('purchase.material.requirement')
        MaterialRequirementLine = pool.get(
            'purchase.material.requirement.line')
        MaterialRequirementLocationHistory = pool.get(
            'purchase.material.requirement.location.history')
        from_location = None
        if self.employee.locations:
            from_location = self.employee.locations[0].location
        elif self.department.locations:
            from_location = self.department.locations[0].location
        if not from_location:
            self.raise_user_error('update_employee_department_warehouse')
        employee = list[0].requirements.employee
        new = MaterialRequirement(
            date=Date.today(),
            state='request' if state == 'goods' else 'confirmed',
            employee=employee,
            department=employee.contract_department
        )
        lines = []
        for row in list:
            line = MaterialRequirementLine(
                requirement=new,
                from_location=from_location,
                product=row.product,
                description=row.description,
                uom=row.uom,
                request_quantity=row.aproved_quantity,
                requirement_line_origin=row,
            )
            line.stock = create_location_by_product(line)
            lines.append(line)
        warehouse = MaterialRequirementLocationHistory(
            requirement=new,
            from_location=from_location,
        )
        new.locations = [warehouse]
        new.lines = lines
        new.save()
        if state == 'services':
            MaterialRequirement.done([new])

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('employee.party.name',) + tuple(clause[1:]),
                  ('department.name',) + tuple(clause[1:]),
                  ('products',) + tuple(clause[1:]),
                  ]
        return domain


class PurchaseBudget(ModelSQL, ModelView):
    'Purchase Requirement Budget Line'
    __name__ = 'purchase.requirement.budget'

    requirement = fields.Many2One(
        'purchase.requirement.line', 'Requerimiento', ondelete='CASCADE')
    budget = fields.Many2One(
        'public.budget', 'Partida',
        states={
            'readonly': Eval('state').in_(['done', 'cancel']),
        },
        depends=['budgets', 'state'])
    total_budguet = fields.Numeric('Total disponible', digits=(16, 4),
                                   states={'readonly': True,'required': True,})
    poa = fields.Many2One(
        'public.planning.unit', 'POA', states={'readonly': True,})
    quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ],
        states={
            'readonly': Eval('state').in_(['done', 'cancel']),
            'required': True,
        },
        depends=['requirement', 'state'])
    amount = fields.Numeric(
        'Valor', digits=(16, 4),
        states={'readonly': Eval('state').in_(['done', 'cancel']),},
        depends=['requirement', 'state'])
    state = fields.Function(fields.Char(
            'Estado', states={'readonly': True}), 'on_change_with_state')

    @classmethod
    def __setup__(cls):
        super(PurchaseBudget, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_uniq', Unique(t, t.requirement, t.budget),
                'Elimine las partidas duplicadas para continuar.'),
        ]
        cls._order = [
            ('requirement', 'ASC'),
            ('budget', 'ASC'),
        ]
        cls._error_messages.update({
            'error_amount': ('El valor por asignar es 0.00.\n Para agregar una '
            'nueva linea reduzca las asignacion en las partidas listadas'),
            'error_quantity': ('La cantidad por asignar es 0.\n Para agregar'
            ' una nueva linea reduzca la cantidad en las partidas listadas')
        })

    @classmethod
    def default_poa(cls):
        pool = Pool()
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        today = Date.today()
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])[0]
        return poa.id

    @classmethod
    def default_requirement(cls):
        pool = Pool()
        Requirement = pool.get('purchase.requirement.line')
        transaction = Transaction()
        context = transaction.context
        requirement = Requirement(context['active_id'])
        return requirement.id

    @fields.depends('quantity', 'requirement')
    def on_change_quantity(self):
        price_tag = self.requirement.price_tag
        if self.quantity and price_tag:
            self.amount = self.quantity*price_tag

    @fields.depends('requirement', '_parent_requirement.budgets')
    def on_change_requirement(self):
        context = Transaction().context
        remaining_value = context.get('remaining_value')
        remaining_quantity = context.get('remaining_quantity')
        aproved_quantity = self.requirement.aproved_quantity
        price_tag = self.requirement.price_tag
        self.state = self.requirement.requirements.state
        tot = Decimal(aproved_quantity)*price_tag
        new_quantity = (
            remaining_quantity if remaining_quantity else aproved_quantity
        )
        new_amount = remaining_value if remaining_value else tot
        if not remaining_value:
            total_amount_line = Decimal(0.0)
            total_quantity_line = Decimal(0.0)
            for budget in self.requirement.requirement_budgets:
                total_amount_line += budget.amount
                total_quantity_line += budget.quantity
            new_amount = tot - total_amount_line
            new_quantity = aproved_quantity - total_quantity_line

        if self.requirement.type == 'service':
            if remaining_value == 0:
                self.raise_user_error('error_amount')
            else:
                self.amount = new_amount
            self.quantity = 1
        else:
            if remaining_quantity == 0:
                self.raise_user_error('error_quantity')
            else:
                self.quantity = new_quantity

            if remaining_value == 0:
                self.raise_user_error('error_amount')
            else:
                self.amount = Decimal(new_quantity)*price_tag


    @fields.depends('budget')
    def on_change_budget(self):
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            self.total_budguet = budget_data.certified_pending
        else:
            self.total_budguet = None

    @fields.depends('requirement', '_parent_requirement.budgets')
    def on_change_with_state(self, name=None):
        if self.requirement:
            return self.requirement.requirements.state
        else:
            return 'draft'


class RequirementLine(ModelSQL, ModelView):
    'Requirement Line'
    __name__ = 'purchase.requirement.line'

    _STATES = {
        'readonly': ~Eval('state').in_(['draft']),
    }
    _DEPENDS = ['state']

    requirements = fields.Many2One(
        'purchase.requirement', 'Requerimiento', ondelete='CASCADE')
    product = fields.Many2One(
        'product.product', 'Producto',
        context={
            'with_childs': True,
            'stock_date_end': Eval('_parent_requirements', {}).get('date',None),
            'locations': Eval('_parent_requirements', {}).get('locations', -1)},
        depends=_DEPENDS + ['requirements'],states=_STATES)
    stock = fields.Numeric(
        'Stock', digits=(16, 2),
        states={
            'readonly': True,
            'required': True,
        })
    description = fields.Char(
        'Genérico ', depends=_DEPENDS, states=_STATES, required=True)
    observations = fields.Text(
        'Observaciones', depends=_DEPENDS, states=_STATES)
    uom = fields.Many2One(
        'product.uom', 'Udm', states=_STATES, depends=_DEPENDS, required=True)
    request_quantity = fields.Numeric(
        'Cantidad solicitada', digits=(16, 2),
        domain=[
            If(Eval('type')=='service',
                ('request_quantity', '=', 1),
                ('request_quantity', '>', 0)
            )
        ], states={
            'readonly': (~Eval('state').in_(['draft']) | 
                Bool(Eval('type')=='service'))
        }, depends=_DEPENDS+ ['type'], required=True)
    aproved_quantity = fields.Numeric(
        'Cantidad Aprobada', digits=(16, 2),
        domain=[
            If(Eval('type')=='service',
                ('aproved_quantity', '=', 1),
                ('aproved_quantity', '>=', 0)
            )
        ], states={
            'readonly': ~((Eval('state') == 'request') & (
                Eval('context', {}).get('groups',[]).contains(
                    Id('public_pac','group_requirements_done'))) & ~(
                Equal(Eval('context', {}).get('employee'),Eval('employee'))))
        },
        depends=_DEPENDS+['type'], required=True)
    state = fields.Function(
        fields.Selection(
            Requirement.state.selection, 'Estado',
            states={'invisible': True, }), 'on_change_with_state')
    type = fields.Selection([
        ('goods', 'Articulos'),
        ('service', 'Servicio'),
        ('assets', 'Activo'), ],
        'Tipo', depends=_DEPENDS, states=_STATES)
    type_to_translated = type.translated('type')
    price_tag = fields.Numeric(
        'Precio Referencial', digits=(16, 4), depends=_DEPENDS,
        domain=[
            ('price_tag', '>=', 0)
        ],states=_STATES, required=True)
    requirement_budgets = fields.One2Many(
        'purchase.requirement.budget', 'requirement', 'Partidas')
    pac_line = fields.Many2One('purchase.pac.line', 'Linea del PAC')
    process = fields.Selection([
        (None, ''),
        ('request', 'Solicitud'),
        ('contract', 'Contratacion'),
    ], 'Proceso sugerido', sort=False)
    process_to_translated = process.translated('process')
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 4)),
        'on_change_with_total'
    )

    @classmethod
    def __setup__(cls):
        super(RequirementLine, cls).__setup__()
        cls._buttons.update({
            'details': {
                'readonly': Eval('requirements.state').in_(['draft']),
                'depends': ['requirements']
            },
            'budgets': {
                'readonly': Eval('requirements.state').in_(['draft']),
                'depends': ['budgets']
            }
        })
        cls._error_messages.update({
            'error_amount': (
                'La suma de los valores de las partidas deben ser iguales a '
                'el total del item'),
            'pac_line_error': (
                'Linea del PAC no concuerda con el tipo seleccionado en la linea.'
                )
        })
        cls._order = [
            ('description', 'ASC'),
        ]

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('aproved_quantity', 0)
        return super(RequirementLine, cls).copy(lines, default=default)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_type():
        return 'goods'

    @staticmethod
    def default_aproved_quantity():
        return Decimal("0")

    @staticmethod
    def default_request_quantity():
        return Decimal("0")

    @staticmethod
    def default_stock():
        return Decimal("0")

    @staticmethod
    def default_price_tag():
        return 0

    @fields.depends('request_quantity')
    def on_change_request_quantity(self):
        self.aproved_quantity = self.request_quantity

    @fields.depends('requirements', 'product')
    def on_change_with_stock(self, name=None):
        stock = Decimal("0")
        if self.product and self.requirements.locations:
            Product = Pool().get('product.product')
            Date = Pool().get('ir.date')
            with Transaction().set_context(with_childs=True,
                    locations=[l.id for l in self.requirements.locations],
                    stock_date_end=Date.today()):
                product = Product(self.product.id)
                stock = Decimal(str(product.forecast_quantity)).quantize(
                    Decimal("0.01"))
        return stock

    @fields.depends('price_tag', 'request_quantity', 'aproved_quantity')
    def on_change_with_total(self, name=None):
        tot = 0
        if self.request_quantity:
            if self.aproved_quantity != Decimal(0.0):
                tot = self.aproved_quantity * self.price_tag
            else:
                tot = self.request_quantity * self.price_tag
        return tot

    @fields.depends('requirements')
    def on_change_with_state(self, name=None):
        if self.requirements:
            return self.requirements.state
        else:
            return 'draft'

    @fields.depends('product', 'description', 'uom')
    def on_change_product(self, names=None):
        if self.product:
            self.description = self.product.name
            self.uom = self.product.default_uom
            self.type = self.product.type
        else:
            self.description = None
            self.uom = None
            self.type = 'goods'

    @classmethod
    @ModelView.button_action(
        'public_pac.wizard_purchase_requirement_line_detail')
    def details(cls, lines):
        pass

    @classmethod
    @ModelView.button_action('public_pac.wizard_purchase_requirement_budget')
    def budgets(cls, requests):
        pass

    def get_rec_name(self, name):
        return f"{self.description}"


class RequirementLineTracking(ModelView, ModelSQL):
    'Requirement Line Tracking'
    __name__ = 'purchase.requirement.line.tracking'

    requirement = fields.Many2One(
        'purchase.requirement', 'Requerimiento'
    )
    requirement_line = fields.Many2One(
        'purchase.requirement.line', 'Solicitud'
    )
    material_requierment_line = fields.Many2One(
        'purchase.material.requirement.line', 'Despacho'
    )
    material_requierment_line_state = fields.Function(
        fields.Char('Estado - Despacho'),
        'on_change_with_material_requierment_line_state'
    )
    consolidation_line = fields.Many2One(
        'purchase.consolidation.line', 'Consolidacion'
    )
    consolidation_line_state = fields.Function(
        fields.Char('Estado - Consolidacion'),
        'on_change_with_consolidation_line_state'
    )
    consolidation_line_observation = fields.Function(
        fields.Char('Observacion - Consolidacion'),
        'on_change_with_consolidation_line_observation'
    )
    consolidation_line_process = fields.Function(
        fields.Char('Proceso - Consolidacion'),
        'on_change_with_consolidation_line_process'
    )

    @classmethod
    def __setup__(cls):
        super(RequirementLineTracking, cls).__setup__()

    @fields.depends('material_requierment_line')
    def on_change_with_material_requierment_line_state(self, name=None):
        state = ''
        if self.material_requierment_line and (
                self.material_requierment_line.requirement) and (
                self.material_requierment_line.requirement.state_to_translated):
            state = (
                self.material_requierment_line.requirement.state_to_translated)
        return state

    @fields.depends('consolidation_line')
    def on_change_with_consolidation_line_state(self, name=None):
        state = ''
        if self.consolidation_line and (
                self.consolidation_line.purchase_consolidation) and (
                self.consolidation_line.purchase_consolidation.state_to_translated):
            state = (
                self.consolidation_line.purchase_consolidation.state_to_translated)
        return state

    @fields.depends('consolidation_line')
    def on_change_with_consolidation_line_observation(self, name=None):
        observation = ''
        if self.consolidation_line and (
                self.consolidation_line.purchase_consolidation) and (
                self.consolidation_line.purchase_consolidation.observation):
            observation = (
                self.consolidation_line.purchase_consolidation.observation)
        return observation

    @fields.depends('consolidation_line')
    def on_change_with_consolidation_line_process(self, name=None):
        process = ''
        if self.consolidation_line and (
                self.consolidation_line.process_to_translated) and (
                self.consolidation_line.purchase_consolidation.state not in
                ['cancel', 'terminate']):
            process = self.consolidation_line.process_to_translated
        return process

    @classmethod
    def table_query(cls):
        pool = Pool()
        PurchaseRequirementLine = pool.get('purchase.requirement.line')
        MaterialRequirementLine = pool.get('purchase.material.requirement.line')
        PurchaseConsolidationLine = pool.get('purchase.consolidation.line')
        PurchaseConsolidationDetail = pool.get('purchase.consolidation.detail')
        PurchaseConsolidationLineDetail = pool.get(
            'purchase.consolidation.line.detail')
        r_l = PurchaseRequirementLine.__table__()
        m_r_l = MaterialRequirementLine.__table__()
        c_l = PurchaseConsolidationLine.__table__()
        c_d = PurchaseConsolidationDetail.__table__()
        c_l_d = PurchaseConsolidationLineDetail.__table__()
        query_consolidation = c_l.join(
            c_l_d, condition=c_l.id == c_l_d.line
        ).join(
            c_d, condition=c_d.id == c_l_d.detail
        ).select(
            c_d.requirement_material_line,
            c_l.id.as_('consolidation_line')
        )
        query = r_l.join(
            m_r_l, type_='LEFT',
            condition=r_l.id == m_r_l.requirement_line_origin
        ).join(
            query_consolidation, type_='LEFT',
            condition=query_consolidation.requirement_material_line == m_r_l.id
        ).select(
            RowNumber(window=Window([])).as_('id'),
            r_l.requirements.as_('requirement'),
            r_l.id.as_('requirement_line'),
            m_r_l.id.as_('material_requierment_line'),
            query_consolidation.consolidation_line,
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            order_by=[r_l.id]
        )
        return query


class RequirementLineDetail(ModelSQL, ModelView):
    'Requirement Line Detail'
    __name__ = 'purchase.requirement.line.detail'

    line = fields.Many2One(
        'purchase.requirement.line', 'Linea del requirimiento', required=True,
        states={
            'readonly': True,
        }, ondelete='CASCADE')

    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    detail = fields.Char(
        'Detalle', required=True,
        states={
            'readonly': Eval('state') != 'draft',
        }, depends=['line', 'state'])

    @classmethod
    def __setup__(cls):
        super(RequirementLineDetail, cls).__setup__()
        cls._order = [
            ('line', 'ASC'),
            ('detail', 'ASC'),
        ]

    @fields.depends('line')
    def on_change_with_state(self, name=None):
        if self.line:
            return self.line.state
        return ''


class EnterRequirementLineDetailStart(ModelView):
    'Enter Requirement Line Detail Start'
    __name__ = 'purchase.requirement.line.detail.enter.start'

    line = fields.Many2One(
        'purchase.requirement.line', 'Linea del requirimiento', required=True,
        states={
            'invisible': True,
        })

    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    detail = fields.One2Many(
        'purchase.requirement.line.detail', None, 'Detalles',
        states={
            'readonly': Eval('state') != 'draft',
        },
        domain=[
            ('line', '=', Eval('line')),
        ], depends=['line', 'state']
    )

    @staticmethod
    def default_line():
        context = Transaction().context
        pool = Pool()
        Line = pool.get('purchase.requirement.line')
        line = Line(context.get('active_id'))
        return line.id

    @staticmethod
    def default_detail():
        context = Transaction().context
        pool = Pool()
        Detail = pool.get('purchase.requirement.line.detail')
        details = Detail.search([
            ('line', '=', context.get('active_id')),
        ])
        return [row.id for row in details]

    @fields.depends('line')
    def on_change_with_state(self, name=None):
        if self.line:
            return self.line.state
        return ''


class EnterRequirementLineDetail(Wizard):
    'Enter Requirement Line Detail'
    __name__ = 'purchase.requirement.line.detail.enter'

    start = StateView(
        'purchase.requirement.line.detail.enter.start',
        'public_pac.requirement_line_detail_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        detail_ids = []
        for row in self.start.detail:
            to_save.append(row)
            detail_ids.append(row.id)
        pool = Pool()
        Detail = pool.get('purchase.requirement.line.detail')
        details = Detail.search([
            ('id', 'not in', detail_ids),
            ('line', '=', self.start.line.id)
        ])
        if details:
            Detail.delete(details)
        if to_save:
            Detail.save(to_save)
        return 'end'


class MaterialRequirement(Workflow, ModelSQL, ModelView):
    'Material Requirement'
    __name__ = 'purchase.material.requirement'
    _history = True

    number = fields.Char('Número', states={'readonly': True})
    company = fields.Many2One(
        'company.company', 'Compañia', readonly=True, required=True)
    employee = fields.Many2One(
        'company.employee', 'Requiriente',
        states={
            'readonly': ~((Eval('state') == 'draft') &(
                Eval('context', {}).get('groups', []).contains(
                    Id('public_pac','group_public_pac_edit'))))
        }, required=True, depends=['state'])
    department = fields.Many2One(
        'company.department', 'Departamento', states={
            'readonly': True,
        }, required=True, depends=['state', 'employee'])
    date = fields.Date('Fecha', readonly=True, required=True)
    lines = fields.One2Many(
        'purchase.material.requirement.line', 'requirement', 'Requerimientos',
        states={
            'readonly': ~Eval('state').in_(['draft']),
        },
    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('request', 'Solicitado'),
        ('confirmed', 'Confirmado'),
        ('cancel', 'Cancelar'),
        ('done', 'Realizado'),
    ], 'Estado', readonly=True, required=True)
    state_to_translated = state.translated('state')
    visible_button_done = fields.Function(
        fields.Boolean('visible boton realizado', states={'invisible': True}),
        'on_change_with_visible_button_done')
    number_product_to_buy = fields.Function(fields.Integer(
        '# de productos a comprar'),
        'on_change_with_number_product_to_buy')
    locations = fields.One2Many(
        'purchase.material.requirement.location.history', 'requirement',
        'Bodegas', states={'readonly': True})
    user = fields.Function(
        fields.Many2One(
            'company.employee', 'Empleado', states={'invisible': True}),
        'on_change_with_user'
    )
    dispatchs = fields.Function(
        fields.One2Many('stock.shipment.internal', None, 'Egresos de bodega'),
        'on_change_with_dispatchs'
    )
    product_names = fields.Function(
        fields.Char('Productos'),
        'on_change_with_product_names',
        searcher='search_requirements_by_products'
    )
    number_request = fields.Function(
        fields.Char('N. Solicitud despacho'),
        'get_info',
        searcher='search_requirements_by_number'
    )
    is_to_dispatch_after_buy = fields.Function(
        fields.Boolean('To dispach after buy'),
        'on_change_with_is_to_dispatch_after_buy'
    )
    pending_dispatch = fields.Function(
        fields.Boolean('Pendiente de envio'),
        'get_pending_dispatch',
        searcher='search_pending_dispatch'
    )

    @classmethod
    def __setup__(cls):
        super(MaterialRequirement, cls).__setup__()
        cls._order = [
            ('company', 'ASC'),
            ('date', 'DESC'),
            ('employee.party.name', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'confirmed'),
            ('confirmed', 'request'),
            ('confirmed', 'done'),
            ('confirmed', 'cancel'),))
        cls._buttons.update({
            'request': {
                'invisible': ~(Eval('state').in_(['draft', 'confirmed'])),
                'depends': ['state'],
                'icon': If(Eval('state') == 'draft', 'tryton-forward',
                           'tryton-back'),
            },
            'confirmed': {
                'invisible': ~(Eval('state').in_(['request'])),
                'depends': ['state']
            },
            'done': {
                'invisible': ~Bool(Eval('visible_button_done')),
                'depends': ['state', 'visible_button_done']
            },
            'cancel': {
                'invisible': ~(Eval('state').in_(['confirmed'])),
                'depends': ['state']
            },
            'asign_to': {
                'invisible': ~(Eval('state').in_(['request'])),
                'depends': ['state']
            },
            'dispatch': {
                'invisible': ~(Eval('state').in_(['request'])),
                'depends': ['state']
            },
            'all_location': {
                'invisible': ~(Eval('state').in_(['request'])),
                'depends': ['state']
            },
            'to_dispatch_after_buy': {
                'invisible': Bool(Eval('is_to_dispatch_after_buy')),
                'depends': ['is_to_dispatch_after_buy'],
                'icon': 'tryton-back',
            }
        })
        cls._error_messages.update({
            'no_have_product': (
                'El/la %(description)s no tiene producto asigando'),
            'no_have_from_location': (
                'El/la %(description)s no tiene bodega orígen'),
            'no_sequence_defined': (
                'No existe secuencia definida para requerimientos de material.'
                'Por favor, diríjase a "Configuración de Compras" y '
                'configure una secuencia.'),
            'no_have_configuration_reason': (
                'No existe definida la razón.'
                'Por favor, diríjase a "Configuración de Compras".'),
            'lines_value_zero': (
                'Existe valores de cero en la cantidad a despachar'),
            'delete_no_draft': 'Debe eliminar solo registros en estado borrador.',
        })

    @classmethod
    def delete(cls, requirements):
        for requirement in requirements:
            if requirement.state not in ['draft']:
                cls.raise_user_error('delete_no_draft')
        super(MaterialRequirement, cls).delete(requirements)

    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee')

    @classmethod
    def default_department(cls):
        employee = Pool().get('company.employee').search([
            ('id', '=', Transaction().context.get('employee')),
        ])
        if employee:
            return employee[0].contract_department.id

    @staticmethod
    def default_date():
        return date.today()

    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.department = self.employee.contract_department
        else:
            self.department = None

    @fields.depends('lines')
    def on_change_with_product_names(self, name=None):
        products = f""
        for line in self.lines:
            products += line.description + ','
        return products

    @classmethod
    def search_requirements_by_products(cls, name, clause):
        return [
            ('lines.requirement_line_origin.description',) +
            tuple(clause[1:])]

    @classmethod
    def get_pending_dispatch(cls, requirements, names=None):
        p_dict = defaultdict(lambda : None)
        for row in requirements:
            temp = True
            if row.state == 'done':
                for line in row.lines:
                    if (line.product and line.product.type == 'service') or (
                            line.requirement_line_origin and (
                            line.requirement_line_origin.type == 'service')
                    ):
                        temp = True
                        break
                    if line.purchase_quantity > Decimal("0"):
                        temp = False
                        break
            p_dict[row.id] = temp
        return {
            'pending_dispatch': p_dict,
        }

    @classmethod
    def search_pending_dispatch(cls, name, clause):
        cursor = Transaction().connection.cursor()
        pool = Pool()

        Purchase = pool.get('purchase.purchase')
        purchases = Purchase.search_read([
            ('state', '=', 'done'),
        ], fields_names=['id', 'requirements'])
        requirement_purchase = ''
        if purchases:
            requirement_purchase = ','.join(
                [x['requirements'] for x in purchases])

        MaterialRequirement = pool.get('purchase.material.requirement')
        MaterialRequirementLine = pool.get('purchase.material.requirement.line')
        RequirementLine = pool.get('purchase.requirement.line')
        Requirement = pool.get('purchase.requirement')
        Product = pool.get('product.product')
        Template = pool.get('product.template')

        template = Template.__table__()
        product = Product.__table__()
        requirement = Requirement.__table__()
        requirement_line = RequirementLine.__table__()
        material_requirement = MaterialRequirement.__table__()
        material_requirement_line = MaterialRequirementLine.__table__()

        where = (material_requirement.state == 'done') & (
            material_requirement_line.recommend_buy == 't') & (
            requirement_line.type != 'service')

        query = material_requirement.join(
            material_requirement_line,
            condition=(
                    material_requirement.id ==
                    material_requirement_line.requirement)
        ).join(
            requirement_line, type_='LEFT',
            condition=(
                    requirement_line.id ==
                    material_requirement_line.requirement_line_origin)
        ).join(
            requirement, type_='LEFT',
            condition=(requirement.id == requirement_line.requirements)
        ).join(
            product, type_='LEFT',
            condition=(product.id == material_requirement_line.product)
        ).join(
            template, type_='LEFT',
            condition=(template.id == product.template)
        ).select(
            material_requirement_line.id,
            template.type.as_('type_line'),
            material_requirement.id.as_('id_material'),
            requirement.number,
            where=where,
            group_by=[
                material_requirement_line.id, template.type,
                material_requirement.id, requirement.number],
            having=((material_requirement_line.request_quantity -
                     material_requirement_line.delivered_quantity) >
                    Decimal("0")),
            order_by=[material_requirement.id]
        )

        condition = 'in' if clause[2] else 'not in'
        ids = {}

        cursor.execute(*query)
        result = cursor.fetchall()
        for id, type_line, id_material, number in result:
            if type_line != 'service' and number in requirement_purchase:
                ids[id_material] = True
        return [('id', condition, list(ids.keys()))]

    @fields.depends('lines', 'state')
    def on_change_with_is_to_dispatch_after_buy(self, name=None):
        if self.state != 'done':
            return True
        temp = True
        number = None
        for line in self.lines:
            if (line.product and line.product.type == 'service') or (
                line.requirement_line_origin and (
                    line.requirement_line_origin.type == 'service')
            ):
                temp = True
                break
            if line.purchase_quantity > Decimal("0"):
                number = line.requirement_line_origin.requirements.number
                break
        if number:
            Purchase = Pool().get('purchase.purchase')
            purchases = Purchase.search_read([
                ('state', '=', 'done'),
            ], fields_names=['id', 'requirements'])
            for row in purchases:
                if number in row['requirements']:
                    temp = False
                    break
        else:
            temp = True
        return temp

    @fields.depends('lines')
    def on_change_with_dispatchs(self, name=None):
        to_list = []
        if self.lines:
            for line in self.lines:
                if line.move:
                    to_list.append(line.move.shipment.id)
        return to_list

    @fields.depends('state')
    def on_change_with_user(self, name=None):
        return Transaction().context.get('employee')

    @fields.depends('lines')
    def on_change_with_number_product_to_buy(self, name=None):
        number = 0
        if self.lines:
            for line in self.lines:
                if line.purchase_quantity > Decimal("0.0"):
                    number += 1
        return number

    @fields.depends('state', 'lines')
    def on_change_with_visible_button_done(self, name=None):
        if self.state and self.state == 'confirmed':
            for row in self.lines:
                if row.move and row.move.shipment.state not in ['done']:
                    return False
            return True
        return False

    @classmethod
    def get_info(cls, requirements, names=None):
        number_dict = defaultdict(lambda : None)
        for requirement in requirements:
            for line in requirement.lines:
                if line.requirement_line_origin:
                    if line.requirement_line_origin.requirements:
                        number_dict[requirement.id] = (
                            line.requirement_line_origin.requirements.number)
                        break
        return {
            'number_request': number_dict,
        }

    @classmethod
    def search_requirements_by_number(cls, name, clause):
        return [
            ('lines.requirement_line_origin.requirements.number',) +
            tuple(clause[1:])]

    @classmethod
    @ModelView.button_action('public_pac.act_purchase_asign_to_location')
    def asign_to(cls, requirements):
        pass

    @classmethod
    def to_dispatch_after_buy(cls, requirements):
        to_save = []
        for row in requirements:
            row.state = 'request'
            for line in row.lines:
                if line.purchase_quantity > Decimal("0"):
                    line.stock = create_location_by_product(line)
                    to_save.append(line)
        cls.save(requirements)
        if to_save:
            Line = Pool().get('purchase.material.requirement.line')
            Line.save(to_save)

    @classmethod
    @ModelView.button_action('public_pac.act_purchase_location_all_lines')
    def all_location(cls, requirements):
        pass

    @classmethod
    @ModelView.button
    def dispatch(cls, requirements):
        Employee = Pool().get('company.employee')
        employee = Employee(Transaction().context.get('employee'))
        dict_warehouse = defaultdict(lambda: defaultdict(lambda: []))
        for requirement in requirements:
            for line in requirement.lines:
                if line.move is None and (employee in line.from_location.users):
                    if line.delivered_quantity > Decimal("0"):
                        dict_warehouse[
                            line.from_location][line.to_location].append(line)
        if dict_warehouse:
            for requirement in requirements:
                cls.generate_shipment(requirement, dict_warehouse)

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, requirements):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, requirements):
        cls.dispatch(requirements)
        for requirement in requirements:
            value_zero = False
            for line in requirement.lines:
                if line.delivered_quantity == Decimal("0"):
                    value_zero = True
            if value_zero:
                cls.raise_user_warning(
                    'lines_value_zero', 'lines_value_zero', {})

    def generate_shipment(self, dict_warehouse):
        pool = Pool()
        Config = pool.get('purchase.configuration')
        reason = Config(1).get_multivalue('shipment_reason')
        if not reason:
            self.raise_user_error('no_have_configuration_reason', {})
        ShipmentInternal = pool.get('stock.shipment.internal')
        StockMove = pool.get('stock.move')
        Line = pool.get('purchase.material.requirement.line')
        to_save_shipments = []
        to_save_moves = []
        to_save_lines = []
        for from_location in dict_warehouse:
            for to_location in dict_warehouse.get(from_location):
                shipment = ShipmentInternal()
                shipment.from_location = from_location
                shipment.to_location = to_location
                shipment.planned_date = self.date
                shipment.planned_start_date = (
                    shipment.on_change_with_planned_start_date())
                shipment.reference = (
                    f"RM: {self.department.name} - "
                    f"{self.employee.party.name}")
                shipment.reason = reason
                shipment.employee = self.employee
                shipment.on_change_employee()
                to_save_shipments.append(shipment)
                for row in dict_warehouse[from_location][to_location]:
                    if row.delivered_quantity > Decimal("0") and (
                            row.from_location == from_location):
                        cost_price = 0
                        if row.product.cost_price:
                            cost_price = row.product.cost_price
                        move = StockMove()
                        move.shipment = shipment
                        move.product = row.product
                        move.uom = row.uom
                        move.quantity = float(row.delivered_quantity)
                        move.from_location = from_location
                        move.to_location = to_location
                        move.cost_price = Decimal(cost_price).quantize(Decimal('.000001'))
                        move.employee = shipment.employee
                        move.department = shipment.department
                        to_save_moves.append(move)
                        row.move = move
                        to_save_lines.append(row)
        if to_save_shipments and to_save_moves:
            ShipmentInternal.save(to_save_shipments)
            StockMove.save(to_save_moves)
            ShipmentInternal.wait(to_save_shipments)
            if ShipmentInternal.assign_try(to_save_shipments) == True:
                ShipmentInternal.assign(to_save_shipments)
                StockMove.assign(to_save_moves)
            Line.save(to_save_lines)
            notify('Despacho de bodega', 'El albarán fue generado exitosamente')

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, requirements):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('purchase.configuration')
        config = Config(1)
        to_save = []
        for requirement in requirements:
            if requirement.number == '/':
                if config.requirement_material_sequence:
                    requirement.number = Sequence.get_id(
                        config.requirement_material_sequence.id)
                else:
                    requirement.raise_user_error('no_sequence_defined')
            for line in requirement.lines:
                if line.move:
                    line.delivered_quantity = Decimal(
                        str(line.move.quantity))
                    to_save.append(line)
        if to_save:
            MaterialRequirementLine = pool.get(
                'purchase.material.requirement.line')
            MaterialRequirementLine.save(to_save)
        cls.save(requirements)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, data):
        pass

    def get_rec_name(self, name):
        return (f"{self.number} - {self.employee.party.name} - "
                f"{self.department.name}")


class MaterialRequirementLine(ModelSQL, ModelView):
    'Material Requirement Line'
    __name__ = 'purchase.material.requirement.line'

    _STATES = {'readonly': True}

    requirement = fields.Many2One(
        'purchase.material.requirement', 'Requerimiento Materiales',
        required=True, ondelete='CASCADE')
    product = fields.Many2One(
        'product.product', 'Producto',
        states={
            'readonly': (Eval(
                '_parent_requirement', {}).get('state', '') != 'request') | (
                Eval('move')),
        }, depends=['requirement', 'move'])
    description = fields.Text('Descripcion', states=_STATES, required=True)
    uom = fields.Many2One('product.uom', 'Udm', states=_STATES, required=True)
    from_location = fields.Many2One(
        'stock.location', 'Desde ubicacion', states=_STATES, required=True)
    to_location = fields.Many2One(
        'stock.location', 'A ubicacion',
        states={
            'readonly': ~Eval('user_and_state'),
            'required': True,
        }, depends=['user_and_state']
    )
    stock = fields.Numeric('Stock', states=_STATES, required=True)
    request_quantity = fields.Numeric(
        'Cantidad solicitada', required=True, digits=(16, 2),
        states={
            'readonly': True,
        }, depends=['requirement'])
    delivered_quantity = fields.Numeric(
        'Cantidad a despachar', digits=(16, 2), required=True,
        states={
            'readonly': ~Eval('user_and_state'),
        },
        depends=['request_quantity', 'user_and_state', 'stock'])
    purchase_quantity = fields.Function(fields.Numeric(
        'Cantidad a comprar', digits=(16, 2)),
        'on_change_with_purchase_quantity')
    recommend_buy = fields.Boolean(
        'Recomienda comprar?',
        states={
            'readonly': Eval('_parent_requirement', {}).get(
                'state', '') != 'confirmed',
        }, depends=['requirement'])
    requirement_line_origin = fields.Many2One(
        'purchase.requirement.line', 'Requerimiento linea',
        readonly=True)
    requirement_line_origin_observation = fields.Function(
        fields.Char('Observacion'),
        'on_change_with_requirement_line_origin'
    )
    asigned_by = fields.Many2One(
        'company.employee', 'Asignado por', states=_STATES)
    parent = fields.Many2One(
        'purchase.material.requirement.line', 'Origen', states=_STATES)
    move = fields.Many2One('stock.move', 'Movimiento')
    move_state = fields.Function(
        fields.Selection([
            ('request', 'Solicitado'),
            ('draft', 'Borrador'),
            ('cancel', 'Cancelado'),
            ('waiting', 'En espera'),
            ('assigned', 'Reservado'),
            ('shipped', 'Enviado'),
            ('done', 'Realizado'),
        ], 'Estado albarán'), 'on_change_with_move_state')
    user_and_state = fields.Function(
        fields.Boolean('Usuario y estado'),
        'on_change_with_user_and_state'
    )

    @classmethod
    def __setup__(cls):
        super(MaterialRequirementLine, cls).__setup__()
        cls._error_messages.update({
            'value_zero_delivered_quantity': (
                'DESPACHO DE BODEGA \n\n'
                'La cantidad a despachar debe ser menor o igual al stock y/o a '
                'la cantidad solicitada \n\n'
                'Bodega origen: %(location)s con un stock de %(stock)s'),
        })

    @classmethod
    def validate(cls, lines):
        for line in lines:
            if line.delivered_quantity >= Decimal("0") and (
                    line.delivered_quantity <= line.request_quantity) and (
                    line.delivered_quantity <= line.stock):
                continue
            else:
                line.raise_user_error('value_zero_delivered_quantity', {
                    'location': line.from_location.name,
                    'stock': line.stock,
                })

    @staticmethod
    def default_delivered_quantity():
        return Decimal("0")

    @staticmethod
    def default_request_quantity():
        return Decimal("0")

    @staticmethod
    def default_recommend_buy():
        return True

    @staticmethod
    def default_to_location():
        pool = Pool()
        Config = pool.get('purchase.configuration')
        to_location = Config(1).get_multivalue('to_location')
        if to_location:
            return to_location.id

    @fields.depends('requirement', 'from_location', 'stock', 'move')
    def on_change_with_user_and_state(self, name=None):
        state = self.requirement.state
        employee_id = Transaction().context.get('employee')
        if state == 'request' and self.from_location and (
                self.stock > Decimal("0")) and not self.move:
            for assigned_by in self.from_location.users:
                if assigned_by.id == employee_id:
                    return True
        return False

    @fields.depends('delivered_quantity', 'request_quantity', 'recommend_buy')
    def on_change_with_purchase_quantity(self, name=None):
        request_quantity = (
            self.request_quantity if self.request_quantity else Decimal("0"))
        delivered_quantity = (
            self.delivered_quantity if self.delivered_quantity else Decimal("0"))
        if self.recommend_buy:
            return request_quantity - delivered_quantity
        else:
            return Decimal("0")

    @fields.depends('from_location', 'product')
    def on_change_product(self, names=None):
        self.stock = create_location_by_product(self)
        if self.product:
            self.uom = self.product.default_uom
        else:
            self.uom = None

    @fields.depends('requirement_line_origin')
    def on_change_with_requirement_line_origin(self, name=None):
        if self.requirement_line_origin:
            return self.requirement_line_origin.observation
        return ''

    def get_rec_name(self, name):
        return (f"{self.requirement.number} - {self.description}: "
                f"{self.requirement.department.name}")


class MaterialRequirementLocationHistory(ModelSQL, ModelView):
    'Material Requirement Location History'
    __name__ = 'purchase.material.requirement.location.history'

    _states = {
        'readonly': Eval(
            '_parent_requirement', {}).get('state', '') != 'request',
    }

    requirement = fields.Many2One(
        'purchase.material.requirement', 'Requerimiento Materiales',
        states={'readonly': True}, required=True, ondelete='CASCADE')
    from_location = fields.Many2One(
        'stock.location', 'Bodega', states={'readonly': True},
        required=True)


class MaterialRequirementLineProductStockPurhcase(ModelSQL, ModelView):
    'Material Requirement Line Product Stock Purhcase'
    __name__ = 'purchase.material.requirement.product.stock'

    line = fields.Many2One(
        'purchase.material.requirement.line', 'Linea', ondelete='CASCADE')
    product = fields.Function(
        fields.Many2One('product.product', 'Producto'),
        'on_change_with_product'
    )
    request_quantity = fields.Function(
        fields.Numeric('Cantidad solicitada'),
        'on_change_with_request_quantity'
    )
    stock = fields.Numeric('Stock', states={'readonly': True})
    asign_to = fields.Boolean('Asignar a')

    @staticmethod
    def default_stock():
        return Decimal("0")

    @staticmethod
    def default_request_quantity():
        return Decimal("0")

    @staticmethod
    def default_asign_to():
        return False

    @fields.depends('line')
    def on_change_with_product(self, name=None):
        if self.line and self.line.product:
            return self.line.product.id
        return None

    @fields.depends('line')
    def on_change_with_request_quantity(self, name=None):
        if self.line:
            return self.line.purchase_quantity
        return None


class EnterAsignToLotacionStart(ModelView):
    'Enter Asign To LotacionStart'
    __name__ = 'purchase.assign.to.location.enter.start'

    from_location = fields.Many2One(
        'stock.location', 'Bodega',
        domain=[
            ('id', 'in', Eval('from_location_lines', [-1])),
        ], depends=['from_location_lines'])
    from_location_lines = fields.One2Many('stock.location', None, 'Bodegas')
    product_stock = fields.One2Many(
        'purchase.material.requirement.product.stock', None, 'Productos',
        states={'readonly': True})

    @staticmethod
    def default_from_location_lines():
        pool = Pool()
        MaterialRequirement = pool.get('purchase.material.requirement')
        material_requirement, = MaterialRequirement.search([
            ('id', '=', Transaction().context.get('active_id')),
        ])
        locations = []
        if material_requirement.employee and material_requirement.department:
            if material_requirement.employee.locations:
                for location in material_requirement.employee.locations:
                    locations.append(location.location.id)
            elif material_requirement.department.locations:
                for location in material_requirement.department.locations:
                    locations.append(location.location.id)
        return locations

    @staticmethod
    def default_product_stock():
        pool = Pool()
        MaterialRequirement = pool.get('purchase.material.requirement')
        MaterialRequirementLineProductStockPurhcase = pool.get(
            'purchase.material.requirement.product.stock')
        Employee = pool.get('company.employee')
        employee = Employee(Transaction().context.get('employee'))
        to_delete = MaterialRequirementLineProductStockPurhcase.search([])
        if to_delete:
            MaterialRequirementLineProductStockPurhcase.delete(to_delete)
        material_requirement, = MaterialRequirement.search([
            ('id', '=', Transaction().context.get('active_id')),
        ])
        to_save = []
        for row in material_requirement.lines:
            if row.product and row.purchase_quantity > Decimal("0") and (
                    employee in row.from_location.users):
                new = MaterialRequirementLineProductStockPurhcase(
                    line=row,
                    request_quantity=row.purchase_quantity
                )
                to_save.append(new)
        if to_save:
            MaterialRequirementLineProductStockPurhcase.save(to_save)
        return [row.id for row in to_save]

    @fields.depends('product_stock', 'from_location')
    def on_change_from_location(self):
        for row in self.product_stock:
            stock = Decimal("0")
            if self.from_location:
                Product = Pool().get('product.product')
                Date = Pool().get('ir.date')
                with Transaction().set_context(
                        with_childs=True,
                        locations=[self.from_location.id],
                        stock_date_end=Date.today()):
                    product = Product(row.product.id)
                    stock = Decimal(str(product.forecast_quantity)).quantize(
                        Decimal("0.01"))
            row.stock = stock


class EnterAsignToLotacion(Wizard):
    'Enter Asign To Lotacion'
    __name__ = 'purchase.assign.to.location.enter'

    start = StateView(
        'purchase.assign.to.location.enter.start',
        'public_pac.enter_asign_to_location_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'ok', 'tryton-ok', default=True),
        ])
    ok = StateTransition()

    def transition_ok(self):
        if self.start.from_location and self.start.product_stock:
            pool = Pool()
            MaterialRequirementLine = pool.get(
                'purchase.material.requirement.line')
            MaterialRequirementLocationHistory = pool.get(
                'purchase.material.requirement.location.history')
            Employee = pool.get('company.employee')
            employee = Employee(Transaction().context.get('employee'))
            to_save = []
            to_update = []
            MaterialRequirement = pool.get(
                'purchase.material.requirement')
            requirement = MaterialRequirement(
                Transaction().context.get('active_id'))
            for row in self.start.product_stock:
                if row.asign_to and row.stock > Decimal("0"):
                    if row.line.delivered_quantity > Decimal("0"):
                        line = MaterialRequirementLine(
                            requirement=requirement,
                            from_location=self.start.from_location,
                            product=row.line.product,
                            description=row.line.description,
                            uom=row.line.uom,
                            request_quantity=row.line.purchase_quantity,
                            requirement_line_origin=row.line.requirement_line_origin,
                            asigned_by=employee,
                            parent=row.line,
                        )
                        line.stock = create_location_by_product(line)
                        to_save.append(line)
                        row.line.request_quantity = row.line.delivered_quantity
                    else:
                        row.line.from_location = self.start.from_location
                        row.line.stock = row.stock
                    to_update.append(row.line)
            warehouse = MaterialRequirementLocationHistory(
                requirement=requirement,
                from_location=self.start.from_location,
            )
            if to_save or to_update:
                if to_save:
                    MaterialRequirementLine.save(to_save)
                if to_update:
                    MaterialRequirementLine.save(to_update)
                warehouse.save()
        return 'end'


class EnterRequirementBudget(ModelView):
    'Enter requirement budget'
    __name__ = 'purchase.requirement.budget.enter.start'

    requirement = fields.Many2One(
        'purchase.requirement.line', 'Requerimiento',
        states={
            'readonly': True,
            'invisible': True
        }
    )
    requirement_type = fields.Function(
        fields.Char('Tipo de requerimiento'),
        'on_change_with_requirement_type'
    )
    list_budgets = fields.Function(fields.One2Many(
        'public.budget', None, 'Lista partidas'),
        'on_change_with_list_budgets')
    price_tag = fields.Function(
        fields.Numeric('Valor de linea', digits=(6, 4)),
        'on_change_with_price_tag'
    )
    remaining_value = fields.Function(
        fields.Numeric('Valor por Asignar', digits=(6, 4)),
        'on_change_with_remaining_value'
    )
    remaining_quantity = fields.Function(
        fields.Numeric('remaining_quantity'),
        'on_change_with_remaining_quantity'
    )
    total = fields.Function(
        fields.Numeric('Subtotal', digits=(6, 4)),
        'on_change_with_total'
    )
    total_general = fields.Function(
        fields.Numeric('Total', digits=(6, 2)),
        'on_change_with_total_general'
    )
    budgets = fields.One2Many(
        'purchase.requirement.budget', None, 'Partidas',
        domain=[
            If(((Eval('process')=='request') & ~(Bool(Eval('pac_line')))),
               [('budget.type', 'in', ['expense']),
                ('budget.activity', 'child_of', Eval('poa', -1), 'parent')],
               [('budget', 'in', Eval('list_budgets'))])
        ],
        context={
            'remaining_value':Eval('remaining_value', 0),
            'remaining_quantity':Eval('remaining_quantity', 0),
        },
        states={
            'readonly': Eval('state') != 'draft',
            'required': If(Eval('process')=='request', True, False),
            'invisible': If(Eval('process')=='contract', True, False),
        }, depends=[
            'state',
            'process',
            'poa',
            'pac_line',
            'list_budgets',
            'remaining_value',
            'remaining_quantity'
        ]
    )
    state = fields.Function(fields.Char(
        'Estado', states={'invisible': True}),
        'on_change_with_state'
    )
    pac_line = fields.Many2One(
        'purchase.pac.line', 'Linea del PAC',
        domain=domain_quarter()+[
            If(Eval('requirement_type') == 'service',
            [('purchase_type', '!=', 'property')],
            [('purchase_type', '=', 'property')]),
            ('pac_budget.purchase_pac.state', '=', 'approved'),
            ('reform_state', 'in', ['added', None])
        ],
        states={
            'readonly': Eval('state') != 'draft',
            'required': If(Eval('process') == 'contract', True, False),
            'invisible': ~Eval('process').in_(['request', 'contract']),
        }, depends=['requirement_type', 'state', 'process']
    )
    poa = fields.Many2One(
        'public.planning.unit', 'POA',
        states={'invisible': True}
    )
    pac_line_budgets = fields.Function(
        fields.One2Many(
            'purchase.pac.line.budget', None, 'Partidas',
            states={
                'invisible': Eval('process')!='contract',
            }, depends=['process']
        ),
        'on_change_with_pac_line_budgets'
    )
    process = fields.Selection([
        (None, ''),
        ('request', 'Solicitud'),
        ('contract', 'Contratacion'),
        ], 'Proceso sugerido', sort=False,
        states={
            'readonly': Eval('state')!='draft',
        }, depends=['state']
    )

    @fields.depends('requirement')
    def on_change_with_requirement_type(self, name=None):
        type = ''
        if self.requirement:
            type = self.requirement.type
        return type

    @fields.depends('requirement', 'budgets', 'pac_line')
    def on_change_with_total(self, name=None):
        total = Decimal(0.0)
        for budget in self.budgets:
            if budget.amount:
                total += budget.amount
        return total

    @fields.depends('requirement', 'budgets', 'pac_line')
    def on_change_with_total_general(self, name=None):
        total = Decimal(0.0)
        for budget in self.budgets:
            if budget.amount:
                total += budget.amount
        return total.quantize(Decimal('0.01'))

    @fields.depends('requirement', 'budgets')
    def on_change_with_price_tag(self, name=None):
        return self.requirement.price_tag * self.requirement.aproved_quantity

    @fields.depends('requirement', 'budgets', 'pac_line')
    def on_change_with_remaining_value(self, name=None):
        aproved_quantity = self.requirement.aproved_quantity
        price_tag = self.requirement.price_tag
        tot = Decimal(aproved_quantity)*price_tag
        amount_budget = Decimal('0.0')
        for budget in self.budgets:
            if budget.amount:
                amount_budget += budget.amount
        return tot - amount_budget

    @fields.depends('requirement', 'budgets', 'pac_line')
    def on_change_with_remaining_quantity(self, name=None):
        quantity = Decimal('0.00')
        for budget in self.budgets:
            if budget.quantity:
                quantity += budget.quantity
        return self.requirement.aproved_quantity - quantity

    @fields.depends('pac_line', 'process', 'budgets', 'requirement')
    def on_change_pac_line(self, name=None):
        #if not self.pac_line:
            #self.budgets = []
        if self.process and self.process == 'request' and self.pac_line and (
                not self.budgets):
            to_list = []
            BudgetLine = Pool().get('purchase.requirement.budget')
            for budget in self.pac_line.budgets:
                if budget.certified_pending > Decimal("0"):
                    new = BudgetLine(
                        requirement=self.requirement,
                        quantity=1, 
                        budget=budget.budget,
                        amount=budget.amount,
                    )
                    new.on_change_budget()
                    to_list.append(new)
            self.budgets = to_list

    @fields.depends('process', 'pac_line')
    def on_change_with_list_budgets(self, name=None):
        allow_budgets = []
        if not self.process or self.process == 'contract':
            pass
        elif self.process and self.process == 'request' and self.pac_line and (
                self.pac_line.budgets):
            allow_budgets = [b.budget.id
                    for b in self.pac_line.budgets
                    if b.certified_pending > Decimal("0")]

        return allow_budgets

    @fields.depends('pac_line', 'process')
    def on_change_with_pac_line_budgets(self, name=None):
        if self.pac_line and self.pac_line.budgets and self.process and (
                self.process=='contract'):
            return [row.id for row in self.pac_line.budgets]
        return []

    @fields.depends('requirement')
    def on_change_with_state(self, name=None):
        if self.requirement and self.requirement.requirements:
            return self.requirement.requirements.state
        return ''
    #TODO: test to calculate amount  pending to budgets



class EnterRequestBudgetWizard(Wizard):
    'Enter Request Budget Wizard'
    __name__ = 'purchase.requirement.budget.enter'

    start = StateView(
        'purchase.requirement.budget.enter.start',
        'public_pac.purchase_requirement_budget_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'run', 'tryton-ok', default=True)
        ]
    )

    run = StateTransition()

    @classmethod
    def __setup__(cls):
        super(EnterRequestBudgetWizard, cls).__setup__()
        cls._error_messages.update({
            'invalid_quantity': (
                'La cantidad en el detalle presupuestario debe '
                'ser igual a la cantidad de la linea del requerimiento.\n'
                '"%(bQuantity)s" -- "%(lQuantity)s"'),
        })

    def default_start(self, fields):
        context = Transaction().context
        pool = Pool()
        RequirementLine = pool.get('purchase.requirement.line')
        requirement = RequirementLine(context.get('active_id'))
        BudgetLine = pool.get('purchase.requirement.budget')
        budgets = BudgetLine.search([
            ('requirement', '=', context.get('active_id')),
        ])
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        today = Date.today()
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])[0]

        return {
            'pac_line': requirement.pac_line.id if requirement.pac_line else None,
            'process': requirement.process,
            'requirement': requirement.id,
            'budgets': [row.id for row in budgets],
            'poa': poa.id,
            'price_tag': requirement.price_tag
        }

    def transition_run(self):
        pool = Pool()
        BudgetLine = pool.get('purchase.requirement.budget')
        Requirement = pool.get('purchase.requirement.line')
        to_save = []
        purchase_tot = Decimal('0.0000')
        requirement = self.start.requirement
        requirement.pac_line = self.start.pac_line
        requirement.process = self.start.process
        requirement.save()

        quantity_budget = 0
        no_founds = []
        for row in self.start.budgets:
            if row.total_budguet < row.amount:
                no_founds.append(row)
            quantity_budget += row.quantity
            budget_line = BudgetLine(
                requirement=requirement,
                budget=row.budget,
                amount=row.amount,
                quantity=row.quantity,
                total_budguet=row.total_budguet
            )
            to_save.append(budget_line)
            purchase_tot += row.amount
        if (no_founds):
            message = ''
            for item in no_founds:
                message += "La paritda "+ str(item.budget.code)+ " no tiene fondos.\n" 
            message += "Debe elegir una partida con fondos o solicitar una reforma presupuestaria"
            self.raise_user_warning("Advertencia", message)

        to_delete = BudgetLine.search([(
            ('requirement', '=', self.start.requirement.id)
        )])
        if requirement.type != 'service':
            if to_save:
                if requirement.aproved_quantity != quantity_budget:
                    self.raise_user_error("invalid_quantity", {
                        'bQuantity': quantity_budget,
                        'lQuantity': requirement.aproved_quantity
                    })
        if requirement.pac_line:
            if requirement.type == 'service':
                if requirement.pac_line.purchase_type != 'property':
                    pass
                else: 
                    self.raise_user_error(Requirement._error_messages.get('pac_line_error', ''))
            else:
                if requirement.pac_line.purchase_type != 'property':
                    self.raise_user_error(Requirement._error_messages.get('pac_line_error', ''))

        if to_save and self.start.remaining_value != 0:
            self.raise_user_error('El valor de la linea es diferente al valor total del detalle presupuestario')


        BudgetLine.delete(to_delete)
        BudgetLine.save(to_save)

        #TODO: eliminar
        #requirement.price_tag = purchase_tot
        #requirement.total = requirement.on_change_with_total()
        #requirement.save()
        return 'end'

    def end(self):
        return 'reload'


class EnterLocationAllLinesStart(ModelView):
    'Enter Location All Lines  Start'
    __name__ = 'purchase.location.all.lines.enter.start'

    to_location = fields.Many2One(
        'stock.location', 'Bodega',
        domain=[
            ('id', 'in', Eval('locations')),
        ], depends=['locations'])
    locations = fields.One2Many('stock.location', None, 'Bodegas')

    @staticmethod
    def default_locations():
        pool = Pool()
        Line = pool.get('purchase.material.requirement.line')
        lines = Line.search([
            ('requirement', '=', Transaction().context.get('active_id')),
            ('move', '=', None),
        ])
        location_ids = []
        if lines:
            location_ids = [l.from_location.id for l in lines]
        Location = pool.get('stock.location')
        locations = Location.search([
            ('id', 'not in', location_ids),
        ])
        if locations:
            return [l.id for l in locations]


class EnterLocationAllLines(Wizard):
    'Enter Location All Lines'
    __name__ = 'purchase.location.all.lines.enter'

    start = StateView(
        'purchase.location.all.lines.enter.start',
        'public_pac.enter_location_all_lines_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'ok', 'tryton-ok', default=True),
        ])
    ok = StateTransition()

    def transition_ok(self):
        if self.start.to_location:
            pool = Pool()
            Line = pool.get('purchase.material.requirement.line')
            lines = Line.search([
                ('requirement', '=', Transaction().context.get('active_id')),
                ('move', '=', None),
            ])
            for line in lines:
                line.to_location = self.start.to_location
            if lines:
                Line.save(lines)
        return 'end'
