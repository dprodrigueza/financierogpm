from decimal import Decimal
from dateutil.relativedelta import relativedelta
from collections import defaultdict
from functools import reduce

from sql import Literal
from sql.functions import CurrentTimestamp
from sql.operators import Concat

from io import BytesIO
import PyPDF2

from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, If, Get
from trytond.model import ModelView, ModelSQL, fields, Workflow, Unique
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.bus import notify
from trytond.config import config


__all__ = [
    'ContractProcess', 'ContractProcessPayment', 'ContractProcessPaymentLine',
    'ContractProcessFunding', 'ContractProcessWarranty', 'ContractProductTaxes',
    'ContractProcessMultiannual', 'ContractProcessPaymentLineAux',
    'ContractProcessPpu', 'ContractProduct', 'ContractSupplierLine',
    'ContractRequirement', 'ContractQuotation', 'ContractProcessResponsible',
    'ContractProcessConsolidationLine', 'ContractProcessConsolidationLineTax',
    'ContractProcessConsolidationLineBudget', 'ContractProcessStartReason',
    'ContractProcessAdjustment',
    'TDRBudget', 'TDROfferEvaluation', 'TDRMinimumEquipment',
    'TDRMinimumExperience', 'TDRTechnicalStaff', 'TDRSpecificObjetive',
    'TDRTechnicalSpecification', 'TDRDeliverie', 'TDRGuarantee',
    'TDRPayment', 'EnterContractRequirement', 'EnterContractRequirementWizard',
    'EnterPurchaseContractQuotation', 'EnterPurchaseContractQuotationStart',
    'EnterContractProcessPaymentLine', 'EnterContractProcessPaymentLineStart',
    'EnterPurchaseContractConsolidationLine',
    'EnterPurchaseContractConsolidationLineStart',
    'EnterConsolidationLineTax', 'EnterConsolidationLineTaxStart',
    'EnterConsolidationLineBudget', 'EnterConsolidationLineBudgetStart',
    'ContractProcessBalanceAmortized', 'ContractProcessBalanceAmortizedContext',
    'EnterProductTax', 'EnterProductTaxStart'
]


STATES = {
    'readonly': Eval('_parent_process', {}).get('state', '') != 'draft',
    'required': True,
}

DEPENDS = ['process']

TYPE_PAYMENT = [
    ('advance', 'Anticipo'),
    ('payment', 'Pago'),
]

_CENT = Decimal("0.01")
product_digits = (16, 4)


class ContractProcessStartReason(ModelSQL, ModelView):
    'Contract Process Start Reason'
    __name__ = 'purchase.contract.process.start.reason'

    name = fields.Char('Razón')

    @classmethod
    def __setup__(cls):
        super(ContractProcessStartReason, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]


class TDRProcess(ModelSQL, ModelView):
    'TDRProcess'

    process = fields.Many2One(
        'purchase.contract.process', 'Proceso', required=True,
        states={'readonly': True}, ondelete='CASCADE')


class ContractProduct(TDRProcess):
    'Contract Product'
    __name__ = 'purchase.contract.process.product'

    product = fields.Many2One(
        'product.product', 'Producto',
        states={
            'readonly': Eval('_parent_process', {}).get(
                'state', '').in_(['close', 'cancel']),
            'required': True,
        }, depends=DEPENDS)
    taxes = fields.One2Many(
        'purchase.contract.process.product.tax', 'product', 'Impuestos',
        states = {
            'readonly': True,
        }, depends=DEPENDS)

    @classmethod
    def __setup__(cls):
        super(ContractProduct, cls).__setup__()
        cls._order = [
            ('product.name', 'ASC'),
        ]
        cls._buttons.update({
            'button_taxes': {
                'readonly': False,
            },
        })

    @classmethod
    @ModelView.button_action(
        'public_pac.act_purchase_contract_product_tax_start')
    def button_taxes(cls, records):
        pass

    def get_rec_name(self, name):
        product_name = ''
        if self.product:
            product_name = f"{self.product.name}"
        return f"{product_name}"


class ContractProductTaxes(ModelSQL, ModelView):
    'ContractProductTaxes'
    __name__ = 'purchase.contract.process.product.tax'

    product = fields.Many2One(
        'purchase.contract.process.product', 'Producto', required=True,
        ondelete='CASCADE')
    tax = fields.Many2One(
        'account.tax', 'Linea de impuestos', required=True)


class ContractProcessMultiannual(TDRProcess):
    'Contract Process Multiannual'
    __name__ = 'purchase.contract.process.multiannual'

    year = fields.Char('Año', states=STATES, depends=DEPENDS)
    budget = fields.Many2One(
        'public.budget', 'Partida',
        domain=[
            ('kind', '=', 'view'),
            ('level', '=', 4),
            ('type', '=', 'expense'),
        ], states=STATES, depends=DEPENDS)
    amount = fields.Numeric(
        'Monto anual', digits=(16, 2), states=STATES, depends=DEPENDS
    )

    @classmethod
    def __setup__(cls):
        super(ContractProcessMultiannual, cls).__setup__()
        cls._order = [
            ('year', 'ASC'),
            ('budget', 'ASC'),
        ]

    @staticmethod
    def default_amount():
        return Decimal("0")


class ContractProcessPpu(TDRProcess):
    'Contract Process Ppu'
    __name__ = 'purchase.contract.process.ppu'

    ppu = fields.Many2One(
        'public.planning.unit', 'Programa', required=True,
        domain=[
            ('type', 'in', ['program', 'project']),
            ('parent', 'child_of', Eval('_parent_process', {}).get('poa', -1),
             'parent'),
        ], states=STATES, depends=DEPENDS)


class TDRBudget(TDRProcess):
    'TDR Budget'
    __name__ = 'purchase.contract.process.tdr.budget'

    budget = fields.Many2One(
        'public.budget', 'Partida(s)',
        domain=[
            If(Get(Eval('_parent_process', {}), 'pac_line', None) == None,
               [('type', 'in', ['expense']),
                ('activity', 'child_of', Eval('_parent_process', {}).get(
                    'ppu', [-1]), 'parent')],
               [('id', 'in', Eval('_parent_process', {}).get(
                   'pac_line_budgets', [-1]))])
        ], states={
            'readonly': True,
            'required': True,
        }, depends=DEPENDS)
    description = fields.Text(
        'Descripcion', states={
            'readonly': True,
            'required': True,
        }
    )
    total_budguet = fields.Function(
        fields.Numeric('Total disponible', digits=(16, 2)),
        'on_change_with_total_budguet')
    unit_price = fields.Numeric(
        'Cantidad a utilizar', digits=(16, 2),
        domain=[
            ('unit_price', '>=', Decimal("0")),
        ], states={
            'readonly': True,
            'required': True,
        }, depends=DEPENDS
    )

    @classmethod
    def __setup__(cls):
        super(TDRBudget, cls).__setup__()
        cls._order = [
            ('budget', 'ASC'),
        ]

    @staticmethod
    def default_unit_price():
        return Decimal("0")

    @fields.depends('budget')
    def on_change_budget(self):
        if self.budget:
            self.description = self.budget.name
        else:
            self.description = None

    @fields.depends('budget')
    def on_change_with_total_budguet(self, name=None):
        total = Decimal("0")
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            total = budget_data.certified_pending
        return total


class TDROfferEvaluation(TDRProcess):
    'TDR Offer Evaluation'
    __name__ = 'purchase.contract.process.tdr.offer.evaluation'

    required_parameter = fields.Char(
        'Paramentro requerido', states=STATES, depends=DEPENDS
    )
    assigned_percentage = fields.Numeric(
        'Porcentaje asignado',
        domain=[
            ('assigned_percentage', '>', Decimal("0")),
            ('assigned_percentage', '<=', Decimal("1")),
        ], states=STATES, depends=DEPENDS
    )

    @classmethod
    def __setup__(cls):
        super(TDROfferEvaluation, cls).__setup__()
        cls._order = [
            ('assigned_percentage', 'DESC'),
        ]

    @staticmethod
    def default_assigned_percentage():
        return Decimal("0")


class TDRMinimumEquipment(TDRProcess):
    'TDR Minimum Equipment'
    __name__ = 'purchase.contract.process.tdr.minimum.equipment'

    quantity = fields.Integer(
        'Cantidad', states=STATES, depends=DEPENDS,
        domain=[
            ('quantity', '>', 0),
        ]
    )
    name = fields.Char(
        'Nombre', states=STATES, depends=DEPENDS
    )
    description = fields.Char(
        'Descripcion', states=STATES, depends=DEPENDS
    )
    observation = fields.Text(
        'Observacion',
        states={
            'readonly': Eval('_parent_process', {}).get('state', '') != 'draft',
        }, depends=DEPENDS
    )

    @staticmethod
    def default_quantity():
        return 0


class TDRMinimumExperience(TDRProcess):
    'TDR Minimum Experience'
    __name__ = 'purchase.contract.process.tdr.minimum.experience'

    description = fields.Char(
        'Description', states=STATES, depends=DEPENDS
    )
    minimum_time = fields.Integer(
        'Tiempo minimo (años)', states=STATES, depends=DEPENDS,
        domain=[
            ('minimum_time', '>', 0),
        ]
    )
    number_project = fields.Integer(
        'Numero de proyectos', states=STATES, depends=DEPENDS,
        domain=[
            ('number_project', '>', 0),
        ]
    )
    amount_proyect = fields.Numeric(
        'Monto de proyectos', digits=(16, 2), states=STATES, depends=DEPENDS,
        domain=[
            ('amount_proyect', '>', Decimal("0")),
        ]
    )

    @staticmethod
    def default_minimum_time():
        return 1

    @staticmethod
    def default_number_project():
        return 1

    @staticmethod
    def default_amount_proyect():
        return Decimal("0")


class TDRTechnicalStaff(TDRProcess):
    'TDR Technical Staff'
    __name__ = 'purchase.contract.process.tdr.technical.staff'

    position = fields.Char(
        'Cargo', states=STATES, depends=DEPENDS
    )
    education_level = fields.Selection([
        (None, ''),
        ('basic', 'Básica'),
        ('medium', 'Media'),
        ('superior', 'Superior')
    ], 'Education Level', states=STATES, depends=DEPENDS)

    education_level_translated = education_level.translated('education_level')

    academic_title = fields.Char(
        'Titulo academico', states=STATES, depends=DEPENDS
    )
    quantity = fields.Integer(
        'Cantidad', states=STATES, depends=DEPENDS,
        domain=[
            ('quantity', '>', 0),
        ]
    )

    @staticmethod
    def default_education_level():
        return None

    @staticmethod
    def default_quantity():
        return 0


class TDRSpecificObjetive(TDRProcess):
    'TDR Specific Objetive'
    __name__ = 'purchase.contract.process.tdr.specific.objetive'

    description = fields.Char('Descripcion', states=STATES, depends=DEPENDS)

    @classmethod
    def __setup__(cls):
        super(TDRSpecificObjetive, cls).__setup__()
        cls._order = [
            ('description', 'ASC'),
        ]


class TDRTechnicalSpecification(TDRProcess):
    'TDR Technical Specification'
    __name__ = 'purchase.contract.process.tdr.technical.specification'

    description = fields.Char('Descripcion', states=STATES, depends=DEPENDS)

    @classmethod
    def __setup__(cls):
        super(TDRTechnicalSpecification, cls).__setup__()
        cls._order = [
            ('description', 'ASC'),
        ]


class TDRDeliverie(TDRProcess):
    'TDR Deliverie'
    __name__ = 'purchase.contract.process.tdr.deliverie'

    description = fields.Char('Descripcion', states=STATES, depends=DEPENDS)
    date = fields.Date('Fecha tentativa', states=STATES, depends=DEPENDS)
    effective_date = fields.Date(
        'Fecha efectiva', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(TDRDeliverie, cls).__setup__()
        cls._order = [
            ('date', 'ASC'),
        ]


class TDRGuarantee(TDRProcess):
    'TDR Guarantee'
    __name__ = 'purchase.contract.process.tdr.guarantee'

    type = fields.Many2One(
        'work.warranty.type', 'Tipo', states=STATES, depends=DEPENDS)
    percentage = fields.Numeric(
        'Porcentaje',
        domain=[
            ('percentage', '>=', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ], states=STATES, depends=DEPENDS
    )
    renovation_days = fields.Integer(
        'Dias de renovacion', states=STATES, depends=DEPENDS,
        domain=[
            ('renovation_days', '>', 0),
        ]
    )

    @staticmethod
    def default_percentage():
        return Decimal("0")

    @staticmethod
    def default_renovation_days():
        return 1

    @fields.depends('type')
    def on_change_with_percentage(self, name=None):
        percentage = Decimal("0")
        if self.type:
            percentage = self.type.percentage
        return percentage


class TDRPayment(TDRProcess):
    'TDR Payment'
    __name__ = 'purchase.contract.process.tdr.payment'

    percentage = fields.Numeric(
        'Porcentaje',
        domain=[
            ('percentage', '>', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ], states=STATES, depends=DEPENDS)

    @staticmethod
    def default_percentage():
        return Decimal("0")


class ContractRequirement(ModelSQL, ModelView):
    'Contract Requirement'
    __name__ = 'purchase.contract.requirement'

    requirement = fields.Char('Requerimiento', required=True)
    parameter_offered = fields.Char('Parametro ofertado')
    assigned_percentage = fields.Numeric(
        'Porcentaje asignado', states={'required': True},
        domain=[
            ('assigned_percentage', '>=', Decimal("0")),
            ('assigned_percentage', '<=', Decimal("1")),
        ])
    met_percentage = fields.Numeric(
        'Porcentaje cumplido',
        states={
            'readonly': Eval('type_') != 'quantitative'
        }, domain=[
            ('met_percentage', '>=', Decimal("0")),
            ('met_percentage', '<=', Decimal("1")),
        ])
    met_check = fields.Boolean(
        'Cumple', states={
            'readonly': Eval('type_') != 'qualitative'
        },)
    type_ = fields.Selection([
        ('quantitative', 'Cuantitativo'),
        ('qualitative', 'Cualitativo')
    ], 'Tipo', required=True)
    supplier_line = fields.Many2One(
        'purchase.contract.supplier.line', 'Proveedor')


class ContractQuotation(ModelSQL, ModelView):
    'Contract Quotation'
    __name__ = 'purchase.contract.quotation'

    supplier_line = fields.Many2One(
        'purchase.contract.supplier.line', 'Proveedor', required=True)
    consolidation_line = fields.Many2One(
        'purchase.contract.process.consolidation.line', 'Linea consolidacion',
        ondelete='CASCADE'
    )
    product = fields.Function(
        fields.Many2One('product.product', 'Producto'),
        'on_change_with_product'
    )
    purchase_quantity = fields.Function(
        fields.Numeric('Cantidad de compra', digits=(16, 2)),
        'on_change_with_purchase_quantity'
    )
    unit_price = fields.Numeric(
        'Precio unitario', digits=(16, 4),
        states={
            'readonly': Eval('state') != 'validated',
        }, depends=['state']
    )
    amount_tax = fields.Function(
        fields.Numeric('Impuesto',  digits=(16, 4)),
        'on_change_with_amount_tax'
    )
    total = fields.Function(
        fields.Numeric('total', digits=(16, 2)),
        'on_change_with_total'
    )
    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    @staticmethod
    def default_unit_price():
        return Decimal("0")

    @fields.depends('supplier_line')
    def on_change_with_state(self, name=None):
        if self.supplier_line:
            return self.supplier_line.state
        return 'draft'

    @fields.depends('unit_price')
    def on_change_with_total(self, name=None):
        purchase_quantity = self.on_change_with_purchase_quantity()
        unit_price = (
            Decimal("0") if self.unit_price is None else self.unit_price)
        amount_tax = self.on_change_with_amount_tax()
        return ((purchase_quantity * unit_price) + amount_tax).quantize(
            Decimal("0.00"))

    @fields.depends('consolidation_line')
    def on_change_with_product(self, name=None):
        if self.consolidation_line:
            return self.consolidation_line.consolidation_line.product.id
        return None

    @fields.depends('consolidation_line')
    def on_change_with_purchase_quantity(self, name=None):
        if self.consolidation_line:
            return self.consolidation_line.consolidation_line.purchase_quantity
        return None

    @fields.depends('purchase_quantity', 'unit_price', 'consolidation_line')
    def on_change_with_amount_tax(self, name=None):
        taxes_amount = Decimal('0')
        unit_price = (
            Decimal("0") if self.unit_price is None else self.unit_price)
        quantity = (Decimal("0") if self.purchase_quantity is None
                    else self.purchase_quantity)
        if self.consolidation_line:
            for tax in self.consolidation_line.taxes:
                taxes_amount += tax.rate * (unit_price * quantity)
        return taxes_amount.quantize(Decimal("0.0000"))


class ContractSupplierLine(TDRProcess):
    'Purchase Contract Supplier Line'
    __name__ = 'purchase.contract.supplier.line'

    _states = {
        'readonly': Eval('_parent_process', {}).get('state', '') != 'validated',
    }
    _depends = ['process']

    party = fields.Many2One(
        'party.party', 'Proveedor', required=True,
        states={
            'readonly': Eval('_parent_process', {}).get('state', '') != 'confirm',  # noqa
        }, depends=_depends)
    requirements = fields.One2Many(
        'purchase.contract.requirement', 'supplier_line', 'Requisitos'
    )
    evaluation = fields.Binary(
        'Evaluacion', filename='evaluation_party', file_id='evaluation_path',
        states=_states, depends=_depends)
    evaluation_party = fields.Char('Evaluation party')
    evaluation_path = fields.Char('Evaluation path')
    quotations = fields.One2Many(
        'purchase.contract.quotation', 'supplier_line', 'Cotizacion',
    )
    quote = fields.Binary(
        'Cotizacion', filename='quote_party', file_id='quote_path',
        states=_states, depends=_depends)
    quote_party = fields.Char('Quote party')
    quote_path = fields.Char('Quote path')
    total_quotation = fields.Function(
        fields.Numeric('Total cotizacion', digits=(16, 2)),
        'on_change_with_total_quotation'
    )
    state = fields.Function(
        fields.Char('Estado'),
        'on_change_with_state'
    )

    @classmethod
    def __setup__(cls):
        super(ContractSupplierLine, cls).__setup__()
        cls._buttons.update({
            'requirements': {
                'depends': ['requirements']
            },
            'quotation': {
                'depends': ['quotation']
            },
        })

    @fields.depends('process')
    def on_change_with_state(self, name=None):
        if self.process:
            return self.process.state
        return 'draft'

    @fields.depends('quotations')
    def on_change_with_total_quotation(self, name=None):
        total = Decimal("0")
        for row in self.quotations:
            total += row.total
        return total.quantize(_CENT)

    @classmethod
    @ModelView.button_action('public_pac.wizard_purchase_contract_requirement')
    def requirements(cls, requests):
        pass

    @classmethod
    @ModelView.button_action('public_pac.act_purchase_contract_quotation_start')
    def quotation(cls, requests):
        pass

    def get_rec_name(self, name):
        return f"{self.party.name}"


class TDR(ModelSQL, ModelView):
    'TDR'

    _states = {
        'readonly': (Eval('state') != 'draft'),
    }

    _states_required = {
        'readonly': Eval('state') != 'draft',
        'required': Eval('state') != 'draft',
    }

    _depends = ['state']

    # 1
    tdr_summary = fields.Text(
        'Resumen ejecutivo', states=_states, depends=_depends)
    # 2
    tdr_background = fields.Text(
        'Antecedentes', states=_states, depends=_depends)
    # 3
    tdr_general_objective = fields.Text(
        'Objetivo General', states=_states, depends=_depends)
    tdr_specific_objective = fields.One2Many(
        'purchase.contract.process.tdr.specific.objetive', 'process',
        'Objetivos Especificos', states=_states, depends=_depends)
    # 4
    tdr_scope = fields.Text(
        'Alcance', states=_states, depends=_depends)
    # 5
    tdr_requirement_area = fields.Many2One('company.department',
        'Area requiriente', states=_states_required, depends=_depends)
    # 6
    tdr_location = fields.Char(
        'Ubicacion', states=_states, depends=_depends)
    # 7
    tdr_justify = fields.Text(
        'Justificacion', states=_states, depends=_depends)
    # 8
    tdr_company_information = fields.Text(
        'Informacion de la compañia', states=_states, depends=_depends
    )
    # 9
    tdr_hiring_object = fields.Text(
        'Objeto de contratacion', states=_states, depends=_depends
    )
    # 10
    tdr_definition_item = fields.Selection([
        ('normalized', 'Normalizado'),
        ('not_normalized', 'No normalizado'),
    ], 'Definicion', states=_states, depends=_depends)

    tdr_definition_item_translated = tdr_definition_item.translated(
        'tdr_definition_item')
    # 11
    tdr_work_methodology = fields.Text(
        'Metodologia de trabajo', states=_states, depends=_depends
    )
    # 12
    tdr_technical_specifications = fields.One2Many(
        'purchase.contract.process.tdr.technical.specification', 'process',
        'Especificaciones tecnicas', states=_states, depends=_depends
    )
    # 13
    tdr_technical_staffs = fields.One2Many(
        'purchase.contract.process.tdr.technical.staff', 'process',
        'Personal tecnico', states=_states, depends=_depends
    )
    tdr_minimum_experience = fields.One2Many(
        'purchase.contract.process.tdr.minimum.experience', 'process',
        'Experiencia minima', states=_states, depends=_depends
    )
    tdr_minimum_equipment = fields.One2Many(
        'purchase.contract.process.tdr.minimum.equipment', 'process',
        'Equipo minimo', states=_states, depends=_depends
    )
    # 14
    tdr_offer_evaluations = fields.One2Many(
        'purchase.contract.process.tdr.offer.evaluation', 'process',
        'Evaluaciones de la oferta', states=_states_required, depends=_depends
    )
    # 15
    tdr_budgets = fields.One2Many(
        'purchase.contract.process.tdr.budget', 'process',
        'Partida(s)', states=_states_required, depends=_depends
    )
    # 16
    tdr_certificate = fields.Many2One(
        'public.budget.certificate', 'Certificado presupuestario',
        context={
            'contract_process': True,
            'contract_poa': Eval('poa'),
        },
        states={
            'invisible': Bool(Eval('is_certificate_multiannual')),
            'readonly': Eval('state') != 'draft',
        }, depends=_depends + ['is_certificate_multiannual', 'poa'])
    tdr_certificate_multiannual = fields.Many2One(
        'public.pluri.certificate', 'Certificado plurianual',
        context={
            'contract_process_multiannual': True,
        },
        states={
            'invisible': ~Bool(Eval('is_certificate_multiannual')),
            'readonly': Eval('state') != 'draft',
        }, depends=_depends + ['is_certificate_multiannual', 'poa']
    )
    # 17
    tdr_advance = fields.Numeric(
        'Anticipo', states=_states_required, depends=_depends, digits=(16, 2),
        domain=[
            ('tdr_advance', '>=', Decimal("0")),
        ])
    tdr_payments = fields.One2Many(
        'purchase.contract.process.tdr.payment', 'process', 'Pagos',
        states=_states, depends=_depends)
    tdr_payments_total_percentage = fields.Function(
        fields.Numeric('Total porcentaje'),
        'on_change_with_tdr_payments_total_percentage'
    )
    # 18
    tdr_penalty = fields.Text(
        'Penalizacion', states=_states, depends=_depends
    )
    # 19
    tdr_number_days_deliveries = fields.Integer(
        'Plazo (# dias)', domain=[
            ('tdr_number_days_deliveries', '>', 0)
        ], states=_states_required, depends=_depends
    )
    tdr_deliveries = fields.One2Many(
        'purchase.contract.process.tdr.deliverie', 'process', 'Entregables',
        states=_states, depends=_depends)

    # 20
    tdr_guarantees = fields.One2Many(
        'purchase.contract.process.tdr.guarantee', 'process', 'Garantias',
        states=_states, depends=_depends)

    # 21
    tdr_administrator = fields.Many2One(
        'company.employee', 'Administrador de contrato',
        )

    @staticmethod
    def default_tdr_definition_item():
        return 'not_normalized'

    @staticmethod
    def default_tdr_number_days_deliveries():
        return Decimal("0")

    @staticmethod
    def default_tdr_advance():
        return Decimal("0")

    @fields.depends('tdr_payments')
    def on_change_with_tdr_payments_total_percentage(self, name=None):
        total = Decimal("0")
        for payment in self.tdr_payments:
            total += payment.percentage
        return total.quantize(_CENT)


class ContractProcess(Workflow, TDR):
    'Contract Process'
    __name__ = 'purchase.contract.process'

    _states = {
        'readonly': Eval('state') != 'draft',
    }
    _depends = ['state']

    type = fields.Selection([
        ('handiwork', 'Obra'),
        ('consultancy', 'Consultoria'),
        ('goods', 'Bienes'),
        ('services', 'Servicios'),
        ('other', 'Otro'),
    ], 'Tipo', required=True, states=_states, depends=_depends)
    type_translated = type.translated('type')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirm', 'Preparatorio'),
        ('validated', 'Precontractual'),
        ('awarded', 'Adjudicado'),
        ('open', 'Ejecución/control'),
        ('close', 'Finalizado'),
        ('cancel', 'Cancelado'),
    ], 'Estado', required=True, states={'readonly': True})
    state_translated = state.translated('state')
    internal_number = fields.Char(
        'Número de proceso', required=True,
        states={
            'readonly': ~Eval('state').in_(['awarded', 'validated', 'open']),
        }, depends=['state'])
    number = fields.Char(
        'Número de contrato', required=True, states={'readonly': True})
    name = fields.Char(
        'Nombre', required=True, states=_states, depends=_depends)
    tentative_start_date = fields.Date(
        'Fecha de inicio tentativa', required=True, states=_states,
        depends=_depends)
    tentative_end_date = fields.Function(
        fields.Date('Fecha de fin tentativa'),
        'on_change_with_tentative_end_date'
    )
    register_date = fields.Date(
        'Fecha de registro', states={'readonly': True})
    start_date = fields.Date(
        'Fecha inicio', states={
            'readonly': Eval('state') != 'awarded',
            'required': Eval('state') == 'open',
        }, depends=['state'],
        help="Ingrese la fecha de inicio según contrato"
    )
    end_date = fields.Date(
        'Fecha fin',
        states={
            'readonly': Eval('state') != 'open',
            'required': Eval('state') == 'close',
        }, depends=['state'],
        help="Ingrese la fecha de fin según contrato"
    )
    poa = fields.Many2One(
        'public.planning.unit', 'POA',
        domain=[
            ('type', '=', 'poa'),
            ('company', '=', Eval('company'))
        ], states={'readonly': True}, depends=['company'], required=True)
    pac_line = fields.Many2One(
        'purchase.pac.line', 'Linea del PAC',
        domain=[
            ('pac_budget.purchase_pac.state', '=', 'approved'),
            ('reform_state', 'in', ['added', None])
        ], states=_states, depends=_depends)
    pac_line_budgets = fields.Function(
        fields.One2Many('public.budget', None, 'Partidas pac line',
                        states={'invisible': True}),
        'on_change_with_pac_line_budgets'
    )
    funds = fields.One2Many(
        'purchase.contract.process.funding', 'process', 'Financiamiento',
        states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state') != 'draft',
        }, depends=_depends)
    funds_percentage = fields.Function(
        fields.Numeric('Total porcentage'),
        'on_change_with_funds_percentage'
    )
    payments = fields.One2Many(
        'purchase.contract.process.payment', 'process', 'Pagos',
        states={
            'readonly': Eval('state') != 'open',
        }, depends=_depends)
    untaxed_payments = fields.Function(
        fields.Numeric('Subtotal', digits=(16, 2)),
        'on_change_with_untaxed_payments'
    )
    taxed_payments = fields.Function(
        fields.Numeric('Impuestos', digits=(16, 2)),
        'on_change_with_taxed_payments'
    )
    total_payments = fields.Function(
        fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total_payments'
    )
    party = fields.Many2One(
        'party.party', 'Contratista',
        domain=[
            ('id', 'in', Eval('supplier_inviteds')),
        ],
        states={
            'readonly': Eval('state') != 'validated',
            'required': Eval('state') == 'awarded',
        }, depends=_depends + ['supplier_inviteds'])
    company = fields.Many2One(
        'company.company', 'Compañia', required=True, states={'readonly': True})
    consolidation_line = fields.One2Many(
        'purchase.contract.process.consolidation.line', 'process',
        'Productos consolidados', states=_states, depends=_depends)
    amount_contract = fields.Function(fields.Numeric(
        'Precio referencial (inc. iva)', digits=(16, 2)),
        'on_change_with_amount_contract')
    amount_budget = fields.Function(
        fields.Numeric('Presupuesto anual (inc. iva)', digits=(16, 2)),
        'on_change_with_amount_budget'
    )
    amount_budget_temp = fields.Function(
        fields.Numeric('Total utilizado', digits=(16, 2)),
        'on_change_with_amount_budget_temp'
    )
    total = fields.Function(
        fields.Numeric('Monto total de contrato (inc. iva)', digits=(16, 2)),
        'on_change_with_total'
    )
    warrantys = fields.One2Many(
        'purchase.contract.process.warranty', 'process', 'Garantias',
        states={
            'readonly': True,
        }, depends=_depends)
    warrantys_state = fields.Function(
        fields.Boolean('Estados de la garantia'),
        'on_change_with_warrantys_state'
    )
    inspector = fields.Many2One(
        'party.party', 'Fiscalizador')
    suppliers = fields.One2Many(
        'purchase.contract.supplier.line', 'process', 'Proveedores',
        states={
            'readonly': Eval('state') != 'confirm',
        }, depends=['state'])
    supplier_inviteds = fields.Function(
        fields.One2Many('party.party', None, 'Proveedores invitados'),
        'on_change_with_supplier_inviteds'
    )
    suggested_procedure = fields.Many2One(
        'purchase.pac.type.contract', 'Tipo de compra',
        domain=[
            ('used_on_public_purchases', '=', True),
        ],
        states={
            'readonly': Eval('state') != 'draft',
            'required': True,
        }, depends=_depends)
    ppu = fields.Many2Many(
        'purchase.contract.process.ppu', 'process', 'ppu', 'Programa',
        domain=[
            ('type', 'in', ['program', 'project']),
            ('parent', 'child_of', Eval('poa', -1), 'parent'),
        ],
        states=_states, depends=_depends + ['poa'])
    certificate_multiannual = fields.One2Many(
        'purchase.contract.process.multiannual', 'process',
        'Datos para el certificado plurianual',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': ~Bool(Eval('is_certificate_multiannual')),
        }, depends=['is_certificate_multiannual', 'state'])
    is_certificate_multiannual = fields.Boolean(
        'Es certificado plurianual?', states=_states, depends=_depends)
    observation = fields.Text(
        'Observacion',
        states={
            'readonly': Eval('state').in_(['close', 'cancel']),
        }, depends=_depends
    )
    products = fields.One2Many(
        'purchase.contract.process.product', 'process', 'Productos',
        states={
            'readonly': Eval('state').in_(['close', 'cancel']),
        }, depends=_depends)
    certificate_lines = fields.Function(
        fields.One2Many(
            'public.budget.certificate.line', None, 'Lineas del certificado'),
        'get_info'
    )
    responsible = fields.One2Many(
        'purchase.contract.process.responsible', 'process', 'Responsables',
        states={
            'readonly': Eval('state').in_(['cancel', 'close']),
        }, depends=['state'])
    attachment = fields.Function(fields.Char(
        'Archivos adjuntos'),
        'get_attachment', searcher='search_attachment'
    )
    project = fields.Many2One('project.work', 'Proyecto', domain=[
        ('type', '=', 'project'),
        ('phase', '=', 'approved')
    ])
    start_reason = fields.Many2One(
        'purchase.contract.process.start.reason', 'Razon de inicio de contrato',
        states={
            'readonly': Eval('state') != 'awarded',
            'required': Eval('state') == 'open',
        }, depends=['state']
    )

    @classmethod
    def __setup__(cls):
        super(ContractProcess, cls).__setup__()
        cls._order = [
            ('register_date', 'DESC'),
            ('name', 'ASC')
        ]
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'validated'),
            ('validated', 'confirm'),
            ('validated', 'awarded'),
            ('awarded', 'open'),
            ('open', 'close'),
            ('open', 'cancel'),
            ('close', 'cancel'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~(Eval('state').in_(['confirm'])),
            },
            'confirm': {
                'invisible': ~(Eval('state').in_(['draft', 'validated'])),
                'icon': If(Eval('state') == 'draft', 'tryton-forward',
                           'tryton-back'),
            },
            'validated': {
                'invisible': ~(Eval('state').in_(['confirm'])),
            },
            'awarded': {
                'invisible': ~(Eval('state').in_(['validated'])),
            },
            'open': {
                'invisible': ~(Eval('state').in_(['awarded'])),
            },
            'close': {
                'invisible': ~(Eval('state').in_(['open'])),
            },
            'cancel': {
                'invisible': ~(Eval('state').in_(['close', 'open'])),
            },
            'add_consolidation': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
            'add_budget': {
                'invisible': True,
                'depends': ['state']
            },
            'add_requirements': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
            'add_budget_multiannual': {
                'invisible': (Eval('state') != 'draft') | ~Bool(
                    Eval('is_certificate_multiannual')),
                'depends': ['state', 'is_certificate_multiannual']
            },
            'recalculate_budget': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
        })
        cls._error_messages.update({
            'cant_delete_certificate': ('No se puede regresar a borrador,'
                ' el certificado se encuentra aprobado.'),
            'certificate_isnt_done': (
                'El certificado no se encuentra aprobado.'),
            'no_suppliers': 'Proveedores invitados son requeridos',
            'no_warranty_active': 'Las garantias no se encuentran activas',
            'error_payments_percentage': 'Los pagos deben ser el 100%',
            'value_zero_in_budget': (
                'No debe existir partida(s) con monto a Presupuestar en 0'),
            'no_generate_warranty_payment': (
                'Para generar las garantias y pagos, primero debe seleccionar '
                'al contratista ganador y tener registrar el valor en la '
                'cotizacion'),
            'no_sequence_defined': (
                'No existe secuencia definida para contratos.'
                'Por favor, diríjase a "Configuración de Compras" y '
                'configure una secuencia.'),
            'warranty_zero_advances': (
                'No hay como generar la garantía %(description)s, debido a que '
                'depende del valor del anticipo'),
            'no_payment_with_advance': (
                'No debe existir un anticipo si no hay pagos'),
            'no_delete_certificate_multiannual': (
                'El certificado plurianual NO se va a eliminar automaticamente,'
                ' debido a que existen varias certificaciones anuales enlazadas.'  # noqa
                ' El proceso de eliminacion del certificado se debe hacer de '
                'manera manual. \nDesea que el sistema desvincule la '
                'certificacion con el contrato?'),
            'certificate_diff_done': (
                'El certificado presupuestario ingresado no esta aprobado, '
                'desea que el sistema genere uno nuevo?'),
            'certificate_no_valid': (
                'El certificado aprobado no concuerda con el contrato, debe '
                'tener el mismo departamento requiriente y el precio '
                'referencial como monto total del certificado'),
            'amount_budget_not_equal_amount_multiannual': (
                'El monto de presupuesto anual no concuerda con el monto del '
                'primer año del certificado plurianual'),
            'need_info_certificate_multiannual': (
                'Los Datos para el certificado plurianual es necesaria, eso lo '
                'puede encontrar en la sección de TDR / Partida(s)'),
            'delete_no_draft': 'Debe eliminar solo registros en estado borrador.',  # noqa
            'amount_must_be_minor_to_pending_certificate': (
                'Va a generar un certificado en el cual la partida '
                'presupuestaria no tiene suficientes fondos'),
            'no_have_products_consolidation': (
                'No existen productos consolidados para generar un certificado '
                'presupuestario'),
            'value_annual_no_equal': (
                'El precio referencial %(amount_budget)s no concuerda con el '
                'valor presupuestario en los productos consolidados %(total)s'),
            'value_multiannual_no_equal': (
                'El precio referencial %(amount_contract)s no concuerda con el '
                'total de impuestos de los productos consolidados %(total)s'),
            'purchase_not_state_draft': (
                'La orden de compra no esta en Borrador, primero debe volver al'
                ' estado indicado para poder cancelar el contrato')
        })

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
            query=False):
        context = Transaction().context
        if context.get('contract_process_adjustment'):
            new_domain = cls.search_contract_process_migrates()
            if new_domain:
                domain += new_domain
        return super(ContractProcess, cls).search(
            domain, offset=offset, limit=limit, order=order, count=count,
            query=query)

    @classmethod
    def search_contract_process_migrates(cls):
        new_domain = []
        ConsolidationLine = Pool().get(
            'purchase.contract.process.consolidation.line')
        consolidation_lines = ConsolidationLine.search_read(
            [], fields_names=['process'])
        if consolidation_lines:
            new_domain += [
                ('id', 'not in', [x['process'] for x in consolidation_lines]),
                ('state', '=', 'open'),
            ]
        return new_domain

    @classmethod
    def delete(cls, contracts):
        for contract in contracts:
            if contract.state not in ['draft']:
                cls.raise_user_error('delete_no_draft')
        super(ContractProcess, cls).delete(contracts)

    @classmethod
    def validate(cls, contracts):
        for contract in contracts:
            if contract.state not in ['draft', 'cancel']:
                for guarantee in contract.tdr_guarantees:
                    if guarantee.type.relation_to_advances and (
                            contract.tdr_advance == Decimal("0")):
                        contract.raise_user_error('warranty_zero_advances', {
                            'description': guarantee.type.name,
                        })
                for row in contract.warrantys:
                    if row.warranty.state != 'active' and (
                            contract.state == 'open'):
                        contract.raise_user_error('no_warranty_active')
                if contract.tdr_advance > Decimal("0") and (
                        len(contract.tdr_payments) == 0):
                    contract.raise_user_error('no_payment_with_advance')
                if contract.tdr_payments and (
                        contract.tdr_payments_total_percentage != Decimal("1")):
                    contract.raise_user_error('error_payments_percentage')

    @classmethod
    def get_attachment(cls, contracts, names=None):
        result = defaultdict(lambda: '')
        return {
            'attachment': result,
        }

    @classmethod
    def search_attachment(cls, name, clause):
        if not clause[2:][0].split('%')[1] or clause[2:][0].split('%')[1] == '':
            return [()]
        Contract = Pool().get('purchase.contract.process')
        contracts = Contract.search_read([], fields_names=['id'])
        dict_attachment = defaultdict(lambda: [])
        list_ids = [f"purchase.contract.process,{c['id']}" for c in contracts]
        Attachment = Pool().get('ir.attachment')
        attachments = Attachment.search_read([
            ('resource', 'in', list_ids),
            ('type', '=', 'data'),
        ], fields_names=['data', 'resource'])
        for values in attachments:
            dict_attachment[
                int(values['resource'].split(',')[1])].append(values['data'])
        ids = []
        for values in dict_attachment.items():
            text = ''
            for row in values[1]:
                try:
                    pdf_file = BytesIO(row)
                    read_pdf = PyPDF2.PdfFileReader(pdf_file)
                    for x in range(0, read_pdf.getNumPages()):
                        page = read_pdf.getPage(x)
                        text += page.extractText() + ' '
                except Exception:
                    pass
            if clause[2:][0].split('%')[1].upper() in text.upper():
                ids.append(values[0])
        return [('id', 'in', ids)]

    @staticmethod
    def default_poa():
        pool = Pool()
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        today = Date.today()
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])[0]
        return poa.id

    @staticmethod
    def default_is_certificate_multiannual():
        return False

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_internal_number():
        return '/'

    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_register_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_tentative_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('pac_line')
    def on_change_pac_line(self):
        TDRBudget = Pool().get('purchase.contract.process.tdr.budget')
        if self.pac_line:
            to_list = []
            for budget in self.pac_line.budgets:
                new = TDRBudget(
                    budget=budget.budget,
                    unit_price=budget.amount
                )
                new.on_change_budget()
                to_list.append(new)
            self.tdr_budgets = to_list
        else:
            self.tdr_budgets = []

    @fields.depends('payments')
    def on_change_with_untaxed_payments(self, name=None):
        amount = Decimal("0")
        for payment in self.payments:
            if payment.state == 'valid':
                amount += (
                    Decimal("0") if payment.amount is None else payment.amount)
        return amount.quantize(_CENT)

    @fields.depends('payments')
    def on_change_with_taxed_payments(self, name=None):
        taxes = Decimal("0")
        for payment in self.payments:
            if payment.state == 'valid':
                taxes += (
                    Decimal("0") if payment.taxes is None else payment.taxes)
        return taxes.quantize(_CENT)

    @fields.depends('payments')
    def on_change_with_total_payments(self, name=None):
        total = Decimal("0")
        for payment in self.payments:
            if payment.state == 'valid':
                total += payment.total
        return total.quantize(_CENT)

    @fields.depends('pac_line')
    def on_change_with_pac_line_budgets(self, name=None):
        if self.pac_line:
            return [b.budget.id for b in self.pac_line.budgets]
        return None

    @fields.depends('suppliers')
    def on_change_with_supplier_inviteds(self, name=None):
        to_list = []
        if self.suppliers:
            for row in self.suppliers:
                if row.party:
                    to_list.append(row.party.id)
            return to_list
        return None

    @fields.depends('certificate_multiannual', 'is_certificate_multiannual',
                    'amount_budget')
    def on_change_with_amount_contract(self, name=None):
        amount = Decimal("0")
        if self.is_certificate_multiannual:
            for row in self.certificate_multiannual:
                amount += row.amount
        else:
            amount += (Decimal("0")
                       if self.amount_budget is None else self.amount_budget)
        return amount

    @fields.depends('warrantys')
    def on_change_with_warrantys_state(self, name=None):
        if self.warrantys:
            for warranty in self.warrantys:
                return True if warranty.state == 'active' else False
        return True

    @fields.depends('funds')
    def on_change_with_funds_percentage(self, name=None):
        amount = Decimal("0")
        if self.funds:
            for funds in self.funds:
                amount += funds.percentage
        return amount

    @fields.depends('suppliers', 'party')
    def on_change_with_total(self, name=None):
        total = Decimal("0")
        if self.party:
            for row in self.suppliers:
                if row.party == self.party:
                    total += row.total_quotation
        return total

    @fields.depends('consolidation_line')
    def on_change_with_amount_budget(self, name=None):
        amount = Decimal("0")
        for row in self.consolidation_line:
            for budget in row.budgets:
                amount += budget.total
        return amount.quantize(_CENT)

    @fields.depends('amount_budget')
    def on_change_with_amount_budget_temp(self, name=None):
        return self.amount_budget

    @fields.depends('tentative_start_date', 'tdr_number_days_deliveries')
    def on_change_with_tentative_end_date(self, name=None):
        if self.tentative_start_date and self.tdr_number_days_deliveries:
            return (self.tentative_start_date + relativedelta(
                days=self.tdr_number_days_deliveries) - relativedelta(days=1))
        return None

    def get_rec_name(self, name):
        return f"{self.number} - {self.name}"

    @classmethod
    def get_info(cls, contracts, names=None):
        certificate_line_dict = defaultdict(lambda: [])
        for contract in contracts:
            certificate = None
            if contract.is_certificate_multiannual and (
                    contract.tdr_certificate_multiannual) and (
                    contract.tdr_certificate_multiannual.certificates):
                certificate_ids = (
                    c.id
                    for c in contract.tdr_certificate_multiannual.certificates)
                pool = Pool()
                POA = pool.get('public.planning.unit')
                Date = pool.get('ir.date')
                Certificate = pool.get('public.budget.certificate')
                today = Date.today()
                poa = POA.search([
                    ('start_date', '<=', today),
                    ('end_date', '>=', today),
                    ('type', '=', 'poa'),
                ])[0]
                certificates = Certificate.search([
                    ('poa', '=', poa.id),
                    ('id', 'in', certificate_ids),
                    ('type_', '=', 'manual'),
                    ('state', '=', 'done'),
                ], order=[('period', 'DESC')])
                if certificates:
                    certificate = certificates[0]
            elif not contract.is_certificate_multiannual and (
                    contract.tdr_certificate):
                certificate = contract.tdr_certificate
            if certificate and certificate.state == 'done':
                for row in certificate.lines:
                    certificate_line_dict[contract.id].append(row.id)
        return {
            'certificate_lines': certificate_line_dict,
        }

    @classmethod
    def view_attributes(cls):
        return super(ContractProcess, cls).view_attributes() + [
            ('//page[@id="page_payment"]', 'states', {
                'invisible': (Eval('state').in_(
                    ['draft', 'confirm', 'validated']))}),
            ('//page[@id="page_warranty"]', 'states', {
                'invisible': (Eval('state').in_(
                    ['draft', 'confirm', 'validated']))}),
            ('//page[@id="suppliers_invited"]', 'states', {
                'invisible': (Eval('state').in_(['draft']))}),
        ]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, contracts):
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        PluriAnnualCard = pool.get('public.pluri.certificate')
        PluriAnnualFiscal = pool.get('account.fiscalyear.pluri')
        PublicBudgetPluri = pool.get('public.budget.pluri')
        for contract in contracts:
            if contract.is_certificate_multiannual:
                if (contract.tdr_certificate_multiannual
                        and contract.tdr_certificate_multiannual.state not in [
                            'done', 'cancel']):
                    certificate_multiannual = (
                        contract.tdr_certificate_multiannual)
                    to_delete = []
                    to_delete_lines = []
                    to_delete_details = []
                    if len(certificate_multiannual.certificates) > 1:
                        contract.raise_user_warning(
                            'no_delete_certificate_multiannual',
                            'no_delete_certificate_multiannual', {})
                    else:
                        to_delete.append(certificate_multiannual)
                        for line in certificate_multiannual.fiscals:
                            to_delete_lines.append(line)
                            for detail in line.budgets:
                                to_delete_details.append(detail)
                        Certificate.delete(certificate_multiannual.certificates)
                        PluriAnnualCard.delete(to_delete)
                        PluriAnnualFiscal.delete(to_delete_lines)
                        PublicBudgetPluri.delete(to_delete_details)
                    contract.tdr_certificate_multiannual = None
                elif (contract.tdr_certificate_multiannual
                      and contract.tdr_certificate_multiannual.state == 'cancel'):  # noqa
                    contract.tdr_certificate_multiannual = None
                else:
                    cls.raise_user_error('cant_delete_certificate')
            else:
                if (contract.tdr_certificate
                        and contract.tdr_certificate.state not in [
                            'done', 'cancel']):
                    Certificate.delete([contract.tdr_certificate])
                    contract.tdr_certificate = None
                elif (contract.tdr_certificate
                      and contract.tdr_certificate.state == 'cancel'):
                    contract.tdr_certificate = None
                else:
                    cls.raise_user_error('cant_delete_certificate')
        cls.save(contracts)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, contracts):
        pool = Pool()
        Company = pool.get('company.company')
        context = Transaction().context
        company = Company(context['company'])
        currency = company.currency
        Certificate = pool.get('public.budget.certificate')
        CertificateLine = pool.get('public.budget.certificate.line')
        CertificateLineTax = pool.get('public.budget.certificate.line.tax')
        CostCenter = pool.get('public.cost.center')
        Budget = pool.get('public.budget')
        Period = pool.get('account.period')
        cost_center = CostCenter.search([])
        Config = pool.get('purchase.configuration')
        Sequence = pool.get('ir.sequence')
        config = Config(1)
        cls.consolidation_budgets_amount(contracts)
        for contract in contracts:
            if contract.consolidation_line:
                total = reduce(
                    lambda a, b: a + b,
                    [x.total_with_tax for x in contract.consolidation_line])
                if contract.is_certificate_multiannual and (
                        contract.amount_contract != total):
                    cls.raise_user_error('value_multiannual_no_equal', {
                        'amount_contract': contract.amount_contract,
                        'total': total.quantize(_CENT),
                    })
                if not contract.is_certificate_multiannual and (
                        contract.amount_budget != total):
                    cls.raise_user_error('value_annual_no_equal', {
                        'amount_budget': contract.amount_budget,
                        'total': total.quantize(_CENT),
                    })
            if contract.number == '/':
                if config.contract_sequence:
                    contract.number = Sequence.get_id(
                        config.contract_sequence.id)
                else:
                    contract.raise_user_error('no_sequence_defined')
            if contract.tdr_certificate or contract.tdr_certificate_multiannual:
                if context.get('migration_process_contract'):
                    continue
                try:
                    if not contract.is_certificate_multiannual:
                        department = (
                            contract.tdr_certificate.requesting_department.id)
                        total = contract.tdr_certificate.total
                    else:
                        department = (
                            contract.tdr_certificate_multiannual.requesting_department.id)
                        total = contract.tdr_certificate_multiannual.balance
                    if not contract.tdr_requirement_area or (
                            department != contract.tdr_requirement_area.id) or (
                            total != contract.amount_contract):
                        cls.raise_user_error('certificate_no_valid')
                    continue
                except Exception:
                    cls.raise_user_error('certificate_no_valid')
            if contract.consolidation_line:
                is_minor_pending_certificate = False
                for row in contract.consolidation_line:
                    for item in row.budgets:
                        if item.total > item.total_budguet:
                            is_minor_pending_certificate = True
                            break
                if is_minor_pending_certificate:
                    contract.raise_user_warning(
                        'amount_must_be_minor_to_pending_certificate',
                        'amount_must_be_minor_to_pending_certificate', {})
            else:
                cls.raise_user_error('no_have_products_consolidation')
            advance = (' (NO CONTEMPLA ENTREGA DE ANTICIPO)'
                       if contract.tdr_advance == 0 else
                       f"Y UN ANTICIPO DE {contract.tdr_advance}")
            observation = (
                '' if contract.observation is None
                else f", {contract.observation}")
            description = (
                f"PROCESO DE CONTRATACION: {contract.name}, CON DURACIÓN DE "
                f"{contract.tdr_number_days_deliveries} DÍAS. CON UN VALOR "
                f"TOTAL DE {contract.amount_budget} DÓLARES, FECHA DE INICIO: "
                f"{contract.tentative_start_date}, FECHA DE FIN: "
                f"{contract.tentative_end_date} CON UN MONTO TOTAL DE CONTRATO "
                f"{contract.amount_contract} {advance} {observation}")
            data = {
                'line': contract,
                'currency': currency,
                'Period': Period,
                'Certificate': Certificate,
                'description': description
            }
            certificate_header = cls.create_certificate_header(data)
            by_budgets = cls.amount_by_budget(contract.consolidation_line)
            data_certificate = {
                'budgets': by_budgets,
                'Budget': Budget,
                'CertificateLine': CertificateLine,
                'CertificateLineTax': CertificateLineTax,
                'certificate': certificate_header,
                'cost_center': cost_center
            }
            certificate_lines, certificate_line_taxes = (
                cls.generate_lines_certificate(data_certificate)
            )
            certificate_multiannual = defaultdict(lambda: Decimal("0"))
            if contract.is_certificate_multiannual:
                if not contract.certificate_multiannual:
                    contract.raise_user_error(
                        'need_info_certificate_multiannual')
                for row in contract.certificate_multiannual:
                    certificate_multiannual[row.year] += row.amount
                for row in certificate_multiannual:
                    if contract.amount_budget != certificate_multiannual[row]:
                        contract.raise_user_error(
                            'amount_budget_not_equal_amount_multiannual')
                    break
                PluriAnnualCard = pool.get('public.pluri.certificate')
                PluriAnnualFiscal = pool.get('account.fiscalyear.pluri')
                PublicBudgetPluri = pool.get('public.budget.pluri')
                Employee = pool.get('company.employee')
                Company = pool.get('company.company')
                multiannual = PluriAnnualCard(
                    name=contract.name,
                    start_date=contract.tentative_start_date,
                    end_date=contract.tentative_end_date,
                    requesting_employee=Employee(context['employee']),
                    company=Company(context['company']),
                    requesting_department=contract.tdr_requirement_area,
                )
                to_fiscalyear = []
                for row in certificate_multiannual:
                    fiscalyear = PluriAnnualFiscal(
                        year=row,
                    )
                    to_budget = []
                    for line in contract.certificate_multiannual:
                        if line.year == row:
                            budget_multiannual = PublicBudgetPluri(
                                budget=line.budget,
                                balance=line.amount,
                                available_balance=0,
                                certificated_balance=0,
                                pending=0,
                            )
                            budget_multiannual.free_balance = (
                                budget_multiannual.get_free_balance(
                                    'free_balance'))
                            to_budget.append(budget_multiannual)
                    fiscalyear.budgets = to_budget
                    fiscalyear.balance = fiscalyear.on_change_with_balance()
                    fields = ['available_balance', 'certificated_balance',
                        'pending', 'free_balance']
                    for name in fields:
                        setattr(fiscalyear, name,
                            fiscalyear.get_balance_data(name))

                    to_fiscalyear.append(fiscalyear)
                multiannual.fiscals = to_fiscalyear
                multiannual.on_change_fiscals()
                multiannual.save()
                certificate_header.pluriannual_check = True
                certificate_header.pluriannual_certificate = multiannual
                contract.tdr_certificate_multiannual = multiannual
            certificate_header.save()
            CertificateLine.save(certificate_lines)
            CertificateLineTax.save(certificate_line_taxes)
            for row in certificate_lines:
                row.on_change_untaxed_amount()
                row.taxed_amount = row.taxed_amount.quantize(Decimal("0.01"))
                row.amount = row.on_change_with_amount()
            CertificateLine.save(certificate_lines)
            if not contract.is_certificate_multiannual:
                contract.tdr_certificate = certificate_header
        cls.save(contracts)

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validated(cls, contracts):
        pool = Pool()
        ContractRequirement = pool.get('purchase.contract.requirement')
        CPCL = pool.get('purchase.contract.process.consolidation.line')
        ContractQuotation = pool.get('purchase.contract.quotation')
        for contract in contracts:
            if (contract.tdr_certificate
                    and contract.tdr_certificate.state != 'done') or (
                    contract.tdr_certificate_multiannual and
                    contract.tdr_certificate_multiannual.state != 'done' and
                    contract.tdr_certificate_multiannual.certificates):
                cls.raise_user_error('certificate_isnt_done')
            if not contract.suppliers:
                cls.raise_user_error('no_suppliers')
            cpc_lines = CPCL.search([
                ('process', '=', contract)
            ])
            to_save_requirements = []
            to_save_quotation = []
            for supplier in contract.suppliers:
                to_delete = ContractQuotation.search([
                    ('supplier_line', '=', supplier.id)
                ])
                ContractQuotation.delete(to_delete)

                for line in cpc_lines:
                    quotation = ContractQuotation(
                        supplier_line=supplier,
                        consolidation_line=line,
                    )
                    to_save_quotation.append(quotation)
                ContractQuotation.save(to_save_quotation)

                to_delete = ContractRequirement.search([
                    ('supplier_line', '=', supplier.id)
                ])
                ContractRequirement.delete(to_delete)
                for evaluation in contract.tdr_offer_evaluations:
                    requirement = ContractRequirement(
                        requirement=evaluation.required_parameter,
                        parameter_offered='',
                        assigned_percentage=evaluation.assigned_percentage,
                        met_percentage=0,
                        type_="quantitative",
                        supplier_line=supplier,
                        met_check=False
                    )
                    to_save_requirements.append(requirement)
                supplier.requirements = to_save_requirements
                supplier.save()
                ContractRequirement.save(to_save_requirements)

    @classmethod
    @ModelView.button
    @Workflow.transition('awarded')
    def awarded(cls, contracts):
        for contract in contracts:
            if contract.total is None or (contract.consolidation_line and (
                    contract.total == Decimal("0"))):
                contract.raise_user_error('no_generate_warranty_payment')
            if contract.tdr_guarantees:
                contract.create_guarantees()
            if contract.tdr_payments:
                contract.create_payments()
            to_save = []
            pool = Pool()
            ContractProduct = pool.get('purchase.contract.process.product')
            ContractProductTax = pool.get('purchase.contract.process.product.tax')
            for row in contract.consolidation_line:
                new = ContractProduct(
                    process = contract,
                    product = row.product,
                )
                to_line = []
                for tax in row.taxes:
                    to_line.append(ContractProductTax(
                        product = new,
                        tax = tax,
                    ))
                new.taxes = to_line
                to_save.append(new)
            if to_save:
                ContractProduct.save(to_save)


    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, contracts):
        to_save = []
        to_save_lines = []
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        CPCL = pool.get('purchase.contract.process.consolidation.line')
        ContractQuotation = pool.get('purchase.contract.quotation')
        Line = pool.get('purchase.line')
        LineBudget = pool.get('purchase.line.budget')
        LineTax = pool.get('purchase.line-account.tax')
        Date = pool.get('ir.date')
        list_taxes = []
        for contract in contracts:
            purchase = Purchase(
                purchase_date=Date.today(),
                description=contract.name,
                department=contract.tdr_requirement_area,
                poa=contract.poa,
                party=contract.party,
                invoice_address=(contract.party.addresses[0]
                    if contract.party.addresses else None),
                certificate=contract.tdr_certificate,
                payment_deadline=Date.today() + relativedelta(
                    days=contract.tdr_number_days_deliveries),
                contract_process=contract
            )
            to_save.append(purchase)
            cpc_lines = CPCL.search([
                ('process', '=', contract)
            ])
            for row in cpc_lines:
                quotation, = ContractQuotation.search([
                    ('consolidation_line', '=', row.id),
                    ('supplier_line.party', '=', contract.party.id),
                ])
                line = Line(
                    unit_price=quotation.unit_price,
                    quantity=row.consolidation_line.purchase_quantity,
                    purchase=purchase,
                    product=row.consolidation_line.product,
                    currency=contract.company.currency,
                    unit=row.consolidation_line.uom,
                )
                if contract.pac_line:
                    line.pac_line = contract.pac_line
                for tax in row.taxes:
                    list_taxes.append(LineTax(
                        line=line,
                        tax=tax,
                    ))
                list_budgets = []
                total_product = (
                        quotation.unit_price * quotation.purchase_quantity)
                total_amount = reduce(
                    lambda a, b: a + b, [x.amount for x in row.budgets])
                count = len(row.budgets)
                for budget in row.budgets:
                    if count == 1:
                        amount = total_product
                    else:
                        amount = ((budget.amount / total_amount) *
                                  quotation.unit_price *
                                  quotation.purchase_quantity).quantize(_CENT)
                        total_product -= amount
                    list_budgets.append(LineBudget(
                        purchase_line=line,
                        budget=budget.budget,
                        quantity=budget.quantity,
                        amount=amount,
                    ))
                    count -= 1
                line.budgets = list_budgets
                to_save_lines.append(line)
        if to_save and to_save_lines:
            Purchase.save(to_save)
            Line.save(to_save_lines)
            LineTax.save(list_taxes)
        cls.save(contracts)

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, contracts):
        if contracts:
            ids = [x.id for x in contracts]
            Purchase = Pool().get('purchase.purchase')
            purchases = Purchase.search([
                ('contract_process', 'in', ids)
            ])
            for purchase in purchases:
                if purchase.state != 'draft':
                    cls.raise_user_error('purchase_not_state_draft')
            Purchase.cancel(purchases)

    @classmethod
    @ModelView.button_action(
        'public_pac.act_purchase_contract_consolidation_line')
    def add_consolidation(cls, contracts):
        pass

    @classmethod
    @ModelView.button
    def recalculate_budget(cls, contracts):
        ContractProcessConsolidationLineBudget = Pool().get(
            'purchase.contract.process.consolidation.line.budget')
        ContractProcessConsolidationLine = Pool().get(
            'purchase.contract.process.consolidation.line')
        to_save = []
        to_line = []
        for contract in contracts:
            for line in contract.consolidation_line:
                if not contract.is_certificate_multiannual:
                    for tax in line.taxes:
                        line.price_tag = (line.price_tag / (
                                1 + tax.rate)).quantize(Decimal("0.01"))
                    to_save.append(line)
                # total = Decimal("0")
                for row in line.budgets:
                    for tax in line.taxes:
                        row.amount = (row.amount / (1 + tax.rate)).quantize(
                            Decimal("0.01"))
                        row.amount_tax = row.on_change_with_amount_tax()
                        row.total = row.on_change_with_total()
                        # total += row.amount
                        to_line.append(row)
                # if total != line.price_tag:
                #     to_line[len(to_line)-1].amount += (line.price_tag - total)
        if to_line:
            if to_save:
                ContractProcessConsolidationLine.save(to_save)
            ContractProcessConsolidationLineBudget.save(to_line)


    @classmethod
    @ModelView.button
    def add_requirements(cls, contracts):
        pool = Pool()
        ConsolidationLineDetail = pool.get(
            'purchase.consolidation.line.detail')
        RequirementLineDetail = pool.get('purchase.requirement.line.detail')
        TDRTechnicalSpecification = pool.get(
            'purchase.contract.process.tdr.technical.specification')
        detail_ids = []
        for contract in contracts:
            for row in contract.consolidation_line:
                details = ConsolidationLineDetail.search([
                    ('line', '=', row.id),
                ])
                for detail in details:
                    try:
                        detail_ids.append(
                            detail.detail.requirement_material_line.requirement_line_origin.id)  # noqa
                    except Exception:
                        pass
            if detail_ids:
                to_delete = TDRTechnicalSpecification.search([
                    ('process', '=', contract),
                ])
                TDRTechnicalSpecification.delete(to_delete)
                details = RequirementLineDetail.search([
                    ('line', 'in', detail_ids),
                ])
                to_save = []
                for detail in details:
                    new = TDRTechnicalSpecification(
                        process=contract,
                        description=detail.detail,
                    )
                    to_save.append(new)
                TDRTechnicalSpecification.save(to_save)

    @classmethod
    @ModelView.button
    def add_budget_multiannual(cls, contracts):
        pool = Pool()
        Date = pool.get('ir.date')
        Budget = pool.get('public.budget')
        ContractProcessMultiannual = pool.get(
            'purchase.contract.process.multiannual')
        year = str(Date.today().year)
        budget_multiannual = defaultdict(lambda: Decimal("0"))
        to_save = []
        for contract in contracts:
            to_delete = ContractProcessMultiannual.search([
                ('process', '=', contract.id),
            ])
            ContractProcessMultiannual.delete(to_delete)
            for line in contract.consolidation_line:
                for budget in line.budgets:
                    parent = Budget.search([
                        ('kind', '=', 'view'),
                        ('level', '=', 4),
                        ('type', '=', 'expense'),
                        ('parent', 'parent_of', budget.budget.id),
                    ])
                    budget_multiannual[parent[0].id] += budget.total
            for row in budget_multiannual:
                new = ContractProcessMultiannual(
                    process=contract,
                    year=year,
                    budget=row,
                    amount=budget_multiannual[row]
                )
                to_save.append(new)
            ContractProcessMultiannual.save(to_save)

    @classmethod
    @ModelView.button
    def add_budget(cls, contracts):
        pool = Pool()
        TDRBudget = pool.get('purchase.contract.process.tdr.budget')
        Budget = pool.get('public.budget')
        to_save = []
        for contract in contracts:
            to_delete = TDRBudget.search([
                ('process', '=', contract.id),
            ])
            TDRBudget.delete(to_delete)
            for ppu in contract.ppu:
                budgets = Budget.search([
                    ('type', '=', 'expense'),
                    ('activity', 'child_of', ppu.id, 'parent')
                ])
                for budget in budgets:
                    BudgetCard = pool.get('public.budget.card')
                    budget_data, = BudgetCard.search([
                        ('id', '=', budget.id),
                    ])
                    if budget_data.certified_pending == Decimal("0"):
                        continue
                    new = TDRBudget(
                        process=contract,
                        budget=budget,
                    )
                    new.on_change_budget()
                    to_save.append(new)
            TDRBudget.save(to_save)

    @classmethod
    def consolidation_budgets_amount(cls, contracts):
        TDRBudget = Pool().get('purchase.contract.process.tdr.budget')
        to_save = []
        for contract in contracts:
            budgets = defaultdict(lambda : Decimal("0"))
            for line in contract.consolidation_line:
                for row in line.budgets:
                    budgets[row.budget.id] += row.total
            if budgets:
                for key in budgets.keys():
                    new = TDRBudget(
                        process = contract,
                        budget = key,
                        unit_price = budgets[key].quantize(Decimal("0.01")),
                    )
                    new.on_change_budget()
                    to_save.append(new)
                to_delete = TDRBudget.search([
                    ('process', '=', contract)
                ])
                if to_delete:
                    TDRBudget.delete(to_delete)
                if to_save:
                    TDRBudget.save(to_save)



    @classmethod
    def create_certificate_header(cls, data):
        pool = Pool()
        line = data['line']
        description = data['description']
        Period = data['Period']
        currency = data['currency']
        Certificate = data['Certificate']
        Date = pool.get('ir.date')
        today = Date.today()
        period, = Period.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today)
        ])
        new_certificate = Certificate(
            company=line.company,
            department=line.tdr_requirement_area,
            period=period,
            currency=currency,
            poa=line.poa,
            type_='manual',
            date=today,
            requesting_employee=line.tdr_administrator,
            requesting_department=line.tdr_requirement_area,
            description=description
        )
        new_certificate.period_start_date = (
            new_certificate.on_change_with_period_start_date())
        new_certificate.period_end_date = (
            new_certificate.on_change_with_period_end_date())
        return new_certificate

    @classmethod
    def generate_lines_certificate(cls, data):
        by_budgets = data['budgets']
        CertificateLine = data['CertificateLine']
        new_certificate = data['certificate']
        cost_center = data['cost_center']
        CertificateLineTax = data['CertificateLineTax']
        taxes = []
        to_save_certificate_line = []
        for item in by_budgets:
            new_certificate_line = CertificateLine(
                certificate=new_certificate,
                type='line',
                budget=by_budgets[item].get('budget'),
                untaxed_amount=by_budgets[item].get('amount').quantize(_CENT),
                cost_center=cost_center[0],
            )
            for tax in by_budgets[item].get('taxes'):
                taxes.append(CertificateLineTax(
                    line=new_certificate_line,
                    tax=tax,
                ))
            to_save_certificate_line.append(new_certificate_line)
        return to_save_certificate_line, taxes

    @classmethod
    def amount_by_budget(cls, consolidated_products):
        by_budgets = defaultdict(lambda: {})
        i = 0
        for row in consolidated_products:
            taxes = [t.id for t in row.taxes]
            for item in row.budgets:
                if item.total == Decimal("0"):
                    cls.raise_user_error('value_zero_in_budget')
                budget_id = item.budget.id
                exist_in_dict = False
                for j in by_budgets:
                    if budget_id == by_budgets[j].get('budget'):
                        exist_in_dict = True
                        by_budgets[j] = {
                            'budget': budget_id,
                            'taxes': taxes,
                            'amount': (
                                    item.amount.quantize(
                                        Decimal("0.01")) + by_budgets[j].get(
                                        'amount'))
                        }
                        break
                if not exist_in_dict:
                    by_budgets[i] = {
                        'budget': budget_id,
                        'taxes': taxes,
                        'amount': item.amount.quantize(Decimal("0.01")),
                    }
                i += 1
        return by_budgets

    def create_guarantees(self):
        to_save_warranty = []
        to_save_process_warranty = []
        pool = Pool()
        ContractProcessWarranty = pool.get('purchase.contract.process.warranty')
        WorkWarranty = pool.get('work.warranty')
        Date = pool.get('ir.date')
        total = Decimal("0")
        for supplier in self.suppliers:
            if self.party == supplier.party:
                for row in supplier.quotations:
                    total += row.purchase_quantity * row.unit_price
                break
        for guarantee in self.tdr_guarantees:
            if guarantee.type.generate_warranty:
                if guarantee.type.relation_to_advances:
                    amount = self.tdr_advance
                else:
                    amount = total.quantize(Decimal("0.00"))
                warranty = WorkWarranty(
                    amount=amount,
                    description=(
                        f"GARANTIA DE CONTRATACION #{self.number} - "
                        f"{self.name}"),
                    start_date=guarantee.process.tentative_start_date,
                    end_date=guarantee.process.tentative_end_date,
                    folder=Decimal("1"),
                    party=self.party,
                    percentage=guarantee.percentage,
                    renovation_days=guarantee.renovation_days,
                    type_=guarantee.type,
                    office=self.tdr_requirement_area.name,
                )
                to_save_warranty.append(warranty)
                new = ContractProcessWarranty(
                    process=self,
                    warranty=warranty,
                )
                to_save_process_warranty.append(new)
        if to_save_warranty and to_save_process_warranty:
            WorkWarranty.save(to_save_warranty)
            ContractProcessWarranty.save(to_save_process_warranty)

    def create_payments(self):
        to_save = []
        pool = Pool()
        ContractProcessPayment = pool.get('purchase.contract.process.payment')
        Date = pool.get('ir.date')
        if self.tdr_advance > Decimal("0"):
            to_save.append(
                ContractProcessPayment(
                    sequence=1,
                    process=self,
                    amount=self.tdr_advance,
                    planned_date=Date.today(),
                    type='advance',
                )
            )
        total = self.total
        total_amortized = self.tdr_advance
        count = len(self.tdr_payments)
        sequence = 1
        for payment in self.tdr_payments:
            if count == 1:
                amount = total
                amortized = total_amortized
            else:
                amount = (self.total * payment.percentage).quantize(
                    _CENT)
                amortized = (
                        self.tdr_advance / len(self.tdr_payments)).quantize(
                    _CENT)
                total -= amount
                total_amortized -= amortized
            new = ContractProcessPayment(
                sequence=sequence,
                process=self,
                tdr_payment_line=payment,
                amortized_value=(
                    Decimal("0") if self.tdr_advance == Decimal("0")
                    else amortized),
                amount=amount,
                planned_date=Date.today(),
                type='payment',
            )
            to_save.append(new)
            count -= 1
            sequence += 1
        if to_save:
            ContractProcessPayment.save(to_save)


class ContractProcessResponsible(TDRProcess):
    'Contract Process Responsible'
    __name__ = 'purchase.contract.process.responsible'

    responsible = fields.Many2One(
        'company.employee', 'Responsable',
        states={
            'readonly': Eval('_parent_process', {}).get(
                'state', '').in_(['close', 'cancel']),
            'required': True,
        }, depends=['process'])


class ContractProcessFunding(TDRProcess):
    'Public Process Contract Funding'
    __name__ = 'purchase.contract.process.funding'

    _states = {
        'readonly': Eval('_parent_process', {}).get('state', '') != 'draft',
    }
    _depends = ['process']

    party = fields.Many2One(
        'party.party', 'Tercero', required=True, states=_states,
        depends=_depends)
    percentage = fields.Numeric(
        'Porcentaje', domain=[
            ('percentage', '>=', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ], states=_states, depends=_depends)
    amount = fields.Function(
        fields.Numeric('Valor propuesto', digits=(16, 2)),
        'on_change_with_amount'
    )
    type = fields.Selection([
        ('own', 'Interno'),
        ('external', 'Externo'),
    ], 'Tipo', required=True, states=_states, depends=_depends)
    type_translated = type.translated('type')
    kind = fields.Selection([
        ('refund', 'Reembolso'),
        ('non_refund', 'Sin reembolso'),
    ], 'Clase', required=True, states=_states, depends=_depends)
    kind_translated = kind.translated('kind')
    description = fields.Text('Descripcion', states=_states, depends=_depends)

    @classmethod
    def __setup__(cls):
        super(ContractProcessFunding, cls).__setup__()
        cls._order = [
            ('party.name', 'ASC'),
        ]

    @staticmethod
    def default_percentage():
        return Decimal("0")

    @staticmethod
    def default_amount():
        return Decimal("0")

    @fields.depends('process', 'percentage')
    def on_change_with_amount(self, name=None):
        amount_contract = (
            Decimal("0") if self.process.amount_contract is None
            else self.process.amount_contract)
        percentage = (
            Decimal("0") if self.percentage is None else self.percentage)
        return (amount_contract * percentage).quantize(_CENT)


class ContractProcessPayment(TDRProcess):
    'Contract Process Payment'
    __name__ = 'purchase.contract.process.payment'

    _states = {
        'readonly': Eval('_parent_process', {}).get('state', '') != 'awarded',
    }
    _depends = ['process']
    sequence = fields.Numeric(
        'N. de planilla',
        states={
            'readonly': If(
                Eval('type') == 'advance', True, Bool(Eval('is_invoice'))),
            'required': True,
        }, depends=['type', 'is_invoice']
    )
    amount = fields.Numeric(
        'Subtotal sin iva', required=True, digits=(16, 2),
        states={'readonly': True}, depends=['process']
    )
    amortized_value = fields.Numeric(
        'Amortizacion', required=True, digits=(16, 2),
        states={
            'readonly': If(Eval('type') == 'advance',
                True,
                Bool(Eval('is_invoice'))),
        }, depends=['type', 'is_invoice'])
    discount = fields.Numeric(
        'Descuento', required=True, digits=(16, 2),
        domain=[
            ('discount', '<=', Decimal("0")),
        ],
        states={
            'readonly': If(Eval('type') == 'advance',
                           True,
                           Bool(Eval('is_invoice'))),
        }, depends=['type', 'is_invoice']
    )
    price_reset = fields.Numeric(
        'Reajuste de precio', required=True, digits=(16, 2),
        states={
            'readonly': If(Eval('type') == 'advance',
                           True,
                           Bool(Eval('is_invoice'))),
        }, depends=['type', 'is_invoice']
    )
    penalty = fields.Numeric(
        'Multa', required=True, digits=(16, 2),
        states={
            'readonly': If(Eval('type') == 'advance',
                True,
                Bool(Eval('is_invoice'))),
        }, depends=['process', 'is_invoice'])
    total = fields.Function(fields.Numeric(
        'Total', digits=(16, 2)),
        'on_change_with_total')
    planned_date = fields.Date(
        'Fecha planificada', states=_states, depends=_depends)
    effective_date = fields.Date(
        'Fecha efectiva', states={'readonly': True}, depends=_depends)

    origin = fields.Reference('Origen', [
        (None, ''),
        ('account.invoice', 'Factura de proveedor'),
    ], states={'readonly': True})
    invoice = fields.Function(
        fields.Many2One('account.invoice', 'Factura'),
        'get_invoice'
    )
    invoice_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('validated', 'Validada'),
            ('posted', 'Contabilizado'),
            ('paid', 'Pagado'),
            ('cancel', 'Cancelado'),
        ], 'Estado factura'),
        'get_invoice'
    )
    retention = fields.Function(
        fields.Numeric('Retencion', digits=(16, 2)),
        'on_change_with_retention'
    )
    taxes = fields.Function(
        fields.Numeric('Impuestos', digits=(16, 2)),
        'on_change_with_taxes'
    )
    tdr_payment_line = fields.Many2One(
        'purchase.contract.process.tdr.payment', 'Pago tdr',
        states={'readonly': True}
    )
    type = fields.Selection(
        TYPE_PAYMENT, 'Tipo', states={'readonly': True})
    type_translated = type.translated('type')
    lines = fields.One2Many(
        'purchase.contract.process.payment.line', 'payment', 'Rubros',
    )
    is_payment = fields.Function(
        fields.Boolean('Es invisible'),
        'on_change_with_is_payment'
    )
    is_invoice = fields.Function(
        fields.Boolean('Es invisible'),
        'on_change_with_is_invoice'
    )
    state = fields.Selection([
        ('valid', 'Válido'),
        ('invalid', 'Inválido'),
    ], 'Estado', states={
        'readonly': Eval('_parent_process', {}).get('state', '') != 'open',
        'required': True,
    }, depends=['process'])

    @classmethod
    def __setup__(cls):
        super(ContractProcessPayment, cls).__setup__()
        cls._order = [
            ('type', 'ASC'),
            ('sequence', 'ASC'),
        ]
        cls._buttons.update({
            'items': {
                'invisible': (Eval('type') == 'advance') | (
                        Eval('_parent_process', {}).get('state', '') != 'open'),
            },
            'create_process': {
                'invisible': Eval('is_invoice'),
            },
        })
        cls._error_messages.update({
            'no_items': ('No existen rubros'),
            'no_delete_payment': (
                'No se puede eliminar anticipos o pagos que ya esten generados '
                'facturas'),
        })

    @staticmethod
    def default_state():
        return 'valid'

    @staticmethod
    def default_amount():
        return Decimal("0")

    @staticmethod
    def default_sequence():
        return 1

    @staticmethod
    def default_amortized_value():
        return Decimal("0")

    @staticmethod
    def default_total():
        return Decimal("0")

    @staticmethod
    def default_planned_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_penalty():
        return Decimal("0")

    @staticmethod
    def default_discount():
        return Decimal("0")

    @staticmethod
    def default_price_reset():
        return Decimal("0")

    @staticmethod
    def default_type():
        return 'payment'

    @classmethod
    def delete(cls, payments):
        AccountInvoice = Pool().get('account.invoice')
        for payment in payments:
            if payment.type == 'advance' or isinstance(
                    payment.origin, AccountInvoice):
                payment.raise_user_error('no_delete_payment')
        super(ContractProcessPayment, cls).delete(payments)

    @fields.depends('process', 'origin', 'type')
    def on_change_with_is_payment(self, name=None):
        if self.type != 'advance':
            return True
        if self.origin or self.process.state != 'open':
            return True
        return False

    @fields.depends('process', 'origin', 'type')
    def on_change_with_is_invoice(self, name=None):
        if self.process.type == 'goods':
            return True
        if self.type != 'payment':
            return True
        if self.origin or self.process.state != 'open':
            return True
        return False

    @fields.depends(
        'amount', 'amortized_value', 'penalty', 'retention', 'taxes',
        'discount', 'price_reset')
    def on_change_with_total(self, name=None):
        amount = Decimal("0") if self.amount is None else self.amount
        taxes = Decimal("0") if self.taxes is None else self.taxes
        amortized_value = (Decimal("0")
            if self.amortized_value is None else self.amortized_value)
        penalty = Decimal("0") if self.penalty is None else self.penalty
        retention = Decimal("0") if self.retention is None else self.retention
        discount = Decimal("0") if self.discount is None else self.discount
        price_reset = (
            Decimal("0") if self.price_reset is None else self.price_reset)
        result = (
                amount - amortized_value - penalty - retention + taxes +
                price_reset + discount)
        return result.quantize(_CENT)

    @fields.depends('origin')
    def on_change_with_retention(self, name=None):
        AccountInvoice = Pool().get('account.invoice')
        retention = Decimal("0")
        if self.origin and isinstance(self.origin, AccountInvoice):
            for row in self.origin.account_tax_retentions:
                if row.state == 'done' and row.authorization_state == 'AUT':
                    for line in row.lines:
                        retention += line.withheld_value
        return retention.quantize(_CENT)

    @fields.depends('origin')
    def on_change_with_taxes(self, name=None):
        AccountInvoice = Pool().get('account.invoice')
        taxes = Decimal("0")
        if self.origin and isinstance(self.origin, AccountInvoice):
            if self.origin.taxes:
                taxes = reduce(lambda a, b: a + b, [
                    x.amount for x in self.origin.taxes])
        return taxes.quantize(_CENT)

    @classmethod
    def get_invoice(cls, payments, names=None):
        invoice = defaultdict(lambda: None)
        invoice_state = defaultdict(lambda: '')
        AccountInvoice = Pool().get('account.invoice')
        for payment in payments:
            if payment.origin and isinstance(payment.origin, AccountInvoice):
                invoice[payment.id] = payment.origin.id
                invoice_state[payment.id] = payment.origin.state
        return {
            'invoice': invoice,
            'invoice_state': invoice_state
        }

    @classmethod
    @ModelView.button_action(
        'public_pac.act_purchase_contract_payment_line_start')
    def items(cls, payments):
        pass

    @classmethod
    @ModelView.button
    def create_process(cls, payments):
        pool = Pool()
        AccountInvoice = pool.get('account.invoice')
        AccountInvoiceLine = pool.get('account.invoice.line')
        AccountInvoiceLineTax = pool.get('account.invoice.line-account.tax')
        InvoiceAccount = pool.get('account.invoice.line.account')
        Product = pool.get('product.product')
        Date = pool.get('ir.date')
        Company = pool.get('company.company')
        Journal = Pool().get('account.journal')
        company = Company(Transaction().context.get('company'))
        for payment in payments:
            certificate = None
            if payment.process.certificate_lines:
                certificate = payment.process.certificate_lines[0].certificate
            if not payment.lines:
                payment.raise_user_error('no_items')
            number = (Decimal("0")
                      if payment.sequence is None else payment.sequence)
            new = AccountInvoice(
                invoice_type='01',
                type='in',
                party=payment.process.party,
                retention_party=payment.process.party,
                description=f"PLANILLA # {number}, {payment.process.name}",
                certificate=certificate,
                company=company,
                poa=payment.process.poa
            )
            new.invoice_address = new.party.address_get(type='invoice')
            new.party_tax_identifier = new.party.tax_identifier
            journals = Journal.search([
                ('type', '=', 'expense'),
                ], limit=1)
            if journals:
                new.journal, = journals
            new.save()
            lines = defaultdict(lambda: defaultdict(lambda: []))
            amount_lines = defaultdict(lambda: defaultdict(lambda: float(0)))
            dict_product_taxes = defaultdict(lambda : [])
            for line in payment.process.products:
                dict_product_taxes[line.product.id] = line.taxes
            unit_price_lines = defaultdict(
                lambda: defaultdict(lambda: Decimal("0")))
            for line in payment.lines:
                unit_price = (line.unit_price + line.discount +
                              line.price_reset).quantize(Decimal("0.000001"))
                lines[line.product.id][unit_price].append(line)
                if line.product.type == 'service':
                    amount_lines[line.product.id][unit_price] = float(line.amount)  # noqa
                    unit_price_lines[line.product.id][unit_price] += unit_price
                else:
                    amount_lines[line.product.id][unit_price] += float(line.amount)  # noqa
                    unit_price_lines[line.product.id][unit_price] = unit_price
            for product_id in list(dict.fromkeys(lines)):
                for unit_price in list(dict.fromkeys(lines[product_id])):
                    product = Product(product_id)
                    new_line = AccountInvoiceLine(
                        type='line',
                        invoice=new,
                        product=product,
                        unit=product.default_uom,
                        quantity=amount_lines[product.id][unit_price],
                        unit_price=unit_price_lines[product.id][unit_price],
                        company=company,
                    )
                    line_accounts = []
                    for line in lines[product.id][unit_price]:
                        line_account = InvoiceAccount(
                            line=new_line,
                            budget=line.budget,
                            quantity=line.amount,
                            amount=line.total,
                        )
                        line_accounts.append(line_account)
                    line_taxes = []
                    for tax in dict_product_taxes[product.id]:
                        line_taxes.append(AccountInvoiceLineTax(
                            line=new_line,
                            tax=tax.tax,
                        ))
                    new_line.accounts = line_accounts
                    new_line.save()
                    AccountInvoiceLineTax.save(line_taxes)
            new.on_change_taxes()
            new.save()
            payment.origin = new
            payment.effective_date = Date.today()
            notify('Contratos públicos', 'La factura se genero exitosamente.')
        cls.save(payments)

    def get_rec_name(self, name):
        return f"Planilla #{self.sequence} de {self.process.name}"


class ContractProcessConsolidationLine(TDRProcess):
    'Contract Process Consolidation Line'
    __name__ = 'purchase.contract.process.consolidation.line'

    consolidation_line = fields.Many2One(
        'purchase.consolidation.line', 'Linea de la consolidacion',
        required=True)
    product = fields.Many2One(
        'product.product', 'Producto',
        states={
            'readonly': True,
            'required': True,
        })
    purchase_quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        states={
            'readonly': True,
            'required': True,
        },
        domain=[
            ('purchase_quantity', '>', Decimal("0")),
        ])
    price_tag = fields.Numeric(
        'Precio Referencial', digits=(16, 4), states=STATES, depends=DEPENDS,
        domain=[
            ('price_tag', '>', Decimal("0")),
        ])
    total = fields.Function(
        fields.Numeric('Total', digits=(16,4)),
        'on_change_with_total'
    )
    total_with_tax = fields.Function(
        fields.Numeric('Total con impuesto', digits=(16,2)),
        'on_change_with_total_with_tax'
    )
    annaul_reference_amount = fields.Numeric(
        'Monto referencial anual', digits=(16, 2), states=STATES,
        depends=DEPENDS)
    taxes = fields.Many2Many(
        'purchase.contract.process.consolidation.line.tax', 'line', 'tax',
        'I', states={'readonly': True}, help='Impuestos')
    budgets = fields.One2Many(
        'purchase.contract.process.consolidation.line.budget', 'line',
        'P', states={'readonly': True}, help='Presupuesto Anual')

    @classmethod
    def __setup__(cls):
        super(ContractProcessConsolidationLine, cls).__setup__()
        cls._order = [
            ('consolidation_line.product', 'ASC'),
        ]
        cls._buttons.update({
            'button_taxes': {
                'readonly': False,
            },
            'button_budgets': {
                'readonly': False,
            },
        })

    @staticmethod
    def default_purchase_quantity():
        return Decimal("1")

    @staticmethod
    def default_price_tag():
        return Decimal("1")

    @staticmethod
    def default_annaul_reference_amount():
        return Decimal("0")

    @fields.depends('purchase_quantity', 'price_tag')
    def on_change_with_total(self, name=None):
        quantity = (Decimal("0") if self.purchase_quantity is None
                    else self.purchase_quantity)
        price_tag = Decimal("0") if self.price_tag is None else self.price_tag
        return (quantity * price_tag).quantize(Decimal("0.0000"))

    @fields.depends('taxes', 'total')
    def on_change_with_total_with_tax(self, name=None):
        quantity = Decimal("0") if self.total is None else self.total
        for row in self.taxes:
            quantity += row.rate * self.total
        return quantity.quantize(Decimal("0.00"))

    @classmethod
    @ModelView.button_action(
        'public_pac.act_purchase_contract_consolidation_taxes_start1')
    def button_taxes(cls, records):
        pass

    @classmethod
    @ModelView.button_action(
        'public_pac.act_purchase_contract_consolidation_budget_start')
    def button_budgets(cls, records):
        pass

    def get_rec_name(self, name):
        product_name = ''
        if self.product:
            product_name = f"{self.product.name}"
        return f"{product_name}, Cant. {self.purchase_quantity}"


class ContractProcessConsolidationLineTax(ModelSQL, ModelView):
    'Contract Process Consolidation Line Tax'
    __name__ = 'purchase.contract.process.consolidation.line.tax'

    line = fields.Many2One(
        'purchase.contract.process.consolidation.line', 'Linea', required=True,
        ondelete='CASCADE')
    tax = fields.Many2One(
        'account.tax', 'Linea de impuestos', required=True)


class ContractProcessConsolidationLineBudget(ModelSQL, ModelView):
    'Contract Process Consolidation Line Budget'
    __name__ = 'purchase.contract.process.consolidation.line.budget'

    line = fields.Many2One(
        'purchase.contract.process.consolidation.line', 'Linea', required=True,
        ondelete='CASCADE')
    budgets = fields.Function(
        fields.One2Many('public.budget', None, 'Partidas'),
        'on_change_with_budgets'
    )
    budget = fields.Many2One(
        'public.budget', 'Partida', required=True,
        states={
            'readonly': Eval('state')!='draft',
        },
        domain=[
            ('id', 'in', Eval('budgets', [-1])),
        ], depends=['budgets', 'state'])
    total_budguet = fields.Function(
        fields.Numeric('Total disponible', digits=(16, 2)),
        'on_change_with_total_budguet')
    quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        states={
            'readonly': Eval('state')!='draft',
            'required': True,
        },
        domain=[
            ('quantity', '>', 0)
        ], depends=['state'])
    amount = fields.Numeric(
        'Valor', digits=(16, 4),
        states={
            'readonly': Eval('state')!='draft',
            'required': True,
        },
        domain=[
            ('quantity', '>', 0)
        ], depends=['state'])
    amount_tax = fields.Function(
        fields.Numeric('Impuesto',  digits=(16, 4)),
        'on_change_with_amount_tax'
    )
    total = fields.Function(
        fields.Numeric('Total',  digits=(16, 2)),
        'on_change_with_total'
    )
    state = fields.Function(fields.Char(
        'Estado'),
        'on_change_with_state')

    @staticmethod
    def default_quantity():
        return Decimal("1")

    @staticmethod
    def default_amount():
        return Decimal("1")

    @fields.depends('line')
    def on_change_with_state(self, name=None):
        if self.line:
            return self.line.process.state
        return ''

    @fields.depends('line')
    def on_change_with_budgets(self, name=None):
        if self.line and self.line.process and self.line.process.tdr_budgets:
            return [b.budget.id for b in self.line.process.tdr_budgets]
        return []

    @fields.depends('budget')
    def on_change_with_total_budguet(self, name=None):
        total = Decimal("0")
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            total = budget_data.certified_pending
        return total

    @fields.depends('line', 'amount')
    def on_change_with_amount_tax(self, name=None):
        taxes_amount = Decimal('0')
        if self.line:
            for tax in self.line.taxes:
                if self.amount:
                    taxes_amount += tax.rate * self.amount
        return taxes_amount.quantize(Decimal("0.0000"))

    @fields.depends('line', 'amount')
    def on_change_with_total(self, name=None):
        amount_tax = self.on_change_with_amount_tax()
        amount = Decimal("0") if self.amount is None else self.amount
        return (amount + amount_tax).quantize(Decimal("0.00"))


class ContractProcessWarranty(TDRProcess):
    'Contract Process Warranty'
    __name__ = 'purchase.contract.process.warranty'

    _states = {
        'readonly': Eval('_parent_process', {}).get('state', '') != 'draft',
    }
    _depends = ['process']

    start_date = fields.Function(
        fields.Date('Fecha inicio'),
        'on_change_with_start_date')
    end_date = fields.Function(
        fields.Date('Fecha fin'),
        'on_change_with_end_date')
    state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('active', 'Activa'),
            ('finished', 'Finalizado')
        ], 'Estado'), 'on_change_with_state'
    )
    warranty = fields.Many2One(
        'work.warranty', 'Garantias',
        states={
            'readonly': True,
        }, depends=_depends)

    @fields.depends('warranty')
    def on_change_with_start_date(self, name=None):
        if self.warranty:
            return self.warranty.start_date
        return None

    @fields.depends('warranty')
    def on_change_with_end_date(self, name=None):
        if self.warranty:
            return self.warranty.end_date
        return None

    @fields.depends('warranty')
    def on_change_with_state(self, name=None):
        if self.warranty:
            return self.warranty.state
        return None


class EnterPurchaseContractConsolidationLineStart(ModelView):
    'Enter Purchase Contract Consolidation Line Start'
    __name__ = 'enter.purchase.contract.consolidation.line.start'

    consolidation_line = fields.Many2Many(
        'purchase.consolidation.line', None, None,
        'Productos consolidados',
        domain=[
            ('id', 'in', Eval('consolidation_line_temp', [-1])),
        ], depends=['consolidation_line_temp'])
    consolidation_line_temp = fields.One2Many(
        'purchase.consolidation.line', None, 'Productos consolidados',
        states={
            'invisible': True,
        })

    @classmethod
    def default_consolidation_line_temp(cls):
        pool = Pool()
        ContractProcessConsolidationLine = pool.get(
            'purchase.contract.process.consolidation.line')
        cpcl = ContractProcessConsolidationLine.search_read([
            ('process.state', '!=', 'cancel'),
        ], fields_names=['consolidation_line'])
        cpcl_ids = [row['consolidation_line'] for row in cpcl]
        ConsolidationLine = pool.get('purchase.consolidation.line')
        consolidation_lines = ConsolidationLine.search_read([
            ('purchase_consolidation.state', '=', 'done'),
            ('process', '=', 'contract'),
            ('id', 'not in', cpcl_ids),
        ], fields_names=['id'])
        if consolidation_lines:
            return [row['id'] for row in consolidation_lines]
        return [-1]


class EnterPurchaseContractConsolidationLine(Wizard):
    'Enter Purchase Contract Consolidation Line'
    __name__ = 'enter.purchase.contract.consolidation.line'

    start = StateView(
        'enter.purchase.contract.consolidation.line.start',
        'public_pac.purchase_contract_consolidation_line_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'ok', 'tryton-ok', default=True),
        ])
    ok = StateTransition()

    def transition_ok(self):
        pool = Pool()
        ContractProcessConsolidationLine = pool.get(
            'purchase.contract.process.consolidation.line')
        ContractProcessConsolidationLineBudget = pool.get(
            'purchase.contract.process.consolidation.line.budget')
        ContractProcess = pool.get('purchase.contract.process')
        ConsolidationLineDetail = pool.get('purchase.consolidation.line.detail')
        TDRBudget = pool.get('purchase.contract.process.tdr.budget')
        Ppu = pool.get('public.planning.unit')
        ContractProcessPpu = pool.get('purchase.contract.process.ppu')
        contract_process = ContractProcess(Transaction().context['active_id'])
        to_save = []
        to_save_line_budget = []
        pac_line = None
        budgets = defaultdict(lambda : Decimal("0"))
        pac_lines = defaultdict(lambda : None)
        for row in self.start.consolidation_line:
            line = ContractProcessConsolidationLine(
                consolidation_line=row,
                product=row.product,
                process=contract_process,
            )
            if row.purchase_quantity is not None and (
                    row.purchase_quantity > Decimal("0")):
                line.purchase_quantity = row.purchase_quantity
            pac_line_temp = None
            budget_by_product = []
            details = ConsolidationLineDetail.search([
                ('line', '=', row.id),
            ])
            for detail in details:
                try:
                    r_material_line = detail.detail.requirement_material_line
                    origin = r_material_line.requirement_line_origin
                    if origin.price_tag is not None and (
                            origin.price_tag > Decimal("0")):
                        line.price_tag = origin.price_tag
                        line.annaul_reference_amount = (
                                line.price_tag * line.purchase_quantity).quantize(
                            Decimal("0.01"))
                    if origin.pac_line and not pac_line_temp:
                        pac_line_temp = origin.pac_line
                    if origin.requirement_budgets:
                        for b in origin.requirement_budgets:
                            budgets[b.budget.id] += b.amount
                        budget_by_product += list(origin.requirement_budgets)
                except Exception:
                    pass
            to_save.append(line)
            pac_line = pac_line_temp
            pac_lines[pac_line] = pac_line.id if pac_line else None
            if pac_line:
                for budget in self.pac_line.budgets:
                    line_budget = ContractProcessConsolidationLineBudget(
                        line=line,
                        budget=budget.budget,
                        quantity=Decimal("1"),
                        amount=budget.amount,
                    )
                    to_save_line_budget.append(line_budget)
            else:
                for budget in budget_by_product:
                    line_budget = ContractProcessConsolidationLineBudget(
                        line=line,
                        budget=budget.budget,
                        quantity=(Decimal("1") if budget.quantity is None
                                  else budget.quantity),
                        amount=budget.amount,
                    )
                    to_save_line_budget.append(line_budget)
        if to_save:
            if len(pac_lines) > 1 or (contract_process.pac_line and (
                    pac_line != contract_process.pac_line)):
                self.raise_user_error(
                    '''Proceso de contratacion mediante linea de pac, solo se 
                    puede hacer de un producto consolidado''')
            ContractProcessConsolidationLine.save(to_save)
            if pac_line:
                contract_process.pac_line = pac_line
                contract_process.on_change_pac_line()
            else:
                to_budgets = []
                for budget_id in budgets.keys():
                    new = TDRBudget(
                        process=contract_process,
                        budget=budget_id,
                        unit_price=budgets[budget_id].quantize(_CENT),
                    )
                    new.on_change_budget()
                    to_budgets.append(new)
                if to_budgets:
                    ppus = Ppu.search([
                        ('budgets', 'in', budgets.keys()),
                    ])
                    to_ppu = []
                    for ppu in ppus:
                        if ppu.parent.type in ['program', 'project']:
                            new_ppu = ContractProcessPpu(
                                process=contract_process,
                                ppu=ppu.parent,
                            )
                            to_ppu.append(new_ppu)
                    ContractProcessPpu.save(to_ppu)
                    TDRBudget.save(to_budgets)
            ContractProcessConsolidationLineBudget.save(to_save_line_budget)
            contract_process.save()
        return 'end'


class EnterContractRequirement(ModelView):
    'Enter request specification'
    __name__ = 'purchase.contract.requirement.enter.start'

    contract_line = fields.Many2One('purchase.contract.supplier.line',
        'Requerimiento',
        states={
            'readonly': True,
            'invisible': True
        })
    requirements = fields.One2Many(
        'purchase.contract.requirement', None,
        'Especificaciones',
        states={
            'readonly': Eval('state', {}).get('state', '') != 'draft',
        },
    )
    state = fields.Char('Estado', states={'invisible': True})

    @staticmethod
    def default_requirements():
        context = Transaction().context
        pool = Pool()
        Specification = pool.get('purchase.contract.requirement')
        specifications = Specification.search([
            ('supplier_line', '=', context.get('active_id')),
        ])
        return [row.id for row in specifications]

    @staticmethod
    def default_contract_line():
        context = Transaction().context
        pool = Pool()
        SuplierLine = pool.get('purchase.contract.supplier.line')
        supplier_line = SuplierLine(context.get('active_id'))
        return supplier_line.id


class EnterContractRequirementWizard(Wizard):
    'Enter Request Specification Wizard'
    __name__ = 'purchase.contract.requirement.enter'

    start = StateView(
        'purchase.contract.requirement.enter.start',
        'public_pac.purchase_contract_requirement_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        specifications_ids = []
        for row in self.start.requirements:
            to_save.append(row)
            specifications_ids.append(row.id)
        pool = Pool()
        Specification = pool.get('purchase.contract.requirement')
        specifications = Specification.search([
            ('id', 'not in', specifications_ids),
            ('supplier_line', '=', self.start.contract_line.id)
        ])
        if specifications:
            Specification.delete(specifications)
        if to_save:
            Specification.save(to_save)
        return 'end'


class EnterPurchaseContractQuotation(ModelView):
    'EnterPurchaseContractQuotation'
    __name__ = 'purchase.contract.quotation.enter'

    quotations = fields.One2Many(
        'purchase.contract.quotation', None, 'Cotizacion',
        states={'readonly': True})

    @staticmethod
    def default_quotations():
        pool = Pool()
        ContractQuotation = pool.get('purchase.contract.quotation')
        quotations = ContractQuotation.search([
            ('supplier_line', '=', Transaction().context.get('active_id')),
        ])
        return [row.id for row in quotations]


class EnterPurchaseContractQuotationStart(Wizard):
    'Enter Purchase Contract Quotation Start'
    __name__ = 'purchase.contract.quotation.enter.start'

    start = StateView(
        'purchase.contract.quotation.enter',
        'public_pac.purchase_contract_quotation_enter_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        for row in self.start.quotations:
            to_save.append(row)
        pool = Pool()
        ContractQuotation = pool.get('purchase.contract.quotation')
        if to_save:
            ContractQuotation.save(to_save)
        return 'end'


class ContractProcessPaymentLine(ModelSQL, ModelView):
    'Contract Process Payment Line'
    __name__ = 'purchase.contract.process.payment.line'

    payment = fields.Many2One(
        'purchase.contract.process.payment', 'Pagos',
        states={
            'readonly': False,
            'required': True,
        }, ondelete='CASCADE')
    is_invoice = fields.Function(
        fields.Boolean('Es factura'),
        'on_change_with_is_invoice'
    )
    budgets = fields.Function(
        fields.One2Many('public.budget', None, 'Partidas'),
        'on_change_with_budgets'
    )
    products = fields.Function(
        fields.One2Many('product.product', None, 'Productos'),
        'on_change_with_products'
    )
    product = fields.Many2One(
        'product.product', 'Producto',
        domain=[
            ('id', 'in', Eval('products')),
        ],
        states={
            'readonly': Bool(Eval('is_invoice')),
            'required': True,
        }, depends=['products', 'is_invoice'])
    budget = fields.Many2One(
        'public.budget', 'Partida(s)',
        domain=[
            ('id', 'in', Eval('budgets')),
        ], states={
            'readonly': Bool(Eval('is_invoice')),
            'required': True,
        }, depends=['budgets', 'is_invoice'])
    amount = fields.Numeric(
        'Cantidad', digits=product_digits,
        domain=[
            ('amount', '>', Decimal("0")),
        ],
        states={
            'readonly': Bool(Eval('is_invoice')),
            'required': True,
        }, depends=['is_invoice'])
    unit_price = fields.Numeric(
        'Precio unitario', digits=product_digits,
        domain=[
            ('unit_price', '>', Decimal("0")),
        ],
        states={
            'readonly': Bool(Eval('is_invoice')),
            'required': True,
        }, depends=['is_invoice'])
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total'
    )
    amortized_value = fields.Numeric(
        'Valor amortizado', digits=(16, 2),
        states={
            'readonly': True,
        })
    penalty = fields.Numeric(
        'Multa', digits=(16, 2),
        states={
            'readonly': True,
        })
    discount = fields.Numeric(
        'Descuento', digits=(16, 2),
        states={
            'readonly': True,
        })
    price_reset = fields.Numeric(
        'Reajuste de precio', digits=(16, 2),
        states={
            'readonly': True,
        })
    total_with_discount = fields.Function(
        fields.Numeric(
            'Total con descuento', digits=(16, 2)),
        'on_change_with_total_with_discount'
    )

    @staticmethod
    def default_amount():
        return Decimal("0")

    @staticmethod
    def default_unit_price():
        return Decimal("0")

    @staticmethod
    def default_amortized_value():
        return Decimal("0")

    @staticmethod
    def default_penalty():
        return Decimal("0")

    @staticmethod
    def default_discount():
        return Decimal("0")

    @staticmethod
    def default_price_reset():
        return Decimal("0")

    @fields.depends('payment')
    def on_change_with_budgets(self, name=None):
        list = []
        for line in self.payment.process.certificate_lines:
            list.append(line.budget.id)
        return list

    @fields.depends('payment')
    def on_change_with_products(self, name=None):
        list = []
        for row in self.payment.process.products:
            list.append(row.product.id)
        return list

    @fields.depends('payment')
    def on_change_with_is_invoice(self, name=None):
        if self.payment:
            return self.payment.is_invoice
        return True

    @fields.depends('amount', 'unit_price', 'discount', 'price_reset')
    def on_change_with_total(self, name=None):
        amount = Decimal("0") if self.amount is None else self.amount
        unit_price = (
            Decimal("0") if self.unit_price is None else self.unit_price)
        discount = Decimal("0") if self.discount is None else self.discount
        price_reset = (
            Decimal("0") if self.price_reset is None else self.price_reset)
        result = (amount * unit_price) + discount + price_reset
        return result.quantize(_CENT)

    @fields.depends('total', 'amortized_value', 'penalty')
    def on_change_with_total_with_discount(self, name=None):
        total = Decimal("0") if self.total is None else self.total
        amortized_value = (
            Decimal("0") if self.amortized_value is None
            else self.amortized_value)
        penalty = Decimal("0") if self.penalty is None else self.penalty
        return (total - amortized_value - penalty).quantize(_CENT)


class EnterContractProcessPaymentLine(Wizard):
    'Enter Contract Process Payment Line'
    __name__ = 'purchase.contract.process.payment.line.enter'

    start = StateView(
        'purchase.contract.process.payment.line.enter.start',
        'public_pac.purchase_contract_payment_line_enter_start_view_tree', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True),
        ]
    )

    @classmethod
    def __setup__(cls):
        super(EnterContractProcessPaymentLine, cls).__setup__()
        cls._error_messages.update({
            'warning_invoice': (
                'Esta intentando vincular una factura a la planilla, no olvide '
                'ingresar primero la informacion de amortización y multa, '
                'posterior a esto no se permite el ingreso de los valores '
                'mencionados'),
            'no_have_money_in_budget': (
                'No hay suficiente dinero en la partida No. %(budget)s, se está'
                ' pidiendo %(require)s pero hay %(pending)s pendiente en la '
                'certificación.'),
        })

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        ids = []
        payment = self.start.payment
        price_reset_origin = (
            payment.price_reset
            if payment.price_reset >= Decimal("0") else payment.price_reset * -1)  # noqa
        total_amortized = payment.amortized_value
        total_penalty = payment.penalty
        total_price_reset = price_reset_origin
        total_discount = payment.discount
        count = len(self.start.items)
        if self.start.invoice:
            self.raise_user_warning('warning_invoice', 'warning_invoice', {})
        total = total_price_reset + total_discount
        for row in self.start.items:
            total += (row.amount * row.unit_price).quantize(_CENT)
        for row in self.start.items:
            if count == 1:
                amortized = total_amortized
                penalty = total_penalty
                price_reset = total_price_reset
                discount = total_discount
            else:
                price_reset = (
                    Decimal("0")
                    if price_reset_origin == Decimal("0")
                    else (((row.amount * row.unit_price) / total) *
                          price_reset_origin).quantize(_CENT))
                discount = (
                    Decimal("0")
                    if payment.discount == Decimal("0")
                    else (((row.amount * row.unit_price) / total) *
                          payment.discount).quantize(_CENT))
                amortized = (
                    Decimal("0")
                    if payment.amortized_value == Decimal("0")
                    else (((row.amount * row.unit_price) / total) *
                          payment.amortized_value).quantize(_CENT))
                penalty = (
                    Decimal("0")
                    if payment.penalty == Decimal("0")
                    else (((row.amount * row.unit_price) / total) *
                          payment.penalty).quantize(_CENT))
                total_amortized -= amortized
                total_penalty -= penalty
                total_discount -= discount
                total_price_reset -= price_reset
            row.payment = payment
            row.price_reset = (
                price_reset
                if payment.price_reset >= Decimal("0")
                else price_reset * -1)
            row.discount = discount
            row.amortized_value = amortized
            row.penalty = penalty
            count -= 1
            to_save.append(row)
            ids.append(row.id)
        pool = Pool()
        if not self.start.invoice:
            Budget = pool.get('public.budget')
            dict_budget = defaultdict(lambda: Decimal("0"))
            if (self.start.payment and self.start.payment.process):
                for row in self.start.payment.process.certificate_lines:
                    dict_budget[row.budget.id] += row.committed_pending
            dict_product_taxes = defaultdict(lambda : [])
            for line in self.start.payment.process.products:
                dict_product_taxes[line.product.id] = line.taxes
            dict_items = defaultdict(lambda: Decimal("0"))
            for row in to_save:
                dict_items[row.budget.id] += row.total
                for tax in dict_product_taxes[row.product.id]:
                    dict_items[row.budget.id] += (
                            row.total * tax.tax.rate).quantize(_CENT)
            for row in dict_items:
                if dict_items.get(row) > dict_budget.get(row):
                    self.raise_user_error('no_have_money_in_budget', {
                        'budget': Budget(row).code,
                        'require': dict_items.get(row),
                        'pending': dict_budget.get(row),
                    })
        Line = pool.get('purchase.contract.process.payment.line')
        lines = Line.search([
            ('id', 'not in', ids),
            ('payment', '=', payment.id)
        ])
        payment.amount = self.start.on_change_with_total()
        if self.start.invoice:
            payment.origin = f"account.invoice,{self.start.invoice.id}"
        if self.start.products:
            ContractProduct = pool.get('purchase.contract.process.product')
            to_products = []
            for product in self.start.products:
                new = ContractProduct(
                    process=payment.process,
                    product=product,
                )
                to_products.append(new)
            ContractProduct.save(to_products)
        payment.save()
        if lines:
            Line.delete(lines)
        if to_save:
            Line.save(to_save)
        return 'end'


class EnterContractProcessPaymentLineStart(ModelView):
    'Enter Contract Process Payment Line Start'
    __name__ = 'purchase.contract.process.payment.line.enter.start'

    payment = fields.Many2One(
        'purchase.contract.process.payment', 'Pago', required=True,
        states={
            'invisible': True,
        })
    products = fields.Function(
        fields.One2Many('product.product', None, 'Productos', states={
            'invisible': True,
        }),
        'on_change_with_products')
    invoices = fields.Function(
        fields.One2Many('account.invoice', None, 'Facturas'),
        'on_change_with_invoices'
    )
    invoice = fields.Many2One(
        'account.invoice', 'Importar de Factura',
        domain=[
            ('id', 'in', Eval('invoices')),
        ],
        states={
            'invisible': Bool(Eval('state')),
        }, depends=['state', 'invoices'])
    state = fields.Function(
        fields.Boolean('Estado', states={'invisible': True}),
        'on_change_with_state'
    )
    items = fields.One2Many(
        'purchase.contract.process.payment.line', None, 'Rubros',
        states={
            'readonly': Bool(Eval('state')),
        },
        domain=[
            ('payment', '=', Eval('payment')),
        ], depends=['payment', 'state', 'invoice']
    )
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total'
    )
    total_with_discount = fields.Function(
        fields.Numeric('Total con descuento', digits=(16, 2)),
        'on_change_with_total_with_discount'
    )
    make_distribution = fields.Boolean(
        'Hacer distribucion?', states={
            'invisible': Bool(Eval('state')),
        }, depends=['state'])
    items_aux = fields.One2Many(
        'purchase.contract.process.payment.line.aux', None,
        'Distribuicion general',
        states={
            'invisible': ~Bool(Eval('make_distribution')),
        },
        domain=[
            ('payment', '=', Eval('payment')),
        ], depends=['payment', 'make_distribution']
    )
    total_items_aux = fields.Function(
        fields.Numeric('Total distribucion', digits=(16, 2), states={
            'invisible': ~Bool(Eval('make_distribution')),
        }, depends=['make_distribution']),
        'on_change_with_total_items_aux'
    )
    calculation = fields.Boolean(
        'Calcular', states={
            'invisible': ~Bool(Eval('make_distribution')),
        }, depends=['make_distribution'])

    @classmethod
    def __setup__(cls):
        super(EnterContractProcessPaymentLineStart, cls).__setup__()
        cls._error_messages.update({
            'no_money_in_certificate': (
                'No hay suficiente dinero en el certificado para generar la '
                'distribucion'),
        })

    @staticmethod
    def default_calculation():
        return False

    @staticmethod
    def default_payment():
        context = Transaction().context
        pool = Pool()
        Payment = pool.get('purchase.contract.process.payment')
        payment = Payment(context.get('active_id'))
        return payment.id

    @fields.depends('payment')
    def on_change_with_state(self, name=None):
        if self.payment and self.payment.process:
            if self.payment.process.state != 'open' or self.payment.origin:
                return True
        return False

    @fields.depends('items')
    def on_change_with_total(self, name=None):
        total = Decimal("0")
        for item in self.items:
            total += item.total
        return total

    @fields.depends('items')
    def on_change_with_total_with_discount(self, name=None):
        total = Decimal("0")
        for item in self.items:
            total += item.total_with_discount
        return total

    @fields.depends('invoice', 'payment')
    def on_change_with_products(self, name=None):
        if self.payment and self.invoice and self.invoice.lines:
            products_dict = defaultdict(lambda: None)
            for product in self.payment.process.products:
                products_dict[product.id] = product.id
            to_products = []
            for line in self.invoice.lines:
                if not products_dict[line.product.id]:
                    to_products.append(line.product.id)
            return to_products
        return []

    @fields.depends('payment')
    def on_change_with_invoices(self, name=None):
        if self.payment and self.payment.process and self.payment.process.party:
            pool = Pool()
            Invoice = pool.get('account.invoice')
            Payment = pool.get('purchase.contract.process.payment')
            payments = Payment.search_read([
                ('origin', '!=', None),
                ('type', '=', 'payment'),
                ('state', '=', 'valid')
            ], fields_names=['origin'])
            payment_ids = [int(p['origin'].split(',')[1]) for p in payments]
            invoices = Invoice.search_read([
                ('state', 'in', ['validated', 'posted', 'paid']),
                ('id', 'not in', payment_ids),
                ('party', '=', self.payment.process.party),
            ], fields_names=['id'])
            return [i['id'] for i in invoices]
        return []

    @fields.depends('invoice', 'payment', 'products')
    def on_change_invoice(self):
        if self.payment and self.invoice and self.invoice.lines:
            to_list = []
            ContractProcessPaymentLine = Pool().get(
                'purchase.contract.process.payment.line')
            for line in self.invoice.lines:
                for row in line.accounts:
                    new = ContractProcessPaymentLine(
                        payment=self.payment,
                        product=line.product,
                        budget=row.budget,
                        amount=row.quantity,
                        unit_price=(
                            row.amount if line.product.type == 'service'
                            else line.unit_price),
                        discount=Decimal("0"),
                        price_reset=Decimal("0"),
                        penalty=Decimal("0"),
                        amortized_value=Decimal("0"),
                    )
                    new.budgets = new.on_change_with_budgets()
                    new.products = new.on_change_with_products()
                    new.is_invoice = new.on_change_with_is_invoice()
                    new.total = new.on_change_with_total()
                    new.total_with_discount = (
                        new.on_change_with_total_with_discount())
                    to_list.append(new)
            self.items = to_list
            self.total = self.on_change_with_total()
            self.total_with_discount = self.on_change_with_total_with_discount()
        else:
            self.items = []

    @staticmethod
    def default_items():
        context = Transaction().context
        pool = Pool()
        Line = pool.get('purchase.contract.process.payment.line')
        lines = Line.search([
            ('payment', '=', context.get('active_id')),
        ])
        return [row.id for row in lines]

    @fields.depends('items_aux')
    def on_change_with_total_items_aux(self, name=None):
        total = Decimal("0")
        for item in self.items_aux:
            total += Decimal("0") if item.total is None else item.total
        return total.quantize(Decimal(_CENT))

    @fields.depends(
        'calculation', 'items_aux', 'total_items_aux', 'payment', 'total',
        'total_with_discount')
    def on_change_calculation(self):
        if self.calculation:
            if self.items_aux:
                self.distribution()
            self.calculation = False

    def distribution(self):
        total_pending = Decimal("0")
        total_count = 0
        for row in self.payment.process.certificate_lines:
            if row.committed_pending > Decimal("0"):
                total_pending += row.committed_pending.quantize(Decimal(_CENT))
                total_count += 1
        if self.total_items_aux > total_pending:
            self.raise_user_error('no_money_in_certificate')
        to_list = []
        ContractProcessPaymentLine = Pool().get(
            'purchase.contract.process.payment.line')
        for item in self.items_aux:
            total_item = item.total
            count = total_count
            for certificate_line in self.payment.process.certificate_lines:
                if certificate_line.committed_pending > Decimal("0"):
                    if count == 1:
                        value = (total_item / item.amount)
                    else:
                        value = ((
                            (item.total / total_pending) *
                            certificate_line.committed_pending / item.amount)
                        ).quantize(Decimal(_CENT))
                        total_item -= (
                                value * item.amount).quantize(Decimal(_CENT))
                    new = ContractProcessPaymentLine(
                        payment=self.payment,
                        product=item.product,
                        budget=certificate_line.budget,
                        amount=item.amount,
                        unit_price=value,
                        discount=Decimal("0"),
                        price_reset=Decimal("0"),
                        penalty=Decimal("0"),
                        amortized_value=Decimal("0"),
                    )
                    new.budgets = new.on_change_with_budgets()
                    new.products = new.on_change_with_products()
                    new.is_invoice = new.on_change_with_is_invoice()
                    new.total = new.on_change_with_total()
                    new.total_with_discount = (
                        new.on_change_with_total_with_discount())
                    to_list.append(new)
                    count -= 1
        self.items = to_list
        self.total = self.on_change_with_total()
        self.total_with_discount = self.on_change_with_total_with_discount()


class ContractProcessPaymentLineAux(ModelView):
    'Contract Process Payment Line Aux'
    __name__ = 'purchase.contract.process.payment.line.aux'

    payment = fields.Many2One('purchase.contract.process.payment', 'Pagos')
    products = fields.Function(
        fields.One2Many('product.product', None, 'Productos'),
        'on_change_with_products'
    )
    product = fields.Many2One(
        'product.product', 'Producto',
        domain=[
            ('id', 'in', Eval('products')),
        ], depends=['products'])
    amount = fields.Numeric(
        'Cantidad', digits=product_digits,
        domain=[
            ('amount', '>', Decimal("0")),
        ])
    unit_price = fields.Numeric(
        'Precio unitario', digits=product_digits,
        domain=[
            ('unit_price', '>', Decimal("0")),
        ])
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 2)), 'on_change_with_total'
    )

    @fields.depends('payment')
    def on_change_with_products(self, name=None):
        list = []
        for row in self.payment.process.products:
            list.append(row.product.id)
        return list

    @fields.depends('amount', 'unit_price')
    def on_change_with_total(self, name=None):
        amount = Decimal("0") if self.amount is None else self.amount
        unit_price = (
            Decimal("0") if self.unit_price is None else self.unit_price)
        return (amount * unit_price).quantize(Decimal(_CENT))


class ContractProcessBalanceAmortizedContext(ModelView):
    'Contract Process Balance Amortized Context'
    __name__ = 'purchase.contract.process.balance_amortized.context'

    start_date = fields.Date(
        'Fecha de inicio',
        domain=[
            ('start_date', '<=', Eval('end_date')),
        ], depends=['end_date'])
    end_date = fields.Date(
        'Fecha de fin',
        domain=[
            ('end_date', '>=', Eval('start_date')),
        ], depends=['start_date'])


class ContractProcessBalanceAmortized(ModelView, ModelSQL):
    'Contract Process Balance Amortized'
    __name__ = 'purchase.contract.process.balance_amortized'

    name = fields.Char('Nombre')
    number = fields.Char('Número de contrato')
    supplier = fields.Many2One('party.party', 'Proveedor')
    payment_date = fields.Date('Fecha de pago anticipo')
    advance = fields.Function(
        fields.Numeric('Anticipo', digits=(16,2)),
        'get_data'
    )
    amortized = fields.Function(
        fields.Numeric('Amortizacion', digits=(16,2)),
        'get_data'
    )
    amortized_pending = fields.Function(
        fields.Numeric('Pendiente por amortizar', digits=(16,2)),
        'get_data'
    )

    @classmethod
    def table_query(cls):
        pool = Pool()
        ContractProcess = pool.get('purchase.contract.process')
        Payment = pool.get('purchase.contract.process.payment')
        Request_line = pool.get('treasury.account.payment.request.line.detail')
        SPI = pool.get('treasury.account.payment.spi')
        Move = pool.get('account.move')

        field_cast = ContractProcess.register_date.sql_type().base

        payment = Payment.__table__()
        request_line = Request_line.__table__()
        spi = SPI.__table__()
        contract = ContractProcess.__table__()
        move = Move.__table__()

        where = Literal(True)

        context = Transaction().context
        if context.get('start_date') or context.get('end_date'):
            where_filter = Literal(True)
            if context.get('start_date'):
                where_filter &= (move.date >= context.get('start_date'))
            if context.get('end_date'):
                where_filter &= (move.date <= context.get('end_date'))
            filter = payment.join(
                move, condition=(move.origin == payment.origin) & (
                        move.state == 'posted'),
            ).select(
                payment.process,
                where=where_filter & (payment.state == 'valid') & (
                        payment.type == 'payment'),
            )
            where &= (contract.id.in_(filter))

        result = payment.join(
            request_line, condition=request_line.origin == Concat(
                'purchase.contract.process.payment,', payment.id)
        ).join(
            spi, condition=(spi.id == request_line.spi) & (
                spi.state.in_(['paid', 'generated']))
        ).select(
            payment.process,
            spi.payment_date,
            where=(payment.type == 'advance') & (payment.state == 'valid')
        )
        query = contract.join(
            result, type_='LEFT',
            condition=contract.id == result.process,
        ).select(
            contract.id,
            contract.name,
            contract.number,
            contract.party.as_('supplier'),
            (result.payment_date).cast(field_cast).as_('payment_date'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            where=where & (contract.state.in_(['open', 'close'])),
            order_by=[contract.name]
        )
        return query

    @classmethod
    def get_data(cls, records, names=None):
        advances = defaultdict(lambda: Decimal("0"))
        amortized = defaultdict(lambda: Decimal("0"))

        _origin = []
        _contract = defaultdict(lambda: None)
        _amortized = defaultdict(lambda: None)

        ids = [x.id for x in records]
        pool = Pool()
        Move = pool.get('account.move')
        ContractProcess = pool.get('purchase.contract.process')
        ContractProcessAdjustment = pool.get(
            'purchase.contract.process.adjustment')
        contract_adjustments = ContractProcessAdjustment.search([])
        for row in contract_adjustments:
            advances[row.process.id] += row.advance
        contracts = ContractProcess.browse(ids)
        for contract in contracts:
            for payment in contract.payments:
                if payment.type == 'advance' and payment.state == 'valid':
                    advances[contract.id] += payment.amount
                if payment.type == 'payment' and payment.origin and (
                        payment.state == 'valid'):
                    invoice = f"account.invoice,{payment.origin.id}"
                    _origin.append(invoice)
                    _contract[invoice] = payment.process.id
                    _amortized[invoice] = payment.amortized_value
        amortized_pending = advances.copy()
        for row in contract_adjustments:
            amortized[row.process.id] += row.amortized
            amortized_pending[row.process.id] -= row.amortized
        _domain = [
            ('origin', 'in', _origin),
            ('state', '=', 'posted'),
        ]
        context = Transaction().context
        _is_filter = False
        if context.get('end_date'):
            _domain.append(('date', '<=', context.get('end_date')))
            _is_filter = True
        moves = Move.search(_domain)
        for move in moves:
            move_invoice = f"account.invoice,{move.origin.id}"
            if _is_filter and move.date >= context.get('start_date') and (
                    move.date <= context.get('end_date')):
                amortized[_contract[move_invoice]] += _amortized[move_invoice]
            elif not _is_filter:
                amortized[_contract[move_invoice]] += _amortized[move_invoice]
            amortized_pending[
                _contract[move_invoice]] -= _amortized[move_invoice]
        return {
            'advance': advances,
            'amortized': amortized,
            'amortized_pending': amortized_pending,
        }


class EnterConsolidationLineTaxStart(ModelView):
    'Enter Consolidation Line Tax Start'
    __name__ = 'enter.purchase.consolidation.line.tax.start'

    consolidation_line = fields.Many2One(
        'purchase.contract.process.consolidation.line', 'Producto consolidado',
        required=True, states={'invisible': True})
    taxes = fields.Many2Many(
        'purchase.contract.process.consolidation.line.tax', None, 'tax',
        'Impuesto',
        states={
            'readonly': Eval('state')!='draft',
        }, depends=['state'])
    state = fields.Function(fields.Char(
        'Estado'),
        'on_change_with_state')

    @staticmethod
    def default_consolidation_line():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_taxes():
        ContractProcessConsolidationLineTax = Pool().get(
            'purchase.contract.process.consolidation.line.tax')
        line_taxes = ContractProcessConsolidationLineTax.search([
            ('line', '=', Transaction().context.get('active_id')),
        ])
        return [x.tax.id for x in line_taxes]

    @fields.depends('consolidation_line')
    def on_change_with_state(self, name=None):
        if self.consolidation_line:
            return self.consolidation_line.process.state
        return ''


class EnterConsolidationLineTax(Wizard):
    'Enter Consolidation Line Tax'
    __name__ = 'enter.purchase.consolidation.line.tax'

    start = StateView(
        'enter.purchase.consolidation.line.tax.start',
        'public_pac.contract_consolidation_taxes_enter_start_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True),
        ])
    enter = StateTransition()

    def transition_enter(self):
        ConsolidationLineTax = Pool().get(
            'purchase.contract.process.consolidation.line.tax')
        to_save = []
        for row in self.start.taxes:
            new = ConsolidationLineTax(
                line=self.start.consolidation_line,
                tax=row,
            )
            to_save.append(new)
        to_delete = ConsolidationLineTax.search([
            ('line', '=', self.start.consolidation_line.id),
        ])
        if to_delete:
            ConsolidationLineTax.delete(to_delete)
        if to_save:
            ConsolidationLineTax.save(to_save)
        return 'end'


class EnterConsolidationLineBudgetStart(ModelView):
    'Enter Consolidation Line Budget Start'
    __name__ = 'enter.purchase.consolidation.line.budget.start'

    consolidation_line = fields.Many2One(
        'purchase.contract.process.consolidation.line', 'Producto consolidado',
        required=True, states={'invisible': True})
    budgets = fields.One2Many(
        'purchase.contract.process.consolidation.line.budget', None,
        'Impuesto',
        states={
            'readonly': Eval('state')!='draft',
        },
        domain=[
            ('line', '=', Eval('consolidation_line', -1)),
        ], depends=['consolidation_line', 'state'])
    pending_quantity = fields.Function(
        fields.Numeric('Cantidad pendiente', digits=(16,2)),
        'on_change_with_pending_quantity'
    )
    pending_amount = fields.Function(
        fields.Numeric('Valor pendiente', digits=(16,4)),
        'on_change_with_pending_amount'
    )
    total_with_tax = fields.Function(
        fields.Numeric('Total con impuesto'),
        'on_change_with_total_with_tax'
    )
    state = fields.Function(fields.Char(
        'Estado'),
        'on_change_with_state')

    @staticmethod
    def default_consolidation_line():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_budgets():
        ContractProcessConsolidationLineBudget = Pool().get(
            'purchase.contract.process.consolidation.line.budget')
        line_budgets = ContractProcessConsolidationLineBudget.search([
            ('line', '=', Transaction().context.get('active_id')),
        ])
        return [x.id for x in line_budgets]

    @fields.depends('consolidation_line')
    def on_change_with_state(self, name=None):
        if self.consolidation_line:
            return self.consolidation_line.process.state
        return ''

    @fields.depends('consolidation_line', 'budgets')
    def on_change_with_pending_quantity(self, name=None):
        if self.consolidation_line.product and (
                self.consolidation_line.product.type == 'service'):
            quantity = 1
            for row in self.budgets:
                quantity *= row.quantity
            return self.consolidation_line.purchase_quantity - quantity
        quantity = self.consolidation_line.purchase_quantity
        for row in self.budgets:
            quantity -= row.quantity
        return quantity.quantize(Decimal("0.01"))

    @fields.depends('consolidation_line', 'budgets')
    def on_change_with_pending_amount(self, name=None):
        amount = self.consolidation_line.annaul_reference_amount
        total_with_tax = self.on_change_with_total_with_tax()
        return (amount - total_with_tax).quantize(Decimal("0.01"))

    @fields.depends('budgets')
    def on_change_with_total_with_tax(self, name=None):
        total = Decimal("0")
        for row in self.budgets:
            total += row.total
        return total.quantize(Decimal("0.00"))


class EnterConsolidationLineBudget(Wizard):
    'Enter Consolidation Line Budget'
    __name__ = 'enter.purchase.consolidation.line.budget'

    start = StateView(
        'enter.purchase.consolidation.line.budget.start',
        'public_pac.contract_consolidation_budget_enter_start_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True),
        ])
    enter = StateTransition()

    @classmethod
    def __setup__(cls):
        super(EnterConsolidationLineBudget, cls).__setup__()
        cls._error_messages.update({
            'pending_amounts': (
                'Existe cantidad o valor pendiente para distribuir por '
                'partidas'),
            'no_money_budget': (
                'No hay suficiente dinero en la partida %(code)s, quiere '
                'certificar %(total)s pero tiene disponible %(total_budget)s')
        })

    def transition_enter(self):
        if self.start.on_change_with_pending_quantity() != Decimal("0") or (
                self.start.on_change_with_pending_amount() != Decimal("0")):
            self.raise_user_error('pending_amounts')
        ContractProcessConsolidationLineBudget = Pool().get(
            'purchase.contract.process.consolidation.line.budget')
        ids = []
        for budget in self.start.budgets:
            ids.append(budget.id)
        if (not self.start.consolidation_line.process.tdr_certificate_multiannual
                and not self.start.consolidation_line.process.tdr_certificate):
            for budget in self.start.budgets:
                if budget.total > budget.total_budguet:
                    self.raise_user_error('no_money_budget', {
                        'code': budget.budget.code,
                        'total': budget.total,
                        'total_budget': budget.total_budguet,
                    })
        to_delete = ContractProcessConsolidationLineBudget.search([
            ('id', 'not in', ids),
            ('line', '=', self.start.consolidation_line.id),
        ])
        if to_delete:
            ContractProcessConsolidationLineBudget.delete(to_delete)
        if self.start.budgets:
            ContractProcessConsolidationLineBudget.save(list(self.start.budgets))
        return 'end'


class EnterProductTaxStart(ModelView):
    'Enter Product Tax Start'
    __name__ = 'enter.purchase.contract.process.product.tax.start'

    product = fields.Many2One(
        'purchase.contract.process.product', 'Producto',
        required=True, states={'invisible': True})
    taxes = fields.Many2Many(
        'purchase.contract.process.product.tax', None, 'tax', 'Impuestos',
        states={
            'readonly': Eval('state')!='open',
        }, depends=['state'])
    state = fields.Function(fields.Char(
        'Estado'),
        'on_change_with_state')

    @staticmethod
    def default_product():
        return Transaction().context.get('active_id')

    @staticmethod
    def default_taxes():
        ContractProductTaxes = Pool().get(
            'purchase.contract.process.product.tax')
        taxes = ContractProductTaxes.search([
            ('product', '=', Transaction().context.get('active_id')),
        ])
        return [x.tax.id for x in taxes]

    @fields.depends('product')
    def on_change_with_state(self, name=None):
        if self.product:
            return self.product.process.state
        return ''


class EnterProductTax(Wizard):
    'Enter Product Tax'
    __name__ = 'enter.purchase.contract.process.product.tax'

    start = StateView(
        'enter.purchase.contract.process.product.tax.start',
        'public_pac.contract_product_tax_enter_start_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True),
        ])
    enter = StateTransition()

    def transition_enter(self):
        ContractProductTaxes = Pool().get(
            'purchase.contract.process.product.tax')
        to_save = []
        for row in self.start.taxes:
            new = ContractProductTaxes(
                product=self.start.product,
                tax=row,
            )
            to_save.append(new)
        to_delete = ContractProductTaxes.search([
            ('product', '=', self.start.product.id),
        ])
        if to_delete:
            ContractProductTaxes.delete(to_delete)
        if to_save:
            ContractProductTaxes.save(to_save)
        return 'end'


class ContractProcessAdjustment(TDRProcess):
    'Contract Process Adjustment'
    __name__ = 'purchase.contract.process.adjustment'

    advance = fields.Numeric(
        'Anticipo', digits=(16,2), required=True, domain=[
            ('advance', '>=', Decimal('0')),
        ])
    amortized = fields.Numeric('Amortizado', digits=(16,2), required=True)

    @classmethod
    def __setup__(cls):
        super(ContractProcessAdjustment, cls).__setup__()
        cls.process.states.update({
            'readonly': False,
        })
        cls.process.context = {
            'contract_process_adjustment': True,
        }
        cls._order = [
            ('process.number', 'ASC'),
        ]
        t = cls.__table__()
        cls._sql_constraints += [
            ('process_uniq', Unique(t, t.process),
             'El contrato se registra una sola vez en los ajustes'),
        ]

    @staticmethod
    def default_advance():
        return Decimal("0")

    @staticmethod
    def default_amortized():
        return Decimal("0")
