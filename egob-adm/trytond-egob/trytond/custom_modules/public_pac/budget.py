from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta

__all__ = [
    'PublicBudgetCertificate', 'PluriAnnualCard'
]


class PublicBudgetCertificate(metaclass=PoolMeta):
    __name__ = 'public.budget.certificate'

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
               query=False):
        context = Transaction().context
        if context.get('contract_process'):
            new_domain = cls.search_contract_process_not_used(context)
            if new_domain:
                domain += new_domain
        return super(PublicBudgetCertificate, cls).search(
            domain, offset=offset, limit=limit, order=order, count=count,
            query=query)

    @classmethod
    def search_contract_process_not_used(cls, context):
        new_domain = []
        pool = Pool()
        Contract = pool.get('purchase.contract.process')
        PurchaseHeader = pool.get('purchase.header')
        contracts = Contract.search_read([
            ('tdr_certificate', '!=', None),
            ('state', '!=', 'cancel'),
        ], fields_names=['tdr_certificate'])
        to_list = [c['tdr_certificate'] for c in contracts]
        purchase_headers = PurchaseHeader.search_read([
            ('certificate', '!=', None),
            ('state', '!=', 'cancel'),
        ], fields_names=['certificate'])
        to_list += [row['certificate'] for row in purchase_headers]
        if to_list:
            new_domain += [
                ('id', 'not in', to_list),
                ('state', '=', 'done'),
                ('pluriannual_check', '=', False),
                ('pluriannual_certificate', '=', None),
                ('type_', '=', 'manual'),
                ('poa', '=', context['contract_poa']),
            ]
        return new_domain


class PluriAnnualCard(metaclass=PoolMeta):
    __name__ = 'public.pluri.certificate'

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
               query=False):
        context = Transaction().context
        if context.get('contract_process_multiannual'):
            new_domain = cls.search_contract_process_not_used()
            if new_domain:
                domain += new_domain
        return super(PluriAnnualCard, cls).search(
            domain, offset=offset, limit=limit, order=order, count=count,
            query=query)

    @classmethod
    def search_contract_process_not_used(cls):
        new_domain = []
        pool = Pool()
        Contract = pool.get('purchase.contract.process')
        contracts = Contract.search_read([
            ('tdr_certificate_multiannual', '!=', None),
            ('state', '!=', 'cancel'),
        ], fields_names=['tdr_certificate_multiannual'])
        to_list = [c['tdr_certificate_multiannual'] for c in contracts]
        if to_list:
            new_domain += [
                ('id', 'not in', to_list),
                ('state', '=', 'done'),
            ]
        return new_domain
