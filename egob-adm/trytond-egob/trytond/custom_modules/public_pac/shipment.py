from trytond.pool import Pool, PoolMeta


__all__ = [
    'ShipmentInternal'
]


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'


    @classmethod
    def delete(cls, shipments):
        RequirementMaterialLine = Pool().get(
            'purchase.material.requirement.line')
        to_save = []
        for shipment in shipments:
            lines = RequirementMaterialLine.search([
                ('move', 'in', shipment.moves),
                ('requirement.state', '!=', 'done'),
            ])
            for line in lines:
                line.move = None
                to_save.append(line)
        if to_save:
            RequirementMaterialLine.save(to_save)
        super(ShipmentInternal, cls).delete(shipments)