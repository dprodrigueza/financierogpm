from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval


__all__ = ['Configuration', 'ConfigurationSequence']


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'

    purchase_plan_tdr_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Purchase TDR Sequence',
            domain=[
                ('code', '=', 'purchase.plan.tdr'),
            ]))

    purchase_consolidation_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Secuencia Consolidacion de compra',
            domain=[
                ('code', '=', 'purchase.consolidation'),
            ]))

    requirement_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Secuencia solicitud de despacho',
            domain=[
                ('code', '=', 'purchase.requirement'),
            ]))

    contract_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Secuencia de contratacion',
            domain=[
                ('code', '=', 'purchase.contract'),
            ]))

    purchase_header_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Secuencia de solicitud de compra',
            domain=[
                ('code', '=', 'purchase.header'),
            ]))

    requirement_material_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', 'Secuencia despacho de bodega',
            domain=[
                ('code', '=', 'purchase.requirement.material'),
            ]))
    reform_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia reforma al PAC',
        domain=[
            ('code', '=', 'purchase.pac.line.reform'),
        ]))
    to_location = fields.MultiValue(fields.Many2One(
        'stock.location', 'Bodega destino', states={
            'required': True,
        }, domain=[
            ('type', '=', 'lost_found'),
        ]))

    shipment_reason = fields.MultiValue(fields.Many2One(
        'stock.shipment.reason', "Razon de albarán",
        states={
            'required': True,
        }, help='Razon de albarán para requerimiento de material'))
    employee_goods = fields.Many2One('company.employee', 'Aprobador de Bienes',
        required=True)
    department_purchase = fields.Many2One('company.department',
        'Departamento de Aprobador de Ordenes',
        required=True)
    request_budgets_requirement = fields.Boolean('Pedir partidas en requerimiento')
    
    @classmethod
    def default_request_budgets_requirement(cls):
        return True

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in [
            'purchase_plan_tdr_sequence', 'purchase_consolidation_sequence',
            'requirement_sequence', 'requirement_material_sequence',
            'to_location', 'shipment_reason', 'purchase_header_sequence',
            'contract_sequence', 'reform_sequence'
        ]:
            return pool.get('purchase.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_purchase_plan_tdr_sequence(cls, **pattern):
        return cls.multivalue_model('purchase_plan_tdr_sequence'
            ).default_purchase_plan_tdr_sequence()

    @classmethod
    def default_purchase_consolidation_sequence(cls, **pattern):
        return cls.multivalue_model('purchase_consolidation_sequence'
            ).default_purchase_consolidation_sequence()

    @classmethod
    def default_requirement_sequence(cls, **pattern):
        return cls.multivalue_model('requirement_sequence'
            ).default_requirement_sequence()

    @classmethod
    def default_reform_sequence(cls, **pattern):
        return cls.multivalue_model('reform_sequence'
            ).default_reform_sequence()

    @classmethod
    def default_contract_sequence(cls, **pattern):
        return cls.multivalue_model('contract_sequence'
            ).default_contract_sequence()
               
    @classmethod
    def default_purchase_header_sequence(cls, **pattern):
        return cls.multivalue_model('purchase_header_sequence'
            ).default_purchase_header_sequence()

    @classmethod
    def default_requirement_material_sequence(cls, **pattern):
        return cls.multivalue_model('requirement_material_sequence'
            ).default_requirement_material_sequence()

    @classmethod
    def default_to_location(cls, **pattern):
        return cls.multivalue_model('to_location').default_to_location()

    @classmethod
    def default_shipment_reason(cls, **pattern):
        return cls.multivalue_model('shipment_reason'
            ).default_shipment_reason()


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'purchase.configuration.sequence'

    purchase_plan_tdr_sequence = fields.Many2One(
        'ir.sequence', 'Purchase TDR Sequence', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.plan.tdr'),
        ], depends=['company'])
    purchase_consolidation_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Consolidacion de Compra', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.consolidation'),
        ], depends=['company'])
    requirement_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia solicitud de despacho', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.requirement'),
        ], depends=['company'])
    contract_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de contratacion', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.contract'),
        ], depends=['company'])
    purchase_header_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de solicitud de compra', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.header'),
        ], depends=['company'])
    requirement_material_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia despacho de bodega', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.requirement.material'),
        ], depends=['company'])    
    reform_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de reformas al PAC', required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'purchase.pac.line.reform'),
        ], depends=['company'])
    to_location = fields.Many2One('stock.location', 'Bodega destino',
        states={
            'required': False,
        },
        domain=[
            ('type', '=', 'lost_found'),
        ])
    shipment_reason = fields.Many2One(
        'stock.shipment.reason', "Razon de albarán",
        states={
            'required': False,
        }, help="Razon de albarán para requerimiento de material")

    @classmethod
    def default_purchase_plan_tdr_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('public_pac', 'sequence_purchase_plan_tdr')
        except KeyError:
            return None

    @classmethod
    def default_purchase_consolidation_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'public_pac', 'sequence_purchase_consolidation')
        except KeyError:
            return None

    @classmethod
    def default_requirement_material_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'public_pac', 'sequence_requirement_material')
        except KeyError:
            return None

    @classmethod
    def default_reform_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id(
                'public_pac', 'sequence_reform')
        except KeyError:
            return None

    @classmethod
    def default_requirement_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('public_pac', 'sequence_requirement')
        except KeyError:
            return None

    @classmethod
    def default_contract_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('public_pac', 'sequence_contract')
        except KeyError:
            return None
    
    @classmethod
    def default_purchase_header_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('public_pac', 'sequence_purchase_header')
        except KeyError:
            return None

    @classmethod
    def default_to_location(cls):
        return None

    @classmethod
    def default_shipment_reason(cls):
        return None
