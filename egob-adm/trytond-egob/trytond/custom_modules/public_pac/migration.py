from decimal import Decimal
import pandas as pd
from io import BytesIO
import unicodedata

from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.model import ModelView, fields
from trytond.tools import file_open
from trytond.wizard import Wizard, Button, StateTransition, StateView

__all__ = [
    'UploadContractProcessStart', 'UploadContractProcess'
]


def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',cadena)
                 if unicodedata.category(c) != 'Mn'))
    return s.upper()

def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        value = row[i]
        if isinstance(value, str):
            value = value.strip()
        _dict.update({field_name: value})
        i += 1
    return _dict


class UploadContractProcessStart(ModelView):
    'Upload Contract Process Start'
    __name__ = 'upload.contract.process.start'

    source_file = fields.Binary('Archivo fuente', required=True)

    template = fields.Binary(
        'Plantilla de contratos', filename='template_filename',
        file_id='template_path', readonly=True)
    template_filename = fields.Char(
        'PlantillaContratos', states={'invisible': True,})
    template_path = fields.Char(
        'Ubicación de archivo ', readonly=True)

    template_example = fields.Binary(
        'Ejemplo de contratos', filename='template_example_filename',
        file_id='template_example_path', readonly=True)
    template_example_filename = fields.Char(
        'EjemploContratos', states={'invisible': True,})
    template_example_path = fields.Char(
        'Ubicación de archivo ejemplo', readonly=True)

    @staticmethod
    def default_template_path():
        return './data/1_migration_contract_process.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('Plantilla informacion Contratos Publicos', 'xlsx')

    @staticmethod
    def default_template():
        path = 'public_pac/data/1_migration_contract_process.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @staticmethod
    def default_template_example_path():
        return './data/1_migration_contract_process_example.xlsx'

    @staticmethod
    def default_template_example_filename():
        return "%s.%s" % ('Ejemplo plantilla Contratos Publicos', 'xlsx')

    @staticmethod
    def default_template_example():
        path = 'public_pac/data/1_migration_contract_process_example.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class UploadContractProcess(Wizard):
    'Upload Contract Process'
    __name__ = 'upload.contract.process'

    start = StateView(
        'upload.contract.process.start',
        'public_pac.upload_contract_process_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UploadContractProcess, cls).__setup__()
        cls._error_messages.update({
            'file_error': (
                'Existen los siguientes errores en el proceso de migración: \n'
                '%(text)s'),
            'no_have_certificates_in_multiannual': (
                'El certificado plurianual debe tener vinculado un certificado '
                'presupuestario')
        })

    def transition_import_(self):
        pool = Pool()
        Contract = pool.get('purchase.contract.process')
        TDRBudget = pool.get('purchase.contract.process.tdr.budget')
        ContractProcessFunding = pool.get('purchase.contract.process.funding')
        ContractSupplierLine = pool.get('purchase.contract.supplier.line')
        ContractProcessStartReason = pool.get(
            'purchase.contract.process.start.reason')
        TDROfferEvaluation = pool.get(
            'purchase.contract.process.tdr.offer.evaluation')
        Employee = pool.get('company.employee')
        Company = pool.get('company.company')
        Department = pool.get('company.department')
        PartyIdentifier = pool.get('party.identifier')
        TypeContract = pool.get('purchase.pac.type.contract')
        Certificate = pool.get('public.budget.certificate')
        C_Multiannual = pool.get('public.pluri.certificate')
        Ppu = pool.get('public.planning.unit')

        start_reason = ContractProcessStartReason.search([])
        if not start_reason:
            self.raise_user_error('file_error', {
                'text': 'No existe registros en Contratación - Razón de inicio'
            })
        deparments = Department.search_read([
            ('active', '=', True),
        ], fields_names=['id', 'name'])
        party_identifiers = PartyIdentifier.search_read([
            ('party.active', '=', True)
        ], fields_names=['code', 'party'])
        type_contracts = TypeContract.search_read([
            ('used_on_public_purchases', '=', True)
        ], fields_names=['id', 'type_contract'])
        certificates = Certificate.search_read([
            ('state', '=', 'done'),
        ], fields_names=['id', 'number'])
        multiannual = C_Multiannual.search_read([
            ('state', '=', 'done'),
        ], fields_names=['id', 'number'])

        dict_multiannual = {c['number']: c['id'] for c in multiannual}
        dict_certificate = {c['number']: c['id'] for c in certificates}
        dict_identifier = {p['code']: p['party'] for p in party_identifiers}
        dict_deparment = {d['name']: d['id'] for d in deparments }
        dict_type_contract={t['type_contract']: t['id'] for t in type_contracts}

        context = Transaction().context
        type = {
            'obra': 'handiwork',
            'obras': 'handiwork',
            'consultoria': 'consultancy',
            'consultorias': 'consultancy',
            'bien': 'goods',
            'bienes': 'goods',
            'servicio': 'services',
            'servicios': 'services',
            'otro': 'other',
            'otros': 'other',
        }
        field_names = [
            'nombre', 'clase', 'tipo_compra', 'plazo_dias',
            'fecha_inicio_tentativa', 'fecha_inicio_real', 'monto_total',
            'area_requiriente', 'cedula_ruc_administrador',
            'cedula_ruc_contratista', 'es_certificado_plurianual',
            'codigo_certificado', 'numero_proceso',
        ]
        file_data = fields.Binary.cast(self.start.source_file)
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        to_save = []
        self.validation(
            df, field_names, dict_certificate, dict_type_contract,
            dict_identifier, dict_deparment, type, dict_multiannual)
        with Transaction().set_context(migration_process_contract=True):
            for count, row in enumerate(df.itertuples()):
                items = get_dict_from_readed_row(field_names, row)
                contract = Contract(
                    company = context['company'],
                    tentative_start_date = items['fecha_inicio_tentativa'],
                    start_date = items['fecha_inicio_real'],
                    start_reason = start_reason[0],
                    name = items['nombre'],
                    type = type[elimina_tildes(items['clase']).lower()],
                    tdr_number_days_deliveries=items['plazo_dias'],
                    suggested_procedure = dict_type_contract[
                        elimina_tildes(items['tipo_compra'])],
                    internal_number='/'
                )
                if items.get('numero_proceso') and str(items.get('numero_proceso')) != 'nan':
                    contract.internal_number = items['numero_proceso']
                contract.party = dict_identifier[
                    items['cedula_ruc_contratista']]
                administrator, = Employee.search([
                    ('party', '=', dict_identifier[
                        items['cedula_ruc_administrador']])
                ])
                contract.tdr_administrator = administrator
                contract.tdr_requirement_area = dict_deparment[
                    elimina_tildes(items['area_requiriente'])]
                contract.funds = [ContractProcessFunding(
                    process = contract,
                    party = Company(context['company']).party,
                    percentage = Decimal("1"),
                    type = 'own',
                    kind = 'non_refund',
                )]
                contract.tdr_offer_evaluations = [TDROfferEvaluation(
                    process = contract,
                    required_parameter = 'PARAMETRO',
                    assigned_percentage = Decimal("1")
                )]
                contract.suppliers = [ContractSupplierLine(
                    process = contract,
                    party=dict_identifier[items['cedula_ruc_contratista']],
                )]
                certificate = None
                if items['es_certificado_plurianual'].upper() == 'NO':
                    contract.tdr_certificate = Certificate(
                        dict_certificate[items['codigo_certificado']])
                    certificate = contract.tdr_certificate
                else:
                    contract.tdr_certificate_multiannual = C_Multiannual(
                        dict_multiannual[items['codigo_certificado']])
                    contract.is_certificate_multiannual = True
                    if contract.tdr_certificate_multiannual.certificates:
                        certificate = (
                            contract.tdr_certificate_multiannual.certificates[0])
                    else:
                        self.raise_user_error(
                            'no_have_certificates_in_multiannual')
                tdr_budgets = []
                for line in certificate.lines:
                    tdr_budget = TDRBudget(
                        process = contract,
                        budget = line.budget,
                        unit_price = line.committed_pending,
                    )
                    tdr_budget.on_change_budget()
                    tdr_budgets.append(tdr_budget)
                ppus = Ppu.search([
                    ('budgets', 'in', [
                        l.budget.id for l in certificate.lines]),
                ])
                to_ppus = []
                for ppu in ppus:
                    if ppu.parent.type in ['program', 'project']:
                        to_ppus.append(ppu.parent)
                contract.ppu = to_ppus
                contract.tdr_budgets = tdr_budgets
                to_save.append(contract)
            if to_save:
                Contract.save(to_save)
                Contract.confirm(to_save)
                Contract.validated(to_save)
                Contract.awarded(to_save)
                Contract.open(to_save)
        return 'end'

    def validation(
            self, df, field_names, dict_certificate, dict_type_contract,
            dict_identifier, dict_deparment, type, dict_multiannual):
        text_certificate = ''
        text_type_contract = ''
        text_admnistrator = ''
        text_supplier = ''
        text_deparment = ''
        text_type = ''
        try:
            for count, row in enumerate(df.itertuples()):
                items = get_dict_from_readed_row(field_names, row)
                if items['es_certificado_plurianual'].upper() == 'NO' and (
                        items['codigo_certificado'] not in dict_certificate.keys()):
                    text_certificate += items['nombre'] + ', '
                if items['es_certificado_plurianual'].upper() == 'SI' and (
                        items['codigo_certificado'] not in dict_multiannual.keys()):
                    text_certificate += items['nombre'] + ', '
                if (elimina_tildes(items['tipo_compra']) not in
                        dict_type_contract.keys()):
                    text_type_contract += items['tipo_compra'] +', '
                if items['cedula_ruc_administrador'] not in dict_identifier.keys():
                    text_admnistrator += str(
                        items['cedula_ruc_administrador']) + ', '
                if items['cedula_ruc_contratista'] not in dict_identifier.keys():
                    text_supplier += str(items['cedula_ruc_contratista']) + ', '
                if (elimina_tildes(items['area_requiriente']) not in
                        dict_deparment.keys()):
                    text_deparment += items['area_requiriente'] + ', '
                if elimina_tildes(items['clase']).lower() not in type.keys():
                    text_type += items['clase'] + ', '
        except Exception:
            self.raise_user_error('file_error', {
                'text': 'Error en la estructura del archivo de migración',
            })
        text = ''
        if text_certificate != '':
            text += f"Certificados: {text_certificate} \n"
        if text_type_contract != '':
            text += f"Tipo de contratos: {text_type_contract} \n"
        if text_admnistrator != '':
            text += f"Administradores: {text_admnistrator} \n"
        if text_supplier != '':
            text += f"Contratistas: {text_supplier} \n"
        if text_deparment != '':
            text += f"Departamentos: {text_deparment} \n"
        if text_type != '':
            text += f"Clase: {text_type} \n"
        if text != '':
            self.raise_user_error('file_error', {
                'text': text,
            })