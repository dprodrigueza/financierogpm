from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['Product']


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    def get_rec_name(self, name):
        name = super(Product, self).get_rec_name(name)
        context = Transaction().context
        if 'locations' in context.keys():
            name += f" Stock: {self.forecast_quantity}"
        return name
