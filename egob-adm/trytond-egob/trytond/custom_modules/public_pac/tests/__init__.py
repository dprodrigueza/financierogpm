# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.public_pac.tests.test_public_pac import suite
except ImportError:
    from .test_public_pac import suite

__all__ = ['suite']
