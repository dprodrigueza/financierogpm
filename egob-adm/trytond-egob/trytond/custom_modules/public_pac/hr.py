from trytond.model import ModelView, ModelSQL, fields, sequence_ordered
from trytond.pool import PoolMeta

__all__ = ['Employee', 'EmployeeRequirementLocation']


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    locations = fields.One2Many(
        'company.employee.requirement.location', 'employee', 'Ubicaciones',
        help='Ubicaciones para solicitudes de despacho')


class EmployeeRequirementLocation(sequence_ordered(), ModelSQL, ModelView):
    'Locations for Requirements'
    __name__ = 'company.employee.requirement.location'

    employee = fields.Many2One('company.employee', 'Empleado',
        required=True, ondelete='CASCADE')
    location = fields.Many2One('stock.location', 'Ubicación', required=True,
        domain=[
            ('type', '=', 'storage'),
        ])
