from decimal import Decimal
from collections import defaultdict

from sql.aggregate import Sum
from sql.operators import Concat
from sql import Literal, Window
from sql.functions import CurrentTimestamp, RowNumber

from trytond.model import fields
from trytond.pyson import Eval, Id, If, Bool
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.product import price_digits
from trytond.tools import grouped_slice, reduce_ids, cursor_dict
from trytond.model import ModelView, ModelSQL, Workflow, Unique
from trytond.wizard import (Wizard, StateView, StateTransition,
    Button, StateAction)
from datetime import date
from functools import partial, reduce
from itertools import groupby
from trytond.modules.hr_ec.hr_ec import date_field, employee_field, set_employee

# mail
from trytond.config import config
from email.header import Header
from email.utils import formataddr, getaddresses
from trytond.report import get_email
from trytond.sendmail import sendmail_transactional, SMTPDataManager

__all__ = [
    'PurchaseRequisition', 'PurchaseRequisitionLine', 'PurchaseRequest',
    'CreatePurchaseRequestQuotation', 'CreatePurchase', 'Quotation',
    'QuotationLine', 'PurchaseLine', 'Purchase', 'PurchaseHeader',
    'PurchaseConsolidation', 'PurchaseConsolidationLine',
    'PurchaseConsolidationDetail', 'PurchaseConsolidationLineDetail',
    'PurchaseConsolidationMaterialRequirements', 'EnterPurchaseConsolidation',
    'EnterPurchaseConsolidationStart', 'PurchaseRequestRequirements',
    'PurchaseRequestQuotationLineRequirements',
    'EnterPuchaseHeaderWinningQuoteStart', 'EnterPurchaseHeaderWinningQuote',
    'EnterPurchaseConsolidationMaterialRequirements',
    'EnterPurchaseConsolidationMaterialRequirementsStart',
    'PurchaseSupplierLine', 'EnterRequestSpecification',
    'EnterRequestSpecificationWizard', 'EnterQuotationSpecificationWizard',
    'EnterQuotationSpecification', 'PurchaseBudget', 'EnterRequestBudget',
    'EnterRequestBudgetWizard', 'PurchaseRequirementMaterialPending',
    'AccountPaymentContractProcess', 'PurchaseLineBudget',
    'PurchaseRequestTax', 'PurchaseHeaderTax', 'EnterRequestTax',
    'EnterRequestTaxWizard'
]

def domain_quarter():
    quarter = {
        '1': [1, 2, 3, 4],
        '2': [5, 6, 7, 8],
        '3': [9, 10, 11, 12]
    }
    month = date.today().month
    domain = []
    for item in quarter:
        if month in quarter[item]:
            domain += [('quarter'+item, '=', True)]
    return domain

ZERO = Decimal('0.0')
CENT = Decimal('0.01')

def get_material_requirement_available():
    pool = Pool()
    MaterialRequirementLine = pool.get('purchase.material.requirement.line')
    PurchaseConsolidationLineDetail = pool.get(
        'purchase.consolidation.line.detail')
    used_products = PurchaseConsolidationLineDetail.search_read([
        ('line.purchase_consolidation.state', '!=', 'cancel'),
    ], fields_names=['detail.requirement_material_line'])
    used_products_ids = [
        row['detail.requirement_material_line'] for row in used_products]
    domain = [
        ('requirement.state', '=', 'done'),
        ('id', 'not in', used_products_ids),
    ]
    requirement_material = Transaction().context.get('requirement_material')
    if requirement_material:
        domain.append(('requirement.id', '=', requirement_material))
    available_products = MaterialRequirementLine.search(domain)
    return available_products


def purchase_consolidation_add_material(dict_, consolidation):  # noqa
    pool = Pool()
    PurchaseConsolidationMaterialRequirements = pool.get(
        'purchase.consolidation.material.requirements')
    PurchaseDetail = pool.get('purchase.consolidation.detail')
    PurchaseLine = pool.get('purchase.consolidation.line')
    PurchaseLineDetail = pool.get('purchase.consolidation.line.detail')
    MaterialRequirement = pool.get('purchase.material.requirement')
    requirement_materials = PurchaseConsolidationMaterialRequirements.search([
        ('purchase_consolidation', '=', consolidation.id),
    ])
    old_ids = [row.id for row in requirement_materials]
    to_save = []
    to_save_detail = []
    to_save_line = []
    to_save_line_detail = []
    for row in dict_:
        if row not in old_ids:
            new = PurchaseConsolidationMaterialRequirements()
            new.purchase_consolidation = consolidation
            new.material_requirements = MaterialRequirement(row)
            to_save.append(new)
            for line in dict_.get(row):
                if line.purchase_quantity > Decimal("0"):
                    new_detail = PurchaseDetail()
                    new_detail.requirement_material_line = line
                    new_detail.on_change_requirement_material_line()
                    to_save_detail.append(new_detail)
                    new_line = PurchaseLine()
                    new_line.product = line.product
                    new_line.description = new_detail.description
                    new_line.uom = new_detail.uom
                    new_line.purchase_consolidation = consolidation
                    to_save_line.append(new_line)
                    new_line_detail = PurchaseLineDetail()
                    new_line_detail.line = new_line
                    new_line_detail.detail = new_detail
                    to_save_line_detail.append(new_line_detail)
    if to_save and to_save_line and to_save_detail and to_save_line_detail:
        PurchaseConsolidationMaterialRequirements.save(to_save)
        PurchaseDetail.save(to_save_detail)
        PurchaseLine.save(to_save_line)
        PurchaseLineDetail.save(to_save_line_detail)
#core method
def get_shipments_returns(model_name):
    "Computes the returns or shipments"
    def method(self, name):
        Model = Pool().get(model_name)
        shipments = set()
        for line in self.lines:
            for move in line.moves:
                if move.shipment:
                    if move.shipment.related_returns:
                        shipments.add(move.shipment.related_returns[0].id)
        return list(shipments)
    return method

def purchase_consolidation_merger(line_merger):
    if line_merger and len(line_merger) > 1:
        pool = Pool()
        Line = pool.get('purchase.consolidation.line')
        LineDetail = pool.get('purchase.consolidation.line.detail')
        line_details = LineDetail.search([
            ('line', 'in', [row.id for row in line_merger]),
        ])
        for line_detail in line_details:
            line_detail.line = line_merger[0]
        to_save = []
        to_delete = []
        i = 0
        while i < len(line_merger):
            if i == 0:
                line_merger[i].merger = False
                to_save.append(line_merger[i])
            else:
                to_delete.append(line_merger[i])
            i += 1
        if to_save and to_delete:
            LineDetail.save(line_details)
            Line.save(to_save)
            Line.delete(to_delete)


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'

    _STATES = {
        'readonly': Eval('state') != 'draft',
    }
    _DEPENDS = ['state']

    fiscal_year = fields.Many2One('account.fiscalyear', 'Fiscal year',
        states=_STATES, depends=_DEPENDS, required=True)
    poa = fields.Many2One('public.planning.unit', 'POA',
        domain=[
            ('company', '=', Eval('company'))
        ],
        states={
            'readonly': True,
        }, depends=['company'], required=True)
    department = fields.Many2One('company.department', 'Department',
        states=_STATES, depends=_DEPENDS, required=True)

    @classmethod
    def __setup__(cls):
        super(PurchaseRequisition, cls).__setup__()
        cls._error_messages.update({
            'line_requisition_without_pac': ('Error in the purchase order '
                'number "%(number)s", the PAC for the product line '
                '"%(description)s" has not been defined.'),
            'quantity_greater_than_pac_quantity': ('Error in the purchase '
                'order number "%(number)s", the product line "%(description)s" '
                'must not define an amount greater than the amount pending to '
                'be purchased ("%(pending_quantity)s") in its PAC line.'),
            'supply_date_error': ('Supply date must inside PAC')
        })

    @classmethod
    def wait(cls, requisitions):
        # Validate errors
        for requisition in requisitions:
            for line in requisition.lines:
                # TODO: DETERMINAR SI ES NECESARIO
                # if not line.pac_line:
                #    cls.raise_user_error('line_requisition_without_pac', {
                #        'number': requisition.number,
                #        'description': line.description
                #    })
                pending_to_buy = (
                    line.pac_line.quantity - line.pac_line.total_acquired)
                if line.quantity > pending_to_buy:
                    cls.raise_user_error(
                        'quantity_greater_than_pac_quantity', {
                            'number': requisition.number,
                            'description': line.description,
                            'pending_quantity': pending_to_buy
                        })
        # Group related requisition lines
        cls.group_related_requisition_lines(requisitions)
        super(PurchaseRequisition, cls).wait(requisitions)

    @fields.depends('fiscal_year', 'poa')
    def on_change_fiscal_year(self):
        if self.fiscal_year:
            POA = Pool().get('public.planning.unit')
            result = POA.search([('fiscalyear', '=', self.fiscal_year)])
            if result:
                self.poa = result[0]

    @classmethod
    def group_related_requisition_lines(cls, requisitions):
        '''
        This groups the request lines that have the same line of pac and the
        same product
        '''
        PurchaseRequisitionLine = Pool().get('purchase.requisition.line')
        to_save = []
        to_delete = []
        for requisition in requisitions:
            if requisition.lines:
                related_lines = {}
                for line in requisition.lines:
                    id_pac_line = line.pac_line.id
                    id_product = line.product.id if line.product else -1
                    if id_pac_line not in related_lines:
                        related_lines[id_pac_line] = {}
                        related_lines[id_pac_line].update({id_product: line})
                    else:
                        if id_product not in related_lines[id_pac_line]:
                            related_lines[id_pac_line].update({
                                id_product: line
                            })
                        else:
                            related_lines[id_pac_line][id_product].quantity \
                                += line.quantity
                            to_delete.append(line)
                lines = []
                for p in related_lines.values():
                    for l in p.values():
                        lines.append(l)
                requisition.lines = lines
                to_save += lines
        PurchaseRequisitionLine.save(to_save)
        PurchaseRequisitionLine.delete(to_delete)


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'
    _history = True

    _states = {
        'readonly': Eval('purchase_requisition_state') != 'draft'
    }
    _depends = ['purchase_requisition_state', 'requisition']

    pac_line = fields.Many2One('purchase.pac.line', 'PAC Line',
        domain=[
            ('pac_budget.purchase_pac.fiscal_year', '=',
                Eval('_parent_requisition', {}).get('fiscal_year', None)),
            ('pac_budget.purchase_pac.state', '=', 'approved'),
            ('reform_state', 'in', ['added', None])
        ], states=_states, depends=_depends)
    pending_purchase_items = fields.Function(fields.Numeric(
        'Pending purchase items', digits=(16, 2),
        states={
            'invisible': (
                Eval('pac_line') &
                Eval('_parent_requisition', {}).get('state', -1).in_(
                    ['done', 'cancel']) &
                Eval('progress').in_([1]))
        }), 'get_pending_purchase_items')

    purchased_quantity = fields.Function(fields.Numeric('Purchased quantity',
        digits=(16, 2)), 'get_purchased_quantity')

    @classmethod
    def __setup__(cls):
        super(PurchaseRequisitionLine, cls).__setup__()
        cls.unit.states['required'] = True
        cls.unit.states['readonly'] = (Eval('pac_line'))
        cls.unit.depends += ['pac_line']
        cls.unit_price.states['required'] = True
        cls.unit_price.states['readonly'] = (Eval('pac_line'))
        cls.unit_price.depends += ['pac_line']
        cls.description.states['required'] = True
        cls.description.states['readonly'] = (Eval('pac_line'))
        cls.description.depends += ['pac_line']
        cls.quantity.digits = (16, 2)

        cls.supplier.states['invisible'] = True

    @fields.depends('pac_line', 'product', 'quantity', 'unit', 'unit_price',
        'description')
    def on_change_pac_line(self, name=None):
        if self.pac_line:
            self.description = self.pac_line.description
            self.quantity = \
                self.pac_line.quantity - self.pac_line.total_acquired
            self.unit_price = self.pac_line.unit_price_without_iva
            self.amount = self.pac_line.total_amount
            self.product = self.pac_line.product
            self.unit = self.pac_line.unit
        else:
            self.product = None
            self.description = None
            self.quantity = None
            self.unit = None
            self.unit_price = None
            self.amount = None

    # TODO: remove these lines, unkown field require_quotations
    # @fields.depends('pac_line')
    # def on_change_with_require_quotations(self, name=None):
    #    if self.pac_line:
    #        if self.pac_line.electronic_catalog:
    #            return False
    #    return True

    @fields.depends('pac_line')
    def on_change_with_pending_purchase_items(self, name=None):
        if self.pac_line:
            return self.pac_line.quantity - self.pac_line.total_acquired
        return None

    @classmethod
    def get_pending_purchase_items(cls, lines, names=None):
        result = defaultdict(lambda: None)
        for line in lines:
            result[line.id] = None
            if line.pac_line:
                result[line.id] = \
                    line.pac_line.quantity - line.pac_line.total_acquired
        return {
            'pending_purchase_items': result
        }

    def compute_request(self):
        if self.requisition:
            request = super(PurchaseRequisitionLine, self).compute_request()
            if request:
                request.poa = self.requisition.poa
                request.department = self.requisition.department
                request.requisition_number = self.requisition.number
                request.pac_line = self.pac_line
                request.estimated_unit_price = self.unit_price
                request.estimated_amount = self.amount
                request.requested_quantity = self.quantity
                return request
        return None

    @classmethod
    def get_purchased_quantity(cls, requisition_lines, names=None):
        pool = Pool()
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequest = pool.get('purchase.request')
        transaction = Transaction()
        cursor = transaction.connection.cursor()

        table = cls.__table__()
        purchase_requisition = PurchaseRequisition.__table__()
        purchase_request = PurchaseRequest.__table__()

        result = defaultdict(lambda: 0)
        requisition_lines_ids = [rl.id for rl in requisition_lines]
        for sub_ids in grouped_slice(requisition_lines_ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(purchase_requisition,
                condition=table.requisition == purchase_requisition.id
            ).join(purchase_request,
                condition=purchase_request.origin == (
                    Concat(PurchaseRequisitionLine.__name__ + ',', table.id))
            ).select(
                table.id,
                Sum(purchase_request.quantity).as_('quantity'),
                where=red_sql & (purchase_request.state == 'purchased'),
                group_by=[table.id, purchase_requisition.number]
            )
            cursor.execute(*query)
            for id, quantity in cursor.fetchall():
                result[id] = float(quantity)

            return {
                'purchased_quantity': result,
            }


class CreatePurchaseRequestQuotation(metaclass=PoolMeta):
    'Create Purchase Request Quotation'
    __name__ = 'purchase.request.quotation.create'

    @classmethod
    def __setup__(cls):
        super(CreatePurchaseRequestQuotation, cls).__setup__()
        cls._error_messages.update({
            'buy_by_electronic_catalog': ('This purchase is made by electronic '
                'catalog. You must select the option "Create purchase" and '
                'choose or enter the data of the provider that the national '
                'system of public purchases has chosen for you.')
        })

    def transition_start(self):
        pool = Pool()
        Request = pool.get('purchase.request')

        requests = Request.browse(Transaction().context['active_ids'])

        reqs = [r for r in requests if r.state in {'draft', 'quotation'}]
        if reqs:
            for r in reqs:
                pass
                # TODO: check, delete next lines
                # if r.electronic_catalog:
                #     self.raise_user_error('buy_by_electronic_catalog')
        return super(CreatePurchaseRequestQuotation, self).transition_start()


class CreatePurchase(metaclass=PoolMeta):
    __name__ = 'purchase.request.create_purchase'

    open_ = StateAction('purchase.act_purchase_form')

    @classmethod
    def __setup__(cls):
        super(CreatePurchase, cls).__setup__()
        cls._error_messages.update({
            'minimum_line_request_quotation': ('The purchase request for the '
                'requisition number "%(number)s", requires a minimum of 3 '
                'quotes.'),
            'request_without_preferred_quotation_line': ('You have not '
                'selected a preferred quote line in the purchase request for '
                'the requisition number "%(number)s".')
        })

    def transition_start(self):
        pool = Pool()
        Request = pool.get('purchase.request')
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Date = pool.get('ir.date')

        requests = Request.browse(Transaction().context['active_ids'])

        if (getattr(self.ask_party, 'party', None)
                and getattr(self.ask_party, 'company', None)):
            def compare_string(first, second):
                return (first or '') == (second or '')

            def to_write(request):
                return (not request.purchase_line
                    and not request.party
                    and request.product == self.ask_party.product
                    and compare_string(
                        request.description, self.ask_party.description))
            reqs = list(filter(to_write, requests))
            if reqs:
                Request.write(reqs, {
                        'party': self.ask_party.party.id,
                        })
            self.ask_party.product = None
            self.ask_party.description = None
            self.ask_party.party = None
            self.ask_party.company = None

        def to_ask_party(request):
            return not request.purchase_line and not request.party
        reqs = filter(to_ask_party, requests)
        if any(reqs):
            return 'ask_party'

        today = Date.today()

        requests = [r for r in requests if not r.purchase_line]

        keyfunc = partial(self._group_purchase_key, requests)
        requests = sorted(requests, key=keyfunc)

        purchases = []
        lines = []
        for key, grouped_requests in groupby(requests, key=keyfunc):
            grouped_requests = list(grouped_requests)
            try:
                purchase_date = min(r.purchase_date
                    for r in grouped_requests
                    if r.purchase_date)
            except ValueError:
                purchase_date = today
            if purchase_date < today:
                purchase_date = today
            department = grouped_requests[0].department
            poa = grouped_requests[0].poa
            purchase = Purchase(
                purchase_date=purchase_date,
                department=department,
                poa=poa
            )
            for f, v in key:
                setattr(purchase, f, v)
            purchases.append(purchase)
            for line_key, line_requests in groupby(
                    grouped_requests, key=self._group_purchase_line_key):
                line_requests = list(line_requests)
                line = self.compute_purchase_line(
                    line_key, line_requests, purchase)
                line.purchase = purchase
                line.requests = line_requests
                lines.append(line)
        Purchase.save(purchases)
        Line.save(lines)
        Request.update_state(requests)
        return 'end'

    def do_open_(self, action):
        pool = Pool()
        Request = pool.get('purchase.request')
        requests = Request.browse(Transaction().context['active_ids'])
        lines = [r.purchase_line for r in requests]
        data = {'res_id': [l.purchase.id for l in lines]}
        return action, data

    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        line = super(CreatePurchase, cls).compute_purchase_line(key,
            requests, purchase)
        try:
            pac_line = min(req.pac_line for req in requests if req.pac_line)
            if pac_line:
                line.pac_line = pac_line
                line.on_change_with_pac_budget()
        except ValueError:
            pass
        return line


class Quotation(metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation'

    file_quotation = fields.Binary('Quotation Report',
        filename='purchase_quotation_name', file_id='purchase_quotation_path')
    purchase_quotation_name = fields.Char('Purchase Q. Name', readonly=True)
    purchase_quotation_path = fields.Char('Purchase Q. Path',
        states={
            'invisible': True
        })
    request_number = fields.Integer('Numero de solicitud',
        states={
            'required': True,
            'readonly': True
        })
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total'
    )

    @classmethod
    def __setup__(cls):
        super(Quotation, cls).__setup__()
        cls._error_messages.update({
            'no_unit_price_defined_on_line': ('Error passing quotation line '
                'number "%(number)s" to "Received" state. A unit price has not '
                'been defined for the product "%(description)s"'),
            'negative_unit_price_defined_on_line': ('Error passing quotation '
                'line number "%(number)s" to "Received" state. A negative unit '
                'price has been defined for the product "%(description)s"'),
            'requirements_not_evaluated': ('La solicitud "%(request)s" '
                'tiene requistos no evaluados.'

            )
        })
        cls.reference.states = {
            'invisible': True
        }
        cls.warehouse.states = {
            'invisible': True
        }
        cls.revision.states = {
            'invisible': True
        }

    @fields.depends('lines')
    def on_change_with_total(self, name=None):
        total = Decimal('0.0')
        for line in self.lines:
            total += line.total
        return total.quantize(Decimal('0.01'))

    @classmethod
    def receive(cls, quotations):
        for quotation in quotations:
            is_evaluated = False
            if quotation.lines:
                for line in quotation.lines:
                    party_requirements = line.purchase_request_requirements
                    if party_requirements:
                        for requirement in party_requirements:
                            if not requirement.meets_requirements:
                                is_evaluated = True
            if is_evaluated:
                cls.raise_user_warning(
                        'requirements_not_evaluated',
                        'requirements_not_evaluated', {
                            'request': line.description
                        })
        super(Quotation, cls).receive(quotations)

    @classmethod
    @ModelView.button
    @Workflow.transition('sent')
    def send(cls, quotations):
        # TODO: if is needed, add manual quotations.
        '''
        pool = Pool()
        PurchaseQuotationRequirements = (
            pool.get('purchase.request.quotation.line.requirements')
        )
        for quotation in quotations:
            for line in quotation.lines:
                requirements = line.request.purchase_request_requirements
                to_save = []
                for requirement in requirements:
                    purchase_requirement = PurchaseQuotationRequirements(
                        purchase_request_requirements = requirement,
                        purchase_request_quotation_line = line,
                        meets_requirements = False,
                    )
                    to_save.append(purchase_requirement)
                PurchaseQuotationRequirements.save(to_save)
        '''
        super(Quotation, cls).send(quotations)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, quotations):
        pool = Pool()
        PurchaseQuotationRequirements = \
            pool.get('purchase.request.quotation.line.requirements')
        for quotation in quotations:

            for line in quotation.lines:
                requirements = line.purchase_request_requirements
                to_delete = []
                for requirement in requirements:
                    to_delete.append(requirement)
                PurchaseQuotationRequirements.delete(to_delete)
        super(Quotation, cls).draft(quotations)

    def get_rec_name(self, name):
        return f"{self.number} - {self.supplier.rec_name}"


class QuotationLine(metaclass=PoolMeta):
    __name__ = 'purchase.request.quotation.line'

    header = fields.Many2One(
        'purchase.header', 'Compra',
        states={
            'readonly': Eval('quotation_state') != 'draft',
            'required': True,
        }, depends=['quotation_state'])
    supply_date = fields.Date(
        'Supply Date', readonly=True)
    purchase_request_requirements = fields.One2Many(
        'purchase.request.quotation.line.requirements',
        'purchase_request_quotation_line',
        'Requisitos', readonly=True)
    total = fields.Function(
        fields.Numeric('Subtotal', digits=(16, 4)),
        'on_change_with_total'
    )

    @classmethod
    def __setup__(cls):
        super(QuotationLine, cls).__setup__()
        cls.unit_price.states['required'] = True
        cls.request.domain = [
            ('purchase_header', '=', Eval('header')),
        ]
        cls.unit.domain = []
        cls.request.depends += ['header']
        cls._order = [
            ('quotation_state', 'ASC'),
            #('meet_requirements', 'DESC'),
            ('unit_price', 'ASC'),
        ]
        cls._buttons.update({
            'open_specifications': {
                'depends': ['purchase_request_requirements']
            },
        })

    @staticmethod
    def default_unit_price():
        return Decimal('0.00')

    @classmethod
    @ModelView.button_action(
        'public_pac.wizard_purchase_request_quotation_specification')
    def open_specifications(cls, requests):
        pass

    @fields.depends('quantity', 'unit_price')
    def on_change_with_total(self, name=None):
        total = Decimal('0.0')
        if self.quantity and self.unit_price:
            total += Decimal(self.quantity) * self.unit_price
        return total.quantize(Decimal('0.0001'))


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    request_date = date_field('Fecha Validacion')
    done_date = date_field('Fecha Aprobación')

    request_by = employee_field('Solicitado por')
    done_by = employee_field('Aprobado por')

    done_by_deparment = fields.Many2One('company.department',
        'Departamento', states={
            'readonly': True,
        })

    purchase_request = fields.Function(
        fields.Char(' N. Solicitud de Compra'),
        'on_change_with_purchase_request'
    )
    contract_process = fields.Many2One(
        'purchase.contract.process', 'Contrato',
        states={
            'readonly': True,
        })

    requirements = fields.Function(
        fields.Char('Solicitud de despacho'),
        'get_requirements', searcher='searcher_requirements'
    )

    payment_deadline = fields.Date('Plazo', states={
        'required': True,
        'readonly': Eval('state') != 'draft'
    })
    products = fields.Function(
        fields.Char('Productos'),
        'on_change_with_products',
        searcher='search_by_products'
    )

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._error_messages.update({
            'error_pac_budget': ('In the purchase line for the product '
                '"%(product)s", the selected budget "%(selected_budget)s" does '
                'not match with the budget of the PAC line "%(pac_budget)s". '
                'Check that the certification of the purchase contains the '
                'requested budget and that budget has pending funds to be '
                'certified.'),
            'error_budget': ('You have not selected a budget.'),
            'there_are_moves': ('No se puede cancelar la orden de compra\n'
                'Existen movimientos en estado realizado'),
            'there_are_moves_in_shipment': ('No se puede cancelar la orden de compra\n'
                'Uno o mas movimientos estan vinculados a un alabaran'),
            'there_are_invoices': ('No se puede cancelar la orden de compra\n'
                'Existen facturas vinculadas')
        })
        cls.poa.states.update({'readonly': Eval('state') != 'draft'})
        cls.certificate.domain=[
            ('poa', '=', Eval('poa')),
        ]
        cls.purchase_date.states={
            'readonly': True
        }
        cls._buttons.update({
            'cancel': {
                'invisible': (
                        Eval('state').in_(['draft', 'quotation', 'cancel']) |
                        ~Id('public_pac', 'group_purchase_cancel_done').in_(
                            Eval('context', {}).get('groups', []))
                        ),
                'depends': ['state']
            },
            'draft': {
                'invisible': ~Eval('state').in_(
                    ['quotation', 'confirmed']),
                'icon': If(Eval('state') == 'cancel', 'tryton-undo',
                    'tryton-back'),
                'depends': ['state'],
            },
        })
        cls._transitions |= set((
            ('done', 'cancel'),
            ('processing', 'cancel')
        ))

    @staticmethod
    def default_invoice_method():
        return 'manual'

    @classmethod
    def get_amount(cls, purchases, names):
        result = {}
        with Transaction().set_context(ignore_rounding=True):
            res =  super(Purchase, cls).get_amount(purchases, names)
            for item in res.get('untaxed_amount', []):
                res['untaxed_amount'][item] = (
                    res['untaxed_amount'][item].quantize(Decimal('0.01'))
                )
            for item in res.get('tax_amount', []):
                res['tax_amount'][item] = (
                    res['tax_amount'][item].quantize(Decimal('0.01'))
                )
            for item in res.get('total_amount', []):
                res['total_amount'][item] = (
                    res['total_amount'][item].quantize(Decimal('0.01'))
                )
            result = res
        return result

    @classmethod
    def get_integration_budgets(cls, ids):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        PurchaseBudget = pool.get('purchase.line.budget')
        #Query to ...
        PPU = pool.get('public.planning.unit')
        Budget = pool.get('public.budget')
        Product = pool.get('product.product')
        ProductTemplate = pool.get('product.template')

        program = PPU.__table__()
        activity = PPU.__table__()
        budget = Budget.__table__()
        purchaseBudget = PurchaseBudget.__table__()
        purchaseLine = PurchaseLine.__table__()
        product = Product.__table__()
        productTemplate = ProductTemplate.__table__()
        purchase = Purchase.__table__()

        lines = PurchaseLine.search([
                ('purchase', 'in', ids)
            ])
        budgets = []
        for line in lines:
            budgets += line.budgets
        budgets_ids = [x.budget.id for x in budgets]
        query = program.join(
            activity,
                 condition=program.id == activity.parent
            ).join(
                budget,
                condition=budget.activity == activity.id
            ).join(
                purchaseBudget,
                condition=purchaseBudget.budget == budget.id
            ).join(
                purchaseLine,
                condition=purchaseLine.id == purchaseBudget.purchase_line
            ).join(
                product,
                condition=product.id == purchaseLine.product
            ).join(
                productTemplate,
                condition=productTemplate.id == product.template
            ).join(
                purchase,
                condition=purchase.id == purchaseLine.purchase
            ).select(
                purchase.number,
                productTemplate.name,
                program.id,
                #program.name,
                activity.code,
                purchaseBudget.amount,
            where=(
                budget.id.in_(budgets_ids)
            )
        )
        cursor.execute(*query)
        return cursor.fetchall()

    @classmethod
    def get_program_by_budgets(cls, data):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Purchase = pool.get('purchase.purchase')
        PurchaseLine = pool.get('purchase.line')
        PurchaseBudget = pool.get('purchase.line.budget')
        #Query to ...
        PPU = pool.get('public.planning.unit')
        Budget = pool.get('public.budget')

        program = PPU.__table__()
        activity = PPU.__table__()
        budget = Budget.__table__()
        purchaseBudget = PurchaseBudget.__table__()
        purchaseLine = PurchaseLine.__table__()
        purchase = Purchase.__table__()
        
        budgets_ids = []
        where = Literal(True)
        if 'purchase' in data: 
            lines = PurchaseLine.search([
                ('purchase', 'in', data['purchase']['ids'])
            ])
            budgets = []
            for line in lines:
                budgets += line.budgets
            budgets_ids = [x.budget.id for x in budgets]
            where &= purchase.id.in_(data['purchase'].get('ids', []))
        else:
            budgets_ids = data['program'].get('budgets', [])
            where &= program.id.in_(data['program'].get('ids', []))
        if budgets_ids:
            where &= ( budget.id.in_(budgets_ids) )  

        query = program.join(
            activity,
                 condition=program.id == activity.parent
            ).join(
                budget,
                condition=budget.activity == activity.id
            ).join(
                purchaseBudget,
                condition=purchaseBudget.budget == budget.id
            ).join(
                purchaseLine,
                condition=purchaseLine.id == purchaseBudget.purchase_line
            ).join(
                purchase,
                condition=purchase.id == purchaseLine.purchase
            ).select(
                purchase.id.as_('purchase'),
                purchaseLine.id.as_('line'),
                purchaseBudget.id.as_('line_budget'), 
                purchaseBudget.amount.as_('amount'),
                budget.id.as_('budget'),
                program.id.as_('program'),
            where=where
        )
        print(str(query) % query.params)
        cursor.execute(*query)
        return cursor_dict(cursor)

    @classmethod
    def get_requirements(cls, purchases, names=None):
        p_dict = {}
        for row in purchases:
            consolidation_lines = []
            numbers = {}
            if row.purchase_request:
                for line in row.lines:
                    for request in line.requests:
                        consolidation_lines.append(request.consolidation_origin)
            if row.contract_process:
                for line in row.contract_process.consolidation_line:
                    consolidation_lines.append(line.consolidation_line)
            if consolidation_lines:
                for line in consolidation_lines:
                    numbers[line.request] = [line.request]
            p_dict[row.id] = ','.join(list(numbers.keys()))
        return {
            'requirements': p_dict,
        }

    @classmethod
    def searcher_requirements(cls, name, clause):
        text = clause[2].split('%')[1] or clause[2]
        ids = []
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        purchases = Purchase.search_read(
            [], fields_names=['id', 'requirements'])
        if purchases:
            ids = [x['id'] for x in purchases if text in x['requirements']]
        return [('id', 'in', ids)]

    @fields.depends('lines')
    def on_change_with_products(self, name=None):
        products = []
        for line in self.lines:
            if line.product:
                products.append(line.product.name)
        return ','.join(dict.fromkeys(products))

    @classmethod
    def search_by_products(cls, name, clause):
        model = ('lines.product')
        return [(model,) + tuple(clause[1:])]

    @classmethod
    @set_employee('done_by')
    def confirm(cls, purchases):
        pool = Pool()
        Date = pool.get('ir.date')
        Config = pool.get('purchase.configuration')
        config = Config(1)
        for purchase in purchases:
            purchase.done_by_deparment = config.department_purchase
            purchase.done_date = Date.today()
        super(Purchase, cls).confirm(purchases)
        cls.process(purchases)

    #OVERRIDE METHOD
    @classmethod
    def cancel(cls, purchases):
        super(Purchase, cls).cancel(purchases)
        for purchase in purchases:
            for move in purchase.moves:
                if move.shipment:
                   cls.raise_user_error('there_are_moves_in_shipment')
                else:
                    if move.state not in ['draft', 'cancel']:
                        cls.raise_user_error('there_are_moves')
                    else:
                        move.state = 'cancel'
                        move.save()
            if purchase.invoices:
                cls.raise_user_error('there_are_invoices')

    def on_change_with_purchase_request(self, name=None):
        numbers = []
        for line in self.lines:
            if line.requests:
                for request in line.requests:
                    numbers.append(request.purchase_header.number)
        return ','.join(list(dict.fromkeys(numbers)))

    @classmethod
    @set_employee('request_by')
    def quote(cls, purchases):
        pool = Pool()
        Date = pool.get('ir.date')
        Uom = pool.get('product.uom')
        for purchase in purchases:
            purchase.request_date = Date.today()
            for line in purchase.lines:
                qty = Uom.compute_qty(line.unit, 1, line.product.default_uom)
        super(Purchase, cls).quote(purchases)

    #OVERRIDE METHOD
    get_shipment_returns = get_shipments_returns('stock.shipment.in.return')

    def create_move(self, move_type):
        pool = Pool()
        Move = pool.get('stock.move')
        MoveAccount = pool.get('stock.move.account')
        MoveTax = pool.get('stock.move.tax')

        moves = []
        to_save_accounts = []
        to_save_taxes = []
        for line in self.lines:
            move = line.get_move(move_type)
            if move:
                move.employee = None
                move.department = None
                accounts = self.create_account_budget(line.budgets, move)
                to_save_accounts += accounts
                to_save_taxes += self.create_moves_tax(line, move)
                moves.append(move)
        Move.save(moves)
        MoveAccount.save(to_save_accounts)
        MoveTax.save(to_save_taxes)
        return moves

    def create_account_budget(self, budgets, move):
        MoveAccount = Pool().get('stock.move.account')
        accounts = []
        for budget in budgets:
            nSMA = MoveAccount(
                move=move,
                product=move.product,
                amount=budget.amount,
                quantity=budget.quantity
            )
            nSMA.on_change_with_stock()
            accounts.append(nSMA)
        return accounts
        #notes: nSMA -> New Stock Move Account

    def create_moves_tax(self, line, move):
        MoveTax = Pool().get('stock.move.tax')
        moveTaxes = []
        for tax in line.taxes:
            newMT = MoveTax(
                move=move,
                tax=tax
            )
            moveTaxes.append(newMT)
        return moveTaxes
        #notes: newMT -> New Move Tax

    def set_shipment_state(self):
        '''
        Inherit method; the status of the requisitions is changed to "done".
        '''
        super(Purchase, self).set_shipment_state()
        if self.shipment_state == 'received':
            self.set_requisition_state_to_done()

    def set_requisition_state_to_done(self):
        PurchaseRequisition = Pool().get('purchase.requisition')
        requisitions = self.get_requisitions_to_modify()
        if requisitions:
            go_done = []
            for requisition in requisitions:
                if self.is_possible_go_done_requisition(requisition):
                    go_done.append(requisition)
            PurchaseRequisition.do(go_done)

    def get_requisitions_to_modify(self):
        pool = Pool()
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequisition = pool.get('purchase.requisition')
        requisitions = []
        for pline in self.lines:
            requests = PurchaseRequest.search([
                ('purchase_line', '=', pline)
            ])
            if requests:
                for request in requests:
                    purchase_requisitions = PurchaseRequisition.search([
                        ('number', '=', request.requisition_number)
                    ])
                    if purchase_requisitions:
                        for requisition in purchase_requisitions:
                            if requisition not in requisitions:
                                requisitions.append(requisition)
        return requisitions

    def is_possible_go_done_requisition(self, requisition):
        PurchaseRequest = Pool().get('purchase.request')
        if requisition.state != 'processing':
            return False
        for rline in requisition.lines:
            requests = PurchaseRequest.search([
                ('origin', '=', 'purchase.requisition.line,' + str(rline.id))
            ])
            if not requests:
                return False
            for request in requests:
                try:
                    purchase = request.purchase_line.purchase
                    if purchase != self:
                        if purchase.shipment_state != 'received':
                            return False
                # TODO: remove exception, replace with raise
                except Exception:
                    return False
        return True

    def get_rec_name(self, name):
        number = f"" if self.number is None else f"{self.number} -"
        return f"{number} {self.party.name}"

class PurchaseLineBudget(Workflow, ModelSQL, ModelView):
    'Purchase Line Budget'
    __name__ = 'purchase.line.budget'

    purchase_line = fields.Many2One('purchase.line', 'Orden de compra')
    budget = fields.Many2One('public.budget', 'Partida', states={
        'readonly': Eval('state')!='draft',
    }, depends=['state'])
    #TODO: update when changed in purchase line
    quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ],
        states={'readonly': True, 'required': True,},
        depends=['purchase_line'])
    amount = fields.Numeric('Valor', digits=(16, 4),states={
        'readonly': Eval('state')!='draft',
    }, depends=['state'])
    state = fields.Function(fields.Char(
        'Estado'),
        'on_change_with_state'
    )

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('purchase_line')
    def on_change_with_state(self, name=None):
        if self.purchase_line and self.purchase_line.purchase and (
                self.purchase_line.purchase.state):
            return self.purchase_line.purchase.state
        return ''


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    pac_line = fields.Many2One('purchase.pac.line', 'PAC Line',
        domain=[
            ('pac_budget.purchase_pac.poa', '=',
                Eval('_parent_purchase', {}).get('poa', -1)),
            ('pac_budget.purchase_pac.state', '=', 'approved'),
            ('reform_state', 'in', ['added', None])
        ],
        states={
            'readonly': Eval('purchase_state').in_(
                ['confirmed', 'processing', 'done', 'cancel']),
        }, depends=['purchase_state', 'purchase'])

    pac_budget = fields.Many2One('public.budget', 'PAC Budget',
        states={
            'invisible': True
        })
    budgets = fields.One2Many(
        'purchase.line.budget', 'purchase_line', 'Partidas',
        states={
            'readonly': Eval('_parent_purchase', {}).get(
                'state', 'draft') != 'draft',
            'required': Eval('_parent_purchase', {}).get(
                'state', 'draft') != 'draft',
        }, depends=['purchase'])

    @classmethod
    def __setup__(cls):
        super(PurchaseLine, cls).__setup__()
        cls.budget.domain = []
        cls.budget.states = {
            'required': False
        }
        cls.unit.domain = []
        cls.amount.digits = (16, 4)
        cls.unit_price.digits = (16, 4)

    #OVERRIDE METHOD
    @fields.depends('product', 'quantity', 'unit',
        methods=['_get_context_purchase_price'])
    def on_change_quantity(self):
        Product = Pool().get('product.product')
        if not self.product:
            return
        with Transaction().set_context(self._get_context_purchase_price()):
            self.unit_price = self.unit_price.quantize(
                Decimal(1) / 10 ** self.__class__.unit_price.digits[1])

    @fields.depends('product', 'pac_line', 'quantity', 'unit_price')
    def on_change_with_unit_price(self, name=None):
        '''
        The unit purchase price should not be modified if it is already given
        by the supplier from the purchase request.
        '''
        return self.unit_price

    #OVERRIDE METHOD
    def get_amount(self, name):
        amount = Decimal('0.0000')
        with Transaction().set_context(ignore_rounding=True):
            res = super(PurchaseLine, self).get_amount(name)
            amount = res.quantize(Decimal('0.0001'))
        return amount

    #OVERRIDE METHOD
    def on_change_with_amount(self):
        amount = Decimal('0.0000')
        with Transaction().set_context(ignore_rounding=True):
            res = super(PurchaseLine, self).on_change_with_amount()
            amount = res.quantize(Decimal('0.0001'))
        return amount

class EnterPurchaseConsolidationMaterialRequirementsStart(ModelView):
    'Enter Purchase Consolidation Material Requirements'
    __name__ = 'enter.purchase.consolidation.material.requirements.start'

    requirement_material = fields.Many2Many(
        'purchase.material.requirement', None, None, 'Requisito de material',
        domain=[
            ('id', 'in', Eval('requirement_materials', -1)),
        ], depends=['requirement_materials'])
    requirement_materials = fields.One2Many(
        'purchase.material.requirement', None, 'Requisitos material lineas')
    # requirement_material_line = fields.Many2Many(
    #     'purchase.material.requirement.line', None, None, 'Requerimientos',
    #     domain=[
    #         ('id', 'in', Eval('requirement_material_lines', [-1])),
    #     ], depends=['requirement_material_lines'])
    # requirement_material_lines = fields.Function(
    #     fields.One2Many(
    #         'purchase.material.requirement.line', None, 'Requerimientos',
    #         states={'invisible': True}),
    #     'on_change_with_requirement_material_lines'
    # )

    @classmethod
    def default_requirement_materials(cls):
        available_products = get_material_requirement_available()
        to_list = []
        for row in available_products:
            if row.purchase_quantity > Decimal("0"):
                to_list.append(row.requirement.id)
        return to_list

    # @fields.depends('requirement_material')
    # def on_change_requirement_material(self):
    #     self.requirement_material_line = None
    #
    # @fields.depends('requirement_material')
    # def on_change_with_requirement_material_lines(self, name=None):
    #     to_list = []
    #     if self.requirement_material:
    #         with Transaction().set_context(
    #                 requirement_material=self.requirement_material.id):
    #             available_products = get_material_requirement_available()
    #             for row in available_products:
    #                 if row.purchase_quantity > Decimal("0"):
    #                     to_list.append(row.id)
    #     return to_list


class EnterPurchaseConsolidationMaterialRequirements(Wizard):
    'Enter Purchase Consolidation Material Requirements'
    __name__ = 'enter.purchase.consolidation.material.requirements'

    start = StateView(
        'enter.purchase.consolidation.material.requirements.start',
        'public_pac.enter_purchase_consolidation_material_requirements_start_view_form',  # noqa
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'ok', 'tryton-ok', default=True),
        ])
    ok = StateTransition()

    def transition_ok(self):
        dict_ = defaultdict(lambda: [])
        for row in self.start.requirement_material:
            for line in row.lines:
                dict_[row.id].append(line)
        consolidation = PurchaseConsolidation(
            Transaction().context['active_id'])
        purchase_consolidation_add_material(dict_, consolidation)
        return 'end'


class PurchaseConsolidationMaterialRequirements(ModelSQL, ModelView):
    'Purchase Consolidation Material Requirements'
    __name__ = 'purchase.consolidation.material.requirements'

    purchase_consolidation = fields.Many2One(
        'purchase.consolidation', 'Consolidacion de compra', required=True)
    material_requirements = fields.Many2One(
        'purchase.material.requirement', 'Requerimiento de material',
        required=True)

    @classmethod
    def delete(cls, items):
        if len(items) > 0:
            pool = Pool()
            material_ids = [row.material_requirements.id for row in items]
            ConsolidationLineDetail = pool.get(
                'purchase.consolidation.line.detail')
            ConsolidationDetail = pool.get('purchase.consolidation.detail')
            ConsolidationLine = pool.get('purchase.consolidation.line')

            consolidation_line_details = ConsolidationLineDetail.search([
                ('line.purchase_consolidation',
                    '=', items[0].purchase_consolidation.id),
                ('detail.requirement_material_line.requirement',
                    'in', material_ids),
            ])
            consolidation_details = [
                row.detail for row in consolidation_line_details]

            if consolidation_line_details:
                ConsolidationLineDetail.delete(consolidation_line_details)
                ConsolidationDetail.delete(consolidation_details)
            exist_line = ConsolidationLineDetail.search_read(
                [], fields_names=['line'])
            exist_line_ids = [row['line'] for row in exist_line]
            to_delete_lines = ConsolidationLine.search([
                ('id', 'not in', exist_line_ids),
            ])
            if to_delete_lines:
                ConsolidationLine.delete(to_delete_lines)
            super(PurchaseConsolidationMaterialRequirements, cls).delete(items)


class PurchaseConsolidation(Workflow, ModelSQL, ModelView):
    'Purchase Consolidation'
    __name__ = 'purchase.consolidation'

    _states = {
        'readonly': Eval('state') != 'draft',
    }
    _depends = ['state']

    number = fields.Char('Número', states={'readonly': True})
    company = fields.Many2One('company.company', 'Empresa',
        states={'readonly': True}, required=True)
    employee = fields.Many2One(
        'company.employee', 'Responsable', states=_states, depends=_depends,
        required=True)
    register_date = fields.Date(
        'Fecha de registro',
        states={
            'readonly': True,
        }, required=True)
    observation = fields.Text(
        'Observación', depends=_depends,
        states={
            'readonly': Eval('state') == 'terminate',
        })
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('request', 'Validado'),
        ('consolidated', 'Consolidado'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado'),
        ('terminate', 'Terminar proceso'),
    ], 'Estado',
        states={
            'readonly': True,
        }, required=True)
    state_to_translated = state.translated('state')
    requirement_material = fields.Many2Many(
        'purchase.consolidation.material.requirements', 'purchase_consolidation',  # noqa
        'material_requirements', 'Despacho requerimiento',
        states=_states, depends=_depends)
    lines = fields.One2Many(
        'purchase.consolidation.line', 'purchase_consolidation', 'Consolidaciones',  # noqa
        states={
            'readonly': True,
        }, depends=_depends)
    employees_request = fields.Function(fields.Char('Requirientes'),
        'on_change_with_employees_request')
    product_names = fields.Function(fields.Char('Productos'),
        'on_change_with_product_names')
    requirements_numbers = fields.Function(
        fields.Char('Numeros de solicitud'),
        'on_change_with_requirements_numbers',
        searcher='search_by_requirements_numbers'
    )
    products = fields.Function(
        fields.Char('Productos'),
        'on_change_with_products',
        searcher='search_by_products'
    )
    busy_consolidation = fields.Function(
        fields.Boolean('Consolidacion ocupada'),
        'on_change_with_busy_consolidation'
    )

    @classmethod
    def __setup__(cls):
        super(PurchaseConsolidation, cls).__setup__()
        cls._order = [
            ('company', 'ASC'),
            ('register_date', 'DESC'),
            ('employee.party.name', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'draft'),
            ('request', 'consolidated'),
            ('consolidated', 'request'),
            ('consolidated', 'done'),
            ('consolidated', 'cancel'),
            ('done', 'cancel'),
            ('cancel', 'terminate'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['request']),
                'depends': ['state']
            },
            'request': {
                'invisible': ~Eval('state').in_(['draft', 'consolidated']),
                'depends': ['state']
            },
            'consolidated': {
                'invisible': ~Eval('state').in_(['request']),
                'depends': ['state']
            },
            'cancel': {
                'invisible': Eval('busy_consolidation'),
                'depends': ['busy_consolidation']
            },
            'terminate': {
                'invisible': ~Eval('state').in_(['cancel']),
                'depends': ['state']
            },
            'done': {
                'invisible': ~Eval('state').in_(['consolidated']),
                'depends': ['state']
            },
            'merger': {
                'invisible': ~Eval('state').in_(['request']),
                'depends': ['state']
            },
            'add_material_requirement': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            }
        })
        cls._error_messages.update({
            'no_sequence_defined': (
                'No existe secuencia definida para consolidaciones de compra. '
                'Por favor, diríjase a "Configuración de Compras Públicas" y '
                'configure una secuencia.'),
            'not_have_line_product': (
                'El/la %(product)s no tiene asigando el producto'),
            'no_product_merge': 'No existe producto al momento de fusionar',
            'no_more_product_error': 'La fusion debe ser del mismo producto',
            'delete_no_draft': 'Debe eliminar solo registros en estado borrador.',
        })

    @classmethod
    def delete(cls, consolidations):
        for consolidation in consolidations:
            if consolidation.state not in ['draft']:
                cls.raise_user_error('delete_no_draft')
        super(PurchaseConsolidation, cls).delete(consolidations)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_employee():
        return Transaction().context.get('employee')

    @staticmethod
    def default_register_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.company = self.employee.company
        else:
            self.company = None

    @fields.depends('lines')
    def on_change_with_products(self, name=None):
        products = []
        for line in self.lines:
            products.append(line.description)
        return ','.join(dict.fromkeys(products))

    @fields.depends('state', 'lines')
    def on_change_with_busy_consolidation(self, name=None):
        if self.state == 'consolidated':
            return False
        if self.state == 'done':
            # averiguar si esta en contrato o solicitud,
            # caso contrario si no existe registro, se envia false
            # para que este visible el boton
            line_ids = []
            if self.lines:
                line_ids = [x.id for x in self.lines]
            pool = Pool()
            ConsolidationLine = pool.get(
                'purchase.contract.process.consolidation.line')
            consolidation_lines = ConsolidationLine.search_read([
                ('process.state', '!=', 'cancel'),
                ('consolidation_line', 'in', line_ids),
            ], fields_names=['id'])
            if consolidation_lines:
                return True
            PurchaseRequest = pool.get('purchase.request')
            purchase_requests = PurchaseRequest.search_read([
                ('purchase_header.state', '!=', 'cancel'),
                ('consolidation_origin', 'in', line_ids),
            ], fields_names=['id'])
            if purchase_requests:
                return True
            return False
        return True

    @classmethod
    def search_by_products(cls, name, clause):
        model = ('lines.description')
        return [(model,) + tuple(clause[1:])]

    @fields.depends('lines')
    def on_change_with_employees_request(self, name=None):
        employees = []
        for line in self.lines:
            for detail in line.details:
                employees.append(
                    detail.requirement_material_line.requirement.employee.party.name)  # noqa
        employees = list(dict.fromkeys(employees))
        return ', '.join(employees)

    @fields.depends('lines')
    def on_change_with_product_names(self, name=None):
        products = ''
        for line in self.lines:
            for detail in line.details:
                name = (detail.requirement_material_line.description
                        if detail.requirement_material_line.product is None
                        else detail.requirement_material_line.product.name)
                products += name + ','
        return products

    @fields.depends('lines')
    def on_change_with_requirements_numbers(self, name=None):
        numbers = []
        for line in self.lines:
            for detail in line.details:
                requirement_line = detail.requirement_material_line.\
                    requirement_line_origin
                if requirement_line.requirements:
                    numbers.append(requirement_line.requirements.number)
        return ','.join(dict.fromkeys(numbers))

    @classmethod
    def search_by_requirements_numbers(cls, name, clause):
        model = ('lines.details.requirement_material_line'
            '.requirement_line_origin.requirements.number')
        return [(model,) + tuple(clause[1:])]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, consolidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, consolidations):
        dict_merger = defaultdict(lambda: [])
        for consolidation in consolidations:
            for line in consolidation.lines:
                if line.product:
                    dict_merger[line.product.id].append(line)
        if dict_merger:
            for product_id in dict_merger:
                purchase_consolidation_merger(dict_merger.get(product_id))

    @classmethod
    @ModelView.button
    @Workflow.transition('consolidated')
    def consolidated(cls, consolidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, consolidations):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('purchase.configuration')
        config = Config(1)
        for consolidation in consolidations:
            for line in consolidation.lines:
                if not line.product:
                    consolidation.raise_user_error('not_have_line_product', {
                        'product': line.description,
                    })
            if consolidation.number == '/':
                if config.purchase_consolidation_sequence:
                    consolidation.number = Sequence.get_id(
                        config.purchase_consolidation_sequence.id)
                else:
                    consolidation.raise_user_error('no_sequence_defined')
        cls.save(consolidations)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, consolidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('terminate')
    def terminate(cls, consolidations):
        pass

    @classmethod
    @ModelView.button
    def merger(cls, consolidations):
        line_merger = []
        dict_product = defaultdict(lambda: None)
        for consolidation in consolidations:
            for line in consolidation.lines:
                if line.merger:
                    if not line.product:
                        consolidation.raise_user_error('no_product_merge')
                    dict_product[line.product.id] = line.product.id
                    line_merger.append(line)
            if len(dict_product) > 1:
                consolidation.raise_user_error('no_more_product_error')
        purchase_consolidation_merger(line_merger)

    @classmethod
    @ModelView.button_action('public_pac.act_purchase_consolidation_m_r')
    def add_material_requirement(cls, consolidations):
        pass

    def get_rec_name(self, name):
        return f"{self.employee.party.name}"


class PurchaseConsolidationLine(ModelSQL, ModelView):
    'Purchase Consolidation Line'
    __name__ = 'purchase.consolidation.line'

    _states = {'readonly': Eval('parent_state') != 'request'}
    _depends = ['parent_state']

    purchase_consolidation = fields.Many2One(
        'purchase.consolidation', 'Consolidacion de compra', required=True)
    description = fields.Char(
        'Descripción', required=True, states=_states, depends=_depends)
    uom = fields.Many2One(
            'product.uom', 'Udm', required=True, states={'readonly': True}, depends=_depends)
    purchase_quantity = fields.Function(
        fields.Numeric('Cantidad de compra', digits=(16, 2)),
        'on_change_with_purchase_quantity')
    details = fields.Many2Many(
        'purchase.consolidation.line.detail', 'line', 'detail', 'Detalles',
        states={
            'readonly': True,
        })
    product = fields.Many2One(
        'product.product', 'Producto', depends=_depends,
        states={
            'readonly': (Eval('parent_state') != 'consolidated'),
        },
        domain=[('purchasable', '=', True)])
    parent_state = fields.Function(
        fields.Char('Estado de consolidacion de compra'),
        'on_change_with_parent_state')
    merger = fields.Boolean('Fusionar', states=_states, depends=_depends)
    process = fields.Selection([
        ('request', 'Solicitud'),
        ('contract', 'Contratacion'),
        ], 'Proceso', sort=False,
        states={
            'readonly': Eval('parent_state').in_(['done', 'cancel']),
        }, depends=_depends)
    process_to_translated = process.translated('process')
    observation = fields.Function(
        fields.Text('Observaciones'),
        'on_change_with_observation'
    )
    request = fields.Function(
        fields.Char('N. Solicitud de despacho'),
        'on_change_with_request',
        searcher='search_request'
    )

    @classmethod
    def __setup__(cls):
        super(PurchaseConsolidationLine, cls).__setup__()
        cls._order = [
            ('purchase_consolidation', 'ASC'),
            ('description', 'ASC'),
        ]

    @staticmethod
    def default_process():
        return 'request'

    @fields.depends('details')
    def on_change_with_purchase_quantity(self, name=None):
        amount = Decimal("0")
        for row in self.details:
            amount += row.purchase_quantity
        return amount.quantize(Decimal("0.01"))

    @fields.depends('product')
    def on_change_product(self):
        if self.product:
            self.uom = self.product.default_uom

    @staticmethod
    def default_merger():
        return False

    @fields.depends('purchase_consolidation')
    def on_change_with_parent_state(self, name=None):
        if self.purchase_consolidation:
            return self.purchase_consolidation.state
        return 'request'

    @fields.depends('details')
    def on_change_with_observation(self, name=None):
        text = ''
        for detail in self.details:
            material_line = detail.requirement_material_line
            if material_line and (
                    material_line.requirement_line_origin) and (
                    material_line.requirement_line_origin.observations):
                observation = (
                    f"{material_line.requirement_line_origin.observations}")
                suggestion_process = ''
                if material_line.requirement_line_origin.process_to_translated:
                    suggestion_process = (
                        f", proceso sugerido: "
                        f"{material_line.requirement_line_origin.process_to_translated}")
                text += f"{observation}{suggestion_process}"
        return text

    @fields.depends('purchase_consolidation')
    def on_change_with_request(self, name=None):
        number = '0'
        if self.purchase_consolidation and (
                self.purchase_consolidation.requirements_numbers):
            number = self.purchase_consolidation.requirements_numbers
        return number

    @classmethod
    def search_request(cls, name, clause):
        pool = Pool()
        ConsolidationLine = pool.get('purchase.consolidation.line')
        EnterPurchaseContractConsolidationLineStart = pool.get(
            'enter.purchase.contract.consolidation.line.start')
        wizard = EnterPurchaseContractConsolidationLineStart()
        lines = ConsolidationLine.browse(
            wizard.default_consolidation_line_temp())
        ids = []
        for line in lines:
            if clause[2].split('%')[1] in line.request:
                ids.append(line.id)
        return [('id', 'in', ids)]


class PurchaseConsolidationLineDetail(ModelSQL, ModelView):
    'Purchase Consolidation Line Detail'
    __name__ = 'purchase.consolidation.line.detail'

    line = fields.Many2One(
        'purchase.consolidation.line', 'Linea', required=True)
    detail = fields.Many2One(
        'purchase.consolidation.detail', 'Detalle', required=True)


class PurchaseConsolidationDetail(ModelSQL, ModelView):
    'Purchase Consolidation Detail'
    __name__ = 'purchase.consolidation.detail'

    _states = {'readonly': True}

    description = fields.Char('Descripción', required=True, states=_states)
    uom = fields.Many2One('product.uom', 'Udm', required=True, states=_states)
    purchase_quantity = fields.Numeric(
        'Cantidad a comprar', digits=(16, 2), required=True, states=_states)
    requirement_material_line = fields.Many2One(
        'purchase.material.requirement.line', 'Requerimiento material',
        required=True, states=_states)

    @classmethod
    def __setup__(cls):
        super(PurchaseConsolidationDetail, cls).__setup__()
        cls._order = [
            ('requirement_material_line', 'ASC'),
            ('description', 'ASC'),
        ]

    @fields.depends('requirement_material_line')
    def on_change_requirement_material_line(self):
        if self.requirement_material_line:
            self.description = self.requirement_material_line.description
            self.uom = self.requirement_material_line.uom
            self.purchase_quantity = (
                self.requirement_material_line.purchase_quantity)
        else:
            self.description = None
            self.uom = None
            self.purchase_quantity = None


class PurchaseSupplierLine(Workflow, ModelSQL, ModelView):
    'Purchase Supplier Line'
    __name__ = 'purchase.supplier.line'

    party = fields.Many2One('party.party', 'Proveedor', readonly=True,
        required=True)
    purchase_header = fields.Many2One('purchase.header', 'Compra')


class PurchaseBudget(ModelSQL, ModelView):
    'Purchase Budget Line'
    __name__ = 'purchase.request.budget'
    _history = True

    purchase_request = fields.Many2One('purchase.request', 'Solicitud Compra',
        ondelete='CASCADE')
    budget = fields.Many2One('public.budget', 'Partida',
        domain=[
            If(Eval('budgets_pac'),
                ('id','in', Eval('budgets_pac')),()),
            ('type', 'in', ['expense']),
            ('activity', 'child_of', Eval('poa', -1),
             'parent')
        ], depends=['purchase_request', 'poa'])
    budgets_pac = fields.Function(fields.One2Many(
        'public.budget', None, 'Partidas de PAC',),
        'on_change_with_budgets_pac')
    total_budguet = fields.Function(fields.Numeric(
        'Total disponible', digits=(16, 4),
        ), 'on_change_with_total_budguet' )
    poa = fields.Many2One('public.planning.unit', 'POA',
        states={
              'readonly': True,
        })
    #TODO: update when changed in request line
    quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ],
        states={'required': True,},
        depends=['purchase_request'])
    amount = fields.Numeric('Valor', digits=(16, 4),
        states={'required': True,})
    amount_tax = fields.Function(
            fields.Numeric('Impuesto',  digits=(16, 4)),
            'on_change_with_amount_tax'
        )
    total = fields.Function(
        fields.Numeric('Total',  digits=(16, 4)),
        'on_change_with_total'
    )

    @classmethod
    def __setup__(cls):
        super(PurchaseBudget, cls).__setup__()
        cls._error_messages.update({
        'incorrect_value_budget': ('El Importe de la partida es '
            'superio al valor disponible'),
        'error_amount': ('El valor por asignar es 0.00.\n Para agregar una '
            'nueva linea reduzca las asignacion en las partidas listadas'),
        'error_quantity': ('La cantidad por asignar es 0.\n Para agregar'
            ' una nueva linea reduzca la catidad en las partidas listadas')
        })

    @classmethod
    def default_purchase_request(cls):
        pool = Pool()
        Request = pool.get('purchase.request')
        transaction = Transaction()
        context = transaction.context
        request = Request(context['active_id'])
        return request.id

    @classmethod
    def default_poa(cls):
        pool = Pool()
        Request = pool.get('purchase.request')
        transaction = Transaction()
        context = transaction.context
        request = Request(context['active_id'])
        return request.poa.id

    @classmethod
    def default_quantity(cls):
        return Decimal('1')

    @fields.depends('quantity', 'purchase_request')
    def on_change_quantity(self):
        price = self.purchase_request.price
        if self.quantity and price:
            self.amount = self.quantity*price

    @fields.depends('purchase_request', '_parent_purchase_request.budgets')
    def on_change_purchase_request(self):
        context = Transaction().context
        remaining_value = context.get('remaining_value')
        remaining_quantity = context.get('remaining_quantity')
        quantity = Decimal(self.purchase_request.quantity)
        price = self.purchase_request.price
        #self.state = self.purchase_request.requirements.state
        tot = Decimal(quantity)*price
        new_quantity = (
            remaining_quantity if remaining_quantity else quantity
        )
        new_amount = remaining_value if remaining_value else tot
        if not remaining_value:
            total_amount_line = Decimal(0.0)
            total_quantity_line = Decimal(0.0)
            for budget in self.purchase_request.budgets:
                total_amount_line += budget.amount
                total_quantity_line += budget.quantity
            new_amount = tot - total_amount_line
            new_quantity = quantity - total_quantity_line

        if self.purchase_request.product.type == 'service':
            if remaining_value == 0:
                self.raise_user_error('error_amount')
            else:
                self.amount = new_amount
            self.quantity = 1
        else:
            if remaining_quantity == 0:
                self.raise_user_error('error_quantity')
            else:
                self.quantity = new_quantity

            if remaining_value == 0:
                self.raise_user_error('error_amount')
            else:
                self.amount = Decimal(new_quantity)*price
        self.on_change_with_amount_tax()




        """request = self.purchase_request
        tot = Decimal(str(request.quantity)) * request.estimated_unit_price
        self.amount = tot"""

    @fields.depends('purchase_request', '_parent_purchase_request.budgets')
    def on_change_with_budgets_pac(self, name=None):
        if self.purchase_request.pac_line:
            #TODO: avoid duplicates
            #in_list = [x.budget.id for x in self.purchase_request.budgets]
            in_pac = (
                [x.budget.id for x in self.purchase_request.pac_line.budgets])
            return in_pac
        return []

    @fields.depends('budget')
    def on_change_with_total_budguet(self, name=None):
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            return  budget_data.certified_pending
        else:
            return Decimal(0)

    @fields.depends('purchase_request', '_parent_purchase_request.taxes',
        'amount', 'quantity')
    def on_change_with_amount_tax(self, name=None):
        taxes_amount = Decimal('0.0000')
        for lTax in self.purchase_request.taxes:
            if self.amount:
                taxes_amount += lTax.tax.rate * self.amount
        return taxes_amount

    @fields.depends('purchase_request', '_parent_purchase_request.taxes',
        'amount', 'budget', 'quantity')
    def on_change_with_total(self, name=None):
        amount_tax = self.on_change_with_amount_tax()
        total_budguet = self.on_change_with_total_budguet()
        withTax = Decimal(0.0)
        if total_budguet:
            withTax = self.amount + amount_tax
        if withTax > total_budguet:
            print("todo:error")
            #self.raise_user_error("incorrect_value_budget")
        return withTax

class PurchaseHeaderTax(ModelSQL, ModelView):
    'Purchase Header Tax'
    __name__ = 'purchase.header.tax'
    _history = True

    purchase_header = fields.Many2One('purchase.header', 'Solicitud de compra',
        ondelete='CASCADE', states={
            'readonly': True,
            'required': True
        })
    tax = fields.Many2One('account.tax', 'Linea de impuestos',
        states={
            'readonly': Eval('_parent_state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])


class PurchaseHeader(Workflow, ModelSQL, ModelView):
    'Solicitud de Compra'
    __name__ = 'purchase.header'
    _history = True

    _STATES = {
        'readonly': ~Eval('state').in_(['draft'])
    }
    _DEPENDS = ['state']

    number = fields.Char('Número', states={'readonly': True})
    company = fields.Many2One('company.company', 'Company', states={
        'readonly': True,
        'required': True
    })
    employee = fields.Many2One('company.employee', 'Responsable',
        states={
            'readonly': ~((Eval('state') == 'draft') &
                (Eval('context', {}).get('groups', []).contains(
                    Id('public_pac', 'group_public_pac_edit'))
                )),
            'required': True
        }, depends=_DEPENDS)
    department = fields.Many2One('company.department', 'Departamento',
        states={
            'readonly': ~((Eval('state') == 'draft') &
                (Eval('context', {}).get('groups', []).contains(
                    Id('public_pac', 'group_public_pac_edit'))
                )),
            'required': True
        }, depends=_DEPENDS + ['employee'])
    date = fields.Date('Date', readonly=True)
    poa = fields.Many2One('public.planning.unit', 'POA',
        domain=[
            ('type', '=', 'poa'),
            ('company', '=', Eval('company'))
        ], states={
            'readonly': True,
            'required': True
        }, depends=_DEPENDS + ['company'])
    lines = fields.One2Many('purchase.request',
        'purchase_header', 'Solicitudes', states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('request', 'Solicitado'),
        ('confirm', 'Aprobar'),
        ('quoted', 'Cotizados'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado')
    ], 'Estado', readonly=True)
    suppliers = fields.Many2Many(
        'purchase.supplier.line', 'purchase_header', 'party', 'Proveedores',
        states={
            'readonly': Eval('state').in_(['done'])
        }
        )
    certificate = fields.Many2One(
        'public.budget.certificate', 'Certificate',
        states={
            'readonly': Eval('state').in_([
                'draft', 'request', 'done', 'cancel'
            ]),
        },
        domain=[('state', '!=', 'cancel')], depends=['state'])
    send_quotation = fields.Boolean('Enviar Cotizacion?', states={
            'readonly':  Eval('state')=='done'
        }, depends=['state'])
    description = fields.Text('Observaciones', states={
            'readonly': ~Eval('state').in_([
                'draft', 'request', 'confirm', 'validated']),
            'required': Eval('state') != 'draft',
        }, depends=['state'])
    request_date = date_field('Fecha solicitud')
    done_date = date_field('Fecha Aprobacion')
    request_by = employee_field('Solicitado por')
    done_by = employee_field('Aprobado por')
    consolidation_numbers = fields.Function(
        fields.Char('Numeros de consolidacion'),
        'get_consolidations',
        searcher='search_consolidations'
    )
    requirement_numbers = fields.Function(
        fields.Char('Numeros de solicitud'),
        'on_change_with_requirement_numbers',
        searcher='search_by_requirement_numbers'
    )
    products = fields.Function(
        fields.Char('Productos'),
        'on_change_with_products',
        searcher='search_by_products'
    )
    taxes = fields.Many2Many('purchase.header.tax', 'purchase_header',
        'tax', 'Lineas de impuestos',
        states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])

    add_iva = fields.Boolean('Añadir IVA',
        states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])
    add_ice = fields.Boolean('Añadir ICE',
        states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total'
    )
    suggested_procedure = fields.Many2One('purchase.pac.type.contract',
        'Tipo de compra',
        domain=[
            ('used_on_public_purchases','=',True),
            ('purchase_request','=',True)
        ],
        states={
            'readonly': ~Eval('state').in_(['draft']),
            'required': True
        }, depends=['state'])
    info = fields.Text('Información', states={
            'readonly': True,
        })

    @classmethod
    def __setup__(cls):
        super(PurchaseHeader, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'draft'),
            ('request', 'confirm'),
            ('confirm', 'quoted'),
            ('confirm', 'cancel'),
            ('quoted', 'done'),
            ('quoted', 'cancel'),
            ('quoted', 'confirm'),
            ('request', 'cancel'),
            ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['request']),
            },
            'winning_quote': {
                'invisible': ~Eval('state').in_(['request']),
            },
            'request': {
                'invisible': ~Eval('state').in_(['draft']),
            },

            'confirm': {
                'invisible': ~Eval('state').in_(['request', 'quoted']),
            },
            'validated': {
                'invisible':  Eval('state').in_(['done'])
            },
            'quoted':{
                'invisible': ~Eval('state').in_(['validated', 'confirm']),
            },
            'cancel': {
                'invisible': ~Eval('state').in_([
                    'request', 'confirm', 'quoted', 'validated'
                ]),
            },
            'done': {
                'invisible': ~Eval('state').in_(['quoted']),
            },
            'add_purchase_consolidation': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
            'request_quotation': {
                'invisible': ~Eval('state').in_(['confirm', 'quoted', 'validated']),
                'depends': ['state']
            },
        })
        cls._error_messages.update({
            'no_pac_line':('La solicitud "%(request)s" '
                'no tienen asignado una linea del PAC.'
            ),
            'no_suggested_procedure':('La solicitud "%(request)s" '
                'no tienen asignado un tipo de adquiscion.'
            ),
            'no_request_requirements':('La solicitud "%(request)s" '
                'no tiene requisistos.'
            ),
            'no_budget':('La solicitud "%(request)s" '
                'no tiene asignada una partida.'
            ),
            'no_suppliers_invited':('La solicitud no tiene '
                'proveedores invitados.'
            ),
            'no_preferred_quotation':('La solicitud "%(request)s" '
                'no tiene un proveedor adjudicado.'
            ),
            'price_doesnt_match':('El total (%(tot_budgets)s) de las partidas'
                ' de la solicitud "%(request)s" es diferente al '
                'total(%(tot_purchase)s) de la compra.'
            ),
            'incorrect_infima_process_type':('El valor "%(value)s" '
                'de la compra supera la infima cuantia'
            ),
            'incorrect_process_type':('El valor "%(value)s" '
                'para el proceso "%(type)s" es incorreto.'
            ),
            'error_total_certificate':('El valor de la compra es superior '
                'al valor certificado.'),
            'no_sequence_defined': (
                'No existe secuencia definida para la solicitud de compra.'
                'Por favor, diríjase a "Configuración de Compras" y '
                'configure una secuencia.'
            ),
            'cant_delete_certificate': (
                'No se puede regresar, el certificado se encuentra aprobado.'),
            'no_certificate': ('La solicitud no tiene certificado '
                'presupuestario.'),
            'budget_no_certificate': ('Una o mas partidas de la solicitud '
                'de compra no se encuentran en el certificado.\n'
                'Las partidas no encontradas seran listadas en el campo '
                'Información'),
            'used_certificate': ('El certificado enlazado esta siendo '
                'utilizado en uno o mas procesos:\n %(models)s'),
            'budget_value_incorrect': ('Una o mas partidas de la certificacion '
                ' no tienes fondos para solventar el gasto de compra.\n'
                'Las partidas que no tienen fondos seran listadas en el campo '
                'Información'),
            'done_certificate': ('El certificado presupuestario '
                'no esta aprobado'),
            'no_email': ('El tercero "%(supplier)s" '
                'no tiene registrado un correo electronico.'),
            'diferent_quantity_of_request': (
                '%(product)s fue solicitado en un inicio %(initial)s, y la '
                'cantidad a comprar es %(buy)s. Esto se debe a un despacho '
                'realizado en bodega, por lo que se pide actualizar esta '
                'información en el presupuesto'),
            'incorrect_value_budget': ('El valor asignado a la '
                'partida %(budget)s es superio al valor disponible.\nLinea: '
                '%(line)s'),
        })

    @staticmethod
    def default_number():
        return '/'

    @classmethod
    def copy(cls, purchases, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('number', '/')
        default.setdefault('request_date', None)
        default.setdefault('done_date', None)
        default.setdefault('request_by', None)
        default.setdefault('done_by', None)
        default.setdefault('date', date.today())
        default.setdefault('certificate', None)
        return super(PurchaseHeader, cls).copy(purchases, default=default)

    @staticmethod
    def default_send_quotation():
        return False

    @staticmethod
    def default_poa():
        pool = Pool()
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        today = Date.today()
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])[0]
        return poa.id

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_employee(cls):
        return Transaction().context.get('employee')

    @classmethod
    def default_department(cls):
        employee = Pool().get('company.employee').search([
            ('id', '=', Transaction().context.get('employee')),
        ])
        if employee:
            return employee[0].contract_department.id

    @staticmethod
    def default_date():
        return date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('suggested_procedure', 'lines')
    def on_change_suggested_procedure(self):
        for line in self.lines:
            line.suggested_procedure = self.suggested_procedure

    @classmethod
    def validate(cls, headers):
        for header in headers:
            if header.state == 'request':
                for line in header.lines:
                    quantity = Decimal("0")
                    if line.budgets:
                        quantity += reduce(lambda a, b: a + b, [
                            x.quantity for x in line.budgets])
                    if line.product.type != 'service':
                        if Decimal(line.quantity) != quantity:
                            cls.raise_user_error(
                                'diferent_quantity_of_request', {
                                    'product': line.product.name,
                                    'initial': quantity,
                                    'buy': Decimal(line.quantity),
                                })

    @fields.depends('add_iva', 'taxes')
    def on_change_add_iva(self):
        pool = Pool()
        AccountTax = pool.get('account.tax')
        HeaderTax = pool.get('purchase.header.tax')
        iva_ = AccountTax.search(['name', '=','IVA 12%'])[0]

        to_save_tax = []
        for tax in self.taxes:
            if tax.id != iva_.id:
                to_save_tax.append(tax)

        if self.add_iva and len(self.taxes)==len(to_save_tax):
            to_save_tax.append(iva_)

        self.taxes = to_save_tax

    @fields.depends('add_ice', 'taxes')
    def on_change_add_ice(self):
        pool = Pool()
        AccountTax = pool.get('account.tax')
        HeaderTax = pool.get('purchase.header.tax')
        ice_ = AccountTax.search(['name', '=','ICE 15%'])[0]
        to_save_tax = []
        for tax in self.taxes:
            if tax.id != ice_.id:
                to_save_tax.append(tax)

        if self.add_ice and len(self.taxes)==len(to_save_tax):
            to_save_tax.append(ice_)

        self.taxes = to_save_tax



    @fields.depends('taxes', 'lines')
    def on_change_taxes(self):
        #TODO: delete line taxes when delete header taxes
        RequestTax = Pool().get('purchase.request.tax')
        for line in self.lines:
            currentTaxes = []
            taxes_amount = Decimal("0.0000")
            for tax in line.taxes:
                currentTaxes.append(tax.tax.id)
                taxes_amount += line.price * tax.tax.rate

            to_save_tax = []
            for tax in self.taxes:
                if tax.id not in currentTaxes:
                    taxes_amount += line.price * tax.rate
                    nRT = RequestTax(
                        request=line,
                        tax=tax,
                        origin='header'
                    )
                    to_save_tax.append(nRT)
            withTax = line.price + taxes_amount
            line.taxes = to_save_tax
            line.total = (
                Decimal(line.quantity) * withTax
            ).quantize(Decimal("0.0001"))
        #Notes
        #nRT => new request tax

    @fields.depends('lines', 'taxes')
    def on_change_with_total(self, name=None):
        total = Decimal('0.0')
        for line in self.lines:
            total += line.total
        return total

    @fields.depends('lines')
    def on_change_with_requirement_numbers(self, name=None):
        numbers = []
        for line in self.lines:
            if line.id != -1:
                if line.consolidation_origin:
                    for detail in line.consolidation_origin.details:
                        requirement_line = detail.requirement_material_line.\
                            requirement_line_origin
                        if requirement_line.requirements:
                            numbers.append(requirement_line.requirements.number)
        return ','.join(dict.fromkeys(numbers))

    @fields.depends('certificate')
    def on_change_certificate(self):
        RequestHeader = Pool().get('purchase.header')
        bdtCertificate = []
        bdtRequest = []
        infoWarning = ""
        budgetNoFound = False
        noFunds = False
        with Transaction().new_transaction(readonly=False) as new_transaction:
            if self.lines and self.certificate:
                ctLines = self.certificate.lines
                #get data for validations
                ctBudgets = {}
                for line in ctLines:
                    ctBudgets[line.budget.code] = {
                        'amount': line.amount
                    }
                scBudgets = []
                for line in self.lines:
                    for budget in line.budgets:
                        scBudgets.append(budget)
                #getting amounts by budgets of request
                bdtAmount = self.amount_by_budget(scBudgets)
                #validations: if budgets request exist inside certificate
                for budget in bdtAmount:
                    if not ctBudgets.get(budget, None):
                        budgetNoFound = True
                        infoWarning += ('La paritda ' + budget +
                                ' no se ecuentra en el certificado.\n')
                    else:
                        withTax = (
                            bdtAmount[budget]['amountWithTax'].quantize(
                                Decimal('0.01'))
                        )
                        if withTax > ctBudgets[budget]['amount']:
                            noFunds = True
                            infoWarning += ('La partida ' + budget + ' no '
                                'tiene fondos para solventar la compra: \n'
                                'Total partida certificacion: '+
                                str(ctBudgets[budget]['amount']) +
                                '\nTotal linea de compra: '+ str(withTax) +'.')

                usedCertificates = RequestHeader.search([
                    ('certificate', '=', self.certificate.id),
                    ('id', '!=', self.id),
                    ('state', '!=', 'cancel')
                ])
                if usedCertificates:
                    models = ''
                    for ucert in usedCertificates:
                        models += 'Solicitud de compra: #' + ucert.number +'\n'
                    infoWarning += ('\nEl certificado enlazado esta siendo '
                        'utilizado en uno o mas procesos:\n' + str(models))
                    self.raise_user_warning(
                        'used_certificate',
                        'used_certificate', {
                            'models': models
                        }
                    )
                if budgetNoFound:
                    self.raise_user_warning(
                        'budget_no_certificate',
                        'budget_no_certificate')
                if noFunds:
                    self.raise_user_warning(
                        'budget_value_incorrect',
                        'budget_value_incorrect')

            self.info = infoWarning

    @classmethod
    def search_by_requirement_numbers(cls, name, clause):
        model = ('lines.consolidation_origin.details.requirement_material_line'
            '.requirement_line_origin.requirements.number')
        return [(model,) + tuple(clause[1:])]

    @fields.depends('lines')
    def on_change_with_products(self, name=None):
        products = []
        for line in self.lines:
            products.append(line.product.name)
        return ','.join(dict.fromkeys(products))

    @classmethod
    def search_by_products(cls, name, clause):
        model = ('lines.product')
        return [(model,) + tuple(clause[1:])]

    def get_consolidations(self, name):
        numbers = []
        for line in self.lines:
            if line.consolidation_origin:
                consolidation = line.consolidation_origin.purchase_consolidation
                numbers.append(consolidation.number)
        return ','.join(dict.fromkeys(numbers))

    @classmethod
    def search_consolidations(cls, name, clause):
        return [
            ('lines.consolidation_origin.purchase_consolidation.number',) +
            tuple(clause[1:])]


    # Buttons
    @classmethod
    @ModelView.button_action('public_pac.act_purchase_header_winning_quote')
    def winning_quote(cls, purchases):
        pass

    @classmethod
    def quotations_exist(cls, purchases, new_supplier):
        pool = Pool()
        Quotation = pool.get('purchase.request.quotation')
        QuotationLine = pool.get('purchase.request.quotation.line')
        exists = False
        for purchase in purchases:
            for supplier in purchase.suppliers:
                if new_supplier.id == supplier.id:
                    quotation = Quotation.search([
                        ('supplier', '=', supplier.id),
                        ('request_number', '=', purchase.number)
                    ])
                    for line in purchase.lines:
                        quotation_line = QuotationLine.search([
                            ('header', '=', purchase.id),
                            ('request', '=', line.id)
                        ])
                        if quotation and quotation_line:
                            exists = True
        return exists

    @classmethod
    @ModelView.button
    def request_quotation(cls, purchases):
        pool = Pool()
        Quotation = pool.get('purchase.request.quotation')
        QuotationLine = pool.get('purchase.request.quotation.line')
        PartyAdress = pool.get('party.address')
        PurchaseQuotationRequirements = (
            pool.get('purchase.request.quotation.line.requirements')
        )
        exists = False
        for purchase in purchases:
            if not purchase.suppliers:
                cls.raise_user_error('no_suppliers_invited')
            for supplier in purchase.suppliers:
                exists = cls.quotations_exist(purchases, supplier)
                if not exists:
                    adress = PartyAdress.search([('party','=',supplier.id)])
                    quotation = Quotation(
                        supplier=supplier,
                        supplier_address=adress[0] if adress else None,
                        state='draft',
                        request_number=purchase.number
                    )
                    to_save_quotation_line = []
                    to_save_quotation_requirements = []
                    for line in purchase.lines:
                        requirements = line.purchase_request_requirements
                        quotation_line = QuotationLine(
                            quotation=quotation,
                            header=purchase,
                            request=line,
                        )
                        quotation_line.on_change_request()
                        for requirement in requirements:
                            purchase_requirement = PurchaseQuotationRequirements(
                                purchase_request_requirements = requirement,
                                purchase_request_quotation_line = quotation_line,
                                meets_requirements = False,
                            )
                            to_save_quotation_requirements.append(
                                purchase_requirement
                            )
                        to_save_quotation_line.append(quotation_line)
                    quotation.save()
                    QuotationLine.save(to_save_quotation_line)
                    Quotation.set_number([quotation])
                    PurchaseQuotationRequirements.save(
                            to_save_quotation_requirements
                    )
                    quotation.state = 'sent'
                    quotation.save()
                    if purchase.send_quotation:
                        cls.send_emails(supplier, purchase)


    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, purchases):
        pass

    @classmethod
    @ModelView.button
    def validated(cls, purchases):
        pool = Pool()
        BudgetLine = pool.get('purchase.request.budget')
        Certificate = pool.get('public.budget.certificate')
        for purchase in purchases:
            for line in purchase.lines:
                total_budget = Decimal('0.0000')
                for budget in line.budgets:
                    total_budget += budget.amount
                to_update = []
                for budget in line.budgets:
                    if budget.amount >0 and total_budget >0 :
                        div = budget.amount / total_budget
                        budget.amount = ( div * line.price*Decimal(line.quantity)
                            ).quantize(Decimal("0.0001"))
                        to_update.append(budget)
                    else:
                        budget.on_change_quantity()
                        to_update.append(budget)
                line.budgets = to_update
                BudgetLine.save(to_update)
            purchase.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    @set_employee('request_by')
    def request(cls, purchases):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('purchase.configuration')
        config = Config(1)
        for purchase in purchases:
            is_checked = False
            if purchase.number == '/':
                if config.purchase_header_sequence:
                    purchase.number = Sequence.get_id(
                        config.purchase_header_sequence.id)
                else:
                    purchase.raise_user_error('no_sequence_defined')
            for line in purchase.lines:
                if len(line.budgets)==0:
                    cls.raise_user_error('no_budget',{
                            'request':  line.description
                        })
                if not line.purchase_request_requirements:
                    is_checked = True
            if is_checked:
                cls.raise_user_warning('no_request_requirements',
                    'no_request_requirements',{
                        'request':  line.description
                })
        cls.save(purchases)

    @classmethod
    def validate_total_request(cls, purchase):
        budget_total = Decimal('0.0000')
        purchase_total = Decimal('0.0000')
        for line in purchase.lines:
            purchase_total += line.total
            for budget in line.budgets:
                budget_total += budget.amountWithTax
        budget_tot = budget_total.quantize(Decimal("0.0001"))
        purchase_tot = purchase_total.quantize(Decimal("0.0001"))
        resp = {
            'purchase': purchase_tot,
            'budget': budget_tot,
            'status': budget_tot == purchase_tot
        }
        return resp

    @classmethod
    def validate_total_line(cls, line):
        budget_total = Decimal('0.0000')
        taxes_amount = Decimal('0.0000')

        if not line.budgets:
            cls.raise_user_error('no_budget',{
                'request':  line.description
            })
        for budget in line.budgets:
            if budget.total > budget.total_budguet:
                cls.raise_user_error('incorrect_value_budget', {
                    'budget': budget.budget.code,
                    'line': line.product.name
                })
            budget_total += budget.total


        budget_total = budget_total.quantize(Decimal('0.0001'))

        if budget_total != line.total:
            cls.raise_user_error('price_doesnt_match', {
                    'tot_budgets': budget_total,
                    'tot_purchase': line.total,
                    'request': line.description,
            })
        return line.budgets

    @classmethod
    @ModelView.button
    @Workflow.transition('quoted')
    def quoted(cls, purchases):
        pool = Pool()
        Company = pool.get('company.company')
        Certificate = pool.get('public.budget.certificate')
        CertificateLine = pool.get('public.budget.certificate.line')
        CostCenter = pool.get('public.cost.center')
        TaxCertificateLine = Pool().get('public.budget.certificate.line.tax')
        Budget = pool.get('public.budget')
        Period = pool.get('account.period')
        company = Company(Transaction().context['company'])
        currency = company.currency
        cost_center = CostCenter.search([])

        for purchase in purchases:
            budgets = []
            suggested_procedure = ''
            for line in purchase.lines:
                #validate winners
                if not line.preferred_quotation_line:
                    if not line.precertification:
                        cls.raise_user_error('no_preferred_quotation',{
                            'request': line.description
                        })
                    if not line.suggested_procedure:
                        cls.raise_user_error('no_suggested_procedure',{
                            'request': line.description
                        })
                if not purchase.certificate:
                    budgets += cls.validate_total_line(line)
                suggested_procedure = line.suggested_procedure.type_contract
                line.state = 'done'

            if not purchase.certificate:
                purchase_description = ''
                if purchase.description:
                    purchase_description = f", {purchase.description}"
                data = {
                    'line': purchase,
                    'currency': currency,
                    'Period': Period,
                    'Certificate': Certificate,
                    'description': (
                        'Solicitud de compra #'+str(purchase.number)+
                        ', mediante el proceso de compra de'
                        ' '+str(suggested_procedure)+" "+purchase_description)
                }
                certificate_header = cls.create_certificate_header(data)
                by_budgets = cls.amount_by_budget(budgets)
                data_certificate = {
                    'budgets': by_budgets,
                    'Budget': Budget,
                    'CertificateLine': CertificateLine,
                    'certificate': certificate_header,
                    'cost_center': cost_center
                }
                certificate_lines = (
                    cls.generate_lines_certificate(data_certificate)
                )
                certificate_header.save()
                CertificateLine.save(certificate_lines['lines'])
                TaxCertificateLine.save(certificate_lines['taxes'])
                purchase.certificate = certificate_header
                purchase.save()
                #purchase.on_change_certificate()

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, purchases):
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        for purchase in purchases:
            if purchase.certificate:
                if purchase.certificate.state == 'draft':
                    Certificate.delete([purchase.certificate])
                elif purchase.certificate.state == 'cancel':
                    pass
                else:
                    cls.raise_user_error('cant_delete_certificate')

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, purchase):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def done(cls, purchases):
        pool = Pool()
        Request = pool.get('purchase.request')
        Purchase = pool.get('purchase.purchase')
        Line = pool.get('purchase.line')
        Date = pool.get('ir.date')
        Company = pool.get('company.company')
        company = Company(Transaction().context['company'])
        currency = company.currency
        Certificate = pool.get('public.budget.certificate')
        CertificateLine = pool.get('public.budget.certificate.line')
        CostCenter = pool.get('public.cost.center')
        Budget = pool.get('public.budget')

        today = Date.today()
        Period = pool.get('account.period')
        cost_center = CostCenter.search([])

        to_save_certificate = []
        to_save_certificate_line = []
        to_update = []
        is_checked_budget = True
        total_purchase = Decimal("0.0")
        for purchase in purchases:
            if purchase.certificate.state != 'done':
                cls.raise_user_error('done_certificate')
            purchase.request_date = Date.today()
            period = Period.search([
                ('start_date', '<=', purchase.date),
                ('end_date', '>=', purchase.date)
            ])
            for line in purchase.lines:
                if not line.preferred_quotation_line:
                    cls.raise_user_error('no_preferred_quotation',{
                        'request': line.description
                    })

                if line.best_quotation_line:
                    total_purchase += (line.best_quotation_line.unit_price*
                    Decimal(line.quantity))
            if total_purchase.quantize(Decimal('0.01')) > purchase.certificate.total:
                cls.raise_user_error('error_total_certificate')

            by_parties = cls.group_by_parties_type(purchase.lines)
            for key, supplier in by_parties.items():
                for key1, type_ in supplier.items():
                    val_by_type = reduce(lambda x,y:x+y,[
                        Decimal(x['line'].quantity)
                        *x['line'].best_quotation_line.unit_price
                        for x in type_])
                    if (val_by_type > type_[0]['type'].min_value
                        and val_by_type <= type_[0]['type'].max_value):
                        purchase_header = cls.create_purchase_header(
                            purchase,
                            Purchase,
                            today,
                            purchase.certificate.description
                        )
                        data_purchase = {
                            'by_type': type_,
                            'Line': Line,
                            'purchase': purchase_header,
                            'certificate': purchase.certificate,
                            'currency': currency
                        }
                        purchase_lines = (
                            cls.generate_lines_purchase(data_purchase)
                        )
                        Line.save(purchase_lines)
                        cls.save(purchases)
                    else:
                        cls.raise_user_error('incorrect_process_type',{
                                'value': val_by_type,
                                'type': type_[0]['type'].type_contract
                            })
    @classmethod
    def amount_by_budget(cls, budgets):
        #TODO: si los impuestos son diferentes no unificar partidas
        by_budgets = {}
        for item in budgets:
            budget = item.budget
            if budget.code in by_budgets:
                by_budgets[budget.code] = {
                    'amount': (by_budgets[budget.code]['amount'] +
                    item.amount),
                    'amountWithTax': (by_budgets[budget.code]['amountWithTax'] +
                    item.total),
                    'taxes': item.purchase_request.taxes
                }
            else:
                by_budgets[budget.code] = {
                    'amount':  item.amount,
                    'amountWithTax': item.total,
                    'taxes': item.purchase_request.taxes
                }
        return by_budgets

    @classmethod
    def create_purchase_header(cls, purchase, Purchase, today, description):
        return Purchase(
            purchase_date=today,
            department=purchase.department,
            poa=purchase.poa,
            payment_deadline=today,
            description=description
        )

    @classmethod
    def create_certificate_header(cls, data):
        pool = Pool()
        BudgetLine = pool.get('purchase.request.budget')
        line = data['line']
        description = data['description']
        Period = data['Period']
        currency = data['currency']
        Certificate = data['Certificate']
        Date = pool.get('ir.date')
        today = Date.today()
        period, = Period.search([
                ('start_date', '<=', today),
                ('end_date', '>=', today)
            ])
        new_certificate = Certificate(
            company=line.company,
            department=line.department,
            period=period,
            currency=currency,
            poa=line.poa,
            type_='manual',
            date=today,
            requesting_employee=line.employee,
            requesting_department=line.department,
            description=description
        )
        new_certificate.period_start_date = (
            new_certificate.on_change_with_period_start_date())
        new_certificate.period_end_date = (
            new_certificate.on_change_with_period_end_date())
        return new_certificate

    @classmethod
    def generate_lines_certificate(cls, data):
        TaxCertificateLine = Pool().get('public.budget.certificate.line.tax')
        by_budgets = data['budgets']
        Budget = data['Budget']
        CertificateLine = data['CertificateLine']
        new_certificate = data['certificate']
        cost_center = data['cost_center']

        to_save_certificate_line = []
        to_save_taxes = []
        for item in by_budgets:
            budget = Budget.search_read([('code', '=', item)])[0]
            #TODO: eliminar cuando se agregue el detalle de impuesto
            #en la certificacion
            tax_amount =  by_budgets[item]['amountWithTax'] \
                - by_budgets[item]['amount']

            new_certificate_line = CertificateLine(
                certificate=new_certificate,
                type='line',
                budget=budget,
                untaxed_amount=(
                    by_budgets[item]['amount'].quantize(Decimal("0.01"))
                ),
                taxed_amount=tax_amount.quantize(Decimal("0.01")),
                amount=(
                    by_budgets[item]['amountWithTax'].quantize(Decimal("0.01"))
                ),
                cost_center=cost_center[0],
            )
            for rTax in by_budgets[item]['taxes']:
                new_tax = TaxCertificateLine(
                    line=new_certificate_line,
                    tax=rTax.tax
                )
                to_save_taxes.append(new_tax)
            #new_certificate_line.on_change_with_amount()
            #new_certificate_line.on_change_untaxed_amount(new_certificate_line)
            to_save_certificate_line.append(new_certificate_line)
            by_budgets[item] = new_certificate
        return {
            'lines': to_save_certificate_line,
            'taxes': to_save_taxes
        }


    @classmethod
    def generate_lines_purchase(cls, data):
        pool = Pool()
        BudgetRequest = pool.get('purchase.request.budget')
        BudgetLine = pool.get('purchase.line.budget')
        BudgetPac = pool.get('purchase.pac.budget')
        BudgetLineTax = pool.get('purchase.line-account.tax')
        by_type = data['by_type']
        Line = data['Line']
        purchase_header = data['purchase']
        certificate_header = data['certificate']
        currency = data['currency']

        to_save_purchase_line = []
        for item in by_type:
            line = item['line']
            budget_request = BudgetRequest.search([
                            ('purchase_request', '=', line.id)
                    ])

            new_line = Line()
            new_line.unit_price = line.price
            new_line.quantity = line.quantity
            purchase_header.party = line.best_quotation_line.supplier
            purchase_header.invoice_address = (
                line.best_quotation_line.supplier.addresses[0]
                if line.best_quotation_line.supplier.addresses
                else None
            )
            new_line.purchase = purchase_header
            new_line.requests = [line]
            new_line.product = line.product
            new_line.currency = currency
            new_line.unit = line.uom
            to_save_budgets = []
            #TODO: delete to implement PAC based in budgets
            #if not line.pac_line:
            for budget in budget_request:
                budget_line = BudgetLine(
                    budget=budget.budget,
                    amount=budget.amount,
                    quantity=budget.quantity
                )
                to_save_budgets.append(budget_line)
            new_line.budgets = to_save_budgets
            BudgetLine.save(to_save_budgets)
            new_line.pac_budget=budget_request[0].budget
            new_line.budget=budget_request[0].budget

            to_save_taxes = []
            for lTax in line.taxes:
                new_tax = BudgetLineTax(
                    line=new_line,
                    tax=lTax.tax
                )
                to_save_taxes.append(new_tax)
            BudgetLineTax.save(to_save_taxes)
            to_save_purchase_line.append(new_line)



            certificate_header.description=('Compra a proveedor: '+
                line.best_quotation_line.supplier.rec_name)
            """
            if line.pac_line:
                new_line.pac_line = line.pac_line
                new_line.pac_budget = line.pac_line.activity_budget
            """



            #new_line.unit_price = line.best_quotation_line.unit_price
            purchase_header.certificate = certificate_header
            purchase_header.save()
            # new_line.on_change_product()
        return to_save_purchase_line

    @classmethod
    def group_by_parties_type(cls, lines):
        pool = Pool()
        by_parties = {}
        for line in lines:
            #budget = (line.pac_line.activity_budget.id
            #if line.pac_line else line.budget.id)
            budgets = []
            #TODO: delete to implement PAC based in budgets
            '''
            if line.pac_line:
                pass
                budgets.append(line.pac_line.activity_budget.id)
            else:
            '''
            BudgetLine = pool.get('purchase.request.budget')
            budget_line = BudgetLine.search([
                        ('purchase_request', '=', line.id)
                ])
            for budget in budget_line:
                budgets.append(budget)

            supplier = (
                line.best_quotation_line.supplier.id
                if line.best_quotation_line
                else "Unico"
            )
            type = line.suggested_procedure.type_contract
            if supplier in by_parties:
                if type in by_parties[supplier]:
                    by_parties[supplier][type].append(
                        {
                            'line': line,
                            'budget': budgets,
                            'amount': (
                                Decimal(line.quantity) * line.price),
                            'type': line.suggested_procedure
                        })
                else:
                    by_parties[supplier][type] = [{
                        'line': line,
                        'budget': budgets,
                        'amount': (
                            Decimal(line.quantity) * line.price),
                        'type': line.suggested_procedure
                    }]
            else:
                by_process = {}
                by_process[type] = [{
                    'line': line,
                    'budget': budgets,
                    'amount': (Decimal(line.quantity) * line.price),
                    'type': line.suggested_procedure
                }]
                by_parties[supplier] = by_process
        return by_parties

    @classmethod
    def group_by_parties_type_save(cls, lines):
        by_parties = {}
        for line in lines:
            budget = (line.pac_line.activity_budget.id
                if line.pac_line else line.budget.id)
            supplier = line.best_quotation_line.supplier.id
            type = line.suggested_procedure.type_contract
            if supplier in by_parties:
                if type in by_parties[supplier]:
                    by_parties[supplier][type].append(
                        {
                            'line': line,
                            'budget': budget,
                            'amount': (
                                Decimal(line.best_quotation_line.quantity) *
                                line.best_quotation_line.unit_price),
                            'type': line.suggested_procedure
                        })
                else:
                    by_parties[supplier][type] = [{
                        'line': line,
                        'budget': budget,
                        'amount': (
                            Decimal(line.best_quotation_line.quantity) *
                            line.best_quotation_line.unit_price),
                        'type': line.suggested_procedure
                    }]
            else:
                by_process = {}
                by_process[type] = [{
                    'line': line,
                    'budget': budget,
                    'amount': (Decimal(line.best_quotation_line.quantity) *
                        line.best_quotation_line.unit_price),
                    'type': line.suggested_procedure
                }]
                by_parties[supplier] = by_process
        return by_parties


    @classmethod
    def compute_purchase_line(cls, key, requests, purchase):
        pool = Pool()
        Line = pool.get('purchase.line')

        line = Line()
        line.unit_price = Decimal(0).quantize(
            Decimal(1) / 10 ** Line.unit_price.digits[1])
        for f, v in key:
            setattr(line, f, v)
        line.quantity = sum(r.quantity for r in requests)
        line.purchase = purchase
        line.on_change_product()
        return line

    @classmethod
    @ModelView.button_action('public_pac.act_purchase_consolidation_add')
    def add_purchase_consolidation(cls, consolidations):
        pass


    def get_rec_name(self, name):
        return (f"{self.department.name} - {self.date}")

    def _email(self, from_, to, cc, bcc, languages, template):
        msg, title = get_email(template, self, languages)
        msg['From'] = from_
        msg['To'] = ', '.join(to)
        msg['Cc'] = ', '.join(cc)
        msg['Bcc'] = ', '.join(bcc)
        msg['Subject'] = Header(title, 'utf-8')
        msg['Auto-Submitted'] = 'auto-generated'
        return msg

    @classmethod
    def send_emails(cls, supplier, purchase):
        pool = Pool()
        datamanager = SMTPDataManager()
        Configuration = pool.get('ir.configuration')
        Lang = pool.get('ir.lang')
        Template = pool.get('ir.action.report')
        with Transaction().set_context(active_supplier=supplier):
            from_ = config.get('email', 'from')
            if len(supplier.contact_mechanisms)==0:
                cls.raise_user_error('no_email',{'supplier':supplier.name})

            for contact in supplier.contact_mechanisms:
                if contact.email:
                    to = []
                    name = str(Header(supplier.name))
                    to.append(formataddr((name, contact.email)))
                    cc = []
                    bcc = []
                    languages = set()
                    lang, = Lang.search([
                        ('code', '=', Configuration.get_language()),
                    ], limit=1)
                    languages.add(lang)

                    Data = pool.get('ir.model.data')
                    template_id = Data.get_id('public_pac',
                        'siim_quotation_email')
                    template = Template(template_id)

                    msg = purchase._email(from_, to, cc, bcc, languages,
                        template)
                    to_addrs = [e for _, e in getaddresses(to + cc + bcc)]
                    if to_addrs:
                        if not pool.test:
                            sendmail_transactional(
                                    from_,
                                    to_addrs,
                                    msg,
                                    datamanager=datamanager)
                else:
                    cls.raise_user_error('no_email',{'supplier':supplier.name})


class EnterPuchaseHeaderWinningQuoteStart(ModelView):
    'Enter Puchase Header Winnig Quote Start'
    __name__ = 'enter.purchase.header.winning.quote.start'

    quotation = fields.Many2One(
        'purchase.request.quotation', 'Cotizacion ganadora',
        domain=[
            ('id', 'in', Eval('quotations', [-1])),
        ], depends=['quotations'])
    quotations = fields.One2Many(
        'purchase.request.quotation', None, 'Cotizaciones',
        states={
            'invisible': True,
        })

    @classmethod
    def default_quotations(cls):
        context = Transaction().context
        pool = Pool()
        Quotation = pool.get('purchase.request.quotation')
        QuotationLine = pool.get('purchase.request.quotation.line')
        quotation_lines = QuotationLine.search_read([
            ('header', '=', context.get('active_id')),
        ], fields_names=['quotation'])
        quotation_lines_ids = [row['quotation'] for row in quotation_lines]
        quotations = Quotation.search_read([
            ('id', 'in', quotation_lines_ids),
            ('state', 'in', ['received']),
        ], fields_names=['id'])
        return [row['id'] for row in quotations]


class EnterPurchaseHeaderWinningQuote(Wizard):
    'Enter Purchase Header Winning Quote'
    __name__ = 'enter.purchase.header.winning.quote'

    start = StateView(
        'enter.purchase.header.winning.quote.start',
        'public_pac.enter_purchase_header_winning_quote_view_form',
        [
            Button('OK', 'ok', 'tryton-ok', default=True),
            Button('Cancelar', 'end', 'tryton-cancel'),
        ]
    )

    ok = StateTransition()

    def transition_ok(self):
        if self.start.quotation and self.start.quotation.lines:
            pool = Pool()
            quotation_lines_ids = [row.id for row in self.start.quotation.lines]
            PurchaseHeader = pool.get('purchase.header')
            purchase_header = PurchaseHeader(
                Transaction().context.get('active_id'))
            to_save = []
            if purchase_header.lines:
                for line in purchase_header.lines:
                    if line.quotation_lines:
                        for quote in line.quotation_lines:
                            if quote.id in quotation_lines_ids:
                                line.preferred_quotation_line = quote
                                to_save.append(line)
                                break
            if to_save:
                PurchaseRequest = pool.get('purchase.request')
                PurchaseRequest.save(to_save)
        return 'end'


class PurchaseRequestRequirements(ModelSQL, ModelView):
    'Purchase request requirements'
    __name__ = 'purchase.request.requirements'

    _STATES = {
        'readonly': ~Eval('state').in_(['draft'])
    }
    _DEPENDS = ['state']

    purchase_request = fields.Many2One('purchase.request', 'Solicitud',
        states={
            'readonly': True,
            'invisible': True,
        })
    name = fields.Text('Requisito', states=_STATES, depends=_DEPENDS)
    state = fields.Function(
        fields.Char('Estado', states={
            'invisible': True,
        }), 'on_change_with_state')

    @fields.depends('purchase_request')
    def on_change_with_state(self, name=None):
        if self.purchase_request:
            return self.purchase_request.state
        else:
            return "draft"

    @staticmethod
    def default_purchase_request():
        context = Transaction().context
        pool = Pool()
        Request = pool.get('purchase.request')
        request = Request(context.get('active_id'))
        return request.id


class PurchaseRequestQuotationLineRequirements(ModelSQL, ModelView):
    'Purchase Request Quotation Line Requirements'
    __name__ = 'purchase.request.quotation.line.requirements'

    purchase_request_requirements = fields.Many2One(
        'purchase.request.requirements',
        'Requisitos',
        readonly=True,
        required=True
    )
    purchase_request_quotation_line = fields.Many2One(
        'purchase.request.quotation.line',
        'Contizacion',
        readonly=True,
        required=True
    )
    meets_requirements = fields.Boolean('Cumple?')

class PurchaseRequestTax(ModelSQL, ModelView):
    'Purchase Request Taxes'
    __name__ = 'purchase.request.tax'
    _history = True

    request = fields.Many2One('purchase.request', 'Solicitud de Compra',
        ondelete='CASCADE', states={
            'readonly': True,
            'required': True
        })
    tax = fields.Many2One('account.tax', 'Linea de impuestos',
        states={
            'readonly': Eval('_parent_state').in_(['done', 'cancel', 'quoted']),
            'required': True
        }, depends=['state'])
    origin = fields.Selection([
            ('line', 'Linea'),
            ('header', 'Cabecera'),
        ], 'Origen',
        states={
            'readonly': True,
            'required': True,
            'invisible': False
        })

    @classmethod
    def __setup__(cls):
        super(PurchaseRequestTax, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('tax_uniq', Unique(t, t.request, t.tax),
                'El impuesto que intenta agregar ya existe.'),
        ]

    @staticmethod
    def default_origin():
        return 'line'


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'
    _history = True

    _STATES = {
        'readonly': (
            Eval('requisition_number') | Eval('state').in_(['done','purchased'])
        )
    }
    _DEPENDS = ['requisition_number', 'state']

    purchase_header = fields.Many2One(
        'purchase.header', 'Solicitud de Compra', required=True)
    poa = fields.Many2One('public.planning.unit', 'POA', required=True,
        ondelete='CASCADE', readonly=True)
    department = fields.Function(fields.Many2One('company.department',
        'Department'), 'on_change_with_department')
    pac_line = fields.Many2One('purchase.pac.line', 'PAC Line',
        domain=domain_quarter() + [
               ('pac_budget.purchase_pac.state', '=', 'approved'),
            ],
        states={
            'readonly': (
                Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['cancel','quoted','done']))
        }, depends=_DEPENDS)
    budgets = fields.One2Many(
        'purchase.request.budget', 'purchase_request', 'Partidas',
        states={
            'readonly': ~Eval('state').in_(['draft']),
            'required': Eval('state').in_(['request', 'quoted'])
        }, depends=['state', 'taxes'])
    estimated_unit_price = fields.Numeric('Estimated unit price',
        digits=price_digits, states={
            'readonly': (
                Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['done', 'cancel']))|
                (Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['quoted']) &
                ~Bool(Eval('precertification')))
        }, depends=['precertification'])
    price = fields.Numeric('Precio',
        digits=(16, 4), states={
            'readonly': True
        })
    estimated_amount = fields.Numeric('Estimated amount',
        digits=(16, 2), readonly=True)
    requested_quantity = fields.Numeric('Requested quantity',
        digits=(16, 2), readonly=True)
    purchase_request_requirements = fields.One2Many(
        'purchase.request.requirements', 'purchase_request', 'Requisitos',
        states={
            'readonly': ~Eval('state').in_(['draft'])
        }, depends=['state'])
    suggested_procedure = fields.Many2One('purchase.pac.type.contract',
        'Tipo de compra',
        domain=[
            ('used_on_public_purchases','=',True),
            ('purchase_request','=',True)
        ],
        states={
            'readonly': True,
            'required': True
        }, depends=['state','pac_line'])
    consolidation_origin = fields.Many2One(
        'purchase.consolidation.line', 'Consolidation',
        domain=[('process','=','request')],
        states={
            'readonly': True,
            'required': True,
        })
    precertification = fields.Function(fields.Boolean('Precertificacion'),
        'on_change_with_precertification')
    total = fields.Function(
        fields.Numeric('Total', digits=(16, 4)),
        'on_change_with_total'
    )
    requirement_number = fields.Function(
        fields.Char('Solicitud de despacho'),
        'on_change_with_requirement_number'
    )
    taxes = fields.One2Many('purchase.request.tax', 'request',
        'Lineas de impuestos',
        states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        }, depends=['state'])
    iva = fields.Function(
        fields.Boolean('IVA'),
        'on_change_with_iva'
    )
    ice = fields.Function(
        fields.Boolean('ICE'),
        'on_change_with_ice'
    )

    @classmethod
    def __setup__(cls):
        super(PurchaseRequest, cls).__setup__()
        cls._error_messages.update({
            'quantity_to_buy_greater_than_requested_quantity': ('Error in the '
                'purchase request of the product "%(description)s" '
                'corresponding to the requisition number "%(requisition)s". '
                'The quantity to be purchased ("%(quantity)s") must not be '
                'greater than the requested quantity ("%(requested)s").'),
            'error_amount': ('El total del detalle presupuestario es'
                ' diferente al total de la linea de compra.')
        })
        cls.quantity.depends = ['requested_quantity']
        cls.quantity.domain = [('quantity', '<=', Eval('requested_quantity'))]
        cls.quantity.digits = (16, 2)
        cls.quantity.help = "Quantity to buy"
        cls.quantity.states={
            'readonly': (
                Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['done', 'cancel']))|
                (Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['quoted']) &
                ~Bool(Eval('precertification')))
        }
        cls.uom.states['readonly'] = True
        cls.uom.help = "Unit of measure of the product to be purchased"
        cls.computed_quantity.states['invisible'] = True
        cls.computed_uom.states['invisible'] = True
        cls.preferred_quotation_line.help = ("Select the supplier to whom the "
            "purchase will be made.")
        cls.preferred_quotation_line.states = {
            'readonly': (
                Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['done', 'cancel']))|
                (Eval('_parent_purchase_header', {}).\
                get('state', '').in_(['quoted']) &
                ~Bool(Eval('precertification')))
        }
        cls.quotation_lines_active.domain = [
            ('unit_price', '!=', 0)
        ]
        cls.quotation_lines_active.order = [
            ('quotation_state', 'ASC'),
            #('meet_requirements', 'DESC'),
            ('unit_price', 'ASC')
        ]
        cls._buttons.update({
            'specifications': {
                'depends': ['purchase_request_requirements']
            },
            'budgets_open': {
                'depends': ['budgets']
            },
            'taxes_open': {
                'depends': ['taxes']
            }
        })

    @staticmethod
    def default_price():
        return Decimal(0.0)

    @fields.depends('taxes', 'purchase_header', '_parent_purchase_header.taxes')
    def on_change_with_iva(self, name=None):
        res = False
        for lTax in self.taxes:
            if lTax.tax.name == 'IVA 12%':
                res = True
        return res

    @fields.depends('taxes', 'purchase_header', '_parent_purchase_header.taxes')
    def on_change_with_ice(self, name=None):
        res = False
        for lTax in self.taxes:
            if lTax.tax.name == 'ICE 15%':
                res = True
        return res


    @fields.depends('purchase_header')
    def on_change_with_department(self, name=None):
        if self.purchase_header:
            return self.purchase_header.department.id

    @fields.depends('suggested_procedure')
    def on_change_with_precertification(self, name=None):
        if self.suggested_procedure:
            return self.suggested_procedure.precertification
        return False

    def on_change_with_requirement_number(self, name=None):
        numbers = []
        if self.consolidation_origin:
            for detail in self.consolidation_origin.details:
                req_line = detail.requirement_material_line.requirement_line_origin
                if req_line:
                    numbers.append(req_line.requirements.number)
        return ','.join(list(dict.fromkeys(numbers)))

    @fields.depends('pac_line')
    def on_change_pac_line(self):
        if self.pac_line:
            self.suggested_procedure = self.pac_line.suggested_procedure
            self.budgets = []

    @fields.depends(
        'price',
        'quantity',
        'estimated_unit_price',
        'taxes',
        '_parent_purchase_header.taxes',
        'preferred_quotation_line'
    )
    def on_change_with_total(self, name=None):
        taxes_amount = Decimal('0.0000')
        quantity = self.quantity or 0
        for tax in self.taxes:
            taxes_amount += tax.tax.rate * self.price
        withTax = self.price + taxes_amount
        return (Decimal(quantity) * withTax).quantize(Decimal("0.0001"))

    @fields.depends('estimated_unit_price', 'price')
    def on_change_estimated_unit_price(self):
        #TODO: change amount of purchase when price changes
        #self.purchase_header.total = self.price * Decimal(self.quantity)
        self.price = self.estimated_unit_price


    @staticmethod
    def default_supply_date():
        return date.today()

    @classmethod
    @ModelView.button_action('public_pac.wizard_purchase_request_specification')
    def specifications(cls, requests):
        pass

    @classmethod
    @ModelView.button_action('public_pac.wizard_purchase_request_budget1')
    def budgets_open(cls, requests):
        pass

    @classmethod
    @ModelView.button_action('public_pac.wizard_purchase_request_tax')
    def taxes_open(cls, requests):
        pass

    @fields.depends('preferred_quotation_line')
    def on_change_preferred_quotation_line(self):
        if self.preferred_quotation_line:
            self.price = self.preferred_quotation_line.unit_price
        else:
            self.price = Decimal(0.0)

    @classmethod
    def copy(cls, requests, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('preferred_quotation_line', None)
        default.setdefault('quotation_lines_active', None)
        default.setdefault('purchase_request_quotation_line', None)
        default.setdefault('state', 'draft')
        return super(PurchaseRequest, cls).copy(requests, default=default)

    @classmethod
    def validate(cls, requests):
        for request in requests:
            if request.requested_quantity:
                if request.quantity > request.requested_quantity:
                    cls.raise_user_error(
                        'quantity_to_buy_greater_than_requested_quantity', {
                            'description': request.description,
                            'requisition': request.requisition_number,
                            'quantity': request.quantity,
                            'requested': request.requested_quantity
                        })

    def get_best_quotation(self, name):
        '''
        Redefinition of the method 'get_best_quotation' defined
        in purchase_request_quotation/purchase.py in order that
        the best quote meets the requirements.
        '''
        if self.preferred_quotation_line:
            return self.preferred_quotation_line
        else:
            for line in self.quotation_lines_active:
                if (line.quotation_state == 'received'):
                    return line
            return None


class EnterPurchaseConsolidationStart(ModelView):
    'Enter Purchase Consolidation'
    __name__ = 'enter.purchase.consolidation.start'

    purchase_consolidations = fields.Many2Many('purchase.consolidation',
        None, None, 'Consolidacion de compra',
        domain=[
            ('state', '=', 'done'),
            ('id', 'not in', Eval('purchase_consolidations_aux', [-1]))
        ], depends=['purchase_consolidations_aux'])
    purchase_consolidations_aux = fields.One2Many('purchase.consolidation',
        None, 'Consolidacion de compra')

    @staticmethod
    def default_purchase_consolidations_aux():
        #TODO: improve method
        pool = Pool()
        ConsolidationLine = pool.get('purchase.consolidation.line')
        Request = pool.get('purchase.request')

        requests = Request.search_read([(
                'purchase_header.state', '!=', 'cancel'
            )], fields_names=['consolidation_origin'])
        consolidation_line_ids = list(
            [x['consolidation_origin'] for x in requests])
        contract_consolidation = ConsolidationLine.search_read([
            ('process', '=', 'contract'),
            ('purchase_consolidation.state','=','done')],
            fields_names=['id'])
        ids_contract = [x['id'] for x in contract_consolidation]
        consolidation_line_ids = consolidation_line_ids + ids_contract
        result = [
            {
                "header": ConsolidationLine(x).purchase_consolidation.id,
                "line": x,
                "tot": len(ConsolidationLine(x).purchase_consolidation.lines),
            }
            for x in consolidation_line_ids]

        consolidations = {}
        for res in result:
            if res['header'] in consolidations:
                consolidations[res['header']] = {
                    "used": consolidations[res['header']]['used'] + 1,
                    "tot": res['tot']
                }
            else:
                consolidations[res['header']] = {
                    "used": 1,
                    "tot": res['tot']
                }

        return [x[0] if x[1]['used'] >= x[1]['tot']
            else -1 for x in consolidations.items()]


class EnterPurchaseConsolidation(Wizard):
    'Enter Purchase Consolidation '
    __name__ = 'enter.purchase.consolidation'

    start = StateView(
        'enter.purchase.consolidation.start',
        'public_pac.enter_purchase_consolidation_view_form',
        [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'ok', 'tryton-ok', default=True),
        ])
    ok = StateTransition()

    def transition_ok(self):
        pool = Pool()
        active_id = Transaction().context.get('active_id')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseLine = pool.get('purchase.consolidation.line')
        StockLocation = pool.get('stock.location')
        PurchaseHeader = pool.get('purchase.header')
        DetailRequirement = pool.get('purchase.requirement.line.detail')
        RequirementRequest = pool.get('purchase.request.requirements')
        BudgetRequirement = pool.get('purchase.requirement.budget')
        BudgetRequest = pool.get('purchase.request.budget')
        # TODO: change this static stock location
        warehouse = StockLocation(4)
        header = PurchaseHeader(active_id)
        requests = PurchaseRequest.search_read([(
            'purchase_header.state', '!=', 'cancel'
        )], fields_names=['consolidation_origin'])
        used_consolidations = list(
            [x['consolidation_origin'] for x in requests])

        cl_line = PurchaseLine.search([
            ('process', '=', 'request'),
            ('purchase_consolidation',
                'in',[x.id for x in  self.start.purchase_consolidations]),
            ('id', 'not in', used_consolidations)
        ])

        to_save = []
        to_save_details = []
        for record in cl_line:
            new_purchase_request = PurchaseRequest(
                purchase_header=header,
                product=record.product,
                poa=header.poa,
                uom=record.uom,
                requested_quantity=record.purchase_quantity,
                quantity=record.purchase_quantity,
                estimated_amount=record.purchase_quantity,
                warehouse=warehouse,
                description=record.description,
                company=header.company,
                consolidation_origin=record,
                suggested_procedure=header.suggested_procedure,
            )
            price = 0.0
            pac_line = None
            to_save_budgets = []
            for detail in record.details:
                id_line_requirement = (
                    detail.requirement_material_line.requirement_line_origin
                )
                price = (id_line_requirement.price_tag
                    if id_line_requirement.price_tag>price else price)
                detail_requirement = DetailRequirement.search([
                    ('line','=',id_line_requirement)
                ])
                for detail in detail_requirement:
                    requirement_request = RequirementRequest(
                        purchase_request=new_purchase_request,
                        name=detail.detail
                    )
                    to_save_details.append(requirement_request)
                new_purchase_request.purchase_request_requirements = (
                    to_save_details
                )
                pac_line = id_line_requirement.pac_line
                budget_requirement = BudgetRequirement.search([
                    ('requirement','=', id_line_requirement)
                ])
                for budget in budget_requirement:
                    budget_request = BudgetRequest(
                        purchase_request=new_purchase_request,
                        budget=budget.budget,
                        amount=budget.amount,
                        total_budguet=budget.total_budguet,
                        poa=budget.poa,
                        quantity=budget.quantity
                    )
                    to_save_budgets.append(budget_request)
                new_purchase_request.budgets = to_save_budgets
                new_purchase_request.pac_line = pac_line

            new_purchase_request.price = price
            new_purchase_request.pac_lie = pac_line
            to_save.append(new_purchase_request)
        PurchaseRequest.save(to_save)
        BudgetRequest.save(to_save_budgets)
        RequirementRequest.save(to_save_details)
        return 'end'

class EnterRequestSpecification(ModelView):
    'Enter request specification'
    __name__ = 'purchase.request.specification.enter.start'

    request = fields.Many2One('purchase.request', 'Solicitud',
        states={
            'readonly': True,
            'invisible': True
        })
    specifications = fields.One2Many(
        'purchase.request.requirements', None, 'Especificaciones',
        states={
            'readonly': Eval('state') != 'draft',
        },
    )
    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    @staticmethod
    def default_specifications():
        context = Transaction().context
        pool = Pool()
        Specification = pool.get('purchase.request.requirements')
        specifications = Specification.search([
            ('purchase_request', '=', context.get('active_id')),
        ])
        return [row.id for row in specifications]

    @staticmethod
    def default_request():
        context = Transaction().context
        pool = Pool()
        Request = pool.get('purchase.request')
        request = Request(context.get('active_id'))
        return request.id

    @fields.depends('request')
    def on_change_with_state(self, name=None):
        if self.request:
            return self.request.state
        return 'draft'


class EnterRequestSpecificationWizard(Wizard):
    'Enter Request Specification Wizard'
    __name__ = 'purchase.request.specification.enter'

    start = StateView(
        'purchase.request.specification.enter.start',
        'public_pac.purchase_request_specification_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        specifications_ids = []
        for row in self.start.specifications:
            to_save.append(row)
            specifications_ids.append(row.id)
        pool = Pool()
        Specification = pool.get('purchase.request.requirements')
        specifications = Specification.search([
            ('id', 'not in', specifications_ids),
            ('purchase_request', '=', self.start.request.id)
        ])
        if specifications:
            Specification.delete(specifications)
        if to_save:
            Specification.save(to_save)
        return 'end'


class EnterQuotationSpecification(ModelView):
    'Enter request specification'
    __name__ = 'purchase.request.quotation.specification.enter.start'

    quotation_line = fields.Many2One('purchase.request.quotation.line',
        'Cotizacion',
        states={
            'readonly': True,
            'invisible': True
        })
    specifications = fields.One2Many(
        'purchase.request.quotation.line.requirements', None,
        'Especificaciones',
        states={
            'readonly': Eval('state') != 'draft',
        },
    )
    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    @staticmethod
    def default_specifications():
        context = Transaction().context
        pool = Pool()
        Specification = pool.get('purchase.request.quotation.line.requirements')
        specifications = Specification.search([
            ('purchase_request_quotation_line', '=', context.get('active_id')),
        ])
        return [row.id for row in specifications]

    @staticmethod
    def default_quotation_line():
        context = Transaction().context
        pool = Pool()
        Request = pool.get('purchase.request.quotation.line')
        request = Request(context.get('active_id'))
        return request.id

    @fields.depends('request')
    def on_change_with_state(self, name=None):
        if self.request:
            return self.request.state
        return ''


class EnterQuotationSpecificationWizard(Wizard):
    'Enter Request Specification Wizard'
    __name__ = 'purchase.request.quotation.specification.enter'

    start = StateView(
        'purchase.request.quotation.specification.enter.start',
        'public_pac.purchase_request_quotation_specification_enter_start_view_form', [  # noqa
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        to_save = []
        specifications_ids = []
        for row in self.start.specifications:
            to_save.append(row)
            specifications_ids.append(row.id)
        pool = Pool()
        Specification = pool.get('purchase.request.quotation.line.requirements')
        specifications = Specification.search([
            ('id', 'not in', specifications_ids),
            ('purchase_request_quotation_line', '=', self.start.quotation_line.id)  # noqa
        ])
        if specifications:
            Specification.delete(specifications)
        if to_save:
            Specification.save(to_save)
        return 'end'


class EnterRequestBudget(ModelView):
    'Enter request budget'
    __name__ = 'purchase.request.budget.enter.start'

    request = fields.Many2One('purchase.request', 'Solicitud',
        states={
            'readonly': True,
            'invisible': True
        })
    price = fields.Function(
        fields.Numeric('Valor de linea', digits=(6, 4)),
        'on_change_with_price'
    )
    remaining_value = fields.Function(
        fields.Numeric('Valor por Asignar', digits=(6, 4)),
        'on_change_with_remaining_value'
    )
    remaining_quantity = fields.Function(
        fields.Numeric('remaining_quantity', digits=(6, 4)),
        'on_change_with_remaining_quantity'
    )
    total = fields.Function(
        fields.Numeric('Subtotal', digits=(6, 4)),
        'on_change_with_total'
    )
    total_general = fields.Function(
        fields.Numeric('Total', digits=(6, 2)),
        'on_change_with_total_general'
    )
    budgets = fields.One2Many(
        'purchase.request.budget', None, 'Partidas',
        states={
            'readonly': Eval('statb oe') != 'draft',
        },
        domain=[
            If((~(Bool(Eval('request.pac_line')))),
               [('type', 'in', ['expense']),
                ('activity', 'child_of', Eval('poa', -1), 'parent')],
               [('id', 'in', Eval('list_budgets'))])
        ],
        context={
            'remaining_value':Eval('remaining_value', 0),
            'remaining_quantity':Eval('remaining_quantity', 0),
        }, depends=[
            'remaining_value',
            'remaining_quantity',
            'request',
            'list_budgets'
        ]
    )
    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )
    list_budgets = fields.Function(fields.One2Many(
        'public.budget', None, 'Lista partidas'),
        'on_change_with_list_budgets')

    @staticmethod
    def default_budgets():
        context = Transaction().context
        pool = Pool()
        BudgetLine = pool.get('purchase.request.budget')
        budgets = BudgetLine.search([
            ('purchase_request', '=', context.get('active_id')),
        ])
        return [row.id for row in budgets]

    @staticmethod
    def default_request():
        context = Transaction().context
        pool = Pool()
        Request = pool.get('purchase.request')
        request = Request(context.get('active_id'))
        return request.id

    @fields.depends('process', 'pac_line')
    def on_change_with_list_budgets(self, name=None):
        allow_budgets = []
        if not self.process or self.process == 'contract':
            pass
        elif self.process and self.process == 'request' and self.pac_line and (
                self.pac_line.budgets):
            allow_budgets = [b.budget.id
                    for b in self.pac_line.budgets
                    if b.certified_pending > Decimal("0")]

        return allow_budgets

    @fields.depends('requirement', 'budgets')
    def on_change_with_total(self, name=None):
        total = Decimal(0.0)
        for budget in self.budgets:
            total += budget.total
        return total

    @fields.depends('requirement', 'budgets')
    def on_change_with_total_general(self, name=None):
        total = Decimal(0.0)
        for budget in self.budgets:
            total += budget.total
        return total.quantize(Decimal('0.01'))

    @fields.depends('request', 'budgets')
    def on_change_with_price(self, name=None):
        return self.request.total

    @fields.depends('request', 'budgets', '_parent_request.total')
    def on_change_with_remaining_value(self, name=None):
        quantity = self.request.quantity
        price = self.request.price
        tot = Decimal(quantity)*price
        amount_budget = Decimal('0.0')
        for budget in self.budgets:
            if budget.amount:
                amount_budget += budget.amount
        return tot - amount_budget

    @fields.depends('request', 'budgets')
    def on_change_with_remaining_quantity(self, name=None):
        quantity = Decimal('0.00')
        for budget in self.budgets:
            if budget.quantity:
                quantity += budget.quantity
        return Decimal(self.request.quantity) - quantity

    @fields.depends('request')
    def on_change_with_state(self, name=None):
        if self.request:
            return self.request.state
        return ''


class EnterRequestBudgetWizard(Wizard):
    'Enter Request Budget Wizard'
    __name__ = 'purchase.request.budget.enter'

    start = StateView(
        'purchase.request.budget.enter.start',
        'public_pac.purchase_request_budget_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        pool = Pool()
        BudgetLine = pool.get('purchase.request.budget')
        to_save = []
        purchase_tot = ZERO

        for row in self.start.budgets:
            pool = Pool()
            budget_line = BudgetLine(
                purchase_request=self.start.request,
                budget=row.budget,
                amount=row.amount,
                total_budguet=row.total_budguet,
                quantity=row.quantity
            )
            budget_line.amount_tax = budget_line.on_change_with_amount_tax()
            budget_line.total = budget_line.on_change_with_total()

            to_save.append(budget_line)
            purchase_tot += row.amount

        if self.start.total != self.start.request.total:
            self.start.request.raise_user_error('error_amount')

        to_delete = BudgetLine.search([(
            ('purchase_request', '=', self.start.request.id)
        )])
        BudgetLine.delete(to_delete)
        BudgetLine.save(to_save)

        return 'end'

class PurchaseRequirementMaterialPending(ModelView, ModelSQL):
    'Purchase Requirement Material Pending'
    __name__ = 'purchase.material.requirement.pendings'

    requirement_material = fields.Integer('Requerimiento de bodega')
    date = fields.Date('Fecha')
    number = fields.Function(
        fields.Char('Numero'), 'get_requirement_material'
    )
    product_names = fields.Function(
        fields.Char('Productos'), 'get_requirement_material'
    )
    employee = fields.Function(
        fields.Many2One('company.employee', 'Requiriente'),
        'get_requirement_material'
    )
    department = fields.Function(
        fields.Many2One('company.department', 'Departamento'),
        'get_requirement_material'
    )
    number_request = fields.Function(
        fields.Char('N. Solicitud despacho'),
        'get_requirement_material',
    )

    @classmethod
    def __setup__(cls):
        super(PurchaseRequirementMaterialPending, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
        ]

    @classmethod
    def get_requirement_material(cls, requirements, names=None):
        dict_number = defaultdict(lambda : None)
        dict_products = defaultdict(lambda : None)
        dict_employee = defaultdict(lambda : None)
        dict_department = defaultdict(lambda : None)
        dict_number_request = defaultdict(lambda : None)
        MaterialRequirement = Pool().get('purchase.material.requirement')
        for row in requirements:
            material_requirement = MaterialRequirement(row.requirement_material)
            dict_number[row.id] = material_requirement.number
            dict_products[row.id] = material_requirement.product_names
            dict_employee[row.id] = material_requirement.employee.id
            dict_department[row.id] = material_requirement.department.id
            dict_number_request[row.id] = material_requirement.number_request

        return {
            'number': dict_number,
            'product_names': dict_products,
            'employee': dict_employee,
            'department': dict_department,
            'number_request': dict_number_request,
        }

    @classmethod
    def table_query(cls):
        available_products = get_material_requirement_available()
        to_list = []
        for row in available_products:
            if row.purchase_quantity > Decimal("0"):
                to_list.append(row.requirement.id)
        MaterialRequirement = Pool().get('purchase.material.requirement')
        material_requirement = MaterialRequirement.__table__()
        where = material_requirement.id == -1
        if to_list:
            where = material_requirement.id.in_(to_list)
        query = material_requirement.select(
            RowNumber(window=Window([])).as_('id'),
            material_requirement.id.as_('requirement_material'),
            material_requirement.date,
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            where = where,
        )
        return query


class AccountPaymentContractProcess(metaclass=PoolMeta):
    __name__ = 'treasury.account.payment.request.contract.process'

    contract = fields.Many2One('purchase.contract.process', 'Contrato')

class EnterRequestTax(ModelView):
    'Enter request tax'
    __name__ = 'purchase.request.tax.enter.start'

    request = fields.Many2One('purchase.request', 'Solicitud',
        states={
            'readonly': True,
            'invisible': True
        })
    taxes = fields.One2Many(
        'purchase.request.tax', None, 'Impuestos',
        states={
            'readonly': Eval('state').in_(['done', 'cancel', 'quoted'])
        },
        depends=['state']
    )
    state = fields.Function(
        fields.Char('Estado', states={'invisible': True}),
        'on_change_with_state'
    )

    def default_taxes():
        context = Transaction().context
        pool = Pool()
        TaxLine = pool.get('purchase.request.tax')
        taxes = TaxLine.search([
            ('request', '=', context.get('active_id')),
        ])
        return [row.id for row in taxes]

    @staticmethod
    def default_request():
        context = Transaction().context
        pool = Pool()
        Request = pool.get('purchase.request')
        request = Request(context.get('active_id'))
        return request.id

    @fields.depends('request')
    def on_change_with_state(self, name=None):
        if self.request:
            return self.request.purchase_header.state
        return ''


class EnterRequestTaxWizard(Wizard):
    'Enter Request Tax Wizard'
    __name__ = 'purchase.request.tax.enter'

    start = StateView(
        'purchase.request.tax.enter.start',
        'public_pac.purchase_request_tax_enter_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Grabar', 'enter', 'tryton-ok', default=True)
        ]
    )

    enter = StateTransition()

    def transition_enter(self):
        pool = Pool()
        TaxtLine = pool.get('purchase.request.tax')
        Request = pool.get('purchase.request')
        to_save = []

        for row in self.start.taxes:
            pool = Pool()
            tax_line = TaxtLine(
                request=self.start.request,
                tax=row.tax,
                origin=row.origin
            )
            to_save.append(tax_line)

        to_delete = TaxtLine.search([(
            ('request', '=', self.start.request.id)
        )])
        TaxtLine.delete(to_delete)
        TaxtLine.save(to_save)
        request = Request(self.start.request.id)
        #self.start.request.on_change_with_total()
        request.total = request.on_change_with_total()
        Request.save([request])
        return 'end'
