from decimal import Decimal
from numbertoletters import number_to_letters
from collections import defaultdict, OrderedDict, Counter

from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.transaction import Transaction
from trytond.pool import Pool


__all__ = [
    'Requirements', 'Purchase_Pac_Line', 'PurchasePacBudget',
    'PurchaseConsolidation', 'Purchase', 'PurchaseHeader',
    'MaterialRequirements', 'ContractProcessTDR', 'ContractProcess',
    'QuotationEmail', 'ContractProcessCertificateCompletion',
    'PurchaseTracking', 'PurchaseTrackingMoves', 'PurchasePac',
    'PurchaseLinkProject', 'PurchasePacComparing'
]


dict_months = {
    1: 'Enero',
    2: 'Febrero',
    3: 'Marzo',
    4: 'Abril',
    5: 'Mayo',
    6: 'Junio',
    7: 'Julio',
    8: 'Agosto',
    9: 'Septiembre',
    10: 'Octubre',
    11: 'Noviembre',
    12: 'Diciembre',
}


class Purchase_Pac_Line(CompanyReportSignature):
    __name__ = 'purchase.pac.line'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(Purchase_Pac_Line, cls).get_context(
            records, data)
        PurchasePacLine = Pool().get('purchase.pac.line')
        id = data['ids']
        purchase_pac = PurchasePacLine.search([('id', 'in', id)])
        if purchase_pac:
            for row in purchase_pac:
                amount = Decimal('0.0')
                for row1 in row.budgets:
                    if row1.amount:
                        amount += row1.amount
        report_context['amount'] = amount
        return report_context

class Requirements(CompanyReportSignature):
    __name__ = 'purchase.requirement'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(Requirements, cls).get_context(
            records, data)
        PurchaseBudget = Pool().get('purchase.requirement.budget')
        purchase_budgets = PurchaseBudget.search([
            ('requirement.requirements', 'in', data['ids']),
        ])
        amount = Decimal('0.0')
        summary_budget = defaultdict(lambda : Decimal("0"))
        info = []
        if purchase_budgets:
            for row in purchase_budgets:
                amount += row.amount
                info.append({
                    'id': row.requirement.requirements.id,
                    'product': (row.requirement.product.template.name
                                if row.requirement.product
                                else row.requirement.description),
                    'budget': row.budget.rec_name,
                    'total_budguet': f'{row.total_budguet.quantize(Decimal("0.00")):,}',
                    'amount': f'{row.amount.quantize(Decimal("0.00")):,}',
                })
            ordered = PurchaseBudget.search([
                ('requirement.requirements', 'in', data['ids']),
            ], order=[('budget.code', 'ASC')])
            for row in ordered:
                summary_budget[row.budget.code] += row.amount.quantize(
                    Decimal("0.00"))
        report_context['amount'] = f'{amount.quantize(Decimal("0.00")):,}'
        report_context['budgets'] = info
        report_context['summary_budget'] = {
            a: f'{summary_budget.get(a):,}' for a in summary_budget}
        return report_context


class MaterialRequirements(CompanyReportSignature):
    __name__ = 'purchase.material.requirement'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MaterialRequirements, cls).get_context(
            records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user_name'] = user.employee.party.name
        report_context['user_id'] = user.employee.id
        return report_context


class Tracking(CompanyReportSignature):
    @classmethod
    def get_context(cls, records, data):
        report_context = super(Tracking, cls).get_context(records, data)
        report_context['list_rec'] = cls._get_tracking(data['ids'])
        return report_context

    @classmethod
    def _get_tracking(cls, ids_purchase):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        Requirement = pool.get('purchase.requirement')
        purchases = Purchase.search([
            ('id', 'in', ids_purchase)
        ])
        data = []
        for row in purchases:
            request_number = ''
            requirements = []
            try:
                if row.purchase_request != '':
                    request_number = (
                        row.lines[0].requests[0].consolidation_origin.request)
                elif row.contract_process and row.contract_process.consolidation_line:
                    request_number = (
                        row.contract_process.consolidation_line[0].consolidation_line.request)
                requirements = Requirement.search([
                    ('number', '=', request_number)
                ])
            except Exception:
                pass
            if requirements:
                move_effective_date = (
                    str(row.lines[0].moves[0].effective_date)
                    if row.lines[0].moves and row.lines[0].moves[0].effective_date else '')
                purchase_create_date = row.create_date if row.state not in ['draft', 'cancel'] else ''
                data.append({
                    'requirement_number': requirements[0].number,
                    'requirement_request_date': str(
                        requirements[0].request_date),
                    'requirement_done_date': str(requirements[0].done_date),
                    'purchase_number': row.number,
                    'purchase_create_date': purchase_create_date,
                    'purchase_done_date': (
                        '' if row.done_date is None else str(row.done_date)),
                    'move_effective_date': move_effective_date
                })
        return data


class PurchaseConsolidation(CompanyReportSignature):
    __name__ = 'purchase.consolidation'

class PurchaseTracking(Tracking):
    __name__ = 'purchase.tracking'

class PurchaseTrackingMoves(Tracking):
    __name__ = 'purchase.tracking.moves'

class PurchaseLinkProject(CompanyReportSignature):
    __name__ = 'purchase.link.project'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        report_context = super(PurchaseLinkProject, cls).get_context(
                records, data)
        PurchaseModel = pool.get('purchase.purchase')
        #kwhere = { 'purchase': { 'ids': data['ids'] } }
        #where = { 'program': { 'ids': [1067], 'budgets': [7106, 5767, 5755, 5758, 5768, 5772, 5433, 5435, 5436, 5415] } }
        bgtProjects = PurchaseModel.get_integration_budgets(data['ids'])
        #col = PurchaseModel.get_program_by_budgets(where)

        dict_records = {}
        for project in bgtProjects:
            dict_line = {}
            dict_line['purchase_code'] = project[0]
            dict_records[project[0]] = dict_line
        report_context['records'] = dict_records
        return report_context

class Purchase(CompanyReportSignature):
    __name__ = 'purchase.purchase'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        report_context = super(Purchase, cls).get_context(
            records, data)
        PurchaseModel = Pool().get('purchase.purchase')
        Config = pool.get('purchase.configuration')
        config = Config(1)

        id = data['ids']
        subtotal_goods = Decimal('0.0')
        subtotal_services = Decimal('0.0')
        total_goods = Decimal('0.0')
        total_services = Decimal('0.0')
        tax_services= Decimal('0.0')
        tax_goods = Decimal('0.0')
        letters = ('')
        purchases = PurchaseModel.search([('id', 'in', id)])
        lines_g = []
        lines_s = []
        request_by = []
        signatures = {}
        contacts = {"email":"", "phone":""}
        service = False
        if purchases:
            for purchase in purchases:
                for contact in purchase.party.contact_mechanisms:
                    if contact.email and contact.type=='email':
                        contacts['email'] = contact.email
                    if contact.type=='mobile' or contact.type=='phone':
                        contacts['phone'] = contact.value
                signatures['create_by'] = cls.get_signature(
                    purchase.create_uid.employee)
                signatures['request_by'] = cls.get_signature(
                    purchase.request_by)
                signatures['done_by'] = {
                    'name': (purchase.done_by.party.name
                        if purchase.done_by else ''),
                    'department': (purchase.done_by_deparment.name
                        if purchase.done_by_deparment else '')
                }
                for row in purchase.lines:
                    row.unit_price = row.unit_price.quantize(Decimal('0.0001'))
                    tax_amount = cls.get_taxes(row)
                    amount_with_tax = row.amount + tax_amount
                    if row.product.type == 'service':
                        service = True
                        subtotal_services += row.amount
                        tax_services += tax_amount
                        total_services += amount_with_tax.quantize(Decimal('0.0001'))
                        lines_s.append(row)
                        request_by = cls.get_employees_request(row.requests)
                    else:
                        subtotal_goods += row.amount
                        tax_goods += tax_amount
                        total_goods += amount_with_tax.quantize(Decimal('0.0001'))
                        lines_g.append(row)

        total = (total_goods + total_services).quantize(Decimal('0.01'))            
        letters = number_to_letters(total)
        report_context['total_goods'] = total_goods.quantize(Decimal('0.01'))
        report_context['total_services'] = total_services.quantize(Decimal('0.01'))
        report_context['tax_s'] = tax_services.quantize(Decimal('0.01'))
        report_context['tax_g'] = tax_goods.quantize(Decimal('0.01'))
        report_context['lines_g'] = lines_g
        report_context['lines_s'] = lines_s
        report_context['letters'] = letters
        report_context['amount_total_goods'] = subtotal_goods.quantize(Decimal('0.01'))
        report_context['amount_total_services'] = subtotal_services.quantize(Decimal('0.01'))
        report_context['amount_goods'] = subtotal_goods
        report_context['amount_services'] = subtotal_services
        report_context['amount_total'] = total 
        report_context['owner'] =  (
            request_by[0] if  request_by
            else config.employee_goods
        )
        report_context['signa'] = signatures
        report_context['contacts'] = contacts
        report_context['service'] = service
        return report_context

    @classmethod
    def get_taxes(cls, row):
        taxes = Decimal('0.0')
        if row.taxes:
            for t in row.taxes:
                taxes += t.rate * row.amount 
        return taxes

    @classmethod
    def get_employees_request(cls, requests):
        employees = []
        for request in requests:
            details = request.consolidation_origin.details
            for detail in details:
                requirement_line = (
                    detail.requirement_material_line.requirement_line_origin)
                requirement = requirement_line.requirements
                employees.append(requirement.request_by)
        return list(dict.fromkeys(employees))

    @classmethod
    def get_signature(cls, employee):
        Contract = Pool().get('company.contract')
        if employee:
            if employee.contract:
                return {
                    'name': employee.party.name,
                    'department': employee.contract.department.name
                }
            else:
                contract = Contract.search(
                    [('employee', '=', employee.id)]
                    ,order=[('start_date', 'ASC')])[0]
                return {
                    'name': employee.party.name,
                    'department': contract.department.name
                }
        else:
            return {
                    'name': '',
                    'department': ''
                }

class PurchaseHeader(CompanyReportSignature):
    __name__ = 'purchase.header'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PurchaseHeader, cls).get_context(
            records, data)
        PurchaseHeaderModel = Pool().get('purchase.header')

        id = data['ids']
        purchases = PurchaseHeaderModel.search([('id', 'in', id)])
        lines_g = []
        lines_s = []
        lines_a= []
        tax_g=Decimal('0.0')
        tax_a=Decimal('0.0')
        tax_s=Decimal('0.0')

        if purchases:
            for purchase in purchases:
                for row in purchase.lines:
                    if row.product.type == 'goods':
                        tax_g = (row.total - (Decimal(row.quantity) * row.price)).quantize(Decimal('0.0001'))
                        lines_g.append(row)
                    elif row.product.type == 'service':
                        tax_s = (row.total - (Decimal(row.quantity) * row.price)).quantize(Decimal('0.0001'))
                        lines_s.append(row)
                    else:
                        tax_a = (row.total - (Decimal(row.quantity) * row.price)).quantize(Decimal('0.0001'))
                        lines_a.append(row)
        report_context['tax_s'] = tax_s
        report_context['tax_a'] = tax_a
        report_context['tax_g'] = tax_g
        report_context['lines_g'] = lines_g
        report_context['lines_s'] = lines_s
        report_context['lines_a'] = lines_a
        return report_context

class PurchasePac(CompanyReportSignature):
    __name__ = 'purchase.pac'

class PurchasePacBudget(CompanyReportSignature):
    __name__ = 'purchase.pac.budget'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        report_context = super(PurchasePacBudget, cls).get_context(
            records, data)
        PurchasePacBudgetModel = Pool().get('purchase.pac')
        ids = data['ids']
        pac = PurchasePacBudgetModel.search([('id', 'in', ids)])[0]
        dict_records = {}
        for pac_budget in pac.pac_budgets:
            for line in pac_budget.pac_lines:
                dict_pac = {}
                dict_pac['description'] = line.description
                dict_pac['budget_code'] = pac_budget.budget_code
                dict_pac['year'] = pac_budget.purchase_pac.fiscal_year.rec_name
                dict_pac['cpc'] = line.cpc.code
                dict_pac['bid_founds'] = 'SI' if line.bid_founds else ''
                dict_pac['project_code'] = line.project_code
                dict_pac['operation_code'] = line.operation_code
                dict_pac['purchase_type'] = line.purchase_type_translated
                dict_pac['unit'] = line.unit.rec_name
                dict_pac['unit_price'] = line.unit_price_without_iva
                dict_pac['suggested_procedure'] = (
                    line.suggested_procedure.rec_name
                    if line.suggested_procedure else '')
                dict_pac['electronic_catalog'] = ('SI'
                    if line.electronic_catalog else '')
                dict_pac['quantity'] = line.quantity
                dict_pac['quarter1'] = 'S' if line.quarter1 else ''
                dict_pac['quarter2'] = 'S' if line.quarter2 else ''
                dict_pac['quarter3'] = 'S' if line.quarter3 else ''
                dict_pac['regime_type'] = line.regime_type_translated
                dict_pac['budget_type'] = line.budget_type_translated
                dict_pac['product_type'] = line.product_type_translated
                dict_records[line.id] = dict_pac

        report_context['records'] = dict_records
        return report_context

class PurchasePacComparing(CompanyReportSignature):
    __name__ = 'purchase.pac.comparing'

    @classmethod
    def get_original_line(cls, line):
        old_line = line.line_that_was_reformed
        if old_line:
            data = cls.get_original_line(old_line) 
            return {'reform': data['reform'], 'line': data['line']}
        else:
            return {'reform': line.reform_state, 'line': line}

    @classmethod
    def template_report(cls, line, pac_budget, reform, state):
        dict_pac = {}
        dict_pac['description'] = line.description
        dict_pac['budget_code'] = pac_budget.budget_code
        dict_pac['year'] = pac_budget.purchase_pac.fiscal_year.rec_name
        dict_pac['cpc'] = line.cpc.code
        dict_pac['bid_founds'] = 'SI' if line.bid_founds else ''
        dict_pac['project_code'] = line.project_code
        dict_pac['operation_code'] = line.operation_code
        dict_pac['purchase_type'] = line.purchase_type_translated
        dict_pac['unit'] = line.unit.rec_name
        dict_pac['unit_price'] = line.unit_price_without_iva
        dict_pac['suggested_procedure'] = (
            line.suggested_procedure.rec_name
            if line.suggested_procedure else '')
        dict_pac['electronic_catalog'] = ('SI'
            if line.electronic_catalog else '')
        dict_pac['quantity'] = line.quantity
        dict_pac['quarter1'] = 'S' if line.quarter1 else ''
        dict_pac['quarter2'] = 'S' if line.quarter2 else ''
        dict_pac['quarter3'] = 'S' if line.quarter3 else ''
        dict_pac['regime_type'] = line.regime_type_translated
        dict_pac['budget_type'] = line.budget_type_translated
        dict_pac['product_type'] = line.product_type_translated
        dict_pac['reform'] = reform
        dict_pac['state'] = state
        return dict_pac

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PurchasePacComparing, cls).get_context(
            records, data)
        PurchasePacBudgetModel = Pool().get('purchase.pac')
        ids = data['ids']
        pac = PurchasePacBudgetModel(ids[0])
        dict_original_records = {}
        dict_current_records = {}
        for pac_budget in pac.pac_budgets:
            for aLine in pac_budget.pac_lines:
                origin = cls.get_original_line(aLine)
                state_reform = ''
                state_executed = ''
                if origin['reform'] == 'added':
                    state_reform = 'Agregado'
                if origin['reform'] == 'modified':
                    state_reform = 'Modificado'
                if aLine.total_acquired > 0:
                    state_executed = 'Ejecutado'
                dict_currect_pac = cls.template_report(aLine, pac_budget, state_reform, state_executed)
                dict_current_records[aLine.id] = dict_currect_pac
                line = origin.get('line', None)
                if line and origin['reform'] != 'added':
                    state_executed_line = ''
                    if line.total_acquired > 0:
                        state_executed_line = 'Ejecutado'
                    dict_pac = cls.template_report(line, pac_budget, '',state_executed_line)
                    dict_original_records[line.id] = dict_pac
                    
            for aLine in pac_budget.reformed_lines:
                if aLine.reform_state == 'deleted':
                    if aLine.state != 'added':
                        dict_pac = cls.template_report(aLine, pac_budget, '','')
                        dict_original_records[aLine.id] = dict_pac
                        dict_current_pac_re = cls.template_report(aLine, pac_budget, 'Eliminado','')
                        dict_current_records[aLine.id] = dict_current_pac_re

        report_context['original_records'] = dict_original_records
        report_context['current_records'] = dict_current_records
        return report_context

class ContractProcessTDR(CompanyReportSignature):
    __name__ = 'purchase.contract.process.tdr'


class ContractProcess(CompanyReportSignature):
    __name__ = 'purchase.contract.process'

class QuotationEmail(CompanyReportSignature):
    __name__ = 'purchase.quotation.email'


class ContractProcessCertificateCompletion(CompanyReportSignature):
    __name__ = 'purchase.contract.process.certificate.completition'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            ContractProcessCertificateCompletion, cls).get_context(
            records, data)
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        Contract = pool.get('purchase.contract.process')
        contract = Contract(data['id'])
        report_context['day'] = (
            contract.start_date.day if contract.start_date else 0)
        report_context['month'] = (
            dict_months.get(contract.start_date.month)
            if contract.start_date else 'XXX')
        report_context['year'] = (
            number_to_letters(contract.start_date.year)
            if contract.start_date else 'XXX')
        if contract.tdr_certificate_multiannual:
            certificates = Certificate.search([
                ('state', '=', 'done'),
                ('pluriannual_certificate', '=',
                 contract.tdr_certificate_multiannual.id),
            ], order=[
                ('period', 'ASC')
            ])
            dict_certificate_lines = defaultdict(lambda : [])
            for certificate in certificates:
                dict_certificate_lines[certificate.period.fiscalyear.name] += (
                    certificate.lines)
            report_context['certificate_pluriannual'] = dict_certificate_lines
        return report_context
