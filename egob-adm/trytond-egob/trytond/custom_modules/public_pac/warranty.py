from decimal import Decimal

from trytond.pyson import Eval, Bool
from trytond.model import ModelView, ModelSQL, fields, Workflow

__all__ = [
    'WorkWarranty', 'WorkWarrantyType'
]


class WorkWarranty(Workflow, ModelSQL, ModelView):  # TODO: this model is on treasury module one of them must to be inherit # noqa
    'Work Warranty'
    __name__ = 'work.warranty'

    _states = {
        'readonly': (Eval('state') != 'draft') | Bool(Eval('renewed_warranty')),
    }
    _depends = ['state', 'renewed_warranty']

    type_ = fields.Many2One(
        'work.warranty.type', 'Tipo', required=True,
        states=_states, depends=_depends)

    def get_rec_name(self, name):
        return f"{self.number} - {self.party.name} - {self.type_.name}"


class WorkWarrantyType(ModelSQL, ModelView):
    'Work Warranty Type'
    __name__ = 'work.warranty.type'

    name = fields.Char('Nombre', required=True)
    generate_warranty = fields.Boolean('Generar garantia fisica')
    relation_to_advances = fields.Boolean('Relacion con anticipos')
    percentage = fields.Numeric(
        'Porcentaje', required=True,
        domain=[
            ('percentage', '>=', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ]
    )

    @classmethod
    def __setup__(cls):
        super(WorkWarrantyType, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]

    @staticmethod
    def default_generate_warranty():
        return False

    @staticmethod
    def default_relation_to_advances():
        return False

    @staticmethod
    def default_duration():
        return False

    @staticmethod
    def default_percentage():
        return Decimal("0")