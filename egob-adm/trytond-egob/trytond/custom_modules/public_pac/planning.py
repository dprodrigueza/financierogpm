from collections import defaultdict

from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['PublicBudgetPlanning']


class PublicBudgetPlanning(metaclass=PoolMeta):
    __name__ = 'public.budget.planning'
    purchase_pac_line = fields.Function(
        fields.Many2Many('purchase.pac.line', None, None, 'PAC Line'),
        'get_purchase_pac_line')

    @classmethod
    def get_purchase_pac_line(cls, lines, name=None):
        result = defaultdict(lambda: [])
        return result
    # TODO: check next lines
    #     pool = Pool()
    #     cursor = Transaction().connection.cursor()
    #     result = defaultdict(lambda: [])
    #     PACLine = pool.get('purchase.pac.line')
    #     pac_line = PACLine.__table__()
    #     table = cls.__table__()
    #     ids = [l.id for l in lines]
    #     for sub_ids in grouped_slice(ids):
    #         red_sql = reduce_ids(table.id, sub_ids)
    #         cursor.execute(*table.join(pac_line,
    #             condition=pac_line.budget_planning==table.id
    #             ).select(table.id, pac_line.id,
    #             where=red_sql))
    #         for line in cursor.fetchall():
    #             result[line[0]].append(line[1])
    #             print(("***:) ",result))
    #     return result
