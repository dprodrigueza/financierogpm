from trytond.pool import Pool, PoolMeta

__all__ = [
    'Invoice'
]



class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def delete(cls, invoices):
        Payment = Pool().get('purchase.contract.process.payment')
        if invoices:
            ids = [f"account.invoice,{i.id}" for i in invoices]
            payments = Payment.search([
                ('origin', 'in', ids),
                ('type', '=', 'payment'),
            ])
            if payments:
                for row in payments:
                    row.origin = None
                Payment.save(payments)
        super(Invoice, cls).delete(invoices)