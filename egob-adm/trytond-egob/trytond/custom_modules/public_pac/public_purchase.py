from datetime import date
from decimal import Decimal
from collections import defaultdict

from sql.aggregate import Sum
from sql import Literal, Null
from sql.functions import CurrentTimestamp
from sql.operators import Concat

from trytond.model import ModelView, ModelSQL, fields, Unique, Workflow
from trytond.pyson import Eval, If, Len, Less, Bool
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids

from trytond.modules.product import price_digits

__all__ = ['PurchasePlanReferenceTerms', 'PurchasePacCategory', 'PurchasePac',
    'FiscalYear', 'PurchasePacPeriod', 'PurchasePacLinePeriod',
    'PurchasePacLine', 'PurchasePacTypeContract', 'PurchasePacLineReform',
    'PurchasePacBudget', 'PurchasePacLineConsulting',
    'PurchasePacLineReformConsulting', 'PurchasePacLineDepartmentCompany',
    'PurchasePacLineReformDepartmentCompany', 'PurchasePacLineBudget', 
    'PurchasePacLineReformBudget']

ZERO = Decimal('0.0')
CENT = Decimal('0.01')


def get_util_fields(model, excluded_fields=[]):
    attrs = []
    for attr in dir(model):
        if attr not in excluded_fields:
            tfield = getattr(model, attr)
            if isinstance(tfield, fields.Field):
                attrs.append(attr)
    return attrs


def get_util_fields_values(object, excluded_fields=[]):
    model = Pool().get(object.__name__)
    fields = get_util_fields(model, excluded_fields)
    attrs = defaultdict(lambda: [])
    for attr in fields:
        attrs[attr] = getattr(object, attr, None)
    return attrs


class PurchasePlanReferenceTerms(Workflow, ModelSQL, ModelView):
    'Purchase Plan TDR'
    __name__ = 'purchase.plan.tdr'

    _STATES = {
        'readonly': Eval('state') != 'draft'
    }
    _DEPENDS = ['state']

    number = fields.Char('Number', readonly=True)
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], states={'readonly': True})
    responsable = fields.Many2One('company.employee', 'Responsable',
        states=_STATES, depends=_DEPENDS)
    date = fields.Date('Date', states=_STATES, depends=_DEPENDS)
    name = fields.Char('Name', states=_STATES, depends=_DEPENDS)

    background = fields.Text('Background', states=_STATES, depends=_DEPENDS,
        help="Background.")
    general_goal = fields.Text('General goal', states=_STATES,
        depends=_DEPENDS, help="Goals (For what?).")
    specific_goal = fields.Text('Specific goal', states=_STATES,
        depends=_DEPENDS, help="Goals (For what?).")
    scope = fields.Text('Scope', states=_STATES, depends=_DEPENDS,
        help="Scope (How far?).")
    methodology = fields.Text('Work methodology', states=_STATES,
        depends=_DEPENDS, help="Work methodology (How?).")
    information = fields.Text('Information available', states=_STATES,
        depends=_DEPENDS, help=" Information available to the entity "
        "(Diagnostics, statistics, etc.).")
    products = fields.Text('Products/services', states=_STATES,
        depends=_DEPENDS, help="Expected products or services (What and how?).")
    term = fields.Text('Term of execution', states=_STATES, depends=_DEPENDS,
        help="Term of execution: partial and/or total (When?).")
    personal = fields.Text('Technical staff', states=_STATES, depends=_DEPENDS,
        help="Technical staff/work team/resources (With whom or with what?).")
    payment = fields.Text('Payment', states=_STATES, depends=_DEPENDS,
        help="Form and conditions of payment.")
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('done', 'Aprobado'),
            ('cancel', 'Cancelado'),
        ], 'State', states=_STATES, depends=_DEPENDS, readonly=True)

    @classmethod
    def __setup__(cls):
        super(PurchasePlanReferenceTerms, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'done'),
            ('done', 'cancel'),
            ('done', 'draft'),
            ('cancel', 'draft'),
            ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['done']),
                'icon': If(Eval('state') == 'cancel', 'tryton-clear',
                    'tryton-undo'),
                'depends': ['state'],
            },
            'done': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state'],
            }
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, purchases):
        cls.set_number(purchases)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, purchases):
        pass

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_responsable():
        return Transaction().context.get('employee')

    @staticmethod
    def default_date():
        return date.today()

    @classmethod
    def set_number(cls, purchases):
        '''
        Fill the number field with the purchase tdr sequence
        '''
        # pool = Pool()
        # Sequence = pool.get('ir.sequence')
        # Config = pool.get('purchase.plan.configuration')
        # config = Config(1)
        for purchase in purchases:
            pass
            # if purchase.get('number') is None:
            #     purchase['number'] = Sequence.get_id(
            #         config.purchase_plan_tdr_sequence.id)


class PurchasePac(Workflow, ModelSQL, ModelView):
    'Purchase Pac'
    __name__ = 'purchase.pac'

    _STATES = {
        'readonly': ~Eval('state').in_(['draft', None]),
        'required': True
    }
    _DEPENDS = ['state']

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
            Eval('context', {}).get('company', -1)),
        ], states={'readonly': True})
    created_by = fields.Many2One('company.employee', 'Created by',
        domain=[
            ('company', '=', Eval('company'))
        ], states=_STATES, depends=_DEPENDS + ['company'])
    description = fields.Text('Description',
        states={
            'readonly': ~Eval('state').in_(['draft', None])
        }, depends=['state'])
    fiscal_year = fields.Many2One('account.fiscalyear', 'Fiscal year',
        states=_STATES, depends=_DEPENDS)
    poa = fields.Many2One('public.planning.unit', 'POA',
        domain=[
            ('company', '=', Eval('company'))
        ],
        states={
            'readonly': True
        }, depends=['company'])
    total_amount = fields.Function(fields.Numeric('Total', digits=(16, 2)),
        'get_total_amount')
    pac_budgets = fields.One2Many('purchase.pac.budget', 'purchase_pac',
        'PACs by budget',
        states={
            'readonly': ~(
                Eval('state').in_([None, 'draft', 'approved']) &
                Eval('fiscal_year')
            )
        }, depends=['state', 'fiscal_year', 'poa'])
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('approved', 'Approved'),
            ('canceled', 'Canceled'),
        ], 'State',
        states={
            'readonly': True,
        })
    state_translated = state.translated('state')
    progress = fields.Function(fields.Float('Progreso', digits=(16, 2)),
        'get_progress')

    @classmethod
    def __setup__(cls):
        super(PurchasePac, cls).__setup__()
        cls._error_messages.update({
            'no_delete': ("You can not delete the PAC '%(pac)s' because it is "
                "in approved state."),
            'pac_tied_to_a_purchase': ("You can not delete or pass to cancel "
                "or draft state a PAC that already has lines attached to a "
                "purchase request."),
            'error_passing_to_draft': ("You can NOT pass the PAC '%(pac)s' to "
                "draft state because it already has reforms."),
            'approval_error_no_pac_budgets': ("Error approving the PAC "
                "'%(pac)s'. No budget lines have been defined."),
            'approval_error_no_pac_lines': ("Error approving the PAC "
                "'%(pac)s'. No PAC lines or reforms have been defined for "
                "budget '%(budget)s'."),
            'approval_error_no_activity_budget': ("Error approving the PAC "
                "'%(pac)s'. An activity budget line for the PAC line "
                "'%(pac_line)s' has not been defined."),
            'approval_error_total_great_than_certified_pending': ("Error "
                "approving the PAC '%(pac)s'. The sum of the totals of the PAC "
                "lines ($'%(total_pac)s') that correspond to the activity "
                "'%(activity)s', exceed the total pending certification of "
                "said activity ($'%(certified_pending)s')."),
            'approval_error_no_certified_pending': ("Error approving the PAC "
                "%(pac)s. No pending values were found to be certified for "
                "the %(activity)s activity on the PAC %(pac_line)s line."),
            'approval_error_pac_line_total_amount': (
                "Error approving the PAC '%(pac)s'. In the Budget "
                "'%(budget)s', the total value ('$%(total_pac_line)s') of the "
                "PAC line '%(pac_line)s' must not be greater than the "
                "certified pending value of the activity budget line "
                "('$%(certified_pending)s')."),
            'approval_error_pac_budget_total_amount': (
                "Error approving the PAC '%(pac)s'. In the Budget "
                "'%(budget)s', the total amount ('$%(total_pac_budget)s') must "
                "not be greater than its assigned initial value "
                "('$%(certified_pending)s')."),
            'reform_error_total_great_than_certified_pending': ("Error "
                "saving a reform to the PAC '%(pac)s'. The sum of the totals "
                "of the PAC lines ($'%(total_pac)s') that correspond to the "
                "activity '%(activity)s', exceed the total pending "
                "certification of said activity ($'%(certified_pending)s')."),
        })
        cls._transitions |= set((
            ('draft', 'approved'),
            ('approved', 'canceled'),
            ('approved', 'draft'),
            ('draft', 'canceled'),
            ('canceled', 'draft'),
            ))
        cls._buttons.update({
                'draft': {
                    'invisible': (~Eval('state').in_(['approved']) |
                        Eval('state').in_(['canceled'])),
                    'icon': If(Eval('state') == 'canceled', 'tryton-clear',
                        'tryton-undo'),
                    'depends': ['state'],
                    },
                'approved': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': Eval('state').in_(['approved']),
                    'depends': ['state'],
                    },
                'canceled': {
                    'invisible': ~Eval('state').in_(['approved', 'draft']),
                    'depends': ['state'],
                    }
        })
        cls._order = [
            ('fiscal_year', 'DESC'),
            ('state', 'ASC'),
        ]

    @classmethod
    def delete(cls, pacs):
        for pac in pacs:
            if pac.state in ['approved']:
                cls.raise_user_error('no_delete', {
                    'pac': pac.rec_name
                })
        cls.verify_relationship_with_purchase(pacs)
        super(PurchasePac, cls).delete(pacs)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_created_by():
        return Transaction().context.get('employee')

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, pacs):
        # Validate data fields and total amounts
        for pac in pacs:
            if not pac.pac_budgets:
                cls.raise_user_error('approval_error_no_pac_budgets', {
                    'pac': pac.rec_name
                })
            for pac_budget in pac.pac_budgets:
                if not pac_budget.pac_lines and not pac_budget.reforms:
                    cls.raise_user_error('approval_error_no_pac_lines', {
                        'pac': pac.rec_name,
                        'budget': pac_budget.rec_name
                    })
                for pac_line in pac_budget.pac_lines:
                    if not pac_line.budgets:
                        pass
                        '''
                        cls.raise_user_error(
                            'approval_error_no_activity_budget', {
                                'pac': pac.rec_name,
                                'pac_line': pac_line.rec_name
                            })
                        '''
                    if not pac_line.certified_pending:
                        pass
                        '''
                        cls.raise_user_error(
                            'approval_error_no_certified_pending', {
                                'pac': pac.rec_name,
                                'activity': pac_line.activity.rec_name,
                                'pac_line': pac_line.rec_name
                            })
                        '''
                    #print("todo:", pac_line)
                    '''
                    if pac_line.total_amount > pac_line.certified_pending:
                        cls.raise_user_error(
                            'approval_error_pac_line_total_amount', {
                                'pac': pac.rec_name,
                                'budget': pac_budget.rec_name,
                                'pac_line': pac_line.rec_name,
                                'total_pac_line':
                                    Decimal(str(
                                        pac_line.total_amount)).quantize(CENT),
                                'certified_pending':
                                    pac_line.certified_pending
                            })
                    '''
                '''
                if pac_budget.total_amount > pac_budget.budget_certified_pending:
                    cls.raise_user_error(
                        'approval_error_pac_budget_total_amount', {
                            'pac': pac.rec_name,
                            'budget': pac_budget.rec_name,
                            'total_pac_budget':
                                Decimal(str(
                                    pac_budget.total_amount)).quantize(CENT),
                            'certified_pending':
                                pac_budget.budget_certified_pending
                        })
                '''
        # Validate total amounts less than certified pendings
        #cls.check_validity_certified_pendings(pacs)

    @classmethod
    def check_validity_certified_pendings(cls, pacs):
        for pac in pacs:
            amounts = {}
            certified_pendings = {}
            for pac_budget in pac.pac_budgets:
                for line in pac_budget.pac_lines:
                    activity = line.activity.rec_name
                    if activity not in amounts:
                        certified_pendings.update({
                            activity: line.certified_pending
                        })
                        amounts.update({
                            activity: line.total_amount
                        })
                    else:
                        amounts[activity] += line.total_amount
            for activity, amount in amounts.items():
                if amount > certified_pendings[activity]:
                    cls.raise_user_error(
                        'approval_error_total_great_than_certified_pending', {
                            'pac': pac.rec_name,
                            'activity': activity,
                            'total_pac': Decimal(str(amount)).quantize(CENT),
                            'certified_pending': certified_pendings[activity]
                        })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, pacs):
        for pac in pacs:
            if pac.pac_budgets:
                for pac_budget in pac.pac_budgets:
                    if pac_budget.reforms:
                        cls.raise_user_error('error_passing_to_draft', {
                            'pac': pac.rec_name
                        })
        cls.verify_relationship_with_purchase(pacs)

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def canceled(cls, pacs):
        cls.verify_relationship_with_purchase(pacs)

    @classmethod
    def verify_relationship_with_purchase(cls, pacs):
        PurchaseRequisitionLine = Pool().get('purchase.requisition.line')
        for pac in pacs:
            if pac.pac_budgets:
                for pac_budget in pac.pac_budgets:
                    if pac_budget.pac_lines:
                        for pac_line in pac_budget.pac_lines:
                            result = PurchaseRequisitionLine.search([
                                ('pac_line', '=', pac_line.id)
                            ])
                            if len(result) > 0:
                                cls.raise_user_error('pac_tied_to_a_purchase')

    @fields.depends('fiscal_year', 'poa')
    def on_change_fiscal_year(self):
        if self.fiscal_year:
            POA = Pool().get('public.planning.unit')
            result = POA.search([
                ('fiscalyear', '=', self.fiscal_year)
            ])
            if result:
                self.poa = result[0]

    @fields.depends('total_amount', 'pac_budgets')
    def on_change_with_total_amount(self, name=None):
        return self.get_total_amount()

    def get_total_amount(self, name=None):
        total_amount = Decimal('0.0').quantize(CENT)
        if self.pac_budgets:
            for pac_budget in self.pac_budgets:
                if pac_budget.total_amount:
                    amount = Decimal(str(pac_budget.total_amount)).quantize(
                        CENT)
                    total_amount += amount
        return total_amount

    @classmethod
    def get_progress(cls, pacs, names=None):
        result = defaultdict(lambda: 0)
        for pac in pacs:
            progress = 0
            total_acquired = 0
            total_quantity = 0
            if pac.pac_budgets:
                for pac_budget in pac.pac_budgets:
                    if pac_budget.pac_lines:
                        for pac_line in pac_budget.pac_lines:
                            if pac_line.total_acquired:
                                total_acquired += pac_line.total_acquired
                            if pac_line.quantity:
                                total_quantity += pac_line.quantity
            if total_quantity != 0:
                progress = total_acquired / total_quantity
            result[pac.id] = progress
        return {
            'progress': result
        }

    def get_rec_name(self, name):
        return 'PAC %s - %s' % (
            self.fiscal_year.name, self.company.business_name)


class PurchasePacBudget(ModelSQL, ModelView):
    'Purchase Pac Budget'
    __name__ = 'purchase.pac.budget'

    purchase_pac = fields.Many2One('purchase.pac', 'PAC',
        states={
            'required': True
        }, ondelete='CASCADE')
    company = fields.Function(fields.Many2One('company.company', 'Company'),
        'get_company')
    poa = fields.Function(fields.Many2One('public.planning.unit', 'POA',
        states={
            'invisible': True
        }), 'on_change_with_poa')
    program = fields.Many2One('public.planning.unit', 'Programa',
        domain=[
            ('type', '=', 'program'),
            ('parent', 'child_of', Eval('_parent_purchase_pac', {}).get('poa', -1)),
        ],
        states={
            'readonly': ~(Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                ['draft', None])),
        }, depends=['purchase_pac'])
    budget = fields.Many2One('public.budget', 'Budget',
        domain=[
            ('company', '=', Eval('company')),
            ('kind', '=', 'view'),
            ('type', '=', 'expense'),
            ('active', '=', True)
        ],
        states={
            'readonly': ~(Eval('_parent_purchase_pac') &
                ((Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                    ['draft', None])) |
                (Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                    ['approved']) &
                 Less(Len(Eval('pac_lines')), 0, True)))),
        }, depends=['company', 'purchase_pac', 'pac_lines'])
    budget_code = fields.Function(fields.Char('Code'), 'get_budget_code')
    description = fields.Text('Descripcion', 
        states={
            'readonly': ~Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                    ['draft', None])
        })
    budget_certified_pending = fields.Function(
        fields.Numeric('Pendiente por Certificar'),
        'get_budget_certified_pending')
    pac_lines = fields.One2Many('purchase.pac.line',
        'pac_budget', 'PAC Lines',
        filter=['OR',
            ('reform_state', '=', None),
            ('reform_state', '=', 'added'),
            ('reform_state', '=', 'pending'),
        ],
        states={
            'readonly': ~(
                Eval('_parent_purchase_pac') & Eval('budget') &
                Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                    ['draft', None])
            )
        }, depends=['purchase_pac', 'program'])
    reformed_lines = fields.One2Many('purchase.pac.line',
        'pac_budget', 'Reformed Lines',
        filter=['OR',
             ('reform_state', '=', 'modified'),
             ('reform_state', '=', 'deleted'),
        ],
        states={
            'readonly': True
        })
    reforms = fields.One2Many('purchase.pac.line.reform', 'pac_budget',
        'Reforms',
        states={
            'readonly': ~(
                Eval('_parent_purchase_pac') & Eval('budget') &
                Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                    ['approved'])
            )
        }, depends=['purchase_pac', 'budget'])
    total_amount = fields.Function(fields.Numeric('Total', digits=(16, 2)),
        'get_total_amount')
    progress = fields.Function(fields.Float('Progreso',
        digits=(16, 2)), 'get_progress')

    @classmethod
    def __setup__(cls):
        super(PurchasePacBudget, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_uniq', Unique(t, t.budget, t.purchase_pac),
             'PAC budgets must be uniques by Budget and Purchase PAC'),
        ]
        cls._order = [
            ('budget.code', 'ASC')
        ]

    @fields.depends('budget')
    def on_change_with_budget_code(self, names=None):
        if self.budget:
            return self.budget.code
        return None
    
    @fields.depends('program')
    def on_change_program(self):
        with Transaction().set_context(hola="hola"):
            pass

    @fields.depends('purchase_pac')
    def on_change_with_company(self, names=None):
        if self.purchase_pac:
            return self.purchase_pac.company.id
        return None

    @fields.depends('total_amount', 'pac_lines')
    def on_change_with_total_amount(self, name=None):
        return self.get_total_amount()

    @fields.depends('purchase_pac', '_parent_purchase_pac.poa')
    def on_change_with_poa(self, name=None):
        if self.purchase_pac:
            return self.purchase_pac.poa.id
        return None

    @fields.depends('pac_lines')
    def on_change_with_progress(self, name=None):
        PurchasePacBudget = Pool().get('purchase.pac.budget')
        if self.pac_lines:
            result = PurchasePacBudget.get_progress([self])['progress']
            return result[self.id]
        return 0

    @fields.depends('budget', 'budget_certified_pending')
    def on_change_budget(self, names=None):
        pass
        '''
        if self.budget:
            self.budget_certified_pending = \
                self.get_budget_certified_pending_individual()
        '''
    @classmethod
    def get_budget_certified_pending(cls, pac_budgets, names=None):
        result = defaultdict(lambda: None)
        for pac_budget in pac_budgets:
            result[pac_budget.id] = \
                pac_budget.get_budget_certified_pending_individual()
        return {
            'budget_certified_pending': result
        }

    def get_budget_certified_pending_individual(self):
        PublicBudgetCard = Pool().get('public.budget.card')
        pending = Decimal("0.0")
        for line in self.pac_lines:
            budgets = []
            for budget in line.budgets:
                budgets.append(budget.budget.id)
            cards = PublicBudgetCard.search([
                ('id', 'in', budgets)
            ])
            if cards:
                for card in cards:
                    pending += card.certified_pending
        return pending

    @fields.depends('pac_lines')
    def get_total_amount(self, name=None):
        total_amount = Decimal('0.0').quantize(CENT)
        if self.pac_lines:
            for line in self.pac_lines:
                if line.reform_state in [None, 'pending', 'added']:
                    if line.total_amount:
                        amount = Decimal(str(line.total_amount)).quantize(CENT)
                        total_amount += amount
        return total_amount

    @classmethod
    def get_company(cls, pac_budgets, names=None):
        result = defaultdict(lambda: [])
        for pac_budget in pac_budgets:
            if pac_budget.purchase_pac and pac_budget.purchase_pac.company:
                result[pac_budget.id] = pac_budget.purchase_pac.company.id
        return {
            'company': result
        }

    @classmethod
    def get_budget_code(cls, pac_budgets, names=None):
        result = defaultdict(lambda: [])
        for pac_budget in pac_budgets:
            if pac_budget.budget:
                result[pac_budget.id] = pac_budget.budget.code
        return {
            'budget_code': result
        }

    @classmethod
    def get_progress(cls, pac_budgets, names=None):
        result = defaultdict(lambda: 0)
        for pac_budget in pac_budgets:
            quantity = 0
            total_acquired = 0
            progress = 0
            if pac_budget.pac_lines:
                for pac_line in pac_budget.pac_lines:
                    if pac_line.quantity:
                        quantity += pac_line.quantity
                    try:
                        total_acquired += pac_line.total_acquired
                    except Exception:
                        pass
            if quantity != 0:
                progress = total_acquired / quantity
            result[pac_budget.id] = progress
        return {
            'progress': result
        }

    def get_rec_name(self, name):
        program_name =  self.budget.rec_name if self.budget else ''
        return '%s' % ( program_name)

class PurchasePacLineBudget(ModelSQL, ModelView):
    'Presupuesto de linea de PAC'
    __name__ = 'purchase.pac.line.budget'

    pac_line = fields.Many2One('purchase.pac.line', 'Pac line',
        ondelete='CASCADE',
        states={
            'readonly': True,
            'required': True
        })
    catalog_budget = fields.Function(
        fields.Many2One(
        'public.budget', 'Partida(s)'),
        'on_change_with_catalog_budget'
    )
    budget = fields.Many2One(
        'public.budget', 'Partida(s)', states={
            'required': True,
        }, domain=[
            ('parent', '=', Eval('catalog_budget')),
            ('kind', '=', 'other'),
            ('type', '=', 'expense'),
        ],depends=['pac_line', 'catalog_budget'])
    activity = fields.Function(fields.Many2One(
        'public.planning.unit',
        'Actividad'
    ), 'on_change_with_activity')
    total_budguet = fields.Numeric('Asignacion Inicial', digits=(16, 2),
        states={'readonly': True,'required': True,}, depends=['budget'])
    certified_pending = fields.Function(
        fields.Numeric('Pendiente por certificar', digits=(16, 2)),
        'on_change_with_certified_pending')
    amount = fields.Numeric('Valor', digits=(16, 4),
        states={
            'readonly': Eval('state') == 'done',
            'required': True
        },
        depends=['pac_line', 'state'])
    
    @fields.depends('budget')
    def on_change_budget(self):
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            self.total_budguet = budget_data.initial_amount
        else:
            self.total_budguet = 0
    
    @fields.depends('budget')
    def on_change_with_certified_pending(self, name=None):
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            return budget_data.certified_pending
        else:
            return Decimal(0.0)

    
    @fields.depends('budget')
    def on_change_with_activity(self, name=None):
        if self.budget:
            return self.budget.activity.id
        else:
            return -1
    
    @fields.depends('pac_line')
    def on_change_with_catalog_budget(self, name=None):
        if self.pac_line:
            return self.pac_line.pac_budget.budget.id
        else:
            return -1

class PurchasePacLine(Workflow, ModelSQL, ModelView):
    'Purchase Pac Lines'
    __name__ = 'purchase.pac.line'

    _STATES = {
        'readonly': ~(Eval('_parent_pac_budget', {}).get(
            '_parent_purchase_pac', {}).get('state', -1).in_(['draft', None]))
    }
    _DEPENDS = ['pac_budget']
    _REFORM_STATE = [
        (None, 'None'),
        ('pending', 'Pendiente'),
        ('added', 'Agregado'),
        ('modified', 'Modificado'),
        ('deleted', 'Eliminado'),
    ]

    pac_budget = fields.Many2One('purchase.pac.budget', 'PAC Budget',
        states={
            'readonly': ~(Eval('_parent_purchase_pac', {}).get('state', -1).in_(
                ['draft', None]))
        }, depends=['purchase_pac'], ondelete='CASCADE')
    #TODO: delete when finished migration
    activity = fields.Many2One('public.planning.unit',
        'Activity', 
        domain=[
            ('parent', 'child_of', Eval('_parent_pac_budget', {}).get('program', -1))
        ],
        states={
            'readonly':  ~Eval('state').in_(['draft', None]),
        }, depends=['state'])
    budgets = fields.One2Many(
        'purchase.pac.line.budget', 'pac_line',
        'Partida(s)', states={
           'readonly': ~Eval('pac_budget')
        }
    )
    state = fields.Selection(_REFORM_STATE, 'Estado',
        states={
            'readonly': True,
        })
    purchase_pac = fields.Function(fields.Many2One('purchase.pac', 'PAC'),
        'get_purchase_pac')
    #TODO: delete when finished migration
    activity_budget = fields.Many2One('public.budget', 'Activity budget',
        domain=[
            ('activity', '=', Eval('activity')),
            ('kind', '=', 'other'),
            ('type', '=', 'expense'),
            ('active', '=', True)
        ], states={
            'readonly': (~(Eval('_parent_pac_budget', {}).get(
            '_parent_purchase_pac', {}).get('state', -1).in_(['draft', None])) |
            Bool(Eval("activity"))==False),

        }, depends=_DEPENDS)
    certified_pending = fields.Function(fields.Numeric('Certified pending',
        digits=(16, 2)), 'on_change_with_certified_pending')
    departments = fields.Many2Many('purchase.pac.line.department.company',
        'pac_line', 'department', 'Departments', states=_STATES,
        depends=_DEPENDS)
    cpc = fields.Many2One('purchase.pac.category', 'CPC',
        domain=[
            ('level', '=', '9')
        ], states=_STATES, depends=_DEPENDS, required=True)
    description = fields.Text('Product description', states=_STATES,
        depends=_DEPENDS, required=True)
    product = fields.Many2One('product.product', 'Product', states=_STATES,
        depends=_DEPENDS)
    purchase_type = fields.Selection(
        [
            ('property', 'Property'),
            ('service', 'Service'),
            ('consultancy', 'Consultancy'),
            ('work', 'Work'),
        ], 'Purchase type', required=True, states=_STATES, depends=_DEPENDS)
    purchase_type_translated = purchase_type.translated('purchase_type')
    unit = fields.Many2One('product.uom', 'Unit', states=_STATES,
        depends=_DEPENDS, required=True)
    quantity = fields.Float('Quantity', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ], states=_STATES, depends=_DEPENDS, required=True)
    unit_price_without_iva = fields.Numeric('Unit price without IVA',
        domain=[
            ('unit_price_without_iva', '>', 0)
        ], 
        states=_STATES, depends=_DEPENDS, digits=(16, 4), required=True)
    total_amount = fields.Function(fields.Numeric('Total amount',
        domain=[
            ('total_amount', '>', 0)
        ], digits=(16, 2)), 'on_change_with_total_amount')
    quarter1 = fields.Boolean('C1', states=_STATES, depends=_DEPENDS)
    quarter2 = fields.Boolean('C2', states=_STATES, depends=_DEPENDS)
    quarter3 = fields.Boolean('C3', states=_STATES, depends=_DEPENDS)
    regime_type = fields.Selection(
        [
            ('common', 'Common'),
            ('special', 'Special'),
            ('no-apply', 'No apply'),
        ], 'Regime type', states=_STATES, depends=_DEPENDS, required=True)
    regime_type_translated = regime_type.translated('regime_type')
    bid_founds = fields.Boolean('BID Founds',
        states={
            'readonly': ~(
                Eval('_parent_pac_budget', {}).get(
                    '_parent_purchase_pac', {}).get('state', -1).in_(
                        ['draft', None]) &
                Eval('regime_type').in_(['common']))
        }, depends=['pac_budget', 'regime_type', 'purchase_pac'])
    project_code = fields.Char('Project code',
        states={
            'readonly': ~(
                Eval('_parent_pac_budget', {}).get(
                    '_parent_purchase_pac', {}).get('state', -1).in_(
                        ['draft', None]) & Eval('bid_founds').in_([True])
                ),
            'invisible': ~(Eval('bid_founds').in_([True])),
            'required': ~(~Eval('bid_founds'))
        }, depends=['pac_budget', 'bid_founds', 'purchase_pac'])
    operation_code = fields.Char('Operation code',
        states={
            'readonly': ~(
                Eval('_parent_pac_budget', {}).get(
                    '_parent_purchase_pac', {}).get('state', -1).in_(
                        ['draft', None]) & Eval('bid_founds').in_([True])
                ),
            'invisible': ~(Eval('bid_founds').in_([True])),
            'required': ~(~Eval('bid_founds'))
        }, depends=['pac_budget', 'bid_founds', 'purchase_pac'])
    budget_type = fields.Selection(
        [
            ('investment', 'Investment project'),
            ('current', 'Current Expenditure'),
        ], 'Budget type', states=_STATES, depends=_DEPENDS, required=True)
    budget_type_translated = budget_type.translated(
        'budget_type')
    product_type = fields.Selection(
        [
            ('normalized', 'Normalized'),
            ('no-normalized', 'No normalized'),
            ('no-apply', 'No apply'),
        ], 'Product type',
        states={
            'readonly': ~(
                Eval('_parent_pac_budget', {}).get(
                    '_parent_purchase_pac', {}).get('state', -1).in_(
                    ['draft', None]) &
                Eval('purchase_type').in_(['property', 'service']) &
                Eval('regime_type').in_(['common'])),
            'required': Eval('purchase_type').in_(['property', 'service'])
        }, depends=['purchase_type', 'pac_budget', 'regime_type',
            'purchase_pac'],)
    product_type_translated = product_type.translated('product_type')
    electronic_catalog = fields.Boolean('Electronic catalog',
        states={
            'readonly': ~(
                Eval('_parent_pac_budget', {}).get(
                    '_parent_purchase_pac', {}).get('state', -1).in_(
                    ['draft', None]) &
                Eval('purchase_type').in_(['property', 'service']) &
                Eval('product_type').in_(['normalized']) &
                Eval('suggested_procedure').in_([None])),
        }, depends=['purchase_type', 'product_type', 'suggested_procedure',
            'pac_budget', 'purchase_pac'])
    suggested_procedure = fields.Many2One('purchase.pac.type.contract',
        'Suggested procedure',
        states={
            'readonly': ~(Eval('_parent_pac_budget', {}).get(
                '_parent_purchase_pac', {}).get('state', -1).in_(
                ['draft', None]) & Eval('electronic_catalog').in_([False])),
            # 'required': ~(~(Eval('electronic_catalog').in_([False])))
        }, depends=['pac_budget', 'electronic_catalog', 'purchase_pac'])
    pac_state = fields.Function(fields.Selection(
        [
            ('draft', 'Draft'),
            ('approved', 'Approved'),
            ('canceled', 'Canceled'),
        ], 'PAC State'), 'get_pac_state')
    pac_state_translated = pac_state.translated('pac_state')

    # Reform information
    reform_state = fields.Selection(_REFORM_STATE, 'Reform state',
        states={
            'readonly': True
        })
    reform_state_translated = reform_state.translated('reform_state')
    reform_state_aux = fields.Function(fields.Selection(_REFORM_STATE,
        'Reform state',
        states={
            'readonly': True
        }), 'get_reform_state_aux')
    reform_state_aux_translated = reform_state_aux.translated(
        'reform_state_aux')
    reform = fields.Many2One('purchase.pac.line.reform', 'Reform',
        states={
            'invisible': True
        })
    line_that_was_reformed = fields.Many2One('purchase.pac.line',
        'Line that was reformed',
        states={
            'readonly': True,
            'invisible': Eval('line_that_was_reformed').in_([None])
        })
    line_emerged_from_reform = fields.Many2One('purchase.pac.line',
        'Line emerged from reform',
        states={
            'readonly': True,
            'invisible': Eval('line_emerged_from_reform').in_([None])
        })

    # Acquisition information
    total_acquired = fields.Function(fields.Numeric('Total acquired',
        digits=(16, 2)), 'get_total_acquired')
    progress = fields.Function(fields.Numeric('Progreso',
        states={
            'invisible': Eval('reform_state').in_(['modified', 'deleted']),
        }, digits=(16, 2)), 'get_progress')

    @classmethod
    def __setup__(cls):
        super(PurchasePacLine, cls).__setup__()
        cls._error_messages.update({
            'quarters_error': ('Error on PAC Line %(pac_line)s. You must select'
                ' at least one quarter.'),
            'no_delete': ('You can not delete a line of a PAC in the Approved '
                'or Canceled state.'),
            'invalid_product_type_selection': ('Error on PAC Line of the '
                'budget "%(budget)s". The product type can not be "NO APPLY" '
                'when the regime type is "COMMON" or "SPECIAL".')
        })
        cls._transitions |= set((
            (None, 'pending'),
            ('pending', 'added'),
            ('pending', 'modified'),
            ('pending', 'deleted'),
            ('modified', 'pending'),
            ('added', 'pending'),
            ('deleted', 'pending'),
            ('pending', None)
        ))
        cls._order = [
            ('reform_state', 'ASC'),
            ('pac_budget.budget.code', 'ASC'),
            ('cpc.code', 'ASC'),
            ('purchase_type', 'ASC'),
            ('product_type', 'ASC'),
        ]
    
    @classmethod
    def validate(cls, pac_lines):
        super(PurchasePacLine, cls).validate(pac_lines)
        for line in pac_lines:
            # Select at least one quarter
            if (not line.quarter1) and (not line.quarter2) and \
                    (not line.quarter3):
                cls.raise_user_error('quarters_error', {
                    'pac_line': line.rec_name
                })
            # Product Type
            line.check_product_type_validity()

    @classmethod
    def delete(cls, pac_lines):
        for line in pac_lines:
            if line.purchase_pac:
                if line.purchase_pac.state in ['approved']:
                    if line.purchase_pac.state not in ['canceled']:
                        cls.raise_user_error('no_delete')
        super(PurchasePacLine, cls).delete(pac_lines)

    @staticmethod
    def default_reform_state():
        return None

    def check_product_type_validity(self):
        if self.product_type and self.product_type in ['no-apply']:
            if self.regime_type and (
                        self.regime_type in ['common', 'special']):
                pass
                #TODO: determine why it is necessary?
                '''        
                self.raise_user_error('invalid_product_type_selection', {
                    'budget': self.pac_budget.budget.code
                })
                '''

    @fields.depends('product')
    def on_change_with_description(self, name=None):
        if self.product:
            description = self.product.name
            if self.product.description:
                description = '%s. %s' % (description, self.product.description)
            return description
        return None

    @fields.depends('product')
    def on_change_with_unit(self, name=None):
        if self.product and self.product.default_uom:
            return self.product.default_uom.id
        return None

    @fields.depends('quantity', 'unit_price_without_iva')
    def on_change_with_total_amount(self, name=None):
        if self.quantity and self.unit_price_without_iva:
            q = Decimal(str(self.quantity))
            u = Decimal(str(self.unit_price_without_iva))
            r = q * u
            return Decimal(str(r)).quantize(CENT)
        return None

    @fields.depends('reform', 'line_emerged_from_reform')
    def on_change_with_reform_state(self, name=None):
        if self.reform:
            if self.line_emerged_from_reform:
                if self.reform.state != 'approved':
                    return 'pending'
        return None

    @fields.depends('pac_budget')
    def on_change_with_purchase_pac(self, name=None):
        if self.pac_budget and self.pac_budget.purchase_pac:
            return self.pac_budget.purchase_pac.id

    @fields.depends('pac_budget')
    def on_change_with_pac_state(self, name=None):
        if self.pac_budget and self.pac_budget.purchase_pac:
            return self.pac_budget.purchase_pac.state

    @fields.depends('reform_state')
    def on_change_with_reform_state_aux(self, name=None):
        if self.reform_state:
            return self.reform_state
        return None

    @fields.depends('purchase_type', 'electronic_catalog')
    def on_change_purchase_type(self, name=None):
        if self.purchase_type:
            if self.purchase_type not in ['service', 'property']:
                self.product_type = 'no-apply'
                self.electronic_catalog = False
                self.regime_type = 'no-apply'

    @fields.depends('product_type', 'electronic_catalog',)
    def on_change_product_type(self, name=None):
        if self.product_type:
            if self.product_type not in ['no-normalized']:
                self.electronic_catalog = None


    @fields.depends('regime_type', 'bid_founds', 'product_type')
    def on_change_regime_type(self, name=None):
        if self.regime_type:
            if self.regime_type in ['special', 'no-apply']:
                self.bid_founds = None
                self.project_code = None
                self.operation_code = None
                if self.regime_type in ['special']:
                    self.product_type = 'no-apply'
                else:
                    self.product_type = 'no-apply'

    # @fields.depends('product_type', 'regime_type')
    # def on_change_product_type(self, name=None):
    #     self.check_product_type_validity()
    #TODO: DAULE-CAMBIOS PARTIDAS MULTIPLES
    '''
    @fields.depends('activity_budget', 'activity', 'certified_pending')
    def on_change_activity_budget(self, names=None):
        PublicBudgetCard = Pool().get('public.budget.card')
        if self.activity_budget:
            if self.activity_budget.activity:
                self.activity = self.activity_budget.activity
                cards = PublicBudgetCard.search([
                    ('activity', '=', self.activity),
                    ('code', '=', self.activity_budget.code)
                ])
                if cards:
                    self.certified_pending = cards[0].certified_pending
        else:
            self.activity = None
            self.certified_pending = None
    '''
    
    @fields.depends('budgets', 'activity', 'certified_pending')
    def on_change_budgets(self, names=None):
        #TODO: WIP - DAULE
        PublicBudgetCard = Pool().get('public.budget.card')
        pending = Decimal("0.0")
        if self.budgets:
            budgets = []
            for budget in self.budgets:
                if budget.budget:
                    budgets.append(budget.budget.id)
            cards = PublicBudgetCard.search([
                ('id', 'in', budgets)
            ])
            if cards:
                pending = Decimal('0.0')
                for card in cards:
                    pending += card.certified_pending
        self.certified_pending = pending

    @fields.depends('quantity', 'total_acquired')
    def on_change_with_progress(self, names=None):
        if self.quantity and self.total_acquired:
            return self.total_acquired / self.quantity
        return 0

    @classmethod
    def get_progress(cls, pac_lines, names=None):
        result = defaultdict(lambda: 0)
        for pac_line in pac_lines:
            result[pac_line.id] = 0
            if pac_line.quantity and pac_line.total_acquired:
                result[pac_line.id] = (
                    pac_line.total_acquired / pac_line.quantity)
        return {
            'progress': result
        }

    @fields.depends('budgets')
    def on_change_with_certified_pending(self, name=None):
        PublicBudgetCard = Pool().get('public.budget.card')
        result = Decimal(0.0)
        budgets = []
        for budget in self.budgets:
            if budget.budget:
                budgets.append(budget.budget.id)
        cards = PublicBudgetCard.search([
            ('id', 'in', budgets)
        ])
        if cards:
            for card in cards:
                result += card.certified_pending
        return result

    @classmethod
    def get_pac_state(cls, pac_lines, names=None):
        result = defaultdict(lambda: [])
        for pac_line in pac_lines:
            if pac_line.pac_budget and pac_line.pac_budget.purchase_pac:
                result[pac_line.id] = pac_line.pac_budget.purchase_pac.state
        return {
            'pac_state': result
        }

    @classmethod
    def get_purchase_pac(cls, pac_lines, names=None):
        result = defaultdict(lambda: [])
        for pac_line in pac_lines:
            if pac_line.pac_budget and pac_line.pac_budget.purchase_pac:
                result[pac_line.id] = pac_line.pac_budget.purchase_pac.id
        return {
            'purchase_pac': result
        }

    @classmethod
    def get_reform_state_aux(cls, pac_lines, names=None):
        result = defaultdict(lambda: [])
        for pac_line in pac_lines:
            result[pac_line.id] = pac_line.reform_state
        return {
            'reform_state_aux': result
        }

    @classmethod
    def get_total_acquired(cls, pac_lines, names=None):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        StockMove = pool.get('stock.move')
        transaction = Transaction()
        cursor = transaction.connection.cursor()

        table = cls.__table__()
        purchase_line = PurchaseLine.__table__()
        stock_move = StockMove.__table__()

        result = defaultdict(lambda: 0)
        pac_line_ids = [p.id for p in pac_lines]
        for sub_ids in grouped_slice(pac_line_ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(purchase_line,
                condition=purchase_line.pac_line == table.id
            ).join(stock_move,
                condition=stock_move.origin == (
                    Concat(PurchaseLine.__name__ + ',', purchase_line.id))
            ).select(
                table.id.as_('id'),
                Sum(stock_move.internal_quantity).as_('internal_quantity'),
                where=red_sql & (stock_move.state == 'done'),
                group_by=[table.id]
            )
            cursor.execute(*query)
            for id, internal_quantity in cursor.fetchall():
                result[id] = internal_quantity

            return {
                'total_acquired': result,
            }

    def get_rec_name(self, name):
        reform_txt = ''
        if self.reform:
            if self.reform_state:
                reform_txt = '(REFORMA %s)' % (
                    self.reform.type_reform_translated
                )
            else:
                reform_txt = '(REFORMA)'
        activity_txt = self.activity.code if self.activity else '(?)'
        code = ''
        if self.pac_budget.budget and self.pac_budget.budget.code:
            code = self.pac_budget.budget.code
        rec_name = 'PAC %s - PARTIDA %s - ACTIVIDAD %s - CPC %s / %s %s %s' % (
            self.purchase_pac.fiscal_year.name, code,
            activity_txt, self.cpc.code, self.purchase_type_translated,
            self.product_type_translated, reform_txt
        )
        return rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('cpc.code',) + tuple(clause[1:]),
            ('description',) + tuple(clause[1:]),
            ('product_type',) + tuple(clause[1:]),
            ('purchase_type',) + tuple(clause[1:]),
            ('pac_budget.budget.code',) + tuple(clause[1:]),
            ('pac_budget.purchase_pac.fiscal_year.name',) + tuple(clause[1:]),
            ('suggested_procedure.type_contract',) + tuple(clause[1:]),
        ]
        return domain


class PurchasePacTypeContract(ModelSQL, ModelView):
    'Purchase Pac Type Contract'
    __name__ = 'purchase.pac.type.contract'

    type_contract = fields.Char('Type Contract', select=True, required=True)
    used_on_public_purchases = fields.Boolean('Is used on public purchases?')
    quotation_process = fields.Boolean('Proceso de cotización?')
    min_value = fields.Float('Minimo', required=True)
    max_value = fields.Float('Maximo', required=True)
    pac_required = fields.Boolean('Requiere PAC?')
    purchase_request = fields.Boolean('Se requiere en Compra?')
    precertification = fields.Boolean('Precertificacion')

    @staticmethod
    def default_quotation_process():
        return False
    
    @staticmethod
    def default_precertification():
        return False

    @staticmethod
    def default_pac_required():
        return False

    @staticmethod
    def default_purchase_request():
        return False

    @staticmethod
    def default_min_value():
        return Decimal('1.00')

    @staticmethod
    def default_max_value():
        return Decimal('1.00')

    def get_rec_name(self, name):
        if self.type_contract:
            return self.type_contract

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('type_contract',) + tuple(clause[1:])
        ]
        return domain


class PurchasePacCategory(ModelSQL, ModelView):
    'Purchase Pac Category'
    __name__ = 'purchase.pac.category'

    code = fields.Char('Code', select=True)
    name = fields.Char('Name', select=True)
    level = fields.Char('Level', size=None, select=True)
    parent = fields.Many2One('purchase.pac.category', 'Parent', readonly=True)
    childs = fields.One2Many('purchase.pac.category', 'parent', 'Childs')

    def get_rec_name(self, name):
        return '%s - %s' % (self.code, self.name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ('level',) + tuple(clause[1:]),
        ]
        return domain


class PurchasePacLineDepartmentCompany(ModelSQL, ModelView):
    'Purchase Pac Line Department Company'
    __name__ = 'purchase.pac.line.department.company'

    pac_line = fields.Many2One('purchase.pac.line', 'PAC Line', required=True,
        ondelete='CASCADE')
    department = fields.Many2One('company.department', 'Department',
        required=True, ondelete='CASCADE')


class PurchasePacLineReformDepartmentCompany(ModelSQL, ModelView):
    'Purchase Pac Line Reform Department Company'
    __name__ = 'purchase.pac.line.reform.department.company'

    pac_line_reform = fields.Many2One('purchase.pac.line.reform',
        'PAC Line Reform', required=True, ondelete='CASCADE')
    department = fields.Many2One('company.department', 'Department',
        required=True, ondelete='CASCADE')


class PurchasePacLineReformBudget(Workflow, ModelSQL, ModelView):
    'Purchase Pac Line Reform Budget'
    __name__ = 'purchase.pac.line.reform.budget'

    pac_line = fields.Many2One('purchase.pac.line.reform', 'Pac line',
        ondelete='CASCADE',
        states={
            'readonly': True,
            'required': True
        })
    catalog_budget = fields.Function(
        fields.Many2One(
        'public.budget', 'Partida(s)'),
        'on_change_with_catalog_budget'
    )
    budget = fields.Many2One(
        'public.budget', 'Partida(s)', states={
            'required': True,
            'readonly': Eval('_parent_pac_line',{}).get('state', 'draft') != 'draft'
        }, domain=[
            ('parent', '=', Eval('catalog_budget')),
            ('kind', '=', 'other'),
            ('type', '=', 'expense'),
        ],depends=['pac_line', 'catalog_budget'])
    activity = fields.Function(fields.Many2One(
        'public.planning.unit',
        'Actividad'
    ), 'on_change_with_activity')
    certified_pending = fields.Numeric('Pendiente por certificar', digits=(16, 2),
            states={'readonly': True,'required': True,}, depends=['budget'])
    total_budguet = fields.Numeric('Asignacion Inicial', digits=(16, 2),
            states={'readonly': True,'required': True,})
    amount = fields.Numeric('Valor', digits=(16, 4),
        states={
            'readonly': Eval('_parent_pac_line',{}).get('state', 'draft') != 'draft',
            'required': True
        },
        depends=['pac_line', 'state'])
    
    @fields.depends('budget')
    def on_change_budget(self):
        if self.budget:
            pool = Pool()
            BudgetCard = pool.get('public.budget.card')
            budget_data, = BudgetCard.search([
                ('id', '=',
                 self.budget.id)])
            self.total_budguet = budget_data.initial_amount
            self.certified_pending = budget_data.certified_pending
        else:
            self.total_budguet = 0
            self.certified_pending = 0
            
    
    @fields.depends('budget')
    def on_change_with_activity(self, name=None):
        if self.budget:
            return self.budget.activity.id
        else:
            return -1
    
    @fields.depends('pac_line')
    def on_change_with_catalog_budget(self, name=None):
        if self.pac_line:
            return self.pac_line.pac_budget.budget.id
        else:
            return -1

class PurchasePacLineReform(Workflow, ModelSQL, ModelView):
    'Purchase Pac Line Refom'
    __name__ = 'purchase.pac.line.reform'

    _STATES = {
        'readonly': ~(
            Eval('state').in_(['draft', None]) & Eval('pac_budget') &
            Eval('type_reform') & (
                (Eval('type_reform').in_(['modify']) & Eval('line_to_reform')) |
                (Eval('type_reform').in_(['add']))
            ))
    }
    _DEPENDS = ['state', 'pac_budget', 'type_reform', 'line_to_reform']

    # REFORM HEADER
    purchase_pac = fields.Function(fields.Many2One('purchase.pac', 'PAC'),
        'get_purchase_pac')
    pac_budget = fields.Many2One('purchase.pac.budget', 'PAC Budget',
        domain=[
            ('purchase_pac.state', '=', 'approved')
        ],
        states={
            'readonly': ~(Eval('state').in_(['draft', None])),
        }, depends=['state'], required=True, ondelete='CASCADE')
    type_reform = fields.Selection(
        [
            ('modify', 'Modify'),
            ('delete', 'Delete'),
            ('add', 'Add'),
        ], 'Type Reform',
        states={
            'readonly': ~(Eval('state').in_(['draft', None]) &
                Eval('pac_budget')),
            'required': True
        }, depends=['state', 'pac_budget'])
    type_reform_translated = type_reform.translated('type_reform')
    line_to_reform = fields.Many2One('purchase.pac.line', 'Line to reform',
        domain=[
            ('pac_budget', '=', Eval('pac_budget')),
            ('reform_state', 'in', [None, 'added']),
        ],
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('type_reform').in_(['modify', 'delete'])
            ),
            'required': (
                Eval('pac_budget') &
                Eval('type_reform').in_(['modify', 'delete'])
            )
        }, depends=['pac_budget', 'type_reform'], ondelete='CASCADE')
    new_line = fields.Many2One('purchase.pac.line', 'New line')
    responsable = fields.Many2One('company.employee', 'Responsable',
        domain=[
            ('company', '=', Eval('_parent_pac_budget', {}).get('company', -1))
        ],
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) &
                Eval('pac_budget') & Eval('type_reform')
            ),
            'required': True
        }, depends=[
            'state',
            'pac_budget',
            'type_reform',
        ])
    date = fields.Date('Date',
        states={
            'readonly': True
        })
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('approved', 'Approved'),
        ], 'State', readonly=True)
    state_translated = state.translated('state')

    # The following fields must have the same name as the public.pac.line fields
    # Reform Body
    activity_budget = fields.Many2One('public.budget', 'Activity budget',
        domain=[
            ('parent', '=', Eval('_parent_pac_budget', {}).get('budget', -1)),
            ('activity', 'child_of',
                Eval('_parent_pac_budget', {}).get('poa', -1), 'parent'),
            ('kind', '=', 'other'),
            ('type', '=', 'expense'),
            ('active', '=', True)
        ], states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('type_reform') & (
                    (Eval('type_reform').in_(['modify']) & Eval(
                        'line_to_reform') &
                     Eval('total_acquired').in_([0])) |
                    (Eval('type_reform').in_(['add'])))),
        }, depends=[
            'state',
            'type_reform',
            'line_to_reform',
            'pac_budget',
            'total_acquired'
        ])
    activity = fields.Many2One('public.planning.unit',
        'Activity', states={
            'readonly':  ~Eval('state').in_(['draft', None]),
        }, depends=['state'])
    budgets = fields.One2Many(
        'purchase.pac.line.reform.budget', 'pac_line',
        'Partida(s)', states=_STATES)
    certified_pending = fields.Function(fields.Numeric('Certified pending',
        digits=(16, 2)), 'on_change_with_certified_pending')
    departments = fields.Many2Many(
        'purchase.pac.line.reform.department.company', 'pac_line_reform',
        'department', 'Departments', states=_STATES, depends=_DEPENDS)
    purchase_type = fields.Selection(
        [
            ('property', 'Property'),
            ('service', 'Service'),
            ('consultancy', 'Consultancy'),
            ('work', 'Work'),
        ], 'Purchase type', states=_STATES, depends=_DEPENDS, required=True)
    purchase_type_translated = purchase_type.translated('purchase_type')
    cpc = fields.Many2One('purchase.pac.category', 'CPC',
        domain=[
            ('level', '=', '9')
        ], states=_STATES, depends=_DEPENDS, required=True)
    product = fields.Many2One('product.product', 'Product', states=_STATES,
        depends=_DEPENDS)
    description = fields.Text('Product description', states=_STATES,
        depends=_DEPENDS, required=True)
    unit = fields.Many2One('product.uom', 'Unit', states=_STATES,
        depends=_DEPENDS, required=True)
    product_type = fields.Selection(
        [
            ('normalized', 'Normalized'),
            ('no-normalized', 'No normalized'),
            ('no-apply', 'No apply'),
        ], 'Product type',
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('purchase_type').in_(['property', 'service']) &
                Eval('regime_type').in_(['common']) &
                Eval('type_reform') & (
                    (Eval('type_reform').in_(['modify']) &
                     Eval('line_to_reform')) |
                    (Eval('type_reform').in_(['add'])))),
            'required': ~(~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('purchase_type').in_(['property', 'service']) &
                Eval('type_reform') & (
                    (Eval('type_reform').in_(['modify']) &
                     Eval('line_to_reform')) |
                    (Eval('type_reform').in_(['add']))))),
        }, depends=[
            'state',
            'pac_budget',
            'purchase_type',
            'type_reform',
            'line_to_reform',
            'regime_type'
        ])
    product_type_translated = product_type.translated('product_type')
    quantity = fields.Float('Quantity', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ], states=_STATES, depends=_DEPENDS, required=True)
    unit_price_without_iva = fields.Numeric('Unit price without IVA',
        domain=[
            ('unit_price_without_iva', '>', 0)
        ], states=_STATES, depends=_DEPENDS, digits=(16, 4), required=True)
    total_amount = fields.Function(fields.Numeric('Total amount',
        domain=[
            ('total_amount', '>', 0)
        ], digits=(16, 2)), 'on_change_with_total_amount')
    quarter1 = fields.Boolean('C1', states=_STATES, depends=_DEPENDS)
    quarter2 = fields.Boolean('C2', states=_STATES, depends=_DEPENDS)
    quarter3 = fields.Boolean('C3', states=_STATES, depends=_DEPENDS)
    bid_founds = fields.Boolean('BID Founds',
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('regime_type').in_(['common']) &
                Eval('type_reform') & (
                    (Eval('type_reform').in_(['modify']) &
                     Eval('line_to_reform')) |
                    (Eval('type_reform').in_(['add']))
                ))
        }, depends=_DEPENDS)
    project_code = fields.Char('Project code',
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('type_reform') & ((
                    Eval('type_reform').in_(['modify']) &
                    Eval('line_to_reform')) |
                    (Eval('type_reform').in_(['add']))
                ) & (Eval('bid_founds'))),
            'invisible': ~(Eval('bid_founds')),
            'required': ~(~Eval('bid_founds'))
        }, depends=['state', 'pac_budget', 'type_reform', 'line_to_reform',
            'bid_founds'])
    operation_code = fields.Char('Operation code',
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('type_reform') & (
                    (Eval('type_reform').in_(['modify']) &
                     Eval('line_to_reform')) |
                    (Eval('type_reform').in_(['add']))
                ) & (Eval('bid_founds'))),
            'invisible': ~(Eval('bid_founds')),
            'required': ~(~Eval('bid_founds'))
        },
        depends=[
            'state',
            'pac_budget',
            'type_reform',
            'line_to_reform',
            'bid_founds'
        ])
    regime_type = fields.Selection(
        [
            ('common', 'Common'),
            ('special', 'Special'),
            ('no-apply', 'No apply'),
        ], 'Regime type', states=_STATES, depends=_DEPENDS, required=True)
    regime_type_translated = regime_type.translated('regime_type')
    budget_type = fields.Selection(
        [
            ('investment', 'Investment project'),
            ('current', 'Current Expenditure'),
        ], 'Budget type', states=_STATES, depends=_DEPENDS, required=True)
    budget_type_translated = budget_type.translated('budget_type')
    electronic_catalog = fields.Boolean('Electronic catalog',
        states={
            'readonly': ~(
                Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                Eval('purchase_type').in_(['property', 'service']) &
                Eval('product_type').in_(['normalized']) &
                Eval('type_reform') & ((Eval('type_reform').in_(['modify']) &
                Eval('line_to_reform')) | (Eval('type_reform').in_(['add']))) &
                Eval('suggested_procedure').in_([None])),
        }, depends=[
            'state',
            'pac_budget',
            'suggested_procedure',
            'type_reform',
            'line_to_reform'
        ])
    suggested_procedure = fields.Many2One('purchase.pac.type.contract',
        'Suggested procedure',
            domain=[
                ('used_on_public_purchases', '=', True)
            ],
            states={
                'readonly': ~(
                    Eval('state').in_(['draft', None]) & Eval('pac_budget') &
                    Eval('type_reform') & (
                        (Eval('type_reform').in_(['modify']) &
                         Eval('line_to_reform')) |
                        (Eval('type_reform').in_(['add']))
                    ) & Eval('electronic_catalog').in_([False])
                ),
                'required': ~(~(Eval('electronic_catalog').in_([False])))
            },
            depends=[
                'pac_budget',
                'electronic_catalog',
                'type_reform',
                'line_to_reform'
            ])
    total_acquired = fields.Function(fields.Numeric('Total acquired',
        digits=(16, 2),
        states={
            'invisible': (
                ~(Eval('line_to_reform')) |
                (Eval('state').in_(['approved']))),
        }, depends=['state'], ), 'get_total_acquired')
    progress = fields.Function(fields.Numeric('Progress',
        states={
            'invisible': (
                ~(Eval('line_to_reform')) |
                (Eval('state').in_(['approved']))),
        }, depends=['state'], digits=(16, 2)), 'get_progress')
    observation = fields.Char('Observaciones', 
        states={
            'readonly': ~(Eval('state').in_(['draft', None]) &
                Eval('pac_budget')),
            'required': False
        }, depends=['state', 'pac_budget'])
    resolution = fields.Char('Numero de Resolución', states={
            'readonly': True,
        })

    @classmethod
    def __setup__(cls):
        super(PurchasePacLineReform, cls).__setup__()
        cls._error_messages.update({
            'no_delete': ('Yo can not delete reforms that are in approved '
                'state.'),
            'no_delete_pac_lines_with_purchased_products': ('You can not '
                'delete the pac "%(line_to_reform)s" line because it already '
                'has "%(total_acquired)s" purchased items.'),
            'modification_not_allowed': ('In the reform of the PAC '
                '"%(line_to_reform)s" line, the new quantity "(%(quantity)s)" '
                'can not be less than the number of items that have already '
                'been acquired "(%(total_acquired)s)".'),
            'no_modify_activity_budget': ('The activity budget line of this '
                'PAC Line can not be modified because its funds have already '
                'been used to buy "%(total_acquired)s" items.'),
            'invalid_product_type_selection': ('Error in the reform of type '
                '"%(reform_type)s" to the PAC Line of the budget "%(budget)s". '
                'The product type can not be "NO APPLY" when the regime type '
                'is "COMMON" or "SPECIAL".'),
            'exists_pending_reforms': ('Error reforming the PAC lines of '
                'budget "%(pac_budget)s". You can not have two reforms in '
                'draft state for the same line.'),
            'no_changes_on_reform': ('Error saving a reform to the PAC '
                '"%(pac)s". The reform of type "%(type_reform)s" and date '
                '"%(date)s" does not register changes with respect to the line '
                'that pretends to reform "%(line_to_reform)s".'),
            'reform_error_no_activity_budget': ("Error in the reform  of the "
                "PAC '%(pac)s'. In budget '%(budget)s', reform of type "
                "'%(type_reform)s' with date of '%(reform_date)s', no activity "
                "has been defined."),
            'reform_error_no_certified_pending': ("Error in the reform  of the "
                "PAC '%(pac)s'. In budget '%(budget)s', no pending values were "
                "found to be certified for the reform of type "
                "'%(type_reform)s' with date of '%(reform_date)s' and activity "
                "'%(activity)s'."),
            'reform_error_pac_reform_total_amount': (
                "Error in the reform  of the PAC '%(pac)s'. In the Budget "
                "'%(budget)s', the total value of the reform of type "
                "'%(type_reform)s' and date '%(reform_date)s' "
                "('$%(total_reform)s') must not be greater than the certified "
                "pending value of the activity budget line "
                "('$%(certified_pending)s')."),
            'reform_error_pac_budget_total_amount': (
                "Error in the reform  of the PAC '%(pac)s'. In the Budget "
                "'%(budget)s', the new total amount ('$%(total_pac_budget)s') "
                "emerged because of reform of type '%(type_reform)s' and date "
                "'%(reform_date)s' must not be greater than its assigned "
                "initial value ('$%(certified_pending)s')."),
            'no_sequence_reform_defined': ('Secuencia de reformas no configurada')
        })
        cls._transitions |= set((
            ('draft', 'approved'),
            ('approved', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['approved']),
                'depends': ['state'],
            },
            'approved': {
                'invisible': Eval('state').in_(['approved']),
                'depends': ['state'],
            }
        })
        cls._order = [
            ('state', 'DESC'),
            ('date', 'DESC'),
            ('type_reform', 'ASC'),
        ]

    @staticmethod
    def default_activity():
        print("todo")

    @staticmethod
    def default_resolution():
        return '/'
        
    @classmethod
    def delete(cls, reforms):
        for reform in reforms:
            if reform.state == 'approved':
                if reform.purchase_pac.state not in ['canceled']:
                    cls.raise_user_error('no_delete')
        super(PurchasePacLineReform, cls).delete(reforms)
    
    #@classmethod
    #def create(cls, vlist):
    #    print(cls)
    #    #return super(PurchasePacLineReform, cls).write(list, vals)
    
    @classmethod
    def validate(cls, reforms):
        for reform in reforms:
            # Product Type
            reform.check_product_type_validity()
            # Deletion Possibility
            if reform.line_to_reform and reform.type_reform in ['delete']:
                reform.check_deletion_possibility()

        # Do not add reforms if a reform with the same 'line_to_reform' are in
        # draft state
        i = 1
        for reform in reforms:
            for reform_aux in reforms[i:]:
                if reform_aux.state in ['draft']:
                    if reform_aux.pac_budget == reform.pac_budget:
                        if reform_aux.line_to_reform == reform.line_to_reform:
                            cls.raise_user_error('exists_pending_reforms', {
                                'pac_budget': reform.pac_budget.budget.code,
                            })
            i += 1
        # Check if exists changes
        for reform in reforms:
            if reform.type_reform in ['modify']:
                if not reform.have_changes():
                    cls.raise_user_error('no_changes_on_reform', {
                        'pac': reform.purchase_pac.rec_name,
                        'type_reform': reform.type_reform,
                        'line_to_reform': reform.line_to_reform.rec_name,
                        'date': reform.date,
                    })
        # Validate data fields and total amounts
        cls.check_information_validity(reforms)
        super(PurchasePacLineReform, cls).validate(reforms)

    @classmethod
    def check_information_validity(cls, reform, names=None):
        # Validate data fields and total amounts
        for reform in reform:
            # Reform Type
            if reform.type_reform in ['modify', 'add']:
                if not reform.budgets:
                    cls.raise_user_error(
                        'reform_error_no_activity_budget', {
                            'pac': reform.pac_budget.purchase_pac.rec_name,
                            'budget': reform.pac_budget.rec_name,
                            'type_reform': reform.type_reform,
                            'reform_date': reform.date,
                        })
                if not reform.certified_pending:
                    #TODO: add name of activities
                    cls.raise_user_error(
                        'reform_error_no_certified_pending', {
                            'pac': reform.pac_budget.purchase_pac.rec_name,
                            'budget': reform.pac_budget.rec_name,
                            'type_reform': reform.type_reform,
                            'reform_date': reform.date,
                            'activity': '',
                        })
                if reform.total_amount > reform.certified_pending:
                    cls.raise_user_error(
                        'reform_error_pac_reform_total_amount', {
                            'pac': reform.pac_budget.purchase_pac.rec_name,
                            'budget': reform.pac_budget.rec_name,
                            'type_reform': reform.type_reform,
                            'reform_date': reform.date,
                            'total_reform':
                                Decimal(str(
                                    reform.total_amount)).quantize(CENT),
                            'certified_pending': reform.certified_pending
                        })

                # Verify the new total amount of pac_budget emerged from this
                # reform
                #TODO:if budget_total_amount > \
                        #reform.pac_budget.budget_certified_pending:
                #TODO: change to save reform
                budget_total_amount = reform.pac_budget.total_amount
                if reform.type_reform in ['modify']:
                    budget_total_amount -= reform.line_to_reform.total_amount
                budget_total_amount += reform.total_amount
                if reform.total_amount > \
                        reform.certified_pending:
                    cls.raise_user_error(
                        'reform_error_pac_budget_total_amount', {
                            'pac': reform.pac_budget.purchase_pac.rec_name,
                            'budget': reform.pac_budget.rec_name,
                            'type_rProgresoeform': reform.type_reform,
                            'reform_date': reform.date,
                            'total_pac_budget':
                                Decimal(str(
                                    budget_total_amount)).quantize(CENT),
                            'certified_pending':
                                reform.pac_budget.budget_certified_pending
                        })

    def check_product_type_validity(self):
        if self.product_type and self.product_type in ['no-apply']:
            if self.regime_type and (
                        self.regime_type in ['common', 'special']):
                pass
                #TODO: determine why it is necessary?
                '''self.raise_user_error('invalid_product_type_selection', {
                    'type_reform': self.type_reform,
                    'budget': self.pac_budget.budget.code
                })'''

    @classmethod
    def generate_new_budgets(cls, pac_line, budgets):
        pool = Pool()
        PurchasePacLineBudget = pool.get('purchase.pac.line.budget')
        newBudgets = []
        for budget in budgets:
            newLineBud = PurchasePacLineBudget(
                    pac_line=pac_line,
                    budget=budget.budget,
                    amount=budget.amount,
                )
            newLineBud.catalog_budget = newLineBud.on_change_with_catalog_budget()
            newLineBud.on_change_budget()
            newLineBud.activity = newLineBud.on_change_with_activity()
            newLineBud.certified_pending = newLineBud.on_change_with_certified_pending()
            newBudgets.append(newLineBud)
        return newBudgets 

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, reforms):
        pool = Pool()
        PurchasePacLine = pool.get('purchase.pac.line')
        PurchasePacLineBudget = pool.get('purchase.pac.line.budget')
        PurchasePacLineReform = pool.get('purchase.pac.line.reform')
        Sequence = pool.get('ir.sequence')
        Config = pool.get('purchase.configuration')
        config = Config(1)
        to_save_lines = []
        to_save_reforms = []
        for reform in reforms:
            reform.date = date.today()
            if reform.resolution == '/':
                if config.reform_sequence:
                    reform.resolution = Sequence.get_id(
                            config.reform_sequence.id)
                else:
                    cls.raise_user_error('no_sequence_reform_defined')

            if reform.type_reform == 'modify':
                # Copy previous information on a new pac line
                previous_line = cls.copy_old_pac_line_fields(reform)
                # Update the original pac_line with new changes keeping its id
                modified_line = cls.update_pac_line_fields(reform)
                if previous_line and modified_line:
                    # ADD PAC LINES DATA
                    #reform.line_to_reform = previous_line
                    modified_line.line_that_was_reformed = previous_line
                    previous_line.line_emerged_from_reform = modified_line
                    # ADD REFORM DATA
                    modified_line.reform = reform
                    previous_line.reform = reform
                    reform.new_line = modified_line
                    # SAVE LINES
                    PurchasePacLine.save([modified_line])
                    previous_line.budgets = PurchasePacLineBudget.copy(reform.line_to_reform.budgets)
                    PurchasePacLineReform.save([reform])
                    # Set reform_state in order to avoid domains of
                    # reform.line_to_reform
                    previous_line.reform_state = 'modified'
                    PurchasePacLine.save([previous_line])
                    #remove budgets before update with reform
                    PurchasePacLineBudget.delete(modified_line.budgets)
                    #create new budgets with reform info
                    PurchasePacLineBudget.save(cls.generate_new_budgets(modified_line, reform.budgets))
            elif reform.type_reform == 'add':
                new_line = cls.generate_new_pac_line(reform)
                if new_line:
                    # ADD REFORM DATA
                    new_line.reform = reform
                    reform.new_line = new_line
                    new_line.reform_state = 'added'
                    # SAVE LINES
                    to_save_lines.append(new_line)
                    to_save_reforms.append(reform)
            elif reform.type_reform == 'delete':
                # ADD PAC LINES DATA
                old_line = reform.line_to_reform
                old_line.reform_state = 'deleted'
                # ADD REFORM DATA
                old_line.reform = reform
                # SAVE LINES
                to_save_lines.append(old_line)
                to_save_reforms.append(reform)
        PurchasePacLine.save(to_save_lines)
        PurchasePacLineReform.save(to_save_reforms)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, reforms):
        pass

    @staticmethod
    def default_responsable():
        return Transaction().context.get('employee')

    @staticmethod
    def default_date():
        return date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('pac_budget')
    def on_change_with_purchase_pac(self, name=None):
        if self.pac_budget and self.pac_budget.purchase_pac:
            return self.pac_budget.purchase_pac.id
        return None

    @fields.depends('quantity', 'unit_price_without_iva')
    def on_change_with_total_amount(self, name=None):
        if self.quantity and self.unit_price_without_iva:
            q = Decimal(str(self.quantity)).quantize(CENT)
            u = Decimal(str(self.unit_price_without_iva)).quantize(CENT)
            return q * u
        return None

    @fields.depends('purchase_type', 'electronic_catalog')
    def on_change_purchase_type(self, name=None):
        if self.purchase_type:
            if self.purchase_type not in ['service', 'property']:
                self.product_type = 'no-apply'
                self.electronic_catalog = False
                self.regime_type = 'no-apply'

    @fields.depends('product_type', 'electronic_catalog',)
    def on_change_product_type(self, name=None):
        if self.product_type:
            if self.product_type not in ['no-normalized']:
                self.electronic_catalog = None

    @fields.depends('regime_type', 'bid_founds', 'product_type')
    def on_change_regime_type(self, name=None):
        if self.regime_type:
            if self.regime_type in ['special', 'no-apply']:
                self.bid_founds = None
                self.project_code = None
                self.operation_code = None
                if self.regime_type in ['special']:
                    self.product_type = 'no-apply'
                else:
                    self.product_type = 'no-apply'

    # TODO: check on_change_product_type redefinition
    # @fields.depends('product_type', 'regime_type', 'type_reform', 'pac_budget')  # noqa
    # def on_change_product_type(self, name=None):
    #     self.check_product_type_validity()
    '''
    @fields.depends('activity_budget', 'activity', 'certified_pending',
        'total_acquired', 'line_to_reform')
    def on_change_activity_budget(self, names=None):
        PublicBudgetCard = Pool().get('public.budget.card')
        if self.activity_budget:
            if self.total_acquired and self.total_acquired > 0:
                aux_activity_budget = self.line_to_reform.activity_budget
                if self.activity_budget != aux_activity_budget:
                    self.raise_user_error('no_modify_activity_budget', {
                        'total_acquired': self.total_acquired
                    })
            if self.activity_budget.activity:
                self.activity = self.activity_budget.activity
                cards = PublicBudgetCard.search([
                    ('activity', '=', self.activity),
                    ('code', '=', self.activity_budget.code)
                ])
                if cards:
                    self.certified_pending = cards[0].certified_pending
        else:
            self.activity = None
            self.certified_pending = None
    '''

    @fields.depends('budgets', 'activity', 'certified_pending')
    def on_change_budgets(self, names=None):
        #TODO: WIP - DAULE
        PublicBudgetCard = Pool().get('public.budget.card')
        pending = Decimal("0.0")
        if self.budgets:
            budgets = []
            for budget in self.budgets:
                if budget.budget:
                    budgets.append(budget.budget.id)
            cards = PublicBudgetCard.search([
                ('id', 'in', budgets)
            ])
            if cards:
                pending = Decimal('0.0')
                for card in cards:
                    pending += card.certified_pending
        self.certified_pending = pending

    @fields.depends('quantity', 'line_to_reform', 'type_reform')
    def on_change_quantity(self, name=None):
        if self.quantity and self.line_to_reform:
            if self.type_reform in ['modify']:
                self.check_modification_possibility()

    @fields.depends('line_to_reform', 'type_reform')
    def on_change_line_to_reform(self, name=None):
        if self.line_to_reform:
            if self.type_reform in ['delete']:
                self.check_deletion_possibility()
            else:
                self.check_if_do_not_exists_pending_reforms()
            self.fill_body_fields()
        else:
            self.clear_body_pac_lines()

    @fields.depends('type_reform', 'line_to_reform')
    def on_change_type_reform(self, name=None):
        if self.type_reform:
            if self.type_reform in ['add']:
                self.clear_body_pac_lines()
                self.line_to_reform = None
            if self.line_to_reform:
                self.fill_body_fields()
                if self.type_reform in ['delete']:
                    self.check_deletion_possibility()

    @fields.depends('quantity', 'total_acquired')
    def on_change_with_progress(self, names=None):
        if self.quantity and self.total_acquired:
            return self.total_acquired / self.quantity
        return 0

    @fields.depends('budgets')
    def on_change_with_certified_pending(self, name=None):
        PublicBudgetCard = Pool().get('public.budget.card')
        result = Decimal(0.0)
        budgets = []
        for budget in self.budgets:
            if budget.budget:
                budgets.append(budget.budget.id)
        cards = PublicBudgetCard.search([
            ('id', 'in', budgets)
        ])
        if cards:
            for card in cards:
                result += card.certified_pending
        return result


    @classmethod
    def get_purchase_pac(cls, pac_reforms, names=None):
        result = defaultdict(lambda: [])
        for pac_reform in pac_reforms:
            if pac_reform.pac_budget and pac_reform.pac_budget.purchase_pac:
                result[pac_reform.id] = pac_reform.pac_budget.purchase_pac.id
        return {
            'purchase_pac': result
        }

    @classmethod
    def get_total_acquired(cls, pac_reforms, names=None):
        result = defaultdict(lambda: None)
        for pac_reform in pac_reforms:
            if pac_reform.line_to_reform:
                result[pac_reform.id] = pac_reform.line_to_reform.total_acquired
        return {
            'total_acquired': result
        }

    @classmethod
    def get_progress(cls, pac_reforms, names=None):
        result = defaultdict(lambda: None)
        for pac_reform in pac_reforms:
            if pac_reform.quantity and pac_reform.total_acquired:
                result[pac_reform.id] = \
                    pac_reform.total_acquired / pac_reform.quantity
        return {
            'progress': result
        }

    @fields.depends('line_to_reform')
    def check_if_do_not_exists_pending_reforms(self):
        if self.line_to_reform:
            pool = Pool()
            PurchasePacLineReform = pool.get('purchase.pac.line.reform')
            pending_reforms = PurchasePacLineReform.search([
                ('state', '=', 'draft'),
                ('line_to_reform', '=', self.line_to_reform)
            ])
            if len(pending_reforms) > 0:
                self.raise_user_error('exists_pending_reforms', {
                    'pac_budget': pending_reforms[0].pac_budget.budget.code,
                })

    @fields.depends('line_to_reform')
    def clear_body_pac_lines(self):
        PurchasePacLine = Pool().get('purchase.pac.line')
        attrs = get_util_fields(PurchasePacLine, self.get_excluded_fields())
        for attr in attrs:
            setattr(self, attr, None)

    def fill_body_fields(self):
        PurchasePacLineBudget = Pool().get('purchase.pac.line.reform.budget')
        to_save_budgets = []
        for budget in self.line_to_reform.budgets:
            new_pac_line_budget = PurchasePacLineBudget(
                activity=budget.activity,
                budget=budget.budget,
                amount=budget.amount,
                total_budguet=budget.total_budguet
            )
            new_pac_line_budget.on_change_with_activity()
            new_pac_line_budget.on_change_budget()
            to_save_budgets.append(new_pac_line_budget)
        self.budgets = to_save_budgets
        attrs = get_util_fields_values(
            self.line_to_reform, self.get_excluded_fields())
        for key, value in attrs.items():
            setattr(self, key, value)
        setattr(self.line_to_reform, 'reform_state', 'pending')

    def check_modification_possibility(self):
        if self.quantity < self.line_to_reform.total_acquired:
            self.raise_user_error('modification_not_allowed', {
                'line_to_reform': self.line_to_reform.rec_name,
                'total_acquired': self.line_to_reform.total_acquired,
                'quantity': self.quantity
            })

    def check_deletion_possibility(self):
        if self.line_to_reform.total_acquired > 0:
            self.raise_user_error(
                'no_delete_pac_lines_with_purchased_products', {
                    'line_to_reform': self.line_to_reform.rec_name,
                    'total_acquired':
                        self.line_to_reform.total_acquired
                }
            )

    def have_changes(self):
        attrs = get_util_fields_values(
            self.line_to_reform, self.get_excluded_fields())
        for key, value in attrs.items():
            rvalue = getattr(self, key, None)
            if rvalue != value:
                return True
        return False

    @classmethod
    def generate_new_pac_line(cls, reform):
        new_line = None
        if reform:
            PurchasePacLineBudget = Pool().get('purchase.pac.line.budget')
            PurchasePacLine = Pool().get('purchase.pac.line')
            new_line = PurchasePacLine()
            new_line.pac_budget = reform.pac_budget
            new_line.purchase_pac = reform.purchase_pac
            new_line.state = 'added'
            fields = get_util_fields(
                PurchasePacLine, reform.get_excluded_fields())
            for field in fields:
                setattr(new_line, field, getattr(reform, field, None))
            to_save_budgets = []
            for budget in reform.budgets:
                new_pac_line_budget = PurchasePacLineBudget(
                    pac_line=new_line,
                    budget=budget.budget,
                    amount=budget.amount
                )
                new_pac_line_budget.on_change_with_activity()
                new_pac_line_budget.on_change_budget()
                to_save_budgets.append(new_pac_line_budget)
            new_line.budgets = to_save_budgets
        return new_line

    @classmethod
    def update_pac_line_fields(cls, reform):
        if reform:
            pac_line = reform.line_to_reform
            if pac_line:
                PurchasePacLine = Pool().get('purchase.pac.line')
                pac_line.pac_budget = reform.pac_budget
                pac_line.purchase_pac = reform.purchase_pac
                fields = get_util_fields(
                    PurchasePacLine, reform.get_excluded_fields())
                for field in fields:
                    setattr(pac_line, field, getattr(reform, field, None))

        return pac_line

    @classmethod
    def set_new_departments(cls, pac_line, reform):
        if reform and pac_line:
            pool = Pool()
            PacLineDepartment = pool.get('purchase.pac.line.department.company')
            to_delete = []
            to_save = []
            no_consider = []
            reform_departments = [d.department for d in reform.departments]
            if reform.line_to_reform and reform.line_to_reform.departments:
                for pac_line_department in reform.line_to_reform.departments:
                    if pac_line_department.department not in reform_departments:
                        to_delete.append(pac_line_department)
                    else:
                        no_consider.append(pac_line_department.department)
            for department in reform_departments:
                if department not in no_consider:
                    pac_line_department = PacLineDepartment()
                    pac_line_department.pac_line = pac_line
                    pac_line_department.department = department
                    to_save.append(pac_line_department)
            PacLineDepartment.delete(to_delete)
            if not pac_line.departments:
                pac_line.departments = []
            pac_line.departments.append(to_save)

    @classmethod
    def copy_old_pac_line_fields(cls, reform):
        new_line = None
        if reform:
            old_pac_line = reform.line_to_reform
            if old_pac_line:
                PurchasePacLine = Pool().get('purchase.pac.line')
                new_line = PurchasePacLine()
                new_line.pac_budget = old_pac_line.pac_budget
                new_line.purchase_pac = old_pac_line.purchase_pac
                new_line.reform_state = old_pac_line.reform_state
                fields = get_util_fields(
                    PurchasePacLine, reform.get_excluded_fields())
                for field in fields:
                    setattr(new_line, field, getattr(old_pac_line, field, None))
        return new_line

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('cpc.code',) + tuple(clause[1:]),
            ('description',) + tuple(clause[1:]),
            ('product_type',) + tuple(clause[1:]),
            ('purchase_type',) + tuple(clause[1:]),
            ('pac_budget.budget.code',) + tuple(clause[1:]),
            ('pac_budget.purchase_pac.fiscal_year.name',) + tuple(clause[1:]),
            ('suggested_procedure.type_contract',) + tuple(clause[1:]),
        ]
        return domain

    def get_excluded_fields(self):
        # Fields of purchase.pac.line that are not considered on a reform
        return [
            'id', 'rec_name', 'create_uid', 'write_uid', 'create_date',
            'write_date', 'purchase_pac', 'pac_budget', 'pac_state', 'reform',
            'reform_state', 'line_emerged_from_reform', 
            'budgets', 'state'
        ]


class PurchasePacLineConsulting(ModelSQL, ModelView):
    'Purchase Pac Lines Consulting'
    __name__ = 'purchase.pac.line.consulting'

    purchase_pac = fields.Many2One('purchase.pac', 'Purchase PAC',
        readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    fiscal_year = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        readonly=True)
    poa = fields.Many2One('public.planning.unit', 'POA', readonly=True)
    pac_budget = fields.Many2One('purchase.pac.budget', 'PAC Budget',
        readonly=True)
    budget = fields.Many2One('public.budget', 'Budget', readonly=True)
    budget_certified_pending = fields.Numeric('Budget Certified Pending',
        readonly=True)
    pac_budget_total_amount = fields.Numeric('PAC Budget Total Amount',
        readonly=True)
    pac_line = fields.Many2One('purchase.pac.line', 'PAC Line', readonly=True)
    activity_budget = fields.Many2One('public.budget', 'Activity Budget',
        readonly=True)
    activity = fields.Many2One('public.planning.unit', 'Activity',
        readonly=True)
    certified_pending = fields.Numeric('Certified Pending', readonly=True)
    cpc = fields.Many2One('purchase.pac.category', 'CPC', readonly=True)
    pac_line_total_amount = fields.Numeric('PAC Line Total Amount',
        readonly=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('approved', 'Approved'),
            ('canceled', 'Canceled'),
        ], 'State', readonly=True)
    state_translated = state.translated('state')

    @classmethod
    def table_query(cls):
        pool = Pool()
        PublicBudgetCard = pool.get('public.budget.card')
        query_budget = PublicBudgetCard.query_get()
        query_activity_budget = PublicBudgetCard.query_get()
        PurchasePac = pool.get('purchase.pac')
        purchase_pac = PurchasePac.__table__()
        PurchasePacBudget = pool.get('purchase.pac.budget')
        purchase_pac_budget = PurchasePacBudget.__table__()
        PublicBudget = pool.get('public.budget')
        public_budget = PublicBudget.__table__()
        ActivityPublicBudget = pool.get('public.budget')
        activity_public_budget = ActivityPublicBudget.__table__()
        PurchasePacLine = pool.get('purchase.pac.line')
        purchase_pac_line = PurchasePacLine.__table__()

        query_budget_amounts = (
            purchase_pac_line.join(purchase_pac_budget,
                condition=purchase_pac_line.pac_budget == purchase_pac_budget.id
            ).select(
                purchase_pac_budget.id,
                Sum(purchase_pac_line.quantity *
                    purchase_pac_line.unit_price_without_iva).as_(
                        'total_amount'),
                where=(
                    (purchase_pac_line.reform_state == 'added') |
                    (purchase_pac_line.reform_state == Null)),
                group_by=purchase_pac_budget.id,
            )
        )

        query_pac = (
            purchase_pac.join(purchase_pac_budget,
                condition=(
                    purchase_pac_budget.purchase_pac == purchase_pac.id)
            ).join(public_budget,
                condition=(
                    purchase_pac_budget.budget == public_budget.id)
            ).join(purchase_pac_line,
                condition=(
                    purchase_pac_line.pac_budget == purchase_pac_budget.id
                )
            ).join(activity_public_budget,
                condition=(purchase_pac_line.activity_budget ==
                    activity_public_budget.id)
            ).join(query_budget,
                condition=query_budget.id == purchase_pac_budget.budget
            ).join(query_activity_budget,
                condition=(query_activity_budget.id ==
                    purchase_pac_line.activity_budget)
            ).join(query_budget_amounts,
                condition=purchase_pac_budget.id == query_budget_amounts.id
            ).select(
                purchase_pac_line.id,
                Literal(0).as_('create_uid'),
                CurrentTimestamp().as_('create_date'),
                Literal(0).as_('write_uid'),
                CurrentTimestamp().as_('write_date'),
                purchase_pac.id.as_('purchase_pac'),
                purchase_pac.company,
                purchase_pac.fiscal_year,
                purchase_pac.poa,
                purchase_pac.state,
                purchase_pac_budget.id.as_('pac_budget'),
                public_budget.id.as_('budget'),
                purchase_pac_line.id.as_('pac_line'),
                purchase_pac_line.cpc,
                purchase_pac_line.activity_budget,
                activity_public_budget.activity,
                (purchase_pac_line.quantity *
                 purchase_pac_line.unit_price_without_iva).as_(
                    'pac_line_total_amount'),
                query_budget.certified_pending.as_('budget_certified_pending'),
                query_activity_budget.certified_pending,
                query_budget_amounts.total_amount.as_(
                    'pac_budget_total_amount'),
                where=(
                    (purchase_pac_line.reform_state == 'added') |
                    (purchase_pac_line.reform_state == Null)),
            )
        )
        return query_pac


class PurchasePacLineReformConsulting(ModelSQL, ModelView):
    'Purchase Pac Lines Consulting'
    __name__ = 'purchase.pac.line.reform.consulting'

    purchase_pac = fields.Many2One('purchase.pac', 'Purchase PAC',
        readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    fiscal_year = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        readonly=True)
    poa = fields.Many2One('public.planning.unit', 'POA', readonly=True)
    pac_budget = fields.Many2One('purchase.pac.budget', 'PAC Budget',
        readonly=True)
    budget = fields.Many2One('public.budget', 'Budget', readonly=True)
    budget_certified_pending = fields.Numeric('Budget Certified Pending',
        readonly=True)
    pac_budget_total_amount = fields.Numeric('PAC Budget Total Amount',
        readonly=True)
    pac_line_reform = fields.Many2One('purchase.pac.line.reform', 'PAC Reform',
        readonly=True)
    type_reform = fields.Selection(
        [
            ('modify', 'Modify'),
            ('delete', 'Delete'),
            ('add', 'Add'),
        ], 'Type Reform', readonly=True)
    type_reform_translated = type_reform.translated('type_reform')
    line_to_reform = fields.Many2One('purchase.pac.line', 'Line to reform',
        readonly=True)
    new_line = fields.Many2One('purchase.pac.line', 'New line', readonly=True)
    date = fields.Date('Date', readonly=True)
    cpc = fields.Many2One('purchase.pac.category', 'CPC', readonly=True)
    activity_budget = fields.Many2One('public.budget', 'Activity Budget',
        readonly=True)
    activity = fields.Many2One('public.planning.unit', 'Activity',
        readonly=True)
    certified_pending = fields.Numeric('Certified Pending', readonly=True)
    pac_reform_total_amount = fields.Numeric('PAC Line Total Amount',
        readonly=True)
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('approved', 'Approved'),
        ], 'State', readonly=True)
    state_translated = state.translated('state')

    @classmethod
    def table_query(cls):
        pool = Pool()
        PublicBudgetCard = pool.get('public.budget.card')
        query_budget = PublicBudgetCard.query_get()
        query_activity_budget = PublicBudgetCard.query_get()
        PurchasePac = pool.get('purchase.pac')
        purchase_pac = PurchasePac.__table__()
        PurchasePacBudget = pool.get('purchase.pac.budget')
        purchase_pac_budget = PurchasePacBudget.__table__()
        PublicBudget = pool.get('public.budget')
        public_budget = PublicBudget.__table__()
        ActivityPublicBudget = pool.get('public.budget')
        activity_public_budget = ActivityPublicBudget.__table__()
        PurchasePacLine = pool.get('purchase.pac.line')
        purchase_pac_line = PurchasePacLine.__table__()
        PurchasePacLineReform = pool.get('purchase.pac.line.reform')
        purchase_pac_line_reform = PurchasePacLineReform.__table__()

        query_budget_amounts = (
            purchase_pac_line.join(purchase_pac_budget,
                condition=purchase_pac_line.pac_budget == purchase_pac_budget.id
            ).select(
                purchase_pac_budget.id,
                Sum(purchase_pac_line.quantity *
                    purchase_pac_line.unit_price_without_iva).as_(
                    'total_amount'),
                where=(
                    (purchase_pac_line.reform_state == 'added') |
                    (purchase_pac_line.reform_state == Null)),
                group_by=purchase_pac_budget.id,
            )
        )

        query_pac = (
            purchase_pac.join(purchase_pac_budget,
                condition=(
                    purchase_pac_budget.purchase_pac == purchase_pac.id)
            ).join(public_budget,
                condition=(
                    purchase_pac_budget.budget == public_budget.id)
            ).join(purchase_pac_line_reform,
                condition=(purchase_pac_line_reform.pac_budget ==
                    purchase_pac_budget.id)
            ).join(activity_public_budget,
                condition=(purchase_pac_line_reform.activity_budget ==
                    activity_public_budget.id)
            ).join(query_budget,
                condition=(query_budget.id == purchase_pac_budget.budget)
            ).join(query_activity_budget,
                condition=(query_activity_budget.id ==
                    purchase_pac_line_reform.activity_budget)
            ).join(query_budget_amounts,
                condition=(
                    purchase_pac_budget.id == query_budget_amounts.id
                )
            ).select(
                purchase_pac_line_reform.id,
                Literal(0).as_('create_uid'),
                CurrentTimestamp().as_('create_date'),
                Literal(0).as_('write_uid'),
                CurrentTimestamp().as_('write_date'),
                purchase_pac.id.as_('purchase_pac'),
                purchase_pac.company,
                purchase_pac.fiscal_year,
                purchase_pac.poa,
                purchase_pac_budget.id.as_('pac_budget'),
                public_budget.id.as_('budget'),
                query_budget.certified_pending.as_('budget_certified_pending'),
                purchase_pac_line_reform.id.as_('pac_line_reform'),
                purchase_pac_line_reform.type_reform,
                purchase_pac_line_reform.line_to_reform,
                purchase_pac_line_reform.new_line,
                purchase_pac_line_reform.date,
                purchase_pac_line_reform.cpc,
                purchase_pac_line_reform.state,
                purchase_pac_line_reform.activity_budget,
                activity_public_budget.activity,
                query_activity_budget.certified_pending,
                (purchase_pac_line_reform.quantity *
                 purchase_pac_line_reform.unit_price_without_iva).as_(
                    'pac_reform_total_amount'),
                query_budget_amounts.total_amount.as_(
                    'pac_budget_total_amount')
            )
        )
        return query_pac


class FiscalYear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'

    description = fields.Text('Description')
    lines = fields.One2Many('purchase.pac.period', 'fiscalyear', 'Lines Period',
        states={
            'readonly': ~Eval('state').in_(['draft', None])
        }, depends=['state'])

class PurchasePacPeriod(ModelSQL, ModelView):
    'Purchase Pac Period'
    __name__ = 'purchase.pac.period'

    name = fields.Char('Name',
        states={
            'readonly': Eval('fiscalyear_state').in_(['open']),
        }, depends=['fiscalyear_state'], required=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        ondelete='RESTRICT', required=True,
        states={
            'readonly': Eval('fiscalyear_state').in_(['open']),
            'required': True,
            },
        depends=['fiscalyear_state'])
    fiscalyear_state = fields.Function(
        fields.Selection([
            ('open', 'Open'),
            ('close', 'Close'),
            ('locked', 'Locked'),
            ], 'Account Fiscal Year State'), 'on_change_with_fiscalyear_state')
    start_date = fields.Date('Start Date',
         domain=[('start_date', '<=', Eval('end_date', None))],
        states={
            'readonly': Eval('fiscalyear_state').in_(['open']),
            'required': True,
            },
        depends=['fiscalyear_state', 'end_date'])
    end_date = fields.Date('End Date',
          domain=[('end_date', '>=', Eval('start_date', None))],
        states={
            'readonly': Eval('fiscalyear_state').in_(['open']),
            'required': True,
            },
        depends=['fiscalyear_state', 'start_date'])

    @classmethod
    def __setup__(cls):
        super(PurchasePacPeriod, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('key_unique', Unique(table, table.fiscalyear, table.name),
                'Fiscal year & period name must be unique'),
            ]
        cls._error_messages.update({
                'years_error': (
                    'Date must has inside Fiscal Year'
                    ),
                'date_error': (
                    'Overlapping dates, review of the year: "%(year)s"'
                    'between the dates: "%(date1)s"''y "%(date2)s"'
                    ),
                'date_error_one': (
                    'Overlapping dates, review of the period name: "%(per)s"'
                    ', problems with: "%(per2)s"'
                    ),
                })

    @classmethod
    def validate(cls, periods):
        super(PurchasePacPeriod, cls).validate(periods)
        for period in periods:
            period.check_dates()
            period.check_fiscalyear_dates()

    @fields.depends('fiscalyear', '_parent_fiscalyear.state')
    def on_change_with_fiscalyear_state(self, name=None):
        if self.fiscalyear:
            print(("**** ", self.fiscalyear.state))
            self.fiscalyear_state = self.fiscalyear.state
            return self.fiscalyear.state

    def check_dates(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        cursor.execute(*table.select(table.id,
                where=(((table.start_date <= self.start_date)
                        & (table.end_date >= self.start_date))
                    | ((table.start_date <= self.end_date)
                        & (table.end_date >= self.end_date))
                    | ((table.start_date >= self.start_date)
                        & (table.end_date <= self.end_date)))
                & (table.fiscalyear == self.fiscalyear.id)
                & (table.id != self.id)))
        period_id = cursor.fetchone()
        if period_id:
            overlapping_period = self.__class__(period_id[0])
            self.raise_user_error('date_error_one', {
                    'per': self.name,
                    'per2': overlapping_period.name,
                    })

    def check_fiscalyear_dates(self):
        if (self.start_date < self.fiscalyear.start_date
                or self.end_date > self.fiscalyear.end_date):
            self.raise_user_error('fiscalyear_dates', (self.name,))


class PurchasePacLinePeriod(ModelSQL, ModelView):
    'Purchase Pac Line Period'
    __name__ = 'purchase.pac.line.period'

    line = fields.Many2One('purchase.pac.line', 'Line', required=True,
        ondelete='CASCADE')
    period = fields.Many2One('purchase.pac.period', 'Period', required=True,
        ondelete='RESTRICT')
