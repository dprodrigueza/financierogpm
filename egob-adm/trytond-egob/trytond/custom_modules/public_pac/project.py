from decimal import Decimal
from collections import defaultdict

from trytond.model import fields
from trytond.pool import Pool, PoolMeta

__all__ = ['Work']


class Work(metaclass=PoolMeta):
    __name__ = 'project.work'

    purchases = fields.Function(
        fields.One2Many('purchase.purchase', None, 'Ordenes de Compra'), 
        'get_purchase_info'
    )
    purchase_total = fields.Function(
        fields.Numeric('Total Compra', digits=(16, 2)),
        'get_purchase_info'
    )

    @classmethod
    def get_purchase_info(cls, works, names=None):
        purchase = defaultdict(lambda: None)
        total = defaultdict(lambda: Decimal('0.0'))
        for work in works:
            if work.ppu_project and work.budgets:
                pool = Pool()
                Purchase = pool.get('purchase.purchase')
                data = {
                    'program': {
                        'ids': [p.id for p in work.ppu_project],
                        'budgets': [b.id for b in work.budgets]
                    }
                }
                cursor_dict = Purchase.get_program_by_budgets(data)
                purchases = []
                amount = Decimal('0.0')
                for row in cursor_dict:
                    amount += row['amount']
                    if row['purchase'] not in purchases:
                        purchases.append(row['purchase'])
                if purchases:
                    purchase[work.id] = purchases
                    total[work.id] = amount
        return {
            'purchases': purchase,
            'purchase_total': total
        }
