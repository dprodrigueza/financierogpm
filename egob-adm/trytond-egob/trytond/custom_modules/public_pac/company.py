from trytond.model import ModelView, ModelSQL, fields, sequence_ordered
from trytond.pool import PoolMeta


__all__ = [
    'Department', 'DepartmentRequirementLocation',
    'DepartmentApproverPurchaseRequirement'
]


class Department(metaclass=PoolMeta):
    __name__ = 'company.department'

    approver_purchase_requirements = fields.Many2Many(
        'company.deparment.approver.purchase.requirement', 'department',
        'employee', 'Aprobadores de solicitudes de despacho')
    locations = fields.One2Many(
        'company.department.requirement.location', 'department', 'Ubicaciones',
        help='Ubicaciones para solicitudes de despacho')


class DepartmentRequirementLocation(sequence_ordered(), ModelSQL, ModelView):
    'Locations for Requirements'
    __name__ = 'company.department.requirement.location'

    department = fields.Many2One('company.department', 'Departamento',
        required=True, ondelete='CASCADE')
    location = fields.Many2One('stock.location', 'Ubicación', required=True,
        domain=[
            ('type', '=', 'storage'),
        ])


class DepartmentApproverPurchaseRequirement(ModelSQL, ModelView):
    'Department Approver Purchase Requirement'
    __name__ = 'company.deparment.approver.purchase.requirement'

    department = fields.Many2One(
        'company.department', 'Departamento', required=True)
    employee = fields.Many2One(
        'company.employee', 'Empleado', required=True)
