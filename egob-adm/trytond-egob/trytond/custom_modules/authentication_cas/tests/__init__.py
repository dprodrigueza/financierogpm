try:
    from trytond.modules.authentication_cas.tests.test_authentication_cas import suite
except ImportError:
    from .test_authentication_cas import suite

__all__ = ['suite']
