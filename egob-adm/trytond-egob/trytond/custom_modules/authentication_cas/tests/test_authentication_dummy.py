import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class AuthenticationCasTestCase(ModuleTestCase):
    'Test Authentication Cas module'
    module = 'authentication_cas'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            AuthenticationCasTestCase))
    return suite
