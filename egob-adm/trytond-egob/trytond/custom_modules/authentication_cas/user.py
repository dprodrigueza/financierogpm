from trytond.pool import PoolMeta
from trytond.config import config

import cas


__all__ = ['User']


class User:
    __metaclass__ = PoolMeta
    __name__ = 'res.user'

    @classmethod
    def _login_cas(cls, login, parameters):
        cas_url = config.get('siim', 'cas_url')
        cas_url_service = config.get('siim', 'cas_url_service')
        if cas_url:
            cas_client = cas.CASClientWithSAMLV1(
                server_url=cas_url, service_url=cas_url_service, renew=True)
            try:
                username, user_data, c  = cas_client.verify_ticket(login)
                if username:
                    name = user_data.get('sn', '') + ' ' + user_data.get(
                        'givenName', '')
                    mail = user_data.get('mail', '')
                    user_id, _ = cls._get_login(username)
                    if user_id:
                        user = cls(user_id)
                        user.name = name
                        user.email = mail
                        user.save()
                        return user_id
                    elif config.getboolean('siim', 'cas_create_user'):
                        user, = cls.create([{
                            'name': name,
                            'login': username,
                            'email': mail,
                        }])
                        return user.id
            except Exception:
                pass
        return
