from trytond import backend
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields, \
    ValueMixin
from trytond.modules.company.model import (CompanyMultiValueMixin,
                                           CompanyValueMixin)
from trytond.tools.multivalue import migrate_property
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['Configuration', 'ConfigurationSequence','MaintenanceSendEmail']

to_location = fields.Many2One(
    'stock.location', "Egresos mantenimiento", states={
        'required': True
    },
    domain=[
        ('type', '=', 'lost_found'),
        ])
shipment_reason = fields.Many2One(
    'stock.shipment.reason', "Razon de albarán para mantenimiento",
    required=True)


from_location = fields.Many2One(
    'stock.location', "Desde almacenamiento",
    states={
      #  'required': True
    },
    domain=[
        ('type', '=','storage'),
    ])


class Configuration(
    ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Parking Place Configuration'
    __name__ = 'parking.place.configuration'
    mobilization_request_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia de solicitud de movilización', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'mobilization_request'),
        ]))
    mobilization_order_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia orden de movilización', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'mobilization_order'),
        ]))
    accident_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia accidente', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'accident'),
        ]))
    maintenance_vehicle_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia mantenimiento', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'maintenance'),
        ]))
    payment_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia Liquidación', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'payment'),
        ]))
    mount_tires_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia de montaje de neumáticos', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'mount_tires'),
        ]))
    disassembly_tires_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia desmontaje neumáticos', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'disassembly_tires'),
        ]))

    actualization_tires_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia actualizacíon neumáticos', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'actualization_tires'),
        ]))

    retreat_tires_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia Reencauche neumáticos', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'retreat_tires'),
        ]))

    to_location = fields.MultiValue(to_location)
    from_location = fields.MultiValue(from_location)
    shipment_reason = fields.MultiValue(shipment_reason)

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['mobilization_request_sequence',
                     'mobilization_order_sequence',
                     'accident_sequence',
                     'maintenance_vehicle_sequence',
                     'payment_sequence',
                     'mount_tires_sequence',
                     'disassembly_tires_sequence',
                     'actualization_tires_sequence',
                     'retreat_tires_sequence',

                     ]:
            return pool.get('parking.place.configuration.sequence')
        if field in ['to_location', 'shipment_reason','from_location']:
            return pool.get('parking.place.configuration.location')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_mobilization_request_sequence(cls, **pattern):
        return cls.multivalue_model('mobilization_request_sequence'
                                    ).default_mobilization_request_sequence()

    @classmethod
    def default_mobilization_order_sequence(cls, **pattern):
        return cls.multivalue_model('mobilization_order_sequence'
                                    ).default_mobilization_order_sequence()

    @classmethod
    def default_accident_sequence(cls, **pattern):
        return cls.multivalue_model('accident_sequence'
                                    ).default_accident_sequence()

    @classmethod
    def default_maintenance_vehicle_sequence(cls, **pattern):
        return cls.multivalue_model('maintenance_vehicle_sequence'
                                    ).default_maintenance_vehicle_sequence()

    @classmethod
    def default_payment_sequence(cls, **pattern):
        return cls.multivalue_model('payment_sequence'
                                    ).default_payment_sequence()

    @classmethod
    def default_mount_tires_sequence(cls, **pattern):
        return cls.multivalue_model('mount_tires_sequence'
                                    ).default_mount_tires_sequence()
    @classmethod
    def default_disassembly_tires_sequence(cls, **pattern):
        return cls.multivalue_model('disassembly_tires_sequence'
                                    ).default_disassembly_tires_sequence()

    @classmethod
    def default_actualization_tires_sequence(cls, **pattern):
        return cls.multivalue_model('actualization_tires_sequence'
                                    ).default_actualization_tires_sequence()

    @classmethod
    def default_retreat_tires_sequence(cls, **pattern):
        return cls.multivalue_model('retreat_tires_sequence'
                                    ).default_retreat_tires_sequence()

    @classmethod
    def default_to_location(cls, **pattern):
        pool = Pool()
        StockLocation = pool.get('stock.location')
        to_location = StockLocation.search(["code", "=", "EGM"])[0]
        return to_location.id

    @classmethod
    def default_from_location(cls, **pattern):
        pool = Pool()
        StockLocation = pool.get('stock.location')
        from_location = StockLocation.search(["code", "=", "STO"])[0]
        return from_location.id


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    'Parking Configuration Sequence'
    __name__ = 'parking.place.configuration.sequence'

    mobilization_request_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de solicitud de movilización',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'mobilization_request'),
        ],
        depends=['company'])
    mobilization_order_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia orden de movilización',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'mobilization_order'),
        ],
        depends=['company'])

    accident_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia accidente',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'accident'),
        ],
        depends=['company'])

    maintenance_vehicle_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia mantenimientos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'maintenance'),
        ],
        depends=['company'])

    payment_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia liquidación de O. de combustible',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'payment'),
        ],
        depends=['company'])

    mount_tires_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de montaje de los neumáticos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'mount_tires'),
        ],
        depends=['company'])
    disassembly_tires_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia desmontaje neumáticos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'disassembly_tires'),
        ])

    actualization_tires_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia actualizacion neumáticos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'actualization_tires'),
        ])

    retreat_tires_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia reencauche neumáticos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'retreat_tires'),
        ])


    @classmethod
    def default_mobilization_request_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_parking_mobilization_request')
        except KeyError:
            return None

    @classmethod
    def default_mobilization_order_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_parking_mobilization_order')
        except KeyError:
            return None

    @classmethod
    def default_accident_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_accident_order')
        except KeyError:
            return None

    @classmethod
    def default_maintenance_vehicle_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_vehicle_maintenance')
        except KeyError:
            return None

    @classmethod
    def default_payment_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_payment')
        except KeyError:
            return None

    @classmethod
    def default_mount_tires_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_mount_tires')
        except KeyError:
            return None

    @classmethod
    def default_disassembly_tires_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_disassembly_tires')
        except KeyError:
            return None

    @classmethod
    def default_actualization_tires_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_actualization_tires')
        except KeyError:
            return None

    @classmethod
    def default_retreat_tires_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('parking_place',
                                    'sequence_retreat_tires')
        except KeyError:
            return None

class ConfigurationLocation(ModelSQL, ValueMixin):
    "Configuration Location"
    __name__ = 'parking.place.configuration.location'
    to_location = fields.Many2One(
        'stock.location', "Bodega de mantenimiento",
        domain=[
             ('type', '=', 'lost_found'),
        ])
    shipment_reason = fields.Many2One(
        'stock.shipment.reason', "Razon de albarán para mantenimiento")

    from_location = fields.Many2One('stock.location', "Desde almacenamiento",
        domain=[
            ('type', 'in',
             ['view', 'storage', 'lost_found']),
        ])

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        exist = TableHandler.table_exist(cls._table)

        super(ConfigurationLocation, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('to_location')
        value_names.append('to_location')
        field_names.append('shipment_reason')
        value_names.append('shipment_reason')
        field_names.append('from_location')
        value_names.append('from_location')
        migrate_property(
            'parking.place.configuration', field_names, cls, value_names,
            fields=fields)


class MaintenanceSendEmail(ModelSQL, ModelView):
    'Maintenance Send Email'
    __name__ = 'parking.place.maintenance.send.email'

    email = fields.Many2One('company.employee', 'Empleado')