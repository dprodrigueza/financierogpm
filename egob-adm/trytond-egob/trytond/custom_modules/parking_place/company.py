from sql import Literal

from trytond.model import fields
from trytond.pool import Pool, PoolMeta

__all__ = ['Employee', 'Position']


class Employee(metaclass=PoolMeta):
    'Employee'
    __name__ = 'company.employee'

    is_driver = fields.Function(fields.Boolean('Es conductor?'),
        'on_change_with_is_driver', searcher='search_is_driver')

    @fields.depends('contract_position')
    def on_change_with_is_driver(self, name=None):
        if self.contract_position:
            return self.contract_position.is_driver
        return False

    @classmethod
    def search_is_driver(cls, name, clause):
        pool = Pool()
        Contract = pool.get('company.contract')
        Position = pool.get('company.position')

        employee = cls.__table__()
        contract = Contract.__table__()
        position = Position.__table__()

        end_query = cls._get_end_contract_sql()
        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]
        where = Literal(True)
        where = Operator(position.is_driver, value)
        query = employee.join(contract,
            condition=employee.id == contract.employee
        ).join(position,
            condition=position.id == contract.position
        ).join(end_query,
            condition=contract.id == end_query.contract
        ).select(
            employee.id,
            where=where
        )
        return [('id', 'in', query)]


class Position(metaclass=PoolMeta):
    'Position'
    __name__ = 'company.position'
    is_driver = fields.Boolean('Es conductor?')
