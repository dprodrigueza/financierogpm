from decimal import Decimal
import functools

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval, If, And, Or
from trytond.transaction import Transaction
from sql.operators import Concat
from sql import Window, Literal
from sql.functions import RowNumber, CurrentTimestamp

__all__ = ['Driver', 'MobilizationOrder', 'MobilizationRequest',
        'Workshop']

ORDER_STATES = {
        'readonly': (~(Eval('state') == 'draft') |
            (Eval('type_') == 'plannified')
            )
        }

ORDER_DEPENDS = ['state', 'type_']

REQUEST_STATES = {
        'readonly': ~(Eval('state') == 'draft')
        }

REQUEST_DEPENDS = ['state']


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)

            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


STATES_CONTRACTED = {
    'readonly': And(Eval('type') != 'contracted', Eval('type') != 'contracted'),
    'required': Or(Eval('type') == 'contracted', Eval('type') == 'contracted')
}

DEPENDS = ['type_']


class Driver(ModelSQL, ModelView):
    "Driver"
    __name__ = 'parking.vehicle.driver'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehiculo asignado')

    show_name = fields.Function(fields.Char('Nombre'),
        'on_change_with_show_name')

    employee = fields.Many2One('company.employee', 'Empleado',
        states={
            'invisible': ~(Eval('type_') == 'institutional'),
            'required': (Eval('type_') == 'institutional')
            }, domain=[
            ('is_driver', '=', True)
        ], depends=['type_'])
    type_ = fields.Selection([
            ('institutional', 'Institucional'),
            ('contracted', 'Contratado')
            ], 'Tipo', required=True)

    driver_license_type = fields.Selection([('', ''),
        ('a1', 'A1'), ('a', 'A'), ('b', 'B'),
        ('c1', 'C1'), ('c', 'C'), ('d1', 'D1'),
        ('d', 'D'), ('e', 'E'), ('f', 'F'), ('g', 'G')], 'Tipo de licencia',
        states={
            'readonly': Eval('type_') == 'institutional'
        })
    driver_license_type_translated = \
        driver_license_type.translated('driver_license_type')

    driver_license_exp_date = fields.Date(
        'Caducidad Licencia Conducir', states={
            'readonly': Eval('type_') == 'institutional'
        }, depends=['type_'], help='Fecha de caducidad de licencia de conducir'
    )
    party = fields.Many2One('party.party', 'Tercero',
        states={
            'invisible': ~(Eval('type_') == 'contracted'),
            'required': (Eval('type_') == 'contracted')
            }, depends=['type_']
    )
    identifier = fields.Function(fields.Char('CED/RUC'),
        'on_change_with_identifier')
    phone = fields.Function(fields.Char('Teléfono'),
        'on_change_with_phone')
    address = fields.Function(fields.Char('Dirección'),
        'on_change_with_address')
    gender = fields.Selection([
        ('male', 'Masculino'),
        ('female', 'Femenino'),
        ('other', 'Otro')], 'Género', states={
            'readonly': Eval('type_') == 'institutional'
        }, depends=['type_'], sort=False)
    civil_state = fields.Selection(
        [
            ('', ''),
            ('single', 'Soltero/a'), ('married', 'Casado/a'),
            ('divorced', 'Divorciado/a'), ('widow', 'Viudo/a'),
            ('consensual_union', 'Union libre')],
        'Estado Civil', states={
            'readonly': Eval('type_') == 'institutional'
        }, depends=['type_']
    )

    @staticmethod
    def default_type_():
        return 'institutional'

    @fields.depends('party', 'employee')
    def on_change_with_identifier(self, name=None):
        if self.party:
            if self.party.identifiers:
                return self.party.identifiers[0].code
        elif self.employee:
            if self.employee.party:
                if self.employee.party.identifiers:
                    return self.employee.party.identifiers[0].code
        return ''

    @fields.depends('party', 'employee')
    def on_change_with_phone(self, name=None):
        if self.party:
            if self.party.phone:
                return self.party.phone
        elif self.employee:
            if self.employee.party:
                if self.employee.party.phone:
                    return self.employee.party.phone
        return None

    @fields.depends('party', 'employee')
    def on_change_with_address(self, name=None):
        if self.party:
            if self.party.addresses:
                return self.party.addresses[0].rec_name
        elif self.employee:
            if self.employee.party:
                if self.employee.party.addresses:
                    return self.employee.party.addresses[0].rec_name
        return ''

    @fields.depends('employee')
    def on_change_employee(self, name=None):
        if self.employee:
            self.driver_license_type = self.employee.driver_license_type
            self.driver_license_exp_date = \
                self.employee.driver_license_exp_date
            self.gender = self.employee.gender
            self.civil_state = self.employee.civil_state

    @fields.depends('type_')
    def on_change_type_(self, name=None):
        self.employee = None
        self.party = None
        self.driver_license_type = None
        self.driver_license_exp_date = None
        self.gender = 'other'
        self.civil_state = 'single'
        self.address = None
        self.phone = None

    @fields.depends('type_', 'employee', 'party')
    def on_change_with_show_name(self, name=None):
        if self.type_ == 'institutional':
            if self.employee:
                return self.employee.party.name
        elif self.type_ == 'contracted':
            if self.party:
                return self.party.name

    def get_rec_name(self, name):
        if self.show_name:
            return self.show_name


class MovilizationMixin(object):

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    city = fields.Many2One('country.subdivision', 'Ciudad',
        domain=[
            ('type', '=', 'city'),
            ('country.code', '=', 'EC')
        ], required=True)
    departure_date = fields.Date('Fecha de salida',
       domain=[
           ('departure_date', '<=', Eval('return_date'))
       ], required=True
    )
    departure_time = fields.Time('Hora de Salida',
        domain=[
            If((Eval('departure_date')) & (Eval('return_date')) &
                (Eval('departure_date') == Eval('return_date')),
                ('departure_time', '<=', Eval('return_time')),
                (),
            )
        ], required=True
    )
    return_date = fields.Date('Fecha de Retorno', required=True,
        domain=[
            ('return_date', '>=', Eval('departure_date'))
        ], depends=['departure_date'])
    return_time = fields.Time('Hora de Retorno', required=True,
          domain=[
                If(
                    (Eval('departure_date')) & (Eval('return_date'))
                    & (Eval('departure_date') == Eval('return_date')),
                    ('return_time', '>=', Eval('departure_time')),
                    (),
                )],
            depends=['departure_date', 'return_date', 'return_time',
                'departure_time']
    )
    from_city = fields.Many2One('country.subdivision', 'Ciudad de partida',
        domain=[
            ('type', '=', 'city'),
            ('country.code', '=', 'EC')
        ], required=True)
    from_location = fields.Char("Ubicación de partida")
    to_city = fields.Many2One('country.subdivision', 'Ciudad de destino',
        domain=[
            ('type', '=', 'city'),
            ('country.code', '=', 'EC')
        ], required=True)
    to_location = fields.Char("Ubicación destino")
    authorized_by = employee_field("Autorizado por")
    authorization_date = fields.Date('Fecha de autorización', readonly=True)
    reason = fields.Text('Razon de movilización', required=True)

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')


class MobilizationRequest(Workflow, ModelSQL, ModelView, MovilizationMixin):
    "Movilization Request"
    __name__ = 'parking.mobilization.request'

    department = fields.Many2One('company.department',
        'Departamento', required=True, readonly=True)
    number = fields.Char('Número de Solicitud', readonly=True)
    request_by = employee_field("Solicitado por")
    request_date = fields.Date('Fecha Solicitud', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('request', 'Solicitada'),
        ('authorized', 'Autorizada'),
        ('done', 'Ejecutada'),
        ('denied', 'Negada')
    ], 'Estado', readonly=True)

    @classmethod
    def __setup__(cls):
        super(MobilizationRequest, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'authorized'),
            ('authorized', 'done'),
            ('request', 'denied'),
            ('request', 'draft'),
        ))
        cls._buttons.update(
            {
                'request': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'deny': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'draft': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'authorize': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'done': {
                    'invisible': ~Eval('state').in_(['authorized']),
                },
            })
        cls._error_messages.update({
            'not_email': ('Actualmente el usuario no tiene un correo '
                'configurado'),
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'no_sequence': ('No existe una secuencia cofigurada para la '
                            'solicitud de movilización.')
        })
        cls.department.domain = []

        cls.city.states = REQUEST_STATES
        cls.departure_date.states = REQUEST_STATES
        cls.departure_time.states = REQUEST_STATES
        cls.return_date.states = REQUEST_STATES
        cls.return_time.states = REQUEST_STATES
        cls.from_location.states = REQUEST_STATES
        cls.from_city.states = REQUEST_STATES
        cls.to_city.states = REQUEST_STATES
        cls.to_location.states = REQUEST_STATES
        cls.reason.states = REQUEST_STATES
        cls.city.states = REQUEST_STATES
        cls.departure_date.depends = ['return_date', 'state']
        cls.departure_time.depends = ['return_date', 'return_time',
                                    'departure_date', 'state']
        cls.return_date.depends = ['departure_date', 'state']
        cls.return_time.depends = ['departure_date', 'return_date',
                                'return_time', 'departure_time', 'state']
        cls.from_city.depends = REQUEST_DEPENDS
        cls.from_location.depends = REQUEST_DEPENDS
        cls.to_city.depends = REQUEST_DEPENDS
        cls.to_location.depends = REQUEST_DEPENDS
        cls.reason.depends = REQUEST_DEPENDS
        cls.city.depends = REQUEST_DEPENDS

    @staticmethod
    def default_state():
        return 'draft'
    
    @staticmethod
    def default_department():
        return Transaction().context.get('department')

    @classmethod
    def set_number(cls, requests):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.mobilization_request_sequence:
            cls.raise_user_error('no_sequence')
        for request in requests:
            if request.number:
                continue
            request.number = Sequence.get_id(
                config.mobilization_request_sequence.id)
        cls.save(requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    @set_employee('request_by')
    def request(cls, requests):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for request in requests:
            request.request_date = today
        cls.set_number(requests)
        super(MobilizationRequest, cls).save(requests)
        #cls.send_email(cls, requests, 'request')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, requests):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('authorized')
    @set_employee('authorized_by')
    def authorize(cls, requests):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for request in requests:
            request.authorization_date = today
        super(MobilizationRequest, cls).save(requests)
        # cls.send_email(cls, requests, 'authorized')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('denied')
    def deny(cls, requests):
        # cls.send_email(cls, requests, 'denied')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, requests):
        # cls.send_email(cls, requests, 'denied')
        pass

    def send_email(cls, requests, state_to):
        pool = Pool()
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Solicitud de movilización'),
            ('template', '=', True),
        ])
        alerts = []
        for request in requests:
            if request:
                employees = []
                if state_to in ['authorized', 'denied']:
                    employees.append(request.request_by)
                elif state_to == 'request':
                    Group = pool.get('res.group')
                    search_group = Group.search([('name', '=',
                    'Parque automotor / Solicitud de movilización: Autorizar')
                    ])
                    if search_group:
                        for user in search_group[0].users:
                            if user.employee:
                                employees.append(user.employee)
                mail_body = cls.generate_mail_body(cls,
                    request, state_to)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                        f"parking.mobilization.request,"
                        f"{request.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                         ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = request
                        # The mail body depends of transition state
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) emplado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_mail_body(cls, request, state_to):
        if state_to == "request":
            return (f"Se ha registrado una nueva solicitud de "
                "movilización para el día: " f"{request.request_date} "
                "desde la ciudad de " f"{request.from_city.name} a la "
                "ciudad de " f"{request.to_city.name} "
                "por el motivo de " f"{request.reason}. \n"
                "Por favor revisar las solicitudes pendientes en el sistema.")
        elif state_to == "denied":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                "movilización el por el motivo de " f"{request.reason} "
                "ha sido rechazada.")
        elif state_to == "authorized":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                "movilización el por el motivo de " f"{request.reason} "
                "ha sido autorizada. Por favor se deberá acercar a retirar "
                "el documento de la órden de movilización autorizada por el "
                "jefe de departamento correspondiente.")

    def get_rec_name(self, name):
        if self.number:
            return self.number


class MobilizationOrder(Workflow, ModelSQL, ModelView, MovilizationMixin):
    "Mobilization Order"
    __name__ = 'parking.mobilization.order'

    number = fields.Char('Número de Orden', readonly=True)
    department = fields.Many2One('company.department',
        'Departamento', required=True, states={
            'readonly': True
        })
    type_ = fields.Selection([
        ('plannified', 'Planificada'),
        ('not_plannified', 'No Planificada')],
        'Tipo', required=True, states={
            'readonly': (Eval('state') != 'draft')
        }
    )
    mobilization_request = fields.Many2One('parking.mobilization.request',
        'Solicitud de movilización',
        states={
            'required': Eval('type_') == 'plannified',
            'invisible': Eval('type_') != 'plannified',
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'],
        domain=[
            If(
                (Eval('state') == 'draft'),
                ('state', '=', 'authorized'),
                ()
            )
        ]
    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('deny', 'Negado'),
        ('authorized', 'Autorizada'),
        ('finished', 'Finalizada')
        ], 'Estado', readonly=True
    )
    passengers = fields.Integer("Ocupantes", states={
        'readonly': ~(Eval('state') == 'draft'),
        'required': Eval('state').in_(['authorized','finished'])
        }, depends=['state']
    )

    current_mileage = fields.Numeric('Km inicio', states={
        'readonly': ~(Eval('state') == 'draft')
        }, depends=['state']
    )

    final_mileage = fields.Numeric('Recorrido del vehículo al regresar', states={
        'readonly': ~(Eval('state') == 'authorized')
        }, depends=['state']
    )

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehiculo', states={
        'readonly': ~(Eval('state') == 'draft'),
        'required': Eval('state').in_(['authorized','finished'])
        }, context={
            'parking_mobilization_order': True,
            'departure_date': Eval('departure_date'),
            'return_date': Eval('return_date'),
            'departure_time': Eval('departure_time'),
            'return_time': Eval('return_time'),
            'order_state': Eval('state'),
        },
        depends=['state', 'departure_date', 'return_date', 'departure_time',
        'return_time']
        
    )
    vehicle_drivers = fields.Function(
        fields.One2Many('parking.vehicle.driver',
            None, 'Conductores del vehículo', states={
                'invisible': True
            }),
        'on_change_with_vehicle_drivers')
    driver = fields.Many2One('parking.vehicle.driver', 'Conductor', states={
        'readonly': ~(Eval('state') == 'draft'),
            'required': Eval('state').in_(['authorized','finished'])
        }, domain=[
            ('id', 'in', Eval('vehicle_drivers'))
        ], depends=['state', 'vehicle_drivers']
    )
    driver_position = fields.Function(
        fields.Char('Puesto de trabajo'),
        'on_change_with_driver_position')
    driver_licence_type = fields.Function(
        fields.Char('Tipo de licencia'),
        'on_change_with_driver_licence_type')

    @classmethod
    def __setup__(cls):
        super(MobilizationOrder, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'authorized'),
            ('draft', 'deny'),
            ('authorized', 'finished'),
        ))
        cls._buttons.update(
            {
                'authorize': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'deny': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'finalize': {
                    'invisible': ~Eval('state').in_(['authorized']),
                },
            })
        cls._error_messages.update({
            'final_mileage_required': ('Para poder finalizar esta orden de'
            'movilización, es necesario agregar un recorrido final para '
            'actualizar el estado del vehiculo.'),
            'no_sequence': ('No existe una secuencia cofigurada para los '
                            'mantenimientos.')
        })
        cls.department.domain = []

        cls.city.states = ORDER_STATES
        cls.departure_date.states = ORDER_STATES
        #cls.departure_time.states = ORDER_STATES
        cls.return_date.states = ORDER_STATES
        #cls.return_time.states = ORDER_STATES
        cls.from_city.states = ORDER_STATES
        cls.from_location.states = ORDER_STATES
        cls.to_city.states = ORDER_STATES
        cls.to_location.states = ORDER_STATES
        cls.reason.states = ORDER_STATES
        cls.city.states = ORDER_STATES
        cls.departure_date.depends = ['return_date', 'state', 'type_']
        cls.departure_time.depends = ['return_date', 'return_time',
                                    'departure_date', 'state', 'type_']
        cls.return_date.depends = ['departure_date', 'state', 'type_']
        cls.return_time.depends = ['departure_date', 'return_date',
                                'return_time', 'departure_time', 'state',
                                'type_']
        cls.from_city.depends = ORDER_DEPENDS
        cls.to_city.depends = ORDER_DEPENDS
        cls.reason.depends = ORDER_DEPENDS
        cls.city.depends = ORDER_DEPENDS

    @classmethod
    def set_number(cls, orders):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.mobilization_order_sequence:
            cls.raise_user_error('no_sequence')
        for order in orders:
            if order.number:
                continue
            order.number = Sequence.get_id(
                config.mobilization_order_sequence.id)
        cls.save(orders)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_type_():
        return 'plannified'

    @fields.depends('driver')
    def on_change_with_driver_licence_type(self, name=None):
        if self.driver:
            return self.driver.driver_license_type_translated
        return None

    @fields.depends('driver')
    def on_change_with_driver_position(self, name=None):
        if self.driver:
            if self.driver.party:
                return "Conductor externo contratado"
            if self.driver.employee:
                return self.driver.employee.contract_position.name
        return None

    @fields.depends('type_', 'mobilization_request')
    def on_change_mobilization_request(self, name=None):
        if self.type_ == 'plannified' and self.mobilization_request:
            self.city = self.mobilization_request.city
            self.department = self.mobilization_request.department
            self.departure_date = self.mobilization_request.departure_date
            self.departure_time = self.mobilization_request.departure_time
            self.return_date = self.mobilization_request.return_date
            self.return_time = self.mobilization_request.return_time
            self.from_city = self.mobilization_request.from_city
            self.from_location = self.mobilization_request.from_location
            self.to_city = self.mobilization_request.to_city
            self.to_location = self.mobilization_request.to_location
            self.reason = self.mobilization_request.reason
        else:
            self.city = None
            self.departure_date = None
            self.departure_time = None
            self.return_date = None
            self.return_time = None
            self.from_city = None
            self.to_city = None
            self.reason = None

    @fields.depends('vehicle')
    def on_change_vehicle(self, name=None):
        if self.vehicle:
            if self.vehicle.driver:
                self.driver = self.vehicle.driver
            if self.vehicle.current_mileage:
                self.current_mileage = self.vehicle.current_mileage
            else:
                self.current_mileage = Decimal(0.0)

    @fields.depends('vehicle')
    def on_change_with_vehicle_drivers(self, name=None):
        if not self.vehicle:
            return
        drivers = []
        for driver in self.vehicle.drivers:
            drivers.append(driver.id)
        return drivers
    
    @fields.depends('type_')
    def on_change_type_(self, name=None):
        if self.type_ == 'not_plannified':
            self.department = Transaction().context.get('department')

    @classmethod
    @ModelView.button
    @Workflow.transition('authorized')
    @set_employee('authorized_by')
    def authorize(cls, orders):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for order in orders:
            order.authorization_date = today
            if order.type_ == 'plannified':
                order.mobilization_request.state = 'done'
                order.mobilization_request.save()
        cls.set_number(orders)
        # cls.send_email(cls, orders, 'authorized')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('deny')
    def deny(cls, orders):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finalize(cls, orders):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        MileageUpdate = pool.get('parking.vehicle.mileage.update')
        UpdateWizard = pool.get('parking.vehicle.update.mileage', type='wizard')
        session_id, _, _ = UpdateWizard.create()
        for order in orders:
            if order.final_mileage:
                if order.vehicle.current_mileage < order.final_mileage:
                    # Using this code because some vehicles doesn't have setted the
                    # current usage yet.
                    if not order.vehicle.current_usage:
                        order.vehicle.current_usage = 0
                        Vehicle.save([order.vehicle])
                    with Transaction().set_context(active_ids=[order.vehicle.id],
                            active_id=order.vehicle.id):
                        update_wizard = UpdateWizard(session_id)
                        update_wizard.start.new_usage = order.vehicle.current_usage
                        update_wizard.start.new_mileage = order.final_mileage
                        update_wizard.start.description = \
                            "Regreso de movilización: " + order.reason
                        update_wizard.transition_change()
        pass

    def get_rec_name(self, name):
        if self.number:
            return self.number

    def send_email(cls, orders, state_to):
        pool = Pool()
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Orden de movilización'),
            ('template', '=', True),
        ])
        alerts = []
        for order in orders:
            if order:
                employees = []
                if state_to in ['authorized']:
                    if order.mobilization_request:
                        request_by = order.mobilization_request.request_by
                        employees.append(request_by)
                mail_body = cls.generate_mail_body(cls,
                    order, state_to)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                        f"parking.mobilization.order,"
                        f"{order.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                         ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = order
                        # The mail body depends of transition state
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) emplado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_mail_body(cls, order, state_to):
        if state_to == "authorized":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                "movilización el por el motivo de " f"{order.reason} "
                "ha sido autorizada. Por favor se deberá acercar a retirar "
                "el documento de la órden de movilización autorizada por el "
                "jefe de departamento correspondiente.")


class Workshop(ModelView, ModelSQL):
    'Workshop'
    __name__ = 'parking.workshop'

    states_own_ = {
        'invisible': ~(Eval('type_') == 'own'),
        'required': (Eval('type_') == 'own'),
        }

    states_external_ = {
        'invisible': (Eval('type_') != 'external'),
        'required': (Eval('type_') == 'external'),
        }

    name = fields.Char('Nombre', required=True)
    type_ = fields.Selection([
        ('own', 'Propio'),
        ('external', 'Particular/Externo'),
    ], 'Tipo', required=True)
    type_translated = type_.translated('type_')
    company = fields.Many2One('company.company', 'Empresa',
        states=states_own_, depends=['type_'])
    company_party = fields.Function(
        fields.Many2One('party.party', 'Tercero', states=states_own_),
        'on_change_with_company_party')
    boss = fields.Many2One('company.employee', 'Jefe/Encargado',
        states=states_own_, depends=['type_'])
    own_workshop_address = fields.Many2One('party.address', 'Dirección',
        domain=[('party', '=', Eval('company_party'))],
        states=states_own_, depends=['company_party', 'type_'])
    party = fields.Many2One('party.party', 'Tercero',
        states=states_external_, depends=['type_'])
    party_address = fields.Many2One('party.address', 'Dirección',
        domain=[('party', '=', Eval('party'))],
        states=states_external_, depends=['party', 'type_'])
    maintenance_history = fields.One2Many(
        'parking.workshop.maintenance.history',
        'workshop', 'Historial de mantenimientos', readonly=True)

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @fields.depends('company')
    def on_change_with_company_party(self, name=None):
        if self.company:
            return self.company.party.id
        else:
            return None

    def get_rec_name(self, name):
        if self.type_ == 'own':
            return self.name + ' - ' + self.own_workshop_address.name
        else:
            return self.name + ' - ' + self.party.name


class WorkshopMaintenanceHistory(ModelSQL, ModelView):
    'Maintenance - History'
    __name__ = 'parking.workshop.maintenance.history'

    workshop = fields.Many2One('parking.workshop',
        'Taller', readonly=True)
    maintenance = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)
    type = fields.Selection([
        ('preventive', 'Preventivo'),
        ('corrective', 'Correctivo'),
        ], 'Tipo', readonly=True)
    start_date = fields.Date('Fecha inicio', readonly=True)
    start_time = fields.Time('Hora inicio', readonly=True)
    end_date = fields.Date('Fecha fin', readonly=True)
    end_time = fields.Time('Hora de finalización', readonly=True)

    @staticmethod
    def table_query():
        pool = Pool()
        Maitenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maitenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Workshop = pool.get('parking.workshop')
        workshop = Workshop.__table__()
        where = (Literal(True))

        query = workshop.join(maintenance,
            condition=maintenance.workshop == workshop.id
        ).join(vehicle,
            condition=vehicle.id == maintenance.vehicle
        ).select(
            workshop.id.as_('workshop'),
            maintenance.id.as_('maintenance'),
            vehicle.id.as_('vehicle'),
            maintenance.type,
            maintenance.start_date,
            maintenance.start_time,
            maintenance.end_date,
            maintenance.end_time,
            where=where)

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.workshop,
            query.maintenance,
            query.vehicle,
            query.type,
            query.start_date,
            query.start_time,
            query.end_date,
            query.end_time,
            where=where
        )

    @classmethod
    def __setup__(cls):
        super(WorkshopMaintenanceHistory, cls).__setup__()
        cls._order = [
            ('start_date', 'ASC')
        ]


class WorkshopQuery(ModelSQL, ModelView):
    'Workshop Query'
    __name__ = 'parking.workshop.query'

    workshop = fields.Many2One('parking.workshop', 'Taller',
        readonly=True)
    maintenance = fields.Many2One('parking.vehicle.maintenance', 'Mantenimiento',
        readonly=True)
    vehicle = fields.Char('Vehicle', readonly=True)
    number = fields.Char('No. de mantenimiento', readonly=True)
    description = fields.Char('Descripción', readonly=True)
    start_date = fields.Date('Fecha inicio', readonly=True)
    end_date = fields.Date('Fecha fin', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        fuel_type_id = context.get('fuel_type_id')

        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Workshop = pool.get('parking.workshop')
        workshop = Workshop.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()

        where = (maintenance.state == 'finished' or
            maintenance.state == 'authorized')
        if context.get('workshop'):
            where &= (maintenance.workshop >= context.get('workshop'))  
        if context.get('start_date'):
            where &= (maintenance.start_date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (maintenance.start_date <= context.get('end_date'))
        if context.get('vehicles'):
            ids = []
            for data in context.get('vehicles'):
                ids.append(data['id'])
            where &= (maintenance.vehicle.in_(ids))

        query = workshop.join(
            maintenance, condition= maintenance.workshop == workshop.id
        ).join(
            vehicle, condition= maintenance.vehicle == vehicle.id
        ).join(
            mark, condition= vehicle.mark == mark.id
        ).join(
            model, condition= vehicle.model == model.id
        ).select(
            workshop.id.as_('workshop'),
            maintenance.id.as_('maintenance'),
            maintenance.number,
            maintenance.description,
            maintenance.start_date,
            maintenance.end_date,
            Concat(Concat(mark.name,Concat(' / ',model.name)),
                Concat(' / ',vehicle.plaque)).as_('vehicle'),
            where=where)

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.workshop,
            query.maintenance,
            query.number,
            query.start_date,
            query.end_date,
            query.vehicle,
            query.description,
        )

    @classmethod
    def __setup__(cls):
        super(WorkshopQuery, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]


class WorkshopQueryContext(ModelView):
    'Workshop Query Context'
    __name__ = 'parking.workshop.query.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')
    workshop = fields.Many2One('parking.workshop', 'Taller')