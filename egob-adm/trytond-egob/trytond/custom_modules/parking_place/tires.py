from trytond.model import Workflow, ModelView, ModelSQL, fields,  Unique
from trytond.pool import  Pool
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.transaction import Transaction
from sql.conditionals import Coalesce
from sql.operators import Concat

from sql import Window, Literal, Union, Null
from sql.functions import RowNumber, CurrentTimestamp
from trytond.transaction import Transaction
from itertools import groupby

__all__ = ['Mark','Desing','Measurement','Tires','UpdateTires',
           'UpdateTiresLines', 'TiresLinesRetread',
           ]


STATE = [
    ('available', 'Disponible'),
    ('mount', 'Montado'),
    ('disassembly', 'Desmontado'),
    ('retread', 'En reencauche'),
    ('emergency', 'De emergencia'),
]


def get_repeated_instances(instances_list, attr_name):
    repeated_list = []
    for i, obj in groupby(sorted(instances_list,
        key=lambda x: getattr(x, attr_name)),
          lambda x: getattr(x, attr_name)):
        group = list(obj)
        if len(group) > 1:
            repeated_list.append(group)
    return repeated_list

class Mark(ModelSQL, ModelView):
    "Mark"
    __name__ = 'parking_place.mark_tires'
    name = fields.Char('Nombre')


class Desing(ModelSQL, ModelView):
    "Desing"
    __name__ = 'parking_place.desing_tires'
    name = fields.Char('Nombre')


class Measurement(ModelSQL, ModelView):
    "Measurement"
    __name__ = 'parking_place.measurement_tires'
    name = fields.Char('Nombre')


class Tires(ModelSQL, ModelView):
    "Tires"
    __name__ = 'parking_place.tires'


    mark = fields.Many2One('parking_place.mark_tires', 'Marca', required=True)
    desing = fields.Many2One('parking_place.desing_tires', 'Diseño',
        required=True)
    measurement = fields.Many2One('parking_place.measurement_tires',
        'Medidas O/R')
    depth = fields.Numeric('Labrado mm')
    pressure = fields.Numeric('Presión PSI')
    history = fields.One2Many('parking.tires.history.query','tires','Estado '
                            'neumaticos')
    state= fields.Function(fields.Selection(STATE,'Estado Actual'),
        'on_change_with_state', searcher='search_state')
    state_translated = state.translated('state')
    date = fields.Date('Fecha registro',readonly=True)

    @classmethod
    def search_state(cls, name, clause):
        pool = Pool()
        Tire = pool.get('parking_place.tires')
        items = Tire.search([
            ('id', '>', 0),
        ])
        ids = []
        for item in items:
            if item.state == 'available':
                ids.append(item.id)
        return [('id', 'in', ids)]

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('history')
    def on_change_with_state(self, name=None):
        if self.history:
            record = self.history[len(self.history)-1]
            if record.origin_model == 'parking_place.mount_lines_tires':
                return 'mount'
            if record.origin_model == \
                'parking_place.disassembly_lines_tires':
                 return 'disassembly'
            if record.origin_model == 'parking_place.tires.retread.lines':
                return 'retread'
        return 'available'


class MountTires(Workflow,ModelSQL,ModelView):
    "Monunt Tires"
    __name__ = 'parking_place.mount_tires'

    number = fields.Numeric('Nº Montaje', readonly=True)
    date = fields.Date('Fecha Montaje',
        states= {
            'readonly': Eval('state').in_(['mount'])
        },)
    order_work = fields.Many2One('parking.vehicle.maintenance',
        'Orden de trabajo',  domain = [('state', '=', 'entering')],
        states= {
        'readonly': Eval('state').in_(['mount'])
        },
                                 required=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
    domain = [('state_mounts', '=', 'disassembly')],
        states= {
        'readonly': Eval('state').in_(['mount'])
        },
    depends=['order_work'])
    km_vehicle = fields.Numeric('Km Vehiculo',  states= {
            'readonly': Eval('state').in_(['mount'])
        },depends=['order_work'])
    supervisor = fields.Many2One('company.employee','Supervisor')
    operator = fields.Many2One('company.employee','Operador')
    lines_tires = fields.One2Many('parking_place.mount_lines_tires',
        'mount_tires','Neumáticos',  states= {
            'readonly': Eval('state').in_(['mount'])
        }, depends=['order_work'])
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('mount', 'Montado'),
        ('disassembly', 'Desmontado'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')


    @classmethod
    def __setup__(cls):
        super(MountTires, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'mount'),
            ('mount', 'disassembly'),))
        cls._buttons.update(
            {
                'mount': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },
                'disassembly': {
                    'invisible':
                        (Eval('state').in_(['draft']))
                },

            })
        cls._error_messages.update ({
            'repeated_position': ('Posición del neumático repetido')
        })
        cls._error_messages.update({
            'repeated_tires': ('Neumático repetido')
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, mounts):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('mount')
    def mount(cls, mounts):
        cls.save(mounts)
        for mount in mounts:
            cls.set_number(mounts)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('disassembly')
    def disassembly(cls, mounts):
        pass

    def get_rec_name(self, name):
        return ' %s ' % (
            str(self.vehicle.mark.name)
        )

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def set_number(cls, tires):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.mount_tires_sequence:
            cls.raise_user_error('no_sequence')
        for tire in tires:
            if tire.number:
                continue
            tire.number = Sequence.get_id(
                config.mount_tires_sequence.id)
        cls.save(tires)

    @fields.depends('order_work','vehicle','km_vehicle','lines_tires')
    def on_change_order_work(self, name=None):
        pool = Pool()
        Line = pool.get('parking_place.mount_lines_tires')
        if self.order_work:
            if self.order_work.vehicle:
                self.vehicle = self.order_work.vehicle
                self.km_vehicle= self.order_work.mileage_arrival
                if self.order_work.lines:
                    for line in self.order_work.lines:
                        aux = line.quantity_to_request
                        for x in range(int(aux)):
                            new_line = Line()
                            new_line.activities =line.description
                            self.lines_tires = self.lines_tires + (new_line,)

    @classmethod
    def validate(cls, mounts):
        super(MountTires, cls).validate(mounts)
        for mount in mounts:
            if mount.lines_tires:
                cls.check_repeated_tires(mount)
                cls.check_repeated_positions(mount)

    @classmethod
    def check_repeated_positions(cls, mount):
        if mount.lines_tires:
            lines = list(mount.lines_tires)
            repeated_list = get_repeated_instances(lines, 'position')
            for groups in repeated_list:
                line = groups[0]
                cls.raise_user_error('repeated_position', {

                })

    @classmethod
    def check_repeated_tires(cls, mount):
        if mount.lines_tires:
            lines = list(mount.lines_tires)
            repeated_list = get_repeated_instances(lines, 'tires')
            for groups in repeated_list:
                line = groups[0]
                cls.raise_user_error('repeated_tires', {

                })


class MountLinesTires(ModelSQL, ModelView):
    "Mount Lines Tires"
    __name__ = 'parking_place.mount_lines_tires'

    activities = fields.Char('Descripción')
    spare = fields.Many2One('product.product', 'Repuesto')
    tires = fields.Many2One('parking_place.tires', 'Neumático',
        domain = [
        ('state', '=', 'available')
        ]
    )
    depth = fields.Function(fields.Numeric('Labrado mm'),
        'on_change_with_depth')
    pressure = fields.Function(fields.Numeric('Presión/PSI'),
        'on_change_with_pressure')
    valve = fields.Selection([
        (None, ''),
        ('yes', 'Si'),
        ('no', 'No'),],'C/Valvulas')
    valve_cover = fields.Selection([
        (None, ''),
        ('yes', 'Si'),
        ('no', 'No'),],'Tapa')
    valve_translated = valve.translated('valve')
    cover_translated=valve_cover.translated('valve_cover')
    observation = fields.Char('Observaciones')
    mount_tires = fields.Many2One('parking_place.mount_tires', 'Montaje')
    position = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ], 'Posición',
       )

    @fields.depends('tires')
    def on_change_with_depth(self, name=None):
        if self.tires:
            if self.tires.depth:
                return self.tires.depth

    @fields.depends('tires')
    def on_change_with_pressure(self, name=None):
        if self.tires:
            if self.tires.pressure:
                return self.tires.pressure




    @classmethod
    def __setup__(cls):
        super(MountLinesTires, cls).__setup__()
        m = cls.__table__()
        cls._sql_constraints += [
            ('unique_number',
             Unique(m, m.tires
                    ),
             'El campo  debe ser único'),
        ]


class DisassemblyTires(Workflow,ModelSQL,ModelView):
    'Disassembly Tires'
    __name__ = 'parking_place.disassembly_tires'

    number = fields.Numeric('Desmontaje Nº', readonly=True)
    date = fields.Date('Fecha Desmontaje',
                       states={
                           'readonly': Eval('state').in_(['disassembly'])
                       }, )
    order_work = fields.Many2One('parking.vehicle.maintenance',
        'Orden de trabajo',
        domain=[('state', '=', 'entering')],

                                 required=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
    domain = [
        ('state_mounts', '=', 'mount')
        ],  depends=['order_work'])


    mount_tire = fields.Many2One('parking_place.mount_tires',
        'Montaje Nº', domain=[
            ('vehicle', '=', Eval('vehicle'))
        ],
          depends=['vehicle'],
    )

    km_vehicle = fields.Numeric('Km Vehículo', states={
        'readonly': Eval('state').in_(['disassembly'])
        },)

    disassembly_lines_tires = fields.One2Many(
        'parking_place.disassembly_lines_tires',
        'disassembly_tires','Neumáticos desmontar',

    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('disassembly', 'Desmontar Neumáticos'),
        ('sent',' Desmontado en bodega'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')


    @classmethod
    def __setup__(cls):
        super(DisassemblyTires, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'disassembly'),
            ('disassembly','sent')))
        cls._buttons.update(
            {
                'disassembly': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },
                'sent': {
                    'invisible':
                        ~(Eval('state').in_(['disassembly']))
                },

            })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('disassembly')
    def disassembly(cls, disassemblies):
        for disassembly in disassemblies:
            cls.set_number(disassemblies)
        cls.save(disassemblies)

    @classmethod
    @ModelView.button
    @Workflow.transition('sent')
    def sent(cls, disassemblies):
        pass


    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def set_number(cls, disassemblies):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.disassembly_tires_sequence:
            cls.raise_user_error('no_sequence')
        for disassembly in disassemblies:
            if disassembly.number:
                continue
            disassembly.number = Sequence.get_id(
                config.disassembly_tires_sequence.id)
        cls.save(disassemblies)



    @fields.depends('order_work', 'vehicle', 'km_vehicle')
    def on_change_order_work(self, name=None):
        if self.order_work:
            if self.order_work.vehicle:
                self.vehicle = self.order_work.vehicle
                self.km_vehicle = self.order_work.mileage_arrival


    # @fields.depends('mount_tire', 'disassembly_lines_tires', 'vehicle')
    # def on_change_mount_tire(self, name=None):
    #     pool = Pool()
    #     Line = pool.get('parking_place.disassembly_lines_tires')
    #     if self.mount_tire:
    #         if self.mount_tire.vehicle:
    #             if self.mount_tire.lines_tires:
    #                 for line in self.mount_tire.lines_tires:
    #                     new_line = Line()
    #                     new_line.tires= line.tires
    #                     new_line.code_emac = line.tires.code_emac
    #                     new_line.position= line.position
    #                     new_line.mark = line.tires.mark.name
    #                     new_line.desing = line.tires.desing.name
    #                     self.disassembly_lines_tires = \
    #                     self.disassembly_lines_tires + (new_line,)


class DisassemblyLinesTires(ModelSQL, ModelView):
    'Disassembly Lines Tires'
    __name__ = 'parking_place.disassembly_lines_tires'
    disassembly_tires = fields.Many2One('parking_place.disassembly_tires',
        'Desmontaje'
        )
    tires = fields.Many2One('parking_place.tires', 'Neumático',)
    position = fields.Char('Posición')
    depth = fields.Numeric('Profundidad mm' ,required=True)
    pressure = fields.Numeric('Presión', required=True)
   # disassembly_check = fields.Boolean('Desmontar' ,required=True)
    observation = fields.Char('Observaciones', required=True)


class UpdateTires(Workflow, ModelSQL, ModelView):
    'Update Tires'
    __name__ = 'parking_place.update_tires'

    number = fields.Numeric('Nº Actualización', readonly=True)
    date = fields.Date('Fecha ',
                       states={
                           'readonly': Eval('state').in_(['update'])
                       }, )
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
                              required=True, domain=[
            ('state_mounts', '=', 'mount')
        ])

    km_vehicle = fields.Numeric('Km Actual', states={
        'readonly': Eval('state').in_(['update'])
    }, )
    mount = fields.Many2One('parking_place.mount_tires', 'Montaje Nº',
        domain=[
            ('vehicle', '=', Eval('vehicle'))
        ],
        depends=['vehicle'],
             # domain=[
             #     ('state', '=', 'mount')
             # ],
            required=True, states={
            'readonly': Eval('state').in_(['update'])
        }, )

    tires_lines_update = fields.One2Many('parking_place.update_lines_tires',
        'update_tires','Actualizar Neumáticos')

    state = fields.Selection([
        ('draft', 'Borrador'),
        ('update', 'Actualizar'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(UpdateTires, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'update'),))
        cls._buttons.update(
            {
                'update': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },

            })
        cls._error_messages.update({
            'depth_greater': (
                "Este  valor debe ser mayor a 3 mm "),
        })
        cls._error_messages.update ({
            'repeated_position': ('Posición del neumático repetido')
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('update')
    def update(cls, updates):
        for upd in updates:
            cls.set_number(updates)
            for line in upd.tires_lines_update:
                if line.depth <= 3:
                 cls.raise_user_error('depth_greater')
        cls.save(updates)

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()


    @classmethod
    def set_number(cls, updates):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.actualization_tires_sequence:
            cls.raise_user_error('no_sequence')
        for update in updates:
            if update.number:
                continue
            update.number = Sequence.get_id(
                config.actualization_tires_sequence.id)
        cls.save(updates)

    @fields.depends('mount','tires_lines_update','vehicle')
    def on_change_mount(self, name=None):
        pool = Pool()
        Line = pool.get('parking_place.update_lines_tires')
        if self.mount:
            if self.mount.vehicle:
                if self.mount.lines_tires:
                    for line in self.mount.lines_tires:
                            new_line = Line()
                            new_line.tires = line.tires
                            new_line.position_before = line.position
                            new_line.position= line.position
                            self.tires_lines_update = self.tires_lines_update+\
                                (new_line,)

    @classmethod
    def validate(cls, updates):
        super(UpdateTires, cls).validate(updates)
        for update in updates:
            if update.tires_lines_update:
                cls.check_repeated_positions(update)

    @classmethod
    def check_repeated_positions(cls, update):
        if update.tires_lines_update:
            lines = list(update.tires_lines_update)
            repeated_list = get_repeated_instances(lines, 'position')
            for groups in repeated_list:
                line = groups[0]
                cls.raise_user_error('repeated_position', {

                })


class UpdateTiresLines(ModelSQL, ModelView):
    "Update Lines Tires parking place"
    __name__ = 'parking_place.update_lines_tires'

    update_tires = fields.Many2One('parking_place.update_tires','Vehículo')
    tires = fields.Many2One('parking_place.tires', 'Neumático',
                            # domain=[
                            #     ('state', '=', 'mount')
                            # ]
                            )
    position_before = fields.Selection([
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    ], 'Posición',
    )

    depth = fields.Numeric('Labrado mm')
    pressure = fields.Numeric('Presión PSI')
    rotation = fields.Selection([
        (None, ''),
        ('yes', 'Si'),
        ('No', 'No'),
    ],'Rotación'
    )
    position = fields.Selection([
        (None,''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
    ], 'Nueva Posición')
    observation = fields.Char('Observación')

    @classmethod
    def __setup__(cls):
        super(UpdateTiresLines, cls).__setup__()


class MountsHistoryVehicle(ModelSQL, ModelView):
    'Parking Mounts History Vehicle'
    __name__ = 'parking.mounts.history.vehicle'

    number = fields.Char('Registro No.', readonly=True)
    origin = fields.Reference('Origen',
        [
          ('parking_place.mount_tires', 'Montaje Vehículo'),
          ('parking_place.disassembly_tires','Desmontaje Vehículo'),
        ], readonly=True)
    origin_translated = origin.translated('origin')
    origin_model = fields.Function(fields.Selection(
        'get_origin_models', 'Modelo origen'), 'on_change_with_origin_model')

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
                              readonly=True)

    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__

    @staticmethod
    def _get_origin():
        return [
            'parking_place.mount_tires',
            'parking_place.disassembly_tires'
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def table_query():
        pool = Pool()
        Mount = pool.get('parking_place.mount_tires')
        mount = Mount.__table__()
        Disassembly = pool.get('parking_place.disassembly_tires')
        disassembly = Disassembly.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        where = (Literal(True))
        query = mount.join(vehicle,
            condition=vehicle.id == mount.vehicle
            ).select(
            mount.number.as_('number'),
            Concat('parking_place.mount_tires,',
                   mount.id).as_('origin'),
            mount.vehicle.as_('vehicle'),
            where=mount.state != "draft")

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.number,
            query.origin,
            query.vehicle,
            where=where

        )

    @classmethod
    def __setup__(cls):
        super(MountsHistoryVehicle, cls).__setup__()
        cls._order = [
            ('vehicle', 'DESC')
        ]


class TiresHistoryQuery(ModelSQL, ModelView):
    'Tires History Query'
    __name__ = 'parking.tires.history.query'

    origin = fields.Reference('Origen',
        [
            ('parking_place.mount_lines_tires', 'Neumaticos Montados'),
            ('parking_place.disassembly_lines_tires', 'Desmontado'),
            ('parking_place.tires.retread.lines','Neumaticos Reencauchados'),
        ], readonly=True)
    origin_translated = origin.translated('origin')
    origin_model = fields.Function(fields.Selection('get_origin_models',
        'Modelo origen'), 'on_change_with_origin_model')

    tires = fields.Many2One('parking_place.tires', 'Neumático', readonly=True)
    start_date = fields.Date('Fecha inico', readonly=True)

    date = fields.Date('Fecha ', readonly=True)
    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__

    @staticmethod
    def _get_origin():
        return [
            'parking_place.mount_lines_tires',
            'parking_place.disassembly_lines_tires',
            'parking_place.tires.retread.lines',
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def table_query():
        pool = Pool()

        Tires = pool.get('parking_place.tires')
        tire = Tires.__table__()
        Mount = pool.get('parking_place.mount_tires')
        mount = Mount.__table__()
        MountTiresLines = pool.get('parking_place.mount_lines_tires')
        lines_tires = MountTiresLines.__table__()
        Disassembly = pool.get('parking_place.disassembly_tires')
        disassembly = Disassembly.__table__()
        DisassemblyTires = pool.get('parking_place.disassembly_lines_tires')
        disassembly_tires = DisassemblyTires.__table__()
        Retread = pool.get('parking.tires.retread')
        retread = Retread.__table__()
        RetreadTires = pool.get('parking_place.tires.retread.lines')
        retread_tires = RetreadTires.__table__()

        where = (Literal(True))

        query_mount = mount.join(lines_tires,
            condition=mount.id == lines_tires.mount_tires
            ).join(tire,
            condition=tire.id == lines_tires.tires
            ).select(
            Concat('parking_place.mount_lines_tires,',
                   lines_tires.id).as_('origin'),
            mount.date.as_('start_date'),
            lines_tires.create_date.as_('date'),
            lines_tires.tires.as_('tires'),
            where=(mount.state !='draft')
        )

        # query_unmount = mount.join(disassembly,
        #     condition=mount.id == disassembly.mount_tire
        #     ).join(disassembly_tires,
        #     condition=disassembly.id == disassembly_tires.disassembly_tires
        #     ).join(tire,
        #     condition=tire.id == disassembly_tires.tires
        #     ).select(
        #     Concat('parking_place.disassembly_lines_tires,',
        #            disassembly_tires.id).as_('origin'),
        #     disassembly.date.as_('start_date'),
        #
        #     disassembly_tires.create_date.as_('date'),
        #     disassembly_tires.tires.as_('tires'),
        #     where=((disassembly.state != 'draft') &
        #            (disassembly.date >= mount.date) &
        #            (disassembly_tires.disassembly_check == True))
        # )

        query_unmount = disassembly.join(
            disassembly_tires,
            condition=disassembly.id == disassembly_tires.disassembly_tires
            ).join(tire,
            condition=tire.id == disassembly_tires.tires
            ).select(
            Concat('parking_place.disassembly_lines_tires,',
                   disassembly_tires.id).as_('origin'),
            disassembly.date.as_('start_date'),

            disassembly_tires.create_date.as_('date'),
            disassembly_tires.tires.as_('tires'),
            where=((disassembly.state != 'draft') )
                    #&(disassembly_tires.disassembly_check == True))
        )


        query_retread = tire.join(retread_tires,
            condition=tire.id == retread_tires.tires
            ).join(retread,
            condition=retread.id == retread_tires.retread
            ).select(
            Concat('parking_place.tires.retread.lines,',
                   retread_tires.id).as_('origin'),
            retread.date.as_('start_date'),
            retread_tires.create_date.as_('date'),
            retread_tires.tires.as_('tires'),
            where=((retread.state !='draft'))

        )
        query = Union(query_mount, query_unmount, all_=True)
        union = Union(query,query_retread, all_=True)

        return union.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            union.origin,
            union.tires,
            union.date,
            union.start_date,
            where=where,
            order_by=(union.date,union.start_date)

        )


class TiresRetread(Workflow,ModelSQL,ModelView):
    "Tires Retread"
    __name__ = 'parking.tires.retread'

    company = fields.Many2One('company.company', 'Empresa',
                              states={
                                  'readonly': True
                              }, required=True)
    number = fields.Numeric('Nº', readonly=True)
    date = fields.Date('Fecha ',
        states={
        'readonly': Eval('state').in_(['retread'])
        }, )
    km_vehicle = fields.Numeric('Km Vehículo', states={
        'readonly': Eval('state').in_(['retread'])
    }, )

    lines = fields.One2Many('parking_place.tires.retread.lines',
                                  'retread', 'Neumáticos')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('retread', 'Reencauchar'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(TiresRetread, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'retread'),))
        cls._buttons.update(
            {
                'retread': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },

            })


    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('retread')
    def retread(cls, retreads):
        cls.set_number(retreads)
        pass

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def set_number(cls, retreats):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.retreat_tires_sequence:
            cls.raise_user_error('no_sequence')
        for retreat in retreats:
            if retreat.number:
                continue
            retreat.number = Sequence.get_id(
                config.retreat_tires_sequence.id)
        cls.save(retreats)


class TiresLinesRetread(ModelSQL,ModelView):
    "Tires Retread Lines"
    __name__ = 'parking_place.tires.retread.lines'

    tires = fields.Many2One('parking_place.tires', 'Neumático',
        #domain = [('state', '=', 'disassembly')],
    )
    observation = fields.Char('Observación')
    retread = fields.Many2One('parking.tires.retread', 'Reencauche')


class TiresContextHeader(ModelSQL, ModelView):
    'Tires Context Header'
    __name__ = 'parking.tires.context.header'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')
    tires = fields.One2Many('parking_place.tires', None, 'Neumáticos')
    mark = fields.One2Many('parking_place.mark_tires', None, 'Marca')
    desing = fields.One2Many('parking_place.desing_tires',None, 'Diseño')
    measurement = fields.One2Many('parking_place.measurement_tires', None,
                                  'Medidas')


class TiresQuery(ModelView,ModelSQL):
    'Tires Query'
    __name__ = 'parking.tires.query'

    sentence = fields.Char('Sentencia', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle','Vehiculo')
    tires = fields.Many2One('parking_place.tires','Neumáticos')
    date = fields.Date('Fecha', readonly=True)
    km = fields.Numeric('kilometraje', readonly=True)
    state = fields.Function(fields.Selection(STATE, 'Estado Actual'),
        'on_change_with_state')
    state = state.translated('state')
    mark = fields.Many2One('parking_place.mark_tires', 'Marca')
    desing = fields.Many2One('parking_place.desing_tires', 'Diseño')
    measurement = fields.Many2One('parking_place.measurement_tires',
                                  'Medidas')
    state_history = fields.Selection([
        ('mount', 'Montado'),
        ('disassembly', 'Desmontado'),
        ('sent', 'Desmontado en bodega'),

    ], 'Estado', states={'readonly': True})
    state_translated = state_history.translated('state_history')
    #performance = fields.Float('Rendimiento', digits=(16, 4))


    @fields.depends('tires')
    def on_change_with_state(self, name=None):
        if self.tires.history:
            record = self.tires.history[len(self.tires.history) - 1]
            if record.origin_model == 'parking_place.mount_lines_tires':
                return 'mount'
            if record.origin_model == \
                'parking_place.disassembly_lines_tires':
                return 'disassembly'
            if record.origin_model == 'parking_place.tires.retread.lines':
                return 'retread'
        return 'available'

    @staticmethod
    def table_query():
        context = Transaction().context

        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()
        MarkTires= pool.get('parking_place.mark_tires')
        mark_tires = MarkTires.__table__()
        Desing = pool.get('parking_place.desing_tires')
        desing = Desing.__table__()
        Measurement = pool.get('parking_place.measurement_tires')
        measurement = Measurement.__table__()
        Tires = pool.get('parking_place.tires')
        tire = Tires.__table__()
        Mount = pool.get('parking_place.mount_tires')
        mount = Mount.__table__()
        MountTiresLines = pool.get('parking_place.mount_lines_tires')
        lines_tires = MountTiresLines.__table__()
        Disassembly = pool.get('parking_place.disassembly_tires')
        disassembly = Disassembly.__table__()
        DisassemblyTires = pool.get('parking_place.disassembly_lines_tires')
        disassembly_tires = DisassemblyTires.__table__()
        Retread = pool.get('parking.tires.retread')
        retread = Retread.__table__()
        RetreadTires = pool.get('parking_place.tires.retread.lines')
        retread_tires = RetreadTires.__table__()

        where = (Literal(True))
        where_mount = (Literal(True))
        where_disassembly = (Literal(True))
        sentence = Concat('', '')

        if context.get('start_date'):
            sentence = Concat(sentence,
            Concat('___F. DESDE: ',context.get('start_date')))
            where_mount &= (mount.date >= context.get('start_date'))
            where_disassembly &= (disassembly.date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
            Concat('___F. HASTA: ', context.get('end_date')))
            where_mount &= (mount.date <= context.get('end_date'))
            where_disassembly &= (disassembly.date <= context.get('end_date'))
        if context.get('vehicles'):
            ids = []
            for data in context.get('vehicles'):
                ids.append(data['id'])
            # sentence = Concat(sentence, Concat('____VEHICULO: ',
            # Concat(mark.name, Concat(" ",
            # Concat(model.name,
            # Concat(' ', vehicle.plaque))))))
            where &= (vehicle.id.in_(ids))
        if context.get('tires'):
            ids = []
            for data in context.get('tires'):
                ids.append(data['id'])
            where &= (tire.id.in_(ids))
        if context.get('mark'):
            ids = []
            for data in context.get('mark'):
                ids.append(data['id'])
            where &= (mark_tires.id.in_(ids))
        if context.get('desing'):
            ids = []
            for data in context.get('desing'):
                ids.append(data['id'])
            where &= (desing.id.in_(ids))
        if context.get('measurement'):
            ids = []
            for data in context.get('measurement'):
                ids.append(data['id'])
            where &= (measurement.id.in_(ids))

        query_mount = tire.join(
            desing, condition=tire.desing == desing.id
            ).join(mark_tires, condition=tire.mark == mark_tires.id
            ).join(measurement, condition=tire.measurement == measurement.id
            ).join(lines_tires, condition=lines_tires.tires == tire.id
            ).join(mount, condition=lines_tires.mount_tires == mount.id
            ).join(vehicle,condition= vehicle.id == mount.vehicle
            ).select(
            sentence.as_('sentence'),
            mount.date,
            mount.vehicle.as_('vehicle'),
            mount.km_vehicle.as_('km'),
            mount.state.as_('state_history'),
            lines_tires.tires.as_('tires'),
            tire.desing.as_('desing'),
            tire.mark.as_('mark'),
            tire.measurement.as_('measurement'),

            where=where & where_mount)

        query_unmount = tire.join(
            desing, condition=tire.desing == desing.id
            ).join(mark_tires, condition=tire.mark == mark_tires.id
            ).join(measurement, condition=tire.measurement == measurement.id
            ).join(disassembly_tires,
            condition=disassembly_tires.tires == tire.id
            ).join(disassembly,
            condition=disassembly_tires.disassembly_tires == disassembly.id
            ).join(vehicle, condition= vehicle.id == disassembly.vehicle
            ).select(
            sentence.as_('sentence'),
            disassembly.date,
            disassembly.vehicle.as_('vehicle'),
            disassembly.km_vehicle.as_('km'),
            disassembly.state.as_('state_history'),
            disassembly_tires.tires.as_('tires'),
            tire.desing.as_('desing'),
            tire.mark.as_('mark'),
            tire.measurement.as_('measurement'),
            where=where & where_disassembly)

        query = Union(query_mount, query_unmount, all_=True)

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.sentence,
            query.tires,
            query.vehicle,
            query.km,
            query.state_history,
            query.date,
            query.mark,
            query.desing,
            query.measurement

        )

    @classmethod
    def __setup__(cls):
        super(TiresQuery, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
            ('tires', 'ASC')
        ]
