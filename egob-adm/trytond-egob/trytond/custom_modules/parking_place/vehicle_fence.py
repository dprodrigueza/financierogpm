from trytond_gis import fields as GeoFields
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['VehicleFence']


class VehicleFence(ModelSQL, ModelView):
    "Vehicle Fence"
    __name__ = 'parking.vehicle.fence'

    name = fields.Char("Nombre de geocerca", required=True)
    geofence = GeoFields.Geometry('Geocerca')
    contains_vehicle = fields.Boolean('Vehículo', readonly=True)
    fence_vehicle = fields.Char('Vehículo')


class VehiclePositionHistory(ModelSQL, ModelView):
    "Vehicle Position History"
    __name__ = 'parking.vehicle.location.history'
    date = fields.Date('Fecha')
    coordinates = GeoFields.Point('Ubicacion')
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)