import functools
import pandas as pd
from datetime import datetime
from io import BytesIO
from decimal import Decimal

from sql import Window, Literal, Union
from sql.functions import RowNumber, CurrentTimestamp, ToChar, ToNumber
from sql.conditionals import Coalesce
from sql.operators import Concat
from sql.aggregate import Count

from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, If
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.tools import file_open

__all__ = ['Maintenance', 'MaintenanceEmployee', 'MaintenanceItem',
    'MaintenanceLine', 'MaintenanceLineExternal',
    'MaintenanceLineLiquid', 'MaintenanceSparesQuery',
    'MaintenanceSparesQueryContext', 'MaintenancePreventive',
    'MaintenanceActivities', 'TemplateMaintenanceHeader',
    'TemplateMaintenanceLine', 'TemplateMaintenanceItem',
    'TemplateMaintenanceActivity', 'MaintenancesQuery',
    'MaintenancesQueryContext', 'UploadExternalMaintenanceStart',
    'NewPreventiveMaintenanceStart', 'PersonalActivityQuery',
    'PersonalActivityContext',  'UploadExternalMaintenance',
    'NewPreventiveMaintenance']


def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        _dict.update({field_name: row[i]})
        i += 1
    return _dict


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def employee_field_selection(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        depends=['company'], required=True)


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class Maintenance(Workflow, ModelSQL, ModelView):
    'Vehicle Maintenance'
    __name__ = 'parking.vehicle.maintenance'

    _states = {
        'readonly': (Eval('state') != 'draft')
        }
    _depends = ['state']

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número', states=_states, depends=_depends)
    plannified_start_date = fields.Date('Fecha inicio planificada',
        domain=[
            # If(Eval('start_date'),
            #     ('plannified_start_date', '<=', Eval('start_date')),
            #     ()
            # )
        ], states={
            'readonly': Eval('state').in_(['finished']),
            'required': Eval('state') != 'draft'
        },
        depends=['state', 'start_date'])
    start_date = fields.Date('Fecha inicio',
        domain=[
            If(Eval('end_date'),
                ('start_date', '<=', Eval('end_date')),
                ()
            )
        ], states={
            'readonly': Eval('state').in_(['finished']),
            'required': Eval('state').in_(['finished'])
        },
        depends=['state', 'end_date'])
    start_time = fields.Time('Hora inicio',
        domain=[
            If((Eval('start_date')) & (Eval('end_date'))
                & (Eval('start_date') == Eval('end_date')),
                ('start_time', '<', Eval('end_time')),
                (),
            )
        ], states={
            'readonly': Eval('state').in_(['finished']),
            'required': Eval('state').in_(['finished'])
        },
         depends=['state', 'start_date',
            'end_date', 'end_time']
    )
    estimated_day = fields.Numeric('Días estimados', states={
        'readonly': (Eval('state') != 'draft'),
        'required': True
        },
        depends=_depends)
    estimated_time = fields.Numeric('Tiempo estimado', states={
        'readonly': (Eval('state') != 'draft'),
        'required': True
        },
        depends=_depends)
    template = fields.Many2One('parking.template.maintenance.header',
        'Usar Plantilla', states=_states, depends=['lines', 'state'])

    end_date = fields.Date('Fecha fin',
        # domain=[
        #    If(
        #        (Eval('state') == 'process'),
        #        ('end_date', '>=', Eval('start_date')),
        #        ()
        #    )
        # ],
        states={
            'readonly': Eval('state').in_(['finished']),
            'required': Eval('state').in_(['finished'])
        }, depends=['state', 'start_date']
    )
    end_time = fields.Time('Hora de finalización',
        # domain=[
        #     If(process
        #         (Eval('start_date')) & (Eval('end_date')) &
        #         (Eval('state') == Eval('process')),
        #         ('end_time', '>', Eval('start_time')),
        #         (),
        #     )
        # ],
        states={
            'readonly': Eval('state').in_(['finished']),
            'required': Eval('state').in_(['finished'])
        }, depends=['state', 'start_date', 'start_time', 'end_date']
    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('send', 'Enviado'),
        ('entering', 'Ingresando'),
        ('request', 'Solicitada'),
        ('deny', 'Negado'),
        ('waiting', 'Pendiente'),
        ('handed', 'Repuestos recibidos'),
        ('process', 'En proceso'),
        ('finished', 'Finalizado'),
        ('canceled', 'Anulado'),
    ], 'Estado', states={'readonly': True})

    state_translated = state.translated('state')
    vehicle = fields.Many2One(
        'parking.vehicle.vehicle', 'Vehiculo',
         states=_states, depends=_depends, required=True)
    type = fields.Selection([
        ('preventive', 'Preventivo'),
        ('corrective', 'Correctivo'),
        ('predictive', 'Predictivo')
        ], 'Tipo', states=_states, depends=_depends, required=True)
    type_translated = type.translated('type')
    related_accident = fields.Many2One('parking.vehicle.accident',
        'Accidente relacionado', states={
            'invisible': ~(Eval('type') == 'corrective'),
            'readonly': ~(Eval('state') == 'draft')
        }, domain=[
            ('vehicle', '=', Eval('vehicle'))
        ], depends=['type', 'vehicle'])
    employees = fields.Many2Many('parking.vehicle.maintenance.employee',
        'maintenance', 'employee',
        'Empleados encargados', states={
            'invisible': ~(Eval('workshop_type') == 'own'),
            'readonly': (Eval('state') == 'finished')
            }, depends=['state', 'workshop_type']
    )
    related_maintenance_preventive = fields.Many2One(
        'parking.vehicle.maintenance.preventive',
        'Linea relacionada', states={
            'invisible': ~(Eval('type') == 'preventive'),
            'required': (Eval('type') == 'preventive'),
            'readonly': ~(Eval('state') == 'draft')
        }, domain=[
            ('vehicle', '=', Eval('vehicle'))
        ], depends=['type', 'vehicle', 'state'],
        help='Linea de mantenimiento planificado que se\
        configuró en el vehiculo, a la cual corresponde este mantenimiento')

    workshop = fields.Many2One('parking.workshop', 'Taller asignado',
        states={
            'readonly': (Eval('state') != 'draft')
        }, depends=['state'])

    workshop_type = fields.Function(fields.Selection([
        ('own', 'Propio'),
        ('external', 'Particular/Externo'),
    ], 'Tipo'), 'on_change_with_workshop_type',
        searcher='search_workshop_type')
    from_location = fields.Many2One(
        'stock.location', "Desde almacenamiento",
        states={
            'invisible': True
        }
    )
    workshop_type_translated = workshop_type.translated('workshop_type')
    description = fields.Text('Motivo del mantenimiento',
        states=_states, depends=_depends, required=True)
    description_post_maintenance = fields.Text(
        'Descripción Post Mantenimineto',
        states={
            'required': Eval('state').in_(['finished'])       
        },
        depends=_depends)
    related_shipment = fields.Many2One(
        'stock.shipment.internal',
        'Albarán interno relacionado', states={
            'readonly': True
        }, help='Albarán interno que deberá estar en estado reservado\
        para poder continuar con el mantenimiento')
    lines = fields.One2Many(
        'parking.vehicle.maintenance.line', 'maintenance', 'Repuestos',
        states={
            'readonly': ~(Eval('state').in_(['draft', 'send'])),
        }, depends=['state', 'workshop_type']
    )
    external_lines = fields.One2Many(
        'parking.vehicle.maintenance.line.external', 'maintenance', 'Repuestos',
        states={
            'readonly': Eval('state').in_(['finished']),
            'invisible': ~(Eval('workshop_type') == 'external')
        }, depends=['state', 'workshop_type']
    )
    liquid_lines = fields.One2Many(
        'parking.vehicle.maintenance.line.liquid', 'maintenance',
        'Combustibles y lubricantes', states={
            'readonly': Eval('state').in_(['finished']),
            'invisible': ~(Eval('workshop_type') == 'external')
        }, depends=['state', 'workshop_type']
    )
    activities_maintenance = fields.One2Many(
        'parking.vehicle.maintenance.activities', 'maintenance', 
        'Actividades', states={
            'readonly': Eval('state').in_(['finished']),
        }, depends=['state'])
    mileage_arrival = fields.Numeric("Recorrido al empezar", states={
        'readonly': ~(
                (
                    (Eval('state').in_(['draft', 'request', 'handed'])) &
                    (Eval('workshop_type').in_(['own'])) 
                ) |
                (
                    (Eval('state').in_(['draft'])) &
                    (Eval('workshop_type').in_(['external']))
                )
            ),
        'required': (
                (
                    (Eval('state').in_(['handed'])) &
                    (Eval('workshop_type').in_(['own'])) 
                ) |
                (
                    (Eval('state').in_(['draft'])) &
                    (Eval('workshop_type').in_(['external']))
                )
            ),
    })
    usage_arrival = fields.Numeric("Horas m. al empezar", states={
        'readonly': ~(
                (
                    (Eval('state').in_(['draft', 'request', 'handed'])) &
                    (Eval('workshop_type').in_(['own'])) 
                ) |
                (	
                    (Eval('state').in_(['draft'])) &
                    (Eval('workshop_type').in_(['external']))
                )
            ),
        'required': (
                (
                    (Eval('state').in_(['handed'])) &
                    (Eval('workshop_type').in_(['own'])) 
                ) |
                (
                    (Eval('state').in_(['draft'])) &
                    (Eval('workshop_type').in_(['external']))
                )
            ),
    })
    request_by = fields.Many2One(
        'company.employee', 'Solicitada por',
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': Eval('state').in_(['finished']),
            'required': ~Eval('state').in_(['draft'])
        },
        depends=['company'])
    request_date = fields.Date('Fecha de solicitud', states={
        'readonly': Eval('state').in_(['finished']),
        'required': ~Eval('state').in_(['draft'])
    })
    denied_by = employee_field("Negada por")
    denied_date = fields.Date('Fecha de negación', states={'readonly': True})
    handed_by = employee_field("Recibido por")
    finished_by = employee_field("Finalizado por")

    # handed_date = fields.Date("Fecha de entrega", states={'readonly': True})
    # total_amount = fields.Function(fields.Numeric('Total',
    #     digits=(16, 2),
    #     depends=['lines']), 'on_change_with_total_amount')

    handed_date = fields.Date("Fecha de entrega", readonly=True)
    total_activities = fields.Numeric('Total mano de obra', digits=(16, 2), states={
            'readonly': True
        }, depends=['lines', 'workshop_type'])
    total_activities_estimated = fields.Numeric('Mano de obra (estimado)', digits=(16, 2), states={
            'readonly': True,
            'invisible': Eval('state').in_(['entering','finished'])
        }, depends=['lines', 'workshop_type', 'state'])
    total_spares = fields.Numeric('Total repuestos', digits=(16, 2), states={
            'readonly': True
        }, depends=['lines', 'workshop_type'])
    total_external_spares = fields.Numeric('Total repuestos externos',
        digits=(16, 2), states={
            'readonly': True,
            'invisible': ~(Eval('workshop_type') == 'external')
        }, depends=['external_lines', 'workshop_type'])
    total_liquids = fields.Numeric('Total líquidos', digits=(16, 2), states={
            'readonly': True,
            'invisible': ~(Eval('workshop_type') == 'external')
        }, depends=['liquid_lines'])
    total_amount = fields.Function(fields.Numeric('Subtotal', digits=(16, 2),
        depends=['total_activities', 'total_spares', 'total_liquids',
            'total_external_spares', 'total_activities_estimated']),
        'on_change_with_total_amount')

    @classmethod
    def __setup__(cls):
        super(Maintenance, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'request'),
            ('draft', 'send'),
            ('request', 'handed'),
            ('request', 'waiting'),
            ('request', 'deny'),
            ('handed', 'process'),
            ('waiting', 'handed'),
            ('waiting', 'deny'),
            ('send', 'entering'),
            ('entering', 'finished'),
            ('finished', 'entering'),
            ('process', 'finished'),
            ('send', 'canceled'),
            ('entering', 'canceled'),
            ('request', 'canceled'),
            ('deny', 'canceled'),
            ('waiting', 'canceled'),
            ('handed', 'canceled'),
            ('process', 'canceled')
        ))
        cls._buttons.update(
            {
                'request': {
                    'invisible':
                        ~(Eval('state').in_(['draft'])) |
                        ~(Eval('workshop_type').in_(['own']))
                },

                'send': {
                    'invisible':
                        ~(Eval('state').in_(['draft'])) |
                        ~(Eval('workshop_type').in_(['external']))
                },

                'entering': {
                    'invisible':
                        ~(Eval('state').in_(['send'])) |
                        ~(Eval('workshop_type').in_(['external']))
                },

                'deny': {
                    'invisible': ~(
                        Eval('state').in_(['request','waiting'])
                    )
                },
                'handed': {
                    'invisible': ~(
                        Eval('state').in_(['request', 'waiting'])
                    )
                },
                'waiting': {
                    'invisible': ~(
                        Eval('state').in_(['request'])
                    )
                },

                'process': {
                    'invisible': ~(
                        Eval('state').in_(['handed'])
                    )
                },

                'finish': {
                    'invisible': ~(
                        Eval('state').in_(['process','entering'])
                    )
                },

                'cancel': {
                    'invisible': (
                        Eval('state').in_(['draft', 'finished', 'canceled'])
                    )
                },

                'return_entering': {
                    'invisible': (
                        ~(Eval('state') == 'finished') |
                        (Eval('workshop_type').in_(['own']))
                    )
                },
            })
        cls._error_messages.update({
            'finish_info_required': ('Para finalizar el mantenimiento es '
                'necesario agregar la fecha de finalización, la hora y una '
                'descripción que detalle el mantenimiento realizado.'),
            'no_sequence': ('No existe una secuencia cofigurada para los '
                'mantenimientos.'),
            'no_quantity_request': ('Revise que todas las lineas tengan un '
                'repuesto y una cantidad a solicitar para poder continuar.'),
            'no_from_location': ('Antes de comenzar un mantenimiento se debe '
                'configurar una bodega desde donde se obtendrán los '
                'repuestos.'),
            'no_spare': ('No todas las líneas de repuesto tienen un producto de '
                'bodega asignado. Agreguelos e inténtelo nuevamente'),
            'no_shimpent_done': ('El albarán interno aun no ha sido '
            'notificado como despachado.'),
            'finished_delete_block': ('Los mantenimientos finalizados o anulados'
                ' no pueden ser eliminados'),
            'related_delete_block': ('Los mantenimientos relacionados a un despacho'
                ' de productos no pueden ser eliminados'),
        })
        # m = cls.__table__()
        # cls._sql_constraints += [
        #     ('unique_number',
        #      Unique(m, m.number
        #             ),
        #      'El número de mantenimiento debe ser único'),
        # ]
    
    @classmethod
    def delete(cls, maintenances):
        for maintenance in maintenances:
            if maintenance.related_requirement or \
                maintenance.related_shipment:
                cls.raise_user_error('related_delete_block')
            if maintenance.state == 'finished' or \
                maintenance.state == 'deny':
                cls.raise_user_error('finished_delete_block')
        super(Maintenance, cls).delete(maintenances)

    @classmethod
    def set_number(cls, maintenances):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        if not config.maintenance_vehicle_sequence:
            cls.raise_user_error('no_sequence')
        for maintenance in maintenances:
            if maintenance.number:
                continue
            maintenance.number = Sequence.get_id(
                config.maintenance_vehicle_sequence.id)
            maintenance.description = str(maintenance.number) + ": " + \
                maintenance.description
        cls.save(maintenances)

    @classmethod
    def get_new_counter(cls, maintenances, name=None):
        for maintenance in maintenances:
            maintenance.counter = maintenance.counter + 1
        cls.save(maintenances)

    @fields.depends('workshop')
    def on_change_with_workshop_type(self, name=None):
        if self.workshop:
            if self.workshop.type_:
                return self.workshop.type_

    @fields.depends('template', 'lines', 'activities_maintenance',
        'workshop_type', 'external_lines', 'workshop')
    def on_change_template(self, name=None):
        pool = Pool()
        Line = pool.get('parking.vehicle.maintenance.line')
        ExternalLine = pool.get('parking.vehicle.maintenance.line.external')
        Activity = pool.get('parking.vehicle.maintenance.activities')


        if self.template:
            if self.template.lines:
                for line in self.template.lines:
                    new_line = Line()
                    new_line.description = line.item.name
                    self.lines = self.lines + (new_line,)
                for line in self.template.lines:
                    new_line = ExternalLine()
                    new_line.description = line.item.name
                    self.external_lines = self.external_lines + (new_line,)
            if self.template.activities:
                for item in self.template.activities:
                    activity = Activity()
                    activity.description = item.item.name
                    activity.item = item.item
                    activity.price = item.item.cost_price
                    self.activities_maintenance = self.activities_maintenance\
                        + (activity, )


    @fields.depends('related_maintenance_preventive', 'template',
        'workshop', 'workshop_type')
    def on_change_related_maintenance_preventive(self, name=None):
        if self.related_maintenance_preventive:
           self.template = self.related_maintenance_preventive.template
           self.on_change_template()
    

    @classmethod
    def create(cls, vlist):

        vlist = [v.copy() for v in vlist]
        for values in vlist:
            if 'number' in values:
                if values['number'] == '':
                    values['number'] = None
        return super(Maintenance, cls).create(vlist)    


    @staticmethod
    def default_start_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_from_location(cls):
        pool = Pool()
        Config = pool.get('parking.place.configuration')    
        if Config(1).get_multivalue('from_location'):
            from_location_id = Config(1).get_multivalue(
                'from_location').id
            return from_location_id
        else:
            cls.raise_user_error('no_from_location')
            return None

    @staticmethod
    def default_start_time():
        return datetime.now()
    
    # @staticmethod
    # def default_number():
    #     return None
        
    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_total_spares():
        return Decimal(0.0)
    
    @staticmethod
    def default_total_external_spares():
        return Decimal(0.0)
    
    @staticmethod
    def default_total_activities():
        return Decimal(0.0)
    
    @staticmethod
    def default_total_activities_estimated():
        return Decimal(0.0)

    @staticmethod
    def default_total_liquids():
        return Decimal(0.0)

    @fields.depends('lines')
    def on_change_lines(self, name=None):
        total_spares = Decimal('0.0')
        if self.lines:
            for line in self.lines:
                if line.cost:
                    total_spares += line.cost
            self.total_spares = total_spares
        else:
            self.total_spares = Decimal('0.0')
    
    @fields.depends('external_lines')
    def on_change_external_lines(self, name=None):
        total_external_spares = Decimal('0.0')
        if self.external_lines:
            for line in self.external_lines:
                if line.cost:
                    total_external_spares += line.cost
            self.total_external_spares = total_external_spares
        else:
            self.total_external_spares = Decimal('0.0')

    @fields.depends('activities_maintenance', 'estimated_time', 'employees',
        'workshop_type', 'state')
    def on_change_activities_maintenance(self, name=None):
        total_activities = Decimal('0.0')
        if self.activities_maintenance:
            if self.workshop_type == 'own':
                if self.estimated_time:
                    for employee in self.employees:
                        total_activities += (
                            employee.contract_position.salary /
                            (30*8)) * self.estimated_time
            else:    
                for line in self.activities_maintenance:
                    if line.cost and line.supplier_item == True:
                        total_activities += line.cost
            self.total_activities = total_activities
        else:
            self.total_activities = Decimal('0.0')

        total_activities_estimated = Decimal('0.0')
        if self.activities_maintenance and (
            self.state not in ["finished", "entering"]):
            if self.workshop_type != 'own':
                if self.state == "finished" or self.state == "entering":
                    return   
                for line in self.activities_maintenance:
                    if line.cost and line.supplier_item == False:
                        total_activities_estimated += line.cost
        self.total_activities_estimated = total_activities_estimated
    
    @fields.depends('liquid_lines')
    def on_change_liquid_lines(self, name=None):
        total_liquids = Decimal('0.0')
        if self.liquid_lines:
            for line in self.liquid_lines:
                if line.total:
                    total_liquids += line.total
            self.total_liquids = total_liquids
        else:
            self.total_liquids = Decimal('0.0')
    
    @fields.depends('total_spares', 'total_activities', 'total_liquids',
        'total_external_spares', 'total_activities_estimated', 'workshop',
        'state')
    def on_change_with_total_amount(self, name=None):
        if self.workshop:
            if self.workshop.type_ == 'own':
                return self.total_activities + self.total_spares + \
                    self.total_liquids + self.total_external_spares + \
                    self.total_activities_estimated
            else:
                if self.state:
                    if self.state == 'entering' or \
                        self.state == 'finished':
                        return self.total_activities + self.total_spares + \
                            self.total_liquids + self.total_external_spares
                    else:
                        return self.total_activities + self.total_spares + \
                            self.total_liquids + self.total_external_spares + \
                            self.total_activities_estimated

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        #if not user.employee:
            #cls.raise_user_error('not_employee')
        for maintenance in maintenances:
            if maintenance.workshop_type == "own":
                cls.request_shipment(maintenance)
                # cls.update_mileage_usage(maintenance)
        cls.set_number(maintenances)
        super(Maintenance, cls).save(maintenances)
        # cls.send_email(cls, maintenances, 'request')
        pass
    
    def update_mileage_usage(maintenance):
        pool = Pool()
        UpdateWizard = pool.get('parking.vehicle.update.mileage', type='wizard')
        session_id, _, _ = UpdateWizard.create()
        if not maintenance.vehicle.current_usage:
            maintenance.vehicle.current_usage = Decimal(0.0)
            maintenance.vehicle.save()
        if not maintenance.vehicle.current_mileage:
            maintenance.vehicle.current_usage = Decimal(0.0)
            maintenance.vehicle.save()
        with Transaction().set_context(active_ids=[maintenance.vehicle.id],
                active_id=maintenance.vehicle.id):
            update_wizard = UpdateWizard(session_id)
            update_wizard.start.new_usage = maintenance.usage_arrival
            update_wizard.start.new_mileage = maintenance.mileage_arrival
            update_wizard.start.description = \
                "Ingreso a mantenimiento: " + maintenance.description
            update_wizard.transition_change()


    @classmethod    
    def request_shipment(cls, maintenance):
        pool = Pool()
        Config = pool.get('parking.place.configuration')
        Maintenance = pool.get('parking.vehicle.maintenance')
        to_location_id = Config(1).get_multivalue(
            'to_location').id
        reason_id = Config(1).get_multivalue(
            'shipment_reason').id
        from_location_id = Config(1).get_multivalue(
            'from_location').id
        ShipmentInternal = pool.get('stock.shipment.internal')
        ShipmentReason= pool.get('stock.shipment.reason')
        StockLocation = pool.get('stock.location')
        ProductTemplate = pool.get('product.template')
        StockMove = pool.get('stock.move')
        shipment = ShipmentInternal()
        shipment.planned_date = maintenance.request_date
        shipment.planned_start_date = maintenance.request_date
        shipment.company = maintenance.company
        to_location = StockLocation.browse([to_location_id])[0]
        shipment.to_location = to_location
        shipment.reference = maintenance.description
        reason = ShipmentReason.browse([reason_id])[0]
        shipment.reason = reason
        shipment.on_change_with_reason_categories()
        from_location = StockLocation.browse([from_location_id])[0]
        shipment.from_location = from_location

        # Setting employee
        User = pool.get('res.user')
        user = User(Transaction().user)
        employee = user.employee
        shipment.employee = employee
        shipment.on_change_employee()
        shipment.save()
        moves = []
        for line in maintenance.lines:
            if line.spare and line.quantity_to_request and line.to_request:
                move = StockMove()
                move.shipment = shipment
                move.product = line.spare
                move.uom = line.spare.default_uom
                move.quantity = float(line.quantity_to_request)
                move.from_location = from_location
                move.to_location = to_location
                move.company = maintenance.company
                move.planned_date = maintenance.request_date
                move.department = shipment.department
                moves.append(move)
            else:
                cls.raise_user_error('no_quantity_request')
        StockMove.save(moves)
        ShipmentInternal.wait([shipment])
        maintenance.related_shipment = shipment
        Maintenance.save([maintenance])

    @classmethod
    @ModelView.button
    @Workflow.transition('send')
    def send(cls, maintenances):
        cls.save(maintenances)
        for maintenance in maintenances:
            # cls.request_shipment_in(maintenance)
            cls.update_mileage_usage(maintenance)
            cls.set_number(maintenances)
            if maintenance.lines:
                cls.request_shipment(maintenance)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('entering')
    def entering(cls, maintenances):
        cls.save(maintenances)
        for maintenance in maintenances:
            cls.request_shipment_in(maintenance)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('entering')
    def return_entering(cls, maintenances):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, maintenances):
        pass

    @classmethod
    def request_shipment_in(cls, maintenance):
        pass
        # THIS WOULD BE CHANGED CAUSE THE EXTERNAL ITEMS ARE NO LONGER PRODUCTS
        # pool = Pool()
        # Config = pool.get('parking.place.configuration')
        # to_location_id = Config(1).get_multivalue(
        #     'to_location').id
        # ShipmentIn = pool.get('stock.shipment.in')
        # StockLocation = pool.get('stock.location')
        # StockMove = pool.get('stock.move')
        # PendingProduct = pool.get('stock.shipment.in.pending.product')
        # pool = Pool()
        # User = pool.get('res.user')
        # user = User(Transaction().user)
        # to_location_id = Config(1).get_multivalue(
        #     'to_location').id
        # from_location_id = Config(1).get_multivalue(
        #     'from_location').id
        # employee = user.employee
        # shipment = ShipmentIn()
        # shipment.effective_date = maintenance.request_date
        # shipment.planned_start_date = maintenance.request_date
        # shipment.company = maintenance.company
        # to_location = StockLocation.browse([1])[0]
        # # from_location = StockLocation.browse([from_location_id])[0]
        # # shipment.warehouse = to_location
        # shipment.reference = maintenance.description
        # shipment.supplier = maintenance.workshop.party
        # shipment.on_change_with_supplier_location()
        # shipment.save()
        # moves = []
        # pending_lines = []
        # for line in maintenance.external_lines:
        #     if line.quantity_to_request and line.price:
        #         if line.spare:
        #             move = StockMove()
        #             move.shipment = shipment
        #             move.product = line.spare
        #             move.uom = line.spare.default_uom
        #             move.quantity = float(line.quantity_to_request)
        #             # move.from_location = shipment.warehouse
        #             move.from_location = shipment.supplier_location
        #             move.to_location = to_location
        #             move.company = maintenance.company
        #             move.planned_date = maintenance.request_date
        #             move.currency = maintenance.company.currency
        #             move.on_change_product()
        #             # move.department = employee.contract_department
        #             moves.append(move)
        #         else:
        #             cls.raise_user_error('no_spare')
        # StockMove.save(moves)
        # PendingProduct.save(pending_lines)
        # # shipment.get_incoming_moves()
        # shipment.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('deny')
    @set_employee('denied_by')
    def deny(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for maintenance in maintenances:
            maintenance.denied_date = today
        super(Maintenance, cls).save(maintenances)
        # cls.send_email(cls, maintenances, 'deny')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('handed')
    @set_employee('handed_by')
    def handed(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        Accident = pool.get('parking.vehicle.accident')
        accidents = []
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for maintenance in maintenances:
            if maintenance.related_shipment:
                if maintenance.related_shipment.state != 'done':
                    cls.raise_user_error('no_shimpent_done')
            maintenance.handed_date = today
            if maintenance.related_accident:
                accident = maintenance.related_accident
                # to set accident end date
                accident.maintenance_date = maintenance.start_date
                accidents.append(accident)
        Accident.save(accidents)
        super(Maintenance, cls).save(maintenances)
        # cls.send_email(cls, maintenances, 'authorized')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('process')
    def process(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        for maintenance in maintenances:
            cls.update_mileage_usage(maintenance)
            if maintenance.type == 'preventive':
                related_maintenance = maintenance.related_maintenance_preventive
                related_maintenance.maintenance_related = maintenance.id
                related_maintenance.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def waiting(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        pass

    @classmethod
    @ModelView.button
    @ModelView.button_action(
        'parking_place.wizard_vehicle_new_preventive_maintenance')
    @Workflow.transition('finished')
    @set_employee('finished_by')
    def finish(cls, maintenances):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        now = datetime.now()
        time = now.strftime("%H:%M:%S")
        for maintenance in maintenances:
            if not maintenance.end_date or not maintenance.end_time or \
                    not maintenance.description_post_maintenance:
                cls.raise_user_error('finish_info_required')
        pass

    def send_email(cls, maintenances, state_to):
        pool = Pool()
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Mantenimiento'),
            ('template', '=', True),
        ])
        alerts = []
        for maintenance in maintenances:
            if maintenance:
                employees = []
                if state_to == 'request':
                    Group = pool.get('res.group')
                    search_group = Group.search([('name', '=',
                    'Parque automotor / Vehiculos / Mantenimientos: Autorizar')
                    ])
                    if search_group:
                        for user in search_group[0].users:
                            if user.employee:
                                employees.append(user.employee)
                if state_to in ['handed', 'deny']:
                    employees.append(maintenance.request_by)
                mail_body = cls.generate_mail_body(cls, maintenance, state_to)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                        f"parking.vehicle.maintenance,"
                        f"{maintenance.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                         ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = maintenance
                        # The mail body depends of transition state
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) empleado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_mail_body(cls, maintenance, state_to):
        if state_to == "request":
            return (f"Se ha registrado una nueva solicitud de "
                "mantenimiento para el día: " f"{maintenance.request_date} "
                "con la siguiente descripción: " f"{maintenance.description}. \n"
                "Por favor revisar las solicitudes pendientes en el sistema.")
        elif state_to == "deny":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                "mantenimiento ha sido rechazada  con la descripción siguiente:"
                    f"{maintenance.description}")
        elif state_to == "handed":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                    "mantenimiento ha sido autorizada "
                    "con la descripción siguiente:"
                    f"{maintenance.description} "
                    "Por favor se deberá acercar a retirar "
                    "el documento de la órden de combustible autorizada por el "
                    "jefe de departamento correspondiente.")

    def get_rec_name(self, name):
        if self.number:
            return self.number
        else:
            return 'Borrador - ID:' + str(self.id)
    
    @classmethod
    def search_workshop_type(cls, name, clause):
        pool = Pool()
        Workshop = pool.get('parking.workshop')

        maintenance = cls.__table__()
        workshop = Workshop.__table__()

        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]
        where = Literal(True)
        where = Operator(workshop.type_, value)
        query = workshop.join(maintenance,
            condition=workshop.id == maintenance.workshop
        ).select(
            maintenance.id,
            where=where
        )
        return [('id', 'in', query)]
        
    @classmethod
    def view_attributes(cls):
        return super(Maintenance, cls).view_attributes() + [(
            '//group[@id="group_spares_external"]', 'states', {
                'invisible': ~Eval('workshop_type').in_(['external']),
            }),
            ('//group[@id="group_liquids"]', 'states', {
                'invisible': ~Eval('workshop_type').in_(['external']),
            }),
            ('/form/notebook/page[@id="employees_info"]', 
            'states', {
                'invisible': Eval('workshop_type') == 'external',
            })
        ]


class MaintenanceEmployee(ModelSQL, ModelView):
    'Maintenance Employee'
    __name__ = 'parking.vehicle.maintenance.employee'
    maintenance = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento', required=True)
    employee = fields.Many2One('company.employee', 'Empleado',
        required=True)



class MaintenanceItem(ModelSQL, ModelView):
    'Maintenance Item '
    __name__ = 'parking.vehicle.maintenance.item'
    
    name = fields.Char('Nombre', states={
            "required": True
        })
    code = fields.Char('Código', states={
            "required": True
        })
    type_ = fields.Selection([
        ('spare', 'Repuesto'),
        ('activity', 'Actividad'),
        ('liquid', 'Líquido')
        ], 'Tipo', states={
            "required": True
        })
    cost_price = fields.Numeric('Costo estimado', digits=(16, 2),
        states={
            "required": True
        })

    @classmethod
    def __setup__(cls):
        super(MaintenanceItem, cls).__setup__()

        t = cls.__table__()
        cls._sql_constraints += [
            ('item_code_unique', Unique(t, t.code),
                'El código debe ser único'),
        ]


class MaintenanceLine(ModelSQL, ModelView):
    "Maintenance Line"
    __name__ = 'parking.vehicle.maintenance.line'

    _depends = ['maintenance']

    _states = {
        'readonly':
            ~Eval('_parent_maintenance', {}).get('state').in_(
                ['draft', 'send'])

    }
    description = fields.Char('Descripción', states=_states, required=True,
        depends=_depends)
    spare = fields.Many2One('product.product', 'Repuesto',
        states= {
        'readonly':
            ~Eval('_parent_maintenance', {}).get('state').in_(
                ['draft', 'send']),
        'required':
            Eval('_parent_maintenance', {}).get('state').in_(['entering']),
        }, depends=_depends,
        # domain=[('quantity', '>', 0)],
        context={
            'locations': [
                Eval(
                    '_parent_maintenance', {}
                ).get('from_location')],
              }
    )
    quantity_to_request = fields.Numeric(
        'Cantidad a Solicitar', states={
        'readonly':
            ~Eval('_parent_maintenance', {}).get('state').in_(
                ['draft', 'send']),
        'required': True
        }, depends=_depends)
    cost = fields.Function(fields.Numeric('Costo', states=_states,
        depends=['quantity_to_request', 'spare', 'maintenance'],
        readonly=True), 'on_change_with_cost')
    maintenance = fields.Many2One(
        'parking.vehicle.maintenance', 'Mantenimiento', ondelete='CASCADE')
    to_request = fields.Boolean('Solicitar?', states={
        'readonly':
            ~Eval('_parent_maintenance', {}).get('state').in_(
                ['draft', 'send'])
        }, depends=_depends)
    price = fields.Function(fields.Numeric('Precio U',  
        depends=['spare']), 'on_change_with_price')


    @fields.depends('spare')
    def on_change_with_price(self, name=None):
        if self.spare:
            return self.spare.cost_price

    @fields.depends('quantity_to_request', 'spare')
    def on_change_with_cost(self, name=None):
        if self.quantity_to_request and self.spare and self.spare.cost_price:
            return self.spare.cost_price * self.quantity_to_request
        return 0

    @classmethod
    def default_price(cls):
        return Decimal(0)

    @classmethod
    def default_cost(cls):
        return Decimal(0)
    
    # @staticmethod
    # def default_to_request():
    #     return True


class MaintenanceLineExternal(ModelSQL, ModelView):
    "Maintenance Line"
    __name__ = 'parking.vehicle.maintenance.line.external'

    _depends = ['maintenance']

    _states = {
        'readonly':
            Eval('_parent_maintenance', {}).get('state').in_(['finished']),
        # 'required':
        #     ~(Eval('_parent_maintenance', {}).get('workshop_type').in_(
        #         ['own']
        #     ))
    }

    description = fields.Char('Descripción', states=_states, depends=_depends)
    item = fields.Many2One('product.product', 'Repuesto',
        domain=[('type', 'in', ('assets', 'goods'))],
        states= {
        'readonly':
            Eval('_parent_maintenance', {}).get('state').in_(['finished']),
        'required':
            Eval('_parent_maintenance', {}).get('state').in_(['entering']),
        }, depends=_depends
    )
    quantity_to_request = fields.Numeric(
        'Cantidad a Solicitar', states=_states, depends=_depends)
    cost = fields.Function(fields.Numeric('Costo', states=_states,
        depends=['quantity_to_request', 'item', 'price'],
        readonly=True), 'on_change_with_cost')
    maintenance = fields.Many2One(
        'parking.vehicle.maintenance', 'Mantenimiento', ondelete='CASCADE')
    price = fields.Numeric('Precio U', depends=['item', 'maintenance'], 
        states=_states, )

    @fields.depends('quantity_to_request', 'item', 'price')
    def on_change_with_cost(self, name=None):
        if self.price and self.quantity_to_request:
            return self.price * self.quantity_to_request
        if self.quantity_to_request and self.item and self.item.cost_price:
            return self.item.cost_price * self.quantity_to_request
        return 0

    @classmethod
    def default_price(cls):
        return Decimal(0)


class MaintenanceLineLiquid(ModelSQL, ModelView):
    "Maintenance Line Liquid"
    __name__ = 'parking.vehicle.maintenance.line.liquid'

    _depends = ['maintenance']

    _states = {
        'readonly':
            Eval('_parent_maintenance', {}).get('state').in_(['finished']),
        'required':
            ~(Eval('_parent_maintenance', {}).get('workshop_type').in_(
                ['own']
            ))
    }

    name = fields.Char('Nombre', states=_states, depends=_depends)
    quantity = fields.Numeric('Cantidad ', states=_states, depends=_depends)
    price = fields.Numeric('Precio U', states=_states, depends=_depends)
    total = fields.Function(fields.Numeric('Total', states=_states,
        depends=['quantity', 'price']), 'on_change_with_total')
    maintenance = fields.Many2One(
        'parking.vehicle.maintenance', 'Mantenimiento', ondelete='CASCADE')

    @classmethod
    def default_price(cls):
        return Decimal(0)

    @fields.depends('quantity', 'price')
    def on_change_with_total(self, name=None):
        if self.quantity and self.price:
            return self.price * self.quantity
        return 0


class MaintenanceSparesQuery(ModelSQL, ModelView):
    'Fuel Order Query'
    __name__ = 'parking.vehicle.maintenance.spares.query'
    sentence = fields.Char('Sentencia', readonly=True)
    number = fields.Char('Matenimiento No.', readonly=True)
    date = fields.Date('Fecha', readonly=True)
    vehicle = fields.Char('Vehículo', readonly=True)
    spare = fields.Char('Repuesto',
        readonly=True)
    quantity = fields.Numeric('Cantidad', digits=(16, 2),
        readonly=True)
    price = fields.Numeric('Precio', digits=(16, 2),
                              readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        fuel_type_id = context.get('fuel_type_id')

        pool = Pool()
        Price = pool.get('product.cost_price')
        price = Price.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Line = pool.get('parking.vehicle.maintenance.line')
        line = Line.__table__()
        ExternalLine = pool.get('parking.vehicle.maintenance.line.external')
        external_line = ExternalLine.__table__()
        Product = pool.get('product.product')
        product = Product.__table__()
        Item = pool.get('parking.vehicle.maintenance.item')
        item = Item.__table__()
        Template = pool.get('product.template')
        template = Template.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()
        sentence = Concat('', '')
        where = (maintenance.state == 'finished')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat('___F. DESDE: ',
                                               context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                              Concat('___F. HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date'))
        if context.get('vehicles'):
            ids = []
            for data in context.get('vehicles'):
                ids.append(data['id'])
            where &= (maintenance.vehicle.in_(ids))

        query = maintenance.join(
            vehicle, condition= maintenance.vehicle == vehicle.id
        ).join(
            mark, condition= vehicle.mark == mark.id
        ).join(
            model, condition= vehicle.model == model.id
        ).join(
            line, condition= line.maintenance == maintenance.id
        ).join(
            product, condition= line.spare == product.id
        ).join(
            template, condition= product.template == template.id
        ).join(
            price, condition= product.id == price.product
        ).select(
            sentence.as_('sentence'),
            maintenance.number,
            maintenance.start_date.as_('date'),
            Concat(Concat(mark.name,Concat(' / ',model.name)),
                Concat(' / ',vehicle.plaque)).as_('vehicle'),
            template.name.as_('spare'),
            price.cost_price.as_('price'),
            line.quantity_to_request,
            where=where)
        
        query_items = maintenance.join(
            vehicle, condition= maintenance.vehicle == vehicle.id
        ).join(
            mark, condition= vehicle.mark == mark.id
        ).join(
            model, condition= vehicle.model == model.id
        ).join(
            external_line, condition= external_line.maintenance == \
                maintenance.id
        ).join(
            product, condition= external_line.item == product.id
        ).join(
            template, condition= product.template == template.id
        ).join(
            price, condition= product.id == price.product
        ).select(
            sentence.as_('sentence'),
            maintenance.number,
            maintenance.start_date.as_('date'),
            Concat(Concat(mark.name,Concat(' / ',model.name)),
                Concat(' / ',vehicle.plaque)).as_('vehicle'),
            template.name.as_('spare'),
            price.cost_price.as_('price'),
            external_line.quantity_to_request,
            where=where)
        query_union = Union(query, query_items, all_=True)

        return query_union.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_union.sentence,
            query_union.number,
            query_union.date,
            query_union.vehicle,
            query_union.spare,
            query_union.price,
            query_union.quantity_to_request.as_('quantity'),
        )

    @classmethod
    def __setup__(cls):
        super(MaintenanceSparesQuery, cls).__setup__()
        cls._order = [
            ('date', 'ASC'),
            ('number', 'ASC')
        ]


class MaintenanceSparesQueryContext(ModelView):
    'Fuel Order Query Context'
    __name__ = 'parking.vehicle.maintenance.spares.query.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')


class MaintenancePreventive(ModelSQL, ModelView):
    'parking.vehicle.maintenance.preventive'
    __name__ = 'parking.vehicle.maintenance.preventive'

    maintenance_description = fields.Text('Razón del Manteninimiento')
    date = fields.Date('Fecha de realizacion')
    expected_use = fields.Numeric('Recorrido previsto')
    expected_time = fields.Numeric('Horas maquina prev.')
    template = fields.Many2One('parking.template.maintenance.header',
        'Plantilla')
    maintenance_related = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento', states={'readonly': True})  
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo')

    def get_rec_name(self, name):
        return 'Mantenimiento planificado: %s' % (self.maintenance_description)

    @classmethod
    def __setup__(cls):
        super(MaintenancePreventive, cls).__setup__()
        cls._error_messages.update({
            'lower_mileage': ('Alguna de las lineas de planificación de '
            'mantenimientos tiene un recorrido esperado menor al recorrido '
            'actual del vehículo'),
            'lower_usage': ('Alguna de las lineas de planificación de '
            'mantenimientos tiene un uso esperado menor a las '
            'horas máquina actuales del vehículo')
        })
        cls._order = [
            ('date', 'DESC'),
            ('expected_use', 'ASC'),
            ('expected_time', 'ASC')
        ]
    
    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            vehicle = Vehicle.browse([vals.get('vehicle')])[0]
            if vals.get('expected_use'):
                if vehicle.current_mileage > vals.get('expected_use'):
                    cls.raise_user_error('lower_mileage')
            if vals.get('expected_time'):
                if vehicle.current_usage > vals.get('expected_time'):
                    cls.raise_user_error('lower_usage')
        return super(MaintenancePreventive, cls).create(vlist)


class MaintenanceActivities(ModelSQL, ModelView):
    'Maintenance Activities'
    __name__ = 'parking.vehicle.maintenance.activities'

    _depends = ['maintenance']
    
    _states = {
        'readonly':
            Eval('_parent_maintenance', {}).get('state').in_(['finished']),
        # 'required': True
    }
    description = fields.Char('Descripción', states=_states, depends=_depends)
    item = fields.Many2One('product.product', 'Servicio',
        domain=[('type', '=', 'service')],
        states={
            'readonly':
                Eval('_parent_maintenance', {}).get('state').in_(['finished']),
            'required':
                Eval('_parent_maintenance', {}).get('state').in_(['entering']),
            # 'required': True
        }, depends=_depends
    )
    maintenance = fields.Many2One(
        'parking.vehicle.maintenance', 'Mantenimiento', ondelete='CASCADE')
    quantity_to_request = fields.Numeric(
        'Cantidad', states=_states, depends=_depends)
    price = fields.Numeric('Precio U.', states=_states, depends=['maintenance',
        'item'])
    supplier_item = fields.Boolean('Item de proveedor', states=_states)
    cost = fields.Function(fields.Numeric('Costo', states=_states,
        depends=['quantity_to_request', 'price', 'maintenance'],
        readonly=True), 'on_change_with_cost')

    
    @fields.depends('quantity_to_request', 'price', 'supplier_item', 'maintenance')
    def on_change_with_cost(self, name=None):
        if self.quantity_to_request and self.price:
            if self.maintenance:
                if self.maintenance.workshop_type == 'own':
                    return self.price * self.quantity_to_request
                else:
                    return self.price * self.quantity_to_request
            else:
                return Decimal(0.0)
        else:
            return Decimal(0.0)
    
    @classmethod
    def default_supplier_item(cls):
        return False
        
    @classmethod
    def default_price(cls):
        return Decimal(0)

                    
class TemplateMaintenanceHeader(ModelSQL,ModelView):
    'Template Maintenance'
    __name__ = 'parking.template.maintenance.header'

    name = fields.Char('Nombre')
    date = fields.Date('Fecha creación', readonly=True)
    lines = fields.One2Many(
        'parking.template.maintenance.line', 'header', 'Descripción',
    )
    activities = fields.One2Many(
        'parking.template.maintenance.activity', 'header', 'Descripción',
    )

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()


class TemplateMaintenanceLine(ModelSQL, ModelView):
    'Template Maintenance Line'
    __name__ = 'parking.template.maintenance.line'

    code = fields.Numeric('Código', states={'required': True})
    item = fields.Many2One('product.product', 'Item',
        states={'required': True}, ondelete='CASCADE')
    header = fields.Many2One('parking.template.maintenance.header', 'Cabecera',
         ondelete='CASCADE')

    @classmethod
    def __setup__(cls):
        super(TemplateMaintenanceLine, cls).__setup__()
        cls._order = [
            ('code', 'ASC')
        ]

    def get_rec_name(self, name):
        if self.item:
            return self.item.name


class TemplateMaintenanceItem(ModelSQL, ModelView):
    'Template Maintenance Item'
    __name__ = 'parking.template.maintenance.item'

    name = fields.Char('Descripción', required=True)
    lines = fields.One2Many('parking.template.maintenance.line', 'item',
        'Lineas', states={'invisible': True})


class TemplateMaintenanceActivity(ModelSQL, ModelView):
    'Template Maintenance Activity'
    __name__ = 'parking.template.maintenance.activity'

    code = fields.Numeric('Código', states={'required': True})
    
    item = fields.Many2One('product.product', 'Servicio',
        states={'required': True},
        domain=[
            ('type', '=', 'service'),
        ]
    )
    header = fields.Many2One('parking.template.maintenance.header', 'activities',
         ondelete='CASCADE')


class MaintenancesQuery(ModelSQL, ModelView):
    'Maintenances Query'
    __name__ = 'parking.maintenance.query'

    sentence = fields.Char('Sentencia', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle',
        'Vehiculo')
    budget = fields.Many2One('public.budget',
        'Partida')
    maintenance = fields.Many2One('parking.vehicle.maintenance',
        'Mantenimiento')
    description = fields.Char('Descripción', readonly=True)
    start_date = fields.Date('Fecha inicio', readonly=True)
    end_date = fields.Date('Fecha fin', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('send', 'Enviado'),
        ('entering', 'Ingresando'),
        ('request', 'Solicitada'),
        ('deny', 'Negado'),
        ('waiting', 'Pendiente'),
        ('handed', 'Repuestos recibidos'),
        ('process', 'En proceso'),
        ('finished', 'Finalizado'),
        ('canceled', 'Anulado'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')

    @staticmethod
    def table_query():
        context = Transaction().context
        # maintenance_id = context.get('fuel_type_id')
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()
        Workshop = pool.get('parking.workshop')
        workshop = Workshop.__table__()
        Budget = pool.get('public.budget')
        budget = Budget.__table__()
        
        where = Literal(True)
        sentence = Concat('','')
        if context.get('start_date'):
            sentence = Concat(sentence, Concat('___F. DESDE: ', context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence, Concat('___F. HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date'))
        if context.get('vehicles'):
            ids = []
            for data in context.get('vehicles'):
                ids.append(data['id'])
            sentence = Concat(sentence, Concat('____VEHICULO: ', 
                Concat(mark.name, Concat(" ",
                Concat(model.name, Concat(' ', vehicle.plaque))))))            
            where &= (maintenance.vehicle.in_(ids))
        if context.get('maintenance_type'):
            if maintenance.type =='preventive':
                sentence = Concat(sentence, '____MANT. TIPO: Preventivo')
            elif maintenance.type =='corrective':
                sentence = Concat(sentence, '____MANT. TIPO: Correctivo')
            elif maintenance.type =='predictive':
                sentence = Concat(sentence, '____MANT. TIPO: Predictivo')
            where &= (maintenance.type == context.get('maintenance_type'))
        if context.get('maintenance_state'):
            if maintenance.state =='draft':
                sentence = Concat(sentence,'____ ESTADO: Borrador')
            elif maintenance.state =='send':
                sentence = Concat(sentence,'____ ESTADO: Enviado')
            elif maintenance.state =='entering':
                sentence = Concat(sentence,'____ ESTADO: Ingresando')
            elif maintenance.state =='request':
                sentence = Concat(sentence,'____ ESTADO: Solicitada')
            elif maintenance.state =='deny':
                sentence = Concat(sentence,'____ ESTADO: Negado')
            elif maintenance.state =='waiting':
                sentence = Concat(sentence,'____ ESTADO: Pendiente')
            elif maintenance.state =='handed':
                sentence = Concat(sentence,'____ ESTADO: Repuestos recibidos')
            elif maintenance.state =='process':
                sentence = Concat(sentence,'____ ESTADO: En proceso')
            elif maintenance.state =='finished':
                sentence = Concat(sentence,'____ ESTADO: Finalizado')
            elif maintenance.state =='canceled':
                sentence = Concat(sentence,'____ ESTADO: Anulado')
            where &= (maintenance.state == context.get('maintenance_state'))
        if context.get('workshops'):
            ids = []
            for data in context.get('workshops'):
                ids.append(data['id'])
            sentence = Concat(sentence, Concat('____TALLER: ', workshop.name))
            where &= (maintenance.workshop.in_(ids))
        if context.get('workshop_type'):
            if workshop.type_  =='own':
                sentence = Concat(sentence,'____TIPO TALLER: Propio')
            else:
                sentence = Concat(sentence,'____TIPO TALLER: Externo')
            where &= (workshop.type_ == context.get('workshop_type'))
        if context.get('discard_paids') == True:
            where &= (maintenance.payments_generated == False)

        if context.get('budget'):
            ids = []
            for data in context.get('budget'):
                ids.append(data['id'])
            sentence = Concat(sentence, Concat('____Partida: ', budget.code))
            where &= (maintenance.budget.in_(ids))

        query = maintenance.join(
            vehicle, condition=maintenance.vehicle == vehicle.id
        ).join(
            mark, condition=vehicle.mark == mark.id
        ).join(
            model, condition=vehicle.model == model.id
        ).join(
            workshop, condition=maintenance.workshop == workshop.id
        ).join(
            budget, type_='LEFT', condition=maintenance.budget == budget.id
        ).select(
            sentence.as_('sentence'),
            maintenance.description.as_('description'),
            maintenance.id.as_('maintenance'),
            maintenance.start_date.as_('start_date'),
            maintenance.end_date.as_('end_date'),
            vehicle.id.as_('vehicle'),
            maintenance.state.as_('state'),
            maintenance.budget.as_('budget'),
            where=where)

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.sentence,
            query.start_date,
            query.end_date,
            query.vehicle,
            query.state,
            query.maintenance,
            query.description,
            query.budget
        )

    @classmethod
    def __setup__(cls):
        super(MaintenancesQuery, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]


class MaintenancesQueryContext(ModelView):
    'Maintenances Query Context'
    __name__ = 'parking.maintenance.query.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')
    workshops = fields.One2Many('parking.workshop',None,'Talleres')
    maintenance_type = fields.Selection([
        ('preventive', 'Preventivo'),
        ('corrective', 'Correctivo'),
        ('predictive', 'Predictivo')
        ], 'Tipo Mantenimiento')
    maintenance_state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('send', 'Enviar'),
            ('request', 'Solicitada'),
            ('deny', 'Negado'),
            ('waiting', 'Pendiente'),
            ('handed', 'Repuestos recibidos'),
            ('process', 'En proceso'),
            ('finished', 'Finalizado'),
        ],'Estado Mantenimiento'
    )
    workshop_type = fields.Selection([
        ('own', 'Propio'),
        ('external', 'Particular/Externo'),
    ], 'Tipo Taller')
    discard_paids = fields.Boolean('Descartar pagados?')
    budget = fields.One2Many('public.budget',None,'Por partida')


class UploadExternalMaintenanceStart(ModelView):
    'Upload External Maintenance Start'
    __name__ = 'upload.maintenance.external.start'

    source_file_external_maintenance = fields.Binary('Archivo información a cargar',
        required=True)
    
    template_ext_maintenance = fields.Binary('Plantilla Mantenimiento Externo'
        ' Laboral', filename='template_ext_maintenance_filename',
        file_id='template_ext_maintenance_path', readonly=True)
    template_ext_maintenance_filename = fields.Char('PlantillaMantenimientoExt',
        states={
            'invisible': True,
        })
    template_ext_maintenance_path = fields.Char('Ubicación archivo'
        'laboral', readonly=True)
    
    template_example_info = fields.Binary('Ejemplo información mantenimiento '
        'laboral', filename='template_example_filename',
        file_id='template_example_info_path', readonly=True)
    template_example_filename = fields.Char('EjemploInformaciónMantenimientoExt',
        states={
            'invisible': True,
        })
    template_example_info_path = fields.Char('Ubicación de archivo ejemplo',
        readonly=True)

    @staticmethod
    def default_template_ext_maintenance_path():
        return './data/external_maintenance.xlsx'

    @staticmethod
    def default_template_ext_maintenance_filename():
        return "%s.%s" % ('Plantilla_mantenimiento_externo', 'xlsx')

    @staticmethod
    def default_template_ext_maintenance():
        path = 'parking_place/data/external_maintenance_example.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file

    @staticmethod
    def default_template_example_info_path():
        return './data/external_maintenance_example.xlsx'

    @staticmethod
    def default_template_example_filename():
        return "%s.%s" % ('Ejemplo_plantilla_mantenimiento_externo', 'xlsx')

    @staticmethod
    def default_template_example_info():
        path = 'parking_place/data/external_maintenance_example.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class UploadExternalMaintenance(Wizard):
    'Upload External Maintenance'
    __name__ = 'upload.maintenance.external'

    start = StateView('upload.maintenance.external.start',
                      'parking_place.external_maintenance_start_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Importar', 'import_', 'tryton-executable'),
                      ])
    import_ = StateTransition()

    # @classmethod
    # def __setup__(cls):
    #     super(UploadWorkExperienceEmployee, cls).__setup__()
    #     cls._error_messages.update({
    #         'no_employee': ('Empleado %(employee)s con cédula %(identifier)s en '
    #                         'la línea %(num_line)s no se encuentra registrado '
    #                         'en el sistema para agregar la capacitación ! '),
    #     })

    def transition_import_(self):
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        ExternalLine = pool.get('parking.vehicle.maintenance.line.external')
        Activities = pool.get('parking.vehicle.maintenance.activities')
        Liquid = pool.get('parking.vehicle.maintenance.line.liquid')
        # TemplateActivity = pool.get(
        #     'parking.template.maintenance.activity.item')
        maintenance = Maintenance(Transaction().context['active_id'])

        field_names = ['DESCRIPCION', 'TIPO', 'CANTIDAD',
                       'PRECIO_UNITARIO']

        file_data = fields.Binary.cast(
            self.start.source_file_external_maintenance)
        df = pd.read_excel(BytesIO(file_data),
                           names=field_names, dtype='object', header=0)

        list_work_experiences = []

        for cont, row in enumerate(df.itertuples()):
            items = get_dict_from_readed_row(field_names, row)
            if items['TIPO'] == 'Repuesto':
                new_line = ExternalLine()
                new_line.description = items['DESCRIPCION']
                new_line.quantity_to_request = items['CANTIDAD']
                new_line.price = items['PRECIO_UNITARIO']
                #maintenance.external_lines+=(new_line,)
                new_line.maintenance = maintenance
                new_line.save()
            if items['TIPO'] == 'Actividad':
                activities = Activities()
                activities.description = items['DESCRIPCION']
                activities.quantity_to_request = items['CANTIDAD']
                activities.price = items['PRECIO_UNITARIO']
                # maintenance.activities_maintenance = \
                #     maintenance.activities_maintenance \
                #     + (activities, )
                activities.maintenance = maintenance
                activities.save()
            if items['TIPO']=='Combustible':
                liquid = Liquid()
                liquid.name = items['DESCRIPCION']
                liquid.quantity = items['CANTIDAD']
                liquid.price = items['PRECIO_UNITARIO']
                # if items['CODE']:
                #     template = TemplateActivity.search([
                #         ('code', '=', str(items['CODE']))
                #     ])    
                #     if template:
                #         liquid.activity = template[0]
                # maintenance.liquid_lines = \
                #     maintenance.liquid_lines \
                #     + (liquid, )
                liquid.maintenance = maintenance
                liquid.save()
        Maintenance.save([maintenance])
        return 'end'


class NewPreventiveMaintenanceStart(ModelView):
    'New Preventive Maintenance Start'
    __name__ = 'parking.vehicle.new.preventive.maintenance.start'

    maintenance_description = fields.Text('Razón del Mantenimiento')
    date = fields.Date('Fecha de realizacion')
    expected_use = fields.Numeric('Recorrido previsto')
    expected_time = fields.Numeric('Horas m. previstas')
    template = fields.Many2One('parking.template.maintenance.header',
        'Plantilla')


class NewPreventiveMaintenance(Wizard):
    'New Preventive Maintenance'
    __name__ = 'parking.vehicle.new.preventive.maintenance'

    start = StateView('parking.vehicle.new.preventive.maintenance.start',
        'parking_place.vehicle_new_preventive_maintenance_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'change', 'tryton-ok', default=True),
            ])
    change = StateTransition()

    @classmethod
    def __setup__(cls):
        super(NewPreventiveMaintenance, cls).__setup__()
        cls._error_messages.update({
                'lower_mileage': ('El recorrido o el tiempo ingresado '
                    'es menor al recorrido o horas m. actual del '
                    'vehículo.')
                })

    def transition_change(self):
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        Vehicle = pool.get('parking.vehicle.vehicle')
        maintenance = Maintenance(Transaction().context['active_id'])
        vehicle = maintenance.vehicle
        NewPreventiveMaintenance = pool.get(
            'parking.vehicle.maintenance.preventive')
        # if (self.start.new_mileage < vehicle.current_mileage) or \
        #        ((self.start.new_usage) < vehicle.current_usage):
        #    self.raise_user_error('lower_mileage')
        # else:
        new_preventive_maintenance = NewPreventiveMaintenance()
        new_preventive_maintenance.maintenance_description = self.start.maintenance_description
        new_preventive_maintenance.date = self.start.date
        new_preventive_maintenance.expected_use = self.start.expected_use
        new_preventive_maintenance.expected_time = self.start.expected_time
        new_preventive_maintenance.template = self.start.template
        new_preventive_maintenance.vehicle = vehicle
        NewPreventiveMaintenance.save([new_preventive_maintenance])
        Vehicle.save([vehicle])
        return 'end'


class PersonalActivityContext(ModelView):
    'Personal Activity  Context'
    __name__ = 'parking.personal.activity.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    employees = fields.One2Many('company.employee',
        None, 'Empleados')

class PersonalActivityQuery(ModelSQL, ModelView):
    'Personal Activity Query'
    __name__ = 'parking.personal.activity.query'

    maintenance = fields.Many2One('parking.vehicle.maintenance', 'Mantenimiento',
        readonly=True)
    date = fields.Date('Fecha ', readonly=True)
    start_time = fields.Time('Hora inicio', readonly=True)
    end_time = fields.Time('Hora fin', readonly=True)
    employee = fields.Char('Empleado', readonly=True)
    cost_per_hour = fields.Numeric('Costo por hora', digits=(16, 2), readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        EmployeeMaintenance = pool.get('parking.vehicle.maintenance.employee')
        employee_maintenance = EmployeeMaintenance.__table__()
        Employee = pool.get('company.employee')
        employee = Employee.__table__()
        Contract = pool.get('company.contract')
        contract = Contract.__table__()
        Party = pool.get('party.party')
        party = Party.__table__()
        ContractSalary = pool.get('company.contract.salary')
        contract_salary = ContractSalary.__table__()
        SalaryTableLine = pool.get('company.salary.table.line')
        salary_t_line = SalaryTableLine.__table__()

        where = Literal(True)
        if context.get('start_date'):
            where &= (maintenance.start_date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (maintenance.start_date <= context.get('end_date'))
        if context.get('employees'):
            ids = []
            for data in context.get('employees'):
                ids.append(data['id'])
            where &= (employee_maintenance.employee.in_(ids))
        query = maintenance.join(
            employee_maintenance, condition= employee_maintenance.maintenance == maintenance.id
        ).join(
            employee, condition=employee.id == employee_maintenance.employee
        ).join(
            contract, condition=employee.id == contract.employee
        ).join(
            party, condition=party.id == employee.party
        ).join(contract_salary,
                condition=contract.id == contract_salary.contract
        ).join(salary_t_line,
            condition=(salary_t_line.salary_table ==
                contract_salary.salary_table) & (
                    salary_t_line.position == contract.position)
        ).select(
            maintenance.id.as_('maintenance'),
            maintenance.start_date.as_('date'),
            maintenance.start_time.as_('start_time'),
            maintenance.end_time.as_('end_time'),
            party.name.as_('employee'),
            (Coalesce(salary_t_line.salary, 0) /
                (30*8)).as_('cost_per_hour'),
            where=where)

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.maintenance,
            query.date,
            query.start_time,
            query.end_time,
            query.cost_per_hour,
            query.employee
        )

    @classmethod
    def __setup__(cls):
        super(PersonalActivityQuery, cls).__setup__()
        cls._order = [
            ('date', 'DESC')
        ]


class IndicatorCorrectivePreventiveContext(ModelView):
    'Indicator Correcive Preventive Context'
    __name__ = 'parking.indicator.corrective.preventive.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')


class IndicatorCorrectivePreventive(ModelSQL, ModelView):
    'Indicator Correcive Preventive'
    __name__ = 'parking.indicator.corrective.preventive'

    preventive = fields.Numeric('Preventivos', readonly=True)
    corrective = fields.Numeric('Correctivos', readonly=True)
    month = fields.Char('Mes', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        where = Literal(True)
        sentence = Concat('', '')
        if context.get('start_date'):
            sentence = Concat(sentence,
                Concat('DESDE: ', context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                Concat('  HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date'))

        query_1 = maintenance.select(
            Count(ToChar(maintenance.start_date, 'YYYY/MM')).as_('corrective'),
            ToChar(maintenance.start_date, 'YYYY/MM').as_('month'),
            where=(maintenance.type == 'corrective') & (where),
            group_by=[ToChar(maintenance.start_date, 'YYYY/MM')])
        query_2 = maintenance.select(
            Count(ToChar(maintenance.start_date, 'YYYY/MM')).as_('preventive'),
            ToChar(maintenance.start_date, 'YYYY/MM').as_('month'),
            where=(maintenance.type == 'preventive') & (where),
            group_by=[ToChar(maintenance.start_date, 'YYYY/MM')])

        query_final = query_1.join(query_2, 'LEFT',
            condition= query_1.month == query_2.month
            ).select(
                Coalesce(query_1.corrective,0).as_('corrective'),
                Coalesce(query_2.preventive,0).as_('preventive'),
                Coalesce(query_2.month,query_1.month).as_('month'),
                where=Literal(True)
            )

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_final.corrective,
            query_final.preventive,
            query_final.month
        )
        
    @classmethod
    def __setup__(cls):
        super(IndicatorCorrectivePreventive, cls).__setup__()
        cls._order = [
            ('month', 'ASC')
        ]


# select 
# 	coalesce(sq1.count,0) as "corrective",
# 	coalesce(sq2.count,0) as "preventive",
# 	sq1."date" from 
# (select count(to_char(start_date,'YYYY/MM')), 
# 		to_char(start_date,'YYYY/MM') as "date"
# 		from financiero.parking_vehicle_maintenance pvm 
# 		where "type" = 'corrective'
# 		group by "date") as sq1
# left join (select count(to_char(start_date,'YYYY/MM')), to_char(start_date,'YYYY/MM') as "date"
# 		from financiero.parking_vehicle_maintenance pvm 
# 		where "type" = 'preventive'
# 		group by "date") as sq2
# on sq2."date" = sq1."date"


class IndicatorComplianceContext(ModelView):
    'Indicator Correcive Preventive Context'
    __name__ = 'parking.indicator.compliance.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    goal = fields.Numeric('Meta')

    @staticmethod
    def default_goal():
        return Decimal(0.0)


class IndicatorCompliance(ModelSQL, ModelView):
    'Indicator Compliance'
    __name__ = 'parking.indicator.compliance'

    plannified = fields.Numeric('Planificados', readonly=True)
    excecuted = fields.Numeric('Ejecutados', readonly=True)
    month = fields.Char('Mes', readonly=True)
    goal = fields.Integer('Meta', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Preventive = pool.get('parking.vehicle.maintenance.preventive')
        preventive = Preventive.__table__()
        where_1 = Literal(True)
        where_2 = Literal(True)
        sentence = Concat('', '')
        goal = ToNumber(Concat(context.get('goal'), ''),'999')
        if context.get('start_date'):
            sentence = Concat(sentence,
                Concat('DESDE: ', context.get('start_date')))
            where_1 &= (maintenance.start_date >= context.get('start_date'))
            where_2 &= ((preventive.date >= context.get('start_date')))
        if context.get('end_date'):
            sentence = Concat(sentence,
                Concat('  HASTA: ', context.get('end_date')))
            where_1 &= (maintenance.start_date <= context.get('end_date'))
            where_2 &= (preventive.date <= context.get('end_date'))

        # query_1 = maintenance.select(
        #     Count(ToChar(maintenance.start_date, 'YYYY/MM')).as_('excecuted'),
        #     ToChar(maintenance.start_date, 'YYYY/MM').as_('month'),
        #     where=(maintenance.type == 'preventive') & (where_1),
        #     group_by=[ToChar(maintenance.start_date, 'YYYY/MM')])

        # query_2 = preventive.join(maintenance, 'LEFT',
        #     condition= preventive.maintenance_related == maintenance.id
        # ).select(
        #     Count(ToChar(Coalesce(maintenance.start_date, preventive.date),
        #         'YYYY/MM')).as_('plannified'),
        #     ToChar(Coalesce(maintenance.start_date, preventive.date),
        #         'YYYY/MM').as_('month'),
        #     where=(where_2 | where_1),
        #     group_by=[ToChar(Coalesce(maintenance.start_date, preventive.date),
        #         'YYYY/MM')])
        query_1 = preventive.join(maintenance, 'LEFT',
                condition= preventive.maintenance_related == maintenance.id
            ).select(
            Count(ToChar(maintenance.start_date, 'YYYY/MM')).as_('excecuted'),
            ToChar(maintenance.start_date, 'YYYY/MM').as_('month'),
            where=((where_1) & (maintenance.state != 'draft')),
            group_by=[ToChar(maintenance.start_date, 'YYYY/MM')])

        query_2 = preventive.join(maintenance, 'LEFT',
            condition= preventive.maintenance_related == maintenance.id
        ).select(
            Count(ToChar(Coalesce(maintenance.start_date, preventive.date),
                'YYYY/MM')).as_('plannified'),
            ToChar(Coalesce(maintenance.start_date, preventive.date),
                'YYYY/MM').as_('month'),
            where=(where_2 | where_1),
            group_by=[ToChar(Coalesce(maintenance.start_date, preventive.date),
                'YYYY/MM')])

        query_final = query_1.join(query_2,
            condition= query_1.month == query_2.month
            ).select(
                Coalesce(query_1.excecuted,0).as_('excecuted'),
                Coalesce(query_2.plannified,0).as_('plannified'),
                Coalesce(query_2.month,query_1.month).as_('month'),
                where=Literal(True)
            )

        return query_final.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_final.excecuted,
            query_final.plannified,
            goal.as_('goal'),
            query_final.month
        )
        
    @classmethod
    def __setup__(cls):
        super(IndicatorCompliance, cls).__setup__()
        cls._order = [
            ('month', 'ASC')
        ]


# select 
# 	coalesce(sq1.count,0) as "corrective",
# 	coalesce(sq2.count,0) as "preventive",
# 	sq1."date" from 
# (select count(to_char(start_date,'YYYY/MM')), 
# 		to_char(start_date,'YYYY/MM') as "date"
# 		from financiero.parking_vehicle_maintenance pvm 
# 		where "type" = 'corrective'
# 		group by "date") as sq1
# left join (select count(to_char(start_date,'YYYY/MM')), to_char(start_date,'YYYY/MM') as "date"
# 		from financiero.parking_vehicle_maintenance pvm 
# 		where "type" = 'preventive'
# 		group by "date") as sq2
# on sq2."date" = sq1."date"
