from decimal import Decimal
import operator
import collections
from collections import OrderedDict, defaultdict
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
mpl.use('Agg')
from matplotlib import pyplot
from io import BytesIO

from trytond.modules.hr_ec.company import CompanyReport ,CompanyReportSignature
from trytond.pool import Pool
from trytond.transaction import Transaction


__all__ = [
    'MobilizationOrderReport', 'MobilizationRequest', 'AccidentVehicle',
    'FuelOrderReport', 'MaintenanceReport', 'WorkOrderReport', 
    'VehicleHistory',
    'FuelOrderQuery1', 'VehicleTemplate', 'WorkshopQueryReport',
    'MaintenanceSparesReport', 'MaintenanceCheckReport',
    'MaintenancePredictiveReport', 'MaintenancePreventive',
    'MaintenanceSummary', 'AvailabilityVehicleCategory']


class MobilizationOrderReport(CompanyReport):
    __name__ = 'parking.mobilization.order.report'


class MobilizationRequest(CompanyReport):
    __name__ = 'parking.mobilization.request.report'


class AccidentVehicle(CompanyReport):
    __name__ = 'parking.vehicle.accident.report'


class FuelOrderReport(CompanyReport):
    __name__ = 'parking.fuel_order.report'


class MaintenanceReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.report'


class WorkOrderReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.work.report'


class MaintenancePredictiveReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.predictive.report'


class MaintenanceSparesReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.spares.report'


class MaintenanceCheckReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.check.report'


class VehicleTemplate(CompanyReport):
    __name__ = 'parking.vehicle.template.report'


class VehicleHistory(CompanyReport):
    __name__ = 'parking.vehicle.history.report'


class FuelOrderQuery1(CompanyReport):
    __name__ = 'parking.fuel_order.query.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            FuelOrderQuery1, cls).get_context(records, data)
        report_context['total'] = cls.get_total(records)
        report_context['total_fuels'] = cls.total_fuels(records)
        original_list = []
        new_list = []
        for order in records:
            if order.vehicle:
                original_list.append(order.vehicle)
            else:
                original_list.append('SIN_VEHICULO')
        new_list = list(set(original_list))
        dictionari = {}
        for vehicle in new_list:
            dictionari[vehicle] = []

        #Se ordena cada grupo por vehículo.
        new_list_sorted = []
        dic = dict()
        for record in records:
            if not record.vehicle:
                if 'SIN_VEHICULO' in dic.keys():
                    dic['SIN_VEHICULO'].append(record)
                else:
                    dic['SIN_VEHICULO'] = []
                    dic['SIN_VEHICULO'].append(record)
            else:
                if record.vehicle in dic.keys():
                    dic[record.vehicle].append(record)
                else:
                    dic[record.vehicle] = []
                    dic[record.vehicle].append(record)
            
        # Se ordena las órdenes por fecha en cada grupo.
        dic = collections.OrderedDict(sorted(dic.items()))
        for d in dic:
            new_list_sorted.extend(sorted(dic[d],
            key=lambda objeto: objeto.date))
        d = 0
        v = ''
        complete_list = []
        dd = []
        for i in range(0, len(new_list_sorted)):
            if new_list_sorted[i].vehicle == None:
                new_list_sorted[i].vehicle = 'SIN_VEHICULO'
        for i in range(0, len(new_list_sorted)):
            complete_list.append([])
            partial_list = []
            if new_list_sorted[i].vehicle != v:
                d = 0
            else:
                d = new_list_sorted[i].mileage - new_list_sorted[i-1].mileage
            v = new_list_sorted[i].vehicle
            partial_list.append(str(new_list_sorted[i].vehicle))
            partial_list.append(str(new_list_sorted[i].date))
            partial_list.append(str(new_list_sorted[i].number))
            partial_list.append(str(new_list_sorted[i].gallons_number))
            partial_list.append(str(new_list_sorted[i].fuel_cost))
            partial_list.append(str(new_list_sorted[i].mileage))
            if d == 0:
                partial_list.append('')
            else:
                partial_list.append(str(d))
            partial_list.append(str(new_list_sorted[i].description))
            if d != 0 and new_list_sorted[i].gallons_number != 0:
                partial_list.append(round(d/new_list_sorted[i].gallons_number, 4))
            else:
                partial_list.append('')
            partial_list.append(str(new_list_sorted[i].iva))
            partial_list.append(str(new_list_sorted[i].fuel_cost_iva))
            complete_list[i] = partial_list
        d2 = 0
        for vehicle in new_list:
            aux_list = [0, 0, 0, 0, 0, None]
            for order in records:
                if order.vehicle == vehicle:
                    aux_list[1] = aux_list[1] + order.gallons_number
                    aux_list[2] = aux_list[2] + order.fuel_cost
                    aux_list[3] = aux_list[3] + order.iva
                    aux_list[4] = aux_list[4] + order.fuel_cost_iva
                    aux_list[5] = order.vehicle_id
            d2 = 0
            for i in range(0, len(complete_list)):
                if complete_list[i][0] == vehicle:
                    if complete_list[i][6] != '':
                        d2 = d2 + Decimal(complete_list[i][6])
            aux_list[0] = d2
            dictionari[vehicle] = aux_list
        
        dic2 = dict()
        for record in complete_list:
            if record[0] not in dic2.keys(): #record[0] rec_name de vehiculo.
                dic2[record[0]] = []
            dic2[record[0]].append(record)
        #Comentario para cuando este en la EMAC, dic2 cambiar por complete_list.
        report_context['orders'] = dictionari
        report_context['vehicles'] = dic2
        return report_context

    @classmethod
    def get_total(cls, orders):
        total = Decimal(0.0)
        for order in orders:
            total = total + order.fuel_cost
        return total

    @classmethod
    def total_fuels(cls, orders):
        complete_list = []
        dic_gallons = {}
        dic_sub = {}
        dic_iva = {}
        dic_total = {}
        for order in orders:
            if order.fuel_type in dic_gallons.keys():
                dic_gallons[order.fuel_type] = dic_gallons[order.fuel_type] + order.gallons_number
            else:
                dic_gallons[order.fuel_type] = order.gallons_number
            if order.fuel_type in dic_sub.keys():
                dic_sub[order.fuel_type] = dic_sub[order.fuel_type] + order.fuel_cost
            else:
                dic_sub[order.fuel_type] = order.fuel_cost
            if order.fuel_type in dic_iva.keys():
                dic_iva[order.fuel_type] = dic_iva[order.fuel_type] + order.iva
            else:
                dic_iva[order.fuel_type] = order.iva
            if order.fuel_type in dic_total.keys():
                dic_total[order.fuel_type] = dic_total[order.fuel_type] + order.fuel_cost_iva
            else:
                dic_total[order.fuel_type] = order.fuel_cost_iva
        sum_gallons = sum(dic_gallons.values())
        sum_sub = sum(dic_sub.values())
        sum_iva = sum(dic_iva.values())
        sum_total = sum(dic_total.values())

        complete_list.append(dic_gallons)
        complete_list.append(dic_sub)
        complete_list.append(dic_iva)
        complete_list.append(dic_total)
        complete_list.append(sum_gallons)
        complete_list.append(sum_sub)
        complete_list.append(sum_iva)
        complete_list.append(sum_total)
        return complete_list


class WorkshopQueryReport(CompanyReport):
    __name__ = 'parking.workshop.query.report'


class MaintenanceSparesQueryReport(CompanyReport):
    __name__ = 'parking.vehicle.maintenance.spares.query.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            MaintenanceSparesQueryReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)
        return report_context

    @classmethod
    def _get_sentences(cls, maintenances):
        sentence_dict = {}
        for maintenance in maintenances:
            if maintenance.sentence not in sentence_dict:
                sentence_dict[maintenance.sentence] = []
            sentence_dict[maintenance.sentence].append(maintenance)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class MaintenancePreventive(CompanyReport):
    __name__ = 'parking.maintenance.preventive.report'


class MaintenanceSummary(CompanyReport):
    __name__ = 'parking.maintenance.summary.report'


class AvailabilityVehicleCategory(CompanyReport):
    __name__ = 'parking.availability.vehicle.category.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            AvailabilityVehicleCategory, cls).get_context(records, data)
        diccionario = dict()
        for record in records:
            if record.category.name in diccionario.keys():
                diccionario[record.category.name].append(record)
            else:
                diccionario[record.category.name] = []
                diccionario[record.category.name].append(record)
        report_context['vehicles'] = diccionario
        return report_context


class MaintenancesQueryReport(CompanyReport):
    __name__ = 'parking.maintenance.query.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            MaintenancesQueryReport, cls).get_context(records, data)
        report_context['sentences'] = cls._get_sentences(records)
        report_context['totals'] = cls._get_totals(records)
        return report_context

    @classmethod
    def _get_sentences(cls, maintenances):
        sentence_dict = {}
        for maintenance in maintenances:
            if maintenance.sentence not in sentence_dict:
                sentence_dict[maintenance.sentence] = []
            sentence_dict[maintenance.sentence].append(maintenance)
        sentences = [x for x in sentence_dict.values()]
        return sentences
    
    @classmethod
    def _get_totals(cls, maintenances):
        totals = [Decimal(0.0), Decimal(0.0), Decimal(0.0)]
        for maintenance in maintenances:
            totals[0] += maintenance.maintenance.total_activities
            totals[1] += maintenance.maintenance.total_spares + \
                maintenance.maintenance.total_external_spares
            totals[2] += maintenance.maintenance.total_amount
        return totals


class PersonalActivityReport(CompanyReport):
    __name__ = 'parking.personal.activity.query.report'
    
    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PersonalActivityReport, cls).get_context(records, data)
        report_context[
            'employees'], report_context[
            'totals_dict'], report_context[
            'empleados'], report_context[
            'total_states'], = \
            cls._get_employees(records)
        return report_context

    @classmethod
    def _get_employees(cls, records):
        employees_dict = {}
        totals_dict = {}
        for record in records:
            if record.employee not in employees_dict:
                employees_dict[record.employee] = []
                totals_dict[record.employee] = {
                    'cost_per_hour': round(record.cost_per_hour, 2),
                    'total_hours': Decimal('0.0'),
                    'total_cost': Decimal('0.0')}
            cost_per_hour = record.cost_per_hour
            if record.maintenance:
                if record.maintenance.estimated_time:
                    totals_dict[record.employee]['total_hours'] += \
                        record.maintenance.estimated_time
                    totals_dict[record.employee]['total_cost'] += \
                        round(record.maintenance.estimated_time *
                            cost_per_hour, 2)
            employees_dict[record.employee].append(record.maintenance)
        employees = employees_dict

        employees_2 = OrderedDict(sorted(employees.items(), key=lambda x: x))

        dict_states = {}
        for record in records:
            if record.employee not in dict_states:
                dict_states[record.employee] = {}
            if not record.maintenance.state_translated in dict_states[record.employee].keys():
                dict_states[record.employee][record.maintenance.state_translated] = Decimal(0.0)
            dict_states[record.employee][record.maintenance.state_translated] += 1

        for x in dict_states:
            t = Decimal('0')
            for y in dict_states[x]:
                t += dict_states[x][y]
            dict_states[x]['SUMA'] = t

        states = OrderedDict(sorted(dict_states.items(), key=lambda x: x))
        
        dict_total = {}
        sum_ = Decimal('0')
        for x in dict_states:
            for y in dict_states[x]:
                if y != 'SUMA':
                    if y not in dict_total.keys():
                        dict_total[y] = Decimal('0')
                    dict_total[y] += dict_states[x][y]
                else:
                    sum_ += dict_states[x][y]

        dict_total['SUMA'] = sum_

        return employees_2, totals_dict, states, dict_total,


class TiresReport(CompanyReportSignature):
    __name__ = 'parking.tires.report'


class DisassemblyTiresReport(CompanyReportSignature):
    __name__ = 'parking.disassembly.tires.report'


class TiresQueryReport(CompanyReportSignature):
    __name__ = 'parking.tires.query.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            TiresQueryReport, cls).get_context(records, data)
        tires_dict = {}
        tires_hist = {}
        for record in records:
            if not record.tires.id in tires_dict.keys():
                tires_dict[record.tires.id] = []
                tires_dict[record.tires.id].append({
                    "km": Decimal(0.0),
                    "performance": "-",
                    "record": record 
                })
                tires_hist[record.tires.id] = record.km
            else:
                next_tire_km = tires_hist[record.tires.id]
                if tires_hist[record.tires.id] < record.km:
                    next_tire_km = tires_hist[record.tires.id] + \
                        (tires_hist[record.tires.id] - record.km)
                tires_dict[record.tires.id].append({
                    "km": next_tire_km,
                    "performance": round(record.tires.product.cost_price / \
                        next_tire_km, 2),
                    "record": record 
                })
                tires_hist[record.tires.id] = next_tire_km
        report_context['tires_dict'] = tires_dict
        report_context['sentences'] = cls._get_sentences(records)
        return report_context

    @classmethod
    def _get_sentences(cls, tires):
        sentence_dict = {}
        for tire in tires:
            if tire.sentence not in sentence_dict:
                sentence_dict[tire.sentence] = []
            sentence_dict[tire.sentence].append(tire)
        sentences = [x for x in sentence_dict.values()]
        return sentences


class IndicatorCorrectivePreventiveReport(CompanyReportSignature):

    __name__ = 'parking.indicator.corrective.preventive.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            IndicatorCorrectivePreventiveReport, cls).get_context(records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user'] = user
        fig, ax = plt.subplots(figsize=(6, 4), subplot_kw=dict(
            aspect="equal"))
        labels = [x.month for x in records]
        data_preventive = [x.preventive for x in records]
        data_corrective = [x.corrective for x in records]
        handles, ax_labels = ax.get_legend_handles_labels()
        ax.legend(handles, ax_labels)
        ax.legend(handles, data_corrective,
                  title="Mantenimientos",
                  loc="center left",
                  bbox_to_anchor=(0, 0, 0, 0))
        plt.grid(True)
        plt.grid(color='b', linestyle='-', linewidth=1)
        plt.figure(figsize=(10,10))
        plt.setp(ax.get_xticklabels(), size=10, weight="bold")
        plt.plot(labels, data_corrective, '-bo')
        plt.plot(labels, data_preventive, '-ro')
        plt.xticks(rotation=45)
        plt.xlabel('Meses')
        plt.ylabel('Nro. mantenimientos')
        f = BytesIO()
        # ax.set_title("Indicador correctivos vs preventivos")
        plt.rcParams["figure.figsize"] = (10,10)
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        plt.close("all")
        return report_context


class IndicatorComplianceReport(CompanyReportSignature):
    __name__ = 'parking.indicator.compliance.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            IndicatorComplianceReport, cls).get_context(records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user'] = user

        fig, ax = plt.subplots(figsize=(6, 4), subplot_kw=dict(
            aspect="equal"))
        labels = [x.month for x in records]
        data_plannified = [x.plannified for x in records]
        data_excecuted = [x.excecuted for x in records]
        data_goal = [x.goal for x in records]
        handles, ax_labels = ax.get_legend_handles_labels()
        ax.legend(handles, ax_labels)
        ax.legend(handles, data_excecuted,
                  title="Mantenimientos",
                  loc="center left",
                  bbox_to_anchor=(0, 0, 0, 0))
        plt.grid(True)
        plt.grid(color='b', linestyle='-', linewidth=1)
        plt.figure(figsize=(10,10))
        plt.setp(ax.get_xticklabels(), size=10, weight="bold")
        plt.plot(labels, data_excecuted, '-bo')
        plt.plot(labels, data_plannified, '-ro')
        plt.plot(labels, data_goal, '-go')
        plt.xticks(rotation=45)
        plt.xlabel('Meses')
        plt.ylabel('Nro. mantenimientos')
        f = BytesIO()
        # ax.set_title("Indicador correctivos vs preventivos")
        plt.rcParams["figure.figsize"] = (10,10)
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        plt.close("all")
        return report_context


class IndicatorAvailabilityReport(CompanyReportSignature):
    __name__ = 'parking.indicator.availability.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            IndicatorAvailabilityReport, cls).get_context(records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user'] = user

        fig, ax = plt.subplots(figsize=(6, 4), subplot_kw=dict(
            aspect="equal"))
        labels = [str(x.vehicle.number_vehicle) for x in records]
        confidence_percentage = [x.confidence_percentage for x in records]
        # handles, ax_labels = ax.get_legend_handles_labels()
        # ax.legend(handles, ax_labels)
        # ax.legend(handles, confidence_percentage,
        #           title="Mantenimientos",
        #           loc="center left",
        #           bbox_to_anchor=(0, 0, 0, 0))
        # plt.bar(labels, confidence_percentage, color='blue')
        # plt.figure(figsize=(10,10))
        # plt.setp(ax.get_xticklabels(), size=10, weight="bold")
        # plt.xlabel('')
        # plt.ylabel('')

        x_pos = [i for i, _ in enumerate(labels)]
        plt.figure(figsize=(10,5))
        plt.bar(labels, confidence_percentage, color='blue', width = 0.4)
        plt.xlabel("Vehículo")
        plt.ylabel("Confiabilidad")
        plt.title("Indicador de congiabilidad")
        plt.xticks(x_pos, labels)



        f = BytesIO()
        # ax.set_title("Indicador correctivos vs preventivos")
        plt.rcParams["figure.figsize"] = (10,5)
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        plt.close("all")
        return report_context


class IndicatorAvailabilityByMonthReport(CompanyReportSignature):
    __name__ = 'parking.indicator.availability.by_month.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            IndicatorAvailabilityByMonthReport, cls).get_context(records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user'] = user

        fig, ax = plt.subplots(figsize=(6, 4), subplot_kw=dict(
            aspect="equal"))
        labels = [str(x.month) for x in records]
        confidence_percentage = [x.confidence_percentage for x in records]
        handles, ax_labels = ax.get_legend_handles_labels()
        ax.legend(handles, ax_labels)
        ax.legend(handles, confidence_percentage,
                  title="Mantenimientos",
                  loc="center left",
                  bbox_to_anchor=(0, 0, 0, 0))
        plt.grid(True)
        plt.grid(color='b', linestyle='-', linewidth=1)
        plt.figure(figsize=(10,10))
        plt.setp(ax.get_xticklabels(), size=10, weight="bold")
        plt.plot(labels, confidence_percentage, '-bo')
        plt.xticks(rotation=45)
        plt.xlabel('Meses')
        plt.ylabel('% Confiabilidad')
        f = BytesIO()
        # ax.set_title("Indicador correctivos vs preventivos")
        plt.rcParams["figure.figsize"] = (10,10)
        plt.savefig(f, format="png")
        report_context['image'] = f.getvalue()
        plt.close("all")
        return report_context


class IndicatorVehicleCostReport(CompanyReportSignature):
    __name__ = 'parking.indicator.vehicle.cost.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            IndicatorVehicleCostReport, cls).get_context(records, data)
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        report_context['user'] = user
        labels = []
        data = []
        for record in records:
            labels.append(record.vehicle)
            data.append(record.cost)
        fig, ax = plt.subplots(figsize=(8, 6))
        # labels = [x.vehicle for x in records]
        # data = [x.cost for x in records]
        #
        # def func(pct, allvals):
        #     absolute = int(Decimal(pct) / Decimal(100.) * Decimal(np.sum(allvals)))
        #     return "{:.1f}%\n({:d} )".format(pct, absolute)
        # wedges, texts, autotexts = ax.pie(data,
        #           autopct=lambda pct: func(pct, data),
        #           textprops=dict(color="w"))
        # ax.legend(wedges, labels,
        #             fontsize='x-small',
        #           title="Costos por vehículo",
        #           loc="lower center",
        #           bbox_to_anchor=(0, -0.5, 0.5, 0.5))

        ax.bar(labels, data, align='center', alpha=0.2)
        plt.xticks(rotation=270)
        # plt.setp(autotexts, size=5, weight="bold")
        f = BytesIO()
        ax.set_title("Costos por Vehículo")
        plt.savefig(f, format="png")
        plt.close("all")
        report_context['image'] = f.getvalue()

        return report_context



class PeriodictTaskByExecutedReport(CompanyReportSignature):
    __name__ = 'parking.periodict.task.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            PeriodictTaskByExecutedReport, cls).get_context(records, data)
        #report_context['maintenances'] = cls._get_vehicles(records)
        return report_context

    # @classmethod
    # def _get_vehicles(cls, maintenances):
    #     vehicles_dict = {}
    #     for maintenance in maintenances:
    #         vehicles_dict[maintenance.vehicle.id]={
    #             'number':maintenance.vehicle.number,
    #             'plaque':maintenance.vehicle.plaque,
    #             'mark':maintenance.vehicle.mark.name,
    #             'model':maintenance.vehicle.model.name,
    #             'description':maintenance.description,
    #             'date':maintenance.date
    #
    #         }
    #     vehicles = [x for x in vehicles_dict.values()]
    #     return vehicles

