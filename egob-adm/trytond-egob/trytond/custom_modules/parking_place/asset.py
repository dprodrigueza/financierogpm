from collections import defaultdict

from trytond.model import fields, ModelSQL
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval
from trytond.transaction import Transaction
from trytond.tools import reduce_ids, grouped_slice


__all__ = ['Asset']


class Asset(metaclass=PoolMeta):
    'Asset'
    __name__ = 'asset'

    # TODO: implemente a Funtion Many2One vehicle_ field in order to
    # can search asset with vehicle plaque
    # Other option is change vehicle from One2One -> Many2Many with size=1
    vehicle = fields.One2One(
        'asset-parking.vehicle.vehicle', 'asset', 'vehicle', 'Item mantenimientos',
        states={
            'readonly': Eval('state') != 'draft'
        }, depends=['asset_type'])
    vehicle_maintenances = fields.Function(
        fields.One2Many('parking.vehicle.maintenance', None, 'Mantenimientos',
            states={
                'invisible': ~Bool(Eval('vehicle'))
            }, depends=['vehicle']), 'get_vehicle_maintenances')

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        cls.maintenances.states.update({
            'invisible': Bool(Eval('vehicle'))
        })
        cls.maintenances.depends += ['vehicle']

    # @classmethod
    # def view_attributes(cls):
    #     return super(Asset, cls).view_attributes() + [(
    #         '//page[@id="vehicle_attributes"]', 'states', {
    #             'invisible': ~Eval('asset_type').in_(['vehicle']),
    #         }),
    #     ]

    @classmethod
    def get_vehicle_maintenances(cls, assets, names):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        table = cls.__table__()
        AssetVehicle = pool.get('asset-parking.vehicle.vehicle')
        asset_vehicle = AssetVehicle.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        maintenances = defaultdict(lambda: [])
        ids = [a.id for a in assets]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(asset_vehicle,
                condition=asset_vehicle.asset == table.id
            ).join(maintenance,
                condition=asset_vehicle.vehicle == maintenance.vehicle
            ).select(
                table.id,
                maintenance.id.as_('maintenance'),
                where=red_sql,
            )
            cursor.execute(*query)
            for asset_id, maintenance_id in cursor.fetchall():
                maintenances[asset_id].append(maintenance_id)
        return {
            'vehicle_maintenances': maintenances,
        }


class AssetVehicle(ModelSQL):
    "Public Project POA"
    __name__ = "asset-parking.vehicle.vehicle"
    asset = fields.Many2One('asset', 'Activo',
        select=True, required=True, ondelete='CASCADE')
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        select=True, required=True, ondelete='CASCADE')
