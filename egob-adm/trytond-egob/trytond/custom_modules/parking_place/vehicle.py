import functools
import datetime as dt
from datetime import datetime, date
from decimal import Decimal
from collections import defaultdict

from sql import Window, Literal, Union, Null
from sql.functions import RowNumber, CurrentTimestamp, Age, DatePart, \
    ToNumber, ToChar, ToDate
from sql.operators import Concat, NotIn
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Or, And
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond_gis import fields as GeoFields

__all__ = ['Category', 'FuelType', 'FuelOrder',
           'Vehicle', 'VehicleHistory', 'VehicleMileageUpdate',
          'VehicleAccident',
           'VehicleDriver', 'Mark', 'Model', 'Colour',
           'FuelOrderPayment']

MAX_DATE = dt.date.max

STATE = [
    ('available', 'Disponible'),
    ('busy', 'Ocupado'),
    ('maintenance', 'En mantenimiento'),
    ('accidented', 'Accidentado'),
]

STATE_MOUNTS = [
    ('mount', 'Montado'),
    ('disassembly', 'Desmontado'),
]


STATES_CONTRACTED = {
    'readonly': And(Eval('type') != 'contracted',
        Eval('type') != 'institutional'),
    'required': Or(Eval('type') == 'contracted',
        Eval('type') == 'institutional')
}

STATES_ACTIVE = {
    'readonly': Eval('type') != 'institutional',
    'required': Eval('type') == 'institutional'
}

DEPENDS = ['type']


def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        _dict.update({field_name: row[i]})
        i += 1
    return _dict


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def employee_field_selection(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        depends=['company'], required=True)


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class Mark(ModelSQL, ModelView):
    "mark"
    __name__ = 'parking.vehicle.mark'
    name = fields.Char('Nombre')


class Model(ModelSQL, ModelView):
    "Model"
    __name__ = 'parking.vehicle.model'
    name = fields.Char('Nombre')
    mark = fields.Many2One('parking.vehicle.mark', 'Marca')


class Colour(ModelSQL, ModelView):
    "Colour"
    __name__ = 'parking.vehicle.colour'
    name = fields.Char('Nombre')


class Category(ModelSQL, ModelView):
    "Category"
    __name__ = 'parking.vehicle.category'
    name = fields.Char('Nombre')


class VehicleDriver(ModelSQL):
    'Vehicle-Driver'
    __name__ = 'parking.vehicle-parking.driver'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        ondelete='CASCADE', select=True, required=True)
    driver = fields.Many2One('parking.vehicle.driver', 'Conductor',
        ondelete='CASCADE', select=True, required=True)


class VehicleTemplateItem(ModelSQL, ModelView):
    "Vehicle Template Item"
    __name__ = 'parking.vehicle.template.item'
    name = fields.Char('Nombre del Ítem')
    description = fields.Text('Descripción')


class VehicleTemplateVehicleTemplateItem(ModelSQL, ModelView):
    "Vehicle Template-Vehicle Template Item"
    __name__ = 'parking.vehicle.template-parking.vehicle.template.item'
    template = fields.Many2One('parking.vehicle.template', 'Template',
                              ondelete='CASCADE', select=True)
    item = fields.Many2One('parking.vehicle.template.item', 'Ítem',
                              ondelete='CASCADE', select=True)


class VehicleTemplate(ModelSQL, ModelView):
    "Template"
    __name__ = 'parking.vehicle.template'
    name = fields.Char('Nombre')
    checklist = fields.Many2Many(
        'parking.vehicle.template-parking.vehicle.template.item',
        'template', 'item', 'Check List')


class VehicleChecklistItem(ModelSQL, ModelView):
    "Vehicle Check list Item"
    __name__ = 'parking.vehicle.check.list.item'
    name = fields.Char('Nombre')
    description = fields.Text('Descripción')
    success = fields.Boolean('Cumplimiento')
    header_checklist = fields.Many2One(
        'parking.vehicle.header.checklist', 'Header')


class VehicleHeaderChecklist(ModelSQL, ModelView):
    "Vehicle Header Checklist"
    __name__ = 'parking.vehicle.header.checklist'
    name = fields.Char('Nombre')
    date = fields.Date('Fecha de revisión')
    checklist_template = fields.Many2One(
        'parking.vehicle.template', 'Plantilla')
    items = fields.One2Many(
        'parking.vehicle.check.list.item', 'header_checklist', 'Ítems')
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo')

    #Función de autorellenado
    @fields.depends('checklist_template', 'items')
    def on_change_checklist_template(self, name=None):
        pool = Pool()
        Item = pool.get('parking.vehicle.check.list.item')
        self.items = ()
        if self.checklist_template:
            if self.checklist_template.checklist:
                for item in self.checklist_template.checklist:
                    checkitem = Item() #CheckItem()
                    checkitem.name = item.name
                    checkitem.description = item.description
                    checkitem.success = False
                    self.items = self.items + (checkitem,)


class Vehicle(Workflow, ModelSQL, ModelView):
    "Vehicle"
    __name__ = 'parking.vehicle.vehicle'

    mark = fields.Many2One('parking.vehicle.mark', 'Marca',
        states=STATES_CONTRACTED, depends=DEPENDS)

    model = fields.Many2One('parking.vehicle.model', 'Modelo',
        domain=[
            ('mark', '=', Eval('mark'))
        ],
        states=STATES_CONTRACTED, depends=DEPENDS + ['mark'])
    colour = fields.Many2One('parking.vehicle.colour', 'Color',
        states=STATES_ACTIVE, depends=DEPENDS)

    type = fields.Selection([
        ('institutional', 'Vehículo institucional'),
        ('contracted', 'Contratado'),
        ('machinery', 'Maquinaria y Equipo'),
        ], 'Tipo', required=True)
    type_translated = type.translated('type')
    year_production = fields.Date('Año Fabricación',
                                  states=STATES_ACTIVE, depends=DEPENDS)
    engine = fields.Char('Motor', states=STATES_ACTIVE,
                         depends=DEPENDS)
    chassis = fields.Char('Chasis', states=STATES_ACTIVE,
                          depends=DEPENDS)
    axis = fields.Char('Ejes', states=STATES_ACTIVE,
                       depends=DEPENDS)
    displacement = fields.Char('Cilindraje',
                               states=STATES_ACTIVE,
                               depends=DEPENDS)
    plaque = fields.Char('Placa', states=STATES_CONTRACTED,
                         depends=DEPENDS)
    current_mileage = fields.Numeric('Recorrido Actual', states={
            # TEMPORAL UNTIL DEVELOP FIX MILEAGE, USAGE UPDATES
            'readonly': False
        })
    unit_measure_track = fields.Many2One('product.uom',
        'U. de medida de recorrido',
        domain=[
            ('category.name', '=', 'Longitud')
        ],
       )
    current_usage = fields.Numeric('Horas m. actual', states={
            # TEMPORAL UNTIL DEVELOP FIX MILEAGE, USAGE UPDATES
            'readonly': False
        })
    unit_measure_use = fields.Many2One('product.uom', 'U. de medida de tiempo',
        domain=[
           ('category.name', '=', 'Tiempo')
        ])

    state = fields.Function(fields.Selection(STATE, 'Estado'),
        'on_change_with_state', searcher='search_state')
    state_mounts = fields.Function(fields.Selection
        (STATE_MOUNTS, 'Montaje/Desmontaje'),
             'on_change_with_state_mounts', searcher='search_state_mounts')
    state_translated = state.translated('state')
    category = fields.Many2One('parking.vehicle.category', 'Categoría',
        states=STATES_ACTIVE, depends=DEPENDS)

    enrolment = fields.Char('Matrícula', states=STATES_ACTIVE, depends=DEPENDS)

    drivers = fields.Many2Many(
        'parking.vehicle-parking.driver', 'vehicle', 'driver', 'Conductores')
    driver = fields.Many2One(
        'parking.vehicle.driver', 'Conductor actual',
            domain=[
                ('id', 'in', Eval('drivers', [])),
            ],
            depends=['drivers'])
    type_fuel = fields.Selection([
        (None, ""),
        ('gasoline', 'Gasolina'),
        ('diesel','Diesel'),
        ('super','Super'),
        ('ecopais', 'Eco País')],
        'Combustible')
    type_propulsion = fields.Selection([
        (None, ""),
        ('thermal', 'Térmica'),
        ('hybrid', 'Híbrida'),
        ('electric', 'Eléctrica')],
        'Propulsión')
    type_propulsion_translated = type_propulsion.translated('type_propulsion')
    tires = fields.Numeric('Nº de llantas')

    history = fields.One2Many('parking.vehicle.vehicle.history',
        'vehicle', 'Historial del vehículo')
    mounts = fields.One2Many('parking.mounts.history.vehicle',
                              'vehicle', 'vehículo')

    mileage_updates = fields.One2Many('parking.vehicle.mileage.update',
        'vehicle', 'Actualizaciones de Recorrido', states={
            # TEMPORAL UNTIL DEVELOP FIX MILEAGE, USAGE UPDATES
            'readonly': False
        })
    maintenances = fields.One2Many('parking.vehicle.maintenance.preventive',
        'vehicle', 'Planificación de Mantenimientos preventivos')
    to_movilization = fields.Boolean('Para movilización?')
    imei = fields.Char('Imei Gps')
    vehicle_name = fields.Char('Nombre')
    checklists = fields.One2Many(
        'parking.vehicle.header.checklist', 'vehicle', 'Revisiones')
    latitude = fields.Numeric('Latitud')
    longitude = fields.Numeric('Longitud')
    point = GeoFields.Point('Ubicacion')
    photo = fields.Binary(
        'Ficha Técnica del Fabricante', filename='photo_producer_maintenance',
        file_id='photo_path')
    photo_producer_maintenance = fields.Char('Photo Name')
    photo_path = fields.Char('Photo Path', readonly=True)
    fence = fields.Many2One('parking.vehicle.fence', 'Cerca por defecto')
    asset = fields.One2One(
        'asset-parking.vehicle.vehicle', 'vehicle', 'asset',
        'Enlace con activo')


    @classmethod
    def __setup__(cls):
        super(Vehicle, cls).__setup__()

        v = cls.__table__()
        cls._sql_constraints += [
            ('unique_plaque',
             Unique(v, v.plaque, v.chassis
                    ),
             'Este campo debe ser único'),
        ]
        cls._error_messages.update({
            '': (
                "Este  campo debe ser único "),
        })

    @classmethod
    def search_state(cls, name, clause):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        items = Vehicle.search([
            ('id', '>', 0),
            ('type', '=', 'institutional')
            ])
        ids = []
        for item in items:
            if item.state == clause[2]:
                ids.append(item.id)
        return [('id', 'in', ids)]

    @classmethod
    def search_state_mounts(cls, name, clause):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        items = Vehicle.search([
            ('id', '>', 0),
            ('type', '=', 'institutional')
        ])
        ids = []
        for item in items:
            if item.state_mounts == clause[2]:
                ids.append(item.id)
        return [('id', 'in', ids)]

    @staticmethod
    def default_type():
        return 'machinery'

    @staticmethod
    def default_current_mileage():
        return Decimal(0.0)

    @staticmethod
    def default_current_usage():
        return Decimal(0.0)

    @staticmethod
    def default_state():
        return 'available'
    
    @staticmethod
    def default_year_production():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_engine():
        return 'S/I'

    @staticmethod
    def default_chassis():
        return 'S/I'

    @staticmethod
    def default_axis():
        return 'S/I'

    @staticmethod
    def default_displacement():
        return 'S/I'

    @staticmethod
    def default_enrolment():
        return 'S/I'

    
    #
    # @fields.depends('latitude', 'point_geojson')
    # def on_change_latitude(self, name=None):
    #     model, id = str(self).split(',')
    #     model = model.replace('.','_')
    #     query = "SELECT json_build_object(" \
    #             "'type', 'FeatureCollection'," \
    #             "'crs',  json_build_object(" \
    #                 "'type',      'name', " \
    #                 "'properties', json_build_object(" \
    #                     "'name', 'EPSG:4326'  " \
    #                 ")" \
    #             "), " \
    #             "'features', json_agg(" \
    #                 "json_build_object(" \
    #                     "'type',       'Feature'," \
    #                     "'id',         id, " \
    #                     "'geometry',   ST_AsGeoJSON(point)::json," \
    #                     "'properties', json_build_object(" \
    #                         "'id', id," \
    #                         "'local', point" \
    #                     ")" \
    #                 ")" \
    #             ")" \
    #         ")" \account
    #         " FROM " + model + \
    #         " WHERE id = " + id + ";"
    #     cursor = Transaction().connection.cursor()
    #     cursor.execute(query)
    #     res = cursor.fetchone()
    #     self.point_geojson = str(res[0]).replace("'",'"')

    # @fields.depends('longitude', 'virtual_fence_geojson')
    # def on_change_longitude(self, name=None):
    #     model, id = str(self).split(',')
    #     model = model.replace('.','_')
    #     query = "SELECT json_build_object(" \
    #             "'type', 'FeatureCollection'," \
    #             "'crs',  json_build_object(" \
    #                 "'type',      'name', " \
    #                 "'properties', json_build_object(" \
    #                     "'name', 'EPSG:4326'  " \
    #                 ")" \
    #             "), " \
    #             "'features', json_agg(" \
    #                 "json_build_object(" \
    #                     "'type',       'Feature'," \
    #                     "'id',         id, " \
    #                     "'geometry',   ST_AsGeoJSON(virtual_fence)::json," \
    #                     "'properties', json_build_object(" \
    #                         "'id', id," \
    #                         "'local', virtual_fence" \
    #                     ")" \
    #                 ")" \
    #             ")" \
    #         ")" \
    #         " FROM " + model + \
    #         " WHERE id = " + id + ";"
    #     cursor = Transaction().connection.cursor()
    #     cursor.execute(query)
    #     res = cursor.fetchone()
    #     self.virtual_fence_geojson = str(res[0]).replace("'",'"')

    @fields.depends('history')
    def on_change_with_state(self, name=None):
        if self.history:
            pool = Pool()
            Date = pool.get('ir.date')
            today = Date.today()
            for record in self.history:
                if record.start_date <= today and \
                        record.end_date >= today:
                    if record.origin_model == 'parking.vehicle.maintenance':
                        return 'maintenance'
                    elif record.origin_model == 'parking.vehicle.accident':
                        return 'accidented'
                    elif record.origin_model == 'parking.mobilization.order':
                        return 'busy'
        return 'available'

    @fields.depends('mounts')
    def on_change_with_state_mounts(self, name=None):
        if self.mounts:
            for record in self.mounts:
                if record.origin_model == 'parking_place.mount_tires':
                    return 'mount'
        return 'disassembly'

    def get_rec_name(self, name):
        if self.type == 'institutional':
            return '%s %s %s [Placa: %s]' % (
                self.mark.name, self.model.name, self.colour.name, self.plaque)
        elif self.type == 'machinery':
            return '%s [%s] (%s) - Maquinaria y equipo' % (self.mark.name,
                self.category.name, self.plaque)
        else:
            return 'VEHICULO CONTRATADO [%s] (%s)' % (self.plaque,
                self.state_translated)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('plaque',) + tuple(clause[1:]),
            ('model',) + tuple(clause[1:]),
            ('mark',) + tuple(clause[1:]),
            ('vehicle_name',) + tuple(clause[1:]),
            ]
        return domain

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
            query=False):
        context = Transaction().context
        if context.get('parking_mobilization_order'):
            if context.get('order_state') != 'draft':
                domain += [('id', '!=', None)]
            else:
                new_domain = cls.search_from_vehicle(context)
                if new_domain:
                    domain += new_domain
                else:
                    domain += [('id', '=', None)]

        return super(Vehicle, cls).search(domain, offset=offset,
            limit=limit, order=order, count=count, query=query)

    @classmethod
    def search_from_vehicle(cls, context):
        pool = Pool()
        VehicleAvailable = pool.get('parking.vehicle.available.query')
        params = {
            'start_date': context.get('departure_date') ,
            'return_date': context.get('return_date'),
            'start_time': context.get('departure_time'),
            'return_time': context.get('return_time'),
        }
        vehicle_ids = []
        with Transaction().set_context(**params):
            vehicle = VehicleAvailable.table_query()
            cursor = Transaction().connection.cursor()
            cursor.execute(*vehicle)
            for row in cursor.fetchall():
                vehicle_ids.append(row[5])

        new_domain = [('id', 'in', vehicle_ids)]
        return new_domain


class VehicleHistory(ModelSQL, ModelView):
    'Vehicle - History'
    __name__ = 'parking.vehicle.vehicle.history'

    number = fields.Char('Registro No.', readonly=True)
    origin = fields.Reference('Origen',
        [
            ('parking.vehicle.accident', 'Accidente'),
            ('parking.mobilization.order', 'Orden de movilización'),
            ('parking.vehicle.maintenance', 'Mantenimiento')
        ], readonly=True)
    origin_translated = origin.translated('origin')
    origin_model = fields.Function(fields.Selection('get_origin_models',
        'Modelo origen'), 'on_change_with_origin_model')
    start_date = fields.Date('Fecha inico', readonly=True)
    end_date = fields.Date('Fecha fin', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)

    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__

    @staticmethod
    def _get_origin():
        return [
            'parking.vehicle.accident',
            'parking.mobilization.order',
            'parking.vehicle.maintenance',
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def table_query():
        pool = Pool()
        Accident = pool.get('parking.vehicle.accident')
        accident = Accident.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Order = pool.get('parking.mobilization.order')
        order = Order.__table__()
        where = (Literal(True))

        queryAccident = accident.join(vehicle,
            condition=vehicle.id == accident.vehicle
        ).select(
            accident.number.as_('number'),
            Concat('parking.vehicle.accident,',
                accident.id).as_('origin'),
            accident.date_accident.as_('start_date'),
            Coalesce(accident.maintenance_date,
                accident.date_accident).as_('end_date'),
            accident.vehicle.as_('vehicle'),
            where=accident.state != "draft")

        queryMaintenance = maintenance.join(vehicle,
            condition=vehicle.id == maintenance.vehicle
        ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, MAX_DATE).as_('end_date'),
            maintenance.vehicle,
            where=maintenance.state != 'draft')

        queryOrder = order.join(vehicle,
            condition=vehicle.id == order.vehicle
        ).select(
            order.number.as_('number'),
            Concat('parking.mobilization.order,',
                order.id).as_('origin'),
            order.departure_date.as_('start_date'),
            order.return_date.as_('end_date'),
            order.vehicle.as_('vehicle'),
            where=(order.state != 'draft'))

        union = Union(queryAccident, queryOrder, all_=True)
        union_two = Union(union, queryMaintenance, all_=True)
        resq = union_two.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            union_two.number,
            union_two.origin,
            union_two.start_date,
            union_two.end_date,
            union_two.vehicle,
            where=where
        )

        return union_two.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            union_two.number,
            union_two.origin,
            union_two.start_date,
            union_two.end_date,
            union_two.vehicle,
            where=where
        )

    @classmethod
    def __setup__(cls):
        super(VehicleHistory, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]


class VehicleMileageUpdate(ModelSQL, ModelView):
    'Vehicle Mileage Update'
    __name__ = 'parking.vehicle.mileage.update'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)
    new_mileage = fields.Numeric('Recorrido', required=True)
    new_usage = fields.Numeric('Horas máquina', required=True)
    description = fields.Text('Antecedente de la actualización',
        required=True)

    @classmethod
    def __setup__(cls):
        super(VehicleMileageUpdate, cls).__setup__()
        cls._order = [
            ('id', 'DESC')
        ]


class VehicleAccident(Workflow, ModelSQL, ModelView):
    'Vehicle Accident'
    __name__ = 'parking.vehicle.accident'

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número de Orden', readonly=True)
    registrated_date = fields.Date('Fecha de registro', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo', states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state'], required=True
    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('registrated', 'Registrado')
    ], 'Estado', readonly=True)
    date_accident = fields.Date('Fecha del accidente', states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state'], required=True
    )
    maintenance_date = fields.Date('Fecha de reparación',
        states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state']
    )
    time_accident = fields.Time('Hora del accidente', states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state'], required=True
    )
    ubication_accident = fields.Many2One('country.subdivision',
        'Ciudad cercana al accidente', domain=[
            ('type', '=', 'city'),
            ('country.code', '=', 'EC')
        ], states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state'], required=True
    )
    mobilization_order = fields.Many2One('parking.mobilization.order',
        'Orden de movilización', states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state']
    )

    # passengers = fields.Integer("Ocupantes", states={
    #     'readonly': ~(Eval('state') == 'draft')
    #     }, depends=['state'], required=True
    # )
    location_description = fields.Text('Detalle de la ubicación',
        help='Ingrese una descripción detallada del lugar donde sucedió el'
        'accidente', states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state'], required=True
    )
    description = fields.Text('Detalle de lo sucedido en el accidente',
        required=True, states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state']
    )
    registrated_by = employee_field('Registrado por')
    stop_functions = fields.Boolean('Imposibilita operación?',
        states={
        'readonly': ~(Eval('state') != 'registrated')
        }, depends=['state']
    )

    @classmethod
    def __setup__(cls):
        super(VehicleAccident, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registrated'),
            ('registrated', 'draft'),
        ))
        cls._buttons.update({
            'do_register': {
                'invisible': ~Eval('state').in_(['draft']),
            },
            'draft': {
                'invisible': ~Eval('state').in_(['registrated']),
            },
        })
        cls._error_messages.update({
            'not_employee': ('Actualmente el usuario no tiene un empleado '
                             'configurado')
        })

    @classmethod
    def set_number(cls, accidents):
        '''
        Fill the number field with the accidentsequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')

        config = Config(1)
        for accident in accidents:
            if accident.number:
                continue
            accident.number = Sequence.get_id(
                config.accident_sequence.id)
        cls.save(accidents)

    @classmethod
    def default_state(cls):
        return 'draft'
    
    @classmethod
    def default_stop_functions(cls):
        return False

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    @ModelView.button
    @Workflow.transition('registrated')
    @set_employee('registrated_by')
    def do_register(cls, accidents):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        Date = pool.get('ir.date')
        today = Date.today()
        if not user.employee:
            cls.raise_user_error('not_employee')
        for accident in accidents:
            accident.registrated_date = today
        cls.set_number(accidents)
        super(VehicleAccident, cls).save(accidents)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, accidents):
        pass

    @fields.depends('mobilization_order')
    def on_change_mobilization_order(self, name=None):
        if self.mobilization_order:
            self.vehicle = self.mobilization_order.vehicle

    def get_rec_name(self, name):
        if self.number:
            return self.number


class FuelType(ModelSQL, ModelView):
    "Fuel Type"
    __name__ = 'parking.fuel_type'
    name = fields.Char('Tipo del Combustible', states={
        'required': True
    })
    cost = fields.Numeric('Costo/Galón', digits=(16, 3), states={
        'required': True
    })


class FuelOrder(Workflow, ModelSQL, ModelView):
    "fuelOrder"
    __name__ = 'parking.fuel_order'

    _states = {
        'readonly': ~(Eval('state') == 'draft')
    }

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número de Orden', required=True,
        states=_states, depends=['state'])
    department = fields.Many2One('company.department', 'Departamento')
    date = fields.Date('Fecha de Orden', required=True,
        states=_states, depends=['state']
    )
    time = fields.Time('Hora de Orden', required=True,
        states=_states, depends=['state']
    )
    registered_by = employee_field("Registrado por")
    registered_date = fields.Date('Fecha Registro', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle',
        'Vehículo',
        states={
            'required': True
        }, depends=['state'])
    description = fields.Text('Descripción', required=True,
        states=_states, depends=['state']
    )
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('registered', 'Registrado'),
    ], 'Estado', readonly=True)
    fuel_type = fields.Many2One('parking.fuel_type', 'Tipo de Combustible',
        states=_states, depends=['state'], required=True)
    mileage = fields.Numeric('Recorrido', digits=(16, 0),
        states=_states, depends=['state'], required=True)
    usage = fields.Numeric('Horas máquina', digits=(16, 0),
        states=_states, depends=['state'], required=True)
    gallons = fields.Numeric('Gallones',digits=(16,4))
    gallons_number = fields.Numeric('Número de galones', digits=(16,3), states=_states,
        depends=['state'])
    price = fields.Numeric('Precio', digits=(16, 3), states=_states,
        depends=['state'])
    fuel_cost = fields.Numeric('Sub total', digits=(16, 2), states=_states,
        depends=['state'])
    fuel_cost_iva = fields.Numeric('Total', digits=(16, 2), states=_states,
        depends=['state'])
    payment = fields.Many2One('parking.fuel_order.payment', 'Liquidación',
        select=True)
    iva = fields.Numeric('Iva', digits=(16, 2), states=_states,
        depends=['state'])
    employee = fields.Many2One('company.employee', 'Empleado',
        domain=[('company', '=', Eval('company', -1))], states=_states,
        depends=['company'])

    @classmethod
    def __setup__(cls):
        super(FuelOrder, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registered'),
            ('registered', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['registered']),
            },
            'register': {
                'invisible': Eval('state').in_(['registered']),
            },
        })
        cls._error_messages.update({
            'not_email': ('Actualmente el usuario no tiene un correo '
                          'configurado'),
            'not_employee': ('Actualmente el usuario no tiene un empleado '
                             'configurado'),
            'already_paid': ('No se puede pasar a borrador una orden que ya ha '
                             'sido pagada')
        })
        t = cls.__table__()
        cls._sql_constraints += [
            ('fuel_order_number_unique', Unique(t, t.number),
                'El número de orden debe ser único'),
        ]

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_time():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('fuel_type','fuel_cost')
    def on_change_fuel_type(self, name=None):
        if self.fuel_type:
            self.price = self.fuel_type.cost
            price = self.price
            if self.fuel_cost and price:
                self.gallons_number = self.fuel_cost/price

    @fields.depends('price', 'fuel_cost')
    def on_change_price(self, name=None):
        if self.fuel_cost and self.price:
            self.gallons_number = self.fuel_cost/self.price

    @fields.depends('fuel_cost', 'gallons_number', 'fuel_cost_iva',
    'price', 'iva')
    def on_change_fuel_cost(self, name=None):
        iva = Decimal(0.12)
        sub_total = self.fuel_cost
        price = self.price
        if sub_total:
            self.fuel_cost_iva = sub_total*(1 + iva)
            self.iva = sub_total*iva
            if price:
                self.gallons_number = sub_total/price

    @fields.depends('employee')
    def on_change_with_department(self, name=None):
        if self.employee:
            if self.employee.contract_department:
                return self.employee.contract_department.id


    # @fields.depends('fuel_cost', 'fuel_type')
    # def on_change_fuel_cost(self, name=None):
    #     self.gallons = self._get_gallons()
    #     self.fuel_cost_iva = self._get_fuel_cost_IVA()
    #     self.iva= self._get_iva()

    # @fields.depends('fuel_type', 'fuel_cost')
    # def on_change_fuel_type(self, name=None):
    #     self.gallons = self._get_gallons()
    #     self.fuel_cost_iva = self._get_fuel_cost_IVA()
    #     self.iva = self._get_iva()


    # def _get_gallons(fuel_order):
    #     if (fuel_order.fuel_type and
    #             fuel_order.fuel_cost and fuel_order.fuel_type.cost):
    #         return (fuel_order.fuel_cost / fuel_order.fuel_type.cost)
    #     else:
    #         return Decimal(0.0)

    # def _get_fuel_cost_IVA(fuel_order):
    #     IVA = Decimal(1.12)
    #     if (fuel_order.fuel_type and
    #             fuel_order.fuel_cost and fuel_order.fuel_type.cost):
    #         return (fuel_order.fuel_type.cost * fuel_order.gallons)*IVA
    #     else:
    #         return Decimal(0.0)

    # def _get_iva(fuel_order):
    #     IVA = Decimal(1.12)
    #     if (fuel_order.fuel_cost_iva and
    #             fuel_order.fuel_cost):
    #         return (fuel_order.fuel_cost_iva - fuel_order.fuel_cost)
    #     else:
    #         return Decimal(0.0)

    @classmethod
    def get_values(cls, orders, names=None):
        gallons_number = defaultdict(lambda: Decimal(0.0))
        ivas = defaultdict(lambda:  Decimal(0.0))
        fuel_cost_ivas = defaultdict(lambda: Decimal(0.0))

        IVA = Decimal(0.12)
        for order in orders:
            if order.fuel_cost and order.fuel_type.cost and order.fuel_cost:
                gallons_number[order.id] = order.fuel_cost / order.fuel_type.cost
                ivas[order.id] = order.fuel_cost * IVA
                fuel_cost_ivas[order.id] = order.fuel_cost + ivas[order.id]
        return {
            'fuel_cost_iva': fuel_cost_ivas,
            'gallons_number':  gallons_number,

            'iva': ivas
        }




    # @classmethod
    # def validate(cls, fuel_orders):
    #     super(FuelOrder, cls).validate(fuel_orders)
    #     for fuel_order in fuel_orders:
    #         fuel_order in fuel_orders
    def get_rec_name(self, name):
        return '%s %s %s [%s] (%s)' % (self.number, self.date,
                                       self.time, self.gallons_number,
                                       self.fuel_cost)

    @classmethod
    @ModelView.button
    @Workflow.transition('registered')
    @set_employee('registered_by')
    def register(cls, fuel_orders):
        pool = Pool()
        User = pool.get('res.user')
        Date = pool.get('ir.date')
        today = Date.today()
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for order in fuel_orders:
            order.registered_date = today
            # order.fuel_cost = cls._get_fuel_cost(order)
        # cls.set_number(fuel_orders)
        super(FuelOrder, cls).save(fuel_orders)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, fuel_orders):
        for order in fuel_orders:
            if order.payment:
                cls.raise_user_error('already_paid')
        pass


class UpdateVehicleMileageStart(ModelView):
    'Update Vehicle Mileage Start'
    __name__ = 'parking.vehicle.update.mileage.start'

    new_mileage = fields.Numeric('Nuevo recorrido', required=True)
    new_usage = fields.Numeric('Nuevas horas máquina', required=True)
    description = fields.Text('Antecedente de la actualización', required=True)


class UpdateVehicleMileage(Wizard):
    'Update Vehicle Mileage'
    __name__ = 'parking.vehicle.update.mileage'

    start = StateView('parking.vehicle.update.mileage.start',
        'parking_place.vehicle_update_mileage_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'change', 'tryton-ok', default=True),
            ])
    change = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UpdateVehicleMileage, cls).__setup__()
        cls._error_messages.update({
                'lower_mileage': ('El recorrido o las horas m. ingresadas '
                    'es menor al recorrido o horas m. actuales del '
                    'vehículo: %s.')
                })

    def transition_change(self):
        pool = Pool()
        Vehicle = pool.get('parking.vehicle.vehicle')
        Maintenance = pool.get('parking.vehicle.maintenance')
        MaintenancePreventive = pool.get(
            'parking.vehicle.maintenance.preventive')
        vehicle = Vehicle(Transaction().context['active_id'])
        MileageUpdate = pool.get('parking.vehicle.mileage.update')
        Date = pool.get('ir.date')
        if (self.start.new_mileage < vehicle.current_mileage) or \
                ((self.start.new_usage) < vehicle.current_usage):
            self.raise_user_error('lower_mileage', vehicle.rec_name)
        else:
            mileage_update = MileageUpdate()
            mileage_update.new_mileage = self.start.new_mileage
            mileage_update.new_usage = self.start.new_usage
            mileage_update.description = self.start.description
            mileage_update.vehicle = vehicle
            MileageUpdate.save([mileage_update])
            vehicle.current_mileage = self.start.new_mileage
            vehicle.current_usage = self.start.new_usage
            Vehicle.save([vehicle])
            for line in vehicle.maintenances:
                text = ''
                if not line.maintenance_related:
                    if line.expected_use:
                        if line.expected_use <= self.start.new_mileage:
                            text = "Se sobrepasó el recorrido"
                    if line.expected_time:
                        if vehicle.expected_time < self.start.new_usage:
                            if text == '':
                                text = "Se sobrepasó las horas m."
                            else:
                                text = text + ', y las horas m.'
                    if line.date:
                        if line.date <= date.today():
                            if text == '':
                                text = "Se pasó de la fecha"
                            else:
                                text = text + ', y la fecha'
                    if text != '':
                        text = text + ' previsto en el mantenimiento ' + \
                            'planificado con descripción: "' + \
                            line.maintenance_description + '", por lo tanto se' + \
                            'ha creado un mantenimiento en borrador.'
                        Note = pool.get('ir.note')
                        ReadNote = pool.get('ir.note.read')
                        note = Note()
                        note.resource = vehicle
                        note.message = text
                        note.create_uid = 1
                        Note.save([note])
                        notes = Note.search([('resource', '=', str(vehicle))])
                        for item in notes:
                            read_notes = ReadNote.search([('note', '=', item.id)])
                            for read in read_notes:
                                read.user = 1
                                ReadNote.save([read])
                        maintenance = Maintenance()
                        if line.template:
                            maintenance.template = line.template
                        maintenance.vehicle = vehicle
                        maintenance.type = 'preventive'
                        maintenance.related_maintenance_preventive = line
                        maintenance.description = line.maintenance_description
                        maintenance.state = 'draft'
                        maintenance.start_date = Date.today()
                        maintenance.start_time = None
                        maintenance.on_change_template()
                        maintenance.company = Transaction().context.get('company')
                        Maintenance.save([maintenance])
                        line.maintenance_related = maintenance
                        MaintenancePreventive.save([line])
                        self.send_email([maintenance],'draft')
        return 'end'

    def send_email(cls, maintenances, state_to):
        pool = Pool()
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Mantenimiento'),
            ('template', '=', True),
        ])
        alerts = []
        for maintenance in maintenances:
            if maintenance:
                employees = []
                if state_to == 'draft':
                    Group = pool.get('res.group')
                    search_group = Group.search([('name', '=',
                                                  'Parque automotor / Mantenimientos')
                                                 ])
                    if search_group:
                        for user in search_group[0].users:
                            if user.employee:
                                employees.append(user.employee)
                if state_to in ['handed', 'deny']:
                    employees.append(maintenance.request_by)
                mail_body = cls.generate_mail_body(cls, maintenance, state_to)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                         f"parking.vehicle.maintenance,"
                         f"{maintenance.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                    ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = maintenance
                        # The mail body depends of transition state
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene mail registrado el(la) empleado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    def generate_mail_body(cls, maintenance, state_to):
        if state_to == "request":
            return (f"Se ha registrado una nueva solicitud de "
                    "mantenimiento para el día: " f"{maintenance.request_date} "
                    "con la siguiente descripción: " f"{maintenance.description}. \n"
                    "Por favor revisar las soliciitudes pendientes en el sistema.")
        elif state_to == "deny":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                    "mantenimiento ha sido rechazada  con la descripción siguiente:"
                    f"{maintenance.description}")
        elif state_to == "handed":
            return (f"Estimado usuario, se le comunica que su solicitud de "
                    "mantenimiento ha sido autorizada "
                    "con la descripción siguiente:"
                    f"{maintenance.description} "
                    "Por favor se deberá acercar a retirar "
                    "el documento de la órden de combustible autorizada por el "
                    "jefe de departamento correspondiente.")

        # def spares_return(self):
        #     pool = Pool()
        #     Spares = pool.get('parking.vehicle.maintenance.line')
        pass


class VehicleAvailabilityHeader(ModelSQL, ModelView):
    'Vehicle Availability Header'
    __name__ = 'parking.vehicle.availability.header'

    number = fields.Char('Registro No.', readonly=True)
    origin = fields.Reference('Origen',
        [
            ('parking.vehicle.accident', 'Accidente'),
            ('parking.mobilization.order', 'Orden de movilización'),
            ('parking.vehicle.maintenance', 'Mantenimiento')
        ], readonly=True)
    origin_model = fields.Function(fields.Selection('get_origin_models',
        'Modelo origen'), 'on_change_with_origin_model')
    start_date = fields.Date('Fecha inicio', readonly=True)
    end_date = fields.Date('Fecha fin', readonly=True)
    color = fields.Char('Color', readonly=True)
    state = fields.Char('Estado', readonly=True)

    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__

    @staticmethod
    def _get_origin():
        return [
            'parking.vehicle.accident',
            'parking.mobilization.order',
            'parking.vehicle.maintenance',
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def table_query():
        context = Transaction().context
        vehicle_id = context.get('vehicle')
        pool = Pool()
        Accident = pool.get('parking.vehicle.accident')
        accident = Accident.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('company.position')
        vehicle = Vehicle.__table__()
        Order = pool.get('parking.mobilization.order')
        order = Order.__table__()
        Workshop = pool.get('parking.workshop')
        workshop = Workshop.__table__()
        where = (Literal(True))

        queryAccident = accident.join(vehicle,
            condition=vehicle.id == accident.vehicle
        ).select(
            accident.number.as_('number'),
            Concat('parking.vehicle.accident,',
                accident.id).as_('origin'),
            accident.date_accident.as_('start_date'),
            Coalesce(accident.maintenance_date,
                accident.date_accident).as_('end_date'),
            accident.vehicle.as_('vehicle'),
            accident.state,
            Concat("#ffcdcd","").as_("color"),
            where=where & (accident.vehicle == vehicle_id))

        queryMaintenanceFinished = maintenance.join(vehicle,
            condition=vehicle.id == maintenance.vehicle
        ).join(workshop,
            condition=workshop.id == maintenance.workshop
        ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, MAX_DATE).as_('end_date'),
            maintenance.vehicle,
            Concat(Concat(maintenance.state," - "),workshop.type_).as_("state"),
            Concat("#5fb7f9","").as_("color"),
            where=where & (maintenance.vehicle == vehicle_id) 
            & (maintenance.state != 'draft') 
            & (maintenance.state != 'finished') )

        queryMaintenanceDraft = maintenance.join(vehicle,
            condition=vehicle.id == maintenance.vehicle
        ).join(workshop,
            condition=workshop.id == maintenance.workshop
        ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, MAX_DATE).as_('end_date'),
            maintenance.vehicle,
            Concat(Concat(maintenance.state," - "),workshop.type_).as_("state"),
            Concat("#ffcdcd","").as_("color"),
            where=where & (maintenance.vehicle == vehicle_id)
            & (maintenance.state == 'draft') )
        
        queryMaintenance = maintenance.join(vehicle,
            condition=vehicle.id == maintenance.vehicle
        ).join(workshop,
            condition=workshop.id == maintenance.workshop
        ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, MAX_DATE).as_('end_date'),
            maintenance.vehicle,
            Concat(Concat(maintenance.state," - "),workshop.type_).as_("state"),
            Concat("#cfefcd","").as_("color"),
            where=where & (maintenance.vehicle == vehicle_id)
            & (maintenance.state == 'finished') )

        queryOrder = order.join(vehicle,
            condition=vehicle.id == order.vehicle
        ).select(
            order.number.as_('number'),
            Concat('parking.mobilization.order,',
                order.id).as_('origin'),
            order.departure_date.as_('start_date'),
            order.return_date.as_('end_date'),
            order.vehicle.as_('vehicle'),
            order.state,
            Concat("#5fb7f9","").as_("color"),
            where=where & (order.state != 'draft') &
                (order.vehicle == vehicle_id))

        union = Union(queryAccident, queryOrder, all_=True)
        union_two = Union(union, queryMaintenanceFinished, all_=True)
        union_two = Union(union, queryMaintenanceDraft, all_=True)
        union_two = Union(union, queryMaintenance, all_=True)

        return union_two.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            union_two.number,
            union_two.origin,
            union_two.start_date,
            union_two.end_date,
            union_two.vehicle,
            union_two.state,
            union_two.color,
            where=where
        )

    @classmethod
    def __setup__(cls):
        super(VehicleAvailabilityHeader, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]


class VehicleAvailabilityHeaderContext(ModelView):
    'Vehicle Availability Header Context'
    __name__ = 'parking.vehicle.availability.header.context'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo')


class FuelOrderQuery(ModelSQL, ModelView):
    'Fuel Order Query'
    __name__ = 'parking.fuel_order.query'

    number = fields.Char('Registro No.', readonly=True)
    date = fields.Date('Fecha', readonly=True)
    time = fields.Time('Hora de Orden', readonly=True)
    vehicle = fields.Char('Vehículo', readonly=True)
    vehicle_id = fields.Many2One('parking.vehicle.vehicle', 
        'Vehículo', readonly=True)
    fuel_type = fields.Char('Tipo de Combustible',
        readonly=True)
    # gallons = fields.Numeric('Galones', digits=(16, 2),
        # readonly=True)
    gallons_number = fields.Numeric('Galones', digits=(16, 3),
        readonly=True)
    fuel_cost_iva = fields.Numeric('Total', digits=(16, 2),
        readonly=True)
    price = fields.Numeric('Precio', digits=(16, 2),
        readonly=True)
    iva = fields.Numeric('IVA', digits=(16, 2),
        readonly=True)
    mileage = fields.Numeric('Recorrido', digits=(16, 2),
                             readonly=True)
    description = fields.Text('Descripción', readonly=True)
    fuel_cost = fields.Numeric('Costo del total', digits=(16, 2),
        readonly=True)
    department = fields.Many2One('company.department', 'Departamento')

    @staticmethod
    def table_query():
        context = Transaction().context
        fuel_type_id = context.get('fuel_type_id')

        pool = Pool()
        FuelOrder = pool.get('parking.fuel_order')
        fuel_order = FuelOrder.__table__()
        pool = Pool()
        Vehicle_Category = pool.get('parking.vehicle.category')
        vehicle_category = Vehicle_Category.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        # Department = pool.get('company.department')
        # department = Department.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()
        FuelType = pool.get('parking.fuel_type')
        fuel_type = FuelType.__table__()
        where = (fuel_order.state == 'registered')
        if context.get('start_date'):
            where &= (fuel_order.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (fuel_order.date <= context.get('end_date'))
        if context.get('fuel_type'):
            where &= (fuel_order.fuel_type == context.get('fuel_type'))
        if context.get('department'):
            where &= (fuel_order.department == context.get('department'))
        if context.get('vehicles'):
            ids = []
            for data in context.get('vehicles'):
                ids.append(data['id'])
            where &= (fuel_order.vehicle.in_(ids))

        query = fuel_order.join(vehicle, type_='LEFT',
            condition=vehicle.id ==  fuel_order.vehicle
        ).join(mark, type_='LEFT',
            condition=vehicle.mark == mark.id
        ).join(model, type_='LEFT',
            condition=vehicle.model == model.id
        ).join(fuel_type, type_='LEFT',
            condition=fuel_order.fuel_type == fuel_type.id
        ).join(vehicle_category, type_='LEFT',
            condition=vehicle.category == vehicle_category.id
        ).select(
            fuel_order.number,
            fuel_order.date,
            fuel_order.time,
            Concat(Concat(Concat(mark.name, Concat(' / ', model.name)),
                Concat(' / ', vehicle.plaque)), 
                Concat(' / ', vehicle_category.name)).as_('vehicle'),
            vehicle.id.as_('vehicle_id'),
            fuel_type.name.as_('fuel_type'),
            fuel_type.cost,
            fuel_order.fuel_cost,
            fuel_order.price,
            fuel_order.gallons_number,
            fuel_order.fuel_cost_iva,
            fuel_order.iva,
            fuel_order.mileage,
            fuel_order.description,
            fuel_order.department,
            where=where)
        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.number,
            query.date,
            query.time,
            query.vehicle,
            query.vehicle_id,
            query.fuel_type,
            query.fuel_cost,
            query.price,
            query.gallons_number,
            query.fuel_cost_iva,
            query.iva,
            query.mileage,
            query.description,
            query.department,
        )

    @classmethod
    def __setup__(cls):
        super(FuelOrderQuery, cls).__setup__()
        cls._order = [
            ('date', 'DESC'),
            ('number', 'ASC'),
        ]


class FuelOrderQueryContext(ModelView):
    'Fuel Order Query Context'
    __name__ = 'parking.fuel_order.query.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')
    fuel_type = fields.Many2One('parking.fuel_type', 'Tipo de Combustible')
    department = fields.Many2One('company.department', 'Departamento')


class FuelOrderPayment(Workflow, ModelSQL, ModelView):
    "Fuel Order Payment"
    __name__ = 'parking.fuel_order.payment'

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('registered', 'Registrado'),
    ], 'Estado', states={'readonly': True})
    number = fields.Char('Número de Liquidación', readonly=True)
    date = fields.Date('Fecha de Liquidacion')
    fuel_orders = fields.One2Many(
        'parking.fuel_order', 'payment', 'Órdenes de Combustible',
        states={
            'readonly': ~Eval('state').in_(['draft'])
        }, depends=['state', 'date'])

    @classmethod
    def __setup__(cls):
        super(FuelOrderPayment, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registered'),
        ))
        cls._buttons.update({
            'registration': {
                'invisible': Eval('state').in_(['registered']),
            },
        })
        cls._error_messages.update({
            'draft_lines': 'Algunas líneas aun no se encuentran ' + \
                'en estado "Registrado"'
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('registered')
    def registration(cls, payments):
        for payment in payments:
            for order in payment.fuel_orders:
                if order.state == 'draft':
                    cls.raise_user_error('draft_lines')
        cls.set_number(payments)
        pass

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def set_number(cls, items):
        '''
        Fill the number field with the payment sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('parking.place.configuration')
        config = Config(1)
        for payment in items:
            if payment.number:
                continue
            payment.number = Sequence.get_id(
                config.payment_sequence.id)
        cls.save(items)


class PreventiveMaintenancesQueryContext(ModelView):
    'Fuel Order Query Context'
    __name__ = 'parking.preventive.maintenance.query.context'


class VehicleMultiMileageUpdateStart(ModelView):
    'Vehicle Multi Mileage Update Start'
    __name__ = 'parking.vehicle.multi.mileage.update.start'

    vehicles = fields.One2Many('parking.vehicle.vehicle',
        None, 'Vehiculos')
    lines = fields.One2Many('parking.vehicle.multi.mileage.update.line',
        None, 'Lineas de actualización')

    @fields.depends('vehicles', 'lines')
    def on_change_vehicles(self, name=None):
        pool = Pool()
        Line = pool.get('parking.vehicle.multi.mileage.update.line')
        lines_to_keep = []
        vehicles_keeped = []
        for line in self.lines:
            if line.vehicle in self.vehicles:
                lines_to_keep.append(line)
                vehicles_keeped.append(line.vehicle)
        self.lines = lines_to_keep
        for vehicle in self.vehicles:
            if vehicle not in vehicles_keeped:
                new_line = Line()
                new_line.vehicle = vehicle
                self.lines = self.lines + (new_line,)


class VehicleMultiMileageUpdateSucceed(ModelView):
    'Vehicle Multi Mileage Update Succeed'
    __name__ = 'parking.vehicle.multi.mileage.update.succeed'


class VehicleMultiMileageUpdateLine(ModelView):
    'Vehicle Multi Mileage Update Line'
    __name__ = 'parking.vehicle.multi.mileage.update.line'

    vehicle = fields.Many2One('parking.vehicle.vehicle',
        'Vehiculo', states={'readonly': True}, domain=[
            ('type', '=', 'institutional')
        ])
    description = fields.Char('Descripción', states={'required': True})
    new_mileage = fields.Numeric('Nuevo recorrido')
    new_usage = fields.Numeric('Nuevas horas m.')


class VehicleMultiMileageUpdate(Wizard):
    'Vehicle Multi Mileage Update'
    __name__ = 'parking.vehicle.multi.mileage.update'

    start = StateView('parking.vehicle.multi.mileage.update.start',
        'parking_place.multi_mileage_update_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Generar actualizaciones', 'generate_updates', 'tryton-executable'),
        ])
    state = fields.Selection([
        ('draft', 'Borrador'),
    ],'Estado')
    generate_updates = StateTransition()
    succeed = StateView('parking.vehicle.multi.mileage.update.succeed',
        'parking_place.multi_mileage_update_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
        ])

    def transition_generate_updates(self):
        pool = Pool()
        # config_wizard = Wizard('ir.module.config_wizard')
        # config_wizard.execute('action')
        UpdateWizard = pool.get('parking.vehicle.update.mileage', type='wizard')
        session_id, _, _ = UpdateWizard.create()
        for line in self.start.lines:
             with Transaction().set_context(active_ids=[line.vehicle.id],
                    active_id=line.vehicle.id):
                update_wizard = UpdateWizard(session_id)
                update_wizard.start.new_usage = line.new_usage
                update_wizard.start.new_mileage = line.new_mileage
                update_wizard.start.description = line.description
                update_wizard.start.description = line.description
                update_wizard.transition_change()
        return 'succeed'


class PaymentFuelOrdersDateRangeStart(ModelView):
    'Payment Fuel Orders Date Range Start'
    __name__ = 'parking.vehicle.payment.fuel.orders.date.range.start'

    start_date = fields.Date('Fecha de Inicio')
    end_date = fields.Date('Fecha de Finalización')


class PaymentFuelOrdersDateRange(Wizard):
    'Payment Fuel Orders Date Range'
    __name__ = 'parking.vehicle.payment.fuel.orders.date.range'

    start = StateView(
        'parking.vehicle.payment.fuel.orders.date.range.start',
        'parking_place.vehicle_payment_fuel_order_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'change', 'tryton-ok', default=True),
        ])
    change = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PaymentFuelOrdersDateRange, cls).__setup__()
        cls._error_messages.update({
            'lower_mileage': 'Error'
        })

    def transition_change(self):
        pool = Pool()
        Payment = pool.get('parking.fuel_order.payment')
        payment = Payment(Transaction().context['active_id'])
        FuelOrder = pool.get('parking.fuel_order')
        #payment.fuel_orders = []
        aux_fuel_order = []
        aux2_fuel_order = []

        aux_fuel_order = FuelOrder.search([
            ('date', '>=', self.start.start_date),
            ('date', '<=', self.start.end_date)
        ], order=[('id', 'ASC')])
        for aux1 in aux_fuel_order:
            if aux1.payment is None:
                aux2_fuel_order.append(aux1)
        payment.fuel_orders = aux2_fuel_order
        Payment.save([payment])
        return 'end'


class IndicatorAvailabilityContext(ModelView):
    'Indicator Correcive Preventive Context'
    __name__ = 'parking.indicator.availability.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    worked_days = fields.Function(
        fields.Numeric('Parámetro de días trabajados',
        depends=['start_date', 'end_date']),
        'on_change_with_worked_days')
    vehicles_to_show = fields.Integer('Nro. vehículos a mostrar')

    @staticmethod
    def default_start_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()
    
    @staticmethod
    def default_end_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_vehicles_to_show():
        return 10
    
    # @staticmethod
    # def default_worked_days():
    #     return Decimal(313.0)

    @fields.depends('start_date', 'end_date')
    def on_change_with_worked_days(self, name=None):
        delta = self.end_date - self.start_date
        days = delta.days + 1
        if (days)> 6:
            days_to_substract = int((days) / 7)
            return days - days_to_substract
        return days


class IndicatorAvailability(ModelSQL, ModelView):
    'Indicator Availability'
    __name__ = 'parking.indicator.availability'

    inactive_hours = fields.Numeric('Horas de paro', digits=(16,2),
        readonly=True)
    available_hours = fields.Numeric('Disponibilidad (horas)', digits=(16,2),
        readonly=True)
    accidented_hours = fields.Numeric('Horas averiado',
        help='Estas horas se encuentran incluidas en las horas '
        'de paro', digits=(16,2), readonly=True)
    confidence_percentage = fields.Numeric('% de confiabilidad', digits=(16,2),
        readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)
    category = fields.Many2One('parking.vehicle.category', 'Categoría',
        readonly=True)
    # month = fields.Char('Mes', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Accident = pool.get('parking.vehicle.accident')
        accident = Accident.__table__()
        Date = pool.get('ir.date')
        today = Date.today()
        start_date = Date.today()
        end_date = Date.today()
        today = Date.today()
        where = Literal(True)
        where_accident = accident.stop_functions == True
        where_accident_before = accident.stop_functions == True
        where_accident_after = accident.stop_functions == True
        where_accident_outrange = accident.stop_functions == True
        sentence = Concat('', '')
        worked_hours = ToNumber(Concat(context.get('worked_days'), ''),'999') * 8
        # inactive_days = Sum(
        #     DatePart('day',Age(maintenance.end_date, maintenance.start_date)))
        start_date = None
        end_date = None
        inactive_days = DatePart(
            'day',Age(maintenance.end_date, maintenance.start_date))
        
        if context.get('start_date'):
            start_date = context.get('start_date')
            sentence = Concat(sentence,
                Concat('DESDE: ', context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date')) & \
                (maintenance.end_date >= context.get('start_date'))
            # MIDDLE
            where_accident &= (accident.date_accident >= context.get('start_date')) & \
                (accident.maintenance_date >= context.get('start_date'))
            # BEFORE
            where_accident_before &= (accident.date_accident < context.get('start_date'))
            # AFTER
            where_accident_after &= (accident.date_accident >= context.get('start_date')) & \
                (accident.maintenance_date >= context.get('start_date'))
            # OUT OF RANGE
            where_accident_outrange &= (accident.date_accident < context.get('start_date'))

            
        if context.get('end_date'):
            end_date = context.get('end_date')
            sentence = Concat(sentence,
                Concat('  HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date')) & \
                (maintenance.end_date <= context.get('end_date'))
            # MIDDLE
            where_accident &= (accident.date_accident <= context.get('end_date')) & \
                (accident.maintenance_date <= context.get('end_date'))
            # BEFORE
            where_accident_before &= (accident.date_accident <= context.get('end_date')) & \
                (accident.maintenance_date <= context.get('end_date'))
            # AFTER
            where_accident_after &= (accident.maintenance_date > context.get('end_date'))
            # OUT OF RANGE
            where_accident_outrange &= (accident.maintenance_date > context.get('end_date'))
        
        no_count_days = 0
        delta = end_date - start_date
        days = delta.days + 1
        if (days)> 6:
            no_count_days = int(days / 7)
        
        vehicles_to_show = None
        if context.get('vehicles_to_show'):
            vehicles_to_show = context.get('vehicles_to_show')
        else:
            vehicles_to_show = None
        
        # month_param = None
        # by_month = context.get('by_month')
        # if not by_month:
        #     month_param = Coalesce(None,None)
        # else:
        #     month_param = ToChar(maintenance.start_date, 'YY/MM')
        

        query_1 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                (Sum(inactive_days) * 8).as_('sum'),
                Literal(0).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                # month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (inactive_days > 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category])
            # group_by=[vehicle.id, vehicle.category, month_param])
        
        query_2 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                Sum(maintenance.estimated_time).as_('sum'),
                Literal(0).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                # month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (inactive_days == 0) & 
                    (maintenance.estimated_time > 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category])
            # group_by=[vehicle.id, vehicle.category, month_param])

        query_3 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                Sum((maintenance.estimated_time + 1) * 8).as_('sum'),
                Literal(0).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                # month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (inactive_days == 0) & 
                    (maintenance.estimated_time == 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category])
            # group_by=[vehicle.id, vehicle.category, month_param])
        
        # MIDDLE
        query_4 = accident.join(vehicle,
                condition=accident.vehicle == vehicle.id
            ).select(
                Sum(
                    (
                        Coalesce(accident.maintenance_date, ToDate(str(today), 'YYYY-MM-DD')) - 
                        accident.date_accident - no_count_days
                    ) * 8
                ).as_('sum'),
                Sum(
                    (
                        Coalesce(accident.maintenance_date, ToDate(str(today), 'YYYY-MM-DD')) - 
                        accident.date_accident - no_count_days
                    ) * 8
                ).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                where=(where_accident),
            group_by=[vehicle.id, vehicle.category])
        # BEFORE
        query_5 = accident.join(vehicle,
                condition=accident.vehicle == vehicle.id
            ).select(
                Sum(
                    (
                        accident.maintenance_date - 
                        ToDate(str(start_date), 'YYYY-MM-DD')  - 
                        no_count_days
                    ) * 8
                ).as_('sum'),
                Sum(
                    (
                        accident.maintenance_date - 
                        ToDate(str(start_date), 'YYYY-MM-DD')  - 
                        no_count_days
                    ) * 8
                ).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                where=(where_accident_before),
            group_by=[vehicle.id, vehicle.category])
        # AFTER
        query_6 = accident.join(vehicle,
                condition=accident.vehicle == vehicle.id
            ).select(
                Sum(
                    (
                        ToDate(str(end_date), 'YYYY-MM-DD') -
                        accident.date_accident - no_count_days
                    ) * 8
                ).as_('sum'),
                Sum(
                    (
                        ToDate(str(end_date), 'YYYY-MM-DD') -
                        accident.date_accident - no_count_days
                    ) * 8
                ).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                where=(where_accident_after),
            group_by=[vehicle.id, vehicle.category])
        # OUT OF RANGE
        query_7 = accident.join(vehicle,
                condition=accident.vehicle == vehicle.id
            ).select(
                Sum(
                    (
                        ToDate(str(end_date), 'YYYY-MM-DD') -
                        ToDate(str(start_date), 'YYYY-MM-DD') - 
                        no_count_days
                    ) * 8
                ).as_('sum'),
                Sum(
                    (
                        ToDate(str(end_date), 'YYYY-MM-DD') -
                        ToDate(str(start_date), 'YYYY-MM-DD') - 
                        no_count_days
                    ) * 8
                ).as_('accidented_hours'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                where=(where_accident_outrange),
            group_by=[vehicle.id, vehicle.category])

        query_union = Union(query_1, query_2, all_=True)
        query_union = Union(query_union, query_3, all_=True)
        query_union = Union(query_union, query_4, all_=True)
        query_union = Union(query_union, query_5, all_=True)
        query_union = Union(query_union, query_6, all_=True)
        query_union = Union(query_union, query_7, all_=True)

        return query_union.select(
                RowNumber(window=Window([])).as_('id'),
                Literal(0).as_('create_uid'),
                CurrentTimestamp().as_('create_date'),
                Literal(0).as_('write_uid'),
                CurrentTimestamp().as_('write_date'),
                (worked_hours - Sum(query_union.sum)).as_('available_hours'),
                Sum(query_union.sum).as_('inactive_hours'),
                (((worked_hours - Sum(query_union.sum)) * 100
                    ) / worked_hours).as_('confidence_percentage'),
                Sum(query_union.accidented_hours).as_('accidented_hours'),
                query_union.vehicle.as_('vehicle'),
                query_union.category.as_('category'),
                # query_union.month.as_('month'),
                where=(Literal(True)),
            group_by=[query_union.vehicle, query_union.category],
            order_by=(((worked_hours - Sum(query_union.sum)) * 100
                    ) / worked_hours).asc,
            limit=vehicles_to_show)
            # group_by=[query_union.vehicle, query_union.category, query_union.month])
        
    @classmethod
    def __setup__(cls):
        super(IndicatorAvailability, cls).__setup__()
        cls._order = [
            ('inactive_hours', 'DESC'),
            # ('month', 'DESC')
        ]

    # dias estimados son = 0
    # la diferencia de dias es = 0
    #     se toman las horas estimadas

    # dias estimados son = 0
    # la diferencia de dias es > 0
    #     se toman los dias * 8

    # las horas estimadas son = 0
    # la diferencia de dias es = 0
    #     se toman los dias * 8

    # select sum(date_part('day', AGE(pvm.end_date, pvm.start_date ))), pvv.id
    # from financiero.parking_vehicle_vehicle pvv 
    # inner join financiero.parking_vehicle_maintenance pvm
    # 	on pvv.id = pvm.vehicle 
    # where pvm.start_date is not null
    # 	and pvm.start_time is not null
    # 	and pvm.end_date is not null
    # 	and pvm.end_time is not null
    # 	and date_part('day', AGE(pvm.end_date, pvm.start_date )) > 0
    # group by pvv.id



class IndicatorVehicleCostContext(ModelView):
    'Indicator Correcive Preventive Context'
    __name__ = 'parking.indicator.vehicle.cost.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicles_to_show = fields.Integer('Nro. vehículos a mostrar')
    
    @staticmethod
    def default_vehicles_to_show():
        return 10


class IndicatorVehicleCost(ModelSQL, ModelView):
    'Indicator VehicleCost'
    __name__ = 'parking.indicator.vehicle.cost'

    cost = fields.Numeric('Costo en mantenimientos', readonly=True)
    vehicle = fields.Char('Vehículo', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Mark = pool.get('parking.vehicle.mark')
        mark = Mark.__table__()
        Model = pool.get('parking.vehicle.model')
        model = Model.__table__()
        where = Literal(True)
        sentence = Concat('', '')
        vehicles_to_show = None
        if context.get('start_date'):
            sentence = Concat(sentence,
                Concat('DESDE: ', context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date')) & \
                (maintenance.end_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                Concat('  HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date')) & \
                (maintenance.end_date <= context.get('end_date'))
        if context.get('vehicles_to_show'):
            vehicles_to_show = context.get('vehicles_to_show')


        query_all = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).join(mark,
                condition=vehicle.mark == mark.id, type_='LEFT'
            ).join(model,
                condition=vehicle.model == model.id, type_='LEFT'
            ).select(
                Sum(maintenance.total_activities + 
                    maintenance.total_external_spares +
                    maintenance.total_spares ).as_('cost'),
                Concat(
                    Concat(
                        Concat(
                            Concat(vehicle.vehicle_name, " - "), 
                            Coalesce(vehicle.plaque, 'Sin placa')
                        ),' - '),
                        Coalesce(model.name,'Sin modelo')
                    ).as_('vehicle'),
                # Concat(vehicle.vehicle_name, vehicle.plaque).as_('vehicle'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (maintenance.state != 'deny') & 
                    (maintenance.state != 'draft') & 
                    (maintenance.state != 'cancel') & 
                    (where),
            group_by=[Concat(
                    Concat(
                        Concat(
                            Concat(vehicle.vehicle_name, " - "), 
                            Coalesce(vehicle.plaque, 'Sin placa')
                        ),' - '),
                        Coalesce(model.name,'Sin modelo')
                    )])

        query_firsts = query_all.select(
            query_all.cost.as_('cost'),
            query_all.vehicle.as_('vehicle'),
            order_by=query_all.cost.desc,
            limit=vehicles_to_show
        )
        query_repair = query_firsts.select(
            query_firsts.cost.as_('cost'),
            query_firsts.vehicle.as_('vehicle')
        )

        query_remove_ids = query_all.select(
            query_all.vehicle,
            order_by=query_all.cost.desc,
            limit=vehicles_to_show
        )
        
        query_restant = query_all.select(
            Sum(query_all.cost).as_('cost'),
            Concat("Otros","").as_('vehicle'),
            where=(NotIn(query_all.vehicle, query_remove_ids))
        )

        query_union = Union(query_restant,query_repair)
        return query_union.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_union.cost,
            # Concat(Concat(query_union.vehicle, ' - $'),
            #     query_union.cost).as_('vehicle')
            query_union.vehicle.as_('vehicle')
        )
        
    @classmethod
    def __setup__(cls):
        super(IndicatorVehicleCost, cls).__setup__()
        cls._order = [
            ('cost', 'DESC')
        ]

    # dias estimados son = 0
    # la diferencia de dias es = 0
    #     se toman las horas estimadas

    # dias estimados son = 0
    # la diferencia de dias es > 0
    #     se toman los dias * 8

    # las horas estimadas son = 0
    # la diferencia de dias es = 0
    #     se toman los dias * 8

    # select sum(date_part('day', AGE(pvm.end_date, pvm.start_date ))), pvv.id
    # from financiero.parking_vehicle_vehicle pvv 
    # inner join financiero.parking_vehicle_maintenance pvm
    # 	on pvv.id = pvm.vehicle 
    # where pvm.start_date is not null
    # 	and pvm.start_time is not null
    # 	and pvm.end_date is not null
    # 	and pvm.end_time is not null
    # 	and date_part('day', AGE(pvm.end_date, pvm.start_date )) > 0
    # group by pvv.id


class IndicatorAvailabilityByMonthContext(ModelView):
    'Indicator Correcive Preventive By MonthContext'
    __name__ = 'parking.indicator.availability.by_month.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    # worked_days = fields.Numeric('Parámetro de días trabajados')
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo')
    
    # @staticmethod
    # def default_worked_days():
    #     return Decimal(313.0)


class IndicatorAvailabilityByMonth(ModelSQL, ModelView):
    'Indicator Availability By Month'
    __name__ = 'parking.indicator.availability.by_month'

    inactive_hours = fields.Numeric('Horas inactivo', digits=(16,2),
        readonly=True)
    confidence_percentage = fields.Numeric('% de confiabilidad', digits=(16,2),
        readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)
    category = fields.Many2One('parking.vehicle.category', 'Categoría',
        readonly=True)
    month = fields.Char('Mes', readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        where = Literal(True)
        sentence = Concat('', '')
        worked_hours = ToNumber(Concat(30, ''),'999') * 8
        # worked_hours = ToNumber(Concat(context.get('worked_days'), ''),'999') * 8
        # inactive_days = Sum(
        #     DatePart('day',Age(maintenance.end_date, maintenance.start_date)))
        inactive_days = DatePart(
            'day',Age(maintenance.end_date, maintenance.start_date))
        if context.get('start_date'):
            sentence = Concat(sentence,
                Concat('DESDE: ', context.get('start_date')))
            where &= (maintenance.start_date >= context.get('start_date')) & \
                (maintenance.end_date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                Concat('  HASTA: ', context.get('end_date')))
            where &= (maintenance.start_date <= context.get('end_date')) & \
                (maintenance.end_date <= context.get('end_date'))
        
        vehicle_id = context.get('vehicle')

        month_param = ToChar(maintenance.start_date, 'YY/MM')
        

        query_1 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                (Sum(inactive_days) * 8).as_('sum'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (maintenance.vehicle == vehicle_id) &
                    (inactive_days > 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category, month_param])
        
        query_2 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                Sum(maintenance.estimated_time).as_('sum'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (maintenance.vehicle == vehicle_id) &
                    (inactive_days == 0) & 
                    (maintenance.estimated_time > 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category, month_param])

        query_3 = maintenance.join(vehicle,
                condition=maintenance.vehicle == vehicle.id
            ).select(
                Sum((maintenance.estimated_time + 1) * 8).as_('sum'),
                vehicle.id.as_('vehicle'),
                vehicle.category.as_('category'),
                month_param.as_('month'),
                where=(maintenance.end_date != None) & 
                    (maintenance.start_date != None) & 
                    (maintenance.vehicle == vehicle_id) &
                    (inactive_days == 0) & 
                    (maintenance.estimated_time == 0) & 
                    (where),
            group_by=[vehicle.id, vehicle.category, month_param])

        query_union = Union(query_1, query_2, all_=True)
        query_union = Union(query_union, query_3, all_=True)

        return query_union.select(
                RowNumber(window=Window([])).as_('id'),
                Literal(0).as_('create_uid'),
                CurrentTimestamp().as_('create_date'),
                Literal(0).as_('write_uid'),
                CurrentTimestamp().as_('write_date'),
                Sum(query_union.sum).as_('inactive_hours'),
                (((worked_hours - Sum(query_union.sum)) / 
                    worked_hours) * 100).as_('confidence_percentage'),
                query_union.vehicle.as_('vehicle'),
                query_union.category.as_('category'),
                query_union.month.as_('month'),
                where=(Literal(True)),
            group_by=[query_union.vehicle, query_union.category, query_union.month],
            order_by=(((worked_hours - Sum(query_union.sum)) / 
                    worked_hours) * 100).asc)
        
    @classmethod
    def __setup__(cls):
        super(IndicatorAvailabilityByMonth, cls).__setup__()
        cls._order = [
            ('month', 'ASC')
        ]


class PeriodicTasksByExecutedContext(ModelView):
    'Periodict tast by execute context'
    __name__ = 'parking.periodict.task.by.executed.context'

    start_date = fields.Date('Fecha inicio')
    end_date = fields.Date('Fecha fin')
    vehicle = fields.One2Many('parking.vehicle.vehicle', None, 'Vehículos')

    @staticmethod
    def default_start_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_end_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()


class PeriodicTasksByExecutedQuery(ModelSQL, ModelView):
    'Periodict tast by execute Query'
    __name__ = 'parking.periodict.task.by.query'

    date = fields.Date('Fecha')
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
                              readonly=True)
    description= fields.Char('Descripcion')
    maintenance = fields.Many2One('parking.vehicle.maintenance.preventive',
                                  'Mantenimientos',readonly=True)

    @classmethod
    def __setup__(cls):
        super(PeriodicTasksByExecutedQuery, cls).__setup__()
        cls._order = [
            ('date', 'DESC')
        ]

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        MaintenancePreventive = pool.get(
            'parking.vehicle.maintenance.preventive')
        maintenance = MaintenancePreventive.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        where = Literal(True)
        sentence = Concat('', '')

        if context.get('start_date'):
            sentence = Concat(sentence,
                              Concat('DESDE: ', context.get('start_date')))
            where &= (maintenance.date >= context.get('start_date'))
        if context.get('end_date'):
            sentence = Concat(sentence,
                              Concat('  HASTA: ', context.get('end_date')))
            where &= (maintenance.date <= context.get('end_date'))
        if context.get('vehicle'):
            ids = []
            for data in context.get('vehicle'):
                ids.append(data['id'])
                where &= (maintenance.vehicle.in_(ids))

        query = maintenance.join(vehicle,
            condition=maintenance.vehicle == vehicle.id
            ).select(
            vehicle.id.as_('vehicle'),
            maintenance.date.as_('date'),
            maintenance.id.as_('maintenance'),
            maintenance.maintenance_description.as_('description'),
            where=(maintenance.maintenance_related == Null) & (where))

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.vehicle,
            query.date,
            query.maintenance,
           query.description)





