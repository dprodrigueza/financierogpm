# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.parking_place.tests.test_parking_place import suite
except ImportError:
    from .test_parking_place import suite

__all__ = ['suite']
