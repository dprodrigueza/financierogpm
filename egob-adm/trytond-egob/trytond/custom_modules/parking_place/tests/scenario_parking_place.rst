===============
Parking Place 
===============

Imports::
    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.galeno.tests.tools import create_country
    >>> from trytond_gis import fields as GeoFields

Install Parking Place::
    >>> config = activate_modules(['parking_place'])

Create Variables::
    >>> Lang = Model.get('ir.lang')
    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    
Create Company::
    >>> _ = create_company()
    >>> company = get_company()

Select lang::
    >>> lang, = Lang.find(['code', '=', 'es'])

Create County::
    >>> country = create_country()
    >>> subdivision = country.subdivisions[0]
    >>> Subdivision = Model.get('country.subdivision')
    >>> city, = Subdivision.find([('parent', '=', subdivision.id)])

