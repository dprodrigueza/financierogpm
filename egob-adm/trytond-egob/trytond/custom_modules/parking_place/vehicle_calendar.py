from sql import Window, Literal, Union, Null
from sql.functions import RowNumber, CurrentTimestamp
from sql.operators import Concat
from sql.conditionals import Coalesce
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.parking_place.vehicle import MAX_DATE
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import If, Bool, Eval

import datetime

__all__ = ['VehicleCalendarAvailableContext', 'VehicleCalendarAvailableHeader']


class VehicleCalendarAvailableContext(ModelView):
    'Vehicle Calendar Available Context'
    __name__ = 'parking.vehicle.calendar.available.context'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        domain=[
            If(Bool(Eval('to_movilization')),
                [('to_movilization', '=', True)],
                [])
            ], depends=['to_movilization']
        )
    to_movilization = fields.Boolean('Para movilización?')

    @classmethod
    def default_to_movilization(cls, name=None):
        return False


class VehicleCalendarAvailableHeader(ModelSQL, ModelView):
    'Vehicle Calendar Available Header'
    __name__ = 'parking.vehicle.calendar.available.header'
    
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)
    number = fields.Char('Registro No.', readonly=True)
    description = fields.Char('Descripción', readonly=True)
    origin = fields.Reference('Origen',
                              [
                                ('parking.vehicle.accident', 'Accidente'),
                                ('parking.mobilization.order',
                                   'Orden de movilización'),
                                ('parking.vehicle.maintenance',
                                   'Mantenimiento'),
                                ('parking.vehicle.maintenance.preventive',
                                   'Planificación')
                              ], readonly=True)
    origin_model = fields.Function(fields.Selection('get_origin_models',
                                                    'Modelo origen'),
                                   'on_change_with_origin_model')
    start_date = fields.Date('Fecha inicio', readonly=True, states={
        'invisible': True
    })
    end_date = fields.Date('Fecha fin', readonly=True, states={
        'invisible': True
    })
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehículo',
        readonly=True)

    color = fields.Char('Color', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('send', 'Enviado'),
        ('entering', 'Ingresando'),
        ('request', 'Solicitada'),
        ('deny', 'Negado'),
        ('waiting', 'Pendiente'),
        ('handed', 'Repuestos recibidos'),
        ('process', 'En proceso'),
        ('finished', 'Finalizado'),
        ('canceled', 'Anulado'),
    ], 'Estado', states={'readonly': True})
    state_translated = state.translated('state')

    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__

    @staticmethod
    def _get_origin():
        return [
            'parking.vehicle.accident',
            'parking.mobilization.order',
            'parking.vehicle.maintenance',
            'parking.vehicle.maintenance.preventive'
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @staticmethod
    def table_query():
        context = Transaction().context
        vehicle_id = context.get('vehicle')
        to_movilization = context.get('to_movilization')
            
        pool = Pool()
        Accident = pool.get('parking.vehicle.accident')
        accident = Accident.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Order = pool.get('parking.mobilization.order')
        order = Order.__table__()
        Preventive = pool.get('parking.vehicle.maintenance.preventive')
        preventive = Preventive.__table__()
        Workshop = pool.get('parking.workshop')
        workshop = Workshop.__table__()
        where = (Literal(True))
        if to_movilization:
            where = where & (vehicle.to_movilization == True)
        if vehicle_id:
            where = where & (vehicle.id == vehicle_id)

        queryAccident = accident.join(vehicle,
            condition=vehicle.id == accident.vehicle
            ).select(
            accident.number.as_('number'),
            Concat('parking.vehicle.accident,',
                   accident.id).as_('origin'),
            accident.date_accident.as_('start_date'),
            accident.date_accident.as_('end_date'),
            accident.vehicle.as_('vehicle'),
            Concat("Accidente: ", accident.state).as_('state'),
            Concat("#5fb7f9", "").as_("color"),
            accident.description.as_('description'),
            where=where )

        queryMaintenanceDraft = maintenance.join(vehicle,
                condition=vehicle.id == maintenance.vehicle
            ).join(workshop, type_='LEFT',
                condition=workshop.id == maintenance.workshop
            ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                   maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, maintenance.start_date
                ).as_('end_date'),
            maintenance.vehicle,
            Concat("Mant: ", Concat(Concat(maintenance.state," - "),Coalesce(workshop.type_, "No asignado"))).as_('state'),
            Concat("#ffcdcd", "").as_("color"),
            maintenance.description.as_("description"),
            where=where
            & (maintenance.state == 'draft') )
        
        queryMaintenanceFinished = maintenance.join(vehicle,
                condition=vehicle.id == maintenance.vehicle
            ).join(workshop, type_='LEFT',
                condition=workshop.id == maintenance.workshop
            ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                   maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, maintenance.start_date
                ).as_('end_date'),
            maintenance.vehicle,
            Concat("Mant: ", Concat(Concat(maintenance.state," - "),Coalesce(workshop.type_, "No asignado"))).as_('state'),
            Concat("#cfefcd", "").as_("color"),
            maintenance.description.as_("description"),
            where=where
             & (maintenance.state == 'finished') )

        queryMaintenance = maintenance.join(vehicle,
                condition=vehicle.id == maintenance.vehicle
            ).join(workshop, type_='LEFT',
                condition=workshop.id == maintenance.workshop
            ).select(
            maintenance.number.as_('number'),
            Concat('parking.vehicle.maintenance,',
                   maintenance.id).as_('origin'),
            maintenance.start_date,
            Coalesce(maintenance.end_date, maintenance.start_date
                ).as_('end_date'),
            maintenance.vehicle,
            Concat("Mant: ", Concat(Concat(maintenance.state," - "),Coalesce(workshop.type_, "No asignado"))).as_('state'),
            Concat("#5fb7f9", "").as_("color"),
            maintenance.description.as_("description"),
            where=where
             & (maintenance.state != 'draft')
             & (maintenance.state != 'finished') )

        queryOrder = order.join(vehicle,
            condition=vehicle.id == order.vehicle
            ).select(
            order.number.as_('number'),
            Concat('parking.mobilization.order,',
                   order.id).as_('origin'),
            order.departure_date.as_('start_date'),
            order.return_date.as_('end_date'),
            order.vehicle.as_('vehicle'),
            Concat("Orden de M.: ", order.state).as_('state'),
            Concat("#5fb7f9", "").as_("color"),
            order.reason.as_("description"),
            where=where & (order.state != 'draft'))

        queryPlannified = preventive.join(vehicle,
            condition=vehicle.id == preventive.vehicle
            ).select(
            Concat("sin #", "").as_('number'),
            Concat('parking.vehicle.maintenance.preventive,',
                   preventive.id).as_('origin'),
            preventive.date.as_('start_date'),
            preventive.date.as_('end_date'),
            preventive.vehicle.as_('vehicle'),
            Concat("Planficado: ", "sin estado").as_('state'),
            Concat("#fdfe94", "").as_("color"),
            preventive.maintenance_description.as_("description"),
            where=where & (preventive.maintenance_related == Null) &
                (preventive.is_cycle_by_date == True)
            )

        union = Union(queryAccident, queryOrder, all_=True)
        union = Union(union, queryMaintenance, all_=True)
        union = Union(union, queryMaintenanceFinished, all_=True)
        union = Union(union, queryMaintenanceDraft, all_=True)
        union = Union(union, queryPlannified, all_=True)

        return union.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            union.number,
            union.origin,
            union.start_date,
            union.end_date,
            union.vehicle,
            union.state,
            union.color,
            union.description
        )

    @classmethod
    def __setup__(cls):
        super(VehicleCalendarAvailableHeader, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]
    
    @classmethod
    def view_attributes(cls):
        return super(VehicleCalendarAvailableHeader, cls).view_attributes() + [
            ('/calendar/field[@name="start_date"]', 'states', {
                'invisible': True,
            }),
            ('/calendar/field[@name="end_date"]', 'states', {
                'invisible': True,
            }),
        ]


class VehicleAvailableContext(ModelView):
    'Vehicle Available Context'
    __name__ = 'parking.vehicle.available.query.context'

    start_date = fields.Date('Fecha salida')
    return_date = fields.Date('Fecha regreso')
    start_time = fields.Time('Hora salida')
    return_time = fields.Time('Hora retorno')


class VehicleAvailableQuery(ModelSQL, ModelView):
    'Vehicle AvailableQuery'
    __name__ = 'parking.vehicle.available.query'

    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehiculo')

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Mobilization = pool.get('parking.mobilization.order')
        mobilization = Mobilization.__table__()
        Vehicle = pool.get('parking.vehicle.vehicle')
        vehicle = Vehicle.__table__()
        Accident = pool.get('parking.vehicle.accident')
        accident = Accident.__table__()
        Maintenance = pool.get('parking.vehicle.maintenance')
        maintenance = Maintenance.__table__()

        querySelect = None
        if context.get('start_date') and context.get('start_time') and context.get('return_date') and context.get('return_time'):
            start = datetime.datetime.combine(context.get('start_date'), context.get('start_time'))
            end = datetime.datetime.combine(context.get('return_date'), context.get('return_time'))
            querySelect = mobilization.join(
                vehicle, condition=mobilization.vehicle == vehicle.id
            ).select(
                vehicle.id.as_('vehicle'),
                where=
                    (
                        (start >= (mobilization.departure_date + mobilization.departure_time)) &
                        (start <= (mobilization.return_date + mobilization.return_time))
                    ) 
                        |
                    (
                        (end >= (mobilization.departure_date + mobilization.departure_time)) &
                        (end <= (mobilization.return_date + mobilization.return_time))
                    )   
                        |
                    (
                        ((mobilization.departure_date + mobilization.departure_time) >= start) &
                        ((mobilization.return_date + mobilization.return_time) <= end)
                    ) 
                )
        else:
            querySelect = mobilization.join(
                vehicle, condition=mobilization.vehicle == vehicle.id
            ).select(
                vehicle.id.as_('vehicle'))

        query = vehicle.select(
            vehicle.id.as_('vehicle'),
            vehicle.to_movilization,
            where=~vehicle.id.in_([querySelect])
        ) 

        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.vehicle,
            where=query.to_movilization == True
        )

    @classmethod
    def __setup__(cls):
        super(VehicleAvailableQuery, cls).__setup__()


class MobilizationOrderCalendar(ModelSQL, ModelView):
    'Mobilization Order Calendar'
    __name__ = 'parking.vehicle.mobilization.order.calendar'

    number = fields.Char('Registro No.', readonly=True)
    start_date = fields.Date('Fecha inicio')
    start_time = fields.Time('Hora salida')
    end_date = fields.Date('Fecha fin')
    return_time = fields.Time('Hora de Retorno')
    range_time = fields.Function(fields.Char('Rango de horas'),
        'on_change_with_range_time')
    color = fields.Char('Color', readonly=True)
    vehicle = fields.Many2One('parking.vehicle.vehicle', 'Vehiculo')

    @fields.depends('start_time', 'return_time')
    def on_change_with_range_time(self, name=None):
        return str(self.start_time) + " - " + str(self.return_time)

    @staticmethod
    def table_query():
        pool = Pool()
        context = Transaction().context
        vehicle_id = context.get('vehicle')
        Order = pool.get('parking.mobilization.order')
        order = Order.__table__()
        Vehicle = pool.get('company.position')
        vehicle = Vehicle.__table__()
        where = (Literal(True))

        queryOrder = order.join(vehicle,
            condition=vehicle.id == order.vehicle
            ).select(
            order.number.as_('number'),
            order.departure_time.as_('start_time'),
            order.return_time.as_('return_time'),
            order.departure_date.as_('start_date'),
            order.return_date.as_('end_date'),
            order.vehicle.as_('vehicle'),
            Concat("#5fb7f9", "").as_("color"),
            where=where & (order.state != 'draft'))

        return queryOrder.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            queryOrder.number,
            queryOrder.start_date,
            queryOrder.end_date,
            queryOrder.start_time,
            queryOrder.return_time,
            queryOrder.vehicle,
            queryOrder.color,
            where=where
        )

    @classmethod
    def __setup__(cls):
        super(MobilizationOrderCalendar, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC')
        ]

