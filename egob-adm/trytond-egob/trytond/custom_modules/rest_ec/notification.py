
__all__ = ['EmailLog']

from trytond.model import fields
from trytond.pool import PoolMeta


class EmailLog(metaclass=PoolMeta):
    __name__ = 'notification.email.log'

    acknowledge = fields.Boolean('Leido')
    subject = fields.Char("Asunto")
    content = fields.Text("Contenido")
