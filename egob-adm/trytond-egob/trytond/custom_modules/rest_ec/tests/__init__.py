# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.siim_rest.tests.test_siim_rest import suite
except ImportError:
    from .test_siim_rest import suite

__all__ = ['suite']
