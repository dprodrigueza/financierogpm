from trytond.pool import PoolMeta

__all__ = ['UserApplication', 'User']


class UserApplication(metaclass=PoolMeta):
    __name__ = 'res.user.application'

    @classmethod
    def __setup__(cls):
        super(UserApplication, cls).__setup__()
        cls.application.selection.append(('siim', 'SIIM Comercial'))


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._error_messages.update({
            'username_exists': ('El nombre de usuario %(username)s ya existe')
        })