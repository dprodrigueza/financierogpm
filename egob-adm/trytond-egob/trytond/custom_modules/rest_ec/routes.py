import json
import base64

from trytond.wsgi import app
from trytond.protocols.wrappers import with_pool, with_transaction, \
        user_application
from trytond.protocols.jsonrpc import JSONEncoder
import decimal
import re
from unicodedata import normalize
from trytond.transaction import Transaction
from werkzeug.wrappers import Response

siim_application = user_application('siim')


def base64_decode(base64_message, encoding='utf-8'):
    base64_bytes = base64_message.encode(encoding)
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode(encoding)
    return message


def get_group_data(groups):
    result = []
    for group in groups:
        s = group.name
        s = re.sub(
            r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
            normalize("NFD", s), 0, re.I
        )
        s = normalize('NFC', s)
        if not group.parent:
            parent = None
        else:
            parent = group.parent.id
        position = {
            'id': group.id,
            'name': s,
            'parent' : parent
        }

        # menus = {}
        # for menu in group.menu_access:
        #     parent = None
        #     if menu.parent:
        #         parent = menu.parent.id
        #     menus[menu.id] = {
        #         'name': menu.name,
        #         'parent': parent
        #     }
        # position['menus'] = menus
        result.append(position)
    return result


@app.route('/<database_name>/siim/user/groups/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def groups(request, pool):
    Group = pool.get('res.group')
    groups = Group.search([])
    result = get_group_data(groups)
    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'))
    return data


@app.route('/<database_name>/siim/user/<login>/groups/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def user_groups(request, pool, login):
    result = []
    User = pool.get('res.user')
    user = User.search([
        ('login', '=', login),
    ])
    if user:
        result += get_group_data(user[0].groups)

    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data


@app.route('/<database_name>/siim/update_user/<login>/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def update_user(request, pool, login):
    User = pool.get('res.user')
    user = User.search([
        ('login', '=', login),
    ])
    if user:
        data = json.loads(request.data)
        user[0].groups = data['groups']
        # user[0].save()


@app.route('/<database_name>/siim/user/menus/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def menus(request, pool):
    Menu = pool.get('ir.ui.menu')
    menus = Menu.search([])
    result = []
    for menu in menus:
        parent = menu.parent
        if not parent:
            s = menu.name
            s = re.sub(
                r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                normalize("NFD", s), 0, re.I
            )
            s = normalize('NFC', s)
            position = {
                'id': menu.id,
                'name': s
            }
            result.append(position)
    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data


@app.route('/<database_name>/siim/user/groups/parent/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_groups_parent(request, pool):
    Group = pool.get('res.group')
    groups = Group.search([])

    resultado = []
    for group in groups:
        if not group.parent:
            s = group.name
            s = re.sub(
                r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                normalize("NFD", s), 0, re.I
            )
            s = normalize('NFC', s)
            parent = None
            position = {
                'id': group.id,
                'name': s,
                'parent': parent
            }

            resultado.append(position)

    resultado = sorted(resultado, key=lambda k: k['name'])
    data = json.dumps(resultado, cls=JSONEncoder, separators=(',', ':'))
    return data

@app.route('/<database_name>/siim/user/all/groups/child/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_all_groups_child(request, pool):
    Group = pool.get('res.group')
    groups = Group.search([])

    resultado = []
    for group in groups:
        if group.parent:
            s = group.name
            s = re.sub(
                r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                normalize("NFD", s), 0, re.I
            )
            s = normalize('NFC', s)
            parent = group.parent.id
            position = {
                'id': group.id,
                'name': s,
                'parent': parent
            }

            resultado.append(position)
    print("Size",len(resultado))
    resultado = sorted(resultado, key=lambda k: k['name'])

    data = json.dumps(resultado, cls=JSONEncoder, separators=(',', ':'))
    return data

@app.route('/<database_name>/siim/user/<int:group_parent>/groups/childs/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_groups_childs(request, pool, group_parent):

    Group = pool.get('res.group')
    groups = Group.search([
        ('parent', '=', group_parent),
        ])

    result = get_group_data(groups)
    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'))
    return data

@app.route('/<database_name>/siim/get_menu_group/<menubusca>/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_menus_group(request, pool, menubusca):
    MenuGrupo = pool.get('ir.ui.menu-res.group')
    menugrupos = MenuGrupo.search([])
    result = []
    for mg in menugrupos:
        menu = mg.menu
        while True:
            parent = menu.parent
            if not parent:
                if(menu.id == int(menubusca)):
                    grupo = mg.group
                    s = grupo.name
                    s = re.sub(
                        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                        normalize("NFD", s), 0, re.I
                    )
                    s = normalize('NFC', s)
                    position = {
                        'id': grupo.id,
                        'name': s
                    }
                    # print("position")
                    # print(position)
                    #print("Tipo",type(result))
                    repite = False
                    for r in result:
                        if (int(r['id']) == grupo.id):
                            repite = True
                            break

                    if(repite == False):
                        result.append(position)
                break
            else:
                Menus = pool.get('ir.ui.menu')
                menu = Menus.search([
                    ('id', '=', parent.id),
                ])
                menu = menu[0]

    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'), ensure_ascii=False)
    return data

@app.route('/<database_name>/siim/get_group_parent_user/<login>/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_groups_parent_user(request, pool, login):
    result = []
    User = pool.get('res.user')
    user = User.search([
        ('login', '=', login),
    ])
    Group = pool.get('res.group')
    groups_ = None
    if user:
        groups_ = Group.search([
            ('parent', '=', None),
            ['OR',
             ('users', 'in', [user[0].id] ),
             ('childs', 'where', [
                 ('users', 'in', [user[0].id])
             ])
        ]])

    if groups_:
        result += get_group_data(groups_)

    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data


@app.route('/<database_name>/siim/get_groups_child_user/<login>/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_groups_child_user(request, pool, login):
    result = []
    User = pool.get('res.user')
    user = User.search([
        ('login', '=', login),
    ])
    Group = pool.get('res.group')
    groups_ = None
    if user:
        groups_ = Group.search([
            ('parent', '!=', None),
            ['OR',
             ('users', 'in', [user[0].id] ),
             ('childs', 'where', [
                 ('users', 'in', [user[0].id])
             ])
        ]])

    if groups_:
        result += get_group_data(groups_)
    result = sorted(result, key=lambda k: k['name'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data



@app.route('/<database_name>/siim/get_notifications_user/<login>/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_notifications_user(request, pool, login):
    result = []
    resultTemporal = []
    User = pool.get('res.user')
    if login == "ADMIN":
        login = "admin"
    users = User.search([
        ('login', '=', login),
    ])
    email = ""
    if users:
        user = users[0]
        if user.employee and user.employee.email:
            email = user.employee.email
    if email:
        Notifications = pool.get('notification.email.log')
        notifications = Notifications.search([
            ('recipients', 'ilike', f'%{email}%'),
        ])
        pattern = "<(.*?)>"
        for n in notifications:
            recipient = re.search(pattern, n.recipients).group(1)
            if(recipient == email):
                resultTemporal.append(n)
        if resultTemporal:
            for n in resultTemporal:
                if n.acknowledge == False:
                    c = n.content
                    if(c!= None):
                        c = re.sub(
                            r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                            normalize("NFD", c), 0, re.I
                        )
                        c = normalize('NFC', c)
                    else:
                        c= ""
                    s = n.subject
                    if (s != None):

                        s = re.sub(
                            r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
                            normalize("NFD", s), 0, re.I
                        )
                        s = normalize('NFC', s)
                    else:
                        s=""
                    position = {
                        'id': n.id,
                        'date': (n.date.day,n.date.month,n.date.year),
                        # 'date': n.date,
                        'acknowledge': n.acknowledge,
                        'subject': s,
                        'content': c,
                        'mail': email.lower()
                    }
                    result.append(position)
        result = sorted(result, key=lambda k: k['id'])
        data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
        return data
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'), ensure_ascii=False)
    return data

@app.route('/<database_name>/siim/update_notification/<id>/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def update_user(request, pool, id):
    Notifications = pool.get('notification.email.log')
    notification = Notifications.search([
        ('id', '=', id),
    ])
    if notification:
        data = json.loads(request.data)
        notification[0].acknowledge = data['acknowledge']
        notification[0].save()


@app.route('/<database_name>/siim/get_modules_user/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def get_modules_user(request, pool):

    data = json.loads(request.data)
    ids = data['ids']
    result = []

    menus = pool.get('ir.ui.menu')

    menuBusca = menus.search([
        ('id', 'in', ids),
    ])
    for menu in menuBusca:
        if menu.parent:
            parent = menu.parent.id
        else:
            parent = ''
        n = menu.name
        n = re.sub(
            r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
            normalize("NFD", n), 0, re.I
        )
        n = normalize('NFC', n)

        position = {
            'id': menu.id,
            'parent': parent,
            'name': n
        }
        result.append(position)
    result = sorted(result, key=lambda k: k['id'])
    datos = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return datos

@app.route('/<database_name>/siim/update_menu_bar/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def update_menu_bar(request, pool):

    TreeState = pool.get('ir.ui.view_tree_state')
    data = json.loads(request.data)
    TreeState.set(data['model'], data['domain'], data['child_name'], data['nodes'], data['selected_nodes'])




@app.route('/<database_name>/siim/get_ids_of_bodega/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_ids_of_bodega(request, pool):

    data = json.loads(request.data)
    ids = data['ids']

    SiimItem = pool.get('public.siim.item')

    items = SiimItem.search([
        ('item_id', 'in', ids),
    ], limit=1000)
    ids = []
    for i in items:
        ids.append(i.item_id)

    data = json.dumps(ids, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data



@app.route('/<database_name>/siim/get_products_of_bodega/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def get_products_of_bodega(request, pool):

    data = json.loads(request.data)
    ids = data['ids']
    informacionTributaria = data['informacion_tributaria']

    SiimItem = pool.get('public.siim.item')


    Company = pool.get('company.company')
    company_id = Company.search([
        ('tax_id', '=', informacionTributaria),
    ])

    with Transaction().set_context(company=company_id[0].id):
        result = SiimItem.read(ids, ['item_id', 'price', 'quantity'])
    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data

@app.route('/<database_name>/siim/send_warehouse_parameters/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def send_warehouse_parameters(request, pool):
    ShipmentInternal= pool.get('stock.shipment.internal')
    data = json.loads(request.data)

    informacionTributaria = data['informacion_tributaria']
    Company = pool.get('company.company')
    company_id = Company.search([
        ('tax_id', '=', informacionTributaria),
    ])

    with Transaction().set_context(company=company_id[0].id):
        result = ShipmentInternal.get_move_data(data['idFacturacion'], data['to_sent'])

    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'), ensure_ascii=False)

    return data

@app.route('/<database_name>/siim/create_move_stock/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def create_move_stock(request, pool):
    Move= pool.get('stock.move')
    data = json.loads(request.data)
    moves = data['data']
    Move.create(moves)

@app.route('/<database_name>/siim/get_bodega/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def get_bodega(request, pool):
    data = json.loads(request.data)
    idItem = data['ids']
    informacionTributaria = data['informacion_tributaria']

    Company = pool.get('company.company')
    company_id = Company.search([
        ('tax_id', '=', informacionTributaria),
    ])

    StockLocation = pool.get('stock.location')

    with Transaction().set_context(company=company_id[0].id):
        result = StockLocation.read(idItem, ['id', 'name', 'type'])

    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data

@app.route('/<database_name>/siim/get_consignment_products/', methods=['GET'])
@with_pool
@with_transaction()
@siim_application
def get_consignment_products(request, pool):

    result = []

    ConsignmentL = pool.get('stock.products.to.consignment.lines')

    items = ConsignmentL .search([
        ('id', '>', 0),
    ],)
    result = sorted(items, key=lambda k: k['siim_item'])

    data = json.dumps(result, cls=JSONEncoder, separators=(',', ':'),ensure_ascii=False)
    return data

@app.route('/<database_name>/siim/create_user/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def create_user(request, pool):
    '''
    Create a new user
    :param request: Http request
        Data/Body:
            username: username of new user (must be unique)
            firstnames: First names of new user
            lastnames: Last names of new user
            email: Electronic mail of new user
            password: Password of new user with base64 encoding on utf-8 chars.
                If the password is not provided, it is understood that the
                authentication in the system is done through an external
                authentication service (for example: CAS), so it is not required
                to save the password.
    :param pool: Tryton connection pool
    :return: Http response (200 ok, 400 invalid body, 500 internal error)
    '''
    User = pool.get('res.user')
    ResGroup = pool.get('res.group')
    ResUserGroup = pool.get('res.user-res.group')
    Party = pool.get('party.party')
    Company = pool.get('company.company')
    Employee = pool.get('company.employee')

    data = json.loads(request.data)
    response = None

    if data:
        username = data.get('username', None)
        password = data.get('password', None)
        firstnames = data.get('firstnames', None)
        lastnames = data.get('lastnames', None)
        email = data.get('email', None)

        name = f'{lastnames} {firstnames}'
        password = base64_decode(password) if password else None

        if username:
            users = User.search([('login', '=', username)])
            if not users:
                # User creation
                user_data = {
                    'name': name,
                    'login': username,
                    'email': email,
                }
                if password:
                    user_data.update({'password': password})

                user, = User.create([user_data])

                # Party
                parties = Party.search([
                    ('name', '=', name)
                ])

                # Employee and Company
                if parties:
                    employees = Employee.search([
                        ('party', '=', parties[0].id)
                    ])
                    if employees:
                        employee = employees[0]
                        company = employee.company
                        user.employees = user.employees + (employee,)
                        user.employee = employee
                        user.main_company = company
                        user.company = company
                        user.save()

                # Default groups
                default_group_search = ResGroup.search([
                    ('name', '=', 'Autoservicios')
                ])
                if default_group_search:
                    user_group = ResUserGroup()
                    user_group.group = default_group_search[0].id
                    user_group.user = user.id
                    user_group.save()
                response = Response(str(user.id), 200)
            else:
                User.raise_user_error('username_exists', {'username': username})
    response = response if response else Response(None, 400)
    return response


@app.route('/<database_name>/siim/remove_session/<login>/', methods=['POST'])
@with_pool
@with_transaction()
@siim_application
def remove_session(request, pool, login):
    if login == 'ADMIN':
        login = 'admin'
    User = pool.get('res.user')
    user = User.search([
        ('login', '=', login),
    ])
    transaction = Transaction()
    cursor = transaction.connection.cursor()
    if user:
        user
        cursor.execute('DELETE FROM ir_session '
            'WHERE create_uid = ' + str(user[0].id) +';')
        return [{
            'status': 200,
            'message': 'Ok'
        }]
    return [{
        'status': 400,
        'message': 'No se encontraron sesiones'
    }]
