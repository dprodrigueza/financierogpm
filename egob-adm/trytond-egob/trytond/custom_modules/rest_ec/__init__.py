# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import user
from . import routes
from . import notification

__all__ = ['register', 'routes']


def register():
    Pool.register(
        user.UserApplication,
        user.User,
        notification.EmailLog,
        module='siim_rest', type_='model')
    Pool.register(
        module='siim_rest', type_='wizard')
    Pool.register(
        module='siim_rest', type_='report')
