from trytond import backend
from trytond.model import Workflow, ModelSQL, fields, ModelView
from trytond.pyson import If, Bool, Eval
from trytond.transaction import Transaction

__all__ = ['Insurance', 'InsuranceLine', 'InsuranceClaim']

_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']

STATES = [
    ('draft', 'Borrador'),
    ('open', 'Abierto'),
    ('close', 'Cerrado'),
    ('cancel', 'Cancelado'),
]


class Insurance(Workflow, ModelSQL, ModelView):
    'Seguro'
    __name__ = 'asset.insurance'
    _history = True
    company = fields.Many2One('company.company', 'Compañía', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], states=_STATES, depends=_DEPENDS)
    party = fields.Many2One('party.party', 'Aseguradora', required=True,
            states=_STATES, depends=_DEPENDS)
    reference = fields.Char('Referencia', required=True, states=_STATES,
            depends=_DEPENDS)
    start_date = fields.Date('Fecha Inicio', required=True,
            states=_STATES, depends=_DEPENDS)
    end_date = fields.Date('Fecha Fin', required=True,
            states=_STATES, depends=_DEPENDS)
    note = fields.Text('Notas', required=True, states=_STATES,
            depends=_DEPENDS)
    state = fields.Selection(STATES, 'Estado', readonly=True)
    lines = fields.One2Many('asset.insurance.line', 'insurance', 'Activos',
        required=True, states=_STATES, depends=_DEPENDS)
    claims = fields.One2Many('asset.insurance.claim', 'insurance',
        'Reclamos', states={
            'readonly': Eval('state') != 'open'
        }, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(Insurance, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'close'),
            ('open', 'cancel'),
            ('close', 'cancel'),
        ))
        cls._buttons.update({
            'open': {
                'invisible': Eval('state') != 'draft',
            },
            'close': {
                'invisible': Eval('state') != 'open',
            },
            'cancel': {
                'invisible': Eval('state').in_(['draft', 'cancel']),
            },
        })
        cls._error_messages.update({
            'error_date': ('La fecha de inicio: "%(start_date)s" '
                'no puede ser mayor que la fecha de fin: "%(end_date)s" .'),
                })

    @classmethod
    def __register__(cls, module_name):
        super(Insurance, cls).__register__(module_name)
        TableHandler = backend.get('TableHandler')
        exist = TableHandler.table_exist(cls._table)
        if exist:
            table_h = cls.__table_handler__(module_name)
            table_h.not_null_action('asset', 'remove')

    @classmethod
    def validate(cls, insurances):
        super(Insurance, cls).validate(insurances)
        for insurance in insurances:
            insurance.chek_dates()

    def chek_dates(self):
        if self.start_date > self.end_date:
            self.raise_user_error('error_date', {
                'start_date': self.start_date,
                'end_date': self.end_date,
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, insurances):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, insurances):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, insurances):
        pass

    def get_rec_name(self, name):
        return '%s - %s' % (self.party.name, self.reference)


class InsuranceLine(ModelSQL, ModelView):
    'Activos asegurados'
    __name__ = 'asset.insurance.line'
    _history = True
    _states = {
        'readonly': Eval('insurance_state') != 'draft',
        }
    _depends = ['insurance_state']

    asset = fields.Many2One('asset', 'Activo', states=_states,
        domain=['OR',
            ('id', 'not in', Eval('assets_in_lines')),
            ('id', '=', Eval('asset'))
        ],
        depends=_depends + ['assets_in_lines', 'assets_allowed'], required=True)
    insured_amount = fields.Numeric('Monto Asegurado', digits=(16, 2),
        required=True, states=_states,
        domain=[('insured_amount', '>', 0)],
        depends=_depends)
    insurance = fields.Many2One('asset.insurance', 'Seguro',
        states=_states, depends=_depends)
    insurance_state = fields.Function(fields.Selection(STATES, 'Invoice State'),
        'on_change_with_insurance_state')
    assets_in_lines = fields.Function(fields.Many2Many('asset',
        None, None, 'Activos',
        states={
            'invisible': True
        }), 'on_change_with_assets_in_lines')
    assets_allowed = fields.Function(fields.Many2Many('asset',
        None, None, 'Activos',
        states={
            'invisible': True
        }), 'on_change_with_assets_allowed')

    @classmethod
    def default_insurance_state(cls):
        return 'draft'

    @fields.depends('insurance', '_parent_insurance.state')
    def on_change_with_insurance_state(self, name=None):
        if self.insurance:
            return self.insurance.state
        return 'draft'

    @fields.depends('insurance', '_parent_insurance.lines')
    def on_change_with_assets_in_lines(self, name=None):
        if not self.insurance or not self.insurance.lines:
            return
        assets = []
        for line in self.insurance.lines:
            if line.asset:
                assets.append(line.asset.id)
        return assets

    @fields.depends('insurance')
    def on_change_with_assets_allowed(self, name=None):
        if not self.insurance or not self.insurance.lines:
            return
        assets = []
        for line in self.insurance.lines:
            if line.asset:
                assets.append(line.asset.id)
        return assets


class InsuranceClaim(Workflow, ModelSQL, ModelView):
    'Reclamo de seguros'
    __name__ = 'asset.insurance.claim'
    _history = True
    _states = {
        'readonly': (Eval('state') != 'draft')
        }
    _depends = ['state']

    asset = fields.Many2One('asset', 'Activo', required=True,
        states={'readonly': _states['readonly'] | ~Bool(Eval('insurance'))},
        domain=[
            ('id', 'in', Eval('assets_allowed'))
        ],
        depends=_depends + ['assets_allowed'])
    reference = fields.Char('Referencia', required=True)
    claim_date = fields.Date('Fecha de Reclamo', required=True,
            states=_states, depends=_depends)
    claim = fields.Text('Reclamo', required=True, states=_states,
        depends=_depends)
    solution_date = fields.Date('Fecha de Solución',
        states={
            'readonly': Eval('state') != 'open'
        }, depends=_depends)
    solution = fields.Text('Solucion',
        states={
            'readonly': Eval('state') != 'open'
        }, depends=_depends)
    state = fields.Selection(STATES, 'Estado', readonly=True)
    insurance = fields.Many2One('asset.insurance', 'Seguro', required=True,
        states={
            'readonly': _states['readonly'] | Bool(Eval('asset'))
        },
        domain=['OR',
            [('state', '=', 'open')],
            ('id', '=', Eval('insurance', -1)),
        ],
        depends=_depends + ['asset'])
    insurance_state = fields.Function(fields.Selection(STATES, 'Seguro Estado'),
        'on_change_with_insurance_state')

    assets_allowed = fields.Function(fields.Many2Many('asset',
        None, None, 'Activos',
        states={
            'invisible': True
        }), 'on_change_with_assets_allowed')

    @classmethod
    def __setup__(cls):
        super(InsuranceClaim, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'close'),
            ('open', 'cancel'),
            ('close', 'cancel'),
        ))
        cls._buttons.update({
            'open': {
                'invisible': Eval('state') != 'draft',
            },
            'close': {
                'readonly': (~Bool(Eval('solution_date')) |
                             ~Bool(Eval('solution'))),
                'invisible': Eval('state') != 'open',
            },
            'cancel': {
                'invisible': Eval('state').in_(['draft', 'cancel']),
            },
        })
        cls._error_messages.update({
            'error_date': ('La fecha de reclamo: "%(claim_date)s" no puede ser '
                'mayor que la fecha de solución: "%(solution_date)s" .'),
            'error_claim_date': ('La fecha de reclamo: "%(claim_date)s" '
                'no esta en el rango de fechas del seguro.'),
            'claim_delete': ('No se puede eliminar el reclamo: "%(claim_ref)s" '
                 'por que no esta en borrador.'),
                })

    @classmethod
    def validate(cls, claims):
        super(InsuranceClaim, cls).validate(claims)
        for claim in claims:
            claim.chek_dates()

    def chek_dates(self):
        if self.solution_date and self.claim_date > self.solution_date:
            self.raise_user_error('error_date', {
                'claim_date': self.claim_date,
                'solution_date': self.solution_date,
            })
        if (self.claim_date < self.insurance.start_date or
                self.claim_date > self.insurance.end_date):
            self.raise_user_error('error_claim_date', {
                'claim_date': self.claim_date,
            })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def default_insurance_state(cls):
        return 'draft'

    @fields.depends('insurance', '_parent_insurance.state')
    def on_change_with_insurance_state(self, name=None):
        if self.insurance:
            return self.insurance.state
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, claims):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, claims):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, claims):
        pass

    @fields.depends('insurance')
    def on_change_with_assets_allowed(self, name=None):
        if not self.insurance or not self.insurance.lines:
            return
        assets = []
        for line in self.insurance.lines:
            assets.append(line.asset.id)
        return assets

    @classmethod
    def delete(cls, claims):
        for claim in claims:
            if claim.state != 'draft':
                cls.raise_user_error('claim_delete', {
                    'claim_ref': claim.reference,
                })
        super(InsuranceClaim, cls).delete(claims)
