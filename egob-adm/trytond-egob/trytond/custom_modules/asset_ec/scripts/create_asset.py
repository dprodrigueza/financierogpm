from proteus import config, Model
from datetime import datetime
import xlrd
import pdb
from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

def create_assets(host, port, database, username, password, filename):
    pdb.set_trace()
    conf = config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        %(username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    print((datetime.now()))

    total = sheet.nrows
    cont = 0

    Asset = Model.get('asset')
    AssetOwner = Model.get('asset.owner')
    Employee = Model.get('company.employee')
    
    for row in range(sheet.nrows)[1:]:
        cont += 1
        print(('cont', cont))
        type_ = sheet.cell(row, 0).value
        input_date = sheet.cell(row, 1).value
        input_date = xlrd.xldate.xldate_as_datetime(input_date,
            wb.datemode).date()
        name = sheet.cell(row, 2).value
        purchase_date = sheet.cell(row, 3).value
        purchase_date = xlrd.xldate.xldate_as_datetime(purchase_date,
            wb.datemode).date()
        asset_state = sheet.cell(row, 4).value
        serial = str(sheet.cell(row, 5).value)
        brand = str(sheet.cell(row, 6).value)
        model = str(sheet.cell(row, 7).value)
        life_time = sheet.cell(row, 8).value and int(sheet.cell(row, 8).value) or None
        invoice_number = sheet.cell(row, 9).value
        asset_date = sheet.cell(row, 10).value
        asset_date = xlrd.xldate.xldate_as_datetime(asset_date,
            wb.datemode).date()
        employee_name = sheet.cell(row, 11).value
        
        #try:
        asset = Asset()
        asset.name = name
        asset.type = type_
        asset.input_date = input_date
        asset.purchase_date = purchase_date
        asset.asset_state = asset_state
        asset.serial = serial
        asset.brand = brand
        asset.model = model
        asset.life_time = life_time
        asset.invoice_number = str(invoice_number)
        asset.save()
        #Busca el owner para asset
        employee = Employee.search([('party.name', '=', employee_name)])
        if employee:
            owner = AssetOwner()
            owner.asset = asset
            owner.employee = employee[0]
            owner.from_date = asset_date
            owner.save()
        else:
            print(("No encuentra empleado para", name, asset.code))
        #except Exception as e:
        #    print('Incorrecto --->', cont, name, str(e))
    print((datetime.now()))

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host',
        help='host', default='localhost')
    parser.add_argument('--port', dest='port',
        help='port', default='8000')
    parser.add_argument('--database', dest='database',
        help='database', required=True)
    parser.add_argument('--user', dest='user',
        help='user', required=True)
    parser.add_argument('--password', dest='password',
        help='password', required=True)
    parser.add_argument('--filename', dest='filename',
        help='filename', required=True)
    options = parser.parse_args()
    create_assets(options.host, options.port, options.database,
        options.user, options.password, options.filename)
    
    
    
