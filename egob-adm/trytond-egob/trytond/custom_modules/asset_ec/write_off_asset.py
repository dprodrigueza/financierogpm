from trytond.pool import Pool
from trytond.model import Workflow, ModelSQL, ModelView, fields
from trytond.pyson import Eval

__all__ = ['WriteOffAsset', 'Assets']

_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


class WriteOffAsset(Workflow, ModelSQL, ModelView):
    'WriteOffAsset'
    __name__ = 'asset.write_off'

    code = fields.Char("Code", size=None, readonly=True, select=True)
    created_by = fields.Many2One('company.employee', 'Created by',
        required=True, states=_STATES, depends=_DEPENDS)
    date = fields.Date('Date', required=True, states=_STATES, depends=_DEPENDS)
    authorized_by = fields.Many2One('company.employee', 'Authorized by',
        required=True, states=_STATES, depends=_DEPENDS)
    custodian = fields.Many2One('company.employee', 'Custodian',
        required=True, states=_STATES, depends=_DEPENDS)
    cause = fields.Many2One('asset.configuration.cause', 'Cause', required=True,
        states=_STATES, depends=_DEPENDS)
    justification = fields.Text('Justification', required=True, states=_STATES,
        depends=_DEPENDS)
    assets = fields.Many2Many('asset.write_off.assets', 'write_off', 'asset',
        'Write Off Assets', required=True,
        domain=[
            ('state', '=', 'open'),
        ])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('canceled', 'Canceled'),
        ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(WriteOffAsset, cls).__setup__()

        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'done'),
            ('confirmed', 'draft'),
            ('done', 'draft'),
            ('draft', 'canceled'),
            ('canceled', 'draft'),
        ))

        cls._buttons.update({
            'confirm': {
                'invisible': Eval('state') != 'draft',
            },
            'draft': {
                'invisible': Eval('state') == 'draft',
            },
            'done': {
                'invisible': Eval('state') != 'confirmed',
            },
            'cancel': {
                'invisible': Eval('state') == 'done',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, writes):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')

        config = Config(1)
        for writeoff in writes:
            if writeoff.code:
                continue
            writeoff.code = Sequence.get_id(config.write_cause_sequence.id)
        cls.save(writes)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, writes):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, writes):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, writes):
        pool = Pool()
        Asset = pool.get('asset')
        assets = []
        for writeoff in writes:
            assets += writeoff.assets
        Asset.close(assets)


class Assets(ModelView, ModelSQL):
    'Assets'
    __name__ = 'asset.write_off.assets'
    _table = 'asset_write_off_assets'

    asset = fields.Many2One('asset', 'Asset', ondelete='CASCADE', select=True)
    write_off = fields.Many2One('asset.write_off', 'Write Off',
        ondelete='CASCADE', select=True)
