from trytond.model import ModelSQL, fields, ModelView
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.modules.company.model import CompanyValueMixin

__all__ = ['Configuration', 'AssetConfigurationSequence']


class Configuration(metaclass=PoolMeta):
    __name__ = 'asset.configuration'

    asset_manager = fields.Many2One('res.user', 'Asset manager', required=True)
    digits = fields.Integer('Digitos')
    value_residual = fields.Numeric('Valor residual %',digits=(16, 2))

    write_cause_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Write Off Cause Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'write_off'),
        ]))
    asset_transfer_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Asset Transfer Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'asset_transfer'),
        ]))
    asset_incident_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Secuencia de incidentes activos", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'asset_incident'),
        ]))
    asset_derecognition_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Asset derecognition Sequence", required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'asset_derecognition'),
        ]))
    asset_inventory_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Secuencia de Inventario de Activos",
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'asset_inventory'),
        ]))

    patrimonial_account = fields.Many2One('account.account', 'Cuenta baja activo',
        required=True,
         domain=['AND', [('kind', '!=', 'view')],
                 ['OR',
                  [('code', 'like', '639.88.%')]
         ]])
    obsolescence_account = fields.Many2One('account.account', 'Cuenta deterioro u obsolecencia',
        required=True,
         domain=['AND', [('kind', '!=', 'view')],
                 ['OR',
                  [('code', 'like', '638.%')]
         ]])

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if (field == 'write_cause_sequence' or
                field == 'asset_transfer_sequence' or
                field == 'asset_derecognition_sequence' or
                field == 'asset_inventory_sequence' or
                field == 'asset_incident_sequence'):
            return pool.get('asset.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_asset_transfer_sequence(cls, **pattern):
        return cls.multivalue_model(
            'asset_transfer_sequence').default_asset_transfer_sequence()

    @classmethod
    def default_asset_incident_sequence(cls, **pattern):
        return cls.multivalue_model(
            'asset_incident_sequence').default_asset_incident_sequence()

    @classmethod
    def default_asset_derecognition_sequence(cls, **pattern):
        return cls.multivalue_model(
            'asset_derecognition_sequence').default_asset_derecognition_sequence()  # noqa

    @classmethod
    def default_write_cause_sequence(cls, **pattern):
        return cls.multivalue_model(
            'write_cause_sequence').default_write_cause_sequence()

    @classmethod
    def default_asset_inventory_sequence(cls, **pattern):
        return cls.multivalue_model(
            'asset_inventory_sequence').default_asset_inventory_sequence()

    @staticmethod
    def default_digits():
        return 5

    @staticmethod
    def default_value_residual():
        return 0.1


class AssetConfigurationSequence(ModelSQL, CompanyValueMixin):
    'Asset Configuration Sequence'
    __name__ = 'asset.configuration.sequence'

    write_cause_sequence = fields.Many2One(
        'ir.sequence', "Write Off Cause Sequence",
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'write_off'),
            ],
        depends=['company'])
    asset_transfer_sequence = fields.Many2One(
        'ir.sequence', "Asset Transfer Sequence",
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'asset_transfer'),
            ],
        depends=['company'])
    asset_incident_sequence = fields.Many2One(
        'ir.sequence', "Secuencia de incidentes activos",
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'asset_incident'),
        ],
        depends=['company'])

    asset_derecognition_sequence = fields.Many2One(
        'ir.sequence', "Asset Derecognition Sequence",
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'asset_derecognition'),
        ],
        depends=['company'])
    asset_inventory_sequence = fields.Many2One(
        'ir.sequence', "Secuencia de Inventario de Activos",
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'asset_inventory'),
        ],
        depends=['company'])

    @classmethod
    def default_asset_transfer_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('asset_ec', 'sequence_asset_transfer')
        except KeyError:
            return None

    @classmethod
    def default_asset_incident_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('asset_ec', 'sequence_asset_incident')
        except KeyError:
            return None

    @classmethod
    def default_asset_derecognition_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('asset_ec', 'sequence_asset_derecognition')
        except KeyError:
            return None

    @classmethod
    def default_write_cause_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('asset_ec', 'sequence_write_off_count')
        except KeyError:
            return None

    @classmethod
    def default_asset_inventory_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('asset_ec', 'sequence_asset_inventory')
        except KeyError:
            return None


class ConfigurationCause(ModelView, ModelSQL):
    'Asset Write Off Cause Config'
    __name__ = 'asset.configuration.cause'

    created_by = fields.Many2One(
        'company.employee', 'Created by', required=True)
    cause = fields.Text("Cause", required=True)



class WarrantyEndSendEmail(ModelSQL, ModelView):
    'Warranty End Send Email'
    __name__ = 'assent.warranty.end.send.email'

    email = fields.Many2One('company.employee', 'Empleado')
