from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['ShipmentInternal']


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    asset_maintenance = fields.Many2One(
        'asset.maintenance', 'Mantenimiento de activos')
