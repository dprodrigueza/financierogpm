from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta


__all__ = [
    'Employee',
]


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'


    location = fields.Many2One(
        'company.location', 'Ubicacion de la empresa',
    )