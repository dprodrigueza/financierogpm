from trytond.model import Workflow, ModelSQL, ModelView, fields
from trytond.pyson import If, Eval
from trytond.transaction import Transaction

__all__ = ['Maintenance']

_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


class Maintenance(Workflow, ModelSQL, ModelView):
    'Maintenance'
    __name__ = 'asset.maintenance'

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id',
            If(Eval('context', {}).contains('company'),
                '=', '!='),
            Eval('context', {}).get('company', -1)),
        ], states=_STATES, depends=_DEPENDS)
    asset = fields.Many2One('asset', 'Asset', required=True,
        domain=[
            ('company', '=', Eval('company'))
        ], states=_STATES, depends=_DEPENDS + ['company'])
    responsable = fields.Many2One('company.employee', 'Responsable',
        domain=[
            ('company', '=', Eval('company'))
        ], states=_STATES,
        depends=_DEPENDS + ['company'], required=True)
    request_date = fields.Date(
        'Request Date', required=True, states=_STATES, depends=_DEPENDS)
    implementation_date = fields.Date(
        'Implementation Date', states=_STATES, depends=_DEPENDS)
    next_maintenance_date = fields.Date(
        'Next maintenance Date', states=_STATES, depends=_DEPENDS)
    description = fields.Text(
        'Description', required=True, states=_STATES, depends=_DEPENDS)
    third_party_maintenance = fields.Many2One('account.invoice', 'Third Party',
        domain=[
            ('company', '=',
            Eval('company'))
        ], states=_STATES, depends=_DEPENDS + ['company'])
    type = fields.Selection([
        ('prevalent', 'Preventivo'),
        ('corrective', 'Correctivo'),
        ], 'Type', required=True, states=_STATES, depends=_DEPENDS)
    type_translated = type.translated('type')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
    ], 'State', readonly=True)
    state_translated = state.translated('state')
    product_list = fields.One2Many('stock.shipment.internal', 'asset_maintenance',
        'Productos ocupados en mantenimiento',
        domain=[
            ('company', '=', Eval('company'))
        ], states=_STATES,
        depends=_DEPENDS + ['company'])

    @classmethod
    def __setup__(cls):
        super(Maintenance, cls).__setup__()

        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
        ))

        cls._buttons.update({
            'confirmed': {
                'invisible': (Eval('state') != 'draft'),
                'icon': 'tryton-ok',
                'depends': ['state'],
            },
            'draft': {
                'invisible': (Eval('state') != 'confirmed'),
                'icon': 'tryton-undo',
                'depends': ['state']
            },
        })
        cls._error_messages.update({
            'error_date': ('Fecha de realización: "%(implementation_date)s" '
                'debe ser mayor a la fecha de solicitud: "%(request_date)s"'),
            'error_maintenance_date': ('Fecha solicitud: "%(request_date)s" '
                ' Fecha realización: "%(implementation_date)s" no puede ser '
                ' mayor a la fecha de siguiente revisión : "%(next_date)s"'),
        })

    @classmethod
    def validate(cls, maintenances):
        super(Maintenance, cls).validate(maintenances)
        for form in maintenances:
            form.chek_dates()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, maintenance):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, maintenance):
        pass

    def chek_dates(self):
        if self.implementation_date:
            if self.request_date > self.implementation_date:
                self.raise_user_error('error_date', {
                    'request_date': self.request_date,
                    'implementation_date': self.implementation_date,
                })
            if self.next_maintenance_date:
                if (self.request_date > self.next_maintenance_date or
                        self.implementation_date > self.next_maintenance_date):
                    self.raise_user_error('error_maintenance_date', {
                        'request_date': self.request_date,
                        'implementation_date': self.implementation_date,
                        'next_date': self.next_maintenance_date,
                    })
