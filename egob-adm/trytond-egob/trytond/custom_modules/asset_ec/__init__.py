# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import asset
from . import configuration
from . import account_asset
from . import maintenance
from . import write_off_asset
from . import report
from . import asset_insurance
from . import stock
from . import warranty
from . import company


def register():
    Pool.register(
        asset.AssetCategory,
        asset.Asset,
        asset.AssetLine,
        asset.AssetAdditionalCost,
        asset.AssetOwner,
        asset.AssetTransfer,
        asset.AssetTransferLine,
        asset.CompanyLocation,
        asset.Company,
        asset.AssetLocation,
        asset.AssetWork,
        asset.AssetIncident,
        asset.AssetIncidentLine,
        asset.AssetDerecognition,
        asset.AssetDerecognitionLine,
        asset.Move,
        asset.AssetSBYECode,
        asset.AssetDerecognitionRequirements,
        asset.AssetTransferSelectStart,
        asset.CreateAssetCertificate,
        asset.CreateAssetCertifcateStart,
        asset.AssetInventory,
        asset.AssetInventoryLine,
        asset.AssetCreateDeprecationStart,
        company.Employee,
        configuration.AssetConfigurationSequence,
        configuration.Configuration,
        configuration.ConfigurationCause,
        configuration.WarrantyEndSendEmail,
        account_asset.Asset,
        account_asset.CreateMovesStart,
        account_asset.AssetUpdateValuesHeader,
        account_asset.AssetUpdateValuesHeaderStart,
        maintenance.Maintenance,
        write_off_asset.WriteOffAsset,
        write_off_asset.Assets,
        asset_insurance.Insurance,
        asset_insurance.InsuranceLine,
        asset_insurance.InsuranceClaim,
        stock.ShipmentInternal,
        warranty.WorkWarranty,
        warranty.WorkWarrantyAsset,
        warranty.Asset,

        module='asset_ec', type_='model')
    Pool.register(
        account_asset.CreateMoves,
        account_asset.UpdateAsset,
        account_asset.AssetUpateValuesHeaderWizard,
        asset.AssetTransferSelect,
        asset.CreateMoves,
        asset.AssetCreateCertificateWizard,
        asset.AssetCreateDeprecation,
        module='asset_ec', type_='wizard')
    Pool.register(
        report.AssetReport,
        report.AssetLabelReport,
        report.AssetListReport,
        report.AssetTransferReport,
        report.AssetIncidentReport,
        report.AssetDerecognition,
        report.AssetOwnerReport,
        report.AssetDataSheetReport,
        report.AssetOwnerDetailReport,
        report.AssetInsuranceDetailReport,
        report.AssetDetailByOwner,
        report.AssetDepartment,
        report.IncomeAssignmentsDerecognition,
        report.AssetAccountReport,
        report.ExpirationInsurancePoliciesReport,
        report.AssetByLocationReport,
        report.AssetCertificate,
        report.AssetReportByCostCenter,
        report.AssetSbye,
        report.AssetCodeQRReport,
        report.AssetCustodians,
        report.AssetInventory,
        module='asset_ec', type_='report')
