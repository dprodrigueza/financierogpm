from decimal import Decimal
import datetime
import io
from collections import defaultdict
from trytond.wizard import Wizard, StateView, StateTransition, StateReport,\
    Button
from sql.aggregate import Max
import treepoem
import json

from sql import Column, Null, Literal
from trytond.pool import Pool, PoolMeta
from trytond.model import Workflow, ModelSQL, ModelView, fields, tree, Unique
from trytond.pyson import If, Bool, Eval, Id
from trytond.transaction import Transaction
from trytond.modules.asset.asset import AssetAssignmentMixin
from trytond.tools import reduce_ids, grouped_slice
from trytond.wizard import Wizard, StateView, Button, StateTransition
import barcode
from barcode.writer import ImageWriter

__all__ = ['AssetCategory', 'Asset', 'AssetLine', 'AssetAdditionalCost',
    'AssetOwner', 'AssetTransfer', 'AssetTransferLine', 'CompanyLocation',
    'Company', 'AssetLocation', 'AssetWork', 'AssetIncident',
    'AssetIncidentLine', 'AssetDerecognition', 'AssetDerecognitionLine',
    'AssetDerecognitionRequirements', 'AssetSBYECode', 'AssetInventory',
    'AssetInventoryLine']

_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


class AssetCategory(ModelSQL, ModelView):
    'Asset Category'
    __name__ = 'asset.category'
    name = fields.Char('Name', required=True)
    company = fields.Many2One('company.company', 'Company',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], readonly=True)
    sequence = fields.Many2One('ir.sequence', "Sequence",
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'asset'),
            ])
    life_time_reference = fields.Integer('Referencia de V. Útil')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AssetDerecognition(Workflow, ModelSQL, ModelView):
    'Asset Derecognition'
    __name__ = 'asset.derecognition'

    name = fields.Text('Razón', required=False,
        states={
            'readonly': Bool(Eval('state') != 'draft')
        })
    derecognition_date = fields.Date('Fecha de baja', 
        domain=[('derecognition_date', '<=', datetime.date.today() + datetime.timedelta(days=10))],
        required=True,
        states=_STATES, depends=_DEPENDS + [
            'derecognition_date'])
    state = fields.Selection([
        ('draft', 'Borrador '),
        ('validate', 'Inspeccion'),
        ('cancel', 'Cancelar'),
        ('confirmed', 'Confirmar'),
        ('done', 'Realizado')
        ], 'Estado', readonly=True)
    state_translated = state.translated('state')
    employee = fields.Many2One('company.employee', 'Empleado', required=True,
        states=_STATES)
    company = fields.Many2One('company.company', 'Compañía', states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], required=True, readonly=True)
    number = fields.Char('Numero', readonly=True)
    derecognition_line = fields.One2Many('asset.derecognition.line',
        'derecognition', 'Activo', required=True,
        states={
            'readonly': Bool(Eval('state') != 'draft')
        })
    derecognition_requirements = fields.One2Many(
        'asset.derecognition.requirements', 'derecognition', 'Requerimientos',
        states={
            'readonly': Bool(Eval('state').in_(['draft', 'confirmed', 'cancel',
            'done']))
        })
    destruction_location = fields.Char('Lugar de destruccion',
        states={
            'invisible': True
        })
    destruction_date = fields.Date('Fecha de Destruccion',
        states={
            'invisible': True
        })
    journal = fields.Many2One('account.journal',
        'Libro diario', states={
            'readonly': Eval('state') != 'draft', 
            'required': True
        }, depends=_DEPENDS)
    period = fields.Many2One('account.period', 'Periodo',
        states={'readonly': True})
    move = fields.Many2One('account.move','Asiento relacionado',
        states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(AssetDerecognition, cls).__setup__()
        cls._order = [
            ('id', 'DESC'),
            ]

        cls._transitions |= set((
                ('draft', 'validate'),
                ('validate', 'confirmed'),
                ('validate', 'cancel'),
                ('confirmed', 'done'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state').in_(
                        ['draft', 'cancel', 'confirmed', 'done', 'validate']),
                    'depends': ['state']
                    },
                'validate': {
                    'invisible': Eval('state').in_(
                        ['cancel', 'confirmed', 'done', 'validate']),
                    'depends': ['state']
                    },
                'confirmed': {
                    'invisible': Eval('state').in_(
                        ['draft', 'done', 'cancel', 'confirmed']),
                    'depends': ['state']
                    },
                'cancel': {
                    'invisible': Eval('state').in_(
                        ['draft', 'done', 'confirmed', 'cancel']),
                    'depends': ['state']
                    },
                'done': {
                    'invisible': Eval('state').in_(
                        ['validate', 'cancel', 'draft', 'done']),
                    'depends': ['state']
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        return Period.search([
            ('start_date','<=',datetime.date.today()),
            ('end_date','>=',datetime.date.today())])[0].id

    @classmethod
    def trigger_create(cls, derecognitions):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, derecognitions):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('validate')
    def validate(cls, derecognitions):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, derecognitions):
        for derecognition in derecognitions:
            if not derecognition.derecognition_requirements:
                cls.raise_user_error('Los requerimientos son necesarios!!')

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, derecognitions):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')

        config = Config(1)
        for derecognition in derecognitions:
            if derecognition.number:
                continue
            derecognition.number = \
                Sequence.get_id(config.asset_derecognition_sequence.id)
        cls.save(derecognitions)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, derecognitions):
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Shipment = pool.get('stock.shipment.internal')
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')

        config = Config(1)

        

        for derecognition in derecognitions:
            if derecognition.number:
                continue
            derecognition.number = \
                Sequence.get_id(config.asset_derecognition_sequence.id)

        for derecognition in derecognitions:
            move = Move()
            if derecognition.move:
                move = derecognition.move
                MoveLine.delete(move.lines)
            move.journal = derecognition.journal
            move.period = derecognition.period
            move.type = 'adjustment'
            move.date = datetime.date.today()
            move.description = 'Baja/Donación activo #' + derecognition.number + \
                ', por el motivo: ' + derecognition.name
            move.origin = 'asset.derecognition,'+str(derecognition.id)
            move.lines = []
            # amount = company.currency.round(Decimal(str(rv[2])))
            # breakpoint()
            for line in derecognition.derecognition_line:
                if line.type_ in ['donation', 'transfer']:
                    # 1000
                    line_debit = MoveLine()
                    line_debit.account = config.patrimonial_account
                    line_debit.debit = Decimal(
                        line.asset.actual_value).quantize(Decimal('.01'))
                    line_debit.adjustment  = True
                    move.lines += (line_debit,)
                    # 9000
                    line_debit = MoveLine()
                    line_debit.account = line.asset.credit_account
                    line_debit.debit = Decimal(
                        line.asset.total_value_depreciated).quantize(Decimal('.01'))
                    line_debit.adjustment  = True
                    move.lines += (line_debit,)
                    # 10000
                    line_credit = MoveLine()
                    line_credit.account = line.asset.asset_account
                    line_credit.credit = Decimal(
                        line.asset.purchase_option_value).quantize(Decimal('.01'))
                    line_credit.adjustment  = True
                    move.lines += (line_credit,)
                elif line.type_ in ['scrap', 'destruction', 'derecognition']:
                    # 1000
                    line_debit = MoveLine()
                    line_debit.account = config.obsolescence_account
                    line_debit.debit = Decimal(
                        line.asset.actual_value).quantize(Decimal('.01'))
                    line_debit.adjustment  = True
                    move.lines += (line_debit,)
                    # 9000
                    line_debit = MoveLine()
                    line_debit.account = line.asset.credit_account
                    line_debit.debit = Decimal(
                        line.asset.total_value_depreciated).quantize(Decimal('.01'))
                    line_debit.adjustment  = True
                    move.lines += (line_debit,)
                    # 10000
                    line_credit = MoveLine()
                    line_credit.account = line.asset.asset_account
                    line_credit.credit = Decimal(
                        line.asset.purchase_option_value).quantize(Decimal('.01'))
                    line_credit.adjustment  = True
                    move.lines += (line_credit,)

            Move.save([move])
            derecognition.move = move

        for derecognition in derecognitions:
            for dline in derecognition.derecognition_line:
                dline.asset.state = 'closed'
                dline.asset.derecognition = derecognition.id
                dline.asset.save()

        cls.save(derecognitions)


class AssetDerecognitionLine(ModelSQL, ModelView):
    'Asset Derecognition Line'
    __name__ = 'asset.derecognition.line'

    derecognition = fields.Many2One('asset.derecognition', 'Activo',
        required=True, ondelete='CASCADE', states=_STATES, depends=_DEPENDS)
    asset = fields.Many2One('asset', 'Activo', required=True,
        domain=[('possesion_type', '=', "own"),
                ('state', '=', "open"),
                ],
        states=_STATES, depends=_DEPENDS)
    asset_custodian = fields.Function(fields.Many2One('company.employee',
        'Custodio'), 'on_change_with_asset_custodian')
    custodian = fields.Many2One('company.employee', 'Custodio',
        domain=[If(Eval('asset_custodian'),
            ('id', '=', Eval('asset_custodian')),
            ())
        ], required=True, states=_STATES, depends=_DEPENDS + [
            'asset_custodian'])
    name = fields.Text('Razón', required=True, states=_STATES,
        depends=_DEPENDS)
    type_ = fields.Selection(
        [
            # ('auction', 'Remate'),
            # ('sale', 'Venta'),
            # ('barter', 'Permuta'),
            ('donation', 'Donación'),
            ('transfer', 'Transferencia gratuita'),
            ('scrap', 'Chatarrización'),
            # ('recyclin', 'Reciclaje'),
            ('destruction', 'Destrucción'),
            ('derecognition', 'Baja'),
        ], 'Tipo de baja', sort=False,
        states={
            'required': True,
            'readonly': _STATES['readonly'],
        }, depends=_DEPENDS)
    type_translated = type_.translated('type_')
    replacement = fields.Boolean('Sujeto o Reposición', states=_STATES,
        depends=_DEPENDS)
    state = fields.Selection([
        ('draft', 'Borrador '),
        ('done', 'Realizado'),
        ], 'Estado', readonly=True)
    destruction_location = fields.Char('Lugar de destruccion',
        states={
            'required': Bool(Eval('type_').in_(
                ['scrap', 'destruction'])),
            'invisible': ~Bool(Eval('type_').in_(
                ['scrap', 'destruction'])),
            'readonly': _STATES['readonly'],
        })
    destruction_date = fields.Date('Fecha de Destruccion',
        states={
            'required': Bool(Eval('type_').in_(
                ['scrap', 'destruction'])),
            'invisible': ~Bool(Eval('type_').in_(
                ['scrap', 'destruction'])),
            'readonly': _STATES['readonly'],
        })

    @staticmethod
    def default_replacement():
        return False

    def default_state():
        return 'draft'

    @classmethod
    def trigger_create(cls, derecognitionLines):
        for line in derecognitionLines:
            line.state = 'done'
        cls.save(derecognitionLines)

    @fields.depends('asset')
    def on_change_with_asset_custodian(self, name=None):
        if self.asset:
            if self.asset.employee:
                return self.asset.employee.id
        return None

    @fields.depends(methods=['on_change_with_asset_custodian'])
    def on_change_with_custodian(self, name=None):
        return self.on_change_with_asset_custodian()


class AssetDerecognitionRequirements(ModelSQL, ModelView):
    'Asset Derecognition Requirements'
    __name__ = 'asset.derecognition.requirements'

    derecognition = fields.Many2One('asset.derecognition', 'Activo',
        required=True, ondelete='CASCADE')
    employee = fields.Many2One('company.employee', 'Inspector', states=_STATES)
    name = fields.Text('Observaciones', states=_STATES)
    state = fields.Selection([
        ('draft', 'Borrador '),
        ('done', 'Realizado'),
        ], 'Estado', readonly=True)

    def default_state():
        return 'draft'

    @classmethod
    def trigger_create(cls, requirements):
        for requirement in requirements:
            requirement.state = 'done'
        cls.save(requirements)

class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['asset.derecognition']

class Asset(tree(), Workflow, metaclass=PoolMeta):
    __name__ = 'asset'

    kind = fields.Selection(
        [
            ('main', 'Principal'),
            ('component', 'Componente'),
        ], 'Clase', required=True, states=_STATES, depends=_DEPENDS,
        help="En un activo de tipo componente no se genera código")
    category = fields.Many2One('asset.category', 'Category', required=True,
        states=_STATES, depends=_DEPENDS)
    parent = fields.Many2One('asset', 'Activo Principal', select=True,
        left="left", right="right", ondelete="RESTRICT",
        states={
            'readonly': True,
            'invisible': ~Bool(Eval('parent')),
        }, depends=_DEPENDS)
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    childs = fields.One2Many('asset', 'parent', 'Componentes',
        domain=[
            ('company', '=', Eval('company'))
        ], states={
            'invisible': (Eval('kind') == 'component'),
            'readonly': Eval('state') != 'draft'
        }, depends=_DEPENDS + ['company'])
    short_name = fields.Function(fields.Char('Name'), 'get_short_name')
    serial = fields.Char('Número de Serie', required=False,
        states=_STATES, depends=_DEPENDS)
    brand = fields.Char('Marca', required=False,
        states=_STATES, depends=_DEPENDS)
    model = fields.Char('Modelo', required=False,
        states=_STATES, depends=_DEPENDS)
    sbye = fields.Char('SByE Cod.', states=_STATES, depends=_DEPENDS)
    sbye_code = fields.Many2One('account.asset.sbye.code', 'Código SByE',
        states=_STATES, depends=_DEPENDS)
    life_time = fields.Integer('Vida útil', states=_STATES, depends=_DEPENDS,
        required=False)
    purchase_date = fields.Date('Fecha de compra', required=True,
        states=_STATES, depends=_DEPENDS,)
    input_date = fields.Date('Fecha de ingreso', required=True,
        states=_STATES, depends=_DEPENDS)
    invoice_number = fields.Char('Factura',
        states=_STATES, depends=_DEPENDS)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('open', 'Abierto'),
        ('closed', 'Dado de baja'),
        ('cancel', 'Anulado'),
        ], 'State', readonly=True)
    state_translated = state.translated('state')
    asset_state = fields.Selection([
        ('good', 'Bueno'),
        ('regular', 'Regular'),
        ('bad', 'Malo'),
        ('unspecified', 'Sin especificar'),
        ], 'Estado del Activo', required=True, states=_STATES, depends=_DEPENDS)
    asset_state_translated = state.translated('asset_state')
    asset_type = fields.Selection([
        ('artistic', 'Artísticos'),
        ('biological', 'Biológicos'),
        ('property', 'Edificios, Locales y Residencias'),
        ('book', 'Libros y colecciones'),
        ('vehicle', 'Vehículos'),
        ('movable_property', 'Bienes Muebles'),
        ('furniture', 'Mobiliario'),
        ('machinery', 'Maquinaria y Equipos'),
        ('system', 'Equipos y Sistemas Informáticos'),
        ('tool', 'Herramientas'),
        ('spare_parts', 'Partes y Repuestos'),
        ('ground', 'Terrenos'),
        ('unspecified', 'No especificado'),
        ], 'Tipo de Activo', required=True, states=_STATES, depends=_DEPENDS)
    asset_type_translated = asset_type.translated('asset_type')
    employee = fields.Function(fields.Many2One('company.employee', 'Employee'),
        'get_employee', searcher='search_employee')
    party = fields.Function(fields.Many2One('party.party', 'Tercero'),
        'get_party', searcher='search_party')
    employee_department = fields.Function(fields.Many2One('company.department',
        'Departamento'), 'get_department', searcher='search_department')
    possesion_type = fields.Selection([
        ('own', 'Propio'),
        ('leasing', 'Arrendamiento'),
        ('pignoracion', 'Pignoracion'),
        ('comodato', 'Comodato')
        ], 'Tipo de poseción', required=True, states=_STATES, depends=_DEPENDS)
    possesion_type_translated = possesion_type.translated('possesion_type')
    account_asset = fields.Many2One('account.asset', 'Depreciación',
        states={
            'readonly': (Eval('possesion_type', '') != 'own')
        },
        # domain=[
        #     ('product', '=', Eval('product')),
        #     ],
        depends=['product', 'possesion_type'])
    supplier = fields.Many2One('party.party', 'Proveedor',
        states={
            'invisible': (Eval('possesion_type', '') != 'leasing')
        },
        depends=['possesion_type'])
    contract = fields.Char('Contrato',
        states={
            'invisible': (Eval('possesion_type', '') != 'leasing')
        }, depends=['possesion_type'])
    purchase_option_value = fields.Numeric('Valor de compra',
       states={
           'readonly': _STATES['readonly'],
           'invisible': (Eval('possesion_type', '') != 'own')
       }, depends=_DEPENDS + ['possesion_type'],
       #digits=(16, Eval('currency_digits', 2)),
       required=True,)
    locations = fields.One2Many('asset.location', 'asset', 'Ubicaciones')
    works = fields.One2Many('asset.work', 'asset', 'Trabajos')
    maintenances = fields.One2Many('asset.maintenance', 'asset',
        'Mantenimientos',
        domain=[
            ('company', '=', Eval('company'))
        ],
        states={
            'readonly': ~Eval('state').in_(['draft', 'open'])
        }, depends=['state', 'company'])
    code_label = fields.Binary(
        'Codigo de etiqueta', file_id='code_label_id', filename='code_filename')
    code_label_id = fields.Char('Code Label ID')
    code_filename = fields.Char('Label Name', states={'invisible': True})
    serial_label = fields.Binary('Etiqueta de serie',
        file_id='serial_label_id', filename='serial_filename')
    serial_filename = fields.Char('Serial Name', states={'invisible': True})
    serial_label_id = fields.Char('Serial Label ID')
    debit_account = fields.Many2One('account.account', 'Gasto de depreciación',
        'Debits',
        required=False, domain=[('kind', '!=', 'view'), ])
    credit_account = fields.Many2One('account.account', 'Depreciación acumulada', # noqa
        'Credits',
        required=False, domain=[('kind', '!=', 'view'), ])
    asset_account = fields.Many2One('account.account', 'Cuenta del activo',
         'Asset Account',
         required=False, domain=[('kind', '!=', 'view'), ])
    currency_digits = fields.Function(
        fields.Integer('Dígitos de la moneda', states={'invisible': True}),
        'on_change_with_currency_digits')
    value = fields.Numeric('Valor en libros',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits', 'state'],
        required=False, states=_STATES)
    residual_value = fields.Numeric('Valor Residual',
        depends=['currency_digits', 'state',],
        required=False,
        digits=(16, Eval('currency_digits', 2)))
    photo = fields.Binary(
        'Foto del Activo', filename='asset_photo',
        file_id='photo_path')
    asset_photo = fields.Char('Photo Name')
    photo_path = fields.Char('Photo Path', readonly=True)
    observations = fields.Text('Observaciones', states=_STATES)
    production_type = fields.Selection(
        [
            ('', ''),
            ('productive', 'Productivo'),
            ('unproductive', 'Improductivo'),
        ], 'Tipo de Producción')
    additional_costs = fields.One2Many(
        'asset.additional_cost', 'asset', 'Costos Adicionales')
    insurance_lines = fields.One2Many(
        'asset.insurance.line', 'asset', 'Seguros lineas')

    derecognition = fields.Many2One('asset.derecognition', 'Detalle de la baja',
        states={
            'readonly': True,
            'invisible': Eval('state') != 'closed'
        }
    )
    actual_value = fields.Function(fields.Numeric(
        'Valor actual', digits=(16, Eval('currency_digits', 2))),
        'on_change_with_actual_value')
    value_already_depreciated = fields.Function(fields.Numeric(
        'Valor depreciado', digits=(16, Eval('currency_digits', 2))),
        'on_change_with_value_already_decreciated'
    )
    total_value_depreciated = fields.Function(fields.Numeric(
        'Depreciacion total acumulada', digits=(16, Eval('currency_digits',
        2))),
        'on_change_with_total_value_depreciated'
    )
    to_the_date = fields.Date('A la fecha', states= _STATES)
    latitude = fields.Numeric('Latitud', states=_STATES)
    longitude = fields.Numeric('Longitud', states=_STATES)
    asset_codeQR = fields.Binary(
        'Código QR', file_id='asset_code_label_idQR',
        filename='asset_code_filenameQR')
    asset_code_label_idQR = fields.Char('Asset Code QR Label ID')
    asset_code_filenameQR = fields.Char('Asset Label Name', states={
        'invisible':  True})

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        # Add workflow states on original asset fields
        fields = ['active', 'name', 'product', 'company', 'type', 'addresses',
                  'owners', 'attribute_set', 'attributes']
        for field in fields:
            asset_field = getattr(cls, field)
            setattr(asset_field, 'states', _STATES)
            setattr(asset_field, 'depends', _DEPENDS)
            if field == 'attributes':
                setattr(asset_field, 'depends', _DEPENDS + ['attribute_set'])
            if field == 'product':
                cls.product.domain.append(('type', 'in', ['assets']))
        cls.type.selection += [('bld', 'BLD'), ('bca', 'BCA'), ('bdi', 'BDI')]
        cls.type.required = True
        cls.code.states.update({
            'readonly': True,
        })
        cls._transitions |= set((
                ('draft', 'open'),
                ('open', 'draft'),
                ('open', 'closed'),
                ))
        cls._buttons.update({
                'run': {
                    'invisible': Eval('state') != 'draft',
                    },
                'close': {
                    'invisible': Eval('state') != 'open',
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['open', 'close']),
                    },
                'generate_labels': {
                    'invisible': False,
                    },
                'generate_depreciation_table': {
                    'invisible': ~Eval('state').in_(['open']),
                    'depends': ['state'],
                },
                })
        cls._error_messages.update({
                'dates_overlaps': ('"%(asset)s" error. '
                'Check purchase date "%(purchase_date)s" '
                'and input date "%(input_date)s". '),
                })
        cls._error_messages.update({
                'serial_unique': ('Serial number must be unique '
                'check "%(serial)s". '),
                })
        cls._error_messages.update({
                'sbye_unique': ('SByE code must be unique '
                'check "%(sbye)s". '),
                })
        cls._error_messages.update({
                'wrong_quantity': ('Quantity (%(quantity)s) for this '
                'Account Asset is at the maximum allowed'),
                })
        cls._error_messages.update({
                'through_date': ('Solo se admite que el campo "Hasta la fecha" esté'
                ' sin rellenar una sola vez en las ubicaciones.'),
                })

    @staticmethod
    def default_kind():
        return 'main'

    @staticmethod
    def default_code():
        return '#'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_asset_type():
        return 'furniture'

    @fields.depends('company')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2

    @fields.depends('possesion_type')
    def on_change_with_account_asset(self, name=None):
        if self.possesion_type != 'own':
            return None
    
    @fields.depends('account_asset', 'purchase_option_value', 'value')
    def on_change_with_actual_value(self, name=None):
        if self.account_asset:
            if self.account_asset.lines:
                value = Decimal('0.0')
                for line in self.account_asset.lines:
                    if line.move:
                        value = line.actual_value
                return value
        elif self.value:
            return self.value
        return 0.0

    @fields.depends('account_asset', 'purchase_option_value',
                    'value_already_depreciated')
    def on_change_with_total_value_depreciated(self, name=None):
        if self.value_already_depreciated:
            if self.account_asset:
                if self.account_asset.lines:
                    aux = Decimal('0.0')
                    for line in self.account_asset.lines:
                        if line.move:
                            value_acu = line.accumulated_depreciation
                            aux = value_acu + self.value_already_depreciated
                    return aux
            elif self.value and self.purchase_option_value:
                aux = (self.purchase_option_value-self.value)
                return aux
        return 0.0

    @fields.depends('purchase_option_value','residual_value')
    def on_change_purchase_option_value(self, name=None):
        pool = Pool()
        Config = pool.get('asset.configuration')
        config = Config(1)
        if self.purchase_option_value:
            self.value = self.purchase_option_value
            aux = self.purchase_option_value * Decimal(config.value_residual)
            self.residual_value = aux
        else:
            self.residual_value = 0.0


    @fields.depends('account_asset', 'purchase_option_value', 'value')
    def on_change_with_value_already_decreciated(self, name=None):
        if self.purchase_option_value:
            if self.value:
                aux = (self.purchase_option_value - self.value)
                return aux
        return 0.0

    @fields.depends('category','life_time')
    def on_change_category(self, name=None):
        if self.category:
            if self.category.life_time_reference:
                self.life_time = self.category.life_time_reference

    @classmethod
    def get_short_name(cls, assets, names):
        short_name = {}
        for asset in assets:
            short_name[asset.id] = asset.name[0:30] + '...'
        result = {
            'short_name': short_name,
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @classmethod
    def get_party(cls, assets, names):
        pool = Pool()
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        asset = Asset.__table__()
        asset_owner = AssetOwner.__table__()
        parties = defaultdict(lambda: None)
        cursor = Transaction().connection.cursor()

        ids = [a.id for a in assets]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(asset.id, sub_ids)
            query_max = asset_owner.select(asset_owner.asset,
                Max(asset_owner.from_date).as_("from_date"),
                group_by=asset_owner.asset)

            query_all = asset.join(asset_owner,
                condition=asset.id == asset_owner.asset
            ).select(
                asset_owner.owner.as_('party'),
                asset_owner.asset,
                asset_owner.from_date,
                where=red_sql)

            query = query_all.join(query_max,
                condition=((query_all.asset == query_max.asset)
                    & (query_all.from_date == query_max.from_date))
            ).select(
                query_all.party,
                query_all.asset
            )
            cursor.execute(*query)
            for party, asset_id in cursor.fetchall():
                parties[asset_id] = party

        result = {
            'party': parties
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @classmethod
    def get_employee(cls, assets, names):
        pool = Pool()
        Employee = pool.get('company.employee')
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        asset = Asset.__table__()
        asset_owner = AssetOwner.__table__()
        employee = Employee.__table__()
        employees = defaultdict(lambda: None)
        cursor = Transaction().connection.cursor()

        ids = [a.id for a in assets]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(asset.id, sub_ids)
            query_max = asset_owner.select(asset_owner.asset,
                Max(asset_owner.from_date).as_("from_date"),
                group_by=asset_owner.asset)

            query_all = asset.join(asset_owner,
                condition=asset.id == asset_owner.asset
            ).join(employee,
                condition=employee.id == asset_owner.employee
            ).select(
                asset_owner.asset,
                asset_owner.employee,
                asset_owner.from_date,
                where=red_sql)

            query = query_all.join(query_max,
                condition=((query_all.asset == query_max.asset)
                    & (query_all.from_date == query_max.from_date))
            ).select(
                query_all.asset,
                query_all.employee,
            )
            cursor.execute(*query)
            for asset_id, employee in cursor.fetchall():
                employees[asset_id] = employee

        result = {
            'employee': employees,
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @classmethod
    def get_department(cls, assets, names):
        pool = Pool()
        Employee = pool.get('company.employee')
        departments = defaultdict(lambda: None)
        asset_data = cls.get_employee(assets, ['employee'])['employee']
        employee_data = Employee.get_contract(
            Employee.search([]), ['contract_department'])['contract_department']
        departments = defaultdict(lambda: None)
        for key, data in asset_data.items():
            departments[key] = employee_data.get(data)

        result = {
            'employee_department': departments,
        }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
            query=False):
        exclude_assets = Transaction().context.get('exclude_assets')
        if exclude_assets:
            excluded = Transaction().context.get('excluded')
            domain = ['AND', domain[:], ('id', 'not in', excluded)]
        return super(Asset, cls).search(domain, offset=offset, limit=limit,
            order=order, count=count, query=query)

    @classmethod
    def search_employee(cls, name, clause):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        asset = Asset.__table__()
        asset_owner = AssetOwner.__table__()
        party = Party.__table__()
        employee = Employee.__table__()

        _, operator_, name = clause
        Operator = fields.SQL_OPERATORS[operator_]

        if isinstance(name, int):
            where = Operator(employee.id, name)
        else:
            where = Operator(party.name, name)

        query_max = asset_owner.select(asset_owner.asset,
            Max(asset_owner.from_date).as_("from_date"),
            group_by=asset_owner.asset)

        query_all = asset.join(asset_owner,
            condition=asset.id == asset_owner.asset
            ).join(employee,
            condition=employee.id == asset_owner.employee
            ).join(party,
            condition=party.id == employee.party).select(
                asset_owner.asset,
                asset_owner.from_date,
                where=where)

        query = query_all.join(query_max,
        condition=((query_all.asset == query_max.asset)
            & (query_all.from_date == query_max.from_date))).select(
                query_all.asset)
        return [('id', 'in', query)]

    @classmethod
    def search_party(cls, name, clause):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        asset = Asset.__table__()
        asset_owner = AssetOwner.__table__()
        party = Party.__table__()
        employee = Employee.__table__()

        _, operator_, name = clause
        Operator = fields.SQL_OPERATORS[operator_]
        if isinstance(name, int):
            where = Operator(party.id, name)
        else:
            where = Operator(party.name, name)

        query_max = asset_owner.select(asset_owner.asset,
            Max(asset_owner.from_date).as_("from_date"),
            group_by=asset_owner.asset)

        query_all = asset.join(asset_owner,
            condition=asset.id == asset_owner.asset
            ).join(party,
            condition=party.id == asset_owner.owner
            ).select(
                asset_owner.asset,
                asset_owner.from_date,
                where=where)

        query = query_all.join(query_max,
        condition=((query_all.asset == query_max.asset)
            & (query_all.from_date == query_max.from_date))).select(
                query_all.asset)
        return [('id', 'in', query)]

    @classmethod
    def search_department(cls, name, clause):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        Contract = pool.get('company.contract')
        Department = pool.get('company.department')
        asset = Asset.__table__()
        asset_owner = AssetOwner.__table__()
        party = Party.__table__()
        employee = Employee.__table__()
        contract = Contract.__table__()
        department = Department.__table__()

        _, operator_, name = clause
        Operator = fields.SQL_OPERATORS[operator_]

        if isinstance(name, int):
            where = Operator(department.id, name)
        else:
            where = Operator(department.name, name)

        end_query = Employee._get_end_contract_sql()

        query_max = asset_owner.select(asset_owner.asset,
            Max(asset_owner.from_date).as_("from_date"),
            group_by=asset_owner.asset)

        query_all = asset.join(asset_owner,
            condition=asset.id == asset_owner.asset
            ).join(employee,
            condition=employee.id == asset_owner.employee
            ).join(party,
            condition=party.id == employee.party).join(contract,
            condition=contract.employee == employee.id).join(department,
            condition=department.id == contract.department).join(end_query,
            condition=contract.id == end_query.contract).select(
                asset_owner.asset,
                asset_owner.from_date,
                where=where)

        query = query_all.join(query_max,
        condition=((query_all.asset == query_max.asset)
            & (query_all.from_date == query_max.from_date))).select(
                query_all.asset)
        return [('id', 'in', query)]

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def run(cls, assets):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, assets):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, assets):
        pass

    @classmethod
    def validate(cls, assets):
        super(Asset, cls).validate(assets)
        cls.check_assets(assets)

    @classmethod
    def check_assets(cls, assets):
        for asset in assets:
            if asset.purchase_date and asset.input_date:
                if asset.purchase_date > asset.input_date:
                    cls.raise_user_error('dates_overlaps', {
                        'asset': asset.rec_name,
                        'purchase_date': asset.purchase_date,
                        'input_date': asset.input_date,
                    })
            if asset.serial:
                if cls.search([('serial', '=', asset.serial),
                        ('id', '!=', asset.id)]):
                    cls.raise_user_error('serial_unique', {
                        'serial': asset.serial
                    })
            
            # if asset.type:
            #     if asset.type == 'bld' and asset.code == '#':
            #         cls.send_email(asset)
            # if asset.sbye:
            #     if cls.search([('sbye', '=', asset.sbye),
            #             ('id', '!=', asset.id)]):
            #         cls.raise_user_error('sbye_unique', {
            #             'sbye': asset.sbye
            #         })
            if asset.account_asset:
                acc_assets = cls.search([
                    ('account_asset', '=', asset.account_asset),
                    ('id', '!=', asset.id)])
                if len(acc_assets) >= asset.account_asset.quantity:
                    cls.raise_user_error('wrong_quantity', {
                        'quantity': asset.account_asset.quantity
                    })
            if asset.locations:
                c = 0
                for loc in asset.locations:
                    if loc.through_date == None:
                        c = c + 1
                if c > 1:
                    cls.raise_user_error('through_date')

    def get_codebar(self, type_='code', print_text=False):
        # if self.kind == 'main':
        #     code = getattr(self, type_)
        # CHILDS HAVE CODE TOO
        # else:
        #     code = getattr(self.parent, type_)
        code = getattr(self, type_)

        if code is None:
            code = ''
        text = ''
        if print_text:
            name = self.name[0:50]
            text = f"{code}\n{name}\n{self.company.party.name}"
        if not code.isascii():
            code = ''
        imgByteArr = io.BytesIO()
        image = barcode.generate(
            'CODE39', code, text=text, pil=True, writer=ImageWriter())
        image.save(imgByteArr, format='PNG')
        imgByteArr.seek(0)
        return imgByteArr.read()

    def get_barcode(self, type_='code'):
        imgByteArr = io.BytesIO()
        if type_ == 'code':
            text = str(self.code)
        else:
            text = str(self.serial)
        image = treepoem.generate_barcode(
            barcode_type='code39',
            data=text,
            options={
                'includetext': True
            }
        )
        image.save(imgByteArr, format='PNG')
        imgByteArr.seek(0)
        return imgByteArr.read()

    @classmethod
    def copy(cls, assets, default=None):
        default.update({'serial': None})
        return super(Asset, cls).copy(assets, default=default)

    @classmethod
    def trigger_create(cls, assets):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        to_save = []
        for asset in assets:
            if asset.code == '#' and asset.kind == 'main':
                asset.code = Sequence.get_id(asset.category.sequence.id)
                to_save.append(asset)
        cls.save(to_save)

    @classmethod
    @ModelView.button
    def generate_labels(cls, assets):
        to_save = []
        for asset in assets:
            asset.code_label = fields.Binary.cast(
                asset.get_barcode(type_='code'))
            if asset.kind == 'main':
                asset.code_filename = f"{asset.code}.png"
            else:
                if asset.parent:
                    asset.code_filename = f"code-{asset.id}.png"
            asset.serial_label = fields.Binary.cast(asset.get_barcode(
                type_='serial'))
            if asset.kind == 'main' and str(asset.serial).isascii():
                asset.serial_filename = f"{asset.serial}.png"
            else:
                if asset.parent:
                    asset.serial_filename = f"serial-{asset.id}.png"
            to_save.append(asset)
        cls.save(to_save)

        to_save_qr = []
        for asset in assets:
            asset.asset_codeQR = fields.Binary.cast(asset.get_QRcode())
            asset.asset_code_filenameQR = f"{asset.code}.png"
            to_save_qr.append(asset)
            cls.save(to_save)

    def get_QRcode(self):
        imgByteArr = io.BytesIO()
        data = {}
        if self.get_rec_name:
            data['Nombre'] = self.rec_name
        if self.employee:
            data['Titular'] = self.employee.party.name
        else:
            data['Titular'] = '------'
        if self.employee_department:
            data['Departamento'] = self.employee_department.name
        else:
            data['Departamento'] = '------'
        image = treepoem.generate_barcode(barcode_type='qrcode',
            data=json.dumps(data))
        image.save(imgByteArr, format='PNG')
        imgByteArr.seek(0)
        return imgByteArr.read()

    @classmethod
    @ModelView.button
    def generate_depreciation_table(cls, assets):
        to_save = []
        AssetDepreciation = Pool().get('account.asset')

        for asset in assets:
            if asset.residual_value == asset.value:
                cls.raise_user_error("No se puede crear la tabla de "
                                     "depreciacion, activo totalmente "
                                     "depreciado")
            if asset.account_asset:
                cls.raise_user_error("Este activo ya"
                    " tiene un registro de depreciación")
            # if (asset.residual_value and asset.value and
            #         asset.credit_account and asset.debit_account and
            #         asset.purchase_date and asset.product):
            # if asset.product:
            if asset.residual_value:
                if asset.value:
                    if asset.credit_account:
                        if asset.debit_account:
                            if asset.purchase_date:
                                depreciation = AssetDepreciation()
                                # if asset.product.depreciation_duration:
                                #     duration = int(asset.product.depreciation_duration)
                                #     end_date = asset.purchase_date.replace(
                                #         year=asset.purchase_date.year + duration)
                                # el
                                if asset.life_time:
                                    duration = int(asset.life_time)
                                    end_date = asset.purchase_date.replace(
                                        year=asset.purchase_date.year + duration)
                                else:
                                    cls.raise_user_error("Se debe añadir los años "
                                        " de depreciación al producto o vida útil "
                                        " al activo")
                                # depreciation.product = asset.product
                                depreciation.debit_account = asset.debit_account
                                depreciation.credit_account = asset.credit_account
                                depreciation.purchase_date = asset.purchase_date
                                depreciation.start_date = asset.purchase_date
                                depreciation.purchase_value= asset.purchase_option_value
                                depreciation.assets = [asset]
                                depreciation.end_date = end_date
                                depreciation.quantity = 1
                                depreciation.residual_value = asset.residual_value
                                depreciation.value = asset.value
                                # depreciation.unit = asset.product.default_uom
                                to_save.append(depreciation)
                            else:
                                cls.raise_user_error("Falta la fecha de "
                                                        "compra del activo")
                        else:
                            cls.raise_user_error("Falta la cuenta "                 
                                " de depreciacion del activo" )
                    else:
                        cls.raise_user_error("Falta la cuenta "
                                                " contable del activo")
                else:
                    cls.raise_user_error("Falta el valor en libros "
                                            "del activo")
            else:
                cls.raise_user_error("Falta el valor residual del activo")
            # else:
            #     cls.raise_user_error("Falta el ingresar el producto")

        AssetDepreciation.save(to_save)
        cls.raise_user_warning('rules_update',
                            "Se creará el registro de"
                             " depreciación en borrador")


    @classmethod
    def send_email(cls, asset):
        pool = Pool()
        SendEmail = pool.get('assent.warranty.end.send.email')
        employees_ = SendEmail.search([])
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Ingreso Nuevo Activo'),
            ('template', '=', True),
        ])
        alerts = []
        employees = []

        if asset:
            for employee in employees_:
                employees.append(employee.email)
            mail_body = cls.generate_text(asset)
            for employee in employees:
                alert_exist = Alert.search([
                    ('state', '=', 'done'),
                    ('model', '=',
                     f"asset,"
                     f"{asset.id}"),
                    ('type', '=', alert_type_employee),
                    ('employee', '=', employee),
                ])
                if not alert_exist:
                    alert = Alert()
                    alert.company = Transaction().context.get('company')
                    alert.model = asset
                    alert.description = mail_body
                    alert.employee = employee
                    alert.type = alert_type_employee
                    alert.state = 'draft'
                    if not alert.employee.email:
                        cls.raise_user_error('not_email')
                        alert.observation = (
                            f"No tiene email registrado el(la) empleado(a) "
                        )
                        alert.send_email = False
                    else:
                        alert.send_email = True
                    alerts.append(alert)
        if alerts:
            Alert.save(alerts)


    @classmethod
    def generate_text(cls, asset):
        return (f"Se ha registrado un nuevo activo tipo BLD con el codigo " 
                f" {asset.code} " f" de nombre "
                "" f" {asset.name}" f" Por favor revisar "
                )
        pass


class AssetCreateDeprecationStart(ModelView):
    'Asset Create Deprecation Start'
    __name__ = 'asset.create.deprecation.start'

    start_date = fields.Date('Fecha inicio', required=True)
    frequency = fields.Selection([
            ('monthly', 'Mensual'),
            ('yearly', 'Anual'),
            ], 'Frecuencia',
        required=True)


class AssetCreateDeprecation(Wizard):
    'Asset Create Deprecation'
    __name__ = 'asset.create.deprecation'

    start = StateView('asset.create.deprecation.start',
        'asset_ec.asset_create_deprecation_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'change', 'tryton-ok', default=True),
            ])
    change = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AssetCreateDeprecation, cls).__setup__()
        # cls._error_messages.update({
        #         'lower_mileage': ('El recorrido o las horas m. ingresadas '
        #             'es menor al recorrido o horas m. actuales del '
        #             'vehículo: %s.')
        #         })

    def transition_change(self):
        to_save = []
        pool = Pool()
        AssetDepreciation = pool.get('account.asset')
        Asset = pool.get('asset')
        asset = Asset(Transaction().context['active_id'])
        if asset.residual_value == asset.value:
            Asset.raise_user_error("No se puede crear la tabla de "
                                    "depreciacion, activo totalmente "
                                    "depreciado")
        if asset.account_asset:
            Asset.raise_user_error("Este activo ya"
                " tiene un registro de depreciación")
        # if (asset.residual_value and asset.value and
        #         asset.credit_account and asset.debit_account and
        #         asset.purchase_date and asset.product):
        # if asset.product:
        if asset.residual_value:
            if asset.value:
                if asset.credit_account:
                    if asset.debit_account:
                        if asset.purchase_date:
                            depreciation = AssetDepreciation()
                            # if asset.product.depreciation_duration:
                            #     duration = int(asset.product.depreciation_duration)
                            #     end_date = asset.purchase_date.replace(
                            #         year=asset.purchase_date.year + duration)
                            # el
                            if asset.life_time:
                                duration = int(asset.life_time)
                                end_date = asset.purchase_date.replace(
                                    year=asset.purchase_date.year + duration)
                            else:
                                Asset.raise_user_error("Se debe añadir los años "
                                    " de depreciación al producto o vida útil "
                                    " al activo")
                            # depreciation.product = asset.product
                            depreciation.debit_account = asset.debit_account
                            depreciation.credit_account = asset.credit_account
                            depreciation.purchase_date = asset.purchase_date
                            depreciation.start_date = self.start.start_date
                            depreciation.frequency = self.start.frequency
                            depreciation.purchase_value= asset.purchase_option_value
                            depreciation.assets = [asset]
                            depreciation.end_date = end_date
                            depreciation.quantity = 1
                            depreciation.residual_value = asset.residual_value
                            depreciation.value = asset.value
                            # depreciation.unit = asset.product.default_uom
                            to_save.append(depreciation)
                        else:
                            Asset.raise_user_error("Falta la fecha de "
                                                    "compra del activo")
                    else:
                        Asset.raise_user_error("Falta la cuenta "                 
                            " de depreciacion del activo" )
                else:
                    Asset.raise_user_error("Falta la cuenta "
                                            " contable del activo")
            else:
                Asset.raise_user_error("Falta el valor en libros "
                                        "del activo")
        else:
            Asset.raise_user_error("Falta el valor residual del activo")
        # else:
        #     cls.raise_user_error("Falta el ingresar el producto")

        AssetDepreciation.save(to_save)
        AssetDepreciation.create_lines(to_save)
        return 'end'



class AssetAdditionalCost(ModelSQL, ModelView):
    'Asset Additional Cost'
    __name__ = 'asset.additional_cost'

    asset = fields.Many2One('asset', 'Activo Fijo', required=True)
    description = fields.Char('Descripciön', required=True)
    amount = fields.Numeric('Monto', digits=(16, 2), required=True)


class AssetSBYECode(ModelSQL, ModelView):
    'AssetSBYECode'
    __name__ = 'account.asset.sbye.code'

    sbye_code = fields.Char('Código SBYE', readonly=True, select=True)
    budget_item = fields.Char('Ítem presupuestario', readonly=True, select=True)
    description = fields.Text('Descripción', readonly=True, select=True)


class AssetLine(metaclass=PoolMeta):
    __name__ = 'account.asset.line'

    units_productions = fields.Function(
        fields.Numeric('Unidades Producción'), 'get_units_productions')

    @classmethod
    def __setup__(cls):
        super(AssetLine, cls).__setup__()
        cls.units_productions.states = {
            'invisible': (Eval('_parent_asset', {}).get('depreciation_method')
                 != 'utpe')
            }
        cls.actual_value.digits = (16, 2)
        cls.accumulated_depreciation.digits = (16, 2)

    @classmethod
    def get_units_productions(cls, lines, name):
        pool = Pool()
        result = defaultdict(lambda: 0)
        work = pool.get('asset.work')
        for line in lines:
            total = 0
            for w in work.search([('line', '=', line.id)]):
                total += w.unit_work
            result[line.id] = total
        return result


class AssetOwner(metaclass=PoolMeta):
    __name__ = 'asset.owner'

    employee = fields.Many2One('company.employee', 'Employee', required=False)
    notes = fields.Text('Notes')
    asset = fields.Many2One('asset', 'Asset', required=True,
        domain=[
            ('company', '=', Eval('company'))
            ])

    @classmethod
    def __setup__(cls):
        super(AssetOwner, cls).__setup__()
        cls.owner.states.update({'invisible': False})
        cls.from_date.domain = [()]
        cls.through_date.domain = [()]
        cls._order = [
            ('id', 'DESC')
        ]
        cls.from_date.required = True
        cls._error_messages.update({
                'dates_overlaps_ec': ('"%(asset)s" error. '
                'Check input date "%(input_date)s" '
                'and from date "%(from_date)s".'),
                })
        cls._error_messages.update({
                'without_through_date': ('"%(asset)s" error. '
                'Check through dates'),
                })

    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.owner = self.employee.party
        else:
            self.owner = None

    def check_dates(self):
        # super(AssetOwner, self).check_dates()
        if self.asset.input_date and self.from_date:
            if self.asset.input_date > self.from_date:
                self.raise_user_error('dates_overlaps_ec', {
                    'asset': self.asset.rec_name,
                    'input_date': self.asset.input_date,
                    'from_date': self.from_date,
                })
            res = self.__class__.search([
                ('asset', '=', self.asset.id),
                ('through_date', '=', None)
            ], count=True)
            if res > 1:
                self.__class__.raise_user_error('without_through_date', {
                    'asset': self.asset.rec_name})


class CreateAssetCertificate(ModelSQL, ModelView):
    'Create Certificate'
    __name__ = 'asset.create.certificate'
    employee = fields.Many2One('company.employee',' Empleado')

    @classmethod
    def __setup__(cls):
        super(CreateAssetCertificate, cls).__setup__()
        cls._order = [
            ('id', 'DESC')
        ]


class CreateAssetCertifcateStart(ModelView):
    'Create Asset Certificate Start'
    __name__ = 'asset.create.certificate.start'

    employee = fields.Many2One('company.employee',' Empleado')
    owner = fields.Many2One('party.party',' Tercero',states={
        'required': True
    })

    @fields.depends('employee')
    def on_change_employee(self):
        if self.employee:
            self.owner = self.employee.party
        else:
            self.owner = None

class AssetCreateCertificateWizard(Wizard):
    'Asset Create Certificate Wizard'
    __name__ = 'asset.create.certificate.wizard'

    start = StateView('asset.create.certificate.start',
                      'asset_ec.create_certificate_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('Imprimir', 'print_', 'tryton-print',
                                 default=True),
                      ])
    print_ = StateReport('asset.certificate')
    def do_print_(self, action):
        Date = Pool().get('ir.date')
        today_date = Transaction().context.get('date') or Date.today()
        owner = self.start.owner
        data = {
            'owner': owner.id,
        }

        return action,data



class AssetTransfer(Workflow, ModelSQL, ModelView):
    'Asset Transfer'
    __name__ = 'asset.transfer'
    _rec_name = 'number'
    _DEPENDS_LINES = _DEPENDS + ['lines']
    _DEPENDS_EMPLOYEES = _DEPENDS + ['from_employee', 'to_employee']

    number = fields.Char('Number', size=None, readonly=True, select=True)
    is_from_party = fields.Boolean('De tercero?')
    from_employee = fields.Many2One('company.employee', 'From employee',
        domain=[
            ('id', '!=', Eval('to_employee')),
            ],
        states={
            'readonly': ((Eval('state') != 'draft')
                | (Eval('lines', [0]))),
            'invisible': Bool(Eval('is_from_party')),
            'required': ~Bool(Eval('is_from_party'))
            }, required=False, depends=_DEPENDS_LINES + ['to_employee'])
    from_party = fields.Many2One('party.party', 'De tercero',
        domain=[
            ('id', '!=', Eval('to_party')),
            ],
        states={
            'readonly': ((Eval('state') != 'draft')
                | (Eval('lines', [0]))),
            'invisible': ~Bool(Eval('is_from_party')),
            'required': Bool(Eval('is_from_party'))
            }, required=False, depends=_DEPENDS_LINES + ['to_party'])
    is_to_party = fields.Boolean('A tercero?')
    to_employee = fields.Many2One('company.employee', 'To employee',
        states={
            'readonly': ((Eval('state') != 'draft')
                | (Eval('lines', [0]))),
            'invisible': Bool(Eval('is_to_party')),
            'required': ~Bool(Eval('is_to_party'))
            }, required=False, depends=_DEPENDS_LINES)
    to_party = fields.Many2One('party.party', 'A tercero',
        states={
            'readonly': ((Eval('state') != 'draft')
                | (Eval('lines', [0]))),
            'invisible': ~Bool(Eval('is_to_party')),
            'required': Bool(Eval('is_to_party'))
            }, required=False, depends=_DEPENDS_LINES)
    transfer_date = fields.Date('Transfer date', required=False,
        states=_STATES, depends=_DEPENDS + ['transfer_date'],
        domain=[('transfer_date', '<=',datetime.date.today() + datetime.timedelta(days=10))],
        help="La fecha no puede ser mayor, a la fecha actual"
    )



    state = fields.Selection([
        ('draft', 'Borrador'),
        ('done', 'Realizado'),
        ('cancel', 'Cancelado'),
    ], 'State', readonly=True, required=True)
    lines = fields.One2Many('asset.transfer.line', 'transfer', 'Lines',
        # domain=[
        #     If(Eval('state').in_(['draft']),
        #        ('OR',
        #             ('asset.employee', '=', Eval('from_employee')),
        #             ('asset.party', '=', Eval('from_party'))
        #        ),
        #        ('asset.employee', '!=', -1),
        #     )],
        states={
            'readonly': ~(Eval('from_employee') & Eval('to_employee')),
            }, depends=_DEPENDS_EMPLOYEES)
    notes = fields.Text('Notes', states=_STATES, depends=_DEPENDS)
    assets = fields.Function(fields.Many2Many('asset', None, None,
        'Assets'), 'on_change_with_assets')

    @fields.depends('lines')
    def on_change_with_assets(self, name=None):
        if not self.lines:
            return []
        assets = []
        for line in self.lines:
            if line.asset and line.asset.id > 0:
                assets.append(line.asset.id)
        return assets

    @fields.depends('lines')
    def on_change_lines(self, name=None):
        pool = Pool()
        TransferLine = pool.get('asset.transfer.line')
        asset_ids = []
        if self.lines:
            for line in self.lines:
                if line.asset:
                    asset_ids.append(line.id)
                    for child in line.asset.childs:
                        if child.id not in asset_ids:
                            asset_ids.append(child.id)
                            new_line = TransferLine()
                            new_line.asset = child
                            self.lines = self.lines + (new_line, )

    @classmethod
    def __setup__(cls):
        super(AssetTransfer, cls).__setup__()
        cls._order = [
            ('transfer_date', 'DESC'),
            ('id', 'DESC'),
            ]
        cls._transitions |= set((
                ('draft', 'done'),
                ('draft', 'cancel'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('context', {}).get('groups', []).contains(
                        Id('asset', 'group_asset_admin')),
                    },
                'done': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('context', {}).get('groups', []).contains(
                        Id('asset', 'group_asset_admin')),
                    },
                'select_assets': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('context', {}).get('groups', []).contains(
                        Id('asset', 'group_asset_admin')),
                    },
                })

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_transfer_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def set_number(cls, transfers):
        '''
        Fill the number field with the transfer sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')

        config = Config(1)
        for transfer in transfers:
            if transfer.number:
                continue
            transfer.number = Sequence.get_id(config.asset_transfer_sequence.id)
        cls.save(transfers)

    def create_owner(self):
        '''
        Create owner for each tranfer line
        '''
        pool = Pool()
        Owner = pool.get('asset.owner')
        TransferLine = pool.get('asset.transfer.line')
        to_save = []
        owners = []
        for line in self.lines:
            last_owner, owner = line.get_owner()
            if last_owner:
                line.from_owner = last_owner
                owners.append(last_owner)
            if owner:
                line.to_owner = owner
                owners.append(owner)
            to_save.append(line)
        Owner.save(owners)
        TransferLine.save(to_save)
        return owners

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, transfers):
        for transfer in transfers:
            transfer.create_owner()
        cls.set_number(transfers)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, transfers):
        pass

    @classmethod
    @ModelView.button_action(
        'asset_ec.wizard_asset_transfer_select')
    def select_assets(cls, payments):
        pass


class AssetTransferLine(ModelSQL, ModelView):
    'Asset Transfer Line'
    __name__ = 'asset.transfer.line'

    transfer = fields.Many2One('asset.transfer', 'Transfer',
        required=True, ondelete='CASCADE')
    asset = fields.Many2One('asset', 'Asset',
        context={
            'exclude_assets': True,
            'excluded': If(Bool(Eval('_parent_transfer', {}).get('assets')),
                Eval('_parent_transfer', {}).get('assets', []), [])},
                depends=['transfer'])
    notes = fields.Char('Notes')
    from_owner = fields.Many2One('asset.owner', 'From onwer')
    to_owner = fields.Many2One('asset.owner', 'To onwer')
    location = fields.Many2One('company.location', 'Location')

    @fields.depends('asset')
    def on_change_with_location(self, name=None):
        if self.asset and self.asset.location:
            return self.asset.location.id

    def get_owner(self):
        '''
        Return owner values for transfer line
        '''
        pool = Pool()
        Owner = pool.get('asset.owner')
        last_owner = None
        if self.asset.employee or self.asset.party:
            last_owner = self.asset.owners[0]
            last_owner.through_date = \
                self.transfer.transfer_date - datetime.timedelta(1)
            self.from_owner = last_owner
            Owner.save([last_owner])
        owner = Owner()
        owner.asset = self.asset
        owner.from_date = self.transfer.transfer_date
        if self.transfer.is_to_party == False:
            owner.employee = self.transfer.to_employee
            owner.on_change_employee()
        else:
            owner.owner = self.transfer.to_party
        owner.notes = self.notes
        return [last_owner, owner]


class CompanyLocation(ModelView, ModelSQL):
    'company_location'
    __name__ = 'company.location'
    name = fields.Char('Name', required=True)
    address = fields.Char('Address')
    company = fields.Many2One('company.company', 'Company', required=True)
    employees = fields.One2Many(
        'company.employee', 'location', 'Empleados en esta ubicacion')

    def get_rec_name(self, name):
        str_address = ''
        if self.address:
            str_address = ' (' + self.address + ')'
        return "%s%s" % (self.name, str_address)


class Company(metaclass=PoolMeta):
    'company_company'
    __name__ = 'company.company'

    company_locations = fields.One2Many('company.location',
        'company', 'Locations')


class AssetLocation(AssetAssignmentMixin):
    'Asset Location'
    __name__ = 'asset.location'

    asset = fields.Many2One('asset', 'Asset', required=True,
        ondelete='CASCADE')
    location = fields.Many2One('company.location', 'Location', required=True)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('location.' + clause[0],) + tuple(clause[1:]),
            ]


class AssetWork(Workflow, ModelSQL, ModelView):
    'Asset Work'
    __name__ = 'asset.work'

    asset = fields.Many2One('asset', 'Activo', required=True,
        ondelete='CASCADE')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('running', 'Ejecucion'),
        ('done', 'Aprobado')
        ], 'State', readonly=True)
    state_translated = state.translated('state')
    line = fields.Many2One('account.asset.line', "Línea Periodo",
        domain=[
            ('asset', 'where', [
                ('assets', 'in', [Eval('asset')]),
                ('state', '=', 'running'),
            ]),

        ], depends=['asset'], required=True)
    unit_work = fields.Numeric('Unidad de producción', required=True)

    @classmethod
    def __setup__(cls):
        super(AssetWork, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('period_unique', Unique(t, t.asset, t.line),
             'El periodo debe ser único por activo.'),
        ]
        cls._error_messages.update({
            'exist_move':
                ('Ya existe un asiento contable para el periodo (%(period)s).'),
        })

    @classmethod
    def validate(cls, asset_works):
        for work in asset_works:
            work.check_exist_move()

    @fields.depends('unit_work')
    def on_change_with_unit_work(self, name=None):
        if self.id:
            if self.id > 0:
                self.check_exist_move()
        return self.unit_work

    def check_exist_move(self):
        pool = Pool()
        AccountAssetLine = pool.get('account.asset.line')
        asset_lines = AccountAssetLine.search([
            ('asset', 'where', [('assets', 'in', [self.asset.id])]),
            ('date', '=', self.line.date),
            ('move', '!=', None),
        ])
        if asset_lines:
            self.raise_user_error('exist_move', {
                'period': str(self.line.date)
            })

    @classmethod
    def delete(cls, asset_works):
        pool = Pool()
        AccountAssetLine = pool.get('account.asset.line')
        for work in asset_works:
            asset_lines = AccountAssetLine.search([
                ('asset', 'where', [('assets', 'in', [work.asset.id])]),
                ('date', '=', work.line.date),
                ('move', '!=', None),
            ])
            if asset_lines:
                cls.raise_user_error('exist_move', {
                    'period': str(work.line.date)
                })
        super(AssetWork, cls).delete(asset_works)


class AssetIncident(Workflow, ModelSQL, ModelView):
    'Averías'
    __name__ = 'asset.incident'
    _history = True
    _states = {
        'readonly': Eval('state') != 'draft'
    }

    company = fields.Many2One('company.company', 'Empresa', required=True,
        states=_states)
    number = fields.Char('Número', readonly=True)
    date = fields.Date('Fecha', required=True, select=True, states=_states)
    requester = fields.Many2One('company.employee', 'Solicitante',
        required=True, states=_states)
    justification = fields.Text('Justificación', required=True, states=_states)
    lines = fields.One2Many('asset.incident.line', 'incident', 'Detalles',
        required=True, states=_states)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('open', 'Abierto'),
        ('close', 'Cerrado'),
        ('cancel', 'Cancelado')], 'Estado', required=True, readonly=True)

    state_translated = state.translated('state')




    @classmethod
    def __setup__(cls):
        super(AssetIncident, cls).__setup__()
        cls._order[0] = ('number', 'DESC')
        cls._error_messages.update({
            'without_asset_incident_sequence': (
                'Secuencia de incidente no configurada.'),
        })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'close'),
            ('open', 'cancel'),
            ('close', 'cancel')
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_([
                    'draft', 'open', 'close', 'cancel', 'done']),
                'depends': ['state'],
            },
            'open': {
                'invisible': Eval('state').in_(['open', 'close', 'cancel']),
                'depends': ['state'],
            },
            'close': {
                'invisible': Eval('state').in_(['close', 'draft', 'cancel']),
                'depends': ['state'],
            },
            'cancel': {
                'invisible': Eval('state').in_(['draft', 'cancel']),
                'depends': ['state'],
            }
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, requests):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, requests):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')
        config = Config(1)
        if not config.asset_incident_sequence:
            cls.raise_user_error('Debe crear una secuencia para ',
                'los incidentes en la configuración de activos')
        for writeoff in requests:
            if writeoff.number:
                continue
            writeoff.number = Sequence.get_id(config.asset_incident_sequence.id)
        cls.save(requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, requests):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, requests):
        pass

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'


class AssetIncidentLine(ModelSQL, ModelView):
    'Asset Incident Line'
    __name__ = 'asset.incident.line'
    _history = True
    _states = {
        'readonly': Eval('_parent_incident', {}).get('state') != 'draft'
    }

    incident = fields.Many2One('asset.incident', 'Incidente', required=True,
                              ondelete='RESTRICT')
    asset = fields.Many2One('asset', 'Activo', required=True,
        domain=[
            ('company', '=', Eval('_parent_incident', {}).get('company'))
        ], states=_states)
    incidents = fields.Char('Incidente', required=True, states=_states)
    result = fields.Char('Resultado',
        states={
            'readonly': Eval('_parent_incident', {}).get('state').in_([
                'draft', 'close', 'cancel']),
            'required': Eval('_parent_incident', {}).get('state').in_(['open']),
        })


class AssetTransferSelectStart(ModelView):
    'Update Vehicle Mileage Start'
    __name__ = 'asset.transfer.select.start'

    assets = fields.One2Many('asset', None, 'Activos',
        domain=['OR',
            ('employee', '=', Eval('from_employee')), 
            ('party', '=', Eval('from_party')),
        ], depends=['from_employee', 'from_party']
    )

    from_employee = fields.Many2One('company.employee', 'De empleado',
        states={'readonly': True})
    from_party = fields.Many2One('party.party', 'De tercero',
        states={'readonly': True})

    @staticmethod
    def default_from_employee():
        pool = Pool()
        Transfer = pool.get('asset.transfer')
        transfer = Transfer(Transaction().context['active_id'])
        if transfer.from_employee:
            return transfer.from_employee.id
        else:
            return None

    @staticmethod
    def default_from_party():
        pool = Pool()
        Transfer = pool.get('asset.transfer')
        transfer = Transfer(Transaction().context['active_id'])
        if transfer.from_party:
            return transfer.from_party.id
        else:
            return None


class AssetTransferSelect(Wizard):
    'Asset Transfer Select'
    __name__ = 'asset.transfer.select'

    start = StateView('asset.transfer.select.start',
        'asset_ec.asset_transfer_select_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'change', 'tryton-ok', default=True),
            ])
    change = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AssetTransferSelect, cls).__setup__()
        cls._error_messages.update({
                'lower_mileage': ('El recorrido o las horas m. ingresadas '
                    'es menor al recorrido o horas m. actuales del '
                    'vehículo: %s.')
                })

    def transition_change(self):
        pool = Pool()
        Line = pool.get('asset.transfer.line')
        assets = []
        Transfer = pool.get('asset.transfer')
        transfer = Transfer(Transaction().context['active_id'])
        for asset in self.start.assets:
            line = Line()
            line.asset = asset
            assets.append(line)
        transfer.lines = assets
        Transfer.save([transfer])
        return 'end'


class CreateMoves(metaclass=PoolMeta):
    'Create Moves'
    __name__ = 'account.asset.create_moves'

    def transition_create_moves(self):

        pool = Pool()
        Asset = Pool().get('account.asset')
        AssetLine = Pool().get('account.asset.line')
        Company = Pool().get('company.company')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Period = pool.get('account.period')
        assets = Asset.search([
                ('state', '=', 'running'),
                ])
        company = Company.search([
                ('id', '=', Transaction().context.get('company')),
            ])[0]
        journal = None
        to_date = self.start.date

        debit_lines = defaultdict(lambda: {})
        credit_lines = defaultdict(lambda: {})
        asset_lines = defaultdict(lambda: [])
        value = 0
        for asset in assets:
            journal = asset.account_journal
            for line in asset.lines:
                if (not line.move) and (line.date <= to_date):
                    date = str(line.date)
                    # DEBIT
                    if date not in debit_lines.keys():
                        debit_lines[date] = {}
                    if asset.debit_account.code not in debit_lines[date].keys():
                        debit_lines[date][asset.debit_account.code]={
                            'account': asset.debit_account.id,
                            'debit': 0,
                            'credit': 0,
                        }
                    debit_lines[date][asset.debit_account.code]['debit'] += \
                        line.depreciation
                    # CREDIT
                    if date not in credit_lines.keys():
                        credit_lines[date] = {}
                    if asset.credit_account.code not in credit_lines[date].keys():
                        credit_lines[date][asset.credit_account.code]={
                            'account': asset.credit_account.id,
                            'debit': 0,
                            'credit': 0,
                        }
                    credit_lines[date][asset.credit_account.code]['credit'] += \
                        line.depreciation
                    value += line.depreciation

                    if date not in asset_lines.keys():
                        asset_lines[date] = []
                    if line.id not in asset_lines[date]:
                        asset_lines[date].append(line)
        for date_key in debit_lines.keys():
            if isinstance(date_key, str):
                new_date = datetime.date(int(date_key.split('-')[0]),
                    int(date_key.split('-')[1]),
                    int(date_key.split('-')[2]))
            period_id = Period.find(company.id, new_date)
            description = "Asiento de deprecación de activos a la fecha: "+\
                date_key
            move = Move(
                company=company.id,
                period=period_id,
                journal=journal.id,
                date=date_key,
                description=description
            )
            lines = []
            for data_key in debit_lines[date_key]:
                # account = Account.search([('id','=',data.account)])
                
                line = MoveLine(
                    debit=debit_lines[date_key][data_key]['debit'],
                    credit=0,
                    account=debit_lines[date_key][data_key]['account'])
                
                lines.append(line)
            for data_key in credit_lines[date_key]:
                # account = Account.search([('id','=',data.account)])
                
                line = MoveLine(
                    debit=0,
                    credit=credit_lines[date_key][data_key]['credit'],
                    account=credit_lines[date_key][data_key]['account'])
                
                lines.append(line)
            move.lines = lines 
            move.save()
            for line_asset_item in asset_lines[date_key]:
                line_asset_item.move = move
            AssetLine.save(asset_lines[date_key])
        return 'end'


class AssetInventory(ModelSQL, ModelView):
    'Asset Line'
    __name__ = 'asset.inventory'
    _history = True

    company = fields.Many2One('company.company', 'Empresa', required=True)
    number = fields.Char('Número', size=None, readonly=True, select=True)
    inspector = fields.Many2One('company.employee', 'Responsable',
        required=True)
    date = fields.Date('Fecha', required=True, select=True)
    custodian = fields.Many2One('company.employee', 'Custodio')
    description = fields.Text('Descripción')
    lines = fields.One2Many('asset.inventory.line', 'inventory', 'Líneas')

    @classmethod
    def __setup__(cls):
        super(AssetInventory, cls).__setup__()

    @classmethod
    def validate(cls, inventory):
        for line in inventory:
            if not line.number:
                cls.set_number(line)

    @classmethod
    def set_number(cls, inventory):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('asset.configuration')

        config = Config(1)
        if not config.asset_inventory_sequence:
            cls.raise_user_error("Debe crear una secuencia para " +
            " el inventario en la configuración de Activos")
        inventory.number = Sequence.get_id(config.asset_inventory_sequence.id)
        cls.save([inventory])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_inspector():
        return Transaction().context.get('employee')


class AssetInventoryLine(ModelSQL, ModelView):
    'Asset Inventory Line'
    __name__ = 'asset.inventory.line'
    _history = True

    inventory = fields.Many2One('asset.inventory', 'Iventario')
    asset = fields.Many2One('asset', 'Activo',
        required=True)
    custodian = fields.Many2One('company.employee', 'Custodio',
        required=True)
    asset_code = fields.Char('Código')
    asset_serial = fields.Char('Serial')


    @fields.depends('asset', 'custodian', 'asset_code')
    def on_change_asset(self, name=None):
        if self.asset:
            if self.asset.employee:
                self.custodian = self.asset.employee
                self.asset_code = self.asset.code
                self.asset_serial = self.asset.serial
