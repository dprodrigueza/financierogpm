from trytond.model import Workflow, ModelSQL, ModelView, fields
from trytond.pyson import Eval
from trytond.pool import Pool, PoolMeta
from datetime import datetime
from datetime import date
from trytond.transaction import Transaction


_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']

__all__ = ['WorkWarranty', 'WorkWarrantyAsset']


class WorkWarranty(metaclass=PoolMeta):
    'Pólizas y Garantías'
    __name__ = 'work.warranty'

    assets = fields.Many2Many('work.warranty-asset', 'warranty', 'asset',
        'Activos', states=_STATES, depends=_DEPENDS)

    @classmethod
    def notify_expiration_warranty(cls):
        pool = Pool()
        today = date.today()
        Warranty = pool.get('work.warranty')
        warrantys = Warranty.search([])
        warrantys_end = []
        for warranty in warrantys:
            if warranty.end_date:
                if warranty.assets:
                    if warranty.end_date == today:
                        warrantys_end.append(warranty)
        if warrantys_end:
            pass
            cls.send_email(cls, warrantys_end)

    def send_email(cls, warrantys_end):
        pool = Pool()
        SendEmail = pool.get('assent.warranty.end.send.email')
        employees_ = SendEmail.search([])
        Alert = pool.get('siim.alert')
        AlertTypeEmployee = pool.get('siim.alert.type.employee')
        alert_type_employee, = AlertTypeEmployee.search([
            ('type.name', '=', 'Garantias Vencidas'),
            ('template', '=', True),
        ])
        alerts = []
        employees = []

        for warranty in warrantys_end:
            if warranty:
                print(warranty.id)
                for employee in employees_:
                    employees.append(employee.email)
                mail_body = cls.generate_text(warranty)
                for employee in employees:
                    alert_exist = Alert.search([
                        ('state', '=', 'done'),
                        ('model', '=',
                         f"work.warranty,"
                         f"{warranty.id}"),
                        ('type', '=', alert_type_employee),
                        ('employee', '=', employee),
                    ])
                    if not alert_exist:
                        alert = Alert()
                        alert.company = Transaction().context.get('company')
                        alert.model = warranty
                        alert.description = mail_body
                        alert.employee = employee
                        alert.type = alert_type_employee
                        alert.state = 'draft'
                        if not alert.employee.email:
                            cls.raise_user_error('not_email')
                            alert.observation = (
                                f"No tiene email registrado el(la) empleado(a) "
                            )
                            alert.send_email = False
                        else:
                            alert.send_email = True
                        alerts.append(alert)
        if alerts:
            Alert.save(alerts)

    @classmethod
    def generate_text(cls, warranty):
        return (f"La garantia " f"{warranty.number} " "esta por cumplirse "
                                                " revisar,"
                                             "" f" {warranty.party.name}"
                )
        pass


class WorkWarrantyAsset(ModelSQL, ModelView):
    'Work Warranty - Asset'
    __name__ = 'work.warranty-asset'

    warranty = fields.Many2One('work.warranty', 'Garantía')
    asset = fields.Many2One('asset', 'Activo')

class Asset(metaclass=PoolMeta):
    'Asset'
    __name__ = 'asset'

    warranties = fields.Many2Many('work.warranty-asset', 'asset', 'warranty',
        'Garantías', states={
            'readonly': True
        })