from calendar import monthrange
import datetime
from decimal import Decimal
from trytond.pool import Pool, PoolMeta
from trytond.model import fields, ModelView, ModelSQL, Workflow
from trytond.pyson import Eval, Bool, If
from trytond.tools import grouped_slice
from trytond.transaction import Transaction
from trytond.wizard import StateAction
from trytond.wizard import (Wizard, Button, StateTransition, StateView)


__all__ = ['Asset', 'CreateMovesStart', 'CreateMoves', 'UpdateAsset']


class Asset(Workflow, metaclass=PoolMeta):
    __name__ = 'account.asset'
    _states = {
        'readonly': Bool(Eval('assets'))
    }

    debit_account = fields.Many2One('account.account', 'Débito', 'Debits',
        required=True, states=_states, domain=[('kind', '!=', 'view'), ])
    credit_account = fields.Many2One('account.account', 'Crédito', 'Credits',
        required=True, states=_states, domain=[('kind', '!=', 'view'), ])
    purchase_value = fields.Numeric(
        'V. Compra', digits=(16, Eval('currency_digits', 2)),  states=_states)
    total_units_production = fields.Numeric('Total Unidades de Produción',
        states={
            'readonly': ((Eval('depreciation_method') != 'utpe') |
                         Eval('lines', [0])),
            'required': Eval('depreciation_method') == 'utpe',
        }, depends=['depreciation_method'])

    assets = fields.One2Many('asset', 'account_asset', 'Assets',
        domain=[
            ('debit_account', '=', Eval('debit_account')),
            ('credit_account', '=', Eval('credit_account')),
                ],
        depends=['company', 'debit_account', 'credit_account', 'product'
        ],
        required=True, states={
            'readonly': (Eval('lines', [0]) | (Eval('state') != 'draft')),
                        })

    asset_code = fields.Function(fields.Char('Código Activo'),
                                   'on_change_with_asset_code',
                                   searcher='search_asset_code')

    @classmethod
    def __setup__(cls):
        super(Asset, cls).__setup__()
        cls.depreciation_method.selection.append((
            'utpe', 'Unidad Producción'))
        cls.quantity.domain.append(([('quantity', '>', 0)]))
        cls.value.states = {'readonly': True}
        cls.residual_value.states = {'readonly': True}
        cls.product.required = False
        cls.quantity.states = {'readonly': True}
        cls.total_units_production.domain.append(
            If(Eval('depreciation_method') == 'utpe',
                ('total_units_production', '>', 0),
                (),
            ))

        cls._buttons.update({
            'update_depreciation': {
                'invisible': (Bool(Eval('depreciation_method') != 'utpe') |
                     Bool(Eval('state') == 'draft')),
                'depends': ['depreciation_method'],
            }
        })

    @classmethod
    def search_asset_code(cls, name, clause):
        pool = Pool()
        Asset = pool.get('asset')
        account_asset = cls.__table__()
        asset = Asset.__table__()
        _, operator, value = clause
        query = account_asset.join(asset,
                 condition=asset.account_asset == account_asset.id
                ).select(
                account_asset.id,
            where=(asset.code.ilike(value))
        )
        return [('id', 'in', query)]
    
    def get_rec_name(self, name):
        return '%s' % (self.number)

    @fields.depends('assets')
    def on_change_with_asset_code(self, name=None):
        if self.assets:
            if self.assets[0].id > 0:
                return self.assets[0].code

    @staticmethod
    def default_quantity():
        return 0.0

    @staticmethod
    def default_value():
        return 0.0

    @staticmethod
    def default_residual_value():
        return 0.0

    @staticmethod
    def default_total_units_production():
        return 0

    @fields.depends('assets', 'value', 'residual_value')
    def on_change_assets(self, name=None):
        value = Decimal('0.0')
        residual_value = Decimal('0.0')
        for asset in self.assets:
            if asset.value:
                value += asset.value
                residual_value += asset.residual_value
        self.value = value
        self.residual_value = residual_value
        self.quantity = len(self.assets)

    def compute_depreciation(self, date, dates):
        if self.depreciation_method == 'linear':
            return super(Asset, self).compute_depreciation(date, dates)
        elif self.depreciation_method == 'utpe':
            return 0

    def depreciate(self):
        """
        Returns all the depreciation amounts still to be accounted.
        """
        Line = Pool().get('account.asset.line')
        amounts = {}
        dates = self.compute_move_dates()
        amount = (self.value - self.get_depreciated_amount()
            - self.residual_value)
        initial_depreciation = 0
        if amount <= 0:
            return amounts
        residual_value, acc_depreciation = amount, Decimal(0)
        asset_line = None
        for date in dates:
            depreciation = self.compute_depreciation(date, dates)

            if initial_depreciation == 0 and self.update_moves:
                day = self.update_moves[0].date.day
                year = self.update_moves[0].date.year
                month = self.update_moves[0].date.month
                lines = [line.depreciation for line in self.lines
                         if line.move and line.move.state == 'posted']
                last_dep = max(lines) if lines else 0
                month_day = monthrange(year, month)
                initial_depreciation = (((last_dep / month_day[1]) * (day - 1)) +
                    ((depreciation / month_day[1]) * (month_day[1] - (day - 1))))
                depreciation = round(initial_depreciation, 2)

            amounts[date] = asset_line = Line(
                acquired_value=self.value,
                depreciable_basis=amount,
                )
            if depreciation > residual_value:
                asset_line.depreciation = residual_value
                asset_line.accumulated_depreciation = (
                    self.get_depreciated_amount()
                    + acc_depreciation + residual_value)
                break
            else:
                residual_value -= depreciation
                acc_depreciation += depreciation
                asset_line.depreciation = depreciation
                asset_line.accumulated_depreciation = (
                    self.get_depreciated_amount() + acc_depreciation)
        else:
            if residual_value > 0 and asset_line is not None:
                asset_line.depreciation += residual_value
                asset_line.accumulated_depreciation += residual_value
        for asset_line in amounts.values():
            asset_line.actual_value = (self.value -
                asset_line.accumulated_depreciation)

        return amounts

    def get_move(self, line):
        """
        Return the account.move generated by an asset line.
        """
        pool = Pool()
        Period = pool.get('account.period')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')

        period_id = Period.find(self.company.id, line.date)
        with Transaction().set_context(date=line.date):
            expense_line = MoveLine(
                credit=0,
                debit=line.depreciation,
                account=self.debit_account,
            )
            depreciation_line = MoveLine(
                debit=0,
                credit=line.depreciation,
                account=self.credit_account,
            )

        return Move(
            company=self.company,
            origin=line,
            period=period_id,
            journal=self.account_journal,
            date=line.date,
            lines=[expense_line, depreciation_line],
        )

    @classmethod
    @ModelView.button
    def update_depreciation(cls, assets):
        Line = Pool().get('account.asset.line')
        Config = Pool().get('asset.configuration')
        config = Config.search(None)[0]
        print(config)
        for i in assets:
            lines = []
            actual_value = i.value
            accumulated_depreciation = 0
            for line in i.lines:
                dup = (i.value - i.residual_value) / i.total_units_production
                dup = dup.quantize(Decimal(str(10.0 ** -config.digits)))
                amount = (dup * line.units_productions)
                line.depreciation = amount.quantize(Decimal('0.01'))
                line.actual_value = (actual_value
                                    - amount).quantize(Decimal('0.01'))
                line.accumulated_depreciation = (accumulated_depreciation
                                    + amount).quantize(Decimal('0.01'))
                lines.append(line)
                actual_value = line.actual_value
                accumulated_depreciation = line.accumulated_depreciation
            Line.save(lines)

    @classmethod
    def create_moves(cls, assets, date):
        """
        Creates all account move on assets before a date.
        """
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.asset.line')

        moves = []
        lines = []
        for asset_ids in grouped_slice(assets):
            lines += Line.search([
                ('asset', 'in', list(asset_ids)),
                ('date', '<=', date),
                ('move', '=', None),
            ])
        for line in lines:
            if line.depreciation != 0:
                moves.append(line.asset.get_move(line))

        Move.save(moves)
        for move, line in zip(moves, lines):
            line.move = move
        Line.save(lines)


class CreateMovesStart(ModelView):
    'Create Moves Start'
    __name__ = 'account.asset.create_moves.start'
    to_open = fields.One2Many('account.move', None, 'Asientos')


class CreateMoves(metaclass=PoolMeta):
    'Create Moves'
    __name__ = 'account.asset.create_moves'

    open_moves = StateAction('account.act_move_form')

    def transition_create_moves(self):
        Asset = Pool().get('account.asset')
        assets = Asset.search([
            ('state', '=', 'running'),
        ])
        Asset.create_moves(assets, self.start.date)
        return 'open_moves'

    def do_open_moves(self, action):
        pool = Pool()
        AssetLine = pool.get('account.asset.line')
        assset_lines = AssetLine.search([
            ('date', '<=', self.start.date),
            ('move.state', '=', 'draft'),
        ])
        to_open = []
        for line in assset_lines:
            to_open.append(line.move.id)

        data = {'res_id': to_open}
        return action, data


class UpdateAsset(metaclass=PoolMeta):
    __name__ = 'account.asset.update'

    def default_show_move(self, fields):
        Asset = Pool().get('account.asset')
        asset = Asset(Transaction().context['active_id'])
        return {
            'amount': self.start.value - asset.value,
            'date': datetime.date.today(),
            'depreciation_account': asset.debit_account.id,
            'counterpart_account': asset.credit_account.id,
            'latest_move_date': self.get_latest_move_date(asset),
            'next_depreciation_date': self.get_next_depreciation_date(asset),
            }


class AssetUpdateValuesHeader(ModelSQL, ModelView):
    ' Assets Update Values Header'
    __name__ = 'asset.update.value.header'

    #asset_account = fields.Many2One('account.asset', 'Depreciaicion')
    aseet = fields.Many2One('asset','Activo',
            domain= [('state', '=', 'draft')])



class AssetUpdateValuesHeaderStart(ModelView):
    'Assets Update Values Header Start'
    __name__ = 'asset.update.value.header.start'


class AssetUpateValuesHeaderWizard(Wizard):
    'Assets Update Values Header Wizard'
    __name__ = 'asset.update.value.header.wizard'


    start = StateView('asset.update.value.header.start',
                      'asset_ec.asset_update_values_view_form', [
                          Button('Cancelar', 'end', 'tryton-cancel'),
                          Button('OK', 'change', 'tryton-ok', default=True),
                      ])
    change = StateTransition()

    def transition_change(self):
        AssetDepreciation = Pool().get('account.asset')
        asset_depreciation = AssetDepreciation(Transaction().context[
            'active_id'])
        Asset= Pool().get('asset')
        to_save = []
        for x in asset_depreciation.assets:
            aux = x.purchase_option_value
            aux1 = x.value
            aux2 = x.residual_value
            asset_depreciation.purchase_value = aux
            asset_depreciation.value = aux1
            asset_depreciation.residual_value =aux2
            to_save.append(asset_depreciation)
        AssetDepreciation.save(to_save)
        return 'end'



