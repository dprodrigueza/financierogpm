from trytond.pool import Pool
from trytond.tools import grouped_slice
from collections import defaultdict
import collections
import datetime
import re
from datetime import datetime
from calendar import different_locale, month_name, monthrange
from decimal import Decimal
from trytond.modules.hr_ec.company import CompanyReportSignature

__all__ = ['AssetReport', 'AssetLabelReport', 'AssetListReport',
           'AssetTransferReport', 'AssetIncidentReport', 'AssetOwnerReport',
           'AssetDataSheetReport', 'AssetOwnerDetailReport',
           'AssetDetailByOwner', 'AssetDepartment', 'IncomeAssignmentsWithdrawals',
           'ExpirationInsurancePoliciesReport', 'AssetByLocationReport','AssetSbye',
           'AssetInventory','AssetCustodians']

def get_month_name(month_no, locale):
    with different_locale(locale) as encoding:
        s = month_name[month_no]
        if encoding is not None:
            s = s.decode(encoding)
        return s

class AssetReport(CompanyReportSignature):
    __name__ = 'asset.detail.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetReport, cls).get_context(
            records, data)
        report_context['asset_manager'] = cls._asset_manager
        return report_context

    @classmethod
    def _asset_manager(cls):
        Config = Pool().get('asset.configuration')
        config = Config(1)
        return config.asset_manager and config.asset_manager.name or ''

    @classmethod
    def _journal_signatures(cls, journal_id, split_size=2):
        pool = Pool()
        Journal = pool.get('account.journal')
        journal = Journal(journal_id)
        signatures = []
        if journal_id:
            for signature in grouped_slice(journal.signatures, split_size):
                signatures_temp = []
                for sign in signature:
                    signatures_temp.append({
                        'name': sign.employee.party.name
                        if sign.employee else '',
                        'position': sign.position,
                    })
                while len(signatures_temp) < split_size:
                    signatures_temp.append({
                        'name': '',
                        'position': '',
                    })
                signatures.append(signatures_temp)
        return signatures


class AssetLabelReport(CompanyReportSignature):
    __name__ = 'asset.label.report'

    @classmethod
    def get_context(cls, records, data):
        context = super(AssetLabelReport, cls).get_context(records, data)

        codes_final = []
        c = 0
        codes_final.append([None, None, None])
        for x in records:
            if c == 3:
                c = 0
                codes_final.append([None, None, None])
            codes_final[len(codes_final) - 1][c] = x
            c = c + 1

        context['codes'] = codes_final
        return context

class AssetSbye(CompanyReportSignature):
    __name__ = 'asset.sbye.report'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        report_context = super(AssetSbye, cls).get_context(
            records, data)
        Asset = Pool().get('asset')
        ids = data['ids']
        dict_records = {}
        for id in ids:
            dict_assets= {}
            assets = Asset.search([('id', '=', id)])
            caracteristicas = ''
            observaciones = ''
            color = ('')
            codigo_contable = ''
            fecha_ingreso = ''
            fecha_comprobante = ''
            accumulated_depreciation = Decimal(0.0)
            date = ''
            material = ''
            dimension = ''
            partida = ''
            for row in assets:
                if row.debit_account:
                    partida = row.debit_account.budget_debit
                # if row.account_asset:
                #     for cod in row.account_asset:
                #         if cod.debit_account:
                #             codigo_contable = cod.debit_account.rec_name
                if row.input_date:
                    aux = row.input_date
                    fecha_ingreso = datetime.strftime(aux, '%m/%d/%y')
                if row.purchase_date:
                    aux = row.purchase_date
                    fecha_comprobante = datetime.strftime(aux, '%m/%d/%y')
                if len(str(row.observations).split('\n')) == 1:
                    observaciones = ''
                elif len(str(row.observations).split('\n')) > 1:
                    observaciones = str(row.observations).split('\n')[0]
                if len(str(row.observations).split('\n')) == 1:
                    caracteristicas = ''
                elif len(str(row.observations).split('\n')) > 1:
                    caracteristicas = str(row.observations).split('\n')[1]
                if row.total_value_depreciated:
                    accumulated_depreciation = row.total_value_depreciated
                if row.value:
                    valor_libros = row.value
                if row.to_the_date:
                    fecha = row.to_the_date
                    date = datetime.strftime(fecha, '%m/%d/%y')
                if row.actual_value:
                    valor_contable = row.actual_value
                if row.residual_value:
                    residual_value = row.residual_value
                if row.product:
                    if row.product.depreciable:
                        depreciacion = "SI"
                    else:
                        depreciacion = "NO"
                else:
                    depreciacion = "NO"
                if row.observations:
                    linea = row.observations
                    match = re.search(r'.*?COLOR = (\w+).*', linea)
                    match_ = re.search(r'.*?COLOR=(\w+).*', linea)
                    match_1 = re.search(r'.*?COLOR =(\w+).*', linea)
                    if match:
                        color = match.group(1)
                    elif match_:
                        color = match_.group(1)
                    elif match_1:
                        color = match_1.group(1)
                    match1 = re.search(r'.*?MATERIAL = (\w+).*', linea)
                    if match1:
                        material = 'Material: ' + match1.group(1)
                    match2 = re.search(r'.*?MATERIAL1 = (\w+).*', linea)
                    if match2:
                        material += ' Material1: '+match2.group(1)
                    match3 = re.search(r'.*?MATERIAL2 = (\w+).*', linea)
                    if match3:
                        material += ' Material2: '+match3.group(1)
                    aux = linea.replace("(CM)", "")
                    match4 = re.search(r'.*?ANCHO  = (\w+/\w+).*', aux)
                    match5 = re.search(r'.*?ANCHO  = (\w+).*', aux)
                    if match4:
                        dimension = 'Ancho: '+match4.group(1)
                    elif match5:
                        dimension = 'Ancho: '+ match5.group(1)
                    match6 = re.search(r'.*?ALTO  = (\w+/\w+).*', aux)
                    match7 = re.search(r'.*?ALTO  = (\w+).*', aux)
                    if match6:
                        dimension += ' Alto: '+match6.group(1)
                    elif match7:
                        dimension += ' Alto: '+match7.group(1)
                    match8 = re.search(r'.*?LARGO  = (\w+/\w+).*', aux)
                    match9 = re.search(r'.*?LARGO  = (\w+).*', aux)
                    if match8:
                        dimension += ' Largo: '+match8.group(1)
                    elif match9:
                        dimension += ' Largo: '+match9.group(1)
                    if row.attributes:
                        if row.attributes.get('COLOR'):
                            color = row.attributes.get('COLOR')
                        if row.attributes.get('ALTO'):
                            dimension = 'Alto: ' + row.attributes.get('ALTO')
                        if row.attributes.get('ANCHO'):
                            dimension += ' Ancho: ' + row.attributes.get('ANCHO')
                        if row.attributes.get('LARGO'):
                            dimension += ' Largo: ' + row.attributes.get('LARGO')
                        if row.attributes.get('MATERIAL'):
                            material = 'Material: ' + row.attributes.get('MATERIAL')
                        if row.attributes.get('MATERIAL1'):
                            material += ' Material1: ' + row.attributes.get('MATERIAL1')
                        if row.attributes.get('MATERIAL2'):
                            material += ' Material2: ' + row.attributes.get('MATERIAL2')
                dict_assets['accumulated_depreciation'] = Decimal(accumulated_depreciation).quantize(Decimal('0.01'))
                dict_assets['caracteristicas'] = caracteristicas
                dict_assets['observaciones'] = observaciones
                dict_assets['valor_libros'] = Decimal(valor_libros).quantize(Decimal('0.01'))
                dict_assets['dimension'] = dimension
                dict_assets['date'] = date
                dict_assets['material'] = material
                dict_assets['codigo_contable'] = codigo_contable
                dict_assets['partida'] = partida
                dict_assets['fecha_ingreso'] = fecha_ingreso
                dict_assets['fecha_comprobante'] = fecha_comprobante
                dict_assets['color'] = color
                dict_assets['depreciacion'] = depreciacion
                dict_assets['serie'] = row.serial
                dict_assets['valor'] = Decimal(row.purchase_option_value).quantize(Decimal('0.01'))
                dict_assets['vida_util'] = row.life_time
                dict_assets['tipo_activo'] = row.asset_type_translated
                dict_assets['estado_activo'] = row.asset_state_translated
                dict_assets['tipo'] = row.type
                dict_assets['empleado'] = row.employee.party.name if row.employee else ''
                dict_assets['marca'] = row.brand
                dict_assets['codigo'] = row.code
                dict_assets['modelo'] = row.model
                dict_assets['valor_residual'] = residual_value.quantize(Decimal('0.01'))
                dict_assets['valor_contable'] = Decimal(valor_contable).quantize(Decimal('0.01'))
                dict_assets['identificador'] = row.sbye
                dict_records[id] = dict_assets
        report_context['records'] = dict_records
        return report_context


class AssetCodeQRReport(CompanyReportSignature):
    __name__ = 'asset.code.qr.report'

class AssetListReport(CompanyReportSignature):
    __name__ = 'asset.list.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetListReport, cls).get_context(
            records, data)
        report_context['asset_manager'] = cls._asset_manager
        return report_context

    @classmethod
    def _asset_manager(cls):
        Config = Pool().get('asset.configuration')
        config = Config(1)
        return config.asset_manager and config.asset_manager.name or ''


class AssetTransferReport(CompanyReportSignature):
    __name__ = 'asset.transfer.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetTransferReport, cls).get_context(
            records, data)
        report_context['asset_manager'] = cls._asset_manager
        return report_context

    @classmethod
    def _asset_manager(cls):
        Config = Pool().get('asset.configuration')
        config = Config(1)
        return config.asset_manager


class AssetIncidentReport(CompanyReportSignature):
    __name__ = 'asset.incident'


class AssetDerecognition(CompanyReportSignature):
    __name__ = 'asset.derecognition'


class AssetOwnerReport(CompanyReportSignature):
    __name__ = 'asset.owner.report'

    @classmethod
    def get_context(cls, records, data):
        record = records[0]
        pool = Pool()
        asset_owner = pool.get('asset.owner')
        records = asset_owner.search([
            ('employee', '=', record.employee),
            ('through_date', '=', None),
        ])
        report_context = super(AssetOwnerReport, cls).get_context(records, data)
        return report_context


class AssetDataSheetReport(CompanyReportSignature):
    __name__ = 'asset.data.sheet.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetDataSheetReport, cls).get_context(
            records, data)
        Asset = Pool().get('asset')
        assets = Asset.search([
            ('id', 'in', data['ids']),
        ])
        amount = Decimal('0.0')
        if assets:
            for row in assets:
                for aux in row.maintenances:
                    if aux.product_list:
                        for line in aux.product_list:
                            if line.moves:
                                for mov in line.moves:
                                    amount += mov.product.cost_price
        report_context['amount'] = amount.quantize(Decimal("0.00"))
        report_context['asset_manager'] = cls._asset_manager
        return report_context

    @classmethod
    def _asset_manager(cls):
        Config = Pool().get('asset.configuration')
        config = Config(1)
        return config.asset_manager and config.asset_manager.name or ''


class AssetOwnerDetailReport(CompanyReportSignature):
    __name__ = 'asset.owner.detail.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetOwnerDetailReport, cls).get_context(
            records, data)
        depreciation, total, total_value = cls.get_depreciation_info(records)
        categories = cls.grouped_categories(records)
        report_context['depreciation'] = depreciation
        report_context['categories'] = categories
        report_context['total'] = total
        report_context['total_value'] = total_value
        return report_context

    @classmethod
    def get_depreciation_info(cls, records):

        total = Decimal(0.0)
        total_value = Decimal(0.0)
        pool = Pool()
        Date = pool.get('ir.date')
        depreciation = defaultdict(lambda: None)
        today = Date.today()
        if records:
            for record in records:
                aux = None
                if record.account_asset:
                    if record.account_asset.frequency == 'monthly':
                        if record.account_asset.lines:
                            lines = list(record.account_asset.lines)
                            lines.sort(key=lambda x: x.date)
                            result = [v for x, v in enumerate(lines) if v.move]
                            if result:
                                try:
                                    aux = result[-1]
                                except:
                                    pass
                            else:
                                #aux = lines[-1]
                                pass
                    if record.account_asset.frequency == 'yearly':
                        if record.account_asset.lines:
                            lines = list(record.account_asset.lines)
                            lines.sort(key=lambda x: x.date)
                            size_list = len(lines)
                            aux = lines[size_list - 1]

                if aux:
                    purchase_value = 0
                    value_b = 0
                    value_acu = 0
                    if record.purchase_option_value:
                        purchase_value = record.purchase_option_value
                    if record.value:
                        value_b = record.value
                    value_acu = purchase_value - value_b
                    total += round((aux.accumulated_depreciation + value_acu),
                                   2)
                    total_value += aux.actual_value

                    depreciation[record.id] = {
                        'date': aux.date,
                        'value': aux.accumulated_depreciation + value_acu,
                        'value_actual': aux.actual_value,
                    }
                if not record.account_asset:
                    purchase_value = 0
                    residual_value = 0
                    if record.purchase_option_value:
                        purchase_value = record.purchase_option_value
                    if record.residual_value:
                        residual_value = record.residual_value

                    total += round((purchase_value - residual_value), 2)
                    total_value += residual_value
                    depreciation[record.id] = {
                        'date': record.purchase_date,
                        'value':0,
                            #(purchase_value - residual_value),
                        'value_actual': 0#residual_value,
                    }

        return depreciation, total, total_value

    @classmethod
    def grouped_categories(cls, records):
        data = {}
        for assets in records:
            account = assets.asset_account
            value = 0
            if assets.purchase_option_value:
                value = assets.purchase_option_value
            if not data.get(account, None):
                data.update({account:value})
            else:
                data[account]+= value
        return data


class AssetInsuranceDetailReport(CompanyReportSignature):
    __name__ = 'asset.insurance.detail.report'


class AssetDetailByOwner(CompanyReportSignature):
    __name__ = 'asset.detail.owner.report'


class AssetDepartment(CompanyReportSignature):
    __name__ = 'asset.department.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetDepartment, cls).get_context(
            records, data)
        report_context['get_assets'] = cls._get_assets
        report_context['get_from_date'] = cls._get_from_date
        return report_context

    @classmethod
    def _get_assets(cls, department):
        pool = Pool()
        Asset = pool.get('asset')
        assets = Asset.search([
            ('employee_department', '=', department.id)
        ])
        return assets

    @classmethod
    def _get_from_date(cls, asset):
        result = ""
        if asset.current_asset_owner:
            result = asset.current_asset_owner.from_date
        return result


class IncomeAssignmentsDerecognition(CompanyReportSignature):
    __name__ = 'asset.income.assigments.derecognition.report'


class AssetAccountReport(CompanyReportSignature):
    __name__ = 'asset.account.report'

    @classmethod
    def get_context(cls, records, data):
        actual_value = 0
        depreciation = 0
        accumulated_depreciation = 0
        pending_depreciation = 0
        total_value_depreciated = 0.0
        residaul_value =0
        total_purchase_value = 0
        dict_result = defaultdict(lambda: {})
        for record in records:
            for asset in record.assets:
                actual = cls._get_actual_totals(asset, record)
                dict_result[asset.id] = actual
                actual_value += actual['actual_value']
                depreciation += actual['depreciation']
                accumulated_depreciation += actual['accumulated_depreciation']
                pending_depreciation += actual['pending_depreciation']
                total_value_depreciated += float(actual[
                    'total_value_depreciated'])
                residaul_value += float(actual['residaul_value'])
                total_purchase_value += float(actual['total_purchase_value'])




        totals = {
            'actual_value': actual_value,
            'depreciation': depreciation,
            'accumulated_depreciation': accumulated_depreciation,
            'pending_depreciation': pending_depreciation,
            'total_value_depreciated':total_value_depreciated,
            'residaul_value':residaul_value,
            'total_purchase_value':total_purchase_value,

        }

        report_context = super(AssetAccountReport, cls).get_context(records, data)
        report_context['dict_result'] = dict_result
        report_context['totals'] = totals
        return report_context

    @classmethod
    def _get_pending_depreciation(cls, asset):
        AccountAssetLine = Pool().get('account.asset.line')
        Date = Pool().get('ir.date')
        today = Date.today()
        asset_lines = AccountAssetLine.search_read([
            ('asset', '=', asset),
            ('date', '>', today),


        ], fields_names=['depreciation'],
            order=[('date', 'DESC')])
        amount = 0
        if asset_lines:
            for line in asset_lines:
                amount += line['depreciation']
            return amount
        return amount

    @classmethod
    def _get_actual_totals(cls, asset, record):
        AccountAssetLine = Pool().get('account.asset.line')
        Date = Pool().get('ir.date')
        today = Date.today()
        asset_lines = AccountAssetLine.search_read([
            ('asset', '=', record),
            ('date', '<=', today),
            ('move', '!=', ' '),
        ], fields_names=['actual_value', 'depreciation',
                         'accumulated_depreciation'], order=[('date', 'DESC')])
        pending_depreciation = cls._get_pending_depreciation(record)
        if asset_lines:
            line = asset_lines[0]
            return {'asset': asset,
                    'start_date': (
                        record.start_date if record.start_date else ''),
                    'end_date': (record.end_date if record.end_date else ''),
                    'debit_account': (record.debit_account.rec_name
                                      if record.debit_account else ''),
                    'credit_account': (record.credit_account.rec_name
                                       if record.credit_account else ''),
                    'actual_value': line['actual_value'],
                    'depreciation': line['depreciation'],
                    'accumulated_depreciation': line['accumulated_depreciation'],
                    'pending_depreciation': pending_depreciation,
                    'total_value_depreciated':asset.total_value_depreciated,
                    'residaul_value':asset.residual_value,
                    'total_purchase_value':asset.purchase_option_value,
                    }
        return {'asset': asset,
                'start_date': (
                        record.start_date if record.start_date else ''),
                'end_date': (record.end_date if record.end_date else ''),
                'debit_account': (record.debit_account.rec_name
                                      if record.debit_account else ''),
                'credit_account': (record.credit_account.rec_name
                                       if record.credit_account else ''),
                'actual_value': 0,
                'depreciation': 0,
                'accumulated_depreciation': 0,
                'pending_depreciation': pending_depreciation,
                'total_value_depreciated': asset.total_value_depreciated,
                'residaul_value': asset.residual_value,
                'total_purchase_value':asset.purchase_option_value,

                }


class ExpirationInsurancePoliciesReport(CompanyReportSignature):
    __name__ = 'asset.expiration.insurance.policies.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ExpirationInsurancePoliciesReport, cls).get_context(records, data)
        
        open_insurance = []
        for x in records:
            if x.state == 'open':
                open_insurance.append(x)
        if open_insurance:
            report_context['empty'] = False
        else:
            report_context['empty'] = True
        report_context['ensures'] = open_insurance
        
        return report_context

class AssetByLocationReport(CompanyReportSignature):
    __name__ = 'asset.by.location.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetByLocationReport, cls).get_context(records, data)
        report_context['locations']= cls._get_location()
        return report_context

    @classmethod
    def _get_location(cls):
        AssetLocation = Pool().get('asset.location')
        assetlocation = AssetLocation.search([])
        location = {}
        for x in assetlocation:
            if not x.location.name in location.keys():
                location[x.location.name] = []
                location[x.location.name].append([])
                location[x.location.name].append(0.0)
            location[x.location.name][0].append(x)
        for x in location:
            new_list = sorted(location[x][0], key=lambda x: x.asset.name)
            location[x][0] = new_list
        location_sorted = collections.OrderedDict(sorted(location.items(), key=lambda x: x))
        
        # s_t = Decimal(0.0)
        # for key in location_sorted:
        #     s = Decimal(0.0)
        #     for l in location_sorted[key][0]:
        #         s = s + Decimal(l.asset.actual_value)
        #     s_t = s_t + s
        #     location_sorted[key][1] = s
        # return location_sorted, s_t
        return location_sorted

class AssetCertificate(CompanyReportSignature):
    __name__ = 'asset.certificate'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Employee = pool.get('company.employee')
        report_context = super(
            AssetCertificate, cls).get_context(records, data)
        report_context['owner'], report_context['party'] = \
            cls.search_employee(data)
        employee = Employee.search([
            ('party','=',report_context['party'])
        ])
        report_context['today'] = datetime.today().strftime("%Y/%m/%d")
        if employee:
            report_context['employee'] = employee[0]
        else:
            report_context['employee'] = ""
        return report_context

    @classmethod
    def search_employee(cls, data):
        pool = Pool()
        Owner = pool.get('asset.owner')
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        owner = Owner.search([
            ('owner','=',data['owner']),
            ('through_date', '=', None)
        ])
        if owner:
            is_owner = False
        else:
            is_owner = True
        party, = Party.search([
            ('id', '=', data['owner']),
        ])
        return  is_owner, party


class AssetReportByCostCenter(CompanyReportSignature):
    __name__ = 'asset.by.cost.center.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(AssetReportByCostCenter, cls).get_context(
            records, data)
        depreciation = cls.get_depreciation_info(records)
        categories = cls.grouped_categories(records)
        report_context['depreciation'] = depreciation
        report_context['categories'] = categories
        return report_context

    @classmethod
    def get_depreciation_info(cls,records):

        pool = Pool()
        Date = pool.get('ir.date')
        depreciation = defaultdict(lambda: None)
        today = Date.today()
        if records:
            for record in records:
                aux = None
                if record.account_asset:
                    if record.account_asset.frequency =='monthly':
                        if record.account_asset.lines:
                            lines = list(record.account_asset.lines)
                            lines.sort(key=lambda x: x.date)
                            is_found= False
                            position = 0
                            for count,line in enumerate(lines):
                                if today <= line.date:
                                    is_found= True
                                    position=count
                                    break
                            if is_found:
                                try:
                                    aux=lines[position-1]
                                except:
                                    pass
                            else:
                                aux=lines[-1]
                    if record.account_asset.frequency =='yearly':
                        if record.account_asset.lines:
                            lines = list(record.account_asset.lines)
                            lines.sort(key=lambda x: x.date)
                            size_list= len(lines)
                            aux=lines[size_list-1]
                if aux:
                    depreciation[record.id]={
                        'date':aux.date,
                        'value':aux.accumulated_depreciation,
                        'value_actual':aux.actual_value,
                    }
                if not record.account_asset:
                    purchase_value=0
                    residual_value=0
                    if record.purchase_option_value:
                        purchase_value = record.purchase_option_value
                    if record.residual_value:
                        residual_value = record.residual_value
                    depreciation[record.id] = {
                        'date': record.purchase_date,
                        'value':
                            (purchase_value -residual_value),
                        'value_actual': residual_value,
                    }

        return depreciation

    @classmethod
    def grouped_categories(cls, records):
        data = {}
        for assets in records:
            account = assets.analytic_accounts
            value = 0
            if assets.purchase_option_value:
                value = assets.purchase_option_value
            if not data.get(account, None):
                data.update({account:value})
            else:
                data[account]+= value
        return data


class AssetInventory(CompanyReportSignature):
    __name__ = 'asset.inventory'

class AssetCustodians(CompanyReportSignature):
    __name__ = 'asset.custodian'
