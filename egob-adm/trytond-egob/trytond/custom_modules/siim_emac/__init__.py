# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import company
from . import hr
from . import treasury
from . import project
from . import configuration
from . import report
from . import account
from . import invoice
from . import labor_ministry
from . import sri
from . import stock
from . import medical_certificate
from . import asset

__all__ = ['register']


def register():
    Pool.register(
        company.ActionOfTheStaff,
        company.CommissionSubrogation,
        company.EmployeeBurden,
        hr.PermissionBalance,
        hr.OvertimeWorkhiftHeader,
        hr.OvertimeWorkhiftHeaderContext,
        hr.EmployeeLoan,
        hr.Overtime,
        hr.OvertimeLine,
        hr.Permission,
        hr.PermissionType,
        hr.PermissionTypeIntervalTimeConfiguration,
        hr.UploadLawBenefitStart,
        hr.PayslipGeneral,
        hr.PermissionConsulting,
        invoice.Invoice,
        treasury.AccountPaymentSPI,
        project.Work,
        project.PublicProjectLocation,
        project.MoveDeliverablesStart,
        project.PublicProjectRisk,
        project.PublicProjectOportunity,
        stock.ShipmentIn,
        stock.ShipmentInternal,
        configuration.WorkConfiguration,
        configuration.WorkConfigurationMatrixTemplate,
        account.Period,
        labor_ministry.LaborMinistryXIIIReportWizardStart,
        labor_ministry.LaborMinistryXIVReportWizardStart,
        sri.Form107,
        sri.FixForm107WizardStart,
        sri.FixForm107WizardNext,
        sri.FixForm107WizardSucceed,
        medical_certificate.MedicalCertificate,
        asset.Asset,
        module='siim_emac', type_='model')
    Pool.register(
        company.CommissionSubrogationChangeEndDate,
        company.UploadDailingRegister,
        sri.FixForm107Wizard,
        module='siim_emac', type_='wizard')
    Pool.register(
        report.LoanOneFee,
        report.ReportContractLiquidationEmac,
        module='siim_emac', type_='report')
