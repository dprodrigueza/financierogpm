from trytond.pool import PoolMeta
from trytond.model import Workflow, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['ShipmentIn', 'ShipmentInternal']


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    to_location = fields.Many2One('stock.location', 'Ubicación Destino',
        required=True,
        states={'readonly': (~Eval('state').in_(
          ['draft']))
              }, )

    @classmethod
    @ModelView.button
    @Workflow.transition('received')
    def receive(cls, shipments):
        Move = Pool().get('stock.move')
        super(ShipmentIn, cls).receive(shipments)
        for shipment in shipments:
            to_save = []
            for line in shipment.inventory_moves:
                line.to_location = shipment.to_location
                to_save.append(line)
            Move.save(to_save)


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    party_barter = fields.Many2One('party.party', 'Cliente',
        states={
           'invisible': (Eval(
               'reason_name') != 'TRUEQUE AMBIENTAL'),
           'required': ~(Eval(
               'reason_name') != 'TRUEQUE AMBIENTAL')

        }, depends=['reason_name'])