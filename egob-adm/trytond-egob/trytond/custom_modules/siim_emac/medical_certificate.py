from datetime import timedelta, datetime, date

from trytond.pool import Pool, PoolMeta
from trytond.pyson import If, Eval

__all__ = ['MedicalCertificate']


class MedicalCertificate(metaclass=PoolMeta):
    __name__ = 'galeno.medical.certificate'

    @classmethod
    def __setup__(cls):
        super(MedicalCertificate, cls).__setup__()
        cls.evaluation.states.update({
            'required': If(Eval('kind')=='internal', True, False),
        })
        cls._error_messages.update({
            'period_not_found_med_cert_ranges': ('No es posible registrar el '
                'certificado médico debido a que no se ha encontrado en ningún '
                'período contable los rangos de fechas disponibles para '
                'ingresar un certificado médico con fecha de inicio del '
                '%(date)s.'),
            'medical_certificate_date_error': ('El período de registro de '
                'certificados médicos que abarcan certificados con fecha de '
                'inicio %(date)s ha terminado. Actualmente, las fechas de '
                'inicio válidas están entre %(cp_start)s y %(cp_end)s.'),
            'certificate_date_overlaps_overtime_line': ('No es posible '
                'pasar a REALIZADO el certificado médico de %(employee)s por '
                '%(certificate_type)s %(certificate_date)s ya que este se '
                'sobrepone a la hora extra realizada el %(overtime_date)s la '
                'cual se encuentra en estado %(overtime_state)s.\n\nRecuerde, '
                'no es posible registrar certificados médicos en la misma '
                'fecha y hora en la que un empleado ha realizado horas extra.'),
        })

    @classmethod
    def validate(cls, medical_certificates):
        for certificate in medical_certificates:
            certificate.check_enabled_period()
        super(MedicalCertificate, cls).validate(medical_certificates)

    @classmethod
    def done(cls, certificates):
        cls.check_overtime_same_day(certificates)
        super(MedicalCertificate, cls).done(certificates)

    def check_enabled_period(self):
        '''
        It is verified that the date of registration of the medical certificate
        belongs to the period of registration of medical certificates configured
        in account.period
        '''
        pool = Pool()
        Period = pool.get('account.period')

        today = datetime.today().date()
        mc_start_date = self.start_date

        current_periods = Period.search([
            ('medical_certificate_start_date', '<=', today),
            ('medical_certificate_end_date', '>=', today),
        ], order=[
            ('medical_certificate_start_date', 'ASC'),
            ('medical_certificate_end_date', 'ASC')])
        if not current_periods:
            self.raise_user_error('period_not_found_med_cert_ranges', {
                'date': today
            })

        cp_start_date = current_periods[0].medical_certificate_start_date
        cp_end_date = current_periods[0].medical_certificate_end_date

        if not (cp_start_date <= mc_start_date <= cp_end_date):
            self.raise_user_error('medical_certificate_date_error', {
                'date': mc_start_date,
                'cp_start': cp_start_date,
                'cp_end': cp_end_date,
            })

    @classmethod
    def check_overtime_same_day(cls, certificates):
        '''
        It is verified that there are no overtime hours performed by the
        employee on the same date as the medical certificate
        '''
        OvertimeLine = Pool().get('hr_ec.overtime.line')
        for mc in certificates:
            overtime_lines = OvertimeLine.search([
                ('overtime_date', '>=', mc.start_date),
                ('overtime_date', '<=', mc.end_date),
                ('state', '=', 'valid'),
                ('overtime.employee', '=', mc.employee),
                ('overtime.state', 'not in', ['draft', 'cancel']),
            ])
            for ol in overtime_lines:
                is_overlap = False
                if mc.start_date <= ol.overtime_date <= mc.end_date:
                    if mc.register_type == 'hours':
                        is_overlap = cls.is_overlap_time(mc, ol)
                    else:
                        is_overlap = True
                if is_overlap:
                    ct = f'{mc.certificate_type.name.upper()}'
                    od = f'[{ol.overtime_date}, {ol.start_time} - {ol.end_time}]' #noqa
                    os = f'{ol.overtime.state_translated.upper()}'
                    if mc.register_type == 'hours':
                        cd = f'[{mc.start_date}, {mc.start_time} - {mc.end_time}]' #noqa
                    else:
                        cd = f'[{mc.start_date} - {mc.end_date}]'

                    cls.raise_user_error(
                        'certificate_date_overlaps_overtime_line', {
                            'employee': mc.employee.rec_name.upper(),
                            'certificate_type': ct,
                            'certificate_date': cd,
                            'overtime_date': od,
                            'overtime_state': os,
                        })

    @classmethod
    def is_overlap_time(cls, certificate, overtime_line):
        mc = certificate
        ol = overtime_line
        if (mc.start_date == ol.overtime_date) and (
            (mc.start_time <= ol.start_time <= mc.end_time <= ol.end_time) or
            (ol.start_time <= mc.start_time <= mc.end_time <= ol.end_time) or
            (ol.start_time <= mc.start_time <= ol.end_time <= mc.end_time) or
            (mc.start_time <= ol.start_time <= ol.end_time <= mc.end_time)):
            return True
        return False