from trytond.model import fields

__all__ = ['PublicGenericMixin']

class PublicGenericMixin(object):
    name = fields.Char('Name', required=True)
    description = fields.Text('Description')
