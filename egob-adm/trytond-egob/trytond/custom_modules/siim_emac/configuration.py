from trytond.model import ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.modules.company.model import (CompanyValueMixin)


__all__ = ['WorkConfiguration', 'WorkConfigurationMatrixTemplate']


class WorkConfiguration(metaclass=PoolMeta):
    __name__ = 'public.project.configuration'

    matrix_information_template = fields.MultiValue(fields.Binary(
        'Plantilla de información matriz de riesgos y oportunidades'
    ))
    matrix_information_template_filename = fields.MultiValue(fields.Binary(
        'PlantillaInformaciónMatrizRiesgosOportunidades'
    ))
    matrix_information_template_path = fields.MultiValue(fields.Binary(
        'Ruta de ubicación'
    ))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if (field == 'matrix_information_template' or
            field == 'matrix_information_template_filename' or
            field == 'matrix_information_template_path'):
            return pool.get(
                'public.project.configuration.matrix_information_template')
        return super(WorkConfiguration, cls).multivalue_model(field)


class WorkConfigurationMatrixTemplate(ModelSQL, CompanyValueMixin):
    "Work Configuration Matrix Information Template"
    __name__ = 'public.project.configuration.matrix_information_template'

    matrix_information_template = fields.Binary(
        'Plantilla  de información de matriz de riesgos y oportunidades',
        filename='matrix_information_template_filename',
        file_id='matrix_information_template_path')
    matrix_information_template_filename = fields.Char(
        'PlantillaInformaciónMatrizRiesgosOportunidades',
        states={
            'invisible': True,
        })
    matrix_information_template_path = fields.Char(
        'Ruta de ubicación', readonly=True)
