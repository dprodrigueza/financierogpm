from decimal import Decimal
from collections import defaultdict
import functools
from sql import Null
from sql.aggregate import Sum
from sql.conditionals import Coalesce
from trytond.model import fields, Workflow, ModelView, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['Invoice']

_ZERO = Decimal('0.0')
CENT = Decimal('0.01')
class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate_invoice(cls, invoices):
        super(Invoice, cls).validate_invoice(invoices)
        for invoice in invoices:
            summary_budget = cls.get_budget_summary(invoices)
            cls.validate_budget_values(invoices, summary_budget)
            # cls.get_budget_summary(invoices)
    @classmethod
    def get_budget_summary(cls, invoices, comparation=False):
        pool = Pool()
        base = defaultdict(lambda: None)
        taxes = defaultdict(lambda: None)

        total_tax = functools.reduce(
            lambda a, b: a + b,
            [i.tax_amount for i in invoices])
        result = {}
        for invoice in invoices:
            for line in invoice.lines:
                for row in line.accounts:
                    amount_tax = _ZERO
                    for tax in line.taxes:
                        amount_tax += tax.rate * row.amount
                    if row.budget:
                        if base[row.budget.id]:
                            base[row.budget.id] += row.amount
                        else:
                            base[row.budget.id] = row.amount
                        if taxes[row.budget.id]:
                            taxes[row.budget.id] += amount_tax
                        else:
                            taxes[row.budget.id] = amount_tax
                    else:
                        cls.raise_user_error('without_budget', {
                            'line': line.rec_name,
                                })
            values = {}
            values['base'] = base
            values['tax'] = taxes
            result[invoice.id] = values
        return result

    @classmethod
    def validate_budget_values(cls, invoices, summary_budget):
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        pool = Pool()

        Budget = pool.get('public.budget')
        Certificate = pool.get('public.budget.certificate')
        CertificateLine= pool.get('public.budget.certificate.line')
        Invoice= pool.get('account.invoice')
        InvoiceLine= pool.get('account.invoice.line')
        InvoiceLineAccount= pool.get('account.invoice.line.account')
        Modifier= pool.get('public.budget.certificate.modifier')
        ModifierLine= pool.get('public.budget.certificate.modifier.line')
        certificate = Certificate.__table__()
        certificate_line = CertificateLine.__table__()
        invoice_tbl = Invoice.__table__()
        invoice_line = InvoiceLine.__table__()
        line_account = InvoiceLineAccount.__table__()
        modifier = Modifier.__table__()
        modifier_line = ModifierLine.__table__()

        for invoice in invoices:
            if invoice.certificate is None:
                cls.raise_user_error('without_certificate')
            keys = summary_budget.get(invoice.id).get('base').keys()
            budgets = summary_budget.get(invoice.id).get('base')
            taxes = summary_budget.get(invoice.id).get('tax')
            certificate_invoice = invoice.certificate
            budgets_certificate = defaultdict(lambda: None)
            query = certificate.join(certificate_line,
                 condition=certificate.id == certificate_line.certificate
            ).select(
                certificate_line.budget.as_('budget'),
                certificate_line.amount.as_('amount'),
                where=(certificate_line.certificate == certificate_invoice.id),
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            for budget, amount in result:
                if budgets_certificate.get(budget) is None:
                    budgets_certificate[budget] = amount
            query_reforms = certificate_line.join(modifier_line,
                condition=modifier_line.certificate_line == certificate_line.id
            ).join(modifier,
                condition=((modifier_line.modifier == modifier.id) &
                      (modifier.date <= invoice.invoice_date) &
                      (modifier.state == 'done')
                ),
            ).select(
                certificate_line.budget.as_('budget'),
                Coalesce(Sum(modifier_line.amount),0).as_('reform'),
                where=(certificate_line.certificate == certificate_invoice.id),
                group_by= certificate_line.budget
            )
            cursor.execute(*query_reforms)
            result_reforms = cursor.fetchall()
            for budget, reform in result_reforms:
                budgets_certificate[budget] += reform
            keys_list = []
            for k in keys:
                keys_list.append(k)
                if budgets_certificate.get(k) is None:
                    cls.raise_user_error('without_budget', {    ##Partida no existente en certificacion
                        'line': Budget.search(['id', '=', k][0].rec_name),
                    })
            diffs = {}
            for k in keys:
                budget = budgets.get(k).quantize(CENT)
                tax = taxes.get(k).quantize(CENT)
                if budgets_certificate.get(k) < budget + tax:
                    diffs[k] = {}
                    diffs[k]['budget'] = Budget(k).code
                    diffs[k]['certificate'] = budgets_certificate.get(k)
                    diffs[k]['invoice'] = budget + tax
                    diffs[k]['diff'] = budgets_certificate.get(k) - (budget + tax)
            if len(diffs):
                message = ''
                for k,v in diffs.items():
                    message += 'Partida: ' + v.get('budget') + \
                        ' Valor certificado: ' + str(v.get('certificate')) + \
                        ' Valor en factura: ' + str(v.get('invoice')) + \
                        ' Diferencia: ' + str(v.get('diff')) + '\n'
                cls.raise_user_error('amount_less_certificate', {
                    'message': message,
                })
            query = invoice_tbl.join(invoice_line,
                    condition=invoice_line.invoice == invoice_tbl.id
                ).join(line_account,
                    condition=invoice_line.id == line_account.line
                ).select(
                    line_account.id.as_('id'),
                    where=(
                        (invoice_tbl.state.in_(['validated', 'posted', 'paid'])) &
                        (invoice_tbl.id != invoice.id) &
                        (invoice_tbl.certificate == certificate_invoice.id) &
                        (line_account.budget.in_(keys_list))
                    )
                )
            cursor.execute(*query)
            lines_related = InvoiceLineAccount.browse(\
                        [x[0] for x in cursor.fetchall()])
            lines_related_summary = defaultdict(lambda: None)
            for line_related in lines_related:
                budget_id = line_related.budget.id
                if lines_related_summary.get(budget_id) is None:
                    lines_related_summary[budget_id] = \
                        Decimal(line_related.amount).quantize(CENT)
                else:
                    lines_related_summary[budget_id] += \
                        Decimal(line_related.amount).quantize(CENT)
                if len(line_related.line.taxes) > 0:
                    current_taxes = line_related.line.taxes
                    for current_tax in current_taxes:
                        lines_related_summary[budget_id] = \
                            lines_related_summary[budget_id] + \
                            Decimal(line_related.amount * current_tax.rate).quantize(CENT)

                   # budgets_certificate[budget] += reform
            diffs = {}
            for k in keys:
                budget = budgets.get(k)
                tax = taxes.get(k).quantize(CENT)
                summary = lines_related_summary.get(k)
                sum = budget + tax
                if summary is not None :
                    sum += summary
                if budgets_certificate.get(k) < sum:
                    diffs[k] = {}
                    diffs[k]['budget'] = Budget(k).code
                    diffs[k]['certificate'] = budgets_certificate.get(k)
                    diffs[k]['invoice'] = sum
                    diffs[k]['diff'] = budgets_certificate.get(k) - (sum)
            if len(diffs):
                message = ''
                for k,v in diffs.items():
                    message += 'Partida: ' + v.get('budget') + \
                        ' Valor certificado: ' + str(v.get('certificate')) + \
                        ' Valor en factura: ' + str(v.get('invoice')) + \
                        ' Differencia: ' + str(v.get('diff')) + '\n'
                cls.raise_user_error('amount_less_certificate', {
                    'message': message,
                })
        return budgets