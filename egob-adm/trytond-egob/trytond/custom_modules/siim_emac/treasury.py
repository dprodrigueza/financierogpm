from trytond.pool import PoolMeta

__all__ = ['AccountPaymentSPI']


class AccountPaymentSPI(metaclass=PoolMeta):
    __name__ = 'treasury.account.payment.spi'

    def generate_provider(self, path='c:\SPI-2005\spi-sp.txt'):
        super(AccountPaymentSPI, self).generate_provider(path=path)