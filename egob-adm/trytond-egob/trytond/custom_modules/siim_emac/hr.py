from decimal import Decimal
from collections import defaultdict
import datetime as dt
from datetime import timedelta, datetime, time
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from sql import Literal, With, Values
from sql.aggregate import Sum
from sql.conditionals import Coalesce, Case
from sql.functions import CurrentTimestamp

from trytond.pool import PoolMeta, Pool
from trytond.model import (ModelView, ModelSQL, fields)
from trytond.pyson import Eval, Bool
from trytond.tools import cursor_dict
from trytond.transaction import Transaction
from trytond.config import config

from trytond.modules.hr_ec.company import DAYS_INFO
from trytond.modules.hr_ec.attendance_calculations import (DAYS_TRANSLATED,
    get_day)

import yearfrac as yf

__all__ = [
    'PermissionBalance', 'OvertimeWorkhiftHeader', 'Overtime', 'Permission',
    'OvertimeLine', 'OvertimeWorkhiftHeaderContext', 'EmployeeLoan',
    'UploadLawBenefitStart', 'PermissionType',
    'PermissionTypeIntervalTimeConfiguration', 'PayslipGeneral',
    'PermissionConsulting',
]


CENT = Decimal('0.01')

INTERVAL_HALF_WORKINGDAYS = {
    'first_half': 'Primera mitad de la jornada',
    'second_half': 'Segunda mitad de la jornada',
}


def is_collission_with_medical_certificates(employee, dates):
    MedicalCertificate = Pool().get('galeno.medical.certificate')
    result = {
        'is_collission': False,
        'date': None
    }
    if dates:
        dates.sort()
        med_certs = MedicalCertificate.search([
            ('employee', '=', employee),
            ('state', '=', 'done'),
            ('end_date', '>=', dates[0]),
            ('register_type', '=', 'days'),
        ])
        for date in dates:
            for mc in med_certs:
                if (mc.start_date <= date <= mc.end_date):
                    result['is_collission'] = True
                    result['date'] = date
    return result


def round_minutes_time(time_round, multiple=30):
    hour = time_round.hour
    minute = round(time_round.minute / Decimal(str(multiple))) * multiple
    if minute == 60:
        minute = 0
        hour = (hour + 1) if hour < 23 else 0
    return time(hour, minute, 0)


def sum_times(time1, time2):
    _datetime = datetime(year=1, month=1, day=1,
        hour=time1.hour,
        minute=time1.minute,
        second=time1.second)
    _timedelta = timedelta(
        hours=time2.hour,
        minutes=time2.minute,
        seconds=time2.second)
    _sum = _datetime + _timedelta
    _time_sum = time(hour=_sum.hour, minute=_sum.minute, second=_sum.second)
    return _time_sum


class PermissionBalance(metaclass=PoolMeta):
    __name__ = 'hr_ec.permission.balance'

    @classmethod
    def get_additional(cls, contract, end_date):
        # Defined in order to redefinition depending of each case
        if contract.work_relationship.additional_days_from:
            years = (yf.yearfrac(contract.start_date, end_date, '30e360'))
            years_int = int(years)
            total = 0
            last_value = 0
            for value in range((years_int -
                    contract.work_relationship.additional_days_from + 1) + 1):
                if value > 15:
                    last_value = 15
                else:
                    last_value = value
                total += last_value
            result = Decimal(str(total + (years - years_int) * last_value))
            return result
        return Decimal('0.0')

    @classmethod
    def get_additional_liquidation(cls, contract, end_date):
        if contract.work_relationship.additional_days_from:
            years = (yf.yearfrac(contract.start_date, end_date, '30e360'))
            last_value = 0
            for value in range((int(years) -
                    contract.work_relationship.additional_days_from + 1) + 1):
                if value > 15:
                    last_value = 15
                else:
                    last_value = value
            if (years - int(years)) == 0:
                return Decimal(str(last_value))
            else:
                start_date = contract.start_date
                actual_year = (f"{(end_date.year)}-{start_date.month}-"
                               f"{start_date.day}")

                initial_date = datetime.strptime(actual_year, '%Y-%m-%d').date()

                if initial_date > end_date:
                    string_date = (f"{(initial_date.year - 1)}-"
                                   f"{initial_date.month}-{initial_date.day}")
                    initial_date = datetime.strptime(
                        string_date, '%Y-%m-%d').date()

                result = (yf.yearfrac(initial_date, end_date, '30e360') *
                          (last_value))
                return Decimal(str(result))
        return Decimal('0.0')


class OvertimeWorkhiftHeader(ModelView, ModelSQL):
    'Overtime Header'
    __name__ = 'hr_ec.overtime.workshift.header'

    employee = fields.Many2One('company.employee', 'Empleado',
        readonly=True)
    department = fields.Many2One('company.department', 'Departamento',
        readonly=True)
    request_date = fields.Date('Fecha de solicitud', readonly=True)
    description = fields.Char('Descripción', readonly=True)
    overtime = fields.Many2One('hr_ec.overtime', 'Hora Extra',
        readonly=True)
    state = fields.Char('Estado', readonly=True)

    @classmethod
    def table_query(cls):
        context = Transaction().context
        pool = Pool()
        workshift_id = context.get('workshift')
        if not workshift_id:
            workshift_id = 0
        Overtime = pool.get('hr_ec.overtime')
        overtime = Overtime.__table__()
        Line = pool.get('hr_ec.overtime.line')
        line = Line.__table__()

        columns = [
            overtime.id,
            overtime.employee,
            overtime.department,
            overtime.request_date,
            overtime.description,
            overtime.id.as_('overtime'),
            overtime.state,
        ]

        where = (overtime.state == 'request') & \
            (line.workshift == workshift_id)
        query = line.join(overtime,
                condition=(
                    (line.overtime == overtime.id)
                )
            ).select(
            *columns + [
                Literal(0).as_('create_uid'),
                CurrentTimestamp().as_('create_date'),
                Literal(0).as_('write_uid'),
                CurrentTimestamp().as_('write_date')
            ],
            where=where,
            order_by=[overtime.request_date]
        )
        return query


class OvertimeWorkhiftHeaderContext(ModelView):
    'Overtime Header Workshift Context'
    __name__ = 'hr_ec.overtime.workshift.header.context'

    workshift = fields.Many2One('company.workshift', 'Horario')


class Overtime(metaclass=PoolMeta):
    __name__ = 'hr_ec.overtime'

    @classmethod
    def __setup__(cls):
        super(Overtime, cls).__setup__()
        cls._error_messages.update({
            'overtime_overlaps_medical_certificates': ('No es posible '
                'solicitar, confirmar ni aprobar la hora extra con fecha de '
                '%(ol_date)s ya que el empleado %(employee)s tiene un '
                'certificado médico registrado en la fecha mencionada.\n'
                'Para continuar, elimine la línea con conflicto o cancele todo '
                'el registro.'),
            'overtime_line_overlaps_permission': ('No es posible solicitar, '
                'confirmar, ni aprobar la hora extra [%(overtime_line)s] ya '
                'que el empleado %(employee)s tiene un permiso aprobado de '
                'tipo %(p_type)s en la misma fecha.\n\nPara continuar, usted '
                'puede eliminar la línea con conflicto; marcar la línea como '
                'inválida; o cancelar todo el registro.'),
        })

    @classmethod
    @ModelView.button
    def request(cls, overtimes):
        '''
        Do not request overtime when there is a permission or a medical
        certificate on the same date.
        '''
        cls.check_permissions_same_day(overtimes)
        cls.check_medical_certificates_same_day(overtimes)
        super(Overtime, cls).request(overtimes)

    @classmethod
    @ModelView.button
    def confirmed(cls, overtimes):
        '''
        Do not confirm overtime when there is a permission or a medical
        certificate on the same date.
        '''
        cls.check_permissions_same_day(overtimes)
        cls.check_medical_certificates_same_day(overtimes)
        super(Overtime, cls).confirmed(overtimes)

    @classmethod
    @ModelView.button
    def done(cls, overtimes):
        '''
        Do not approve overtime when there is a permission or a medical
        certificate on the same date.
        '''
        cls.check_permissions_same_day(overtimes)
        cls.check_medical_certificates_same_day(overtimes)
        super(Overtime, cls).done(overtimes)

    @classmethod
    def check_medical_certificates_same_day(cls, overtimes):
        Employee = Pool().get('company.employee')
        overtime_dates = defaultdict(lambda: [])
        for o in overtimes:
            if o.employee.id not in overtime_dates:
                overtime_dates[o.employee.id] = []
            for line in o.lines:
                if line.overtime_date not in overtime_dates[o.employee.id]:
                    overtime_dates[o.employee.id].append(line.overtime_date)
        for employee, dates in overtime_dates.items():
            result = is_collission_with_medical_certificates(employee, dates)
            if result['is_collission']:
                employee_record = Employee.search([
                    ('id', '=', employee)
                ])[0]
                cls.raise_user_error('overtime_overlaps_medical_certificates', {
                    'employee': employee_record.rec_name,
                    'ol_date': result['date']
                })

    @classmethod
    def check_permissions_same_day(cls, overtimes):
        Permission = Pool().get('hr_ec.permission')
        for o in overtimes:
            permissions = Permission.search([
                ('employee', '=', o.employee),
                ('kind', '=', 'days'),
                ('state', '=', 'done'),
            ])
            for p in permissions:
                for ol in o.lines:
                    if ol.state == 'valid':
                        if p.start_date <= ol.overtime_date <= p.end_date:
                            cls.raise_user_error(
                                'overtime_line_overlaps_permission', {
                                    'overtime_line': ol.rec_name,
                                    'employee': o.employee.rec_name,
                                    'p_type': p.type_.name.upper(),
                                })


class OvertimeLine(metaclass=PoolMeta):
    __name__ = 'hr_ec.overtime.line'

    @classmethod
    def __setup__(cls):
        super(OvertimeLine, cls).__setup__()
        cls.workshift.required = False
        cls.workshift.states.update({
            'required': True,
            'readonly': True,
        })
        cls._error_messages.update({
            'overtime_overlaps_medical_certificates': ('No puede registrar la '
                'hora extra con fecha de %(ol_date)s ya que el empleado '
                '%(employee)s tiene un certificado médico registrado en la '
                'fecha mencionada.\nPara continuar, elimine la línea con '
                'conflicto.'),
        })

    @classmethod
    def create(cls, vlist):
        '''
        No overtime should be created when there is a medical certificate on
        the same date.
        '''
        cls.check_medical_certificates_same_day(vlist)
        works = super(OvertimeLine, cls).create(vlist)
        return works

    @classmethod
    def check_medical_certificates_same_day(cls, vlist):
        Overtime = Pool().get('hr_ec.overtime')
        Employee = Pool().get('company.employee')
        overtime_dates = defaultdict(lambda: [])

        for v in vlist:
            readed_fields = Overtime.search_read([('id', '=', v['overtime'])],
                fields_names=['employee'])
            if readed_fields:
                employee = readed_fields[0]['employee']
                overtime_date = v['overtime_date']
                if employee not in overtime_dates:
                    overtime_dates[employee] = [overtime_date]
                else:
                    if overtime_date not in overtime_dates[employee]:
                        overtime_dates[employee].append(overtime_date)

        for employee, dates in overtime_dates.items():
            result = is_collission_with_medical_certificates(employee, dates)
            if result['is_collission']:
                employee_record = Employee.search([
                    ('id', '=', employee)
                ])[0]
                cls.raise_user_error('overtime_overlaps_medical_certificates', {
                        'employee': employee_record.rec_name,
                        'ol_date': result['date']
                    })

class EmployeeLoan(metaclass=PoolMeta):
    'Employee Loan'
    __name__ = 'company.employee.loan'

    @classmethod
    def __setup__(cls):
        super(EmployeeLoan, cls).__setup__()
        cls.end_date.required = True
        cls.total_amount.domain = [('total_amount', '>', 0.0),]
        cls.fee_number.states = {
            'readonly': (
                Bool(Eval('percentage_loan_type')) |
                (Eval('state') != 'draft')
            ),
        }
        cls.fee_amount.invisible = True

        cls.payment_capacity.help = ('Indica el valor máximo de cuota mensual '
            'que el empleado es capaz de pagar.\n\nRepresenta el ingreso '
            'promedio de los últimos 6 meses.')

        cls._order = [
            ('company', 'ASC'),
            ('employee', 'ASC'),
            ('start_date', 'DESC'),
        ]
        cls._transitions |= set((
            ('draft', 'open'),
            ('draft', 'cancel'),
            ('open', 'paid'),
            ('open', 'cancel'),
        ))
        cls._buttons.update(
            {
                'open': {
                    'invisible': Eval('state').in_(['open', 'cancel', 'paid']),
                },
                'cancel': {
                    'invisible': Eval('state').in_(['cancel', 'paid', 'open']),
                },
                'create_lines': {
                    'invisible': Eval('state').in_([
                        'request', 'open', 'cancel', 'paid']),
                },
                'advance_payment_button': {
                    'invisible': ~(Eval('state') == 'open'),
                },
                'adjustment_payslip_button': {
                    'invisible': ~(Eval('state') == 'open'),
                },
            })
        cls._error_messages.update({
            'other_loan_is_open': (
                '%(name)s tiene un prestamo pendiente de pago'),
            'lines_in_this_loan': ('Hay líneas para este prestamo'),
            'sum_lines_wrong': ('La suma de los montos capitales de cada cuota '
            'del préstamo del empleado "%(employee)s" no coincide con el '
                                'valor inicial prestado.'),
            'not_loan_lines': ('No hay registro de cuotas'),
            'fee_must_be_lower_salary': (
                'Las cuotas mensuales de %(fee_amount)s deben ser '
                'menores al salario'),
            'number_fee_cannot_max_fee': (
                'El # de cuótas no de deben exceder a %(fee)s cuotas'),
            'total_amount_lower_salary': (
                'El prestamo de %(contract)s se debe pagar en menos de %(fee)s '
                'cuotas debido a que la cantidad no supera el salario.'),
            'on_loan_employee_pending': (
                '%(employee)s tiene un prestamo pendiente de pago'),
            'exceed_maximum_amount': (
                'La cantidad no debe exceder el monto máximo %(maximum_amount)s'
            ),
        })

    @classmethod
    def view_attributes(cls):
        return [
            ('//label[@name="fee_amount"]', 'states', {
                'invisible': True,
            }),
        ]

    @classmethod
    def validate(cls, loans):
        super(EmployeeLoan, cls).validate(loans)
        for loan in loans:
            if loan.total_amount > loan.maximum_amount:
                cls.raise_user_error('exceed_maximum_amount', {
                'maximum_amount': loan.maximum_amount,
            })
            if loan.state != 'cancel':
                loan.check_amounts()

    def pre_validate(self):
        if self.state == 'draft':
            self.check_max_fee()
            self.check_total_amount_lower_salary()

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    @ModelView.button
    def create_lines(cls, loans):
        pool = Pool()
        LoanLine = pool.get('company.employee.loan.line')

        # Delete previous lines
        to_delete = []
        for loan in loans:
            to_delete += list(loan.lines)
        LoanLine.delete(to_delete)

        # Generate new lines
        for loan in loans:
            if loan.contract and loan.contract.work_relationship and (
                    loan.contract.work_relationship.discount_december) and (
                    loan.contract.work_relationship.percentage_december > 0):
                line_start_date = loan.start_date
                count = 0
                is_december = False
                while count < loan.fee_number:
                    line_start_date = line_start_date + (
                        relativedelta(day=31) if count == 0 else
                        relativedelta(months=1) + relativedelta(day=31))
                    if line_start_date.month == 12:
                        is_december = True
                        break
                    count += 1
                if is_december:
                    loan.generate_fee_december(
                        loan.id, loan.start_date, loan.end_date, loan.salary,
                        loan.contract, loan.total_amount)
                else:
                    loan.generate_fee(
                        loan.id, loan.start_date, loan.end_date,
                        loan.total_amount, loan.salary)
            else:
                loan.generate_fee(
                    loan.id, loan.start_date, loan.end_date, loan.total_amount,
                    loan.salary)

    def generate_fee(self, loan_id, loan_start_date, loan_end_date,
                     loan_total_amount, loan_salary, is_advance_payment=False):
        Line = Pool().get('company.employee.loan.line')
        to_create = []
        if not is_advance_payment:
            line_loan = Line.search(
                [('loan.id', '=', loan_id)]
            )
            if line_loan:
                self.raise_user_error('lines_in_this_loan')
        line_start_date = loan_start_date
        fees = relativedelta(loan_end_date, loan_start_date).months
        if relativedelta(loan_end_date, loan_start_date).years > 0:
            fees += relativedelta(loan_end_date, loan_start_date).years * 12
        fee_amount = (loan_total_amount / fees).quantize(
            Decimal("0.01"))
        if fee_amount > loan_salary:
            self.raise_user_error('fee_must_be_lower_salary', {
                'fee_amount': fee_amount,
            })
        count = 0
        lines_sum = 0
        while count < fees:
            line_start_date = line_start_date + (
                relativedelta(day=31) if count == 0 else
                relativedelta(months=1) + relativedelta(day=31))
            if count == fees - 1:
                fee_amount = loan_total_amount - lines_sum
            lines_sum += fee_amount
            to_create.append({
                'capital_amount': fee_amount,
                'start_date': line_start_date,
                'loan': loan_id,
                'state': 'pending',
            })
            count += 1

        if to_create:
            Line.create(to_create)

    def generate_fee_december(self, loan_id, loan_start_date, loan_end_date,
                              loan_salary, loan_contract, loan_total_amount,
                              is_advance_payment=False):
        to_create = []
        pool = Pool()
        EmployeeLoanLine = pool.get('company.employee.loan.line')
        if not is_advance_payment:
            line_loan = EmployeeLoanLine.search(
                [('loan.id', '=', loan_id)]
            )
            if line_loan:
                self.raise_user_error('lines_in_this_loan')
        line_start_date = loan_start_date
        fees = relativedelta(loan_end_date, loan_start_date).months
        if relativedelta(loan_end_date, loan_start_date).years > 0:
            fees += relativedelta(loan_end_date, loan_start_date).years * 12
        fee_amount_december = (
            (loan_salary *
             Decimal(loan_contract.work_relationship.percentage_december / 100)
             ).quantize(Decimal("0.01")))
        count = 0
        lines_sum = 0
        while count < fees:
            line_start_date = line_start_date + (
                relativedelta(day=31) if count == 0 else
                relativedelta(months=1) + relativedelta(day=31))
            if line_start_date.month == 12:
                fee_amount = fee_amount_december
            else:
                fee_amount = (
                    ((loan_total_amount - fee_amount_december) / (fees - 1)
                     ).quantize(Decimal("0.01")))
            if fee_amount > loan_salary:
                self.raise_user_error('fee_must_be_lower_salary', {
                    'fee_amount': fee_amount,
                })
            if count == fees - 1:
                fee_amount = loan_total_amount - lines_sum
            lines_sum += fee_amount
            if lines_sum > loan_total_amount:
                fee_amount = (loan_total_amount - lines_sum) + fee_amount
            if fee_amount > 0.0:
                to_create.append({
                    'capital_amount': fee_amount,
                    'start_date': line_start_date,
                    'loan': loan_id,
                    'state': 'pending',
                })
            count += 1
        if to_create:
            EmployeeLoanLine.create(to_create)

    def check_amounts(self):
        pass
        # TODO Verify this way of controlling whether the sum of the "capital
        # TODO amount" of the lines of a loan is equal to the field "amount"
        # TODO of the loan.
        # pool = Pool()
        # EmployeeLoanLine = pool.get('company.employee.loan.line')
        # Amounts = pool.get('company.employee.loan.amounts')
        # EmployeeLoanLine = pool.get('company.employee.loan.line')
        # amounts = Amounts.search([('loan', '=', self.id)])
        # line_loan = EmployeeLoanLine.search(
        #     [('loan', '=', self.id)]
        # )
        # if amounts and line_loan:
        #     if amounts[0].total_lines != self.total_amount:
        #         self.raise_user_error('sum_lines_wrong', {
        #             'employee': self.employee.rec_name
        #         })

    @fields.depends('type', 'fee_number', 'end_date', 'start_date')
    def on_change_type(self):
        if self.type and self.type.salary_percentage > Decimal("0"):
            self.fee_number = 1
            self.end_date = (
                    self.start_date + relativedelta(months=self.fee_number))
        else:
            self.fee_number = None

    @fields.depends('start_date', 'fee_number')
    def on_change_with_end_date(self, name=None):
        if self.start_date and self.fee_number:
            return self.start_date + relativedelta(months=self.fee_number)
        return None

    @classmethod
    def calculate_payment_capacity(cls, loan):
        '''
        Calculate the average income of the last 6 months before the calculation
        period. If the employee has not yet 6 months in the company, the months
        worked up to the calculation date are considered.
        '''
        Payslip = Pool().get('payslip.payslip')
        total_amount = 0
        num_payslips = 0
        if loan.start_date and loan.employee:
            # End date
            end_date = loan.start_date - relativedelta(months=1)
            last_day = monthrange(end_date.year, end_date.month)[1]
            end_date = datetime(
                year=end_date.year,
                month=end_date.month,
                day=last_day).date()

            # Start date
            start_date = loan.start_date - relativedelta(months=6)
            start_date = datetime(
                year=start_date.year,
                month=start_date.month,
                day=1).date()

            # Find payslips history
            payslips = Payslip.search([
                ('period.start_date', '>=', start_date),
                ('period.end_date', '<=', end_date),
                ('employee', '=', loan.employee),
                ('state', '=', 'done'),
                ('template.type', '=', 'payslip'),
            ])
            if payslips:
                num_payslips = len(payslips)
                for payslip in payslips:
                    total_amount += payslip.total_value

        # Payment capacity
        if num_payslips > 0:
            capacity = total_amount / Decimal(str(num_payslips))
        capacity = capacity if capacity > 0 else Decimal('0.00')
        return Decimal(str(capacity)).quantize(CENT)


class Permission(metaclass=PoolMeta):
    __name__ = 'hr_ec.permission'

    type_state = fields.Function(fields.Boolean('Estado de tipo'),
        'on_change_with_type_state')
    require_interval_time_config = fields.Function(
        fields.Boolean('Requiere intervalo de tiempos'),
        'on_change_with_require_interval_time_config')
    time_to_request = fields.Function(fields.Selection(
        'get_time_to_request_selection_values', 'Tiempo a solicitar',
        states={
            'invisible': (Eval('kind') == 'days') |
                ~Eval('require_interval_time_config') |
                    (Eval('state') != 'draft'),
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('require_interval_time_config'))
        }, depends=['kind', 'state', 'require_interval_time_config']),
        'get_time_to_request', setter='set_time_to_request')

    @classmethod
    def __setup__(cls):
        super(Permission, cls).__setup__()
        # Field 'employee'
        cls.employee.states['readonly'] |= Bool(Eval('type_state'))
        cls.employee.depends += ['type_state']
        # Field 'type_'
        cls.type_.states['readonly'] = Bool(Eval('type_state'))
        cls.type_.depends = ['type_state']
        # Field 'workshift'
        cls.workshift.states['readonly'] |= Bool(Eval('type_state'))
        cls.workshift.depends += ['type_state']
        # Field 'end_time'
        cls.end_time.states['readonly'] |= Eval('require_interval_time_config')
        cls.end_time.states['readonly'] |= Eval('time_to_request').in_(
            list(INTERVAL_HALF_WORKINGDAYS.keys()))
        cls.end_time.depends += [
            'time_to_request',
            'require_interval_time_config']
        # Field 'start_time'
        cls.start_time.states['readonly'] |= Eval('time_to_request').in_(
            list(INTERVAL_HALF_WORKINGDAYS.keys()))
        cls.start_time.depends += ['time_to_request']

    @classmethod
    def default_type_(cls):
        ModelData = Pool().get('ir.model.data')
        PermissionType = Pool().get('hr_ec.permission.type')
        permission_type = PermissionType.search([
            ('name', 'like', '%acacione%'),
        ])
        context = Transaction().context
        group_permission_hr = ModelData.get_id(
            'siim_emac', 'group_permission_hr')
        if group_permission_hr and permission_type and (
                group_permission_hr not in context['groups']):
            return permission_type[0].id
        return None

    @fields.depends('state')
    def on_change_with_type_state(self, name=None):
        if self.state and self.state != 'draft':
            return True
        ModelData = Pool().get('ir.model.data')
        PermissionType = Pool().get('hr_ec.permission.type')
        permission_type = PermissionType.search([
            ('name', 'like', '%acacione%'),
        ])
        context = Transaction().context
        group_permission_hr = ModelData.get_id(
            'siim_emac', 'group_permission_hr')
        if group_permission_hr and permission_type and (
                group_permission_hr not in context['groups']):
            return True
        else:
            return False

    @fields.depends('start_time', 'end_time', 'total', 'duration_with_weekend',
        'employee', 'kind', 'duration_without_weekend', 'discount_to',
        'time_to_request', 'type_', 'workshift', 'start_date')
    def on_change_time_to_request(self):
        if self.time_to_request:
            if self.kind == 'hours':
                if self.start_date and self.workshift and (
                        self.time_to_request in INTERVAL_HALF_WORKINGDAYS):
                    time_values = self.get_half_workinday_times(
                        self.start_date, self.workshift, self.time_to_request)
                    if time_values['start_time']  and time_values['end_time']:
                        self.start_time = time_values['start_time']
                        self.end_time = time_values['end_time']
                        super(Permission, self).calculation_duration_time()
                else:
                    if self.start_time:
                        time_intervals = self.type_.interval_time_config
                        time_value = self.get_time_value(
                            time_intervals, self.time_to_request)
                        if time_value:
                            self.end_time = sum_times(
                                self.start_time, time_value)
                            super(Permission, self).calculation_duration_time()

    @fields.depends('start_time', 'end_time', 'kind', 'time_to_request',
        'workshift', 'start_date', 'total', 'duration_with_weekend',
        'duration_without_weekend')
    def on_change_workshift(self):
        if self.workshift:
            if self.time_to_request in INTERVAL_HALF_WORKINGDAYS:
                if self.kind == 'hours' and self.start_date:
                    time_values = self.get_half_workinday_times(
                        self.start_date, self.workshift, self.time_to_request)
                    if time_values['start_time'] and time_values['end_time']:
                        self.start_time = time_values['start_time']
                        self.end_time = time_values['end_time']
                    else:
                        self.start_time = None
                        self.end_time = None
                        self.total = None
                        self.duration_with_weekend = None
                        self.duration_without_weekend = None
        super(Permission, self).on_change_workshift()

    @fields.depends('start_time', 'end_time', 'kind', 'time_to_request',
        'type_', 'employee', 'total')
    def on_change_start_time(self):
        if self.time_to_request:
            if self.kind == 'hours' and self.start_time:
                time_intervals = self.type_.interval_time_config
                time_value = self.get_time_value(
                    time_intervals, self.time_to_request)
                if time_value:
                    self.end_time = sum_times(self.start_time, time_value)
                    super(Permission, self).on_change_start_time()

    @fields.depends('kind', 'type_')
    def get_time_to_request_selection_values(self):
        selection = [(None, '')]
        if self.kind and self.type_:
            if self.kind == 'hours' and self.type_.require_interval_time_config:
                # FIRST: Get descriptions of half workingdays
                if self.type_.first_half_working_day:
                    code = 'first_half'
                    description = INTERVAL_HALF_WORKINGDAYS[code]
                    selection.append((code, description))
                if self.type_.second_half_working_day:
                    code = 'second_half'
                    description = INTERVAL_HALF_WORKINGDAYS[code]
                    selection.append((code, description))
                # SECOND: Get descriptions of interval time configuration
                for it in self.type_.interval_time_config:
                    selection.append((it.description, it.description))
        return selection

    @fields.depends('type_', 'kind')
    def on_change_with_require_interval_time_config(self, name=None):
        if self.type_ and self.kind and self.kind == 'hours':
            if self.type_.require_interval_time_config:
                if self.type_.interval_time_config:
                    return True
                elif self.type_.first_half_working_day:
                    return True
                elif self.type_.second_half_working_day:
                    return True
        return False

    @classmethod
    def set_time_to_request(cls, lines, name, value):
        pass

    @classmethod
    def get_time_to_request(cls, permissions, names=None):
        result = defaultdict(lambda: None)
        return {
            'time_to_request': result,
        }

    @fields.depends('time_to_request')
    def check_max_duration_time(self):
        if self.kind == 'hours':
            if not self.is_half_workingday_times():
                super(Permission, self).check_max_duration_time()

    def is_half_workingday_times(self):
        for key in INTERVAL_HALF_WORKINGDAYS.keys():
            time_values = self.get_half_workinday_times(
                self.start_date, self.workshift, key)
            if self.start_time == time_values['start_time']:
                if self.end_time == time_values['end_time']:
                    return True
        return False

    def get_time_value(self, time_intervals, description):
        for interval in time_intervals:
            if interval.description == description:
                return interval.interval_time
        return None

    def get_half_workinday_times(self, date, workshift, half_code):
        pool = Pool()
        WorkingDayTimetable = pool.get('company.workshift.workingday.timetable')

        start_time, end_time = None, None
        day = get_day(date.strftime('%A'))

        timetables = WorkingDayTimetable.search([
            ('workingday.workshift', '=', workshift),
            ('workingday.day', '=', day)
        ], order=[('hour', 'ASC')])

        if timetables:
            type_dialings = []
            for line in timetables:
                type_dialings.append(line.hour)
            if half_code == 'first_half':
                if len(type_dialings) >= 3:
                    start_time = type_dialings[0]
                    end_time = type_dialings[1]
                elif len(type_dialings) == 2:
                    start_time = type_dialings[0]
                    end_time = type_dialings[1]
                    h = int(start_time.hour + 4)
                    if 0 <= h <= 23:
                        end_time = time(h, start_time.minute, start_time.second)
                    else:
                        end_time = time(23, 59, 59)
            elif half_code == 'second_half':
                if len(type_dialings) == 4:
                    start_time = type_dialings[2]
                    end_time = type_dialings[3]
                elif len(type_dialings) == 3:
                    start_time = type_dialings[1]
                    end_time = type_dialings[2]
                elif len(type_dialings) == 2:
                    start_time = type_dialings[0]
                    end_time = type_dialings[1]
                    h = int(end_time.hour - 4)
                    if 0 <= h <= 23:
                        start_time = time(h, end_time.minute, end_time.second)
                    else:
                        start_time = time(0, 0, 0)
            if start_time and end_time and (end_time < start_time):
                start_time = None
                end_time = None
        return {
            'start_time': start_time,
            'end_time': end_time
        }


class PermissionType(metaclass=PoolMeta):
    __name__ = 'hr_ec.permission.type'

    require_interval_time_config = fields.Boolean(
        'Configurar intervalos de tiempo', select=True,
        help='Permite definir una lista de intervalos de tiempo disponibles '
             'para cuando el empleado solicita un permiso por horas.')
    interval_time_config = fields.One2Many(
        'hr_ec.permission.type.interval.time.configuration', 'permission_type',
        'Intervalos de tiempo predefinidos para permisos por horas',
        states={
            'invisible': ~Bool(Eval('require_interval_time_config'))
        },
        depends=['require_interval_time_config']
    )
    first_half_working_day = fields.Boolean('Primera mitad de la jornada',
        states={
            'invisible': ~Bool(Eval('require_interval_time_config'))
        },
        depends=['require_interval_time_config'])
    second_half_working_day = fields.Boolean('Segunda mitad de la jornada',
        states={
            'invisible': ~Bool(Eval('require_interval_time_config'))
        },
        depends=['require_interval_time_config'])

    @classmethod
    def view_attributes(cls):
        return super(PermissionType, cls).view_attributes() + [
            ('//page[@id="interval_time_config"]', 'states', {
                    'invisible': ~Bool(Eval('require_interval_time_config')),
            }),
            ('//group[@id="check_require_configs"]', 'col', 8),
        ]

    @fields.depends('require_interval_time_config', 'interval_time_config')
    def on_change_require_interval_time_config(self, name=None):
        if not self.require_interval_time_config:
            self.interval_time_config = None
            self.first_half_working_day = False
            self.second_half_working_day = False


class PermissionTypeIntervalTimeConfiguration(ModelSQL, ModelView):
    'HR Permission Type Interval Time Configuration'
    __name__ = 'hr_ec.permission.type.interval.time.configuration'

    permission_type = fields.Many2One(
        'hr_ec.permission.type', 'Tipo de permiso',
        states={
            'required': True
        })
    interval_time = fields.Time('Tiempo a solicitar',
        states={
            'required': True,
        })
    description = fields.Text('Descripción',
        states={
            'required': True,
        })

    @classmethod
    def __setup__(cls):
        super(PermissionTypeIntervalTimeConfiguration, cls).__setup__()
        cls._error_messages.update({
            'duplicated_error': ('No puede registrar dos horas asociadas al '
                'mismo intervalo de tiempo en un mismo tipo de permiso.\n\nPor '
                'favor, revise los registros de tiempo %(interval_time)s.'),
            'zero_minutes_time_error': ('No es posible configurar intervalos '
                'de tiempo inferiores a 0 minutos.')
        })
        cls._order.insert(0, ('interval_time', 'ASC'))
        cls._order.insert(1, ('description', 'ASC'))

    @classmethod
    def validate(cls, items):
        for item in items:
            # Find duplicates
            duplicateds = cls.search([
                ('id', '!=', item.id),
                ('permission_type', '=', item.permission_type),
                ('interval_time', '=', item.interval_time),
            ])
            if duplicateds:
                cls.raise_user_error('duplicated_error', {
                    'interval_time': str(duplicateds[0].interval_time)
                })
            # Check time value
            if time(0,0,0) <= item.interval_time < time(0,1,0):
                cls.raise_user_error('zero_minutes_time_error')
        super(PermissionTypeIntervalTimeConfiguration, cls).validate(items)

    @staticmethod
    def default_interval_time():
        return time(0, 30, 0)

    @fields.depends('interval_time')
    def on_change_with_description(self, name=None):
        if self.interval_time:
            return self.__class__.get_time_in_words(self.interval_time)
        return None

    @classmethod
    def get_time_in_words(cls, time_value):
        words = ""
        if time_value:
            h = time_value.hour
            m = time_value.minute
            s = time_value.second
            # HOURS
            if h > 0:
                words += f'{h} hora' if (h == 1) else f'{h} horas'
            # MINUTES
            if m > 0:
                words += f' y ' if (h > 0) else f''
                words += f'{m} minuto' if m == 1 else f'{m} minutos'
            # SECONDS
            if s > 0:
                words += f' con ' if (h > 0 or m > 0) else f''
                words += f'{s} segundo' if (s == 1) else f'{s} segundos'
        words = words if len(words) > 0 else None
        return words

    def get_rec_name(self, name):
        name = self.description if self.description else str(self.interval_time)
        return name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('description',) + tuple(clause[1:]),
        ]
        return domain


class UploadLawBenefitStart(metaclass=PoolMeta):
    __name__ = 'company.contract.law.benefit.template.start'

    @staticmethod
    def default_read_column_has_fr():
        return False


class PayslipGeneral(metaclass=PoolMeta):
    __name__ = 'payslip.general'

    @classmethod
    def validate(cls, generals):
        super(PayslipGeneral, cls).validate(generals)


class PermissionConsulting(metaclass=PoolMeta):
    __name__ = 'hr_ec.permission.consulting'

    @classmethod
    def get_amount_accumulate_rmu(cls, contract, start_date, end_date):
        # Defined in order to redefinition depending of each case
        PayslipLine = Pool().get('payslip.line')
        payslip_lines_rmu = PayslipLine.search([
            ('rule.code', '=', 'rmu'),
            ('payslip.contract', '=', contract),
            ('payslip.state', '=', 'done'),
            ['OR',
             ('payslip.period.start_date', '>=', start_date),
             ('payslip.period.end_date', '>=', start_date), ],
            ('payslip.period.end_date', '<=', end_date),
            ('payslip.template.accumulated_law_benefits', '=', False),
            ('payslip.template.code', '!=',
             'rol_ajuste_horas_extra_codigo_trabajo'),
        ])

        total_amount_rmu = 0
        for line in payslip_lines_rmu:
            payslip = line.payslip
            salary_day = (payslip.salary / Decimal('30.0'))
            certificates = payslip.medical_certificates_discount

            total_iess = 0
            for certificate in certificates:
                total_iess += (certificate.number_days_month * salary_day *
                               certificate.percentage_payslip_iess)

            total_amount_rmu += (line.amount + total_iess)

        return total_amount_rmu
