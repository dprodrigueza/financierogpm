from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta

__all__ = ['Period']


class Period(metaclass=PoolMeta):
    __name__ = 'account.period'

    medical_certificate_start_date = fields.Date(
        'Inicio registro de certificados médicos',
        domain=[
            ('medical_certificate_start_date', '<=', Eval(
                'medical_certificate_end_date', None))
        ], depends=['medical_certificate_end_date'])
    medical_certificate_end_date = fields.Date(
        'Fin registro de certificados médicos',
        domain=[
            ('medical_certificate_end_date', '>=', Eval(
                'medical_certificate_start_date', None))
        ], depends=['medical_certificate_start_date'])

    @fields.depends('start_date')
    def on_change_with_payslip_start_date(self):
        if self.start_date:
            month = self.start_date.month - 1
            year = self.start_date.year
            if month <= 0:
                month = 12
                year -= 1
            return datetime(year, month, 16).date()

    @fields.depends('end_date')
    def on_change_with_payslip_end_date(self):
        if self.end_date:
            _date = self.end_date
            return datetime(_date.year, _date.month, 15).date()

    @fields.depends('start_date')
    def on_change_with_overtime_start_date(self):
        if self.start_date:
            month = self.start_date.month - 1
            year = self.start_date.year
            if month <= 0:
                month = 12
                year -= 1
            return datetime(year, month, 16).date()

    @fields.depends('end_date')
    def on_change_with_overtime_end_date(self):
        if self.end_date:
            _date = self.end_date
            return datetime(_date.year, _date.month, 20).date()

    @fields.depends('start_date')
    def on_change_with_medical_certificate_start_date(self):
        if self.start_date:
            month = self.start_date.month - 1
            year = self.start_date.year
            if month <= 0:
                month = 12
                year -= 1
            return datetime(year, month, 16).date()

    @fields.depends('end_date')
    def on_change_with_medical_certificate_end_date(self):
        if self.end_date:
            _date = self.end_date
            return datetime(_date.year, _date.month, 20).date()

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            if not vals.get('medical_certificate_start_date'):
                vals['medical_certificate_start_date'] = vals['start_date']
            if not vals.get('medical_certificate_end_date'):
                vals['medical_certificate_end_date'] = vals['end_date']
        return super(Period, cls).create(vlist)
