from collections import defaultdict
from decimal import Decimal

from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool
from trytond.model import fields, tree, ModelView, ModelSQL
from .mixin import PublicGenericMixin

__all__ = ['Work', 'PublicProjectLocation', 'MoveDeliverablesStart', 
    'PublicProjectRisk', 'PublicProjectOportunity']


class Work(metaclass=PoolMeta):
    __name__ = 'project.work'
    
    @classmethod
    def view_attributes(cls):
        return super(Work, cls).view_attributes() + [
            ('/form/notebook/page[@id="deliverables"]', 'states', {
                'invisible': True
            }),
            ('/form/notebook/page[@id="portfolio"]', 'states', {
                'invisible': True
            })
        ]


class PublicProjectLocation(metaclass=PoolMeta):
    __name__ = 'public.project.location'

    municipal_visor_url = fields.Function(
        fields.Char('Link Visor Municipal'), 'get_url')
    

    @staticmethod
    def default_municipal_visor_url():
        return 'http://ide.cuenca.gob.ec/geoportal-web/viewer.jsf'

    @classmethod
    def get_url(cls, works, names):
        urls = defaultdict(lambda: '')
        for work in works:
            urls[work.id] = 'http://ide.cuenca.gob.ec/geoportal-web/viewer.jsf'
        return {
            'municipal_visor_url': urls
        }      
    

class MoveDeliverablesStart(metaclass=PoolMeta):
    'Move Deliverables Start'
    __name__ = 'move.deliverables.start'

    @classmethod
    def __setup__(cls):
        super(MoveDeliverablesStart, cls).__setup__()

        field = getattr(cls, 'type')
        field.states.update({
            'invisible': True,
            'readonly': True
        })

class PublicProjectRisk(metaclass=PoolMeta):
    """Public Project Risk"""
    __name__ = 'public.project.risk'

    matrix_information_template = fields.Function(fields.Binary(
        'Plantilla de calificación de riesgos', states={
            'invisible': True
        }),
        'on_change_with_matrix_information_template')
    
    @fields.depends('risk')
    def on_change_with_matrix_information_template(self, name=None):
        pool = Pool()
        Configuration = pool.get('public.project.configuration')
        config = Configuration(1)
        if config:
            return config.matrix_information_template


class PublicProjectOportunity(metaclass=PoolMeta):
    """Public Project Oportunity"""
    __name__ = 'public.project.oportunity'

    matrix_information_template = fields.Function(fields.Binary(
        'Plantilla de calificación de oportunidades', states={
            'invisible': True
        }),
        'on_change_with_matrix_information_template')
    
    @fields.depends('risk')
    def on_change_with_matrix_information_template(self, name=None):
        pool = Pool()
        Configuration = pool.get('public.project.configuration')
        config = Configuration(1)
        if config:
            return config.matrix_information_template
