from collections import defaultdict
from decimal import Decimal
from datetime import date, datetime

from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id, Bool, If
from trytond.model import fields, tree, ModelView, ModelSQL
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.tools import reduce_ids, grouped_slice, cursor_dict
from .mixin import PublicGenericMixin

import pandas as pd
from io import BytesIO


__all__ = [
    'Form107', 'FixForm107WizardStart', 'FixForm107WizardNext',
    'FixForm107WizardSucceed', 'FixForm107Wizard'
]



def get_dict_from_readed_row(field_names, row):
    _dict = {}
    i = 1
    for field_name in field_names:
        _dict.update({field_name: row[i]})
        i += 1
    return _dict


class Form107(metaclass=PoolMeta):
    __name__ = 'sri.form107'

    def get_field_amounts_from_rules(self):
        pool = Pool()
        Rule = pool.get('sri.form107.rule')
        PayslipLine = pool.get('payslip.line')
        LiquidationLine = pool.get('contract.liquidation.line')

        field_amounts = defaultdict(lambda: Decimal('0.00'))

        # Search SRI FORM 107 rules
        sri_rules = Rule.search([])

        for sri_rule in sri_rules:
            # Get form_field_name and payslip_rule
            field_name = sri_rule.reference_field.name
            rule_model = sri_rule.reference_model

            if not (field_name and rule_model):
                continue

            # Payslip rule
            if 'payslip' in rule_model._table:
                payslip_rule = rule_model

                # Set domain
                _domain = self.get_payslip_line_domain(sri_rule, payslip_rule)

                # Search payslip line amounts
                payslip_lines = PayslipLine.search(_domain)

                # PARCHE XIV (El archivo de migración de roles de pago tenia
                # rubros de XIV como mensualizados pero la cabecera decia
                # acumulado, por ende se duplican los valores)
                if field_name == 'r_XIV':
                    if payslip_rule.id == 28:
                        pl = []
                        for payslip_line in payslip_lines:
                            payslip = payslip_line.payslip
                            if payslip.xiv != 'accumulate':
                                pl.append(payslip_line)
                        payslip_lines = pl

                # PARCHE XIII (El archivo de migración de roles de pago tenia
                # rubros de XIII como mensualizados pero la cabecera decia
                # acumulado, por ende se duplican los valores)
                elif field_name == 'r_XIII':
                    if payslip_rule.id == 27:
                        pl = []
                        for payslip_line in payslip_lines:
                            payslip = payslip_line.payslip
                            if payslip.xiii != 'accumulate':
                                pl.append(payslip_line)
                        payslip_lines = pl

                amount = Decimal('0.00')
                for payslip_line in payslip_lines:
                    amount += payslip_line.amount

                    # En los archivos de migración para los roles migrados se
                    # envió un valor duplicado de XIV para los roles con ID
                    # 9964 y 12133
                    if field_name == 'r_XIV':
                        if payslip_line.payslip.id in [9964, 12133]:
                            amount -= Decimal('32.83')

                    # En los archivos de migración para los roles migrados se
                    # envió un valor duplicado de XIII para los roles con ID
                    # 12133
                    if field_name == 'r_XIII':
                        if payslip_line.payslip.id in [12133]:
                            amount -= Decimal('76.61')

                # Set result
                if field_name not in field_amounts:
                    field_amounts[field_name] = amount
                else:
                    field_amounts[field_name] += amount

            # Liquidation rule
            elif 'liquidation' in rule_model._table:
                liquidation_rule = rule_model

                # Set domain
                _domain = self.get_liquidation_line_domain(
                    sri_rule, liquidation_rule)

                # Search payslip line amounts
                liquidation_lines = LiquidationLine.search_read(
                    _domain, fields_names=['amount'])

                # Get total amount
                amount = sum(
                    [field_value['amount'] for field_value in liquidation_lines]
                )

                # Set result
                if field_name not in field_amounts:
                    field_amounts[field_name] = amount
                else:
                    field_amounts[field_name] += amount
        return field_amounts

    def get_payslip_line_domain(self, sri_rule, payslip_rule):
        _domain = super(
            Form107, self).get_payslip_line_domain(sri_rule, payslip_rule)

        # Se exceptuan los roles mal pagados de las personas:
        #   GUTIERRES ALVAREZ LIGIA SUSANA      (ROL ID: 25002)
        #   ZAMBRANO ILLESCAS JOSE SIGIFREDO    (ROL ID: 25054)
        _domain += [
            ('payslip.id', 'not in', [25002, 25054])
        ]
        return _domain


class FixForm107WizardStart(ModelView):
    'Fix Form107 Wizard Start'
    __name__ = 'fix.form107.wizard.start'


    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
        ], states={
            'readonly': True
        })
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True)
    source_file = fields.Binary('Archivo Fuente', required=True)

    @classmethod
    def __setup__(cls):
        super(FixForm107WizardStart, cls).__setup__()

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        now = date.today()
        year = now.year - 1
        search_date = date(year, 12, 31)
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', search_date),
            ('end_date', '>=', search_date)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    def get_employees(self):
        pool = Pool()
        Employee = pool.get('company.employee')

        registers = []
        field_names = ['CEDULA', 'NOMBRES']

        file_data = fields.Binary.cast(self.source_file)
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        df = df.fillna(False)

        for cont, row in enumerate(df.itertuples()):
            items = get_dict_from_readed_row(field_names, row)
            if items['CEDULA']:
                if len(str(items['CEDULA'])) == 9:
                    identifier = f"0{items['CEDULA']}"
                else:
                    identifier = str(items['CEDULA'])

                employees = Employee.search([
                    ('identifier', '=', identifier),
                ])

                if employees:
                    registers.append(employees[0].id)
                else:
                    print('Empleado con cedula=', identifier, ' no encontrado!')
            else:
                print('Empleado sin cedula: ' + items['NOMBRES'])
        return registers


class FixForm107WizardNext(ModelView):
    'Fix Form107 Wizard Next'
    __name__ = 'fix.form107.wizard.next'

    total_create = fields.Function(fields.Numeric('Total a crear',
        digits=(16,0)), 'on_change_with_total_create')
    employees = fields.One2Many('company.employee', None,
        'Empleados para Formulario 107')

    @classmethod
    def __setup__(cls):
        super(FixForm107WizardNext, cls).__setup__()

    @fields.depends('employees')
    def on_change_with_total_create(self, name=None):
        return len(self.employees)


class FixForm107WizardSucceed(ModelView):
    'Fix Form107 Wizard Succeed'
    __name__ = 'fix.form107.wizard.succeed'


class FixForm107Wizard(Wizard):
    'Fix Form107 Wizard'
    __name__ = 'fix.form107.wizard'

    start = StateView('fix.form107.wizard.start',
        'siim_emac.fix_form107_wizard_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Siguiente', 'next_', 'tryton-arrow-right'),
        ])
    next = StateView('fix.form107.wizard.next',
        'siim_emac.fix_form107_wizard_next_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Volver', 'back_', 'tryton-arrow-left'),
            Button('Generar', 'generate_', 'tryton-ok'),
        ])
    succeed = StateView('fix.form107.wizard.succeed',
        'siim_emac.fix_form107_wizard_succeed_view_form', [
            Button('Ok', 'end', 'tryton-executable'),
        ])

    next_ = StateTransition()
    back_ = StateTransition()
    generate_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(FixForm107Wizard, cls).__setup__()
        cls._error_messages.update({
            'no_employees': ('No ha seleccionado ningún empleado.')
        })

    def default_next(self, fields):
        defaults = {}
        defaults['employees'] = self.start.get_employees()
        return defaults

    def transition_next_(self):
        return 'next'

    def transition_back_(self):
        return 'start'

    def transition_generate_(self):
        pool = Pool()
        Form107 = pool.get('sri.form107')

        employees = self.next.employees
        company = self.start.company
        fiscalyear = self.start.fiscalyear

        if not employees:
            self.raise_user_error('no_employees')

        to_save = []
        to_cancel = []

        ids = [employee.id for employee in employees]
        forms = Form107.search([
            ('employee', 'in', ids),
            ('fiscal_year', '=', fiscalyear),
            ('state', '!=', 'cancel')
        ])
        if forms:
            to_cancel += list(forms)

        for employee in employees:
            # Create new forms
            form = Form107()
            form.company = company
            form.fiscal_year = fiscalyear
            form.employee = employee
            form.delivery_date = datetime.now().date()
            form.set_fields()
            form.get_income_tax()
            to_save.append(form)
        Form107.cancel(to_cancel)
        Form107.save(to_save)
        return 'succeed'
