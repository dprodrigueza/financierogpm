import ast
import json
from calendar import different_locale, month_name
from collections import defaultdict
from decimal import Decimal

from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.modules.hr_ec_payslip import evaluator
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = [
    'LoanOneFee', 'ReportContractLiquidationEmac',
]


def get_employee():
    e_id = Transaction().context.get('employee')
    Employee = Pool().get('company.employee')
    employee, = Employee.browse([e_id])
    employee_name = employee.party.name.lower()
    name = ''
    for word in employee_name.split(' '):
        name += word.capitalize() + " "
    return name

def get_month_name(month_no, locale):
    with different_locale(locale) as encoding:
        s = month_name[month_no]
        if encoding is not None:
            s = s.decode(encoding)
        return s


class LoanOneFee(CompanyReportSignature):
    __name__ = 'company.employee.loan_one_fee'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(
            LoanOneFee, cls).get_context(records, data)
        report_context['month_name'] = cls._get_month_name
        return report_context

    @classmethod
    def _get_month_name(cls, start_date, locale):
        month = get_month_name(start_date.month, locale)
        return month.capitalize()



class ReportContractLiquidationEmac(CompanyReportSignature):
    __name__ = 'contract.liquidation.emac'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportContractLiquidationEmac, cls).get_context(
            records, data)

        report_context['_get_totals_liquidation'] = cls._get_totals_liquidation
        report_context['_get_income_payslip_xiii'] = cls._get_income_payslip_xiii
        report_context['_get_exist_payslip'] = cls._get_exist_payslip
        report_context['_get_income_liquidation_xiii'] = cls._get_income_liquidation_xiii
        report_context['_get_total_income_xiii'] = cls._get_total_income_xiii
        report_context['_get_total_income_xiv'] = cls._get_total_income_xiv
        report_context['_get_adjustment_xiv_amount'] = cls._get_adjustment_xiv_amount
        report_context['_get_total_income_desahucio'] = cls._get_total_income_desahucio

        return report_context

    @classmethod
    def _get_totals_liquidation(cls, records):
        totals_liquidation = {}
        totals_income = {}
        totals_deduction = {}
        for liquidation in records:

            for rule_line in liquidation.income_lines:
                totals_liquidation[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
                totals_income[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
            totals_liquidation['total_income'] = {
                'header': 'Total Ingresos',
                'amount': liquidation.total_income
            }
            totals_income['total_income'] = {
                'header': 'Total Ingresos',
                'amount': liquidation.total_income
            }
            for rule_line in liquidation.deduction_lines:
                totals_liquidation[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
                totals_deduction[rule_line.rule.code] = {
                    'header': rule_line.rule.name,
                    'amount': rule_line.amount
                }
            totals_liquidation['total_deduction'] = {
                'header': 'Total Egresos',
                'amount': liquidation.total_deduction
            }
            totals_deduction['total_deduction'] = {
                'header': 'Total Egresos',
                'amount': liquidation.total_deduction
            }

        return {
            'totals_liquidation': totals_liquidation,
            'totals_income': totals_income,
            'totals_deduction': totals_deduction,
        }

    @classmethod
    def _get_income_payslip_xiii(cls, payslip, contract, period):
        Payslip = Pool().get('payslip.payslip')
        PayslipLine = Pool().get('payslip.line')

        if not payslip:
            payslip, = Payslip.search([
                ('contract', '=', contract),
                ('period', '=', period),
                ('state', '=', 'done'),
                ('template.accumulated_law_benefits', '=', False),
                (
                'template.code', '!=', 'rol_ajuste_horas_extra_codigo_trabajo'),
            ])
        condition = True
        result = Decimal('0.00')
        for line in payslip.template.lines:
            rule = line.rule
            if condition and not rule.is_law_benefit:
                payslip_lines = PayslipLine.search([
                    ('rule.code', '=', rule.code),
                    ('payslip', '=', payslip),
                ])
                if payslip_lines:
                    if rule.code == 'rmu':
                        salary_day = (payslip.salary / Decimal('30.0'))
                        certificates = payslip.medical_certificates_discount

                        total_iess = 0
                        for certificate in certificates:
                            total_iess += (certificate.number_days_month * salary_day *
                                    certificate.percentage_payslip_iess)

                        result += (payslip_lines[0].amount + total_iess)
                    else:
                        result += payslip_lines[0].amount

            if rule.code == 'xiii':
                condition = False
                break

        return (Decimal(str(result)).quantize(Decimal('0.00')))


    @classmethod
    def _get_exist_payslip(cls, contract, period):
        Payslip = Pool().get('payslip.payslip')

        payslips = Payslip.search([
            ('contract', '=', contract),
            ('period', '=', period),
            ('state', '=', 'done'),
            ('template.accumulated_law_benefits', '=', False),
            ('template.code', '!=', 'rol_ajuste_horas_extra_codigo_trabajo'),
        ])
        if payslips:
            return True
        return False


    @classmethod
    def _get_income_liquidation_xiii(cls, liquidation):
        LiquidationLine = Pool().get('contract.liquidation.line')

        condition = True
        result = Decimal('0.00')
        for line in liquidation.template.liquidation_rules:
            rule = line.rule
            if condition and not rule.is_law_benefit:
                liquidation_lines = LiquidationLine.search([
                    ('rule.associated_rule.code', '=', rule.code),
                    ('liquidation', '=', liquidation),
                ])
                if liquidation_lines:
                    result += liquidation_lines[0].amount

            if rule.code == 'xiii':
                condition = False
                break

        return (Decimal(str(result)).quantize(Decimal('0.00')))

    @classmethod
    def _get_total_income_xiii(cls, liquidation, law_benefits_xiii):

        result = Decimal('0.00')
        total_xiii = Decimal('0.00')
        if liquidation.state == 'done':
            for law in law_benefits_xiii:
                if (law.xiii_line.generator_payslip or
                        cls._get_exist_payslip(liquidation.contract,
                                               law.xiii_line.period)):
                    result += cls._get_income_payslip_xiii(law.xiii_line.generator_payslip,
                                                           liquidation.contract, law.xiii_line.period)
                else:
                    result += cls._get_income_liquidation_xiii(liquidation)
                total_xiii += law.xiii_line.amount
        else:
            for law in law_benefits_xiii:
                result += cls._get_income_payslip_xiii(law.xiii_line.generator_payslip,
                    liquidation.contract, law.xiii_line.period)
                total_xiii += law.xiii_line.amount

            if (liquidation.xiii_proportional > 0 and
                    cls._get_exist_payslip(liquidation.contract,
                                           liquidation.period)):
                result += cls._get_income_payslip_xiii(False, liquidation.contract, liquidation.period)
                total_xiii += liquidation.xiii_proportional
            elif liquidation.xiii_proportional > 0:
                result += cls._get_income_liquidation_xiii(liquidation)
                total_xiii += liquidation.xiii_proportional


        return {
            'total_income': (Decimal(str(result)).quantize(Decimal('0.00'))),
            'total_xiii': (Decimal(str(total_xiii)).quantize(Decimal('0.00'))),
        }

    @classmethod
    def _get_adjustment_xiv_amount(self, law_amount, liquidation, period):
        actual_base_salary = liquidation.period.fiscalyear.base_salary
        previous_base_salary = period.fiscalyear.base_salary

        new_amount = ((law_amount * actual_base_salary) / previous_base_salary)

        return new_amount.quantize(Decimal('0.00'))

    @classmethod
    def _get_total_income_xiv(cls, liquidation):

        result = sum([round(float(xiv.xiv_line.amount * 30) / float(
            liquidation.period.fiscalyear.base_salary / 12)) for xiv in
                      liquidation.law_benefits_xiv])
        total_xiv = Decimal('0.00')
        for line in liquidation.law_benefits_xiv:
            if line.xiv_line.kind != 'adjustment':
                total_xiv += cls._get_adjustment_xiv_amount(line.xiv_line.amount,
                    liquidation, line.xiv_line.period)

        if liquidation.state != 'done' and liquidation.xiv_proportional > 0:
            result += float(liquidation.xiv_proportional * 30) / float(
                liquidation.period.fiscalyear.base_salary / 12)
            total_xiv += liquidation.xiv_proportional


        return {
            'total_income':(Decimal(str(result)).quantize(Decimal('0.00'))),
            'total_xiv': (Decimal(str(total_xiv)).quantize(Decimal('0.00'))),
        }

    @classmethod
    def _get_total_income_desahucio(cls, last_payslip):

        result = Decimal('0.00')
        for line in last_payslip.income_lines:

            if line.rule.code == 'rmu':
                salary_day = (last_payslip.salary / Decimal('30.0'))
                certificates = last_payslip.medical_certificates_discount

                total_iess = 0
                for certificate in certificates:
                    total_iess += (
                                certificate.number_days_month * salary_day *
                                certificate.percentage_payslip_iess)

                result += (line.amount + total_iess)
            elif line.rule.code == 'overtimes' or line.rule.code == 'recargo_nocturno':
                result += line.amount


        return (Decimal(str(result)).quantize(Decimal('0.00')))
