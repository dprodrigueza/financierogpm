from datetime import date, timedelta
from decimal import Decimal
from calendar import monthrange

from sql import Column, Literal, Window
from sql.functions import RowNumber
from trytond.model import (Workflow, ModelView, fields)
from trytond.pool import PoolMeta, Pool
from trytond import backend
from trytond.pyson import Eval, Bool, If
from trytond.transaction import Transaction

import numpy as np

__all__ = ['UploadDailingRegister', 'ActionOfTheStaff', 'CommissionSubrogation',
           'CommissionSubrogationChangeEndDate', 'EmployeeBurden']


ZERO = Decimal('0.0')
CENT = Decimal('0.0000')


def diff_dates(start_date, end_date, weekmask='1111100'):
    pool = Pool()
    Holiday = pool.get('hr_ec.holiday')
    holidays = []
    for holiday in Holiday.browse(Holiday.between_dates(start_date, end_date)):
        holidays.append(holiday.date_discount)
    # Holidays is not considered on calculation of values to pay
    return np.busday_count(
        start_date.date(), (end_date + timedelta(days=1)).date(),
        holidays=[], weekmask=weekmask)


def diff_dates_change_end_date(start_date, end_date, weekmask='1111100'):
    pool = Pool()
    Holiday = pool.get('hr_ec.holiday')
    holidays = []
    for holiday in Holiday.browse(Holiday.between_dates(start_date, end_date)):
        holidays.append(holiday.date_discount)
    # Holidays is not considered on calculation of values to pay
    return np.busday_count(start_date, end_date + timedelta(days=1),
        holidays=[], weekmask=weekmask)


def check_exceeded_days(start_date, end_date, days):
    # una subrogacion solo se calcula hasta el dia 30
    # una subrogacion no puede tener mas de 30 dias
    if (start_date.day > 1 and end_date.day == 31) | (days > 30):
        return False
    else:
        return True


class EmployeeBurden(metaclass=PoolMeta):
    __name__ = 'company.employee_burden'

    @classmethod
    def __setup__(cls):
        super(EmployeeBurden, cls).__setup__()
        cls.kindergarten_value.domain = [
            If(Bool(Eval('kindergarten')), ('kindergarten_value', '>', 0), ()),
        ]
        cls.kindergarten_value.depends = ['kindergarten']

    @staticmethod
    def default_kindergarten_value():
        return Decimal('110.00')

    @fields.depends('kindergarten', 'kindergarten_value')
    def on_change_with_kindergarten_value(self, name=None):
        value = None
        if not self.kindergarten:
            value = Decimal('0.00')
        else:
            if not self.kindergarten_value:
                value = Decimal('110.00')
            else:
                value = self.kindergarten_value
        return value


class UploadDailingRegister(metaclass=PoolMeta):
    'Upload Dailing Register'
    __name__ = 'company.upload.dialing.register'

    def get_fieldsnames(self):
        return ['', 'ID', 'NOMBRE', 'FECHA', 'ESTADO', 'F', 'G',
                'TIPO REGISTRO']

    def get_header(self):
        return 5

    def get_skipfooter(self):
        return 1


class ActionOfTheStaff(metaclass=PoolMeta):
    __name__ = 'company.action.staff'

    @classmethod
    def recalculate_comm_sub(cls, item, end_date):
        # Se hereda el metodo ya que la funcionalidad de la herencia de
        # 'drecrease_period' en este archivo no es considerada cuando se
        # ejecuta desde el core, debido a que la clase que la declara
        # CommissionSubrogationChangeEndDate es un Wizard y el Pool() no
        # contiene instancias de wizards por ende, al llamar a la función en
        # codigo, no se ejecuta el procedimiento que consta en este archivo
        # que es exclusivo de EMAC
        CommissionSubrogation = Pool().get('company.commission.subrogation')
        subrogation = item.select_comm_sub

        if end_date < item.select_comm_sub.start_date:
            cls.raise_user_error('date_to_low')
        if item.select_comm_sub.end_date < end_date:
            CommissionSubrogation.request([subrogation],
                change_end_date=end_date, new_list=[subrogation])
        else:
            CommissionSubrogationChangeEndDate.decrease_periods(
                end_date, subrogation)


class CommissionSubrogation(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation'

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    def request(cls, comm_subs, change_end_date=None, new_list=None):
        """
        Create detail lines.
        If change_end_date is provided, it will be used to modify the actual
        lines (wizard)
        """
        pool = Pool()
        CommissionSubrogation = pool.get('company.commission.subrogation')
        CommissionSubrogationDetail = pool.get(
            'company.commission.subrogation.detail')
        Period = pool.get('account.period')
        to_delete_detail = []
        if new_list:
            comm_subs = new_list

        temp_start_date = None
        for comm_sub in comm_subs:
            for detail in comm_sub.detail:
                # Details lines that have not been added to a payment role are
                # deleted
                if not detail.payslip:
                    to_delete_detail.append(detail)
                    if not temp_start_date:
                        temp_start_date = detail.start_date

        if to_delete_detail:
            CommissionSubrogationDetail.delete(to_delete_detail)
        to_save_commission = []
        to_save_detail = []
        periods = {}

        ids = [comm_sub.id for comm_sub in comm_subs]
        comm_subs = CommissionSubrogation.search([
            ('id', 'in', ids)
        ])
        for comm_sub in comm_subs:
            start_date = comm_sub.start_date
            if change_end_date:
                comm_sub.end_date = change_end_date
            if temp_start_date:
                start_date = temp_start_date
            days = np.arange(start_date, comm_sub.end_date + timedelta(days=1),
                timedelta(days=1)).astype(date)
            amount = Decimal(0.0)
            for day in days:
                period = None
                period_search = Period.search([
                    ('start_date', '<=', day),
                    ('end_date', '>=', day),
                ])
                if not period_search:
                    cls.raise_user_error('not_period', {})
                else:
                    period = period_search[0]
                if period:
                    if not periods.get(period.id):
                        periods[period.id] = {
                            'period': period,
                            'start_date': day,
                            'end_date': day
                        }
                    else:
                        if day < periods[period.id]['start_date']:
                            periods[period.id]['start_date'] = day
                        if day > periods[period.id]['end_date']:
                            periods[period.id]['end_date'] = day

            for key, values in periods.items():
                # Momentary validation to prevent the creation of two lines of
                # the same period (it happens when the line has already been
                # added to a payment role, when recalculating, it was doubling)
                is_valid = True
                for detail_aux in comm_sub.detail:
                    if detail_aux.start_date == values['start_date'].date():
                        is_valid = False
                        break
                if is_valid:
                    detail = CommissionSubrogationDetail()
                    detail.commission = comm_sub.id
                    detail.start_date = values['start_date']
                    detail.end_date = values['end_date']
                    detail.effective_date = values['end_date']
                    detail.work_days = diff_dates(
                        values['start_date'], values['end_date'],
                        weekmask='1111111')
                    if not check_exceeded_days(detail.start_date,
                            detail.end_date, detail.work_days):
                        detail.work_days = detail.work_days - 1
                    if values['start_date'] and values['end_date']:
                        effective_date = values['end_date']
                        if effective_date:
                            detail.effective_date = effective_date
                    else:
                        detail.effective_date = (values['end_date'])
                    total_days_month = monthrange(
                        detail.end_date.year, detail.end_date.month)[1]
                    if total_days_month > 30:
                        total_days_month = 30
                    if detail.work_days >= total_days_month:
                        detail.amount = ((30 * (
                                comm_sub.salary_difference / 30))
                                         .quantize(CENT))
                    else:
                        detail.amount = ((detail.work_days * (
                                comm_sub.salary_difference / total_days_month))
                                         .quantize(CENT))
                    amount += detail.amount
                    to_save_detail.append(detail)
            comm_sub.amount = amount.quantize(Decimal('0.01'))
            to_save_commission.append(comm_sub)
        CommissionSubrogation.save(to_save_commission)
        CommissionSubrogationDetail.save(to_save_detail)


class CommissionSubrogationChangeEndDate(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation.change_end_date'

    @classmethod
    def decrease_periods(cls, end_date, subrogation):
        pool = Pool()
        CommissionSubrogationDetail = pool.get(
            'company.commission.subrogation.detail')
        to_delete_detail = []
        for detail in subrogation.detail:
            if detail.payslip:
                if (detail.start_date < end_date < detail.end_date):
                    cls.raise_user_error('already_payslipped')
            else:
                if detail.start_date > end_date:
                    to_delete_detail.append(detail)
                elif detail.end_date <= end_date:
                    if not check_exceeded_days(detail.start_date,
                            detail.end_date, detail.work_days):
                        detail.work_days = detail.work_days - 1
                        CommissionSubrogationDetail.save([detail])
                else:
                    detail.end_date = end_date
                    detail.effective_date = end_date
                    detail.work_days = diff_dates_change_end_date(
                        detail.start_date, detail.end_date, weekmask='1111111')
                    total_days_month = monthrange(
                        detail.end_date.year, detail.end_date.month)[1]
                    if total_days_month > 30:
                        total_days_month = 30
                    if detail.work_days > total_days_month:
                        detail.amount = ((30 * (
                            subrogation.salary_difference / 30))
                                .quantize(CENT))
                    else:
                        detail.amount = ((detail.work_days * (
                            subrogation.salary_difference / total_days_month))
                                .quantize(CENT))
                    detail.amount = ((detail.work_days * (
                        subrogation.salary_difference / total_days_month))
                            .quantize(CENT))
                    detail.save()
        CommissionSubrogationDetail.delete(to_delete_detail)
        amount = 0
        for detail in subrogation.detail:
            amount += detail.amount
        subrogation.end_date = end_date
        subrogation.amount = amount.quantize(Decimal('0.00'))
        subrogation.save()
        return 'end'



