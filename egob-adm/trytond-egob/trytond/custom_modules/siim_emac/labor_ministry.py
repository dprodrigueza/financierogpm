import datetime

from trytond.pool import PoolMeta, Pool


__all__ = [
    'LaborMinistryXIIIReportWizardStart', 'LaborMinistryXIVReportWizardStart'
]


class LaborMinistryXIIIReportWizardStart(metaclass=PoolMeta):
    __name__ = 'labor.ministry_xiii.report.wizard.start'

    @staticmethod
    def default_retention_rules():
        PayslipTemplate = Pool().get('payslip.template')
        templates = PayslipTemplate.search([
            ('type', '=', 'accumulated_law_benefits'),
            ('law_benefit_type.code', '=', 'xiii'),
        ])
        ids = []
        for template in templates:
            for line in template.lines:
                if 'retencion_judicial' in line.rule.code:
                    ids.append(line.rule.id)
        return ids


class LaborMinistryXIVReportWizardStart(metaclass=PoolMeta):
    __name__ = 'labor.ministry_xiv.report.wizard.start'

    @staticmethod
    def default_start_date():
        now = datetime.date.today()
        year = (now.year - 1) if (now.month == 8) else (now.year - 2)
        return datetime.date(year, 8, 1)

    @staticmethod
    def default_retention_rules():
        PayslipTemplate = Pool().get('payslip.template')
        templates = PayslipTemplate.search([
            ('type', '=', 'accumulated_law_benefits'),
            ('law_benefit_type.code', '=', 'xiv'),
        ])
        ids = []
        for template in templates:
            for line in template.lines:
                if 'retencion_judicial' in line.rule.code:
                    ids.append(line.rule.id)
        return ids