from trytond.pool import Pool, PoolMeta
from trytond.model import Workflow, ModelSQL, ModelView, fields, tree, Unique
from trytond.pyson import If, Bool, Eval, Id
from trytond.transaction import Transaction


_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


class Asset( metaclass=PoolMeta):
    __name__ = 'asset'
    warranty_date = fields.Date('F.Vencimiento Garantia',
    states=_STATES, depends=_DEPENDS, required=True)