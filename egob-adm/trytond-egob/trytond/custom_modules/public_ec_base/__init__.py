# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import public_budget
from . import public_siim
from . import account

__all__ = ['register']


def register():
    Pool.register(
        public_budget.PublicBudgetTemplate,
        public_budget.PublicBudget,
        public_budget.CreateChartStart,
        public_budget.CreateChartBudget,
        public_budget.CreateChartSucceed,
        public_siim.Company,
        public_siim.PublicSIIMModule,
        public_siim.PublicSIIMItem,
        public_siim.PublicSIIMItemAccount,
        public_siim.PublicSIIMItemAccountPrevious,
        account.AccountTemplate,
        account.Account,
        module='public_ec_base', type_='model')
    Pool.register(
        public_budget.CreateChart,
        module='public_ec_base', type_='wizard')
    Pool.register(
        module='public_ec_base', type_='report')
