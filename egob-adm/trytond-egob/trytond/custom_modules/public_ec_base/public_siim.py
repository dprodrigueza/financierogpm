from decimal import Decimal
import logging

from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, If
from trytond.transaction import Transaction
from trytond.config import config as trytond_config

import requests

__all__ = ['Company', 'PublicSIIMModule', 'PublicSIIMItem',
    'PublicSIIMItemAccount', 'PublicSIIMItemAccountPrevious']

logger = logging.getLogger(__name__)

digits_ = (16, trytond_config.getint('siim', 'digits', default=2))


class Company:
    __metaclass__ = PoolMeta
    __name__ = 'company.company'
    tax_id = fields.Integer('Tax Identification', required=True)

    @staticmethod
    def default_tax_id():
        return 1


class PublicSIIMModule(ModelView, ModelSQL):
    'Public SIIM Module'
    __name__ = 'public.siim.module'
    module_id = fields.Integer('SIIM ID', required=True, readonly=True)
    name = fields.Char('Name', required=True, size=255, readonly=True)
    active = fields.Boolean('Active')

    @classmethod
    def __setup__(cls):
        super(PublicSIIMModule, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
            ]
        table = cls.__table__()
        cls._sql_constraints = [
            ('siim_name_uniq',
             Unique(table, table.name),
             'SIIM module name already exists.'),
            ('siim_siim_id_uniq',
             Unique(table, table.module_id),
             'SIIM  ID module already exists.'),
        ]

    @staticmethod
    def default_active():
        return True


class PublicSIIMItem(ModelView, ModelSQL):
    'SIIM Item'
    __name__ = 'public.siim.item'
    _history = True

    states_ = {
        'readonly': (Eval('type_') == '1'),
    }
    depends_ = ['type_']

    item_id = fields.Integer(
        'SIIM ID', readonly=True, states=states_, depends=depends_)
    type_ = fields.Selection(
        [
            ('1', 'Fixed, non editable'),
            ('0', 'Editable'),
        ], 'Type', sort=False, required=True,
        states=states_, depends=depends_)
    description = fields.Text(
        'Name', required=True, size=255, states=states_, depends=depends_)
    active = fields.Boolean('Active')
    state = fields.Selection(
        [
            ('1', 'Active'),
            ('0', 'Deleted')
        ], 'State', sort=False, required=True, states=states_, depends=depends_)
    calculable = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'Fixed value'),
        ], 'Calculable', sort=False, required=True,
        states=states_, depends=depends_)
    value = fields.Numeric('Value', required=True, digits=digits_,
        states=states_, depends=depends_)
    tax = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No')
        ], 'Tax', required=True, states=states_, depends=depends_)
    is_tax = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No')
        ], 'Is Tax?', required=True, sort=False,
        states=states_, depends=depends_)
    main_item = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No')
        ], 'Main Item', required=True, sort=False,
        states=states_, depends=depends_)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=states_, depends=depends_)
    tax_id = fields.Function(
        fields.Integer('Tax Information'), 'on_change_with_tax_id')
    module = fields.Many2One('public.siim.module', 'Module', required=True,
        states=states_, depends=depends_)
    module_id = fields.Function(
        fields.Integer('Module ID'), 'on_change_with_module_id')
    share = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No')
        ], 'Share', required=True, sort=False,
        states=states_, depends=depends_)
    billable = fields.Selection(
        [
            ('1', 'Yes'),
            ('0', 'No')
        ], 'Billable', required=True, sort=False,
        states=states_, depends=depends_)
    account_lines = fields.One2Many(
        'public.siim.item.account', 'item', 'Account configuration by dates')

    del states_
    del depends_

    @classmethod
    def __setup__(cls):
        super(PublicSIIMItem, cls).__setup__()
        cls._order = [
            ('module.name', 'ASC'),
            ('description', 'ASC'),
            ]
        cls.active.invisible = True
        table = cls.__table__()
        cls._sql_constraints = [
            ('siim_id_uniq', Unique(table, table.item_id),
             'SIIM ID already exists.'),
            ('siim_module_description',
             Unique(table, table.module, table.description),
             'SIIM description - module already exists.'),
        ]
        cls._error_messages.update({
            'siim_error': ('Error on SIIM communication'),
            'delete_forbidden': ('Items can not be deleted '
                'for logging purpose.\n'
                'Instead you must inactivate them.'),
        })

    @staticmethod
    def default_type_():
        return '0'

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def default_state():
        return '1'

    @staticmethod
    def default_calculable():
        return '0'

    @staticmethod
    def default_value():
        return Decimal(0)

    @staticmethod
    def default_tax():
        return '0'

    @staticmethod
    def default_is_tax():
        return '0'

    @staticmethod
    def default_main_item():
        return '0'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_share():
        return '0'

    @staticmethod
    def default_billable():
        return '1'

    @fields.depends('active', 'state')
    def on_change_state(self):
        if self.state:
            if self.state == '1':
                self.active = True
            else:
                self.active = False
        else:
            self.active = None

    @fields.depends('company')
    def on_change_with_tax_id(self, name=None):
        if self.company:
            return self.company.tax_id
        return None

    @fields.depends('module')
    def on_change_with_module_id(self, name=None):
        if self.module:
            return self.module.module_id
        return None

    def get_rec_name(self, name):
        return self.module.rec_name + ' / ' + self.description

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('module.rec_name',) + tuple(clause[1:]),
            ('description',) + tuple(clause[1:]),
            ]

    @classmethod
    def get_url_rest(cls):
        return 'getRubro/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s/%s'

    @classmethod
    def get_url_rest_params(cls, values):
        url_params = [values['item_id'],
            values['description'], values['module_id'], values['state'],
            values['calculable'], values['value'], values['tax'],
            values['is_tax'], values['main_item'], values['tax_id'],
            values['share'], values['billable']]
        return url_params

    @classmethod
    def to_int_fields(cls):
        return ['state', 'calculable', 'tax', 'is_tax', 'main_item', 'share',
            'billable']

    @classmethod
    def siim_rest_item(cls, values):
        url_item_callback = trytond_config.get('siim', 'url_callback')
        url_item_callback += cls.get_url_rest()

        to_int = cls.to_int_fields()

        for value in values:
            if value in to_int:
                values[value] = int(values[value])

        defaults = cls.default_get(cls._fields.keys())
        defaults.update(values)

        defaults['description'] = defaults['description'].replace('/', '%2F')
        defaults['description'] = defaults['description'].replace('#', '%23')

        url_rest = url_item_callback % tuple(cls.get_url_rest_params(defaults))

        result = None

        try:
            r = requests.get(url_rest)
            # 200 is OK response code
            if r.status_code == 200:
                values['item_id'] = int(r.content)
            else:
                cls.raise_user_error('siim_error')
            result = r.content
        except (requests.HTTPError, requests.ConnectionError) as e:
            logger.error(e.message)
            cls.raise_user_error('siim_error')
        if r.content == 0:
            cls.raise_user_error('siim_error')
        return result

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Company = pool.get('company.company')
        SIIMModule = pool.get('public.siim.module')
        vlist = [v.copy() for v in vlist]
        for values in vlist:
            if not values.get('item_id'):
                company = Company(values['company'])
                siim_module = SIIMModule(values['module'])

                values_siim = values.copy()
                values_siim.update({
                    'item_id': 0,  # Send 0 to create a new item
                    'module_id': siim_module.module_id,
                    'tax_id': company.tax_id,
                })
                rest_item = cls.siim_rest_item(values_siim)
                values['item_id'] = int(rest_item)
        return super(PublicSIIMItem, cls).create(vlist)

    @classmethod
    def write(cls, items, values):
        pool = Pool()
        Company = pool.get('company.company')
        SIIMModule = pool.get('public.siim.module')
        super(PublicSIIMItem, cls).write(items, values)
        for item in items:
            if values.get('company'):
                company = Company(values['company'])
                tax_id = company.tax_id
            else:
                tax_id = item.tax_id
            if values.get('module'):
                siim_module = SIIMModule(values['module'])
                module_id = siim_module.module_id
            else:
                module_id = item.module_id
            siim_values = cls.read([item.id], [])[0]
            siim_values.update(values)
            siim_values['module_id'] = module_id
            siim_values['tax_id'] = tax_id
            cls.siim_rest_item(siim_values)

    @classmethod
    def delete(cls, users):
        cls.raise_user_error('delete_forbidden')


class PublicSIIMItemAccount(ModelView, ModelSQL):
    'SIIM Item Accounting'
    __name__ = 'public.siim.item.account'
    _history = True

    item = fields.Many2One('public.siim.item', 'Item',
        ondelete='CASCADE', select=True, required=True)
    fiscalyear = fields.Many2One(
        'account.fiscalyear', 'Fiscal Year', required=True)
    start_date = fields.Function(
        fields.Date('Start date'), 'on_change_with_start_date')
    end_date = fields.Function(
        fields.Date('End date'), 'on_change_with_end_date')
    # TODO add domain for payable account on revenue account
    account_revenue = fields.Many2One('account.account', 'Account revenue',
        domain=[
            ('code', 'like', '62%'),
        ], required=True)
    budget = fields.Function(
        fields.Many2One('public.budget', 'Budget'), 'on_change_with_budget')
    account_receivable = fields.Many2One('account.account',
        'Account receivable',
        domain=[
            ('code', 'like', '113.%'),
            If(Bool(Eval('budget')),
               ['OR',
                ('budget_debit', 'child_of', Eval('budget', -1), 'parent'),
                ('budget_credit', 'child_of', Eval('budget', -1), 'parent')
               ],
               []
            )
        ], depends=['budget'], required=True)
    previous_years = fields.One2Many('public.siim.item.account.previous',
        'item_account', 'Previous years')

    @classmethod
    def __setup__(cls):
        super(PublicSIIMItemAccount, cls).__setup__()
        cls._order = [
            ('fiscalyear.start_date', 'DESC'),
            ]
        table = cls.__table__()
        cls._sql_constraints = [
            ('item_fiscalyear_uniq',
             Unique(table, table.item, table.fiscalyear),
             'Configuration for item and fiscalyear already exists.'),
        ]

    @fields.depends('fiscalyear')
    def on_change_with_start_date(self, name=None):
        if self.fiscalyear:
            return self.fiscalyear.start_date
        return None

    @fields.depends('fiscalyear')
    def on_change_with_end_date(self, name=None):
        if self.fiscalyear:
            return self.fiscalyear.end_date
        return None

    @fields.depends('account_revenue')
    def on_change_with_budget(self, name=None):
        if self.account_revenue:
            return self.account_revenue.budget_credit.id
        return None


class PublicSIIMItemAccountPrevious(ModelView, ModelSQL):
    'SIIM Item Accounting Previuos Years'
    __name__ = 'public.siim.item.account.previous'
    _history = True

    item_account = fields.Many2One('public.siim.item.account', 'Item',
        ondelete='CASCADE', select=True, required=True)
    item_year = fields.Function(
        fields.Integer('Fiscal Year'), 'on_change_with_item_year')
    type_ = fields.Selection([
        ('all', 'To all previous years'),
        ('by_year', 'By year'),
    ], 'Type', required=True)
    year = fields.Integer('Year',
        domain=[
            If((Eval('type_') == 'by_year'),
               ['AND',
                ('year', '>', 0),
                ('year', '<', Eval('item_year'))
               ],
               [])
        ],
        states={
            'required': (Eval('type_') == 'by_year'),
            'invisible': ~(Eval('type_') == 'by_year'),
            },
        depends=['type_', 'item_year']
    )
    # TODO add domain for payable account on revenue account
    account_revenue = fields.Many2One('account.account', 'Account revenue',
        domain=[
            ('code', 'like', '62%'),
        ], required=True)
    budget = fields.Function(
        fields.Many2One('public.budget', 'Budget'), 'on_change_with_budget')
    account_receivable = fields.Many2One('account.account',
        'Account receivable',
        domain=[
            ('code', 'like', '113.%'),
            If(Bool(Eval('budget')),
               ['OR',
                ('budget_debit', 'child_of', Eval('budget', -1), 'parent'),
                ('budget_credit', 'child_of', Eval('budget', -1), 'parent')
               ],
               []
            )
        ], depends=['budget'], required=True)

    @classmethod
    def __setup__(cls):
        super(PublicSIIMItemAccountPrevious, cls).__setup__()
        cls._order = [
            ('year', 'DESC'),
            ]
        table = cls.__table__()
        cls._sql_constraints = [
            ('item_account_year_uniq',
             Unique(table, table.item_account, table.year),
             'Configuration previous year already exists.'),
        ]
        cls._error_messages.update({
            'previous_config_duplicated': (
                'Duplicated previous year configuration for item "%(name)s"'),
        })

    @fields.depends('item_account', 'type_')
    def on_change_with_item_year(self, name=None):
        if self.item_account:
            if self.type_ == 'by_year':
                return self.item_account.fiscalyear.start_date.year
        return None

    @fields.depends('account_revenue')
    def on_change_with_budget(self, name=None):
        if self.account_revenue:
            return self.account_revenue.budget_credit.id
        return None

    @classmethod
    def validate(cls, previous):
        super(PublicSIIMItemAccountPrevious, cls).validate(previous)
        for prev in previous:
            siblings = cls.search([
                ('id', '!=', prev.id),
                ('item_account', '=', prev.item_account.id),
                ('type_', '!=', prev.type_)
            ])
            if siblings:
                cls.raise_user_error('previous_config_duplicated', {
                    'name': prev.item_account.item.description,
                })
