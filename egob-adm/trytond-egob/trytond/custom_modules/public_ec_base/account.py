import re

from sql import Null

from trytond.model import fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Or, Bool, If
from trytond.transaction import Transaction

from .utils import update_level

__all__ = ['AccountTemplate', 'Account']


class AccountTemplate:
    __metaclass__ = PoolMeta
    __name__ = "account.account.template"

    description = fields.Text('Description')
    budget_debit = fields.Many2One(
        'public.budget.template', "Public Budget Template Debit")
    budget_credit = fields.Many2One(
        'public.budget.template', "Public Budget Template Credit")

    def _get_account_value(self, account=None):
        Budget = Pool().get('public.budget')
        res = super(AccountTemplate, self)._get_account_value(account)
        if self.budget_debit:
            budget = Budget.search([('template', '=', self.budget_debit)])
            if budget:
                res['budget_debit'] = budget[0]
        if self.budget_credit:
            budget = Budget.search([('template', '=', self.budget_credit)])
            if budget:
                res['budget_credit'] = budget[0]

        return res


class Account:
    __metaclass__ = PoolMeta
    __name__ = "account.account"

    level = fields.Integer("Level", readonly=True)
    description = fields.Text('Description')
    budget_debit = fields.Many2One('public.budget', "Public Budget Debit",
        states={
            'required': Bool(Eval('parent_budget_debit')),
            'readonly': ~Bool(Eval('parent_budget_debit')),
        },
        domain=[
            If(~Bool(Eval('template')),
               ('parent', 'child_of', [Eval('parent_budget_debit')]),
               ()),
            ('kind', '=', 'view'),
        ],
        depends=['parent_budget_debit'])
    budget_credit = fields.Many2One('public.budget', "Public Budget Credit",
        states={
            'required': Bool(Eval('parent_budget_credit')),
            'readonly': ~Bool(Eval('parent_budget_credit')),
        },
        domain=[
            If(~Bool(Eval('template')),
               ('parent', 'child_of', [Eval('parent_budget_credit')]),
               ()),
            ('kind', '=', 'view'),
        ],
        depends=['parent_budget_credit'])
    parent_budget_debit = fields.Function(fields.Many2One('public.budget',
        'Parent Public Budget Debit',
            states={
                'invisible': True
            }), 'on_change_with_parent_budget_debit')
    parent_budget_credit = fields.Function(fields.Many2One('public.budget',
        'Parent Public Budget Credit',
            states={
                'invisible': True
            }), 'on_change_with_parent_budget_credit')

    @classmethod
    def __setup__(cls):
        super(Account, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('code_uniq', Unique(t, t.code),
                'Code already exists'),
        ]
        cls._error_messages.update({
            'parent_code_not_related': ('the account code %(code)s must be '
                'start with %(parent_code)s'),
            'code_format_invalid': ('the account code %(code)s don\'t match '
                'with the format %(parent_code)sXX'),
        })

        exclude_fields = ['childs','description','note']
        for attr in dir(cls):
            if attr not in exclude_fields:
                tfield = getattr(cls, attr)
                if isinstance(tfield, fields.Field):
                    field = getattr(cls, attr, None)
                    field.states['readonly'] = Or(
                        field.states.get('readonly', False),
                        Bool(Eval('template', [0])))

        cls.template.states['invisible'] = True

        cls.parent.states['required'] = Or(
            cls.parent.states.get('required', False),
            ~Eval('template', False))
        cls.parent.states['readonly'] = Or(
            cls.parent.states.get('readonly', False),
            Bool(Eval('childs', [0])))
        cls.parent.domain += [
            ('kind', '=', 'view'),
            If(~Bool(Eval('template')),
                [
                    'OR',
                    ('template', '=', None),
                    ('childs', 'where', ('template', '=', None)),
                    ('childs', '=', None)
                ], [])
        ]
        cls.parent.depends += ['template', 'childs', 'kind']

    @classmethod
    def create(cls, vlist):
        result = super(Account, cls).create(vlist)
        update_level(cls, result)
        return result

    @classmethod
    def write(cls, accounts, values):
        super(Account, cls).write(accounts, values)
        if 'parent' in values.keys():
            update_level(cls, accounts)

    @classmethod
    def validate(cls, accounts):
        super(Account, cls).validate(accounts)
        cls.check_codes(accounts)

    @classmethod
    def check_codes(cls, accounts):
        def is_valid_format(code, parent_code):
            if ((len(code.split('.')) != (len(parent_code.split('.')) + 1)) or
                (len(code.split('.')[len(code.split('.')) - 1]) == 0) or
                (re.search('^[0-9]{3,}([.][0-9]{2,})*', code).group(0) != code)
            ):
                return False
            return True

        for account in accounts:
            if not account.template:
                if account.code.find(account.parent.code + '.') != 0:
                    cls.raise_user_error('parent_code_not_related', {
                        'code': account.code,
                        'parent_code': account.parent.code + '.',
                    })
                if not is_valid_format(account.code, account.parent.code):
                    cls.raise_user_error('code_format_invalid', {
                        'code': account.code,
                        'parent_code': account.parent.code + '.',
                    })

    @fields.depends('parent')
    def on_change_with_parent_budget_debit(self, name=None):
        return self.get_parent_budget('debit')

    @fields.depends('parent')
    def on_change_with_parent_budget_credit(self, name=None):
        return self.get_parent_budget('credit')

    def get_parent_budget(self, name):
        if self.parent:
            pool = Pool()
            Budget = pool.get('public.budget')
            budget_table = Budget.__table__()
            account_table = self.__class__.__table__()

            if name == 'debit':
                b_condition = (budget_table.id == account_table.budget_debit)
            else:
                b_condition = (budget_table.id == account_table.budget_credit)

            query_parent = self.search([
                ('parent', 'parent_of', [self.parent.id])
            ], query=True)

            query_final = account_table.join(query_parent,
                condition=query_parent.id == account_table.id
                ).join(budget_table, type_='LEFT',
                    condition=b_condition
                ).select(
                budget_table.id.as_('budget_id'),
                where=(budget_table.id != Null),
                order_by=[account_table.right], limit=1
            )

            cursor = Transaction().connection.cursor()
            cursor.execute(*query_final)
            result = cursor.fetchone()
            if result:
                return result[0]
        return None
