from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Or, If
from trytond.transaction import Transaction

from .utils import update_level

__all__ = ['PublicBudgetTemplate', 'PublicBudget', 'CreateChartStart',
    'CreateChartBudget', 'CreateChartSucceed', 'CreateChart']


class PublicBudgetTemplate(ModelView, ModelSQL):
    'Public Budget Template'
    __name__ = 'public.budget.template'
    name = fields.Char('Name', size=None, required=True, select=True)
    code = fields.Char('Code', size=None, required=True, select=True)
    description = fields.Text('Description')
    parent = fields.Many2One(
        'public.budget.template', 'Parent', select=True, ondelete="RESTRICT")
    childs = fields.One2Many('public.budget.template', 'parent', 'Children')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetTemplate, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('code_uniq', Unique(table, table.code),
             'Public budget template code already exists.'),
        ]

    def _get_budget_value(self, budget=None):
        '''
        Set the values for budget creation.
        '''
        res = {}
        if not budget or budget.name != self.name:
            res['name'] = self.name
        if not budget or budget.code != self.code:
            res['code'] = self.code
        if not budget or budget.descripttion != self.description:
            res['description'] = self.description
        if not budget or budget.template != self:
            res['template'] = self.id
        return res

    def create_budget(self, company_id, template2budget=None):
        '''
        Create recursively budgets based on template.
        template2budget is a dictionary with template id as key and budget id
        as value, used to convert template id into budget. The dictionary is
        filled with new budgets
        '''
        pool = Pool()
        Budget = pool.get('public.budget')
        assert self.parent is None

        if template2budget is None:
            template2budget = {}

        def create(templates):
            values = []
            created = []
            for template in templates:
                if template.id not in template2budget:
                    vals = template._get_budget_value()
                    vals['company'] = company_id
                    vals['active'] = True
                    if template.parent:
                        vals['parent'] = template2budget[template.parent.id]
                    else:
                        vals['parent'] = None
                    values.append(vals)
                    created.append(template)

            budgets = Budget.create(values)
            for template, budget in zip(created, budgets):
                template2budget[template.id] = budget.id

        childs = [self]
        while childs:
            create(childs)
            childs = sum((c.childs for c in childs), ())


class PublicBudget(ModelView, ModelSQL):
    'Public Budget'
    __name__ = 'public.budget'

    s_template = {
        'readonly': Bool(Eval('template')),
    }
    d_template = ['template']
    name = fields.Char('Name', size=None, required=True, select=True,
        states=s_template, depends=d_template)
    code = fields.Char('Code', size=None, required=True, select=True,
        states=s_template, depends=['kind', 'template'])
    description = fields.Text(
        'Description', states=s_template, depends=d_template)
    active = fields.Boolean('Active', select=True,
        states=s_template, depends=d_template)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=s_template, depends=d_template, ondelete="RESTRICT")
    parent = fields.Many2One('public.budget', 'Parent', select=True,
        left="left", right="right", ondelete="RESTRICT",
        domain=[
            ('company', '=', Eval('company')),
            ('kind', '=', 'view'),
            If(~Bool(Eval('template')),
               [
                   'OR',
                   ('template', '=', None),
                   ('childs', 'where', ('template', '=', None)),
                   ('childs', '=', None)
               ], [])
        ],
        states={
            'readonly': s_template['readonly'] | Bool(Eval('childs', [0])),
            'required': Eval('kind') != 'view',
        }, depends=['company', 'template', 'childs', 'kind'])
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    level = fields.Integer('Level', readonly=True)
    childs = fields.One2Many('public.budget', 'parent', 'Children',
        states={
            'invisible': Eval('kind') != 'view',
        }, depends=['kind', 'template'])
    kind = fields.Selection([
        ('view', 'View'),
        ('other', 'Other'),
        ], "Kind", states=s_template, depends=d_template, required=True)
    template = fields.Many2One('public.budget.template', 'Template')
    del s_template
    del d_template

    @classmethod
    def __setup__(cls):
        super(PublicBudget, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_code_uniq', Unique(t, t.code),
                'Budget code must be unique'),
        ]
        cls._error_messages.update({
            'budget_kind': (
                'Budget "%(budget)s" has childs of differents kinds.'),
            'parent_code_not_related': ('the account code %(code)s must be '
                'start with %(parent_code)s')
        })
        cls.parent.states['required'] = Or(
            cls.parent.states.get('required', False),
            ~Eval('template', False))

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_active(cls):
        return True

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @classmethod
    def default_kind(cls):
        return 'view'

    def get_rec_name(self, name):
        return self.code + ' - ' + self.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ]

    @fields.depends('parent', 'name', 'code', 'company', 'kind')
    def on_change_parent(self):
        if not self.parent:
            return
        for field in ['name', 'code', 'company', 'kind']:
            if not getattr(self, field):
                setattr(self, field, getattr(self.parent, field))

    @classmethod
    def validate(cls, budgets):
        super(PublicBudget, cls).validate(budgets)
        cls.check_recursion(budgets)
        for budget in budgets:
            if not budget.template:
                if budget.code.find(budget.parent.code + '.') != 0:
                    cls.raise_user_error('parent_code_not_related', {
                        'code': budget.code,
                        'parent_code': budget.parent.code + '.',
                    })

    @classmethod
    def create(cls, vlist):
        result = super(PublicBudget, cls).create(vlist)
        update_level(cls, result)
        return result

    @classmethod
    def write(cls, budgets, values):
        super(PublicBudget, cls).write(budgets, values)
        if 'parent' in values.keys():
            update_level(cls, budgets)


class CreateChartStart(ModelView):
    'Create Chart'
    __name__ = 'budget.create_chart.start'


class CreateChartBudget(ModelView):
    'Create Chart'
    __name__ = 'budget.create_chart.template'
    company = fields.Many2One('company.company', 'Company', required=True)
    budget_template = fields.Many2One('public.budget.template',
            'Budget Template', required=True, domain=[('parent', '=', None)])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class CreateChartSucceed(ModelView):
    'Update Chart'
    __name__ = 'budget.create_chart.succeed'


class CreateChart(Wizard):
    'Create Chart'
    __name__ = 'budget.create_chart'
    start = StateView('budget.create_chart.start',
        'public_ec_base.create_chart_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'template', 'tryton-ok', default=True),
            ])
    template = StateView('budget.create_chart.template',
        'public_ec_base.create_chart_template_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_budget', 'tryton-ok', default=True),
            ])
    create_budget = StateTransition()
    succeed = StateView('budget.create_chart.succeed',
        'public_ec_base.create_chart_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
            ])

    def transition_create_budget(self):
        with Transaction().set_context(company=self.template.company.id):
            budget_template = self.template.budget_template
            company = self.template.company
            template2budget = {}
            budget_template.create_budget(
                company.id,
                template2budget=template2budget)
        return 'succeed'
