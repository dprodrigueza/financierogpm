from decimal import Decimal
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from getpass import getpass

from proteus import config, Model

import xlrd


def create_items(host, port, database, filename):
    username = raw_input('Username: ')
    password = getpass('Password: ')

    config.set_xmlrpc('http://%s:%s@%s:%s/%s/'
        % (username, password, host, port, database))

    wb = xlrd.open_workbook(filename)
    sheet = wb.sheet_by_index(0)

    total = sheet.nrows
    cont = 0

    import pdb; pdb.set_trace()

    Company = Model.get('company.company')
    SIIMModule = Model.get('public.siim.module')
    SIIMItem = Model.get('public.siim.item')
    Product = Model.get('product.template')
    companies = {}
    modules = {}

    for company in Company.find([]):
        companies[company.tax_id] = company

    for module in SIIMModule.find([]):
        modules[module.module_id] = module

    product_item_dict = {}

    import pdb; pdb.set_trace()
    to_save = []
    for row in range(sheet.nrows)[1:]:
        cont += 1
        try:
            product_id = int(sheet.cell(row, 0).value)
            product = Product(product_id)
            item = SIIMItem()
            # item.item_id = int(sheet.cell(row, 0).value)
            item.item_id = 0
            item.description = sheet.cell(row, 1).value
            module_id = int(sheet.cell(row, 2).value)
            item.module = modules[module_id]
            item.type_ = str(int(sheet.cell(row, 3).value))
            item.state = str(int(sheet.cell(row, 4).value))
            if item.state == '1':
                item.active = True
            else:
                item.active = False
            item.state = str(int(sheet.cell(row, 4).value))
            item.calculable = str(int(sheet.cell(row, 5).value))
            item.value = Decimal(str(sheet.cell(row, 6).value)).quantize(4)
            item.tax = str(int(sheet.cell(row, 7).value))
            item.is_tax = str(int(sheet.cell(row, 8).value))
            item.main_item = str(int(sheet.cell(row, 9).value))
            tax_id = int(sheet.cell(row, 10).value)
            item.company = companies[tax_id]
            item.share = str(int(sheet.cell(row, 11).value))
            item.billable = str(int(sheet.cell(row, 12).value))
            item.save()
            # to_save.append(item)
            product.is_item = True
            product.item = item
            product.save()
        except Exception as e:
            print(str(e))
            return
        print('%s de %s, %d %d' % (cont, total, item.id, product.id))


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', dest='host',
        help='host', default='localhost')
    parser.add_argument('--port', dest='port',
        help='port', default='8000')
    parser.add_argument('--database', dest='database',
        help='database', required=True)
    parser.add_argument('--filename', dest='filename',
        help='filename', required=True)
    options = parser.parse_args()
    create_items(options.host, options.port, options.database, options.filename)
