from trytond.transaction import Transaction


def update_level(cls, records):
    table = cls.__table__()
    cursor = Transaction().connection.cursor()
    for record in records:
        level = cls.search([
            ('parent', 'parent_of', [record.id])
        ], count=True) - 1
        cursor.execute(*table.update([table.level], [level],
            where=table.id == record.id))
        if record.childs:
            update_level(cls, record.childs)
