from collections import defaultdict

from trytond.pool import Pool, PoolMeta


__all__ =[
    'TransparencyDistributiveProcess'
]


class TransparencyDistributiveProcess(metaclass=PoolMeta):
    __name__ = 'transparency.distributive.process'


    @classmethod
    def get_distributive_by_process(
            cls, period, include_finalized_contracts=False):
        pool = Pool()
        Contract = pool.get('company.contract')
        PositionProcess = pool.get('company.position.process')

        result = defaultdict(lambda: [])

        # Get processes
        processes = PositionProcess.search([], order=[('sequence', 'ASC')])
        for process in processes:
            result[process.name] = []

        no_process_txt = 'N/A'

        # Get contratcs
        _domain = [('start_date', '<=', period.end_date)]
        if include_finalized_contracts:
            _domain.append(['OR',
                ('state', '=', 'done'),
                [
                    ('state', '=', 'finalized'),
                    ('end_date', '>=', period.start_date),
                    ('end_date', '<=', period.end_date)
                ]
                ])
        else:
            _domain.append(['OR',
                ('state', '=', 'done'),
                [
                    ('state', '=', 'finalized'),
                    ('end_date', '>', period.end_date)
                ]
                ])
        contracts = Contract.search(
            _domain,
            order=[
                ('department.name', 'ASC'),
                ('employee.party', 'ASC')
            ])
        for contract in contracts:
            have_process = False
            process = contract.department.process
            if process and process.name:
                result[process.name].append(contract)
                have_process = True
            if not have_process:
                result[no_process_txt].append(contract)

        return result