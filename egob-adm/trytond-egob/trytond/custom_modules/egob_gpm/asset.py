from decimal import Decimal

from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool
from trytond.pyson import Eval, Or, And
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition

__all__ = [
    'AssetMultiCreateStart', 'AssetMultiCreateSucceed', 'AssetMultiCreate', 
]


class AssetMultiCreateStart(ModelView):
    'Asset Multi Create Start'
    __name__ = 'asset.multi.create.start'

    name = fields.Char('Nombre')
    brand = fields.Char('Marca')
    model = fields.Char('Modelo')
    life_time = fields.Integer('Vida útil')
    purchase_date = fields.Date('Fecha de compra')
    input_date = fields.Date('Fecha de ingreso')
    invoice_number = fields.Char('Factura')
    asset_type = fields.Selection([
        ('artistic', 'Artísticos'),
        ('biological', 'Biológicos'),
        ('property', 'Edificios, Locales y Residencias'),
        ('book', 'Libros y colecciones'),
        ('vehicle', 'Vehículos'),
        ('movable_property', 'Bienes Muebles'),
        ('furniture', 'Mobiliario'),
        ('machinery', 'Maquinaria y Equipos'),
        ('system', 'Equipos y Sistemas Informáticos'),
        ('tool', 'Herramientas'),
        ('spare_parts', 'Partes y Repuestos'),
        ('ground', 'Terrenos'),
        ('unspecified', 'No especificado'),
        ], 'Tipo de Activo')
    possesion_type = fields.Selection([
        ('own', 'Propio'),
        ('leasing', 'Arrendamiento'),
        ('pignoracion', 'Pignoracion')
        ], 'Tipo de posesión')
    possesion_type = fields.Selection([
        ('own', 'Propio'),
        ('leasing', 'Arrendamiento'),
        ('pignoracion', 'Pignoracion')
        ], 'Tipo de poseción')
    location = fields.Many2One('company.location', 'Ubicación')
    observations = fields.Text('Observaciones')
    value = fields.Numeric('Valor de compra')
    product = fields.Many2One('product.product', 'Producto',
        domain=[
            ('type', 'in', ['assets']),
            ])
    kind = fields.Selection(
        [
            ('main', 'Principal'),
            ('component', 'Componente'),
        ], 'Clase')
    category = fields.Many2One('asset.category', 'Categoría')
    asset_state = fields.Selection([
        ('good', 'Bueno'),
        ('regular', 'Regular'),
        ('bad', 'Malo'),
        ('unspecified', 'Sin especificar'),
        ], 'Estado del Activo')
    employee = fields.Many2One('company.employee', 'Empleado')
    type = fields.Selection([
        ('bld', 'BLD'),
        ('bca', 'BCA'),
        ], 'Tipo')
    quantity = fields.Integer('Cantidad')

    @fields.depends('category','life_time')
    def on_change_category(self, name=None):
        if self.category:
            if self.category.life_time_reference:
                self.life_time = self.category.life_time_reference


class AssetMultiCreateSucceed(ModelView):
    'Asset Multi Create Succeed'
    __name__ = 'asset.multi.create.succeed'


class AssetMultiCreate(Wizard):
    'Asset Multi Create'
    __name__ = 'asset.multi.create'

    start = StateView('asset.multi.create.start',
        'egob_gpm.asset_multi_create_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Generar activos', 'generate_assets', 'tryton-executable'),
        ])
    state = fields.Selection([
        ('draft', 'Borrador'),
    ],'Estado')
    generate_assets = StateTransition()
    succeed = StateView('asset.multi.create.succeed',
        'egob_gpm.asset_multi_create_succeed_view_form', [
            Button('OK', 'end', 'tryton-ok', default=True),
        ])

    def transition_generate_assets(self):
        pool = Pool()
        Asset = pool.get('asset')
        AssetOwner = pool.get('asset.owner')
        Location = pool.get('asset.location')
        Date = Pool().get('ir.date')
        Sequence = Pool().get('ir.sequence')
        Config = pool.get('asset.configuration')
        config = Config(1)
        today = Date.today()
        i = 0
        while i < self.start.quantity:
            i += 1
            asset = Asset()
            asset.name = self.start.name
            asset.brand = self.start.brand
            asset.model = self.start.model
            asset.life_time = self.start.life_time
            asset.purchase_date = self.start.purchase_date
            asset.input_date = self.start.input_date
            asset.invoice_number = self.start.invoice_number
            asset.asset_type = self.start.asset_type
            asset.type = self.start.type
            asset.possesion_type = self.start.possesion_type
            asset.observations = self.start.observations
            asset.value = self.start.value
            asset.purchase_option_value = self.start.value
            asset.kind = self.start.kind
            asset.asset_state = self.start.asset_state
            asset.product = self.start.product
            asset.category = self.start.category
            if self.start.value:
                aux = (self.start.value * Decimal(config.value_residual)).quantize(Decimal('0.01'))
                asset.residual_value = aux
            else:
                asset.residual_value = 0.0


            sequence = self.start.category.sequence
            asset.code = Sequence.get_id(sequence.id)
            Asset.save([asset])

            location = Location()
            location.location = self.start.location
            location.asset = asset
            Location.save([location])
            # asset.locations.append(location)

            owner = AssetOwner()
            owner.employee = self.start.employee
            owner.owner = self.start.employee.party
            owner.from_date = today
            owner.asset = asset
            AssetOwner.save([owner])
            # asset.owners.append(owner)
            
            # asset.employee = self.start.employee

        # config_wizard = Wizard('ir.module.config_wizard')
        # config_wizard.execute('action')
        # UpdateWizard = pool.get('parking.vehicle.update.mileage', type='wizard')
        # session_id, _, _ = UpdateWizard.create()
        # for line in self.start.lines:
        #      with Transaction().set_context(active_ids=[line.vehicle.id],
        #             active_id=line.vehicle.id):
        #         update_wizard = UpdateWizard(session_id)
        #         update_wizard.start.new_usage = line.new_usage
        #         update_wizard.start.new_mileage = line.new_mileage
        #         update_wizard.start.description = line.description
        #         update_wizard.start.description = line.description
        #         update_wizard.transition_change()
        return 'succeed'


