# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.egob_gpm.tests.test_egob_gpm import suite
except ImportError:
    from .test_egob_gpm import suite

__all__ = ['suite']
