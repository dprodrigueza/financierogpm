from urllib.parse import urljoin

from collections import defaultdict
from decimal import Decimal
import json
from stripe.http_client import requests

import pendulum

from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, Bool
from trytond.config import config, logger
from trytond.wizard import Wizard
from trytond.modules.hr_ec.company import ActionOfTheStaffSituationMixin


__all__ = [
    'Employee', 'Department', 'Contract', 'UploadDailingRegister',
    'UploadDailingRegisterStart', 'ActionOfTheStaffSituationActual',
    'ActionOfTheStaffSituationProposed', 'CommissionSubrogation'
]

service = 'marcacionbiometrico/'
authorization = 'Bearer d5e4d388caa44284b2e249fcf6aa211d9fb0d3ed5d7e4d15ab3673b3a0042566'
invisible_contract_professional = {'invisible': (Eval('contract_type', -1)==7)}

class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    @classmethod
    def get_years_antiguaty(cls, employees, names):
        years = defaultdict(lambda: 0)
        Date = Pool().get('ir.date')
        for employee in employees:
            if employee.antiquity_date:
                temp_year = employee.antiquity_date.year
                if temp_year < 2003:
                    temp_year = 2003
                diff = Date.today().year - temp_year
                years[employee.id] = (diff)
            else:
                years[employee.id] = 0
        return {'years_antiguaty': years}


class Department(metaclass=PoolMeta):
    __name__ = 'company.department'

    process = fields.Many2One('company.position.process', 'Proceso')


class Contract(metaclass=PoolMeta):
    __name__ = 'company.contract'

    budget_contract = fields.Char(
        'Partida presupuestaria',
        states={
            'readonly': Eval('state') != 'draft',
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super(Contract, cls).__setup__()
        cls.occupational_group.states.update(invisible_contract_professional)
        cls.hierarchical_level.states.update(invisible_contract_professional)
        cls.hour_cost.states.update(invisible_contract_professional)
        cls.caution.states.update(invisible_contract_professional)

    @classmethod
    def view_attributes(cls):
        return super(Contract, cls).view_attributes() + [
            ('//page[@id="medical_benefit"]', 'states', #Beneficios medicos
             invisible_contract_professional
            ),
            ('//page[@id="min_salary_rate"]', 'states', #Rol de pagos
             invisible_contract_professional
            ),
            ('//page[@id="benefits"]', 'states', #Beneficios de ley
            invisible_contract_professional
            ),
            ('//page[@id="account_data"]', 'states', #Contabilidad
             invisible_contract_professional
            ),
            ('//page[@id="income_and_deductions"]', 'states', #Ingresos o Deducciones
             invisible_contract_professional
            ),
            ('//page[@id="social_secure"]', 'states', #Seguro social
             invisible_contract_professional
            ),
        ]


class UploadDailingRegisterStart(metaclass=PoolMeta):
    __name__ = 'company.upload.dialing.register.start'

    biometric_code = fields.Boolean('Obtener del dispositivo biometrico?')
    start_date = fields.Date(
        'Fecha inicio',
        domain=[
            ('start_date', '<=', Eval('end_date')),
        ],
        states={
            'invisible': ~Bool(Eval('biometric_code')),
            'required': Bool(Eval('biometric_code')),
        }, depends=['end_date', 'biometric_code'])
    end_date = fields.Date(
        'Fecha fin',
        domain=[
            ('end_date', '>=', Eval('start_date')),
        ],
        states={
            'invisible': ~Bool(Eval('biometric_code')),
            'required': Bool(Eval('biometric_code')),
        }, depends=['start_date', 'biometric_code'])


    @classmethod
    def __setup__(cls):
        super(UploadDailingRegisterStart, cls).__setup__()
        cls.source_file.states.update({'required': ~Eval('biometric_code')})

    @staticmethod
    def default_biometric_code():
        return False

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class UploadDailingRegister(metaclass=PoolMeta):
    __name__ = 'company.upload.dialing.register'


    @classmethod
    def __setup__(cls):
        super(UploadDailingRegister, cls).__setup__()
        cls._error_messages.update({
            'no_employee': (
                'Se ha encontrado el siguiente personal estan con otro '
                'codigo biometrico o con un contrato no vigente y son:'
                '\n\n%(error_messagge)s\n\n'),
        })

    def transition_import_(self):
        if not self.start.biometric_code:
            super(UploadDailingRegister, self).transition_import_()
        api_base_url = config.get('ws', 'url_service')
        params = f"?fechadesde={self.start.start_date.strftime('%d/%m/%Y')}&fechahasta={self.start.end_date.strftime('%d/%m/%Y')}"
        url = urljoin(api_base_url, service)
        url += params

        try:
            dailings = requests.get(
                url, headers={'Authorization': authorization})
            if dailings.status_code != 200:
                self.raise_user_error('''Error de conexion''')
            else:
                list_api = json.loads(dailings.content)
                pool = Pool()
                Employee = pool.get('company.employee')
                DialingRegister = pool.get('company.dialing.register')
                DialingRegisterDate = pool.get('company.dialing.register.date')
                DialingRegisterHour = pool.get('company.dialing.register.hour')

                employees = {}
                for employee in Employee.search([('contract_state', 'not in', [
                    'draft', 'cancel'])]):
                    if employee.biometric_code:
                        employees[employee.biometric_code] = employee

                dates = []
                hours = []
                ids_emp = []

                for row in list_api:
                    code_biometric = str(row.get('cedula').strip())

                    result = self.get_dialing_date_time(row.get('fecha'))
                    date = result['date']
                    hour = result['time']
                    if not date or not hour:
                        self.raise_user_error('datetime_error', {
                            'datetime': str(row.get('fecha')),
                            'code': code_biometric
                        })

                    dates.append(date)
                    hours.append(hour)
                    if employees.get(code_biometric):
                        ids_emp.append(employees.get(code_biometric).id)

                # Eliminamos los repetidos
                dates = list(set(dates))
                hours = list(set(hours))
                ids_emp = list(set(ids_emp))

                dialing_registers = {}
                for dialing in DialingRegister.search([()]):
                    dialing_registers[dialing.employee.biometric_code] = dialing

                dialing_reg_date = {}
                for dialing_date in DialingRegisterDate.search([
                    ('dialing_register.employee', 'in', ids_emp),
                    ('date', 'in', dates)]):
                    code_bio = dialing_date.dialing_register.biometric_code
                    key = f"{code_bio}-{dialing_date.date}"
                    dialing_reg_date[key] = dialing_date

                dialing_reg_hour = {}
                for dialing_hour in DialingRegisterHour.search([
                    ('dialing_register_date.dialing_register.employee', 'in',
                     ids_emp),
                    ('dialing_register_date.date', 'in', dates),
                    ('hour', 'in', hours)]):
                    dialing_reg = dialing_hour.dialing_register_date.dialing_register
                    key = f"{dialing_reg.biometric_code}-"
                    key += f"{dialing_hour.dialing_register_date.date}-"
                    key += f"{dialing_hour.hour}"
                    dialing_reg_hour[key] = dialing_hour

                list_dialig_regiter = []
                list_dialig_regiter_date = []
                list_dialig_regiter_hour = []
                list_employees_not_register = {}

                error_messagge = ''

                for row in list_api:
                    code_biometric = str(row.get('cedula').strip())
                    result = self.get_dialing_date_time(row.get('fecha'))
                    date = result['date']
                    hour = result['time']
                    if employees.get(code_biometric):
                        if dialing_registers.get(code_biometric):
                            dialing = dialing_registers.get(code_biometric)
                        else:
                            dialing = DialingRegister()
                            employee = employees[code_biometric]
                            dialing.employee = employee
                            dialing.company = employee.company
                            dialing.biometric_code = code_biometric
                            dialing_registers[code_biometric] = dialing
                            list_dialig_regiter.append(dialing)

                        if dialing_reg_date.get(f"{code_biometric}-{date}"):
                            dialing_rd = dialing_reg_date.get(
                                f"{code_biometric}-{date}")
                        else:
                            dialing_rd = DialingRegisterDate()
                            dialing_rd.dialing_register = dialing
                            dialing_rd.date = date
                            dialing_reg_date[f"{code_biometric}-{date}"] = dialing_rd
                            list_dialig_regiter_date.append(dialing_rd)

                        if not dialing_reg_hour.get(f"{code_biometric}-{date}-{hour}"):
                            dialing_rh = DialingRegisterHour()
                            dialing_rh.dialing_register_date = dialing_rd
                            dialing_rh.hour = hour
                            dialing_reg_hour[f"{code_biometric}-{date}-{hour}"] = (
                                dialing_rh)
                            list_dialig_regiter_hour.append(dialing_rh)

                    else:
                        if not list_employees_not_register.get(code_biometric):
                            list_employees_not_register[code_biometric] = 1
                            error_messagge += (
                                f"{row.get('nombre')}: {code_biometric}\n")

                if len(error_messagge) > 0:
                    self.raise_user_warning(
                        'no_employee', 'no_employee',{
                            'error_messagge': error_messagge
                        }),



                DialingRegister.save(list_dialig_regiter)
                DialingRegisterDate.save(list_dialig_regiter_date)
                DialingRegisterHour.save(list_dialig_regiter_hour)

        except (requests.HTTPError, requests.ConnectionError) as e:
            logger.error(e.message)
            self.raise_user_error('''Error de conexion''')
        return 'open_dialing'


class StaffSituationToBudgetMixin(ActionOfTheStaffSituationMixin):
    'Action of staff Situation to Budget'

    budget_contract = fields.Function(
        fields.Char('Partida presupuestaria'),
        'on_change_with_budget_contract'
    )

    @fields.depends('contract')
    def on_change_with_budget_contract(self, name=None):
        if self.contract and self.contract.budget_contract:
            return self.contract.budget_contract
        return 'N/A'


class ActionOfTheStaffSituationActual(
    StaffSituationToBudgetMixin, metaclass=PoolMeta):
    __name__ = 'company.action.staff.situation.actual'


class ActionOfTheStaffSituationProposed(
    StaffSituationToBudgetMixin, metaclass=PoolMeta):
    __name__ = 'company.action.staff.situation.proposed'


class CommissionSubrogation(metaclass=PoolMeta):
    __name__ = 'company.commission.subrogation'


    @fields.depends('salary_commission', 'salary_subrogation', 'type_')
    def on_change_with_salary_difference(self, name=None):
        if self.salary_commission and self.salary_subrogation and self.type_:
            if self.type_.name == 'Encargo':
                return self.salary_subrogation
            elif self.type_.name == 'Subrogación':
                return self.salary_subrogation - self.salary_commission
            return Decimal('0')
        return Decimal('0')