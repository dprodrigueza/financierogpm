from decimal import Decimal
from dateutil.relativedelta import relativedelta
from calendar import monthrange
from datetime import datetime

from trytond.pool import PoolMeta, Pool


__all__ = [
    'EmployeeLoan'
]


CENT = Decimal('0.01')


class EmployeeLoan(metaclass=PoolMeta):
    __name__ = 'company.employee.loan'

    @classmethod
    def calculate_payment_capacity(cls, loan):
        '''
        Ingresos de RMU, SMU, ENCARGO, SUBROGACION
        Gastos de APORTE PERSONAL, PH, PQ, IESS ESPOSA
        '''


        Payslip = Pool().get('payslip.payslip')
        total_amount = Decimal("0")
        num_payslips = 0
        if loan.start_date and loan.employee:
            # End date
            end_date = loan.start_date - relativedelta(months=1)
            last_day = monthrange(end_date.year, end_date.month)[1]
            end_date = datetime(
                year=end_date.year,
                month=end_date.month,
                day=last_day).date()

            # Start date
            start_date = loan.start_date - relativedelta(months=3)
            start_date = datetime(
                year=start_date.year,
                month=start_date.month,
                day=1).date()

            # Find payslips history
            payslips = Payslip.search([
                ('period.start_date', '>=', start_date),
                ('period.end_date', '<=', end_date),
                ('employee', '=', loan.employee),
                ('state', 'in', ['done', 'posted']),
                ('template.type', '=', 'payslip'),
                ('template.name', 'ilike', 'rol mensual%'),
            ])
            if payslips:
                num_payslips = len(payslips)
                total_income = Decimal("0")
                total_deduction = Decimal("0")
                for payslip in payslips:
                    for line in payslip.lines:
                        if line.rule.id in [1, 51, 5, 6]:
                            total_income += line.amount
                        if line.rule.id in [40, 14, 23, 24, 45]:
                            total_deduction += line.amount
                total_amount = total_income - total_deduction

        # Payment capacity
        capacity = Decimal("0")
        if num_payslips > 0:
            capacity = total_amount / Decimal(str(num_payslips))
        capacity = capacity if capacity > 0 else Decimal('0.00')
        return Decimal(str(capacity)).quantize(CENT)