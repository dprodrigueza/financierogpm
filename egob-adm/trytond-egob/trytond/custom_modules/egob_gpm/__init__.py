# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import product
from . import stock
from . import company
from . import asset
from . import hr
from . import report
from . import project
from . import budget

__all__ = ['register', 'routes']


def register():
    Pool.register(
        company.Employee,
        company.Department,
        company.Contract,
        company.UploadDailingRegisterStart,
        company.ActionOfTheStaffSituationProposed,
        company.ActionOfTheStaffSituationActual,
        company.CommissionSubrogation,
        product.ProductTemplate,
        stock.ShimpentInInvoiceQuery,
        asset.AssetMultiCreateStart,
        asset.AssetMultiCreateSucceed,
        hr.EmployeeLoan,
        project.Work,
        budget.PublicBudgetCertificate,
        module='egob_gpm', type_='model')
    Pool.register(
        asset.AssetMultiCreate,
        company.UploadDailingRegister,
        module='egob_gpm', type_='wizard')
    Pool.register(
        stock.ShipmentInternalEgressMaintenance,
        stock.ShipmentInternalPetreusMaterial,
        stock.ShipmentInternalHumanDevelopment,
        stock.ShipmentInternalReturn,
        stock.ShipmentInternalLocationReasignament,
        stock.ShimpentInInvoiceQueryReport,
        report.TransparencyDistributiveProcess,
        module='egob_gpm', type_='report')
