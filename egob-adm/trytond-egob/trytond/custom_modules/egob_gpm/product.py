from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool, PoolMeta

__all__ = [
    'ProductTemplate'
]

class ProductTemplate(metaclass=PoolMeta):
    __name__ = "product.template"

    reference_code = fields.Char('Referencia')
    application_name = fields.Char('Aplicación')
    location_code = fields.Char('Ubicación')
