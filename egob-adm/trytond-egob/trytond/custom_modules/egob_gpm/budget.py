from collections import defaultdict
from decimal import Decimal

from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.tools import cursor_dict


class PublicBudgetCertificate(metaclass=PoolMeta):
    'Public Budget Certificate'
    __name__ = 'public.budget.certificate'

    @classmethod
    def check_availability(cls, certificates):
        pool = Pool()
        Budget = pool.get('public.budget')
        PlanningUnit = pool.get('public.planning.unit')
        PlanningCard = pool.get('public.planning.unit.card')
        BudgetCard = pool.get('public.budget.card')
        ProjectWork = pool.get('project.work')
        pr = defaultdict(lambda: {'max': 0, 'cert': 0})
        pw = defaultdict(lambda: {'max': 0, 'cert': 0})
        budgets = defaultdict(lambda: 0)
        for certificate in certificates:
            for line in certificate.lines:
                projects_t = PlanningUnit.search([
                    ('type', '=', 'project'),
                    ('parent', 'parent_of', line.budget.activity.id)
                ])
                p_ids = []
                for p in projects_t:
                    if p.project_work:
                        p_ids.append(p.project_work.id)
                projects_w = ProjectWork.search([
                    ('id', 'in', p_ids)
                ])
                if projects_w:
                    for proj in projects_w:
                        pw[proj.id] = proj.total_amount

                if projects_t:
                    for proj in projects_t:
                        if pr[proj.id]['max'] == 0:
                            if proj.project_work is None:
                                cls.raise_user_error('without_project_work', {
                                    'code': proj.code,
                                    'proj': proj.name,
                                })
                            codified = cls.get_codified_project(proj)
                            if proj.ceiling_budget <= codified:  # noqa
                                pr[proj.id]['max'] = codified
                            else:
                                pr[proj.id]['max'] = proj.ceiling_budget
                        pr[proj.id]['cert'] += line.amount
                budgets[line.budget.id] += line.amount
        messages = ""
        for budget in Budget.browse(list(budgets.keys())):
            budget_card, = BudgetCard.search([('id', '=', budget.id)])
            if budgets[budget.id] > budget_card.certified_pending:
                budget_ = budget.rec_name
                available = budget_card.certified_pending
                need = budgets[budget.id]
                messages += (f"Partida: {budget_} " +
                    f"Disponible: {available} Necesario: {need} " +
                    f"Diferencia: {need - available}\n")
        if messages != "":
            cls.raise_user_error('unavailable', {
                'budget_names': messages,
                })
        cards = PlanningCard.search([('id', 'in', list(pr.keys()))])
        for card in cards:
            if (pr[card.id]['max'] < (
                    card.certified_amount + pr[card.id]['cert'])):
                cls.raise_user_error('unavailable_project', {
                    'project': '%s - %s' % (card.code, card.name),
                    'ceiling_budget': pr[card.id]['max'],
                    })

    @classmethod
    def get_codified_project(cls, project):
        codified = Decimal('0.0')
        pool = Pool()
        Date = pool.get('ir.date')
        end_date = Date.today()
        #context = Transaction().context
        Card = pool.get('public.budget.card')
        params = {
            'program': project.parent.id,
            'project': project.id,
            'end_date': end_date
        }
        with Transaction().set_context(**params):
            card = Card.table_query()
            query = card.select(
                card.codified_amount,
                where=card.level == 5
            )
            cursor = Transaction().connection.cursor()
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                codified += row['codified_amount']
            
        return codified