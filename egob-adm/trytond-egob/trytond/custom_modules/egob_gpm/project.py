from trytond.pool import PoolMeta, Pool

class Work(metaclass=PoolMeta):
    __name__ = 'project.work'

    def get_budgets(self, name):
        super(Work, self).get_budgets(name)
        if self.ppu_project or self.is_simulation:
            if self.ppu_project:
                ppu = self.ppu_project
            if self.is_simulation and self.origin.ppu_project:
                ppu = self.origin.ppu_project
            pool = Pool()
            Budget = pool.get('public.budget')
            PPU = pool.get('public.planning.unit')
            Item = pool.get('project.financial.item')
            if self.deliverables:
                tasks = [c.id for c in self.deliverables]
                items = Item.search([
                    ('parent', 'child_of', tasks)
                ])
                if items:
                    tasks += [i.id for i in items]
                activities = PPU.search([
                    ('task', 'in', tasks)
                ]) 
            else:
                ppus = [p.id for p in ppu]
                activities = PPU.search([
                    ('parent', 'in', ppus)
                ])
            if activities:
                ppu_ids = [p.id for p in activities]
                budgets = Budget.search([
                    ('activity', 'in', ppu_ids)
                ])
                if budgets:
                    return [b.id for b in budgets]
                else: return None
        return None