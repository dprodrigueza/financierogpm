from trytond.pyson import Eval
from decimal import Decimal
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import ModelView, fields, ModelSQL
from trytond.wizard import Wizard, StateView, StateTransition, StateReport, Button
from trytond.modules.hr_ec.company import CompanyReportSignature

from sql import Window, Literal, Union, Cast, Null
from sql.functions import RowNumber, CurrentTimestamp, ToNumber, ToChar
from sql.aggregate import Sum
from sql.operators import Concat
from sql.conditionals import Case, Coalesce
import numpy as np

__all__ = ['ShipmentInternalEgressMaintenance']

class ShipmentInternalEgressMaintenance(CompanyReportSignature):
    __name__ = 'stock.shipment.internal.egress.maintenance'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShipmentInternalEgressMaintenance, cls).get_context(records, data)
        context['subtotal'] = cls._subtotal
        context['subtotal_base'] = cls._subtotal_base
        context['Decimal'] = Decimal
        return context

    @classmethod
    def _subtotal(cls, move):
        return (Decimal(str(move.base_unit_price or move.cost_price or Decimal('0.0'))) *
               Decimal(str(move.quantity or 0)))

    @classmethod
    def _subtotal_base(cls, move):
        if move.base_unit_price:
            return (Decimal(str(move.base_unit_price)) *
                Decimal(str(move.quantity or 0)))
        else:
            return (Decimal(str(move.unit_price or 0)) *
                Decimal(str(move.quantity or 0)))


class ShipmentInternalPetreusMaterial(CompanyReportSignature):
    __name__ = 'stock.shipment.internal.petreus.material'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShipmentInternalPetreusMaterial, cls).get_context(records, data)
        context['subtotal'] = cls._subtotal
        context['subtotal_base'] = cls._subtotal_base
        context['Decimal'] = Decimal
        return context

    @classmethod
    def _subtotal(cls, move):
        return (Decimal(str(move.base_unit_price or move.cost_price or Decimal('0.0'))) *
               Decimal(str(move.quantity or 0)))

    @classmethod
    def _subtotal_base(cls, move):
        if move.base_unit_price:
            return (Decimal(str(move.base_unit_price)) *
                Decimal(str(move.quantity or 0)))
        else:
            return (Decimal(str(move.unit_price or 0)) *
                Decimal(str(move.quantity or 0)))


class ShipmentInternalHumanDevelopment(CompanyReportSignature):
    __name__ = 'stock.shipment.internal.human.development'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShipmentInternalHumanDevelopment, cls).get_context(records, data)
        context['subtotal'] = cls._subtotal
        context['subtotal_base'] = cls._subtotal_base
        context['Decimal'] = Decimal
        return context

    @classmethod
    def _subtotal(cls, move):
        return (Decimal(str(move.base_unit_price or move.cost_price or Decimal('0.0'))) *
               Decimal(str(move.quantity or 0)))

    @classmethod
    def _subtotal_base(cls, move):
        if move.base_unit_price:
            return (Decimal(str(move.base_unit_price)) *
                Decimal(str(move.quantity or 0)))
        else:
            return (Decimal(str(move.unit_price or 0)) *
                Decimal(str(move.quantity or 0)))


class ShipmentInternalReturn(CompanyReportSignature):
    __name__ = 'stock.shipment.internal.return'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShipmentInternalReturn, cls).get_context(records, data)
        context['subtotal'] = cls._subtotal
        context['subtotal_base'] = cls._subtotal_base
        context['Decimal'] = Decimal
        return context

    @classmethod
    def _subtotal(cls, move):
        return (Decimal(str(move.base_unit_price or move.cost_price or Decimal('0.0'))) *
               Decimal(str(move.quantity or 0)))

    @classmethod
    def _subtotal_base(cls, move):
        if move.base_unit_price:
            return (Decimal(str(move.base_unit_price)) *
                Decimal(str(move.quantity or 0)))
        else:
            return (Decimal(str(move.unit_price or 0)) *
                Decimal(str(move.quantity or 0)))


class ShipmentInternalLocationReasignament(CompanyReportSignature):
    __name__ = 'stock.shipment.location.reasignament'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShipmentInternalLocationReasignament, cls).get_context(records, data)
        context['subtotal'] = cls._subtotal
        context['subtotal_base'] = cls._subtotal_base
        context['Decimal'] = Decimal
        return context

    @classmethod
    def _subtotal(cls, move):
        return (Decimal(str(move.base_unit_price or move.cost_price or Decimal('0.0'))) *
               Decimal(str(move.quantity or 0)))

    @classmethod
    def _subtotal_base(cls, move):
        if move.base_unit_price:
            return (Decimal(str(move.base_unit_price)) *
                Decimal(str(move.quantity or 0)))
        else:
            return (Decimal(str(move.unit_price or 0)) *
                Decimal(str(move.quantity or 0)))


class ShimpentInInvoiceQuery(ModelSQL, ModelView):
    "Shimpent In Invoice Query"
    __name__ = 'stock.shipment.in.invoice.query'

    shipment_number = fields.Char('Nro. Ingreso', readonly=True)
    effective_date = fields.Date('Fecha efectiva', readonly=True)
    provider = fields.Many2One('party.party', 'Proveedor', readonly=True)
    total = fields.Numeric('Total', digits=(16,2), readonly=True)
    invoice = fields.Many2One('account.invoice', 'Factura', readonly=True)
    invoice_state = fields.Selection([
        ('draft', 'Borrador'),
        ('validated', 'Validada'),
        ('posted', 'Contabilizada'),
        ('paid', 'Pagada'),
        ('cancel', 'Cancelada'),
        ], 'Estado de factura', readonly=True)
    in_type = fields.Selection([
        ('goods', 'Productos'),
        ('assets', 'Activos'),
        ('service', 'Servicios'),
        ], 'Tipo de ingreso', readonly=True)
    # supplier_establishment
    # supplier_emission_point
    # reference
    to_location = fields.Many2One('stock.location', 'A ubicación', readonly=True)
    
    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice.__table__()
        InvoiceLine = pool.get('account.invoice.line')
        invoice_line = InvoiceLine.__table__()
        Move = pool.get('stock.move')
        move = Move.__table__()
        Shipment = pool.get('stock.shipment.in')
        shipment = Shipment.__table__()
        Party = pool.get('party.party')
        party = Party.__table__()
        Location = pool.get('stock.location')
        location = Location.__table__()
        Template = pool.get('product.template')
        template = Template.__table__()
        Product = pool.get('product.product')
        product = Product.__table__()

        location_query = shipment.join(move,
            condition=move.shipment == Concat('stock.shipment.in,',shipment.id)
        ).join(location,
            condition=move.to_location == location.id
        ).select(
            shipment.number.as_('number'),
            location.id.as_('location'),
            where=(move.base_unit_price == Null),
            group_by=[shipment.number,location.id]
        )
        
        query = invoice.join(invoice_line,
            condition=invoice.id == invoice_line.invoice
        ).join(move,
            condition=Concat('stock.move,',move.id) == invoice_line.origin 
        ).join(shipment,
            condition=Concat('stock.shipment.in,',shipment.id) == move.shipment
        ).join(party,
            condition=party.id == shipment.supplier 
        ).join(location_query,
            condition=location_query.number == shipment.number
        ).join(product,
            condition=product.id == move.product
        ).join(template,
            condition=template.id == product.template
        ).select(
            shipment.number.as_('shipment_number'),
            shipment.effective_date.as_('effective_date'),
            party.id.as_('provider'),
            Sum(move.base_unit_price * move.quantity).as_('total'),
            invoice.id.as_('invoice'),
            invoice.state.as_('invoice_state'),
            location_query.location.as_('to_location'),
            template.type.as_('in_type'),
            group_by=[
                shipment.number, shipment.effective_date, party.id, 
                invoice.id, invoice.state, location_query.location,
                template.type
            ]
        )
        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.shipment_number,
            query.effective_date,
            query.provider,
            query.total,
            query.invoice,
            query.invoice_state,
            query.to_location,
            query.in_type
        )

    @classmethod
    def __setup__(cls):
        super(ShimpentInInvoiceQuery, cls).__setup__()
        cls._order = [
            ('effective_date', 'DESC')
        ]

class ShimpentInInvoiceQueryReport(CompanyReportSignature):
    'Shimpent In Invoice Query Report'
    __name__ = 'stock.shipment.in.invoice.query.report'

    @classmethod
    def get_context(cls, records, data):
        context = super(ShimpentInInvoiceQueryReport, cls).get_context(records, data)

        asset_locations_dict = {}
        for record in records:
            if record.in_type != 'assets':
                continue
            key = record.to_location.name
            if not key in asset_locations_dict.keys():
                asset_locations_dict[key] = []
            row = {}
            row['shipment_number'] = record.shipment_number
            row['effective_date'] = record.effective_date
            row['provider'] = record.provider
            row['total'] = record.total
            row['invoice'] = record.invoice
            row['invoice_state'] = record.invoice_state
            asset_locations_dict[key].append(row)
        context['asset_locations_dict'] = asset_locations_dict

        product_locations_dict = {}
        for record in records:
            if record.in_type != 'goods':
                continue
            key = record.to_location.name
            if not key in product_locations_dict.keys():
                product_locations_dict[key] = []
            row = {}
            row['shipment_number'] = record.shipment_number
            row['effective_date'] = record.effective_date
            row['provider'] = record.provider
            row['total'] = record.total
            row['invoice'] = record.invoice
            row['invoice_state'] = record.invoice_state
            product_locations_dict[key].append(row)
        context['product_locations_dict'] = product_locations_dict
        return context