from pathlib import Path
import csv
from collections import OrderedDict, defaultdict

from trytond.config import config
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import (grouped_slice, reduce_ids, cursor_dict, Literal,
    file_open)

from sql.conditionals import Case, Coalesce
from sql import Null

from calendar import different_locale, month_name, monthrange
from dateutil.relativedelta import relativedelta
import datetime

import pandas as pd

from . import utils


__all__ = ['Cron', 'Employee', 'Contract', 'Payslip', 'Capacitation']


INITIAL_CHARGE_DATE = datetime.date(year=2019, month=7, day=1)


def sort_fields(data=[]):
    '''
    :param data: must be a list of dictionaries, for example:
        [{key: value}, {key: value}]
    :return: a list of sorted dictionaries
    '''
    _data = []
    for d in data:
        d = OrderedDict(sorted(d.items()))
        _data.append(d)
    return _data


def get_range_dates(end_date, months_back=5):
    # start date
    start_date = end_date - relativedelta(months=months_back)
    initial = datetime.date(year=start_date.year, month=start_date.month, day=1)
    # end date
    last = monthrange(end_date.year, end_date.month)[1]
    final = datetime.date(year=end_date.year, month=end_date.month, day=last)
    return initial, final


def get_periods(start_date, end_date):
    pool = Pool()
    Period = pool.get('account.period')
    result = []
    periods = Period.search([
        ('start_date', '>=', start_date),
        ('end_date', '<=', end_date)
    ])
    # Exclude start and close periods
    for period in periods:
        if period.start_date != period.end_date:
            result.append(period)
    return result


def get_fiscalyears(periods):
    fiscalyears = []
    for period in periods:
        if period.fiscalyear not in fiscalyears:
            fiscalyears.append(period.fiscalyear)
    return fiscalyears


class Cron(metaclass=PoolMeta):
    __name__ = 'ir.cron'

    @classmethod
    def __register__(cls, module_name):
        super(Cron, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        tbl_cron = cls.__table__()

        to_delete = [
            ('payslip.payslip.overtime.line',
                'Generación de datos de horas extras para BI'),
            ('hr_ec.absence',
                'Generación de datos de faltas para BI'),
        ]

        for td in to_delete:
            cursor.execute(
                *tbl_cron.delete(
                    where=((tbl_cron.model == td[0]) & (tbl_cron.name == td[1]))
                )
            )


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    @classmethod
    def get_data(cls, end_date):
        fields = ['id', 'party', 'antiquity_date', 'biometric_code',
            'birthdate', 'gender', 'civil_state', 'blood_type', 'ethnic_group',
            'country', 'city', 'parish', 'street', 'mobile', 'phone']
        return utils.get_employees_at_specific_date(end_date, fields=fields)

    @classmethod
    def generate_file(cls, path_name, period):
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/hr_ec/employee/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{period.name}.csv"

        data = cls.get_data(period.end_date)

        data = sort_fields(data)
        data_frame = pd.DataFrame.from_dict(data)
        data_frame.reindex(columns=sorted(data_frame.columns))
        data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
            quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        pool = Pool()
        Date = pool.get('ir.date')

        start_date, end_date = get_range_dates(Date.today(), months_back=5)

        # This date is used for initial charge, remove this line when the
        # initial charge ends.
        if INITIAL_CHARGE_DATE:
            start_date = INITIAL_CHARGE_DATE

        periods = get_periods(start_date, end_date)
        fiscalyears = get_fiscalyears(periods)

        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            for period in periods:
                cls.generate_file(bi_path, period)
            for fiscalyear in fiscalyears:
                cls.generate_file(bi_path, fiscalyear)


class Contract(metaclass=PoolMeta):
    __name__ = 'company.contract'

    @classmethod
    def get_data(cls, end_date):
        fields = ['absence_apply', 'budget', 'caution', 'code_iess',
            'code_sectoral_activity', 'company', 'contract_date',
            'contract_type', 'create_date', 'create_uid', 'department',
            'employee', 'employer_type_code_iess', 'end_date', 'fr', 'have_fr',
            'hierarchical_level', 'id', 'loan_fortnightly',
            'maximum_percentage_loan', 'medical_benefit',
            'min_salary_rate_to_receive', 'number',
            'obligatory_dialing_register', 'occupational_group',
            'origin_payment_iess', 'payslip_template_account_budget',
            'position', 'register_date_iess', 'salary', 'salary_table',
            'start_date', 'state', 'work_relationship',
            'working_relationship_iess', 'workingday_iess', 'write_date',
            'write_uid', 'xiii', 'xiv']
        return utils.get_contracts_at_specific_date(end_date, fields=fields)

    @classmethod
    def generate_file(cls, path_name, period):
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/hr_ec/contract/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{period.name}.csv"

        data = cls.get_data(period.end_date)

        data = sort_fields(data)
        data_frame = pd.DataFrame.from_dict(data)
        data_frame.reindex(columns=sorted(data_frame.columns))
        data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
            quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        pool = Pool()
        Date = pool.get('ir.date')

        start_date, end_date = get_range_dates(Date.today(), months_back=5)

        # This date is used for initial charge, remove this line when the
        # initial charge ends.
        if INITIAL_CHARGE_DATE:
            start_date = INITIAL_CHARGE_DATE

        periods = get_periods(start_date, end_date)
        fiscalyears = get_fiscalyears(periods)

        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            for period in periods:
                cls.generate_file(bi_path, period)
            for fiscalyear in fiscalyears:
                cls.generate_file(bi_path, fiscalyear)


class Payslip(metaclass=PoolMeta):
    __name__ = 'payslip.payslip'

    @classmethod
    def generate_file(cls, path_name, period):
        # fields = [fname for fname in cls._fields.keys()]
        fields = ['id', 'period', 'contract', 'employee', 'state', 'salary',
            'income_lines', 'deduction_lines', 'internal_lines', 'total_income',
            'total_deduction', 'total_value', 'worked_days',
            'worked_days_with_normative', 'total_days_disease',
            'total_absences', 'total_absences_with_normative',
            'total_days_with_dialing', 'nocturne_surcharge_hours', 'template',
            'department', 'position', 'work_relationship', 'contract_type']
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/hr_ec/payslip/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{period.name}.csv"
        data = cls.search_read([
            ('state', 'not in', ['draft', 'confirm', 'cancel']),
            ('period.start_date', '>=', period.start_date),
            ('period.end_date', '<=', period.end_date),
        ], fields_names=fields)
        data = sort_fields(data)
        data_frame = pd.DataFrame.from_dict(data)
        data_frame.reindex(columns=sorted(data_frame.columns))
        data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
            quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        pool = Pool()
        Date = pool.get('ir.date')

        start_date, end_date = get_range_dates(Date.today(), months_back=5)

        # This date is used for initial charge, remove this line when the
        # initial charge ends.
        if INITIAL_CHARGE_DATE:
            start_date = INITIAL_CHARGE_DATE

        periods = get_periods(start_date, end_date)
        fiscalyears = get_fiscalyears(periods)

        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            for period in periods:
                cls.generate_file(bi_path, period)
            for fiscalyear in fiscalyears:
                cls.generate_file(bi_path, fiscalyear)


class Capacitation(metaclass=PoolMeta):
    __name__ = 'company.employee_capacitation'

    @classmethod
    def generate_file(cls, path_name, period):
        fields = ['create_date', 'create_uid', 'date', 'duration', 'employee',
            'id', 'name', 'rec_name', 'speaker', 'write_date', 'write_uid']
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/hr_ec/capacitation/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{period.name}.csv"
        data = cls.search_read([
            ('employee.contract', '!=', None),
            ('date', '<=', period.end_date),
        ], fields_names=fields)
        data = sort_fields(data)
        data_frame = pd.DataFrame.from_dict(data)
        data_frame.reindex(columns=sorted(data_frame.columns))
        data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
            quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        pool = Pool()
        Date = pool.get('ir.date')

        start_date, end_date = get_range_dates(Date.today(), months_back=5)

        # This date is used for initial charge, remove this line when the
        # initial charge ends.
        if INITIAL_CHARGE_DATE:
            start_date = INITIAL_CHARGE_DATE

        periods = get_periods(start_date, end_date)
        fiscalyears = get_fiscalyears(periods)

        bi_path = config.get('siim', 'bi_data_path')
        if bi_path:
            for period in periods:
                cls.generate_file(bi_path, period)
            for fiscalyear in fiscalyears:
                cls.generate_file(bi_path, fiscalyear)
