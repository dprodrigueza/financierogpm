from trytond.pool import Pool
from . import bi

__all__ = ['register']


def register():
    Pool.register(
        bi.Cron,
        bi.Employee,
        bi.Contract,
        bi.Payslip,
        bi.Capacitation,
        module='hr_ec_bi_data', type_='model')
    Pool.register(
        module='hr_ec_bi_data', type_='wizard')
    Pool.register(
        module='hr_ec_bi_data', type_='report')
