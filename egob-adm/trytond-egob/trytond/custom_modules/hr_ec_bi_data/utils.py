from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import (grouped_slice, reduce_ids, cursor_dict, Literal,
    file_open)

from sql.conditionals import Case, Coalesce
from collections import defaultdict

import datetime


######################
#    QUERIES
######################

# CONTRACT
def get_query_contracts_history(
        consulting_date, states=['done', 'finalized', 'disabled']):
    pool = Pool()
    Contract = pool.get('company.contract')

    tbl_contract_history = Contract.__table_history__()

    query_contract = tbl_contract_history.select(
        where=((tbl_contract_history.state.in_(states)) &
               (Coalesce(
                   tbl_contract_history.write_date,
                   tbl_contract_history.create_date) <= consulting_date)),
        distinct_on=[tbl_contract_history.id],
        order_by=[
            tbl_contract_history.id.asc,
            tbl_contract_history.write_date.desc.nulls_last
        ]
    )
    return query_contract


def get_query_contracts_salary_table_history(consulting_date):
    pool = Pool()
    ContractSalary = pool.get('company.contract.salary')

    tbl_contract_salary_history = ContractSalary.__table_history__()

    query_contract_salary = tbl_contract_salary_history.select(
        tbl_contract_salary_history.contract,
        tbl_contract_salary_history.salary_table,
        where=((tbl_contract_salary_history.start_date <= consulting_date) &
               (Coalesce(
                   tbl_contract_salary_history.end_date,
                   datetime.date.max) >= consulting_date) &
               (Coalesce(
                   tbl_contract_salary_history.write_date,
                   tbl_contract_salary_history.create_date
               ) <= consulting_date)),
        distinct_on=[tbl_contract_salary_history.contract],
        order_by=[
            tbl_contract_salary_history.contract.asc,
            tbl_contract_salary_history.start_date.desc.nulls_last,
            tbl_contract_salary_history.end_date.desc.nulls_last,
            tbl_contract_salary_history.write_date.desc.nulls_last
        ]
    )
    return query_contract_salary


def get_query_salary_table_line_history(consulting_date):
    pool = Pool()
    SalaryTableLine = pool.get('company.salary.table.line')

    tbl_salary_line_history = SalaryTableLine.__table_history__()

    query_salary_table_line = tbl_salary_line_history.select(
        tbl_salary_line_history.position.as_('_position'),
        tbl_salary_line_history.salary_table.as_('_salary_table'),
        tbl_salary_line_history.salary,
        where=((Coalesce(
            tbl_salary_line_history.write_date,
            tbl_salary_line_history.create_date) <= consulting_date)),
        distinct_on=[tbl_salary_line_history.id],
        order_by=[
            tbl_salary_line_history.id.asc,
            tbl_salary_line_history.write_date.desc.nulls_last
        ]
    )
    return query_salary_table_line


def get_query_contract_and_salary_history(consulting_date, contract_states=[]):
    query_contract = get_query_contracts_history(
        consulting_date, states=contract_states)
    query_contract_salary = get_query_contracts_salary_table_history(
        consulting_date)
    query_salary_table_line = get_query_salary_table_line_history(
        consulting_date)

    query_1 = query_contract.join(query_contract_salary, #type_='LEFT',
        condition=(query_contract_salary.contract == query_contract.id)
    ).select()

    query_2 = query_1.join(query_salary_table_line, #type_='LEFT',
        condition=(
            (query_salary_table_line._salary_table == query_1.salary_table) &
            (query_salary_table_line._position == query_1.position)
        )
    ).select()

    return query_2


# EMPLOYEE
def get_query_employees_history(consulting_date):
    pool = Pool()
    Employee = pool.get('company.employee')

    tbl_employee_history = Employee.__table_history__()

    query_employee = tbl_employee_history.select(
        where=((Coalesce(
                   tbl_employee_history.write_date,
                   tbl_employee_history.create_date) <= consulting_date)),
        distinct_on=[tbl_employee_history.id],
        order_by=[
            tbl_employee_history.id.asc,
            tbl_employee_history.write_date.desc.nulls_last
        ]
    )
    return query_employee

def get_query_party_address(consulting_date):
    pool = Pool()
    PartyAddress = pool.get('party.address')

    tbl_party_address = PartyAddress.__table__()

    query_party_address = tbl_party_address.select(
        tbl_party_address.party.as_('_party'),
		tbl_party_address.city,
		tbl_party_address.country,
		tbl_party_address.street,
		tbl_party_address.subdivision,
		tbl_party_address.parish,
        where=((tbl_party_address.active == True) &
               (Coalesce(
                   tbl_party_address.write_date,
                   tbl_party_address.create_date) <= consulting_date)),
        distinct_on=[tbl_party_address.party],
        order_by=[
            tbl_party_address.party.asc,
            tbl_party_address.write_date.desc.nulls_last
        ]
    )
    return query_party_address

def get_query_party_contact_mechanism(
        consulting_date, types=['mobile', 'phone']):
    pool = Pool()
    PartyContactMechanism = pool.get('party.contact_mechanism')

    tbl_party_cm = PartyContactMechanism.__table__()

    query_party_contact_mechanism = tbl_party_cm.select(
        where=((tbl_party_cm.active == True) &
               (tbl_party_cm.type.in_(types)) &
               (Coalesce(
                   tbl_party_cm.write_date,
                   tbl_party_cm.create_date) <= consulting_date)),
        order_by=[
            tbl_party_cm.write_date.desc.nulls_last
        ]
    )
    return query_party_contact_mechanism


def get_query_employee_and_party_address_history(consulting_date):
    query_employee = get_query_employees_history(consulting_date)
    query_party_address = get_query_party_address(consulting_date)

    query = query_employee.join(query_party_address, type_='LEFT',
        condition=(query_employee.party == query_party_address._party)
    ).select()

    return query



######################
#    DATA
######################
def get_contracts_at_specific_date(consulting_date, fields=[]):
    cursor = Transaction().connection.cursor()
    contracts = []

    # DATA: CONTRACTS
    query = get_query_contract_and_salary_history(
        consulting_date, contract_states=['done', 'finalized', 'disabled'])
    cursor.execute(*query)
    except_columns = ['__id', '_position', '_salary_table', 'contract']
    for row in cursor_dict(cursor):
        for c in except_columns:
            if c in row:
                del row[c]
        contracts.append(row)

    # EXTRACT FIELDS
    if fields:
        for contract in contracts:
            to_delete_keys = []
            for key, value in contract.items():
                if key not in fields:
                    to_delete_keys.append(key)
            for key in to_delete_keys:
                del contract[key]
    return contracts


def get_employees_at_specific_date(consulting_date, fields=[]):
    cursor = Transaction().connection.cursor()
    employees = []
    contracts = []
    contact_mechanisms = []

    # DATA: EMPLOYEES AND PARTY ADDRESS
    query = get_query_employee_and_party_address_history(consulting_date)
    cursor.execute(*query)
    except_columns = ['__id', '_party']
    for row in cursor_dict(cursor):
        for c in except_columns:
            if c in row:
                del row[c]
        employees.append(row)

    # DATA: PARTY CONTACT MECHANISM
    types = ['mobile', 'phone']
    query = get_query_party_contact_mechanism(consulting_date, types=types)
    cursor.execute(*query)
    for row in cursor_dict(cursor):
        contact_mechanisms.append(row)

    # DATA: LEFT JOIN EMPLOYEES AND PARTY CONTACT MECHANISM
    for employee in employees:
        for type in types:
            employee.update({type: None})

    for employee in employees:
        for contact_mechanism in contact_mechanisms:
            if employee['party'] == contact_mechanism['party']:
                if contact_mechanism['type'] and contact_mechanism['value']:
                    _type = contact_mechanism['type']
                    _value = contact_mechanism['value']
                    if not employee[_type]:
                        employee[_type] = _value

    # DATA: CONTRACTS AND SALARIES
    query = get_query_contract_and_salary_history(
        consulting_date, contract_states=['done', 'finalized', 'disabled'])
    cursor.execute(*query)
    except_columns = ['__id', '_position', '_salary_table', 'contract']
    for row in cursor_dict(cursor):
        for c in except_columns:
            if c in row:
                del row[c]
        contracts.append(row)
    for contract in contracts:
        to_delete_key = []
        to_update_values = []
        for attr, value in contract.items():
            # Change all contract attribute names by 'contract_attribute_name'
            to_update_values.append((f'contract_{attr}', contract[attr]))
            to_delete_key.append(attr)
        for value in to_update_values:
            contract.update({
                value[0]: value[1]
            })
        for key in to_delete_key:
            del contract[key]

    # DATA: LEFT JOIN EMPLOYEES AND CONTRACTS
    for employee in employees:
        for contract in contracts:
            if employee['id'] == contract['contract_employee']:
                employee.update(contract)

    # EXTRACT FIELDS
    if fields:
        for employee in employees:
            to_delete_keys = []
            for key, value in employee.items():
                if key not in fields:
                    to_delete_keys.append(key)
            for key in to_delete_keys:
                del employee[key]
    return employees