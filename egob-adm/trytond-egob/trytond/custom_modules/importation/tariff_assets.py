import re

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool

__all__ = ['TariffAsset']

class TariffAsset(ModelSQL, ModelView):
    'Tariff Asset'
    __name__ = 'importation.tariff.asset'
    _history = True

    parent = fields.Many2One('importation.tariff.asset', 'Superior',
        select=True, left="left", right="right", ondelete="RESTRICT")

    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)

    code = fields.Char('Código', states={
        'required': True,
    })
    name = fields.Char('Nombre', states={
        'required': True,
    })
    unit = fields.Many2One('product.uom', 'Unidad', ondelete='RESTRICT',
    states={
        'required': Bool(Eval('is_required')),
        'readonly': False,
    }, depends=['parent'])

    tariff = fields.Numeric('Tarifa arancelaria', states={
        'required': Bool(Eval('is_required')),
    }, depends=['parent'])
    observations = fields.Text('Observaciones')

    is_required = fields.Boolean('Es necesario campos requeridos ?', states={
        'readonly': True,
        'invisible': True,
    })
    childs = fields.One2Many('importation.tariff.asset', 'parent', 'Hijos',
                             states={
                                 'readonly': Bool(Eval('is_required')),
                                 'invisible': Bool(Eval('is_required')),
                             })

    @classmethod
    def __setup__(cls):
        super(TariffAsset, cls).__setup__()
        cls._error_messages.update({
            '': (''),
        })
        # cls._buttons.update(
        #     {
        #         'create_lines': {},
        #     })
        cls._order.insert(0, ('code', 'ASC'))

    @staticmethod
    def default_is_required():
        return False

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @fields.depends('parent')
    def on_change_with_code(self):
        if self.parent:
            return self.parent.code
        else:
            return None

    @fields.depends('code')
    def on_change_code(self):
        self.is_required = False
        if self.code:
            patron = re.compile(r'^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$')
            match_patron = patron.search(self.code)
            if match_patron:
                self.is_required = True

    def get_rec_name(self, name):
        return '%s - %s' % (self.code, self.name)