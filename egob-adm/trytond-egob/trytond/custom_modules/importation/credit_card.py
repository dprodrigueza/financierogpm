import functools
from collections import defaultdict
from datetime import datetime, timedelta
from decimal import Decimal

from trytond.model import (ModelView, ModelSQL, fields, Workflow, Unique)
from trytond.pool import Pool
from trytond.pyson import Eval, If, Bool
from trytond.transaction import Transaction

__all__ = ['CreditCard', 'CreaditCardCreditCardRequirements',
           'CreditCardRequirements', 'LiquidationCreditCard']

STATES = {
        'readonly': Bool(Eval('state') != 'draft'),
    }

def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)

            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field)], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class CreditCard(Workflow, ModelSQL, ModelView):
    'CreditCard'
    __name__ = 'credit_card.creditcard'
    _history = True

    importation = fields.Many2One('importation.importation', 'Importación',
        states={
            'required': True,
            'readonly': STATES['readonly'],
        }, domain=[
            ('state', '=', 'done'),
            # ('credit_cards', '=', None),
        ], depends=['state'])
    origin = fields.Selection([
        ('01', 'Nota de pedido'),
        ('02', 'Factura proforma'),
        ('03', 'Contrato'),
    ], 'Origen', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    note_number = fields.Char('Número nota', states={
        'invisible': ~(Eval('origin') == '01'),
        'required': (Eval('origin') == '01'),
        'readonly': STATES['readonly'],
    })
    invoice_number = fields.Char('Número factura', states={
        'invisible': ~(Eval('origin') == '02'),
        'required': (Eval('origin') == '02'),
        'readonly': STATES['readonly'],
    })
    public_contract = fields.Many2One('purchase.contract.process', 'Contrato',
        states={
            'invisible': ~(Eval('origin') == '03'),
            'required': (Eval('origin') == '03'),
            'readonly': STATES['readonly'],
    }, domain=[('state', '=', 'open')])
    origin_type = fields.Selection([
        ('import', 'Importación'),
        ('export', 'Exportación'),
    ], 'Tipo de origen', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    origin_type_translated = origin_type.translated('origin_type')
    number_credit_card = fields.Char('Número autorización.', states={
        'required': False,
        'readonly': Bool(Eval('state').in_(['done', 'liquidated'])),
    }, help='Ingresar el número de autorización proporcionado por el BCE.')
    emission_date = fields.Date('Fecha emisión', states={
        'required': False,
        'readonly': Bool(Eval('state').in_(['done', 'liquidated'])),
    }, help='Ingresar la fecha de EMISION proporcionada por el BCE.')
    authorization_date = fields.Date('Fecha autorización', states={
        'required': False,
        'readonly': Bool(Eval('state').in_(['done', 'liquidated'])),
    }, help='Ingresar la fecha de AUTORIZACION proporcionada por el BCE.')
    expired_date = fields.Function(fields.Date('Fecha expiración'),
        'on_change_with_expired_date')

    importer = fields.Function(fields.Many2One('importer.importer', 'Ordenante'),
        'on_change_with_importer')
    importer_address = fields.Function(fields.Text('Dirección ordenante',
        states={
            'readonly': STATES['readonly'],
        }), 'on_change_with_importer_address')
    provider = fields.Function(fields.Many2One('party.party', 'Proveedor'),
        'on_change_with_provider', searcher='search_provider')
    provider_address = fields.Function(fields.Text('Dirección proveedor'),
        'on_change_with_provider_address')
    credit_card_type = fields.Selection([
        ('01', 'Revocable'),
        ('02', 'Irrevocable'),
        ('03', 'Irrevocable - no confirmada'),
        ('04', 'Irrevocable - confirmada'),
        ('05', 'Transferible'),
        ('06', 'Standby'),
    ], 'Tipo carta crédito', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    credit_card_type_translated = credit_card_type.translated('credit_card_type')
    creditcard_requirements = fields.One2Many(
        'credit_card.credit_card_requirements', 'creditcard',
        'Documentos requeridos para pago', states={
            'required': True,
            'readonly': STATES['readonly'],
        })
    confirmed_by = fields.Many2One(
        'company.employee', 'Confirmado por', readonly=True)
    confirmed_date = fields.DateTime('Fecha confirmación', readonly=True)
    done_by = fields.Many2One(
        'company.employee', 'Aprobado por', readonly=True)
    done_date = fields.DateTime('Fecha aprobación', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('done', 'Aprobado'),
        ('liquidated', 'Liquidado'),
        ('expired', 'Vencida'),
        ('cancel', 'Cancelado')
    ], 'Estado', readonly=True, required=True)
    state_translated = state.translated('state')
    currency_origin = fields.Many2One('currency.currency', 'Moneda origen',
        states={
            'required': True,
            'readonly': STATES['readonly'],
        })
    rate_origin = fields.Numeric('Tasa origen', digits=(16, 2), states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    amount = fields.Numeric('Monto', states={
        'required': True,
        'readonly': True,
    })
    currency_destination = fields.Many2One('currency.currency',
        'Moneda destino', states={
            'required': True,
            'readonly': STATES['readonly'],
        })
    rate_destination = fields.Numeric('Tasa destino', digits=(16,2), states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    amount_with_currency = fields.Numeric('Monto con cambio moneda', states={
        'required': True,
        'readonly': True,
    }, digits=(16,2))

    liquidations = fields.One2Many('credit_card.liquidation.creditcard',
        'credit_card', 'Liquidación', states={
            'readonly': True,
        })
    payment_request = fields.Function(
        fields.Many2One('treasury.account.payment.request',
        'Pago carta crédito'), 'get_payment_request')
    payment_request_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('approved', 'Aprobado'),
            ('processing', 'Procesando'),
            ('denied', 'Revisión'),
            ('cancel', 'Cancelado'),
        ], 'Estado pago'), 'get_payment_request_state',
        searcher='')

    @classmethod
    def __setup__(cls):
        super(CreditCard, cls).__setup__()
        t = cls.__table__()
        cls._error_messages.update({
            'no_delete': ('Solo se pueden eliminar registro que se encuentren '
                'en estado BORADOR'),
            'required_camps': ('Es necesario la información de los campos: '
                '"Número de carta de cŕedito", "Fecha de emissión", '
                '"Fecha de autorización".'),
        })
        cls._sql_constraints = [
            ('importation_uniq', Unique(t, t.importation),
             'Ya existe una importación para esta carta de crédito. \n La '
             'importación es única.')
        ]
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'done'),
            ('confirmed', 'cancel'),
            ('done', 'liquidated'),
            ('done', 'expired'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done', 'cancel',
                                                'liquidated', 'expired']),
                'depends': ['state'],
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'cancel', 'done',
                                                'liquidated', 'expired']),
                'depends': ['state'],
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft', 'cancel',
                                                'liquidated', 'expired']),
                'depends': ['state'],
            },
            'cancel': {
                'invisible': Eval('state').in_(['draft', 'cancel', 'done',
                                                'liquidated', 'expired']),
                'depends': ['state'],
            },
        })

    @staticmethod
    def default_origin_type():
        return 'import'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_origin():
        return '01'

    @staticmethod
    def default_credit_card_type():
        return '01'

    @staticmethod
    def default_currency_origin():
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
            return company.currency.id

    @fields.depends('importation')
    def on_change_with_importer(self, name=None):
        if self.importation:
            return self.importation.importer.id
        return None

    @fields.depends('importation')
    def on_change_with_importer_address(self, name=None):
        if self.importation:
            return self.importation.importer.street
        return None

    @fields.depends('importation')
    def on_change_with_provider_address(self, name=None):
        if self.importation:
            return self.importation.provider_address
        return None

    @fields.depends('importation')
    def on_change_with_provider(self, name=None):
        if self.importation:
            return self.importation.provider.id
        return None

    @fields.depends('note_number', 'invoice_number')
    def on_change_origin(self, name=None):
        # if self.origin:
        self.note_number = None
        self.invoice_number = None

    @fields.depends('amount', 'currency_destination', 'rate_destination',
                    'currency_origin', 'rate_origin')
    def on_change_amount(self, name=None):
        if (self.amount and self.currency_destination and self.rate_destination
                and self.currency_origin and self.rate_origin):
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        else:
            self.amount_with_currency = None

    @fields.depends('amount', 'currency_destination', 'currency_origin',
                    'rate_origin')
    def on_change_currency_destination(self, name=None):
        if (self.amount and self.currency_destination and self.currency_origin
                and self.rate_origin):
            self.rate_destination = self.currency_destination.rate
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        elif self.currency_destination:
            self.rate_destination = self.currency_destination.rate
        else:
            self.amount_with_currency = None
            self.rate_destination = None

    @fields.depends('amount', 'currency_destination', 'rate_destination',
                    'currency_origin', 'rate_origin')
    def on_change_rate_destination(self, name=None):
        if (self.amount and self.currency_destination and self.rate_destination
            and self.currency_origin and self.rate_origin):
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        else:
            self.amount_with_currency = None

    @fields.depends('currency_origin', 'amount', 'currency_destination',
                    'rate_destination')
    def on_change_currency_origin(self, name=None):
        if self.amount and self.currency_origin and self.currency_destination:
            self.rate_origin = self.currency_origin.rate
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        elif self.currency_origin:
            self.rate_origin = self.currency_origin.rate
        else:
            self.amount_with_currency = None
            self.rate_origin = None

    @fields.depends('amount', 'currency_destination', 'rate_destination',
                    'currency_origin', 'rate_origin')
    def on_change_rate_origin(self, name=None):
        if (self.amount and self.currency_destination and self.rate_destination
                and self.currency_origin and self.rate_origin):
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        else:
            self.amount_with_currency = None

    @fields.depends('importation')
    def on_change_with_amount(self, name=None):
        if self.importation:
            total = Decimal('0.00')
            for asset in self.importation.assets:
                total += asset.total_price
            return total
        else:
            return None

    @fields.depends('emission_date')
    def on_change_with_expired_date(self, name=None):
        ImportationConfiguration = Pool().get('importation.configuration')
        if self.emission_date:
            config = ImportationConfiguration.search([()])
            days = config[0].days_expired_credit_card
            return self.emission_date + timedelta(days=days)
        else:
            return None

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, cards):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirmed(cls, cards):
        for card in cards:
            card.confirmed_date = datetime.today()
        cls.save(cards)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def done(cls, cards):
        for card in cards:
            if not (card.number_credit_card and card.emission_date and
                    card.authorization_date):
                cls.raise_user_error(
                    'required_camps', {}
                )
            card.done_date = datetime.today()
        cls.save(cards)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, cards):
        pass

    @classmethod
    def delete(cls, cards):
        for card in cards:
            if card.state in ['done', 'confirmed', 'cancel']:
                cls.raise_user_error('no_delete')
        super(CreditCard, cls).delete(cards)

    @classmethod
    def get_payment_request(self, cards, name):
        pool = Pool()
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')
        result = {}
        for card in cards:
            payment_request_line_detail = PaymentRequestLineDetail.search([
                ('origin', '=', str(card)),
                ('state', '=', 'paid')
            ])
            if payment_request_line_detail:
                result[card.id] = payment_request_line_detail[0].request.id
            else:
                result[card.id] = None
        return result

    @classmethod
    def get_payment_request_state(self, cards, name):
        pool = Pool()
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')
        result = {}
        for card in cards:
            payment_request_line_detail = PaymentRequestLineDetail.search([
                ('origin', '=', str(card)),
                ('state', '=', 'paid')
            ])
            if payment_request_line_detail:
                result[card.id] = payment_request_line_detail[0].request.state
            else:
                result[card.id] = None
        return result

    @classmethod
    def expiration_credit_card(cls):
        ImportationConfiguration = Pool().get('importation.configuration')
        today = datetime.today().date()
        config = ImportationConfiguration.search([()])
        days = config[0].days_expired_credit_card

        expired_cards = cls.search([
            ('emission_date', '=', (today - timedelta(days=days))),
            ('liquidations', '=', None),
        ])
        for credit_card in expired_cards:
            credit_card.state = 'expired'
            credit_card.save()

    @classmethod
    def search_provider(cls, name, clause):
        return [('importation.provider',) + tuple(clause[1:])]

    def get_rec_name(self, name):
        return ' %s ' % self.number_credit_card

    @classmethod
    def search_rec_name(cls, name, clause):
        return [('number_credit_card',) + tuple(clause[1:])]


class CreaditCardCreditCardRequirements(ModelSQL, ModelView):
    'CreaditCard CreditCard Requirements'
    __name__ = 'credit_card.credit_card_requirements'

    creditcard = fields.Many2One('credit_card.creditcard', 'Carta crédito',
        states={
            'readonly': True,
        }, domain=[
            # ('liquidations', '=', None),
        ])
    liquidation_creditcard = fields.Many2One(
        'credit_card.liquidation.creditcard', 'Carta crédito', states={
            'readonly': True,
        })
    creditcard_requirements = fields.Many2One('credit_card.requirements.pay',
        'Documentos', states={
            'readonly': Eval('_parent_creditcard', {}).get('state') != 'draft',
        }, required=True)
    revised = fields.Boolean('Fué revisado ?', states={
        'readonly': Eval('_parent_liquidation_creditcard', {}).get('state')
                    != 'draft',
    }, depends=['liquidation_creditcard'])
    adjunct_document = fields.Binary('Documento adjunto', states={
        'readonly': Eval('_parent_liquidation_creditcard', {}).get('state')
                    != 'draft',
    }, depends=['liquidation_creditcard'])


    @staticmethod
    def default_revised():
        return False


class CreditCardRequirements(ModelSQL, ModelView):
    'Credit Card Requirements'
    __name__ = 'credit_card.requirements.pay'
    _history = True

    name = fields.Char('Nombre')


class LiquidationCreditCard(Workflow, ModelSQL, ModelView):
    'Liquidation Credit Card'
    __name__ = 'credit_card.liquidation.creditcard'
    _history = True

    credit_card = fields.Many2One('credit_card.creditcard',
        'Número autorización C.C.', states={
            'required': True,
            'readonly': STATES['readonly'],
        }, domain=[
            ('state', '=', 'done'),
            # ('payment_request_state', '=', 'approved'),
        ])
    creditcard_requirements = fields.One2Many(
        'credit_card.credit_card_requirements', 'liquidation_creditcard',
        'Documentos requeridos para pago', states={
            'required': True,
            'readonly': True,
        })
    credit_card_type = fields.Function(fields.Selection([
        ('01', 'Revocable'),
        ('02', 'Irrevocable'),
        ('03', 'Irrevocable - no confirmada'),
        ('04', 'Irrevocable - confirmada'),
        ('05', 'Transferible'),
        ('06', 'Standby'),
    ], 'Tipo carta crédito', states={
        'required': True,
        'readonly': STATES['readonly'],
    }), 'on_change_with_credit_card_type')
    emission_date = fields.Function(fields.Date('Fecha emisión', states={
        'required': False,
    }, help='Ingresar la fecha de EMISION proporcionada por el BCE.'),
        'on_change_with_emission_date')
    authorization_date = fields.Function(fields.Date('Fecha autorización', states={
        'required': False,
    }, help='Ingresar la fecha de AUTORIZACION proporcionada por el BCE.'),
        'on_change_with_authorization_date')
    importer = fields.Function(fields.Many2One('importer.importer', 'Ordenante',
        states={
            'required': True,
            'readonly': STATES['readonly'],
        }), 'on_change_with_importer')
    provider = fields.Function(fields.Many2One('party.party', 'Proveedor', states={
        'required': True,
        'readonly': STATES['readonly'],
    }), 'on_change_with_provider')

    currency_origin = fields.Function(fields.Many2One('currency.currency',
        'Moneda origen'), 'on_change_with_currency_origin')
    rate_origin = fields.Function(fields.Numeric('Tasa origen', digits=(16, 2)),
        'on_change_with_rate_origin')
    amount = fields.Function(fields.Numeric('Monto'), 'on_change_with_amount')
    currency_destination = fields.Function(fields.Many2One('currency.currency',
        'Moneda destino'), 'on_change_with_currency_destination')
    rate_destination = fields.Numeric('Tasa de cambio', digits=(16, 2), states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    amount_with_currency = fields.Numeric('Monto con cambio moneda', states={
        'required': True,
        'readonly': True,
    }, digits=(16,2))

    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('liquidated', 'Liquidado'),
    ], 'Estado', readonly=True, required=True)
    state_translated = state.translated('state')
    confirmed_by = fields.Many2One(
        'company.employee', 'Confirmado por', readonly=True)
    confirmed_date = fields.DateTime('Fecha confirmación', readonly=True)
    liquidated_by = fields.Many2One(
        'company.employee', 'Liquidado por', readonly=True)
    liquidated_date = fields.DateTime('Fecha liquidación', readonly=True)
    adjustment_change_currency = fields.Function(fields.Numeric(
        'Cambiario de ajuste', states={
            'readonly': True,
        }), 'get_adjustment_change_currency')

    payment_request = fields.Function(
        fields.Many2One('treasury.account.payment.request',
                        'Pago ajuste carta crédito'), 'get_payment_request')
    payment_request_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('approved', 'Aprobado'),
            ('processing', 'Procesando'),
            ('denied', 'Revisión'),
            ('cancel', 'Cancelado')
        ], 'Estado pago'), 'get_payment_request_state',
        searcher='')

    @classmethod
    def __setup__(cls):
        super(LiquidationCreditCard, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('credit_card_uniq', Unique(t, t.credit_card),
             'Ya existe una liquidación para esta carta de crédito. \n La carta '
             'de crédito es único.')
        ]
        cls._error_messages.update({
            'not_revised_card_requirements': ('Para continuar con el proceso '
                'primero debe de REVISAR cada uno de los REQUERIMIENTOS DE '
                'PAGO e ir dando click en el check de revisado.'),
        })
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'liquidated'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'liquidated']),
                'depends': ['state'],
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'liquidated']),
                'depends': ['state'],
            },
            'liquidated': {
                'invisible': Eval('state').in_(['draft', 'liquidated']),
                'depends': ['state'],
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('credit_card')
    def on_change_with_creditcard_requirements(self, name=None):
        if self.credit_card:
            ids_requirements = []
            for requirement in self.credit_card.creditcard_requirements:
                ids_requirements.append(requirement.id)
            return ids_requirements
        else:
            return []

    @fields.depends('credit_card')
    def on_change_with_credit_card_type(self, name=None):
        if self.credit_card:
            return self.credit_card.credit_card_type
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_emission_date(self, name=None):
        if self.credit_card:
            return self.credit_card.emission_date
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_authorization_date(self, name=None):
        if self.credit_card:
            return self.credit_card.authorization_date
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_importer(self, name=None):
        if self.credit_card:
            return self.credit_card.importer.id
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_provider(self, name=None):
        if self.credit_card:
            return self.credit_card.provider.id
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_amount(self, name=None):
        if self.credit_card:
            return self.credit_card.amount
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_currency_origin(self, name=None):
        if self.credit_card:
            return self.credit_card.currency_origin.id
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_rate_origin(self, name=None):
        if self.credit_card:
            return self.credit_card.rate_origin
        else:
            return None

    @fields.depends('credit_card')
    def on_change_with_currency_destination(self, name=None):
        if self.credit_card:
            return self.credit_card.currency_destination.id
        else:
            return None

    @fields.depends('credit_card')
    def on_change_credit_card(self):
        if self.credit_card:
            self.rate_destination = self.credit_card.rate_destination
            self.amount_with_currency = self.credit_card.amount_with_currency
        else:
            self.rate_destination = None
            self.amount_with_currency = None

    @fields.depends('amount', 'currency_destination', 'rate_origin')
    def on_change_currency_destination(self, name=None):
        if self.amount and self.currency_destination:
            self.rate_destination = self.currency_destination.rate
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        elif self.currency_destination:
            self.rate_destination = self.currency_destination.rate
        else:
            self.amount_with_currency = None
            self.rate_destination = None

    @fields.depends('amount', 'currency_destination', 'rate_destination',
                    'rate_origin')
    def on_change_rate_destination(self, name=None):
        if self.amount and self.currency_destination and self.rate_destination:
            self.amount_with_currency = ((self.amount / self.rate_origin) *
                                         self.rate_destination)
        else:
            self.amount_with_currency = None

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, liquidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirmed(cls, liquidations):
        cls.ckeck_revised_requirements(liquidations)
        for liquidation in liquidations:
            liquidation.confirmed_date = datetime.today()
        cls.save(liquidations)

    @classmethod
    @ModelView.button
    @Workflow.transition('liquidated')
    @set_employee('liquidated_by')
    def liquidated(cls, liquidations):
        CreditCard = Pool().get('credit_card.creditcard')
        cls.ckeck_revised_requirements(liquidations)
        for liquidation in liquidations:
            credit_card = liquidation.credit_card
            credit_card.state = 'liquidated'
            liquidation.liquidated_date = datetime.today()
            CreditCard.save([credit_card])
        cls.save(liquidations)

    @classmethod
    def ckeck_revised_requirements(cls, liquidations):
        for liquidation in liquidations:
            for requirement in liquidation.creditcard_requirements:
                if not requirement.revised:
                    cls.raise_user_error(
                        'not_revised_card_requirements', {}
                    )
        cls.save(liquidations)

    @classmethod
    def get_adjustment_change_currency(cls, cards, name):
        result = defaultdict(lambda: [])
        for card in cards:
            if card.amount and card.amount_with_currency:
                charge = ((card.credit_card.amount *
                          card.amount_with_currency) /
                         (card.credit_card.amount_with_currency))
                total = (card.credit_card.amount - charge).quantize(
                    Decimal('0.00'))
                # total = (card.credit_card.amount_with_currency -
                #          card.amount_with_currency).quantize(
                #     Decimal('0.00'))
                result[card.id] = total

        return result

    @classmethod
    def get_payment_request(self, cards, name):
        pool = Pool()
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')
        result = {}
        for card in cards:
            payment_request_line_detail = PaymentRequestLineDetail.search([
                ('origin', '=', str(card)),
                ('state', '=', 'paid')
            ])
            if payment_request_line_detail:
                result[card.id] = payment_request_line_detail[0].request.id
            else:
                result[card.id] = None
        return result

    @classmethod
    def get_payment_request_state(self, cards, name):
        pool = Pool()
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')
        result = {}
        for card in cards:
            payment_request_line_detail = PaymentRequestLineDetail.search([
                ('origin', '=', str(card)),
                ('state', '=', 'paid')
            ])
            if payment_request_line_detail:
                result[card.id] = payment_request_line_detail[0].request.state
            else:
                result[card.id] = None
        return result

