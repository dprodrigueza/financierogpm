import datetime
from decimal import Decimal
import io
import json
from datetime import date
import logging
from dateutil.relativedelta import relativedelta
from sql import Window, Literal, Union
from sql.conditionals import Case
from sql.aggregate import Sum
from sql.functions import RowNumber, CurrentTimestamp
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import Workflow, ModelView, fields, ModelSQL, \
    sequence_ordered, Unique
from trytond.pyson import Bool, Eval, If, Id, Date as pyDate
from trytond.wizard import (Wizard, StateView, StateTransition, Button,
    StateAction)
from trytond.rpc import RPC
from trytond.config import config as trytond_config

from trytond.modules.company import CompanyReport
from trytond.modules.product import price_digits

import copy
import requests
import pandas as pd
from fbprophet import Prophet
import treepoem


_ZERO = Decimal('0.0')

__all__ = ['Move']

class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        models = super(Move, cls)._get_origin()
        models.append('importation.importation.line')
        return models
