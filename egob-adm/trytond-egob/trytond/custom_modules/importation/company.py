from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Company']


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    code_economic_activity = fields.Char('Código de actividad económica')
    name_economic_activity = fields.Char('Nombre de actividad económica')


