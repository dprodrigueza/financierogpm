# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.importation.tests.test_importation import suite
except ImportError:
    from .test_importation import suite

__all__ = ['suite']
