# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.modules.importation import invoice
from trytond.pool import Pool

from . import importation 
from . import credit_card
from . import importation_license
from . import tariff_assets
from . import importer
from . import company
from . import party
from . import stock
from . import report

#__all__ = ['register']


def register():
    Pool.register(
        company.Company,
        importation.Importation,
        importation.Custom,
        importation.ImportationLine,
        importation.ImportationLineAccount,
        importation.ImportationLineTax,
        importation.ImportationDeclarant,
        importation.ImportationLiquidationImportation,
        importation.ImportationNature,
        importation.ImportationPaymentTerm,
        importation.ImportationConfiguration,
        importation.ImportationTracking,
        importer.Importer,
        credit_card.CreditCard,
        credit_card.CreaditCardCreditCardRequirements,
        credit_card.CreditCardRequirements,
        credit_card.LiquidationCreditCard,
        importation_license.ImportLicense,
        tariff_assets.TariffAsset,
        party.Party,
        invoice.Invoice,
        invoice.InvoiceLine,
        stock.Move,
        module='importation', type_='model')
    Pool.register(
        module='importation', type_='wizard')
    Pool.register(
        report.ReportImportationImportation,
        report.ReportCreditCard,
        module='importation', type_='report')