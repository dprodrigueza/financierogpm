import functools
from datetime import datetime

from trytond.model import ModelView, ModelSQL, fields, Workflow, Unique
from decimal import Decimal

from trytond.pool import Pool
from trytond.pyson import Bool, Eval
from trytond.transaction import Transaction

__all__ = ['Importation', 'Custom', 'ImportationLine', 'ImportationLineAccount',
           'ImportationLineTax', 'ImportationDeclarant',
           'ImportationLiquidationImportation', 'ImportationNature',
           'ImportationPaymentTerm', 'ImportationConfiguration',
           'ImportationTracking',]

STATES = {
        'readonly': Bool(Eval('state') != 'draft'),
    }

def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)

            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field)], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class Importation(Workflow, ModelSQL, ModelView):
    'Importation'
    __name__ = 'importation.importation'
    _history = True

    number_expedition = fields.Char('Número expedición', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    date_expedition = fields.Date('Fecha expedición', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    place_expedition = fields.Many2One('importation.custom', 'Lugar expedición',
        states={
            'required': True,
            'readonly': STATES['readonly'],
        })
    regime_code = fields.Selection([
        ('21', 'Adminisión temporal para perfeccionamiento activo, '
               'transferencias a terceros de insumos, productos en procesos '
               'y productos terminados'),
        ('20', 'Admisión temporal para reexportación en el mismo estado (cambio'
               ' de beneficiario, cambio de obra.)'),
        ('75', 'Almacén especial'),
        ('73', 'Almacén libre'),
        ('70', 'Deposito aduanero público y privado'),
        ('86', 'Destrucción de sobrantes'),
        ('24', 'Ferias internacionales'),
        ('10', 'Importación a consumo'),
        ('87', 'Regularización po pérdida o destrucción'),
        ('31', 'Reimportación de mercancias exportadas temporalmente para '
               'perfeccionamiento pasivo'),
        ('32', 'Reimportación de mercancias en el mismo estado/exportadas a '
               'consumo'),
        ('11', 'Reposición de mercancias con franquicia arancelaria'),
        ('72', 'Transformación bajo control aduanero'),
    ], 'Código regimen', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    regime_code_translated = regime_code.translated('regime_code')
    dispatch_type = fields.Selection([
        ('5', 'Autorización de salida Zede'),
        ('EZ', 'Autorización de salida Zede - exportación por Zede'),
        ('6', 'Despacho de energía eléctrica'),
        ('3', 'Despacho de envío de socorro'),
        ('2', 'Despacho de envío de urgencia'),
        ('9', 'Despacho de hidrocarburos'),
        ('4', 'Despacho de material bélico'),
        ('7', 'Despacho fluvial'),
        ('0', 'Despacho normal'),
        ('R', 'Despacho retorno de exportación'),
        ('X', 'Despacho sin ingreso a depósito temporal'),
        ('Y', 'Despacho sin número de carga'),
        ('8', 'Despacho traspaso de obra'),
        ('P', 'Despacho viajeros internacionales'),
        ('E', 'Envio sin finalidad comercial'),
        ('D', 'Exportación'),
        ('ZP', 'Exportación por Zede/Pertrechos/equipos'),
        ('Z', 'Reexportación por Zede'),
        ('S', 'Simplificado'),
    ], 'Tipo de despacho', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    dispatch_type_translated = dispatch_type.translated('dispatch_type')
    payment_type = fields.Selection([
        ('C', 'Facilidades de pago'),
        ('A', 'Pago anticipado'),
        ('B', 'Pago garantizado'),
    ], 'Tipo de pago', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    payment_type_translated = payment_type.translated('payment_type')

    importer = fields.Many2One('importer.importer', 'Ordenante', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    importer_code_oce = fields.Char('Código OCE', states={
        'required': True,
        'readonly': STATES['readonly'],
    }, help='Código de Operación de Comercialización.')
    importer_address = fields.Text('Dirección', states={
        'readonly': STATES['readonly'],
    })
    importer_phone = fields.Char('Telefono', states={
        'readonly': STATES['readonly'],
    })
    importer_email = fields.Char('E-mail', states={
        'readonly': STATES['readonly'],
    })
    importer_country = fields.Char('País', states={
        'readonly': STATES['readonly'],
    })
    importer_ciiu = fields.Char('Actividad económica', states={
        'readonly': STATES['readonly'],
    })

    provider = fields.Many2One('party.party', 'Proveedor', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    provider_address = fields.Text('Dirección proveedor', states={
        'readonly': STATES['readonly'],
    })

    declarant = fields.Many2One('importation.declarant', 'Declarante',
        states={
            'required': False,
            'readonly': STATES['readonly'],
        })
    declarant_code = fields.Char('Código declarante', states={
        'required': False,
        'readonly': STATES['readonly'],
    })
    declarant_address = fields.Text('Dirección declarante', states={
        'readonly': STATES['readonly'],
    })
    description_merchant = fields.Text('Descripción mercancia', states={
        'required': False,
        'readonly': STATES['readonly'],
    })
    from_country = fields.Many2One('country.country', 'País de procedencia',
        states={
            'required': True,
            'readonly': STATES['readonly'],
        })
    endorsement_code = fields.Selection([
        ('00', 'Sin endoso'),
        ('01', 'Endoso extranjero'),
        ('02', 'Endoso local'),
    ], 'Código de endoso', states={
        'required': True,
        'readonly': STATES['readonly'],
    })
    endorsement_code_translated = endorsement_code.translated('endorsement_code')
    consignee = fields.Char('Nombre consignatario',
        states={
            'readonly': STATES['readonly'],
        })
    charge_number = fields.Char('Número de carga',
        states={
            'readonly': STATES['readonly'],
        })
    transport_document = fields.Binary('Documento de transporte', states={
        'required': False,
        'readonly': STATES['readonly'],
    })

    assets = fields.One2Many('importation.importation.line',
        'importation', 'Bienes / Partidas arancelarias', states={
            'required': True,
            'readonly': STATES['readonly'],
        }, domain=[('asset.is_required', '=', True)])

    certificate = fields.Many2One('public.budget.certificate', 'Certificado',
        states={
            'required': True,
            'readonly': STATES['readonly'],
    })
    confirmed_by = fields.Many2One(
        'company.employee', 'Confirmado por', readonly=True)
    confirmed_date = fields.DateTime('Fecha confirmación', readonly=True)
    done_by = fields.Many2One(
        'company.employee', 'Realizado por', readonly=True)
    done_date = fields.DateTime('Fecha realizado', readonly=True)

    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('done', 'Realizado'),
    ], 'Estado', readonly=True, required=True)
    credit_cards = fields.One2Many('credit_card.creditcard', 'importation',
        'Carta de crédito', states={
            'readonly': True,
        })
    liquidations = fields.One2Many('importation.liquidation.importation',
        'importation', 'Liquidación importación', states={
            'readonly': True,
        })
    total = fields.Function(fields.Numeric('Total', digits=(16,2)),
        'on_change_with_total')
    tracking = fields.One2Many('importation.tracking', 'importation',
        'Estado pedido')


    @classmethod
    def __setup__(cls):
        super(Importation, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'done'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'done']),
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'done']),
            },
            'done': {
                'invisible': Eval('state').in_(['done', 'draft']),
            },
        })
        cls._error_messages.update({
            'maximum_total_certificate': 'El total de la importación '
            '(%(total_amount)s) supera el total de la certificación '
            '(%(total_certificate)s).',
        })

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('importer')
    def on_change_with_importer_code_oce(self, name=None):
        if self.importer:
            return self.importer.code_oce
        return None

    @fields.depends('importer')
    def on_change_with_importer_address(self, name=None):
        if self.importer:
            return self.importer.street
        return None

    @fields.depends('importer')
    def on_change_with_importer_phone(self, name=None):
        if self.importer:
            return self.importer.phone
        return None

    @fields.depends('importer')
    def on_change_with_importer_email(self, name=None):
        if self.importer:
            return self.importer.email
        return None

    @fields.depends('importer')
    def on_change_with_importer_country(self, name=None):
        if self.importer:
            return self.importer.country
        return None

    @fields.depends('importer')
    def on_change_with_importer_ciiu(self, name=None):
        if self.importer:
            return self.importer.ciiu
        return None

    @fields.depends('declarant')
    def on_change_with_declarant_code(self, name=None):
        if self.declarant:
            return self.declarant.code
        return None

    @fields.depends('declarant')
    def on_change_with_declarant_address(self, name=None):
        if self.declarant:
            return self.declarant.address
        return None

    @fields.depends('assets')
    def on_change_with_total(self, name=None):
        total = Decimal('0.00')
        for asset in self.assets:
            if asset.total_price:
                total += asset.total_price
        return total

    @fields.depends('provider', )
    def on_change_with_provider_address(self, name=None):
        Address = Pool().get('party.address')
        if self.provider:
            addresses = Address.search([('party', '=', self.provider)])
            for address in addresses:
                return address.street
        return None

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, importations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirmed(cls, importations):
        for importation in importations:
            cls._check_total_certificate(importation)
            importation.confirmed_date = datetime.today()
        cls.save(importations)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def done(cls, importations):
        for importation in importations:
            importation.done_date = datetime.today()
        cls.save(importations)

    @classmethod
    def _check_total_certificate(cls, importation):
        if importation.certificate.total < importation.total:
            cls.raise_user_error('maximum_total_certificate', {
                'total_amount': importation.total,
                'total_certificate': importation.certificate.total,
            })

    def get_rec_name(self, name):
        return '%s' % (self.number_expedition)


class Custom(ModelSQL, ModelView):
    'Custom'
    __name__ = 'importation.custom'
    _history = True

    name =fields.Char('Nombre', required=True)
    address = fields.Char('Dirección', required=True)
    phone = fields.Char('Telefono')


class ImportationLine(ModelSQL, ModelView):
    'Importation Line'
    __name__ = 'importation.importation.line'
    _history = True

    importation = fields.Many2One('importation.importation', 'Importacion',
        required=True, states={
            'readonly': True,
        })
    liquidation_importation = fields.Many2One(
        'importation.liquidation.importation', 'liquidación Importacion',
        required=False, states={
            'readonly': True,
        })
    product = fields.Many2One('product.product', 'Producto',
        ondelete='RESTRICT', states={
            'required': True,
            'readonly': Eval('_parent_importation', {}).get('state', '') != 'draft',
    })
    asset = fields.Many2One('importation.tariff.asset', 'Partidas bienes',
        states={
            'required': True,
            'readonly': Eval('_parent_importation', {}).get('state', '') != 'draft',
        })
    quantity = fields.Numeric('Cantidad', states={
        'required': True,
        'readonly': Eval('_parent_importation', {}).get('state', '') != 'draft',
    }, digits=(16,2))
    unit = fields.Function(fields.Many2One('product.uom', 'Unidad'),
        'on_change_with_unit')
    unit_price = fields.Numeric('Precio unitario', digits=(16,2), states={
        'required': True,
        'readonly': Eval('_parent_importation', {}).get('state', '') != 'draft',
    })
    tariff_surcharge = fields.Numeric('Recargo arancelario', digits=(16,2),
        states={
            'required': True,
            'readonly': True,
        })
    total_price = fields.Numeric('Total', digits=(16,2), states={
        'readonly': True,
        'required': True,
    })
    accounts = fields.One2Many('importation.importation.line.account',
        'importation_tariff_asset', 'Detalle presupuestario', states={
            'readonly': Eval('_parent_importation', {}).get('state','') != 'draft',
        })
    taxes = fields.Many2Many('account.importation.line-account.tax',
         'line', 'tax', 'Impuestos',
         order=[('tax.sequence', 'ASC'), ('tax.id', 'ASC')],
         domain=[],
         states={
             'invisible': False,
             'readonly': False
         })

    importation_state = fields.Function(fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('done', 'Realizado'),
    ], 'Estado importación', states={
        'invisible': True,
    }), 'on_change_with_importation_state')

    @classmethod
    def default_importation_state(cls):
        return 'draft'

    @fields.depends('asset')
    def on_change_with_unit(self, name=None):
        if self.asset:
            return self.asset.unit.id
        else:
            return None

    @fields.depends('quantity', 'unit_price', 'asset')
    def on_change_with_tariff_surcharge(self, name=None):
        if self.quantity and self.unit_price and self.asset:
            return ((self.unit_price * self.quantity) * (self.asset.tariff / 100)
                    ).quantize(Decimal('0.00'))
        else:
            return None

    @fields.depends('quantity', 'unit_price', 'asset')
    def on_change_with_total_price(self, name=None):
        if self.quantity and self.unit_price and self.asset:
            return ((self.quantity * self.unit_price) + (self.unit_price *
                    self.quantity) * (self.asset.tariff / 100)
                    ).quantize(Decimal('0.00'))
        else:
            return None

    @fields.depends('importation', '_parent_importation.state')
    def on_change_with_importation_state(self, name=None):
        if self.importation:
            return self.importation.state
        return 'draft'


class ImportationLineAccount(ModelSQL, ModelView):
    'Importation Line Account'
    __name__ = 'importation.importation.line.account'
    _history = True

    importation_tariff_asset = fields.Many2One(
        'importation.importation.line', 'Aranceles', ondelete='CASCADE',
        states={
            'readonly': True,
            'required': True
        })
    budget = fields.Many2One('public.budget', 'Partida', states={
        'readonly': (Eval('_parent_importation_tariff_asset', {}
                          ).get('importation_state', '') != 'draft'),
    }, depends=['importation_tariff_asset'])
    quantity = fields.Numeric('Cantidad', digits=(16, 4), domain=[
        ('quantity', '>', 0)
    ], states={
        'required': True,
        'readonly': (Eval('_parent_importation_tariff_asset', {}
                          ).get('importation_state', '') != 'draft'),
    }, depends=['importation_tariff_asset'])
    amount = fields.Numeric('Valor', digits=(16, 4), states={
        'readonly': (Eval('_parent_importation_tariff_asset', {}
                          ).get('importation_state', '') != 'draft'),
        'required': True
    }, depends=['importation_tariff_asset'])


class ImportationLineTax(ModelSQL):
    'Importation Line - Tax'
    __name__ = 'account.importation.line-account.tax'
    _history = True

    line = fields.Many2One('importation.importation.line', 'Linea importacion',
            ondelete='CASCADE', select=True, required=True)
    tax = fields.Many2One('account.tax', 'Tax', ondelete='RESTRICT',
            required=True)


class ImportationDeclarant(ModelSQL, ModelView):
    'Importation Declarant'
    __name__ = 'importation.declarant'
    _history = True

    name = fields.Char('Nombres', states={
        'required': True,
    })
    code = fields.Char('Código declarante', states={
        'required': True,
    })
    address = fields.Char('Dirección', states={
        'required': False,
    })


class ImportationLiquidationImportation(Workflow, ModelSQL, ModelView):
    'Importation Liquidation Importation'
    __name__ = 'importation.liquidation.importation'
    _history = True

    importation = fields.Many2One('importation.importation', 'Importación',
        states={
            'required': True,
            'readonly': Bool(Eval('state') != 'draft'),
        }, domain=[
            ('state', '=', 'done'),
            # ('liquidations', '=', None),
        ])
    creditcard = fields.Many2One('credit_card.creditcard', 'Carta crédito',
        states={
            'required': True,
            'readonly': Bool(Eval('state') != 'draft'),
        }, domain=[
            ('importation', '=', Eval('importation')),
        ], depends=['importation'])
    liquidation_creditcard = fields.Many2One(
        'credit_card.liquidation.creditcard', 'Liquidación Carta crédito',
        states={
            'required': True,
            'readonly': Bool(Eval('state') != 'draft'),
        }, domain=[
            ('credit_card', '=', Eval('creditcard')),
        ], depends=['creditcard'])
    adjustment_change_currency = fields.Function(fields.Numeric(
        'Cambiario de ajuste', states={
            'readonly': True,
        }), 'on_change_with_adjustment_change_currency')
    assets_importation = fields.One2Many('importation.importation.line',
        'liquidation_importation', 'Bienes', states={
            'required': True,
            'readonly': True,
        })
    confirmed_by = fields.Many2One(
        'company.employee', 'Confirmado por', readonly=True)
    confirmed_date = fields.DateTime('Fecha confirmación', readonly=True)
    liquidated_by = fields.Many2One(
        'company.employee', 'Liquidado por', readonly=True)
    liquidated_date = fields.DateTime('Fecha liquidación', readonly=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('liquidated', 'Liquidado'),
    ], 'Estado', readonly=True, required=True)
    to_location = fields.Many2One('stock.location', 'Ubicación destino',
        states={
            'required': True,
            'readonly': Bool(Eval('state') != 'draft'),
        })


    nature = fields.Many2One('importation.nature', 'Naturaleza', states={
        'readonly': Bool(Eval('state') != 'draft'),
    })
    negotiation_term = fields.Selection([
        ('', ''),
        ('CFR', 'Costo y flete'),
        ('CIF', 'Costo, seguro y flete'),
        ('EXW', 'En fábrica'),
        ('DEQ', 'Entrgea en el muelle'),
        ('DES', 'Entrega sobre el buque'),
        ('DDU', 'Entrega derechos no pagados'),
        ('DDP', 'Entrega derechos pagados'),
        ('DAF', 'Entregada en la frontera'),
        ('DAP', 'Entregada en lugar'),
        ('DAT', 'Entregada en terminal'),
        ('FOB', 'Franco a bordo'),
        ('FAS', 'Franco al costado del buque'),
        ('FCA', 'Franco transportista'),
        ('XXX', 'Otras condiciones de entrega'),
        ('CPT', 'Transporte pagado hasta'),
        ('CIP', 'Transporte y seguro pagado hasta'),
    ], 'Término de negociación', states={
        'readonly': Bool(Eval('state') != 'draft'),
    })
    place_importation = fields.Char('Lugar entrega', states={
        'readonly': Bool(Eval('state') != 'draft'),
    })
    shipping_way = fields.Selection([
        ('', ''),
        ('1', 'Envío fraccionado'),
        ('2', 'Envío único'),
    ], 'Forma de envío', states={
        'readonly': Bool(Eval('state') != 'draft'),
    })
    payment_term = fields.Many2One('importation.payment.term', 'Forma de pago',
        states={
            'readonly': Bool(Eval('state') != 'draft'),
        })
    payment_method = fields.Selection([
        ('', ''),
        ('01', 'Efectivo'),
        ('02', 'Cheque'),
        ('03', 'Orden de pago simple'),
        ('04', 'Remesa simple'),
        ('05', 'Remesa documentaria'),
        ('06', 'Cŕedito documentario'),
        ('07', 'Otro'),
    ], 'Medio de pago', states={
        'readonly': Bool(Eval('state') != 'draft'),
    })

    @classmethod
    def __setup__(cls):
        super(ImportationLiquidationImportation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('importation_uniq', Unique(t, t.importation),
             'Ya existe una liquidación para la importación. \n La importación '
             'es único.')
        ]
        cls._error_messages.update({
            # 'not_revised_card_requirements': ('Para continuar con el proceso '
            # 'primero debe de REVISAR cada uno de los REQUERIMIENTOS DE '
            # 'PAGO e ir dando click en el check de revisado.'),
        })
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'liquidated'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state').in_(['draft', 'liquidated']),
                'depends': ['state'],
            },
            'confirmed': {
                'invisible': Eval('state').in_(['confirmed', 'liquidated']),
                'depends': ['state'],
            },
            'liquidated': {
                'invisible': Eval('state').in_(['draft', 'liquidated']),
                'depends': ['state'],
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('importation')
    def on_change_with_assets_importation(self, name=None):
        if self.importation:
            ids_assets = []
            for asset in self.importation.assets:
                ids_assets.append(asset.id)
            return ids_assets
        else:
            return []

    @fields.depends('importation')
    def on_change_with_creditcard(self, name=None):
        if self.importation:
            for credit_card in self.importation.credit_cards:
                return credit_card.id
            return None
        else:
            return None

    @fields.depends('importation')
    def on_change_with_liquidation_creditcard(self, name=None):
        if self.importation:
            for credit_card in self.importation.credit_cards:
                for liquidation_credit_card in credit_card.liquidations:
                    return liquidation_credit_card.id
            return None
        else:
            return None

    @fields.depends('importation')
    def on_change_with_adjustment_change_currency(self, name=None):
        if self.importation:
            for credit_card in self.importation.credit_cards:
                for liquidation_credit_card in credit_card.liquidations:
                    return liquidation_credit_card.adjustment_change_currency
            return None
        else:
            return None

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, liquidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirmed(cls, liquidations):
        for liquidation in liquidations:
            liquidation.confirmed_date = datetime.today()
        cls.save(liquidations)

    @classmethod
    @ModelView.button
    @Workflow.transition('liquidated')
    @set_employee('liquidated_by')
    def liquidated(cls, liquidations):
        for liquidation in liquidations:
            liquidation.liquidated_date = datetime.today()
            cls.create_shipment_in(liquidation)
        cls.save(liquidations)


    @classmethod
    def create_shipment_in(cls, liquidation):
        Shipment = Pool().get('stock.shipment.in')
        StockMove = Pool().get('stock.move')
        StockLocation = Pool().get('stock.location')
        MoveTax = Pool().get('stock.move.tax')
        MoveAccount = Pool().get('stock.move.account')

        new_shipment = Shipment()
        new_shipment.company = liquidation.importation.importer.company
        new_shipment.supplier = liquidation.importation.provider
        new_shipment.planned_date = datetime.today().date()
        new_shipment.certificate = liquidation.importation.certificate

        if hasattr(Shipment, 'to_location'):
            new_shipment.to_location = liquidation.to_location

        list_products = []
        adjustment = 0
        if liquidation.adjustment_change_currency != 0:
            adjustment = (liquidation.adjustment_change_currency /
                          len(liquidation.importation.assets))
        to_save_taxes = []
        to_save_accounts = []
        for asset in liquidation.importation.assets:
            from_location, = StockLocation.search([
                ('code', '=', 'SUP'),
            ])
            to_location, = StockLocation.search([
                ('code', '=', 'IN'),
            ])
            unit_price = ((asset.unit_price + (adjustment / asset.quantity) +
                           (asset.tariff_surcharge / asset.quantity))
            ).quantize(Decimal('0.000000'))
            new_stock_move = StockMove()
            new_stock_move.product = asset.product
            new_stock_move.uom = asset.unit
            new_stock_move.quantity = float(asset.quantity)
            new_stock_move.unit_price = unit_price
            new_stock_move.base_unit_price = unit_price
            new_stock_move.shipment = new_shipment
            new_stock_move.from_location = from_location
            new_stock_move.to_location = to_location
            new_stock_move.origin = asset

            new_stock_move.state = 'draft'
            list_products.append(new_stock_move)

            to_save_accounts += cls.create_account_budget(asset.accounts, new_stock_move)
            to_save_taxes += cls.create_moves_tax(asset, new_stock_move)

        Shipment.save([new_shipment])
        StockMove.save(list_products)
        MoveAccount.save(to_save_accounts)
        MoveTax.save(to_save_taxes)
        # print()

    @classmethod
    def create_moves_tax(cls, line, move):
        MoveTax = Pool().get('stock.move.tax')
        moveTaxes = []
        for tax in line.taxes:
            newMT = MoveTax(
                move=move,
                tax=tax
            )
            moveTaxes.append(newMT)
        return moveTaxes

    @classmethod
    def create_account_budget(self, line_accounts, move):
        MoveAccount = Pool().get('stock.move.account')
        accounts = []
        for account in line_accounts:
            nSMA = MoveAccount(
                move=move,
                product=move.product,
                amount=account.amount,
                quantity=account.quantity
            )
            nSMA.on_change_with_stock()
            accounts.append(nSMA)
        return accounts


class ImportationNature(ModelSQL, ModelView):
    'Importation Nature'
    __name__ = 'importation.nature'
    _history = True

    name = fields.Char('Nombre', states={
        'required': True,
    })


class ImportationPaymentTerm(ModelSQL, ModelView):
    'Importation Payment Term'
    __name__ = 'importation.payment.term'
    _history = True

    name = fields.Char('Nombre', states={
        'required': True,
    })


class ImportationConfiguration(ModelSQL, ModelView):
    'Importation Configuration'
    __name__ = 'importation.configuration'
    _history = True

    days_expired_credit_card = fields.Integer('Días para expiración C.C.')

    @staticmethod
    def default_days_expired_credit_card():
        return 90


class ImportationTracking(ModelSQL, ModelView):
    'Importation Tracking'
    __name__ = 'importation.tracking'
    _history = True

    importation = fields.Many2One('importation.importation', 'Importacion',
        states={
            'readonly': True,
            'required': True,
        }, ondelete='CASCADE')
    state_tracking = fields.Char('Estado del pedido', states={
        'required': True,
        'readonly': False,
    })
    date = fields.DateTime('Fecha/Hora', states={
        'required': True,
        'readonly': False,
    })
