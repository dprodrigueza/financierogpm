from trytond.model import ModelView, ModelSQL, fields, DeactivableMixin
from trytond.transaction import Transaction

__all__ = ['Importer']

class Importer(DeactivableMixin,ModelSQL, ModelView):
    'Importer'
    __name__ = 'importer.importer'
    _history = True

    company = fields.Many2One('company.company', 'Empresa', states={
        'required': True,
        'readonly': False,
    })
    identifier = fields.Function(fields.Char('RUC'),
        'get_identifier', searcher='search_identifier')
    phone = fields.Function(fields.Char('Telefono'), 'on_change_with_phone')
    country = fields.Function(fields.Char('País'), 'on_change_with_country')
    street = fields.Function(fields.Char('Dirección'), 'on_change_with_street')
    email = fields.Function(fields.Char('E-Mail'), 'on_change_with_email')
    ciiu = fields.Function(fields.Char('Actividad económica'),
        'on_change_with_ciiu')
    code_oce = fields.Char('Código OCE', states={'required': True},
        help='Código de Operación de Comercialización.')
    licenses = fields.One2Many('importation_license.import_license', 'importer',
        'Licencias', states={
            'readonly': False
        })


    @staticmethod
    def default_company():
        return Transaction().context.get('company')


    def get_identifier(self, name):
        for identifier in self.company.party.identifiers:
            return identifier.code

    @classmethod
    def search_identifier(cls, name, domain):
        return [
            ('party.identifiers.code',) + tuple(domain[1:]),
        ]

    @fields.depends('company')
    def on_change_with_identifier(self, name=None):
        if self.company:
            for identifier in self.company.party.identifiers:
                if identifier.type == 'ec_ci':
                    return identifier.code
        return ''

    @fields.depends('company')
    def on_change_with_phone(self, name=None):
        if self.company:
            return getattr(self.company.party, 'phone')
        return None

    @fields.depends('company')
    def on_change_with_email(self, name=None):
        if self.company:
            return getattr(self.company.party, 'email')
        return None

    @fields.depends('company')
    def on_change_with_country(self, name=None):
        if self.company and self.company.party.addresses:
            default_address = self.company.party.addresses[0]
            if default_address.country:
                return default_address.country.name
            return ''
        return ''

    @fields.depends('company')
    def on_change_with_street(self, name=None):
        if self.company and self.company.party.addresses:
            default_address = self.company.party.addresses[0]
            if default_address.street:
                return default_address.street
            return ''
        return ''

    @fields.depends('company')
    def on_change_with_ciiu(self, name=None):
        if self.company and self.company.name_economic_activity:
            return self.company.name_economic_activity
        return ''

    def get_rec_name(self, name):
        return "%s / %s" % (self.company.party.name, self.identifier)
