from trytond.model import Workflow, ModelView, ModelSQL, fields, \
    DeactivableMixin
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['ImportLicense']


class ImportLicense(DeactivableMixin, ModelSQL, ModelView):
    'ImportLicense'
    __name__ = 'importation_license.import_license'

    importer = fields.Many2One('importer.importer', 'Ordenante', states={
        'required': True,
        'readonly': False
    })
    authorization = fields.Char('Número de autorización', states={
        'required': True
    })
    emission_date = fields.Date('Fecha de emisión', states={
        'required': True,
    })
    expired_date = fields.Date('Fecha de vencimiento', states={
        'required': True,
    }, domain=[
        ('expired_date', '>=', Eval('emission_date'))
    ], depends=['emission_date'])
    license_type = fields.Selection([
        ('automatic', 'Automática'),
        ('not_automatic', 'No automática'),
    ], 'Tipo de licencia', states={
        'required': True
    })

    @staticmethod
    def default_license_type():
        return 'automatic'

    @staticmethod
    def default_emission_date():
        Date = Pool().get('ir.date')

        today = Date.today()
        return today


    
