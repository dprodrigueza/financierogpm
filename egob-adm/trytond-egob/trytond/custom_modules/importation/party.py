from trytond.model import fields, ModelView, ModelSQL
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool

__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    credit_cart_account = fields.Many2One('account.account',
                                          'Cuenta de carta crédito ',)

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()



