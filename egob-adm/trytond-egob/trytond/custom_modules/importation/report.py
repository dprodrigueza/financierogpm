import ast
import json
from calendar import different_locale, month_name
from collections import defaultdict
from decimal import Decimal

from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.modules.hr_ec_payslip import evaluator
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['ReportImportationImportation', 'ReportCreditCard',]


class ReportImportationImportation(CompanyReportSignature):
    __name__ = 'importation.importation.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportImportationImportation, cls).get_context(
            records, data)
        # report_context['_get_totals_liquidation'] = cls._get_totals_liquidation
        return report_context


class ReportCreditCard(CompanyReportSignature):
    __name__ = 'credit_card.creditcard.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(ReportCreditCard, cls).get_context(
            records, data)
        # report_context['_get_totals_liquidation'] = cls._get_totals_liquidation
        return report_context

