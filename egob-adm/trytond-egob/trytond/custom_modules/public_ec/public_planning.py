# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from collections import defaultdict
import re

from trytond.model import Workflow, ModelView, ModelSQL, fields, \
    Unique, sequence_ordered
from trytond.pyson import Bool, Eval, If
from trytond.transaction import Transaction
from trytond.pool import Pool

from public_mixin import PublicGenericMixin, PublicContextMixin


__all__ = ['PublicPlanningUnitContext', 'PublicIndicator',
    'PublicPlanningUnit', 'PublicPlanningUnitUser',
    'PublicPlanningUnitIndicator', 'PublicPoaConfiguration']

_STATES = {'readonly': Eval('state') != 'draft'}
_DEPENDS = ['state']
_ZERO = Decimal(0)
STATES = [
    ('draft', 'Draft'),
    ('close', 'Close'),
    ('open', 'Open'),
    ('cancel', 'Canceled'),
]
PPU_TYPES = [
    ('poa', 'POA'),
    ('program', 'Program'),
    ('project', 'Project'),
    ('activity', 'Activity')
]


class PublicPlanningUnitContext(PublicContextMixin, ModelView):
    'Public Planning Unit Context'
    __name__ = 'public.planning.unit.context'

    program = fields.Many2One('public.planning.unit', 'Program', required=False,
        domain=[
            ('type', '=', 'program'),
            ('parent', 'child_of', Eval('poa'), 'parent'),
        ], depends=['poa'])
    project = fields.Many2One('public.planning.unit', 'Project', required=False,
        states={
            'readonly': ~Bool(Eval('program')),
        },
        domain=[
            ('type', '=', 'project'),
            ('parent', 'child_of', Eval('program'), 'parent'),
        ], depends=['program'])

    def on_change_poa(self):
        super(PublicPlanningUnitContext, self).on_change_poa()
        self.program = None
        self.project = None

    @fields.depends('program')
    def on_change_program(self):
        self.project = None


class PublicIndicator(PublicGenericMixin, ModelSQL, ModelView):
    """Public Indicator"""
    __name__ = 'public.indicator'


class PublicPlanningUnit(sequence_ordered(), Workflow, ModelView, ModelSQL):
    """Public Planning Unit"""
    __name__ = 'public.planning.unit'
    company = fields.Many2One('company.company', 'Company', required=True,
        states=_STATES, depends=_DEPENDS)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        states={
            'readonly': (Eval('type') != 'poa') | (Eval('state') != 'draft'),
        }, required=True)
    name = fields.Char('Name', required=True, states=_STATES, depends=_DEPENDS)
    code = fields.Char(
        'Code', required=True, states=_STATES, depends=_DEPENDS)
    description = fields.Text('Description', states=_STATES, depends=_DEPENDS)
    state = fields.Selection(STATES, 'State', readonly=True, required=True)
    start_date = fields.Date('Starting Date', required=True, states=_STATES,
        domain=[('start_date', '<=', Eval('end_date', None))],
        depends=_DEPENDS + ['end_date'])
    end_date = fields.Date('Ending Date', required=True, states=_STATES,
        domain=[('end_date', '>=', Eval('start_date', None))],
        depends=_DEPENDS + ['start_date'])
    type = fields.Selection('get_ppu_types', 'Type', select=True, required=True,
        states={
            'readonly': (Eval('state') != 'draft') & (Eval('type') == 'poa'),
        }, depends=['state', 'type'])
    budgets = fields.One2Many('public.budget', 'planning_unit', 'Budgets',
        states={
            'readonly': Eval('type') != 'activity',
            'required': Eval('type') == 'activity'
        },
        domain=[
            ('kind', '=', 'other'),
        ], depends=['type'])
    # budget_type = fields.Selection([
    #    ('income', 'Income'),
    #    ('expense', 'Expense')],
    #    'Budget Type', required=True, states=_STATES, depends=_DEPENDS)
    notes = fields.Text('Notes')
    parent = fields.Many2One('public.planning.unit', 'Parent',
        left='left', right='right', ondelete='RESTRICT',
        domain=[
            ('type', 'in', Eval('context', {}).get('parent_type',
                ['poa', 'program', 'project', 'activity'])),
        ],
        states={
            'invisible': Eval('type') == 'poa',
        }, depends=['type'])
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    parent_type = fields.Function(fields.Selection(PPU_TYPES, 'Parent Type',
         states={
             'invisible': Eval('type') == 'poa',
         }, depends=['type']), 'on_change_with_parent_type')
    children = fields.One2Many('public.planning.unit', 'parent', 'Children',
        states={
            'invisible': Bool(Eval('budgets')),
        },
        domain=[
            ('company', '=', Eval('company')),
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['company', 'fiscalyear'])
    indicators = fields.One2Many(
        'public.planning.unit.indicator', 'planning_unit', 'Indicators',
        states={
            'readonly': _STATES['readonly'],
        }, depends=_DEPENDS + ['type'])
    ceiling_budget = fields.Numeric('Ceiling Budget', digits=(16, 2),
        states={
            'readonly': _STATES['readonly'],
            'required': Eval('type') == 'project',
        }, depends=_DEPENDS + ['type'])
    initial_amount = fields.Function(
        fields.Numeric('Initial Budget', digits=(16, 2)), 'get_budget')
    codified_amount = fields.Function(
        fields.Numeric('Codified Budget', digits=(16, 2)), 'get_budget')
    certificate_amount = fields.Function(
        fields.Numeric('Certificate Budget', digits=(16, 2)), 'get_budget')
    compromise_amount = fields.Function(
        fields.Numeric('Compromise Budget', digits=(16, 2)), 'get_budget')
    progress = fields.Function(
        fields.Float('Progress', digits=(16, 2)), 'get_budget')
    project_works = fields.One2Many(
        'project.work', 'planning_unit', 'Project Detail',
        domain=[
            ('type', '=', 'project'),
            ('company', '=', Eval('company')),
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': Eval('type') == 'project',
        }, depends=_DEPENDS + ['company', 'type'])
    users = fields.Many2Many(
        'public.planning.unit-res.user', 'planning_unit', 'user', 'Users')

    @classmethod
    def __setup__(cls):
        super(PublicPlanningUnit, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('ppu_name_uniq', Unique(t, t.fiscalyear, t.name, t.parent),
             'Name must be unique per fiscalyear and parent'),
            ('ppu_code_uniq', Unique(t, t.code),
                'Planning Unit code must be unique'),
        ]
        cls._error_messages.update({
            'overweight': 'Total weight of project "%(project)s" is over 100.',
            'parent_code_not_related': ('the planning unit code %(code)s '
                'must be start with %(parent_code)s')
        })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'draft'),
            ('open', 'close'),
            ('draft', 'cancel')))
        eval_poa = (Eval('type') != 'poa')
        cls._buttons.update({
            'draft': {
                'invisible': eval_poa | (~Eval('state').in_(['open'])),
                'icon': 'tryton-go-previous'
                },
            'open': {
                'invisible': eval_poa | (~Eval('state').in_(['draft'])),
                'icon': 'tryton-go-next'
                },
            'close': {
                'invisible': eval_poa | (~Eval('state').in_(['open'])),
                'icon': 'tryton-close'
                },
            'cancel': {
                'invisible': eval_poa | (~Eval('state').in_(['draft'])),
                'icon': 'tryton-cancel'}
        })
        del eval_poa

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_type():
        return 'poa'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @staticmethod
    def default_project_works():
        if Transaction().user == 0:
            return []
        return [{}]

    @fields.depends('parent')
    def on_change_parent(self):
        if self.parent:
            if self.parent.start_date:
                self.start_date = self.parent.start_date
            if self.parent.end_date:
                self.end_date = self.parent.end_date
            self.code = self.parent.code

    @fields.depends('parent', '_parent_parent.type')
    def on_change_with_parent_type(self, name=None):
        if self.parent:
            if self.parent.type:
                return self.parent.type
        return None

    @fields.depends('parent_type')
    def get_ppu_types(self):
        if self.parent_type:
            if self.parent_type == 'poa':
                return [PPU_TYPES[1]]
            elif self.parent_type == 'program':
                return PPU_TYPES[2:]
            elif self.parent_type == 'project':
                return PPU_TYPES[2:]
            elif self.parent_type == 'activity':
                return [PPU_TYPES[3]]
        else:
            return [PPU_TYPES[0]]

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            if 'parent' in vals and vals.get('parent', None):
                parent = cls(vals['parent'])
                vals['fiscalyear'] = parent.fiscalyear
        return super(PublicPlanningUnit, cls).create(vlist)

    @classmethod
    def write(cls, *args):
        super(PublicPlanningUnit, cls).write(*args)
        actions = iter(args)
        for planning_units, values in zip(actions, actions):
            if values.get('users'):
                pool = Pool()
                pool.get('ir.rule')._domain_get_cache.clear()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, ppus):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, ppus):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, ppus):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, ppus):
        pass

    @classmethod
    def search(cls, args, offset=0, limit=None, order=None, count=False,
            query=False):
        args = args[:]
        context = Transaction().context
        if context.get('poa'):
            poa = context['poa']
            args.append(('parent', 'child_of', poa))
        return super(PublicPlanningUnit, cls).search(args, offset=offset,
            limit=limit, order=order, count=count, query=query)

    @classmethod
    def get_budget(cls, units, names):
        initials = defaultdict(lambda: 0.0)
        codifieds = defaultdict(lambda: 0.0)
        certificates = defaultdict(lambda: 0.0)
        compromises = defaultdict(lambda: 0.0)
        progress = defaultdict(lambda: 0.67)

        result = {}
        result['progress'] = progress
        return result

    def get_rec_name(self, name):
        return '%s - %s' % (self.name, self.code)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('budgets.' + clause[0],) + tuple(clause[1:]),
            ]

    @classmethod
    def validate(cls, ppus):
        super(PublicPlanningUnit, cls).validate(ppus)
        cls.check_recursion(ppus)
        cls.check_codes(ppus)

    @classmethod
    def check_codes(cls, ppus):
        def is_valid_format(code, parent_code):
            search_result = re.search('^[0-9A-Z]{4,}([.][0-9A-Z]{3,})*', code)
            if search_result:
                if search_result.group(0) != code:
                    return False
            else:
                return False
            return True

        for ppu in ppus:
            if ppu.parent:
                if ppu.code.find(ppu.parent.code + '.') != 0:
                    cls.raise_user_error('parent_code_not_related', {
                        'code': ppu.code,
                        'parent_code': ppu.parent.code + '.',
                    })
                if not is_valid_format(ppu.code, ppu.parent.code):
                    cls.raise_user_error('code_format_invalid', {
                        'code': ppu.code,
                        'parent_code': ppu.parent.code + '.',
                    })

    @classmethod
    def view_attributes(cls):
        return super(PublicPlanningUnit, cls).view_attributes() + [
            ('/form/notebook/page[@id="project_detail"]', 'states', {
                    'invisible': Eval('type') != 'project',
                    }),
            ('/form/notebook/page[@id="budget_detail"]', 'states', {
                    'invisible': Eval('type') != 'activity',
                    }),
        ]


class PublicPlanningUnitUser(ModelSQL):
    """Public Planning Unit - User"""
    __name__ = 'public.planning.unit-res.user'
    _table = 'public_planning_unit_res_user_rel'
    planning_unit = fields.Many2One('public.planning.unit', 'Planning Unit',
        select=True, ondelete='CASCADE', required=True)
    user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
        select=True, required=True)


class PublicPlanningUnitIndicator(ModelSQL, ModelView):
    """Public Planning Unit Indicator"""
    __name__ = 'public.planning.unit.indicator'
    planning_unit = fields.Many2One('public.planning.unit',
        'Planning Unit', select=True, required=True, ondelete='CASCADE')
    indicator = fields.Many2One(
        'public.indicator', 'Indicator', required=True)
    weight = fields.Numeric(
        'Weight', digits=(16, 2), required=True,
        domain=[
            ('weight', '>=', 0),
            ('weight', '<=', 1)
        ])


class PublicPoaConfiguration(ModelView, ModelSQL):
    'Public Poa Configuration'
    __name__ = 'public.poa.configuration'
    _history = True

    company = fields.Many2One('company.company', 'Company',
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ])
    poa = fields.Many2One('public.planning.unit', 'POA',
        domain=[
            ('type', '=', 'poa'),
            ('company', '=', Eval('company'))
        ], depends=['company'], required=True)
    reform_sequence = fields.Many2One(
        'ir.sequence', 'Reform Sequence',
        domain=[('code', '=', 'public.budget.reform'),
            ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None)
            ]],
        context={
            'code': 'public.budget.reform',
            'company': Eval('company'),
        }, depends=['company'], required=True)
    certificate_sequence = fields.Many2One(
        'ir.sequence', 'Certificate Sequence',
        domain=[('code', '=', 'public.budget.certificate'),
            ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None)
            ]],
        context={
            'code': 'public.budget.certificate',
            'company': Eval('company'),
        }, depends=['company'], required=True)
    compromise_sequence = fields.Many2One('ir.sequence', 'Compromise Sequence',
        domain=[('code', '=', 'public.budget.compromise'),
            ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None)
            ]],
        context={
            'code': 'public.budget.compromise',
            'company': Eval('company'),
        }, depends=['company'], required=True)
    liquidation_sequence = fields.Many2One(
        'ir.sequence', 'Liquidation Sequence',
        domain=[('code', '=', 'public.budget.liquidation'),
            ['OR',
                ('company', '=', Eval('company')),
                ('company', '=', None)
            ]],
        context={
            'code': 'public.budget.liquidation',
            'company': Eval('company'),
        }, depends=['company'], required=True)

    @classmethod
    def __setup__(cls):
        super(PublicPoaConfiguration, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('poa_conf_uniq', Unique(t, t.poa),
                'POA must be unique'),
        ]
