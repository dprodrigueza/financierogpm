from collections import defaultdict
import functools
from sql import Column
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique, \
    sequence_ordered, Check
from trytond.pool import Pool
from trytond.tools import reduce_ids, grouped_slice
from trytond.pyson import Eval, If, Bool, Or
from trytond.transaction import Transaction

from public_context import PublicContextMixin


__all__ = ['PublicBudgetContext', 'PublicBudget', 'PublicBudgetInitial',
    'PublicBudgetReform', 'PublicBudgetReformLine',
    'PublicBudgetCertificate', 'PublicBudgetCertificateLine',
    'PublicBudgetCompromise', 'PublicBudgetCompromiseLine',
    'PublicBudgetLiquidation', 'PublicBudgetLiquidationLine']

_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_DEPENDS = ['state']
STATES = [
    ('draft', 'Draft'),
    ('confirmed', 'Confirmed'),
    ('done', 'Done'),
    ('cancel', 'Canceled'),
]


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, records, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)

            result = func(cls, records, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [s for s in records
                        if not getattr(s, field) and s.company == company], {
                        field: employee.id,
                        })
            else:
                cls.raise_user_error('without_employee')
            return result
        return wrapper
    return decorator


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def date_field(string):
    return fields.Date(string,
        states={
            'readonly': True,
            })


class PublicBudgetContext(PublicContextMixin, ModelView):
    'Public Budget Context'
    __name__ = 'public.budget.context'


class PublicBudget(ModelView, ModelSQL):
    'Public Budget'
    __name__ = 'public.budget'

    planning_unit = fields.Many2One('public.planning.unit', "Planning Unit",
        states={
            'invisible': Eval('kind') == 'view',
            'required': Eval('kind') != 'view',
        },
        domain=[
            ('type', '=', 'activity'),
        ], depends=['kind'], ondelete="RESTRICT",)
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'on_change_with_currency_digits')
    initial_assignment = fields.Function(fields.Numeric('Initial Assignment',
        digits=(16, Eval('currency_digits', 2)),
        states={
            'readonly': Eval('kind') != 'other',
            'required': (Eval('kind') != 'view') & Eval('planning_unit'),
        }, depends=['currency_digits', 'planning_unit', 'kind']),
        'get_initial_assignment', setter='set_initial_assignment')

    @classmethod
    def __setup__(cls):
        super(PublicBudget, cls).__setup__()
        cls.code.states['readonly'] = Or(
            cls.code.states.get('readonly', True),
            Eval('kind') == 'other')
        cls.code.depends.append('kind')
        cls._error_messages.update({
            'without_planning_unit': (
                'First you must define a Planning Unit.'),
        })
        cls._buttons.update({
            'generate_code': {
                'invisible': ~(Bool(Eval('parent')) &
                    Bool(Eval('planning_unit'))),
            },
        })

    @fields.depends('kind')
    def on_change_parent(self):
        if not self.parent:
            return
        super(PublicBudget, self).on_change_parent()
        if self.kind == 'other':
            self.code = self.parent.code + '.'

    @fields.depends('company')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2

    @classmethod
    def get_initial_assignment(cls, budgets, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Initial = pool.get('public.budget.initial')
        table_a = cls.__table__()
        table_c = cls.__table__()
        initial = Initial.__table__()
        result = {}

        initials = defaultdict(lambda: 0)
        ids = [b.id for b in budgets]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table_a.id, sub_ids)
            cursor.execute(*table_a.join(table_c,
                    condition=(table_c.left >= table_a.left) &
                    (table_c.right <= table_a.right)
                    ).join(initial, condition=initial.budget == table_c.id
                    ).select(
                    table_a.id,
                    Sum(Coalesce(initial.amount, 0)),
                    where=red_sql & (table_c.active == True),
                    group_by=table_a.id))
            lines = cursor.fetchall()
            for line in lines:
                initials[line[0]] = line[1]
        result['initial_assignment'] = initials
        return result

    @classmethod
    def set_initial_assignment(cls, budgets, name, amount):
        pool = Pool()
        Initial = pool.get('public.budget.initial')
        to_create = []
        to_save = []
        for b in budgets:
            initial = Initial.search([('budget', '=', b)])
            if initial:
                initial = initial[0]
                if amount:
                    initial.amount = amount
                else:
                    initial.amount = None
                to_save.append(initial)
            else:
                if amount:
                    to_create.append({
                        'budget': b.id,
                        'amount': amount,
                        'company': b.company.id,
                    })
        if to_create:
            Initial.create(to_create)
        if to_save:
            Initial.save(to_save)

    @classmethod
    @ModelView.button
    def generate_code(cls, budgets):
        for budget in budgets:
            budget.code = "%s.%s" % (budget.parent.code,
                budget.planning_unit.code)
        cls.save(budgets)


class PublicBudgetInitial(Workflow, ModelView, ModelSQL):
    'Public Budget Initial'
    __name__ = 'public.budget.initial'
    _history = True

    company = fields.Many2One('company.company', 'Company',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS)
    budget = fields.Many2One('public.budget', "Budget", required=True,
        domain=[
            ('kind', '=', 'other')
        ], select=True, ondelete="RESTRICT")
    amount = fields.Numeric("Amount", required=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('cancel', 'Cancel'),
        ('close', 'Close'),
    ], 'State', readonly=True)
    poa = fields.Function(fields.Many2One('public.planning.unit', "Poa"),
        'on_change_with_poa', searcher='search_poa')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetInitial, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_initial_uniq', Unique(t, t.budget),
                'Budget initial must be unique'),
        ]
        cls._error_messages.update({
            'lines_existing': ('There are account move lines related to: '
            '"%(budget)s".'),
        })
        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'draft'),
            ('open', 'close'),
            ('draft', 'cancel')))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(['open']),
                'icon': 'tryton-go-previous'
                },
            'open': {
                'invisible': ~Eval('state').in_(['draft']),
                'icon': 'tryton-go-next'
                },
            'close': {
                'invisible': ~Eval('state').in_(['open']),
                'icon': 'tryton-close'
                },
            'cancel': {
                'invisible': ~Eval('state').in_(['draft']),
                'icon': 'tryton-cancel'}
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_amount():
        return 0

    def get_rec_name(self, name):
        return '%s - %s' % (self.budget.code, self.budget.name)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('budget.' + clause[0],) + tuple(clause[1:]),
            ]

    @fields.depends('budget')
    def on_change_with_poa(self, name=None):
        pool = Pool()
        PlanningUnit = pool.get('public.planning.unit')
        if self.budget:
            poa = PlanningUnit.search([
                ('type', '=', 'poa'),
                ('parent', 'parent_of', self.budget.planning_unit.id)
            ])
            if poa:
                return poa[0].id
        return None

    @classmethod
    def search_poa(cls, name, clause):
        return [('budget.p' + clause[0],) + tuple(clause[1:])]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, initials):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        for initial in initials:
            lines = MoveLine.search([('budget', '=', initial.budget)])
            if lines:
                cls.raise_user_error('lines_existing', {
                    'budget': initial.budget
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, initials):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, initials):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, initials):
        pass


class BudgetObjectMixin(object):
    'Mixin class to setup certificate objects'

    _rec_name = 'number'

    company = fields.Many2One('company.company', 'Company',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS)
    number = fields.Char('Number', size=None, readonly=True, select=True)
    description = fields.Char(
        'Description', required=True, states=_STATES, depends=_DEPENDS)
    state = fields.Selection(STATES, 'State', readonly=True, required=True)
    confirmed_date = date_field("Confirmed Date")
    confirmed_by = employee_field("Confirmed By")
    date = date_field("Done Date")
    done_by = employee_field("Done By")
    currency = fields.Many2One('currency.currency', 'Currency', required=True,
        states={
            'readonly': ((Eval('state') != 'draft') |
                (Eval('lines', [0]) & Eval('currency'))),
            },
        depends=['state'])
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'on_change_with_currency_digits')
    notes = fields.Text(
        'Notes', required=True, states=_STATES, depends=_DEPENDS)

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            company = Company(company)
            return company.currency.id

    @fields.depends('currency')
    def on_change_with_currency_digits(self, name=None):
        if self.currency:
            return self.currency.digits
        return 2


class PublicBudgetReform(BudgetObjectMixin, Workflow, ModelView, ModelSQL):
    'Public Budget Reform'
    __name__ = 'public.budget.reform'
    _history = True

    poa = fields.Many2One('public.planning.unit', "POA", required=True,
        domain=[('type', '=', 'poa')],
        states=_STATES, depends=_DEPENDS)
    type = fields.Selection([
        ('increase', 'Increase'),
        ('decrease', 'Decrease'),
        ('transfer', 'Transfer'),
        ], 'Type', required=True,
        states=_STATES, depends=_DEPENDS)
    lines = fields.One2Many('public.budget.reform.line', 'reform',
        'Lines', states=_STATES, depends=_DEPENDS)
    reform_amount = fields.Function(fields.Numeric('Reform Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    budgets = fields.Function(
        fields.Many2Many('public.budget', None, None, 'Budgets'),
        'on_change_with_budgets', searcher='search_budgets')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetReform, cls).__setup__()
        cls._order[0] = ('number', 'DESC')
        cls._error_messages.update({
            'unavailable': ('Not enough funds on budget "%(budget)s", '
                'available amount %(available)s.'),
            'without_employee': ('No employee assigned.'),
            'without_reform_sequence': (
                'No reform sequence configured.'),
        })
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'cancel'),
                ('confirmed', 'done'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'draft': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'do': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                })

    @staticmethod
    def default_type():
        return 'increase'

    @classmethod
    def default_poa(cls):
        pool = Pool()
        Poa = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        date = Date.today()
        poas = Poa.search([
            ('type', '=', 'poa'),
            ('start_date', '<=', date),
            ('end_date', '>=', date)]
        )
        if not poas:
            poas = Poa.search([('state', '=', 'open')])
            if not poas:
                return None
        return poas[0].id

    @fields.depends('lines')
    def on_change_with_budgets(self, name=None):
        result = []
        for line in self.lines:
            result.append(line.budget.id)
        return result

    @classmethod
    def search_budgets(cls, name, clause):
        return [('lines.budget' + clause[0].lstrip(name),) + tuple(clause[1:])]

    @classmethod
    def get_amount(cls, reforms, names):
        pool = Pool()
        reform = cls.__table__()
        cursor = Transaction().connection.cursor()
        result = {}
        ref_amounts = defaultdict(lambda: 0)

        ReformQuery = pool.get('public.budget.reform.query')
        reform_query = ReformQuery.query_get()

        ids = [c.id for c in reforms]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(reform.id, sub_ids)
            query = reform.join(reform_query,
                condition=reform.id == reform_query.reform
            ).select(
                reform.id,
                Sum(reform_query.amount),
                where=red_sql,
                group_by=[reform.id]
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            ref_amounts.update(result)
        return {
            'reform_amount': ref_amounts,
        }

    @classmethod
    def check_availability(cls, reforms):
        pool = Pool()
        BudgetCard = pool.get('public.budget.card')
        for reform in reforms:
            if reform.type in [('decrease', 'transfer')]:
                for line in reform.lines:
                    budget_card, = BudgetCard.search([
                        ('id', '=', line.from_location.id)])
                    if line.amount > budget_card.codified_amount:
                        cls.raise_user_error('unavailable', {
                            'budget': line.from_budget.rec_name,
                            'available': budget_card.codified_amount,
                            })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, reforms):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, reforms):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirm(cls, reforms):
        pool = Pool()
        Date = pool.get('ir.date')
        cls.check_availability(reforms)
        for reform in reforms:
            reform.confirmed_date = Date.today()
        cls.save(reforms)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def do(cls, reforms):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Date = pool.get('ir.date')
        POAConfiguration = pool.get('public.poa.configuration')
        cls.check_availability(reforms)
        for reform in reforms:
            reform.date = Date.today()
            if reform.number:
                continue
            configuration = POAConfiguration.search([('poa', '=', reform.poa)])
            if configuration and configuration[0].reform_sequence:
                reform.number = Sequence.get_id(
                    configuration[0].reform_sequence)
            else:
                cls.raise_user_error('without_reform_sequence')
        cls.save(reforms)

    @classmethod
    def validate(cls, reforms):
        super(PublicBudgetReform, cls).validate(reforms)
        for reform in reforms:
            for line in reform.lines:
                line.check_budget_domain()


class PublicBudgetReformLine(sequence_ordered(), ModelView, ModelSQL):
    'Public Budget Reform Line'
    __name__ = 'public.budget.reform.line'
    _history = True
    reform = fields.Many2One('public.budget.reform', 'Reform',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('reform_state') != 'draft') &
                Bool(Eval('reform'))),
            },
        depends=['reform_state'])
    reform_state = fields.Function(
        fields.Selection(STATES, 'Reform State'),
        'on_change_with_reform_state')
    reform_type = fields.Function(
        fields.Char('Reform Type'), 'on_change_with_reform_type')
    type = fields.Selection([
        ('line', 'Line'),
        ('comment', 'Comment'),
        ], 'Type', select=True, required=True,
        states={
            'readonly': Eval('reform_state') != 'draft',
            },
        depends=['reform_state'])
    from_budget = fields.Many2One('public.budget', 'From Budget',
        states={
            'readonly': (Eval('reform_state') != 'draft') | (
                Eval('reform_type') == 'increase'),
            'invisible': ~Eval('type').in_(['line']) & (
                Eval('reform_type') == 'increase'),
            'required': Eval('type').in_(['line']) &
            Eval('reform_type').in_(['decrease', 'transfer']),
            },
        domain=[
            ('planning_unit.parent', 'child_of',
                Eval('_parent_reform', {}).get('poa', -1), 'parent'),
            ('kind', '=', 'other'),
            ('id', "!=", Eval('to_budget', -1)),
        ], depends=['reform_state', 'reform_type', 'type', 'to_budget'])
    available_budget = fields.Function(
        fields.Numeric('Budget available',
            states={'invisible': False}), 'on_change_with_available_budget')
    to_budget = fields.Many2One('public.budget', 'To Budget',
        states={
            'readonly': (Eval('reform_state') != 'draft') | (
                Eval('reform_type') == 'decrease'),
            'invisible': ~Eval('type').in_(['line']) & (
                Eval('reform_type') == 'decrease'),
            'required': Eval('type').in_(['line']) & (
                Eval('reform_type').in_(['increase', 'transfer'])),
            },
        domain=[
            ('planning_unit.parent', 'child_of',
             Eval('_parent_reform', {}).get('poa', -1), 'parent'),
            ('kind', '=', 'other'),
            ('id', "!=", Eval('from_budget', -1)),
        ], depends=['reform_state', 'reform_type', 'type', 'from_budget'])
    amount = fields.Numeric('Amount',
            digits=(16,
                Eval('_parent_reform', {}).get('currency_digits', 2)),
            states={
                'invisible': ~Eval('type').in_(['line']),
                'required': Eval('type').in_(['line']),
                },
            domain=[
                ('amount', '>', 0),
                If(Eval('reform_type') in ['decrease', 'transfer'],
                   ('amount', '<=', Eval('available_budget')),
                   ())
            ], depends=['type'])
    notes = fields.Text('Notes',
        states={
            'readonly': Eval('reform_state') != 'draft',
            },
        depends=['reform_state'])

    @classmethod
    def __setup__(cls):
        super(PublicBudgetReformLine, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_uniq', Unique(t, t.reform, t.from_budget, t.to_budget),
             'Budget must be unique per reform'),
            ('check_from_to_budgets',
                Check(t, t.from_budget != t.to_budget),
                'Source and destination budget must be different'),
        ]
        cls._error_messages.update({
            'invalid_domain_line': ('Invalid line domain '
                'from budget:"%(from_budget)s" to budget:"%(to_budget)s".'),
        })

    @staticmethod
    def default_type():
        return 'line'

    @staticmethod
    def default_amount():
        return 0

    def get_rec_name(self, name):
        from_name = self.from_budget and self.from_budget.rec_name or ''
        to_name = self.to_budget and self.to_budget.rec_name or ''
        return self.reform.rec_name + ' - ' + from_name + ' - ' + to_name

    @fields.depends('reform', '_parent_reform.state')
    def on_change_with_reform_state(self, name=None):
        if self.reform:
            return self.reform.state

    @fields.depends('reform', '_parent_reform.type')
    def on_change_with_reform_type(self, name=None):
        if self.reform:
            return self.reform.type

    @classmethod
    def validate(cls, lines):
        super(PublicBudgetReformLine, cls).validate(lines)
        for line in lines:
            line.check_budget_domain()

    def check_budget_domain(self):
        error = False
        if self.type == "line":
            if self.reform.type == 'increase':
                if self.to_budget is None or self.from_budget is not None:
                    error = True
            if self.reform.type == 'decrease':
                if self.to_budget is not None or self.from_budget is None:
                    error = True
            if self.reform.type == 'transfer':
                if self.to_budget is None or self.from_budget is None:
                    error = True
            if error:
                self.raise_user_error('invalid_domain_line', {
                    'from_budget': self.from_budget and
                    self.from_budget.rec_name or '',
                    'to_budget': self.to_budget and
                    self.to_budget.rec_name or '',
                })

    @fields.depends('from_budget')
    def on_change_with_available_budget(self, name=None):
        pool = Pool()
        BudgetCard = pool.get('public.budget.card')
        if self.from_budget:
            budget_card, = BudgetCard.search([
                ('id', '=', self.from_budget.id)])
            return budget_card.codified_amount
        return 0


class PublicBudgetCertificate(BudgetObjectMixin, Workflow, ModelView, ModelSQL):
    'Public Budget Certificate'
    __name__ = 'public.budget.certificate'
    _history = True

    poa = fields.Many2One('public.planning.unit', "POA", required=True,
        domain=[('type', '=', 'poa')],
        states=_STATES, depends=_DEPENDS)
    lines = fields.One2Many('public.budget.certificate.line', 'certificate',
        'Lines', states=_STATES, depends=_DEPENDS)
    certificate_amount = fields.Function(fields.Numeric('Certificate Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    compromise_amount = fields.Function(fields.Numeric('Compromise Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    liquidation_amount = fields.Function(fields.Numeric('Liquidation Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    available_amount = fields.Function(fields.Numeric('Available Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount',
        searcher='search_available_amount')
    budgets = fields.Function(
        fields.Many2Many('public.budget', None, None, 'Budgets'),
        'on_change_with_budgets', searcher='search_budgets')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetCertificate, cls).__setup__()
        cls._order[0] = ('number', 'DESC')
        cls._error_messages.update({
            'unavailable': ('Not enough funds on budget "%(budget)s", '
                'available amount %(available)s.'),
            'without_employee': ('No employee assigned.'),
            'without_certificate_sequence': (
                'No certificate sequence configured.'),
        })
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'cancel'),
                ('confirmed', 'done'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'draft': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'do': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                })

    @classmethod
    def default_poa(cls):
        pool = Pool()
        Poa = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        date = Date.today()
        poas = Poa.search([
            ('type', '=', 'poa'),
            ('start_date', '<=', date),
            ('end_date', '>=', date)]
        )
        if not poas:
            poas = Poa.search([('state', '=', 'open')])
            if not poas:
                return None
        return poas[0].id

    @fields.depends('lines')
    def on_change_with_budgets(self, name=None):
        result = []
        for line in self.lines:
            result.append(line.budget.id)
        return result

    @classmethod
    def search_budgets(cls, name, clause):
        return [('lines.budget' + clause[0].lstrip(name),) + tuple(clause[1:])]

    @classmethod
    def get_amount(cls, certificates, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        result = {}
        cert_amounts = defaultdict(lambda: 0)
        comp_amounts = defaultdict(lambda: 0)
        liq_amounts = defaultdict(lambda: 0)
        avail_amounts = defaultdict(lambda: 0)

        Card = pool.get('public.budget.certificate.card')

        ids = [c.id for c in certificates]
        for sub_ids in grouped_slice(ids):
            query_card = Card.table_query()
            red_sql = reduce_ids(query_card.id, sub_ids)
            query = query_card.select(
                query_card.id,
                query_card.certificate_amount,
                query_card.compromise_amount,
                query_card.liquidation_amount,
                where=red_sql
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            for line in result:
                cert_amounts[line[0]] = line[1]
                comp_amounts[line[0]] = line[2]
                liq_amounts[line[0]] = line[3]
                avail_amounts[line[0]] = line[1] - line[2] - line[3]
        return {
            'certificate_amount': cert_amounts,
            'compromise_amount': comp_amounts,
            'liquidation_amount': liq_amounts,
            'available_amount': avail_amounts,
        }

    @classmethod
    def search_available_amount(cls, name, clause):
        pool = Pool()
        _, operator, value = clause
        CertificateCard = pool.get('public.budget.certificate.card')
        certificates = CertificateCard.search([
            ('available_amount', operator, value)
        ], query=True)
        return [('id', 'in', certificates)]

    @classmethod
    def check_availability(cls, certificates):
        pool = Pool()
        BudgetCard = pool.get('public.budget.card')
        for certificate in certificates:
            for line in certificate.lines:
                budget_card, = BudgetCard.search([('id', '=', line.budget.id)])
                if line.amount > budget_card.codified_amount:
                    cls.raise_user_error('unavailable', {
                        'budget': line.budget.rec_name,
                        'available': budget_card.codified_amount,
                        })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, certificates):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, certificates):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirm(cls, certificates):
        pool = Pool()
        Date = pool.get('ir.date')
        cls.check_availability(certificates)
        for certificate in certificates:
            certificate.confirmed_date = Date.today()
        cls.save(certificates)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def do(cls, certificates):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Date = pool.get('ir.date')
        POAConfiguration = pool.get('public.poa.configuration')
        cls.check_availability(certificates)
        for certificate in certificates:
            certificate.date = Date.today()
            if certificate.number:
                continue
            configuration = POAConfiguration.search(
                [('poa', '=', certificate.poa)]
            )
            if configuration and configuration[0].certificate_sequence:
                certificate.number = Sequence.get_id(
                    configuration[0].certificate_sequence)
            else:
                cls.raise_user_error('without_certificate_sequence')
        cls.save(certificates)


class PublicBudgetCertificateLine(sequence_ordered(), ModelView, ModelSQL):
    'Public Budget Certificate Line'
    __name__ = 'public.budget.certificate.line'
    _history = True
    certificate = fields.Many2One('public.budget.certificate', 'Certificate',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('certificate_state') != 'draft') &
                Bool(Eval('certificate'))),
            },
        depends=['certificate_state'])
    certificate_state = fields.Function(
        fields.Selection(STATES, 'Certificate State'),
        'on_change_with_certificate_state')
    type = fields.Selection([
        ('line', 'Line'),
        ('comment', 'Comment'),
        ], 'Type', select=True, required=True,
        states={
            'readonly': Eval('certificate_state') != 'draft',
            },
        depends=['certificate_state'])
    budget = fields.Many2One('public.budget', 'Budget',
        states={
            'readonly': Eval('certificate_state') != 'draft',
            'invisible': ~Eval('type').in_(['line']),
            'required': Eval('type').in_(['line']),
            },
        domain=[
            ('planning_unit.parent', 'child_of',
             Eval('_parent_certificate', {}).get('poa', -1), 'parent'),
            ('kind', '=', 'other'),
            ('type', '=', 'expense'),
        ], depends=['certificate_state', 'type'])
    available_budget = fields.Function(
        fields.Numeric('Budget Available'), 'on_change_with_available_budget')
    cost_center = fields.Many2One('public.cost.center', 'Cost Center',
        states={
            'readonly': Eval('certificate_state') != 'draft',
            'invisible': ~Eval('type').in_(['line']),
            'required': Eval('type').in_(['line']),
        }, depends=['certificate_state', 'type'])
    amount = fields.Numeric('Amount',
            digits=(16,
                Eval('_parent_certificate', {}).get('currency_digits', 2)),
            states={
                'invisible': ~Eval('type').in_(['line']),
                'required': Eval('type').in_(['line']),
                },
            domain=[
                ('amount', '>', 0),
                ('amount', '<=', Eval('available_budget'))
            ], depends=['type', 'available_budget'])
    compromise_amount = fields.Function(fields.Numeric('Compromise Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    liquidation_amount = fields.Function(fields.Numeric('Liquidation Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    available_amount = fields.Function(fields.Numeric('Available Amount',
        digits=(16, Eval('currency_digits', 2))), 'get_amount')
    notes = fields.Text('Notes',
        states={
            'readonly': Eval('certificate_state') != 'draft',
            },
        depends=['certificate_state'])

    @classmethod
    def __setup__(cls):
        super(PublicBudgetCertificateLine, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('budget_uniq', Unique(t, t.certificate, t.budget),
             'Budget must be unique per certificate'),
        ]

    @staticmethod
    def default_type():
        return 'line'

    @staticmethod
    def default_amount():
        return 0

    def get_rec_name(self, name):
        return self.certificate.rec_name + ' / ' + self.budget.rec_name

    @fields.depends('certificate', '_parent_certificate.state')
    def on_change_with_certificate_state(self, name=None):
        if self.certificate:
            return self.certificate.state

    @classmethod
    def get_amount(cls, lines, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        result = {}
        comp_amounts = defaultdict(lambda: 0)
        liq_amounts = defaultdict(lambda: 0)
        avail_amounts = defaultdict(lambda: 0)

        CertificateLine = pool.get('public.budget.certificate.line')
        CompromiseQuery = pool.get('public.budget.compromise.query')
        LiquidationQuery = pool.get('public.budget.liquidation.query')

        line = CertificateLine.__table__()
        comp_query = CompromiseQuery.query_get()
        liq_query = LiquidationQuery.query_get()

        ids = [c.id for c in lines]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(line.id, sub_ids)
            query_temp = line.join(comp_query, type_='LEFT',
                condition=line.id == comp_query.certificate_line
            ).join(liq_query, type_='LEFT',
                condition=line.id == liq_query.certificate_line
            ).select(line.id,
                line.amount.as_('certificate_amount'),
                Coalesce(Sum(comp_query.amount), 0).as_('compromise_amount'),
                Coalesce(Sum(liq_query.amount), 0).as_('liquidation_amount'),
                where=red_sql & (line.type == 'line'),
                group_by=line.id
            )
            rfields = [str(c).replace('"', '') for c in query_temp._columns]
            result_columns = [Column(query_temp, c).as_(c) for c in rfields] + [
                Coalesce(query_temp.certificate_amount -
                    query_temp.compromise_amount -
                    query_temp.liquidation_amount, 0).as_('available_amount')
            ]
            query = query_temp.select(*result_columns)
            cursor.execute(*query)
            result = cursor.fetchall()
            for line in result:
                comp_amounts[line[0]] = line[2]
                liq_amounts[line[0]] = line[3]
                avail_amounts[line[0]] = line[1] - line[2] - line[3]
        return {
            'compromise_amount': comp_amounts,
            'liquidation_amount': liq_amounts,
            'available_amount': avail_amounts,
        }

    @fields.depends('budget')
    def on_change_with_available_budget(self, name=None):
        pool = Pool()
        BudgetCard = pool.get('public.budget.card')
        if self.budget:
            budget_data, = BudgetCard.search([('id', '=', self.budget.id)])
            return budget_data.available_amount
        return 0


class PublicBudgetCompromise(BudgetObjectMixin, Workflow, ModelView, ModelSQL):
    'Public Budget Compromise'
    __name__ = 'public.budget.compromise'
    _history = True
    # TODO: add wizard to create compromise from certificate

    certificate = fields.Many2One('public.budget.certificate', 'Certificate',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('state') != 'draft') &
                Bool(Eval('certificate'))),
            },
        domain=[
            ('state', '=', 'done'),  # TODO: try to do xxx2many search
        ], depends=['state'])
    party = fields.Many2One('party.party', 'Party', required=True,
        states={
            'readonly': ((Eval('state') != 'draft') |
                (Eval('lines', [0]) & Eval('party'))),
            },
        select=True, depends=['state'])
    poa = fields.Function(fields.Many2One('public.planning.unit', 'POA'),
        'on_change_with_poa', searcher='search_poa')
    lines = fields.One2Many('public.budget.compromise.line', 'compromise',
        'Lines', states=_STATES, depends=_DEPENDS)
    amount = fields.Function(
        fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2))),
        'get_amount')
    accrued = fields.Function(
        fields.Numeric('Accrued', digits=(16, Eval('currency_digits', 2))),
        'get_amount')
    executed = fields.Function(
        fields.Numeric('Executed', digits=(16, Eval('currency_digits', 2))),
        'get_amount')
    # TODO: add function fields, available, etc
    budgets = fields.Function(
        fields.One2Many('public.budget', None, 'Budgets'), 'get_budgets')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetCompromise, cls).__setup__()
        cls._order[0] = ('number', 'DESC')
        cls._error_messages.update({
            'unavailable': ('Not enough funds on budget "%(budget)s", '
                'available amount %(available)s.'),
            'without_employee': ('No employee assigned.'),
            'without_compromise_sequence': (
                'No compromise sequence configured.'),
        })
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'cancel'),
                ('confirmed', 'done'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'draft': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'do': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                })

    @fields.depends('certificate', '_parent_certificate.poa')
    def on_change_with_poa(self, name=None):
        if self.certificate:
            return self.certificate.poa.id

    @classmethod
    def search_poa(cls, name, clause):
        return [('certificate.poa' + clause[0].lstrip(name),) +
            tuple(clause[1:])]

    @classmethod
    def get_amount(cls, compromises, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        result = {}
        comp_amounts = defaultdict(lambda: 0)
        accrued_amounts = defaultdict(lambda: 0)
        executed_amounts = defaultdict(lambda: 0)

        Card = pool.get('public.budget.compromise.card')

        ids = [c.id for c in compromises]
        for sub_ids in grouped_slice(ids):
            query_card = Card.table_query()
            red_sql = reduce_ids(query_card.id, sub_ids)
            query = query_card.select(
                query_card.id,
                query_card.compromise_amount,
                query_card.accrued_amount,
                query_card.executed_amount,
                where=red_sql
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            for c_id, c_amount, a_amount, e_amount in result:
                comp_amounts[c_id] = c_amount
                accrued_amounts[c_id] = a_amount
                executed_amounts[c_id] = e_amount
        return {
            'amount': comp_amounts,
            'accrued': accrued_amounts,
            'executed': executed_amounts,
        }

    @classmethod
    def get_budgets(cls, compromises, names):
        budgets = defaultdict(lambda: [])
        pool = Pool()
        cursor = Transaction().connection.cursor()
        CertificateLine = pool.get('public.budget.certificate.line')
        CompromiseLine = pool.get('public.budget.compromise.line')

        comp = cls.__table__()
        comp_line = CompromiseLine.__table__()
        cert_line = CertificateLine.__table__()

        ids = [c.id for c in compromises]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(comp.id, sub_ids)
            query = comp.join(comp_line,
                condition=comp.id == comp_line.compromise
            ).join(cert_line,
                condition=cert_line.id == comp_line.certificate_line
            ).select(
                comp.id,
                cert_line.budget,
                where=red_sql
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            for line in result:
                budgets[line[0]].append(line[1])
        return {
            'budgets': budgets,
        }

    @classmethod
    def check_availability(cls, compromises):
        for compromise in compromises:
            for line in compromise.lines:
                if line.amount > line.certificate_line.available_amount:
                    cls.raise_user_error('unavailable', {
                        'budget': line.certificate_line.budget.rec_name,
                        'available': line.certificate_line.available_amount,
                        })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, compromises):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, compromises):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirm(cls, compromises):
        pool = Pool()
        Date = pool.get('ir.date')
        cls.check_availability(compromises)
        for compromise in compromises:
            compromise.confirmed_date = Date.today()
        cls.save(compromises)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def do(cls, compromises):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Date = pool.get('ir.date')
        POAConfiguration = pool.get('public.poa.configuration')
        cls.check_availability(compromises)
        for compromise in compromises:
            compromise.date = Date.today()
            if compromise.number:
                continue
            configuration = POAConfiguration.search([
                ('poa', '=', compromise.poa)]
            )
            if configuration and configuration[0].compromise_sequence:
                compromise.number = Sequence.get_id(
                    configuration[0].compromise_sequence)
            else:
                cls.raise_user_error('without_compromise_sequence')
        cls.save(compromises)


class PublicBudgetCompromiseLine(sequence_ordered(), ModelView, ModelSQL):
    'Public Budget Compromise Line'
    __name__ = 'public.budget.compromise.line'
    _history = True
    compromise = fields.Many2One('public.budget.compromise', 'Compromise',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('compromise_state') != 'draft') &
                Bool(Eval('compromise'))),
            },
        depends=['compromise_state'])
    compromise_state = fields.Function(
        fields.Selection(STATES, 'Compromise State'),
        'on_change_with_compromise_state')
    certificate_lines = fields.Function(
        fields.One2Many('public.budget.certificate.line', None,
        'Certificate lines'), 'on_change_with_certificate_lines')
    type = fields.Selection([
        ('line', 'Line'),
        ('comment', 'Comment'),
        ], 'Type', select=True, required=True,
        states={
            'readonly': Eval('compromise_state') != 'draft',
            },
        depends=['compromise_state'])
    certificate_line = fields.Many2One(
        'public.budget.certificate.line', 'Certificate Line',
        states={
            'readonly': Eval('compromise_state') != 'draft',
            'invisible': ~Eval('type').in_(['line']),
            'required': Eval('type').in_(['line']),
            },
        domain=[
            ('id', 'in', Eval('certificate_lines'))
        ], depends=['compromise_state', 'type', 'certificate_lines'])
    available_line = fields.Function(
        fields.Numeric('Line available',
            states={'invisible': False}), 'on_change_with_available_line')
    amount = fields.Numeric('Amount',
            digits=(16,
                Eval('_parent_compromise', {}).get('currency_digits', 2)),
            states={
                'invisible': ~Eval('type').in_(['line']),
                'required': Eval('type').in_(['line']),
                },
            domain=[
                ('amount', '>', 0),
                ('amount', '<=', Eval('available_line'))
            ], depends=['type', 'available_line'])
    notes = fields.Text('Notes',
        states={
            'readonly': Eval('compromise_state') != 'draft',
            },
        depends=['compromise_state'])

    @staticmethod
    def default_type():
        return 'line'

    @staticmethod
    def default_amount():
        return 0

    @fields.depends('compromise', '_parent_compromise.state')
    def on_change_with_compromise_state(self, name=None):
        if self.compromise:
            return self.compromise.state

    @fields.depends('compromise', '_parent_compromise.certificate')
    def on_change_with_certificate_lines(self, name=None):
        if self.compromise:
            if self.compromise.certificate.lines:
                return [line.id for line in self.compromise.certificate.lines]
        return None

    @fields.depends('certificate_line')
    def on_change_with_available_line(self, name=None):
        if self.certificate_line:
            return self.certificate_line.available_amount
        return 0


class PublicBudgetLiquidation(BudgetObjectMixin, Workflow, ModelView, ModelSQL):
    'Public Budget Liquidation'
    __name__ = 'public.budget.liquidation'
    _history = True
    # TODO: add wizard to create liquidation from certificate

    certificate = fields.Many2One('public.budget.certificate', 'Certificate',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('state') != 'draft') &
                Bool(Eval('certificate'))),
            },
        domain=[
            ('state', '=', 'done'),  # TODO: try to do xxx_many search
        ], depends=['state'])
    poa = fields.Function(fields.Many2One('public.planning.unit', 'POA'),
        'on_change_with_poa', searcher='search_poa')
    lines = fields.One2Many('public.budget.liquidation.line', 'liquidation',
        'Lines', states=_STATES, depends=_DEPENDS)
    amount = fields.Function(
        fields.Numeric('Amount', digits=(16, Eval('currency_digits', 2))),
        'get_amount')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetLiquidation, cls).__setup__()
        cls._order[0] = ('number', 'DESC')
        cls._error_messages.update({
            'unavailable': ('Not enough funds on budget "%(budget)s", '
                'available amount %(available)s.'),
            'without_employee': ('No employee assigned.'),
            'without_liquidation_sequence': (
                'No liquidation sequence configured.'),
        })
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('draft', 'cancel'),
                ('confirmed', 'done'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'draft': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                'confirm': {
                    'invisible': Eval('state') != 'draft',
                    },
                'do': {
                    'invisible': Eval('state') != 'confirmed',
                    },
                })

    @fields.depends('certificate', '_parent_certificate.poa')
    def on_change_with_poa(self, name=None):
        if self.certificate:
            return self.certificate.poa.id

    @classmethod
    def search_poa(cls, name, clause):
        return [('certificate.poa' + clause[0].lstrip(name),) +
            tuple(clause[1:])]

    @classmethod
    def get_amount(cls, liquidations, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        result = {}
        liq_amounts = defaultdict(lambda: 0)

        Card = pool.get('public.budget.liquidation.card')

        ids = [l.id for l in liquidations]
        for sub_ids in grouped_slice(ids):
            query_card = Card.table_query()
            red_sql = reduce_ids(query_card.id, sub_ids)
            query = query_card.select(
                query_card.id,
                query_card.liquidation_amount,
                where=red_sql
            )
            cursor.execute(*query)
            result = cursor.fetchall()
            liq_amounts.update(dict(result))

        return {
            'amount': liq_amounts,
        }

    @classmethod
    def check_availability(cls, liquidations):
        for liquidation in liquidations:
            for line in liquidation.lines:
                if line.amount > line.certificate_line.available_amount:
                    cls.raise_user_error('unavailable', {
                        'budget': line.certificate_line.budget.rec_name,
                        'available': line.certificate_line.available_amount,
                        })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, liquidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, liquidations):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    @set_employee('confirmed_by')
    def confirm(cls, liquidations):
        pool = Pool()
        Date = pool.get('ir.date')
        cls.check_availability(liquidations)
        for liquidation in liquidations:
            liquidation.confirmed_date = Date.today()
        cls.save(liquidations)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    def do(cls, liquidations):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Date = pool.get('ir.date')
        POAConfiguration = pool.get('public.poa.configuration')
        cls.check_availability(liquidations)
        for liquidation in liquidations:
            liquidation.date = Date.today()
            if liquidation.number:
                continue
            configuration = POAConfiguration.search([
                ('poa', '=', liquidation.poa)]
            )
            if configuration and configuration[0].liquidation_sequence:
                liquidation.number = Sequence.get_id(
                    configuration[0].liquidation_sequence)
            else:
                cls.raise_user_error('without_liquidation_sequence')
        cls.save(liquidations)


class PublicBudgetLiquidationLine(sequence_ordered(), ModelView, ModelSQL):
    'Public Budget Liquidation Line'
    __name__ = 'public.budget.liquidation.line'
    _history = True
    liquidation = fields.Many2One('public.budget.liquidation', 'Liquidation',
        ondelete='CASCADE', select=True, required=True,
        states={
            'readonly': ((Eval('liquidation_state') != 'draft') &
                Bool(Eval('liquidation'))),
            },
        depends=['liquidation_state'])
    liquidation_state = fields.Function(
        fields.Selection(STATES, 'Liquidation State'),
        'on_change_with_liquidation_state')
    certificate_lines = fields.Function(
        fields.One2Many('public.budget.certificate.line', None,
        'Certificate lines'), 'on_change_with_certificate_lines')
    type = fields.Selection([
        ('line', 'Line'),
        ('comment', 'Comment'),
        ], 'Type', select=True, required=True,
        states={
            'readonly': Eval('liquidation_state') != 'draft',
            },
        depends=['liquidation_state'])
    certificate_line = fields.Many2One(
        'public.budget.certificate.line', 'Certificate Line',
        states={
            'readonly': Eval('liquidation_state') != 'draft',
            'invisible': ~Eval('type').in_(['line']),
            'required': Eval('type').in_(['line']),
            },
        domain=[
            ('id', 'in', Eval('certificate_lines'))
        ], depends=['compromise_state', 'type', 'certificate_lines'])
    available_line = fields.Function(
        fields.Numeric('Line available',
            states={'invisible': False}), 'on_change_with_available_line')
    amount = fields.Numeric('Amount',
            digits=(16,
                Eval('_parent_liquidation', {}).get('currency_digits', 2)),
            states={
                'invisible': ~Eval('type').in_(['line']),
                'required': Eval('type').in_(['line']),
                },
            domain=[
                ('amount', '>', 0),
                ('amount', '<=', Eval('available_line'))
            ], depends=['type', 'available_line'])
    notes = fields.Text('Notes',
        states={
            'readonly': Eval('liquidation_state') != 'draft',
            },
        depends=['liquidation_state'])

    @staticmethod
    def default_type():
        return 'line'

    @staticmethod
    def default_amount():
        return 0

    @fields.depends('liquidation', '_parent_liquidation.state')
    def on_change_with_liquidation_state(self, name=None):
        if self.liquidation:
            return self.liquidation.state

    @fields.depends('liquidation', '_parent_liquidation.certificate')
    def on_change_with_certificate_lines(self, name=None):
        if self.liquidation:
            if self.liquidation.certificate.lines:
                return [line.id for line in self.liquidation.certificate.lines]
        return None

    @fields.depends('certificate_line')
    def on_change_with_available_line(self, name=None):
        if self.certificate_line:
            return self.certificate_line.available_amount
        return 0
