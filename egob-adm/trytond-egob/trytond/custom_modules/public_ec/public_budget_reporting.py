from sql import Union, Literal, Null
from sql.conditionals import Coalesce, Case

from trytond.pool import Pool
from trytond.model import ModelSQL, fields
from trytond.transaction import Transaction


__all__ = ['ReformQuery', 'CertificateQuery', 'CompromiseQuery',
    'LiquidationQuery', 'BudgetAccountQuery']


def get_main_columns(table):
    return [table.id,
        table.create_uid,
        table.create_date,
        table.write_uid,
        table.write_date]


class BaseQuery(ModelSQL):
    'Base Query, based on budget as main concept'
    company = fields.Many2One('company.company', "Company")
    budget = fields.Many2One('public.budget', 'Budget')
    planning_unit = fields.Many2One('public.planning.unit', 'Planning Unit')

    @classmethod
    def table_query(cls):
        from_item, tables = cls._joins()
        return from_item.select(*cls._columns(tables),
            where=cls._where(tables),
            group_by=cls._group_by(tables))

    @classmethod
    def _joins(cls):
        pool = Pool()
        Company = pool.get('company.company')
        Budget = pool.get('public.budget')
        PlanningUnit = pool.get('public.planning.unit')

        tables = {}
        tables['budget'] = budget = Budget.__table__()
        tables['budget.ppu'] = ppu = PlanningUnit.__table__()
        tables['budget.company'] = company = Company.__table__()

        from_item = (budget
            .join(company, condition=budget.company == company.id)
            .join(ppu, condition=budget.planning_unit == ppu.id))
        return from_item, tables

    @classmethod
    def _columns(cls, tables):
        budget = tables['budget']
        main_columns = get_main_columns(budget)
        return main_columns + [
            budget.company.as_('company'),
            budget.id.as_('budget'),
            budget.planning_unit.as_('planning_unit')]

    @classmethod
    def _column_id(cls, tables):
        budget = tables['budget']
        return budget.id

    @classmethod
    def _group_by(cls, tables):
        return []

    @classmethod
    def _where(cls, tables):
        pool = Pool()
        context = Transaction().context
        budget = tables['budget']

        Budget = pool.get('public.budget')
        PlanningUnit = pool.get('public.planning.unit')

        where = budget.company == context.get('company')

        planning_unit_filter = None
        budget_filter = None

        if context.get('poa'):
            planning_unit_filter = context.get('poa')
        if context.get('program'):
            planning_unit_filter = context.get('program')
        if context.get('project'):
            planning_unit_filter = context.get('project')

        if planning_unit_filter:
            planning_query = PlanningUnit.search(
                [('parent', 'child_of', planning_unit_filter)], query=True)

            where &= budget.planning_unit.in_(planning_query)

        if context.get('budget'):
            budget_filter = context.get('budget')
            budget_query = Budget.search([
                ('parent', 'child_of', budget_filter)
            ], query=True)
            where &= budget.id.in_(budget_query)

        return where


class ReformQuery(ModelSQL):
    'Reform Query, id -> reform_line'
    __name__ = 'public.budget.reform.query'

    sequence = fields.Integer('Sequence')
    reform = fields.Many2One('public.budget.reform', 'Reform')
    budget = fields.Many2One('public.budget', 'Budget')
    amount = fields.Numeric('Certified amount', digits=(16, 2))
    date = fields.Date('Certified date')

    @classmethod
    def __setup__(cls):
        super(ReformQuery, cls).__setup__()
        cls._order.insert(0, ('reform', 'ASC'))
        cls._order.insert(1, ('sequence', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return reform line query, depending of the context:
        context params: budget, date_start, date_end
        '''
        pool = Pool()
        context = Transaction().context
        Reform = pool.get('public.budget.reform')
        ReformLine = pool.get('public.budget.reform.line')

        reform = Reform.__table__()
        reform_line = ReformLine.__table__()

        where = (reform.state == 'done')

        if context.get('start_date'):
            where &= (reform.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (reform.date <= context.get('end_date'))
        where &= (reform_line.type == 'line')
        query_positive = reform.join(reform_line,
            condition=reform.id == reform_line.reform
        ).select(
            *get_main_columns(reform_line) + [
                reform_line.reform,
                reform_line.sequence,
                reform_line.to_budget.as_('budget'),
                Coalesce(reform_line.amount, 0).as_('amount'),
                reform.date],
            where=where & (reform_line.to_budget != None))
        query_negative = reform.join(reform_line,
            condition=reform.id == reform_line.reform
        ).select(
            *get_main_columns(reform_line) + [
                reform_line.reform,
                reform_line.sequence,
                reform_line.from_budget.as_('budget'),
                (Coalesce(reform_line.amount, 0) * -1).as_('amount'),
                reform.date],
            where=where & (reform_line.from_budget != None))
        query = Union(query_positive, query_negative)
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class CertificateQuery(ModelSQL):
    'Certificate Query, id -> certificate_line'
    __name__ = 'public.budget.certificate.query'

    sequence = fields.Integer('Sequence')
    certificate = fields.Many2One('public.budget.certificate', 'Certificate')
    budget = fields.Many2One('public.budget', 'Budget')
    cost_center = fields.Many2One('public.cost.center', 'Cost Center')
    amount = fields.Numeric('Certified amount', digits=(16, 2))
    date = fields.Date('Certified date')

    @classmethod
    def __setup__(cls):
        super(CertificateQuery, cls).__setup__()
        cls._order.insert(0, ('certificate', 'ASC'))
        cls._order.insert(1, ('sequence', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return certificate line query, depending of the context:
        context params: budget, date_start, date_end
        '''
        pool = Pool()
        context = Transaction().context
        Certificate = pool.get('public.budget.certificate')
        CertificateLine = pool.get('public.budget.certificate.line')

        certificate = Certificate.__table__()
        certificate_line = CertificateLine.__table__()

        where = (certificate.state == 'done')

        if context.get('by_cost_center'):
            cost_center_column = certificate_line.cost_center
        else:
            cost_center_column = Literal(Null).as_('cost_center')

        if context.get('start_date'):
            where &= (certificate.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (certificate.date <= context.get('end_date'))
        where &= (certificate_line.type == 'line')
        query = certificate.join(certificate_line,
            condition=certificate.id == certificate_line.certificate
        ).select(
            *get_main_columns(certificate_line) + [
                certificate_line.certificate,
                certificate_line.sequence,
                certificate_line.budget,
                cost_center_column,
                Coalesce(certificate_line.amount, 0).as_('amount'),
                certificate.date],
            where=where)

        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class CompromiseQuery(ModelSQL):
    'Compromise Query, id -> compromise_line'
    __name__ = 'public.budget.compromise.query'

    sequence = fields.Integer('Sequence')
    compromise = fields.Many2One('public.budget.compromise', 'Compromise')
    certificate_line = fields.Many2One(
        'public.budget.certificate.line', 'Certificate line')
    budget = fields.Many2One('public.budget', 'Budget')
    cost_center = fields.Many2One('public.cost.center', 'Cost Center')
    amount = fields.Numeric('Commited amount', digits=(16, 2))
    date = fields.Numeric('Commited date')

    @classmethod
    def __setup__(cls):
        super(CompromiseQuery, cls).__setup__()
        cls._order.insert(0, ('compromise', 'ASC'))
        cls._order.insert(1, ('sequence', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return compromise line query, depending of the context:
        context params: date_start, date_end
        '''
        pool = Pool()
        context = Transaction().context
        Compromise = pool.get('public.budget.compromise')
        CompromiseLine = pool.get('public.budget.compromise.line')
        CertificateLine = pool.get('public.budget.certificate.line')
        certificate_line = CertificateLine.__table__()
        compromise = Compromise.__table__()
        compromise_line = CompromiseLine.__table__()

        where = (compromise.state == 'done')
        if context.get('start_date'):
            where &= (compromise.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (compromise.date <= context.get('end_date'))
        where &= (compromise_line.type == 'line')
        query = compromise.join(compromise_line,
            condition=compromise.id == compromise_line.compromise
        ).join(certificate_line,
            condition=certificate_line.id == compromise_line.certificate_line
        ).select(
            *get_main_columns(compromise_line) + [
                compromise_line.compromise,
                compromise_line.sequence,
                compromise_line.certificate_line,
                certificate_line.budget,
                certificate_line.cost_center,
                Coalesce(compromise_line.amount, 0).as_('amount'),
                compromise.date],
            where=where)

        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class LiquidationQuery(ModelSQL):
    'Liquidation Query, id -> liquidation_line'
    __name__ = 'public.budget.liquidation.query'

    sequence = fields.Integer('Sequence')
    liquidation = fields.Many2One('public.budget.liquidation', 'Liquidation')
    certificate_line = fields.Many2One(
        'public.budget.certificate.line', 'Certificate line')
    budget = fields.Many2One('public.budget', 'Budget')
    cost_center = fields.Many2One('public.cost.center', 'Cost Center')
    amount = fields.Numeric('Liquidation amount', digits=(16, 2))
    date = fields.Numeric('Commited date')

    @classmethod
    def __setup__(cls):
        super(LiquidationQuery, cls).__setup__()
        cls._order.insert(0, ('liquidation', 'ASC'))
        cls._order.insert(1, ('sequence', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return liquidation line query, depending of the context:
        context params: date_start, date_end
        '''
        pool = Pool()
        context = Transaction().context
        Liquidation = pool.get('public.budget.liquidation')
        LiquidationLine = pool.get('public.budget.liquidation.line')
        CertificateLine = pool.get('public.budget.certificate.line')
        certificate_line = CertificateLine.__table__()
        liquidation = Liquidation.__table__()
        liquidation_line = LiquidationLine.__table__()

        where = (liquidation.state == 'done')
        if context.get('start_date'):
            where &= (liquidation.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (liquidation.date <= context.get('end_date'))
        where &= (liquidation_line.type == 'line')
        query = liquidation.join(liquidation_line,
            condition=liquidation.id == liquidation_line.liquidation
        ).join(certificate_line,
            condition=certificate_line.id == liquidation_line.certificate_line
        ).select(
            *get_main_columns(liquidation_line) + [
                liquidation_line.liquidation,
                liquidation_line.sequence,
                liquidation_line.certificate_line,
                certificate_line.budget,
                certificate_line.cost_center,
                Coalesce(liquidation_line.amount, 0).as_('amount'),
                liquidation.date],
            where=where)

        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class BudgetAccountQuery(ModelSQL):
    'Budget Account Query'
    __name__ = 'public.budget.account.query'

    move = fields.Many2One('account.move', 'Account Move', readonly=True)
    budget = fields.Many2One('public.budget', 'Budget', readonly=True)
    planning_unit = fields.Many2One(
        'public.planning.unit', 'Planning Unit', readonly=True)
    account = fields.Many2One('account.account', 'Account', readonly=True)
    accrued = fields.Numeric('Accrued', digits=(16, 2), readonly=True)
    executed = fields.Numeric('Executed', digits=(16, 2), readonly=True)
    certificate = fields.Many2One(
        'public.budget.certificate', 'Certificate', readonly=True)
    compromise = fields.Many2One(
        'public.budget.compromise', 'Compromise', readonly=True)
    date = fields.Date('Date', readonly=True)

    @classmethod
    def query_get(cls):
        '''
        Return accrued, executed query, depending on the context:
        context params: date_start, date_end
        '''
        pool = Pool()
        context = Transaction().context
        Account = pool.get('account.account')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Budget = pool.get('public.budget')
        Certificate = pool.get('public.budget.certificate')
        Compromise = pool.get('public.budget.compromise')

        account = Account.__table__()
        move = Move.__table__()
        mline = MoveLine.__table__()
        budget = Budget.__table__()
        certificate = Certificate.__table__()
        compromise = Compromise.__table__()

        where = (move.state == 'posted')
        if context.get('start_date'):
            where &= (mline.date >= context.get('start_date'))
        if context.get('end_date'):
            where &= (mline.date <= context.get('end_date'))

        balance = mline.debit - mline.credit
        query = budget.join(mline,
            condition=budget.id == mline.budget
        ).join(account,
            condition=account.id == mline.account,
        ).join(move,
            condition=move.id == mline.move
        ).join(compromise,
            condition=compromise.id == move.compromise
        ).join(certificate,
            condition=certificate.id == compromise.certificate
        ).select(
            mline.move,
            mline.budget,
            budget.planning_unit,
            mline.account,
            Coalesce(Case(
                ((account.kind == 'receivable') & (balance > 0), mline.debit),
                ((account.kind == 'payable') & ( balance < 0), mline.credit),
                 else_=0), 0).as_('accrued'),
            Coalesce(Case(
                ((account.kind == 'receivable') & (balance < 0), mline.credit),
                ((account.kind == 'payable') & ( balance > 0), mline.debit),
                 else_=0), 0).as_('executed'),
            move.compromise,
            compromise.certificate,
            move.date,
            where=where
        )
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()
