from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.transaction import Transaction

from public_planning import PPU_TYPES

__all__ = ['PublicPlanningUnitCard']


class PublicPlanningUnitCard(ModelSQL, ModelView):
    "Public Planning Unit Card"
    __name__ = 'public.planning.unit.card'
    company = fields.Many2One('company.company', 'Company', readonly=True)
    code = fields.Char('Code', readonly=True)
    name = fields.Char('Name', readonly=True)
    type = fields.Selection(PPU_TYPES, 'Type', readonly=True)
    parent = fields.Many2One('public.planning.unit.card', 'Parent')
    initial_amount = fields.Numeric(
        "Initial Amount", digits=(16, 2), readonly=True)
    codified_amount = fields.Numeric(
        "Codified Amount", digits=(16, 2), readonly=True)
    certificate_amount = fields.Numeric(
        'Certified', digits=(16, 2), readonly=True)
    compromise_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)

    @classmethod
    def __setup__(cls):
        super(PublicPlanningUnitCard, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return planning query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        PlanningUnit = pool.get('public.planning.unit')
        BudgetCard = pool.get('public.budget.card')
        query_card = BudgetCard.query_get()
        table_a = PlanningUnit.__table__()
        table_c = PlanningUnit.__table__()

        context = Transaction().context
        planning_unit_filter = None

        if context.get('poa'):
            planning_unit_filter = context.get('poa')
        if context.get('program'):
            planning_unit_filter = context.get('program')
        if context.get('project'):
            planning_unit_filter = context.get('project')

        if planning_unit_filter:
            planning_query = PlanningUnit.search(
                [('parent', 'child_of', planning_unit_filter)], query=True)
        else:
            return
        columns = [
            table_a.id,
            table_a.create_date,
            table_a.create_uid,
            table_a.write_date,
            table_a.write_uid,
            table_a.company,
            table_a.code,
            table_a.name,
            table_a.type,
            table_a.parent,
        ]
        query = table_a.join(table_c,
            condition=(table_c.left >= table_a.left) &
            (table_c.right <= table_a.right)
        ).join(query_card, type_='LEFT',
            condition=table_c.id == query_card.planning_unit
        ).select(
            *columns + [
                Sum(Coalesce(
                    query_card.initial_amount, 0)).as_('initial_amount'),
                Sum(Coalesce(
                    query_card.codified_amount, 0)).as_('codified_amount'),
                Sum(Coalesce(query_card.certificate_amount, 0)
                    ).as_('certificate_amount'),
                Sum(Coalesce(query_card.compromise_amount, 0)
                    ).as_('compromise_amount')],
            where=table_a.id.in_(planning_query),
            group_by=columns
        )
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()
