#!/usr/bin/env python

import sys
import csv

sys.stdout.write(u'<?xml version="1.0"?>\n')
sys.stdout.write(u'<tryton>\n')
sys.stdout.write(u'    <data>\n')

with open('public_budget_template.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    plan = ''
    for row in reader:
        length = 1 if len(row['code']) <= 2 else len(row['code']) - 2
        parent = '%s<field name="parent" ref="public_budget_template_%s"/>' % \
            ('\n            ', row['code'][:length]) \
            if len(row['code']) > 1 else \
            '%s<field name="parent" ref="public_budget_template_%s"/>' % \
            ('\n            ',plan)
        if str(row['code'])[:1] == 'P':
            parent = ''
            plan = row['code']
        record = u'''
        <record model="public.budget.template" id="public_budget_template_%s">
            <field name="code">%s</field>
            <field name="name">%s</field>
            <field name="description">%s</field>%s
        </record>\n''' % (row['code'],
                          row['code'],
                          row['name'].decode('utf-8'),
                          row['description'].decode('utf-8'),
                          parent
                          )
        sys.stdout.write(record.encode('utf-8'))
sys.stdout.write(u'    </data>\n')
sys.stdout.write(u'</tryton>\n')
