#!/usr/bin/env python

import sys
import csv

sys.stdout.write(u'<?xml version="1.0"?>\n')
sys.stdout.write(u'<tryton>\n')
sys.stdout.write(u'    <data>\n')

public_ndp_policy_type = {}
with open('public_ndp_policy_type.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    for i, row in enumerate(reader):
        record = u'''
        <record model="public.ndp.policy.type" id="public_ndp_policy_type_%s">
            <field name="name">%s</field>
        </record>\n''' % (str(i+1), row['policy_type'].decode('utf-8'))
        sys.stdout.write(record.encode('utf-8'))
        public_ndp_policy_type[row['policy_type'].decode('utf-8')] = \
            "public_ndp_policy_type_%s" % (str(i+1))



with open('public_ndp_policy.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    for i, row in enumerate(reader):
        parent = '%s<field name="ndp_policy_type" ref="%s"/>' % \
                 (
                     '\n            ',
                     public_ndp_policy_type
                     [
                         row['policy_type'].decode('utf-8')
                     ]) \
            if row['policy_type'] \
            else ''
        record = u'''
        <record model="public.ndp.policy" id="public_ndp_policy_%s">
            <field name="code">%s</field>
            <field name="name">%s</field>%s
        </record>\n''' % \
                 (
                     str(i+1),
                     row['code'].decode('utf-8'),
                     row['public_policy'].decode('utf-8'),
                     parent
                 )
        sys.stdout.write(record.encode('utf-8'))
sys.stdout.write(u'    </data>\n')
sys.stdout.write(u'</tryton>\n')
