#!/usr/bin/env python

import sys
import csv

sys.stdout.write(u'<?xml version="1.0"?>\n')
sys.stdout.write(u'<tryton>\n')
sys.stdout.write(u'    <data>\n')

with open('public_strategic_component.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile)
    for i, row in enumerate(reader):
        record = u'''
        <record model="public.strategic.component" id="public_strategic_component_%s">
            <field name="name">%s</field>
        </record>\n''' % (str(i+1), row['component'].decode('utf-8'))
        sys.stdout.write(record.encode('utf-8'))
sys.stdout.write(u'    </data>\n')
sys.stdout.write(u'</tryton>\n')
