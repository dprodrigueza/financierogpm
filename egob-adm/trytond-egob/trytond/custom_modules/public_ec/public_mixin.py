from trytond.model import fields
from trytond.pool import Pool
from trytond.pyson import Eval, If
from trytond.transaction import Transaction


__all__ = ['PublicGenericMixin', 'PublicContextMixin']


class PublicGenericMixin(object):
    name = fields.Char('Name', required=True)
    description = fields.Text('Description')


class PublicContextMixin(object):
    poa = fields.Many2One('public.planning.unit', 'POA', required=True,
        domain=[
            ('company', '=', Eval('company')),
            ('type', '=', 'poa')
        ], depends=['company'])
    poa_start_date = fields.Function(
        fields.Date('POA Start Date'), 'on_change_with_poa_start_date')
    poa_end_date = fields.Function(
        fields.Date('POA End Date'), 'on_change_with_poa_end_date')
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date("Start Date",
        domain=[
            If(Eval('end_date') & Eval('poa_start_date'), [
                ('start_date', '<=', Eval('end_date')),
                ('start_date', '>=', Eval('poa_start_date'))],
                [()]),
            ],
        depends=['end_date', 'poa_start_date'])
    end_date = fields.Date("To Date",
        domain=[
            If(Eval('start_date') & Eval('poa_end_date'),
               [('end_date', '>=', Eval('start_date')),
                ('end_date', '<=', Eval('poa_end_date'))],
               [()]),
            ],
        depends=['start_date', 'poa_end_date'])

    @classmethod
    def default_poa(cls):
        pool = Pool()
        Poa = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        date = Date.today()
        poas = Poa.search([
            ('type', '=', 'poa'),
            ('start_date', '<=', date),
            ('end_date', '>=', date)]
        )
        if not poas:
            poas = Poa.search([('state', '=', 'open')])
            if not poas:
                return None
        return poas[0].id

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @fields.depends('poa')
    def on_change_with_poa_start_date(self, name=None):
        if self.poa:
            return self.poa.start_date

    @fields.depends('poa')
    def on_change_with_poa_end_date(self, name=None):
        if self.poa:
            return self.poa.end_date

    @fields.depends('poa')
    def on_change_poa(self):
        if self.poa:
            self.start_date = self.poa.start_date
            self.end_date = self.poa.end_date
        else:
            self.start_date = None
            self.end_date = None
