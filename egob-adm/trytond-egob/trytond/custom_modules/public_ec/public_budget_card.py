from sql import Column
from sql.aggregate import Max, Sum
from sql.conditionals import Coalesce

from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.tools import reduce_ids

from public_budget_reporting import BaseQuery
from .public_budget import STATES


__all__ = ['PublicBudgetCertificateCard', 'PublicBudgetCompromiseCard',
    'PublicBudgetLiquidationCard', 'PublicBudgetCard', 'PublicBudgetCostCard']


class PublicBudgetCertificateCard(ModelSQL, ModelView):
    'Public Budget Certificate Card'
    __name__ = 'public.budget.certificate.card'

    poa = fields.Many2One('public.planning.unit', 'POA', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    number = fields.Char('Number', readonly=True)
    state = fields.Selection(STATES, 'State', readonly=True)
    confirmed_date = fields.Date('Confirmed Date', readonly=True)
    date = fields.Date('Done Date', readonly=True)
    certificate_amount = fields.Numeric(
        'Certified', digits=(16, 2), readonly=True)
    compromise_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)
    liquidation_amount = fields.Numeric(
        'Liquidation', digits=(16, 2), readonly=True)
    available_amount = fields.Numeric(
        'Available', digits=(16, 2), readonly=True)

    @classmethod
    def query_get(cls):
        '''
        Return certificate query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        certificate = Certificate.__table__()

        CertificateQuery = pool.get('public.budget.certificate.query')
        CompromiseQuery = pool.get('public.budget.compromise.query')
        LiquidationQuery = pool.get('public.budget.liquidation.query')

        base_query = BaseQuery.table_query()
        cert_query = CertificateQuery.query_get()
        comp_query = CompromiseQuery.query_get()
        liq_query = LiquidationQuery.query_get()

        columns = [certificate.id,
            certificate.create_uid,
            certificate.create_date,
            certificate.write_uid,
            certificate.write_date,
            certificate.poa,
            certificate.company,
            certificate.number,
            certificate.state,
            certificate.confirmed_date,
            certificate.date]

        query_temp = base_query.join(cert_query,
            condition=base_query.budget == cert_query.budget
        ).join(comp_query, type_='LEFT',
            condition=cert_query.id == comp_query.certificate_line
        ).join(liq_query, type_='LEFT',
            condition=cert_query.id == liq_query.certificate_line
        ).join(certificate, type_='LEFT',
            condition=certificate.id == cert_query.certificate
        ).select(*columns + [
            Coalesce(Sum(cert_query.amount), 0).as_('certificate_amount'),
            Coalesce(Sum(comp_query.amount), 0).as_('compromise_amount'),
            Coalesce(Sum(liq_query.amount), 0).as_('liquidation_amount')],
            group_by=columns
        )

        rfields = [str(c).replace('"', '') for c in query_temp._columns]
        result_columns = [Column(query_temp, c).as_(c) for c in rfields] + [
            Coalesce(query_temp.certificate_amount -
                query_temp.compromise_amount - query_temp.liquidation_amount, 0
            ).as_('available_amount')
        ]
        query = query_temp.select(*result_columns)
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class PublicBudgetCompromiseCard(ModelSQL, ModelView):
    'Public Budget Compromise Card'
    __name__ = 'public.budget.compromise.card'

    poa = fields.Many2One('public.planning.unit', 'POA', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    number = fields.Char('Number', readonly=True)
    state = fields.Selection(STATES, 'State', readonly=True)
    confirmed_date = fields.Date('Confirmed Date', readonly=True)
    date = fields.Date('Done Date', readonly=True)
    compromise_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)
    accrued_amount = fields.Numeric(
        'Accrued', digits=(16, 2), readonly=True)
    executed_amount = fields.Numeric(
        'Executed', digits=(16, 2), readonly=True)
    certificate = fields.Many2One(
        'public.budget.certificate', 'Certificate', readonly=True)

    @classmethod
    def query_get(cls):
        '''
        Return compromise query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        certificate = Certificate.__table__()
        Compromise = pool.get('public.budget.compromise')
        compromise = Compromise.__table__()

        CompromiseQuery = pool.get('public.budget.compromise.query')
        BudgetAccountQuery = pool.get('public.budget.account.query')

        base_query = BaseQuery.table_query()
        comp_query = CompromiseQuery.query_get()
        budget_account = BudgetAccountQuery.query_get()

        columns = [compromise.id,
            compromise.create_uid,
            compromise.create_date,
            compromise.write_uid,
            compromise.write_date,
            compromise.certificate,
            certificate.poa,
            compromise.company,
            compromise.number,
            compromise.state,
            compromise.confirmed_date,
            compromise.date]

        query = base_query.join(comp_query,
            condition=base_query.budget == comp_query.budget
        ).join(compromise, type_='LEFT',
            condition=compromise.id == comp_query.compromise
        ).join(budget_account,
            condition=compromise.id == budget_account.compromise
        ).join(certificate, type_='LEFT',
            condition=certificate.id == compromise.certificate
        ).select(*columns + [
            Coalesce(Sum(comp_query.amount), 0).as_('compromise_amount'),
            Coalesce(Sum(budget_account.accrued), 0).as_('accrued_amount'),
            Coalesce(Sum(budget_account.executed), 0).as_('executed_amount'),
        ],
            group_by=columns
        )

        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class PublicBudgetLiquidationCard(ModelSQL, ModelView):
    'Public Budget Liquidation Card'
    __name__ = 'public.budget.liquidation.card'

    poa = fields.Many2One('public.planning.unit', 'POA', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    number = fields.Char('Number', readonly=True)
    state = fields.Selection(STATES, 'State', readonly=True)
    confirmed_date = fields.Date('Confirmed Date', readonly=True)
    date = fields.Date('Done Date', readonly=True)
    liquidation_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)
    certificate = fields.Many2One(
        'public.budget.certificate', 'Certificate', readonly=True)

    @classmethod
    def query_get(cls):
        '''
        Return liquidation query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        Certificate = pool.get('public.budget.certificate')
        certificate = Certificate.__table__()
        Liquidation = pool.get('public.budget.liquidation')
        liquidation = Liquidation.__table__()

        LiquidationQuery = pool.get('public.budget.liquidation.query')

        base_query = BaseQuery.table_query()
        liq_query = LiquidationQuery.query_get()

        columns = [liquidation.id,
            liquidation.create_uid,
            liquidation.create_date,
            liquidation.write_uid,
            liquidation.write_date,
            liquidation.certificate,
            certificate.poa,
            liquidation.company,
            liquidation.number,
            liquidation.state,
            liquidation.confirmed_date,
            liquidation.date]

        query = base_query.join(liq_query,
            condition=base_query.budget == liq_query.budget
        ).join(liquidation, type_='LEFT',
            condition=liquidation.id == liq_query.liquidation
        ).join(certificate, type_='LEFT',
            condition=certificate.id == liquidation.certificate
        ).select(*columns + [
            Coalesce(Sum(liq_query.amount), 0).as_('liquidation_amount')],
            group_by=columns
        )

        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class PublicBudgetCard(ModelSQL, ModelView):
    'Public Budget Card'
    __name__ = 'public.budget.card'

    company = fields.Many2One('company.company', 'Company', readonly=True)
    name = fields.Char('Name', readonly=True)
    code = fields.Char('Code', readonly=True)
    kind = fields.Selection([
        ('view', 'View'),
        ('other', 'Other'),
        ], "Kind", readonly=True)
    level = fields.Integer('Level', readonly=True)
    planning_unit = fields.Many2One(
        'public.planning.unit', "Planning Unit", readonly=True)
    initial_amount = fields.Numeric(
        "Initial Amount", digits=(16, 2), readonly=True)
    reform_amount = fields.Numeric(
        "Reform Amount", digits=(16, 2), readonly=True)
    codified_amount = fields.Numeric(
        "Codified Amount", digits=(16, 2), readonly=True)
    certificate_amount = fields.Numeric(
        'Certified', digits=(16, 2), readonly=True)
    compromise_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)
    available_amount = fields.Numeric(
        'Available', digits=(16, 2), readonly=True)

    @classmethod
    def __setup__(cls):
        super(PublicBudgetCard, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return budget query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        Budget = pool.get('public.budget')
        BudgetInitial = pool.get('public.budget.initial')
        table_a = Budget.__table__()
        table_c = Budget.__table__()
        budget = Budget.__table__()
        initial = BudgetInitial.__table__()

        ReformQuery = pool.get('public.budget.reform.query')
        CertificateQuery = pool.get('public.budget.certificate.query')
        CompromiseQuery = pool.get('public.budget.compromise.query')
        LiquidationQuery = pool.get('public.budget.liquidation.query')

        base_query = BaseQuery.table_query()
        reform_query = ReformQuery.query_get()
        reform_budget = reform_query.select(
            reform_query.budget,
            Sum(Coalesce(reform_query.amount, 0)).as_('amount'),
            group_by=[reform_query.budget]
        )
        cert_query = CertificateQuery.query_get()
        comp_query = CompromiseQuery.query_get()
        liq_query = LiquidationQuery.query_get()

        select_columns = [cert_query.budget, cert_query.cost_center]

        cert_budget = cert_query.join(comp_query, type_='LEFT',
            condition=cert_query.id == comp_query.certificate_line
        ).join(liq_query, type_='LEFT',
            condition=cert_query.id == liq_query.certificate_line
        ).select(
            *select_columns + [
                Sum(Coalesce(cert_query.amount, 0)).as_('certificate_amount'),
                Sum(Coalesce(comp_query.amount, 0)).as_('compromise_amount'),
                Sum(Coalesce(liq_query.amount, 0)).as_('liquidation_amount'),
            ],
            group_by=select_columns
        )

        query_temp = base_query.join(initial, type_="LEFT",
            condition=base_query.budget == initial.budget
        ).join(reform_budget, type_='LEFT',
            condition=base_query.budget == reform_budget.budget
        ).join(cert_budget, type_='LEFT',
            condition=base_query.budget == cert_budget.budget
        ).join(budget, type_='LEFT',
            condition=budget.id == base_query.budget
        ).select(budget.id,
                 budget.left,
                 budget.right,
                 cert_budget.cost_center,
            Coalesce(Max(initial.amount), 0).as_('initial_amount'),
            Coalesce(Max(reform_budget.amount), 0).as_('reform_amount'),
            (Coalesce(Sum(cert_budget.certificate_amount), 0) -
             Coalesce(Sum(cert_budget.liquidation_amount), 0)
            ).as_('certificate_amount'),
            Coalesce(Sum(cert_budget.compromise_amount), 0
            ).as_('compromise_amount'),
            group_by=[budget.id, budget.left, budget.right,
                cert_budget.cost_center]
        )

        rfields = [str(c).replace('"', '') for c in query_temp._columns]
        result_columns = [Column(query_temp, c).as_(c) for c in rfields] + [
            Coalesce(query_temp.initial_amount +
                query_temp.reform_amount, 0).as_('codified_amount'),
            Coalesce(query_temp.initial_amount + query_temp.reform_amount -
                query_temp.certificate_amount, 0).as_('available_amount')
        ]
        query_budget = query_temp.select(*result_columns)
        columns = [
            table_a.id,
            table_a.create_date,
            table_a.create_uid,
            table_a.write_date,
            table_a.write_uid,
            table_a.company,
            table_a.name,
            table_a.code,
            table_a.kind,
            table_a.level,
            table_a.planning_unit,
            query_budget.cost_center,
        ]
        query = table_a.join(table_c,
            condition=(table_c.left >= table_a.left) &
            (table_c.right <= table_a.right)
        ).join(query_budget, type_='LEFT',
            condition=table_c.id == query_budget.id
        ).select(
            *columns + [
                Sum(Coalesce(
                    query_budget.initial_amount, 0)).as_('initial_amount'),
                Sum(Coalesce(
                    query_budget.reform_amount, 0)).as_('reform_amount'),
                Sum(Coalesce(
                    query_budget.codified_amount, 0)).as_('codified_amount'),
                Sum(Coalesce(
                    query_budget.certificate_amount, 0)
                ).as_('certificate_amount'),
                Sum(Coalesce(
                    query_budget.compromise_amount, 0)
                ).as_('compromise_amount'),
                Sum(Coalesce(
                    query_budget.available_amount, 0)
                ).as_('available_amount')],
            group_by=columns
        )
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()


class PublicBudgetCostCard(ModelSQL, ModelView):
    'Public Budget Cost Card'
    __name__ = 'public.budget.cost.card'

    name = fields.Char('Name', readonly=True)
    code = fields.Char('Code', readonly=True)
    certificate_amount = fields.Numeric(
        'Certified', digits=(16, 2), readonly=True)
    compromise_amount = fields.Numeric(
        'Commited', digits=(16, 2), readonly=True)
    available_amount = fields.Numeric(
        'Available', digits=(16, 2), readonly=True)
    parent = fields.Many2One('public.budget.cost.card', 'Parent')
    children = fields.Function(
        fields.One2Many('public.budget.cost.card', None, 'Lines'),
        'on_change_with_children')

    @classmethod
    def __setup__(cls):
        super(PublicBudgetCostCard, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def query_get(cls):
        '''
        Return budget query, depending of the context:
        context params: poa, program, project, budget, date_start, date_end
        '''
        pool = Pool()
        Budget = pool.get('public.budget')
        table_a = Budget.__table__()
        table_c = Budget.__table__()
        budget = Budget.__table__()

        CertificateQuery = pool.get('public.budget.certificate.query')
        CompromiseQuery = pool.get('public.budget.compromise.query')
        LiquidationQuery = pool.get('public.budget.liquidation.query')

        CostCenter = pool.get('public.cost.center')
        cost_center = CostCenter.__table__()

        context = Transaction().context
        budget_ids = context.get('budget_ids')
        if not budget_ids:
            return None
        budget_red_sql = reduce_ids(table_a.id, budget_ids)

        base_query = BaseQuery.table_query()
        with Transaction().set_context(by_cost_center=True):
            cert_query = CertificateQuery.query_get()
        comp_query = CompromiseQuery.query_get()
        liq_query = LiquidationQuery.query_get()

        select_columns = [cert_query.budget, cert_query.cost_center]

        cert_budget = cert_query.join(comp_query, type_='LEFT',
            condition=cert_query.id == comp_query.certificate_line
        ).join(liq_query, type_='LEFT',
            condition=cert_query.id == liq_query.certificate_line
        ).select(
            *select_columns + [
                Sum(Coalesce(cert_query.amount, 0)).as_('certificate_amount'),
                Sum(Coalesce(comp_query.amount, 0)).as_('compromise_amount'),
                Sum(Coalesce(liq_query.amount, 0)).as_('liquidation_amount'),
            ],
            group_by=select_columns
        )

        query_temp = base_query.join(cert_budget, type_='LEFT',
            condition=base_query.budget == cert_budget.budget
        ).join(budget, type_='LEFT',
            condition=budget.id == base_query.budget
        ).select(budget.id,
                 budget.left,
                 budget.right,
                 cert_budget.cost_center,
            (Coalesce(Sum(cert_budget.certificate_amount), 0) +
             Coalesce(Sum(cert_budget.liquidation_amount), 0)
            ).as_('certificate_amount'),
            Coalesce(Sum(cert_budget.compromise_amount), 0
            ).as_('compromise_amount'),
            group_by=[budget.id, budget.left, budget.right,
                cert_budget.cost_center]
        )

        rfields = [str(c).replace('"', '') for c in query_temp._columns]
        result_columns = [Column(query_temp, c).as_(c) for c in rfields] + [
            Coalesce(query_temp.certificate_amount -
                query_temp.compromise_amount, 0).as_('available_amount'),
        ]
        query_budget = query_temp.select(*result_columns)

        columns = [
            cost_center.id,
        ]

        query_cost = table_a.join(table_c,
            condition=(table_c.left >= table_a.left) &
            (table_c.right <= table_a.right)
        ).join(query_budget, type_='LEFT',
            condition=table_c.id == query_budget.id
        ).join(cost_center,
            condition=cost_center.id == query_budget.cost_center
        ).select(
            *columns + [
                Sum(Coalesce(
                    query_budget.certificate_amount, 0)
                ).as_('certificate_amount'),
                Sum(Coalesce(
                    query_budget.compromise_amount, 0)
                ).as_('compromise_amount'),
                Sum(Coalesce(
                    query_budget.available_amount, 0)
                ).as_('available_amount')],
            where=budget_red_sql,
            group_by=columns
        )
        cost_a = CostCenter.__table__()
        cost_c = CostCenter.__table__()

        columns = [
            cost_a.id,
            cost_a.create_uid,
            cost_a.create_date,
            cost_a.write_uid,
            cost_a.write_date,
            cost_a.code,
            cost_a.name,
            cost_a.parent,
        ]

        query = cost_a.join(cost_c,
            condition=(cost_c.left >= cost_a.left) &
            (cost_c.right <= cost_a.right)
        ).join(query_cost, type_='LEFT',
            condition=cost_c.id == query_cost.id
        ).select(*columns + [
            Sum(Coalesce(
                query_cost.certificate_amount, 0)
            ).as_('certificate_amount'),
            Sum(Coalesce(
                query_cost.compromise_amount, 0)
            ).as_('compromise_amount'),
            Sum(Coalesce(
                query_cost.available_amount, 0)
            ).as_('available_amount')],
            group_by=columns
        )
        return query

    @classmethod
    def table_query(cls):
        return cls.query_get()

    def on_change_with_children(self, name=None):
        pool = Pool()
        CostCenter = pool.get('public.cost.center')
        cost_center = CostCenter(self.id)
        return [child.id for child in cost_center.children]
