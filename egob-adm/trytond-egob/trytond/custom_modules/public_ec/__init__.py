# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import public_budget
from . import public_planning
from . import project
from . import public_budget_card
from . import public_planning_card
from . import public_cost_center
from . import public_budget_reporting
from . import account
from . import user


def register():
    Pool.register(
        public_budget.PublicBudgetContext,
        public_budget.PublicBudget,
        public_budget.PublicBudgetInitial,
        public_budget.PublicBudgetReform,
        public_budget.PublicBudgetReformLine,
        public_budget.PublicBudgetCertificate,
        public_budget.PublicBudgetCertificateLine,
        public_budget.PublicBudgetCompromise,
        public_budget.PublicBudgetCompromiseLine,
        public_budget.PublicBudgetLiquidation,
        public_budget.PublicBudgetLiquidationLine,
        public_planning.PublicPlanningUnitContext,
        public_planning.PublicIndicator,
        public_planning.PublicPlanningUnit,
        public_planning.PublicPlanningUnitUser,
        public_planning.PublicPlanningUnitIndicator,
        public_planning.PublicPoaConfiguration,
        project.PublicStrategicComponent,
        project.PublicProjectType,
        project.PublicNDPTarget,
        project.PublicNDPPolicyType,
        project.PublicNDPPolicy,
        project.PublicRiskType,
        project.PublicRiskCategory,
        project.PublicRisk,
        project.Work,
        project.PublicProjectRisk,
        project.PublicProjectRiskMitigation,
        project.PublicProjectPolicies,
        project.PublicProjectCloseCondition,
        public_budget_card.PublicBudgetCertificateCard,
        public_budget_card.PublicBudgetCompromiseCard,
        public_budget_card.PublicBudgetLiquidationCard,
        public_budget_card.PublicBudgetCard,
        public_budget_card.PublicBudgetCostCard,
        public_planning_card.PublicPlanningUnitCard,
        public_cost_center.PublicCostCenter,
        public_budget_reporting.ReformQuery,
        public_budget_reporting.CertificateQuery,
        public_budget_reporting.CompromiseQuery,
        public_budget_reporting.LiquidationQuery,
        public_budget_reporting.BudgetAccountQuery,
        account.Account,
        account.Move,
        account.Line,
        user.User,
        module='public_ec', type_='model')
    Pool.register(
        module='public_ec', type_='wizard')
    Pool.register(
        module='public_ec', type_='report')
