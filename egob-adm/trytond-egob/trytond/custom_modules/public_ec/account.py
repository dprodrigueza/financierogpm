from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, If, Bool

__all__ = ['Account', 'Move', 'Line']


class Account:
    __metaclass__ = PoolMeta
    __name__ = 'account.account'

    def allowed_budget_childs(self, type_=None):
        pool = Pool()
        childs = []
        if type_ is not None:
            Budget = pool.get('public.budget')
            attribute = 'budget_%s' % (type_)
            budget = getattr(self, attribute)
            if budget:
                childs = Budget.search([
                    ('parent', 'child_of', budget),
                    ('kind', '=', 'other'),
                ])
        return [b.id for b in childs]


class Move:
    __metaclass__ = PoolMeta
    __name__ = 'account.move'
    _history = True

    type = fields.Selection([
        ('open', 'Open'),
        ('financial', 'Financial'),
        ('adjustment', 'Adjustment'),
        ('close', 'Close'),
        ], 'Type', select=True, required=True,
        states={
            'readonly': Eval('state') != 'draft',
            },
        depends=['state'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromise',
        states={
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('compromise_required')) == True,
            },
        depends=['state', 'compromise_required'],
        help='Required in order to use expense budgets')
    compromise_required = fields.Function(
        fields.Boolean('Compromise required'),
        'on_change_with_compromise_required')

    @fields.depends('lines')
    def on_change_with_compromise_required(self, name=None):
        if self.lines:
            for line in self.lines:
                if line.budget and line.budget.type == 'expense':
                    return True
        return False

    @classmethod
    def validate(cls, moves):
        super(Move, cls).validate(moves)
        for move in moves:
            for line in move.lines:
                line.check_budget()

class Line:
    __metaclass__ = PoolMeta
    __name__ = 'account.move.line'
    _history = True
    _states = {
        'readonly': Eval('move_state') == 'posted',
        }
    _depends = ['move_state']
    budget = fields.Many2One('public.budget', 'Budget',
        states={
            'readonly': _states['readonly'],
            'required': Bool(Eval('budget_required')),
        },
        domain=[
            If(Bool(Eval('budget_required')),
               ('id', 'in', Eval('budgets_allowed')),
               ('id', '=', None))
        ], depends=_depends + ['budget_required', 'budgets_allowed']
    )
    budget_required = fields.Function(fields.Boolean('Budget required',
        states={
            'invisible': True,
        }), 'on_change_with_budget_required')
    budgets_allowed = fields.Function(
        fields.Many2Many('public.budget', None, None, 'Budgets allowed',
            states={
                'invisible': True,
            }), 'on_change_with_budgets_allowed')

    @classmethod
    def __setup__(cls):
        super(Line, cls).__setup__()
        cls._error_messages.update({
                'error_budget_domain': ('Budget "%(budget)s" '
                    'is not in compromise "%(compromise)s".'),
                'without_compromise': ('Compromise is required'),
                })

    @fields.depends('account', 'debit', 'credit')
    def on_change_with_budget_required(self, name=None):
        if self.account:
            if (self.credit and self.account.budget_credit) or (
                    self.debit and self.account.budget_debit):
                return True
        return False

    @fields.depends('account', 'credit', 'debit')
    def on_change_with_budgets_allowed(self, name=None):
        childs = []
        if self.account:
            type_ = None
            if self.credit:
                type_ = 'credit'
            elif self.debit:
                type_ = 'debit'
            childs = self.account.allowed_budget_childs(type_)
        return childs

    @fields.depends('move', 'account', 'budgets_allowed')
    def on_change_credit(self):
        super(Line, self).on_change_credit()
        if self.account and self.credit:
            childs = self.account.allowed_budget_childs('credit')
            if childs and len(childs) == 1:
                self.budget = childs[0]

    @fields.depends('move', 'account', 'budgets_allowed')
    def on_change_debit(self):
        super(Line, self).on_change_debit()
        if self.account and self.debit:
            childs = self.account.allowed_budget_childs('debit')
            if childs and len(childs) == 1:
                self.budget = childs[0]

    @fields.depends('budget', 'move', '_parent_move.compromise')
    def check_budget(self):
        if self.budget and self.budget.type == 'expense':
            if self.move.compromise:
                if self.budget not in self.move.compromise.budgets:
                    self.raise_user_error('error_budget_domain', {
                        'budget': self.budget.rec_name,
                        'compromise': self.move.compromise.rec_name,
                        })
            else:
                self.raise_user_error('without_compromise')

    @fields.depends('move', '_parent_move.compromise', 'budget')
    def on_change_budget(self):
        if self.budget:
            self.check_budget()
