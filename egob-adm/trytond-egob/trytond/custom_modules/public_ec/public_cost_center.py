from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Eval

__all__ = ['PublicCostCenter']


class PublicCostCenter(ModelView, ModelSQL):
    'Cost Center'
    __name__ = 'public.cost.center'

    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    description = fields.Text('Description')
    parent = fields.Many2One('public.cost.center', 'Parent',
        left='left', right='right', ondelete='RESTRICT')
    left = fields.Integer('Left', required=True, select=True)
    right = fields.Integer('Right', required=True, select=True)
    children = fields.One2Many('public.cost.center', 'parent', 'Children',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'])

    @classmethod
    def __setup__(cls):
        super(PublicCostCenter, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('cost_center_name_uniq', Unique(t, t.name),
             'Name must be unique'),
            ('cost_center_code_uniq', Unique(t, t.code),
             'Code must be unique'),
        ]

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0

    @classmethod
    def validate(cls, centers):
        super(PublicCostCenter, cls).validate(centers)
        cls.check_recursion(centers)
