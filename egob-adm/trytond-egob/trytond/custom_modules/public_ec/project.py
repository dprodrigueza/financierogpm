from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pyson import Bool, Eval
from trytond.pool import PoolMeta

from public_mixin import PublicGenericMixin


__all__ = ['PublicStrategicComponent', 'PublicProjectType', 'PublicNDPTarget',
    'PublicNDPPolicyType', 'PublicNDPPolicy', 'PublicRiskType',
    'PublicRiskCategory', 'PublicRisk', 'Work', 'PublicProjectRisk',
    'PublicProjectRiskMitigation', 'PublicProjectPolicies',
    'PublicProjectCloseCondition']


class PublicStrategicComponent(PublicGenericMixin, ModelSQL, ModelView):
    """Public Strategic Component"""
    __name__ = 'public.strategic.component'


class PublicProjectType(PublicGenericMixin, ModelSQL, ModelView):
    """Public Project Type"""
    __name__ = 'public.project.type'


class PublicNDPTarget(PublicGenericMixin, ModelSQL, ModelView):
    """Public National Development Plan Target"""
    __name__ = 'public.ndp.target'


class PublicNDPPolicyType(PublicGenericMixin, ModelSQL, ModelView):
    """Public National Development Plan Policy Type"""
    __name__ = 'public.ndp.policy.type'


class PublicNDPPolicy(PublicGenericMixin, ModelSQL, ModelView):
    """Public National Development Plan Policy"""
    __name__ = 'public.ndp.policy'
    code = fields.Char('Code', size=None, select=True)
    ndp_policy_type = fields.Many2One('public.ndp.policy.type', 'Policy Type')


class PublicRiskType(PublicGenericMixin, ModelSQL, ModelView):
    """Public Risk Type"""
    __name__ = 'public.risk.type'


class PublicRiskCategory(PublicGenericMixin, ModelSQL, ModelView):
    """Public Risk Category"""
    __name__ = 'public.risk.category'


class PublicRisk(PublicGenericMixin, ModelSQL, ModelView):
    """Public Risk"""
    __name__ = 'public.risk'
    type_ = fields.Many2One('public.risk.type', 'Type', required=True)
    category = fields.Many2One(
        'public.risk.category', 'Category', required=True)


class Work:
    __metaclass__ = PoolMeta
    __name__ = 'project.work'

    planning_unit = fields.Many2One('public.planning.unit', 'Public Project',
        states={
            'required': Eval('type') == 'project',
        }, depends=['type'])
    department = fields.Many2One('company.department', 'Department',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    responsible = fields.Many2One('company.employee', 'Responsible',
        domain=[
            ('department', '=', Eval('department'))
        ],
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    strategic_component = fields.Many2One(
        'public.strategic.component', 'Strategic Component',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    public_type = fields.Many2One('public.project.type', 'Public Project Type',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    background = fields.Text('Background',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    substantiation = fields.Text('Substantiation',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    general_target = fields.Text('General Target',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    specific_target = fields.Text('Specific Target',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    ndp_target = fields.Many2One(
        'public.ndp.target', 'National Development Plan Target',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    ndp_policies = fields.Many2Many(
        'public.project.ndp.policies', 'project', 'policy', 'Policies',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    risks = fields.One2Many('public.project.risk', 'project', 'Risks',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])
    close_conditions = fields.One2Many(
        'public.project.close.condition', 'project', 'Close Conditions',
        states={
            'required': (Eval('type') == 'project'),
        }, depends=['type'])

    @classmethod
    def __setup__(cls):
        super(Work, cls).__setup__()
        field_names = ['company', 'type', 'parent', 'sequence']
        for name in field_names:
            getattr(cls, name).states.update({
                'invisible': cls.company.states.get('invisible', False) |
                Bool(Eval('planning_unit'))
            })

    @classmethod
    def view_attributes(cls):
        return super(Work, cls).view_attributes() + [
            ('/form/notebook/page[@id="planning_detail"]', 'states', {
                'invisible': (Eval('type') != 'project'),
                }),
            ('/form/notebook/page[@id="policies"]', 'states', {
                'invisible': (Eval('type') != 'project'),
                }),
            ('/form/notebook/page[@id="risks"]', 'states', {
                'invisible': (Eval('type') != 'project'),
                }),
            ('/form/notebook/page[@id="close_conditions"]', 'states', {
                'invisible': (Eval('type') != 'project'),
                }),
        ]


class PublicProjectRisk(ModelSQL, ModelView):
    """Public Project Risk"""
    __name__ = 'public.project.risk'
    project = fields.Many2One('project.work', 'Project',
        domain=[
            ('type', '=', 'project'),
        ], select=True, required=True, ondelete='CASCADE')
    risk = fields.Many2One('public.risk', 'Risk', required=True)
    mitigations = fields.One2Many(
        'public.project.risk.mitigation', 'project_risk', 'Mitigations')
    weight = fields.Numeric('Weight', digits=(16, 2), required=True,
        domain=[
            ('weight', '>=', 0),
            ('weight', '<=', 1)
        ])

    @classmethod
    def __setup__(cls):
        super(PublicProjectRisk, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('project_risk_uniq', Unique(t, t.project,
            t.risk), 'Risk must be unique per project')
        ]


class PublicProjectRiskMitigation(ModelSQL, ModelView):
    """Public Project Risk Mitigation"""
    __name__ = 'public.project.risk.mitigation'
    project_risk = fields.Many2One(
        'public.project.risk', 'Project Risk', select=True, required=True,
        ondelete='CASCADE')
    taken_action = fields.Char('Taken action', required=True)
    accomplished = fields.Boolean('Accomplished')

    @staticmethod
    def default_accomplished():
        return False


class PublicProjectPolicies(ModelSQL):
    """Public Project Policy"""
    __name__ = 'public.project.ndp.policies'
    project = fields.Many2One('project.work', 'Project',
        domain=[
            ('type', '=', 'project'),
        ], select=True, required=True, ondelete='CASCADE')
    policy = fields.Many2One('public.ndp.policy', 'Policy', select=True,
        ondelete='CASCADE', required=True)


class PublicProjectCloseCondition(PublicGenericMixin, ModelSQL, ModelView):
    """Public Project Close Condition"""
    __name__ = 'public.project.close.condition'
    project = fields.Many2One('project.work', 'Project',
        domain=[
            ('type', '=', 'project'),
        ], select=True, required=True, ondelete='CASCADE')
    accomplished = fields.Boolean('Accomplished')

    @staticmethod
    def default_accomplished():
        return False
