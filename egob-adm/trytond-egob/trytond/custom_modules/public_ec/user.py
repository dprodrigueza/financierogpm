from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['User']


class User:
    __metaclass__ = PoolMeta
    __name__ = 'res.user'
    planning_units = fields.Many2Many('public.planning.unit-res.user',
        'user', 'planning_unit', 'Planning Units')

    @classmethod
    def __setup__(cls):
        super(User, cls).__setup__()
        cls._context_fields.insert(0, 'planning_units')
