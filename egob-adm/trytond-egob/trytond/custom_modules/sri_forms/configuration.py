from decimal import Decimal

from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, Workflow, fields, ModelSingleton
from trytond.pyson import Eval
from trytond.transaction import Transaction


__all__ = ['Configuration', 'IncomeTax']


class Configuration(ModelSQL, ModelView):
    'SRI Configuration'
    __name__ = 'sri.configuration'

    max_value_total_expense = fields.Numeric('Max value 107', digits=(16, 2),
        required=True)
    max_value_expense = fields.Numeric('Max value c106', digits=(16, 2),
        required=True)
    configs = fields.One2Many('sri.configuration.income.tax', 'configuration',
        'Basic Fraction Configuration')
    start_date = fields.Date('Starting Date', required=True,
        domain=[('start_date', '<=', Eval('end_date', None))])
    end_date = fields.Date('Ending Date', required=True,
        domain=[('end_date', '>=', Eval('start_date', None))])

    @classmethod
    def __setup__(cls):
        super(Configuration, cls).__setup__()
        cls._order.insert(0, ('start_date', 'ASC'))
        cls._error_messages.update({
        'configurations_overlap': ('"%(first)s" and "%(second)s" configurations '
            'overlap.'),
        })

    @classmethod
    def validate(cls, configurations):
        super(Configuration, cls).validate(configurations)
        for configuration in configurations:
            configuration.check_dates()

    def get_rec_name(self, name=None):
        return "%s - %s" % (self.start_date, self.end_date)

    def check_dates(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        cursor.execute(*table.select(table.id,
                where=(((table.start_date <= self.start_date)
                        & (table.end_date >= self.start_date))
                    | ((table.start_date <= self.end_date)
                        & (table.end_date >= self.end_date))
                    | ((table.start_date >= self.start_date)
                        & (table.end_date <= self.end_date)))
                        & (table.id != self.id)))
        configuration_id = cursor.fetchone()
        if configuration_id:
            overlapping_configuration = self.__class__(configuration_id[0])
            self.raise_user_error('configurations_overlap', {
                    'first': self.rec_name,
                    'second': overlapping_configuration.rec_name,
                    })


class IncomeTax(ModelSQL, ModelView):
    'Income Tax'
    __name__ = 'sri.configuration.income.tax'

    configuration = fields.Many2One('sri.configuration', 'configuration',
        required=True)
    basic_fraction = fields.Numeric('Basic Fraction', digits=(16, 2),
        required=True)
    excess = fields.Numeric('Excess up to', digits=(16, 2), required=True)
    basic_fraction_tax = fields.Numeric('Basic fraction tax', digits=(16,2),
        required=True)
    excess_fraction_percentage = fields.Numeric('Excess fraction tax',
        digits=(16, 2), required=True,
        domain=[
			('excess_fraction_percentage', '>=', Decimal('0')),
			('excess_fraction_percentage', '<=', Decimal('1')),
			],)
