from trytond.modules.company.company  import CompanyReport
from trytond.pool import Pool

__all__ = ['Report']


class Report_107(CompanyReport):
    __name__ = 'sri.form.107'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(Report_107,cls).get_context(records, data)
        report_context['date_str'] = cls._date_str
        return report_context

    @classmethod
    def _date_str(cls, date):
        return date.strftime("%Y%m%d")
