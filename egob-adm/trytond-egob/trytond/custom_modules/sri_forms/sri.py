from decimal import Decimal

from trytond.pool import Pool
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Form_107']

class Form_107(Workflow, ModelSQL, ModelView):
    'Form_107'
    __name__='sri.form.107'
    _history = True
    _states = {
        'readonly' : Eval('state') != 'draft',
    }
    _depends=['state']
    checkbox = fields.Boolean('Check Disease')
    basic_fraction = fields.Function(
        fields.Numeric('Basic Fraction', digits=(16,2)), 'on_change_with_basic_fraction')
    fiscalyear_start_date = fields.Function(
        fields.Date('Start Date'), 'on_change_with_fiscalyear_start_date')
    fiscalyear_end_date = fields.Function(
        fields.Date('End Date'), 'on_change_with_fiscalyear_end_date')
    company = fields.Many2One('company.company', 'Empresa', required=True,
        states=_states, depends=_depends)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Ejercicio Fiscal',
        required=True,
        states=_states, depends=_depends)
    form_date = fields.Date('Form Date', required=True,
        domain=[
            ('form_date', '>=', Eval('fiscalyear_start_date')),
            ('form_date', '<=', Eval('fiscalyear_end_date'))
        ], states=_states, depends=_depends + [
            'fiscalyear_start_date', 'fiscalyear_end_date'])
    employee = fields.Many2One('company.employee', 'Cédula', required = True,
        states=_states, depends=_depends)
    c_103 = fields.Numeric('c_103', digits=(8,2), required = True,
        states=_states, depends=_depends)
    c_104 = fields.Numeric('c_104', digits=(8,2), required = True,
        states=_states, depends=_depends)
    total_105 = fields.Function(
        fields.Numeric('total_105', digits=(16,2)),'on_change_with_total_105')
    c_106 = fields.Numeric('c_106', digits=(8,2), required = True,
		domain=[
			('c_106', '>=', Decimal('0')),
			], states=_states, depends=_depends)
    c_107 = fields.Numeric('c_107', digits=(8,2), required = True,
        domain=[
			('c_107', '>=', Decimal('0')),
			], states=_states, depends=_depends)
    c_108 = fields.Numeric('c_108', digits=(9,2), required = True,
        domain=[
			('c_108', '>=', Decimal('0')),
			], states=_states, depends=_depends)
    c_109 = fields.Numeric('c_109', digits=(8,2), required = True,
        domain=[
			('c_109', '>=', Decimal('0')),
			], states=_states, depends=_depends)
    c_110 = fields.Numeric('c_110', digits=(8,2), required = True,
        domain=[
			('c_110', '>=', Decimal('0')),
			], states=_states, depends=_depends)
    total_111 = fields.Function(
    fields.Numeric('total_111', digits=(16,2)),'on_change_with_total_111')
    state = fields.Selection([
		('draft', 'Draft'),
		('confirmed', 'Confirmed'),
		('cancelled', 'Cancelled'),
		], 'State', readonly = True)

    @classmethod
    def __setup__(cls):
        super(Form_107, cls).__setup__()

        cls._transitions |= set((
			('draft', 'confirmed'),
			('draft', 'cancelled'),
            ('confirmed', 'draft'),
			))

        cls._buttons.update({
			'confirm':{
				'invisible': (Eval('state') != 'draft'),
				'icon':'tryton-ok',
				'depends' : ['state'],
			},
			'cancel':{
				'invisible': (Eval('state') != 'draft'),
				'icon':'tryton-cancel',
				'depends' : ['state'],
			},
			'draft':{
				'invisible': (Eval('state') != 'confirmed'),
				'icon':'tryton-go-previous',
				'depends' : ['state']
				},
			})

        cls._error_messages.update({
            'error_total_income': ('Error on form "%(form)s" this value can not '
                'exeed 50% of your total income: "%(total_income)s" .'),

            'error_max_value': ('Error on form "%(form)s" this value can not '
                'exeed: "%(max_value)s" .'),

            'error_minimum_basic': ('Error on form "%(form)s" this value can not '
                'exeed the minimum basic: "%(minimum_basic)s" .'),

            'error_cell': ('Error on form "%(form)s" this value can not '
                'be greater than: "%(max_cell)s" .'),

            'error_max_health': ('Error on form "%(form)s" this value can not '
                'be greater than "%(max_value)s" .'),

            'error_no_date': ('Error no configuration for this date .'),
			})

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), exception=False)

    @staticmethod
    def default_employee():
        return Transaction().context.get('employee')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_total_105():
        return Decimal('0')

    @staticmethod
    def default_total_111():
        return Decimal('0')

    @staticmethod
    def default_c_103():
        return Decimal('0')

    @staticmethod
    def default_c_104():
        return Decimal('0')

    @staticmethod
    def default_c_106():
        return Decimal('0')

    @staticmethod
    def default_c_107():
        return Decimal('0')

    @staticmethod
    def default_c_108():
        return Decimal('0')

    @staticmethod
    def default_c_109():
        return Decimal('0')

    @staticmethod
    def default_c_110():
        return Decimal('0')

    @staticmethod
    def default_monto():
        return Decimal('0')

    @staticmethod
    def default_form_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('c_103', 'c_104')
    def on_change_with_total_105(self, name=None):
        total = (self.c_103 or  Decimal('0')) + (self.c_104 or  Decimal('0'))
        return total.quantize(Decimal('0.01'))

    @fields.depends('c_106', 'c_107', 'c_108', 'c_109', 'c_110')
    def on_change_with_total_111(self, name=None):
        total = ((self.c_106 or Decimal('0')) + (self.c_107 or  Decimal('0')) +
            (self.c_108 or  Decimal('0')) + (self.c_109 or  Decimal('0')) +
            (self.c_110 or  Decimal('0')))
        return total.quantize(Decimal('0.01'))

    @fields.depends('fiscalyear')
    def on_change_with_fiscalyear_start_date(self, name=None):
            if self.fiscalyear:
                return self.fiscalyear.start_date
            return None

    @fields.depends('fiscalyear')
    def on_change_with_fiscalyear_end_date(self, name=None):
            if self.fiscalyear:
                return self.fiscalyear.end_date
            return None

    @fields.depends('total_105')
    def on_change_with_basic_fraction(self, name=None):
        pool = Pool()
        Values = pool.get('sri.configuration.income.tax')
        lines = Values.search([
            ('configuration.start_date', '<=', self.form_date),
            ('configuration.end_date', '>=', self.form_date),
            ('basic_fraction', '<=', self.total_105),
            ('excess', '>=', self.total_105),
            ])
        if lines:
            return lines[0].basic_fraction
        else:
            return 0

    @classmethod
    def validate(cls, forms):
        super(Form_107, cls).validate(forms)
        for form in forms:
            form.check_totals()

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, forms):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, forms):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, forms):
        pass

    def check_totals(self):
        pool = Pool()
        Conf = pool.get('sri.configuration')
        conf = Conf.search([
            ('start_date', '<=', self.form_date),
            ('end_date', '>=', self.form_date),
            ])
        total_income = Decimal(self.total_105) * Decimal('0.5')
        minimum_basic = (self.total_105 - self.basic_fraction) * Decimal('1.3')
        print('FRACTION MINIMAA ', self.basic_fraction)
        print('BASICOO MINIMAA ', minimum_basic)
        if not conf:
            self.raise_user_error('error_no_date')
        max_casillero = conf[0].max_value_expense
        max_value = conf[0].max_value_total_expense

        if (self.total_111 > total_income):
                self.raise_user_error('error_total_income', {
                    'form': self.rec_name,
                    'total_income': total_income,
                    })

        if (self.total_111 > max_value):
            self.raise_user_error('error_max_value', {
                'form': self.rec_name,
                'max_value': max_value,
                })

        if (self.total_111 > minimum_basic):
            self.raise_user_error('error_minimum_basic', {
                'form': self.rec_name,
                'minimum_basic': minimum_basic,
                })

        if ((self.c_106 or self.c_107 or self.c_109 or self.c_110) >=
            max_casillero):
            self.raise_user_error('error_cell', {
                'form': self.rec_name,
                'max_cell': max_casillero,
                })

        if ((self.c_108) >= max_value):
            self.raise_user_error('error_max_health', {
                'form': self.rec_name,
                'max_value': max_value,
                })
