# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import sri
from . import configuration
from . import report

__all__ = ['register']


def register():
    Pool.register(
        sri.Form_107,
        configuration.Configuration,
        configuration.IncomeTax,
        module='sri_forms', type_='model')
    Pool.register(
        module='sri_forms', type_='wizard')
    Pool.register(
        report.Report_107,
        module='sri_forms', type_='report')
