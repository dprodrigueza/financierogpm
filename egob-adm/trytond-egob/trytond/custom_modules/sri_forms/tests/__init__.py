# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.sri_forms.tests.test_sri_forms import suite
except ImportError:
    from .test_sri_forms import suite

__all__ = ['suite']
