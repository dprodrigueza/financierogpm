import stdnum.ec.ci as ci
import stdnum.ec.ruc as ruc
import stdnum.exceptions

from trytond.model import Unique
from trytond.pool import PoolMeta

__all__ = ['Party', 'PartyIdentifier']


class Party:
    __metaclass__ = PoolMeta
    __name__ = 'party.party'

    @classmethod
    def _tax_identifier_types(cls):
        return ['ec_ci', 'ec_ruc']


class PartyIdentifier:
    __metaclass__ = PoolMeta
    __name__ = 'party.identifier'

    @classmethod
    def __setup__(cls):
        super(PartyIdentifier, cls).__setup__()
        t = cls.__table__()
        cls._error_messages.update({
                'invalid_ci': ('Invalid CI number "%(code)s" '
                    'on party "%(party)s".'),
                })
        cls._error_messages.update({
                'invalid_ruc': ('Invalid RUC number "%(code)s" '
                    'on party "%(party)s".'),
                })
        cls._sql_constraints += [
            ('code_uniq', Unique(t, t.code), 'Identifier already exist'),
        ]
        cls.type.selection = [('ec_ci', 'CED'), ('ec_ruc', 'RUC')]

    def on_change_with_code(self):
        if self.type == 'ec_ci':
            try:
                return ci.compact(self.code)
            except stdnum.exceptions.ValidationError:
                pass
        elif self.type == 'ec_ruc':
            try:
                return ruc.compact(self.code)
            except stdnum.exceptions.ValidationError:
                pass
        super(PartyIdentifier, self).on_change_with_code()

    def check_code(self):
        if self.type == 'ec_ci':
            if not ci.is_valid(self.code):
                if self.party and self.party.id > 0:
                    party = self.party.rec_name
                else:
                    party = ''
                self.raise_user_error('invalid_ci', {
                        'code': self.code,
                        'party': party,
                        })
        if self.type == 'ec_ruc':
            if not ruc.is_valid(self.code):
                if self.party and self.party.id > 0:
                    party = self.party.rec_name
                else:
                    party = ''
                self.raise_user_error('invalid_ruc', {
                        'code': self.code,
                        'party': party,
                        })
        super(PartyIdentifier, self).check_code()
