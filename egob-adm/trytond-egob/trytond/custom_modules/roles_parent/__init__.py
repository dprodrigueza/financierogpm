# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import group

__all__ = ['register']


def register():
    Pool.register(
        group.Group,
        module='roles_parent', type_='model')
    Pool.register(
        module='roles_parent', type_='wizard')
    Pool.register(
        module='roles_parent', type_='report')
