# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.roles_parent.tests.test_roles_parent import suite
except ImportError:
    from .test_roles_parent import suite

__all__ = ['suite']
