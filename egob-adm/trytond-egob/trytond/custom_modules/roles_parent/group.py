from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.model import (ModelSingleton, ModelSQL, UnionMixin, fields,
    sequence_ordered)

__all__ = ['Group']

class Group(sequence_ordered(),metaclass=PoolMeta):
    "Group"
    __name__ = "res.group"
    parent = fields.Many2One('res.group', 'Grupo padre')
    childs = fields.One2Many('res.group', 'parent', 'Grupos hijos')


