from decimal import Decimal
from datetime import date
from collections import defaultdict
from calendar import monthrange
import functools
from itertools import groupby

from sql.operators import Concat
from sql import Literal, Window, Null, Union
from sql.aggregate import Sum
from sql.functions import CurrentTimestamp, RowNumber
from sql.conditionals import Coalesce, Case

from trytond.model import ModelView, ModelSQL, Workflow, fields, ModelSingleton
from trytond.pool import Pool
from trytond.pyson import Bool, If, Eval
from trytond.transaction import Transaction
from trytond.tools import cursor_dict, reduce_ids, grouped_slice


__all__ = ['CheckbookConfigurationGeneral', 'Checkbook', 'CheckDrawn',
    'CheckReceived', 'CheckReturned', 'CheckReceivedType',
    'CheckReturnedReasonType', 'CheckConsultingContext', 'CheckConsulting']


DIGITS = (16, 2)

ZERO = Decimal('0.00')

CHECKBOOK_STATES = [
    ('draft', 'Borrador'),
    ('open', 'Abierto'),
    ('close', 'Cerrada'),
]

CHECK_STATES = [
    ('draft', 'Borrador'),
    ('confirm', 'Confirmado'),
    ('done', 'Realizado'),
    ('cancel', 'Anulado'),
    ('effective', 'Efectivizado'),
    ('return', 'Devuelto'),
]

CHECK_RETURNED_TYPES = [
    ('drawn', 'Emitido'),
    ('received', 'Receptado'),
]

CHECK_RETURNED_STATES = [
    ('draft', 'Borrador'),
    ('confirm', 'Confirmado'),
    ('done', 'Realizado'),
]

CHECK_RETURNED_SANCTION_STATES = [
    ('no_sanction', ''),
    ('pending', 'Pendiente'),
    ('paid', 'Pagada'),
]


def date_field(string):
    return fields.Date(string,
        states={
            'readonly': True,
            })


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                cls.write(
                    [it for it in items
                        if not getattr(it, field)], {
                            field: employee.id,
                        })
            return result
        return wrapper
    return decorator


def set_date(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            Date = pool.get('ir.date')
            result = func(cls, items, *args, **kwargs)
            _date = Date.today()
            if _date:
                cls.write(
                    [it for it in items
                        if not getattr(it, field)], {
                            field: _date,
                        })
            return result
        return wrapper
    return decorator


class CheckbookConfigurationGeneral(ModelSingleton, ModelSQL, ModelView):
    'Checkbook Configuration General'
    __name__ = 'checkbook.configuration.general'
    _history = True

    checkbook_sequence = fields.Many2One('ir.sequence',
        'Secuencia de chequeras',
        domain=[
            ('code', '=', 'checkbook.checkbook'),
        ])
    check_drawn_sequence = fields.Many2One('ir.sequence',
        'Secuencia de cheques girados',
        domain=[
            ('code', '=', 'checkbook.check.drawn'),
        ])


class Checkbook(Workflow, ModelView, ModelSQL):
    'Checkbook'
    __name__ = 'checkbook.checkbook'
    _history = True

    _STATES = {
        'readonly': Eval('state') != 'draft'
    }
    _DEPENDS = ['company', 'state']

    company = fields.Many2One('company.company', 'Empresa',
        domain=[
            ('id', If(Eval('context', {}).contains('company'),
                '=', '!='), Eval('context', {}).get('company', -1)),
        ],
        states={
            'readonly': True
        }, depends=['company'], required=True)
    party_company = fields.Function(fields.Many2One('party.party',
        'Tercero/empresa',
        states={
            'invisible': True
        }), 'get_party_company')
    sequence = fields.Char('Secuencia',
        states={
            'readonly': True,
            'required': True
        })
    bank_account = fields.Many2One('bank.account', 'Cuenta bancaria',
        domain=[
            ('owners', 'in', [Eval('party_company')])
        ], states=_STATES, depends=_DEPENDS, required=True)
    emission_date = fields.Date('Fecha de emisión', states=_STATES,
        depends=_DEPENDS, required=True)
    responsable = fields.Many2One('company.employee', 'Responsable',
        domain=[
            ('company', '=', Eval('company'))
        ], states=_STATES, depends=_DEPENDS, required=True)
    reference = fields.Char('Referencia', states=_STATES, depends=_DEPENDS)
    number_start = fields.Integer('Número desde',
        domain=[
            ('number_start', '>', 0)
        ], states=_STATES, depends=_DEPENDS, required=True)
    number_end = fields.Integer('Número hasta',
        domain=[
            ('number_end', '>=', Eval('number_start')),
        ], states=_STATES, depends=_DEPENDS + ['number_start'], required=True)
    number_available_checks = fields.Function(
        fields.Integer('Cheques disponibles'),
        'get_number_available_checks')
    state = fields.Selection(CHECKBOOK_STATES, 'Estado',
        states={
            'readonly': True,
            'required': True
        })
    state_translated = state.translated('state')

    # Info
    checks_drawn = fields.One2Many('checkbook.check.drawn', 'checkbook',
        'Cheques girados',
        states={
            'readonly': True
        })
    checks_returned = fields.Function(
        fields.One2Many('checkbook.check.returned', 'checkbook',
        'Cheques devueltos'), 'get_checks_returned')
    checks_availables = fields.Function(
        fields.Text('Números disponibles',
        help="Listado de números de cheques que aún no han sido usados."),
        'get_checks_availables')

    @classmethod
    def __setup__(cls):
        super(Checkbook, cls).__setup__()
        cls._error_messages.update({
            'no_sequence': ('No se ha encontrado una secuencia definida para '
                'las chequeras.\n\nDiríjase a "Cheques / Configuración de '
                'cheques / Configuración general" y defina una secuencia.'),
            'inactive_sequence': ('No es posible definir la secuencia debido a '
                'que la secuencia usada para chequeras se encuentra inactiva.'
                '\n\nDiríjase a "Cheques / Configuración de cheques / '
                'Configuración general" y active la secuencia o defina otra en '
                'su lugar.'),
            'no_draft': ('No es posible pasar a estado BORRADOR la chequera '
                '"%(checkbook)s" por que ya se han emitido algunos de sus '
                'cheques.'),
            'warning_close_available_checks': ('La chequera "%(checkbook)s" '
                'tiene %(available_checks)s cheques que no han sido usados. Si '
                'cierra esta chequera, ya no podrá usar los cheques que aún '
                'están disponibles.')
        })

        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'draft'),
            ('open', 'close'),
        ))

        cls._buttons.update({
            'draft': {
                'invisible': (Eval('state').in_(['draft', 'close'])),
                'icon': 'tryton-undo',
                'depends': ['state']
            },
            'open': {
                'invisible': (Eval('state').in_(['open', 'close'])),
                'icon': 'tryton-arrow-up',
                'depends': ['state'],
            },
            'close': {
                'invisible': (Eval('state').in_(['draft', 'close'])),
                'icon': 'tryton-arrow-down',
                'depends': ['state'],
            },
        })
        cls._order.insert(0, ('company', 'ASC'))
        cls._order.insert(1, ('emission_date', 'DESC'))
        cls._order.insert(2, ('sequence', 'DESC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_sequence():
        return '/'

    @staticmethod
    def default_responsable():
        return Transaction().context.get('employee')

    @staticmethod
    def default_emission_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, checkbooks):
        for checkbook in checkbooks:
            if checkbook.checks_drawn or checkbook.checks_returned:
                cls.raise_user_error('no_draft', {
                    'checkbook': checkbook.rec_name,
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, checkbooks):
        cls.set_sequence(checkbooks)

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, checkbooks):
        cls.check_close_checkbooks_warning(checkbooks)

    @fields.depends('company', 'bank_account', 'responsable', 'party_company')
    def on_change_company(self):
        if self.company:
            self.party_company = self.company.party
        else:
            self.party_company = None
            self.responsable = None
            self.bank_account = None

    @classmethod
    def get_party_company(cls, checkbooks, names=None):
        result = defaultdict(lambda: None)
        for checkbook in checkbooks:
            if checkbook.company and checkbook.company.party:
                result[checkbook.id] = checkbook.company.party.id
        return {
            'party_company': result
        }

    @classmethod
    def get_number_available_checks(cls, checkbooks, names=None):
        result = defaultdict(lambda: 0)
        available_numbers = cls.get_available_numbers(checkbooks)
        for checkbook_id, numbers in available_numbers.items():
            result[checkbook_id] = len(numbers)
        return {
            'number_available_checks': result
        }

    @classmethod
    def get_checks_returned(cls, checkbooks, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Checkbook = pool.get('checkbook.checkbook')
        CheckDrawn = pool.get('checkbook.check.drawn')
        CheckReturned = pool.get('checkbook.check.returned')
        tbl_checkbook = Checkbook.__table__()
        tbl_check_drawn = CheckDrawn.__table__()
        tbl_check_returned = CheckReturned.__table__()

        result = defaultdict(lambda: [])
        checkbooks_ids = [cb.id for cb in checkbooks]

        query = tbl_check_returned.join(tbl_check_drawn,
            condition=(tbl_check_drawn.id == tbl_check_returned.check_drawn)
        ).join(tbl_checkbook,
            condition=(tbl_checkbook.id == tbl_check_drawn.checkbook)
        ).select(
            tbl_checkbook.id.as_('checkbook'),
            tbl_check_returned.id.as_('check_returned'),
            where=(tbl_checkbook.id.in_(checkbooks_ids))
        )

        cursor.execute(*query)
        for row in cursor_dict(cursor):
            checkbook_id = row['checkbook']
            check_returned_id = row['check_returned']
            result[checkbook_id].append(check_returned_id)
        return {
            'checks_returned': result
        }

    @classmethod
    def get_checks_availables(cls, checkbooks, names=None):
        result = defaultdict(lambda: 0)
        available_numbers = cls.get_available_numbers(checkbooks)
        for checkbook_id, numbers in available_numbers.items():
            result[checkbook_id] = ', '.join([str(n) for n in numbers])
        return {
            'checks_availables': result
        }

    @classmethod
    def set_sequence(cls, checkbooks):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('checkbook.configuration.general')

        config = Config(1)
        sequence = config.checkbook_sequence
        if not sequence:
            cls.raise_user_error('no_sequence')
        if not sequence.active:
            cls.raise_user_error('inactive_sequence')

        for checkbook in checkbooks:
            if checkbook.sequence == '/' or not checkbook.sequence:
                checkbook.sequence = Sequence.get_id(sequence.id)
        cls.save(checkbooks)

    def get_next_available_number(self):
        available_numbers = Checkbook.get_available_numbers([self])
        if available_numbers:
            numbers = available_numbers[self.id]
            if numbers:
                return numbers[0]
        return None

    @classmethod
    def get_available_numbers(cls, checkbooks):
        available_numbers = defaultdict(lambda: [])
        for checkbook in checkbooks:
            available_numbers[checkbook.id] = []

        used_numbers = cls.get_used_numbers(checkbooks)
        for cb in checkbooks:
            if cb.number_start and cb.number_end:
                used = used_numbers[cb.id]
                for n in range(cb.number_start, cb.number_end + 1):
                    if n not in used:
                        available_numbers[cb.id].append(n)
        return available_numbers

    @classmethod
    def get_used_numbers(cls, checkbooks):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Checkbook = pool.get('checkbook.checkbook')
        CheckDrawn = pool.get('checkbook.check.drawn')
        tbl_checkbook = Checkbook.__table__()
        tbl_check_drawn = CheckDrawn.__table__()

        used_numbers = defaultdict(lambda: [])
        for checkbook in checkbooks:
            used_numbers[checkbook.id] = []
        checkbooks_ids = [cb.id for cb in checkbooks]

        if checkbooks_ids:
            query = tbl_check_drawn.join(tbl_checkbook,
                condition=(tbl_checkbook.id == tbl_check_drawn.checkbook)
            ).select(
                tbl_checkbook.id.as_('checkbook'),
                tbl_check_drawn.number,
                where=(tbl_checkbook.id.in_(checkbooks_ids)),
                order_by=[tbl_checkbook.id.asc, tbl_check_drawn.number.asc]
            )

            cursor.execute(*query)
            for row in cursor_dict(cursor):
                checkbook_id = row['checkbook']
                number = row['number']
                used_numbers[checkbook_id].append(number)
        return used_numbers

    @classmethod
    def check_close_checkbooks_warning(cls, checkbooks):
        for cb in checkbooks:
            available_checks = cb.number_available_checks
            if available_checks and available_checks > 0:
                cls.raise_user_warning('warning_close_%d' % cb.id,
                    'warning_close_available_checks', {
                        'checkbook': cb.rec_name,
                        'available_checks': available_checks
                    })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
            ('company',) + tuple(clause[1:]),
            ('sequence',) + tuple(clause[1:]),
            ('bank_account',) + tuple(clause[1:]),
            ('bank_account.bank',) + tuple(clause[1:]),
            ('responsable',) + tuple(clause[1:]),
            ('state',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        seq = self.sequence.upper()
        bank = self.bank_account.bank.party.name.upper()
        if self.bank_account.numbers:
            bank += f' - CUENTA {self.bank_account.numbers[0].number}'
        state = self.state_translated.upper()
        return '%s - %s [%s]' % (seq, bank, state)


class CheckDrawn(Workflow, ModelView, ModelSQL):
    'Check Drawn'
    __name__ = 'checkbook.check.drawn'
    _history = True

    _STATES = {
        'readonly': Eval('state') != 'draft'
    }
    _DEPENDS = ['state']

    checkbook = fields.Many2One('checkbook.checkbook', 'Chequera',
        domain=[
            If(Eval('state', '').in_(['draft']),
               ('state', '=', 'open'), ('state', 'in', ['open', 'close']))
        ], states=_STATES, depends=_DEPENDS, required=True)
    sequence = fields.Char('Secuencia',
        states={
            'readonly': True,
            'required': True
        })
    number = fields.Integer('Número',
        domain=[
            ('number', '>', 0)
        ], states=_STATES, depends=_DEPENDS, required=True)
    transmitter = fields.Many2One('party.party', 'Solicitante',
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS)
    beneficiary = fields.Many2One('party.party', 'Beneficiario',
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS)
    amount = fields.Numeric('Monto',
        domain=[
            ('amount', '>', 0)
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS, digits=DIGITS)
    emission_date = fields.Date('Fecha de emisión',
        states={
            'readonly': True,
            'required': True
        }, depends=_DEPENDS)
    check_date = fields.Date('Fecha de cheque',
        domain=[
          If(Bool(Eval('is_postdated')),
             ('check_date', '>', Eval('emission_date')),
             ('check_date', '<=', Eval('emission_date')))
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS + ['is_postdated', 'check_date', 'emission_date'])
    effective_date = fields.Date('Fecha de efectivización',
        states={
            'invisible': ~Eval('state').in_(['done', 'effective']),
            'readonly': ~Eval('state').in_(['done']),
            'required': Eval('state').in_(['effective']),
        }, depends=_DEPENDS)
    is_postdated = fields.Boolean('Cheque posfechado?', states=_STATES,
        depends=_DEPENDS, help='Cheque posfechado?')
    is_reconsignment = fields.Boolean('Reconsignación?', states=_STATES,
        depends=_DEPENDS, help='Cheque de reconsignación?')
    check_received = fields.Many2One('checkbook.check.returned',
        'Cheque devuelto',
        domain=[
            ('state', 'in', ['done']),
            ('type_', 'in', ['drawn']),
        ], states={
            'invisible': ~Bool(Eval('is_reconsignment')),
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('is_reconsignment')),
        }, depends=['state', 'is_reconsignment'])
    state = fields.Selection(CHECK_STATES, 'Estado',
        states={
            'readonly': True,
            'required': True
        })
    state_translated = state.translated('state')
    responsable = fields.Function(
        fields.Many2One('company.employee', 'Responsable'), 'get_responsable')

    # Audit
    created_by = employee_field('Creado por')
    created_date = date_field('Se creó el')
    confirmed_by = employee_field('Confirmado por')
    confirmed_date = date_field('Se confirmó el')
    done_by = employee_field('Aprobado por')
    done_date = date_field('Se realizó el')
    effective_by = employee_field('Efectivizado por')
    effectivization_date = date_field('Se efectivizó el')
    cancel_by = employee_field('Anulado por')
    cancel_date = date_field('Se anuló el')
    cancel_reason = fields.Text('Razón de anulación',
        states={
            'readonly': ~(Eval('state', '').in_(['confirm', 'done'])),
        }, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(CheckDrawn, cls).__setup__()
        cls._error_messages.update({
            'no_cancel_reason':('Para poder anular un registro, debe '
                'ingresar la razón en el campo "Razón anulación".'),
            'no_sequence': ('No se ha encontrado una secuencia definida para '
                'los cheques girados.\n\nDiríjase a "Cheques / Configuración '
                'de cheques / Configuración general" y defina una secuencia.'),
            'inactive_sequence': ('No es posible definir la secuencia debido a '
                'que la secuencia usada para cheques girados se encuentra '
                'inactiva.\n\nDiríjase a "Cheques / Configuración de cheques / '
                'Configuración general" y active la secuencia o defina otra en '
                'su lugar.'),
            'no_available_numbers': ('La chequera "%(checkbook)s" no tiene '
                'cheques disponibles.'),
            'used_number': ('El número de cheque "%(number)s" no se encuentra '
                'disponible en la chequera "%(checkbook)s" ya que está siendo '
                'usando en el cheque "%(conflict_check_drawn)s". Por favor, '
                'utilice otro número para este registro.'),
            'number_out_of_range': ('El número de cheque "%(number)s" no se '
                'encuentra en el rango de números de la chequera '
                '"%(checkbook)s". Por favor, utilice un número entre '
                '%(range)s.'),
            'no_effective_date': ('Para efectivizar el cheque "%(check)s" es '
                'necesario especificar la fecha de efectivización.'),
        })

        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
            ('done', 'return'),
            ('done', 'effective'),
        ))

        cls._buttons.update({
            'draft': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-undo',
                'depends': ['state']
            },
            'confirm': {
                'invisible': ~(Eval('state').in_(['draft'])),
                'icon': 'tryton-forward',
                'depends': ['state'],
            },
            'done': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-ok',
                'depends': ['state'],
            },
            'effective': {
                'invisible': ~(Eval('state').in_(['done'])),
                'icon': 'tryton-copy',
                'depends': ['state'],
            },
            'cancel': {
                'invisible': ~(Eval('state').in_(['confirm', 'done'])),
                'icon': 'tryton-clear',
                'depends': ['state'],
            },
        })
        cls._order.insert(0, ('number', 'DESC'))
        cls._order.insert(1, ('check_date', 'DESC'))
        cls._order.insert(2, ('emission_date', 'DESC'))
        cls._order.insert(3, ('checkbook', 'ASC'))

    @staticmethod
    def default_sequence():
        return '/'

    @staticmethod
    def default_transmitter():
        pool = Pool()
        Employee = pool.get('company.employee')
        employee_id = Transaction().context.get('employee', None)
        if employee_id:
            employee = Employee(employee_id)
            return employee.party.id
        return None

    @staticmethod
    def default_amount():
        return 0

    @staticmethod
    def default_emission_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_check_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_created_by():
        employee_id = Transaction().context.get('employee', None)
        return employee_id

    @staticmethod
    def default_created_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def validate(cls, checks):
        CheckDrawn.check_available_number(checks)
        super(CheckDrawn, cls).validate(checks)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    @set_employee('confirmed_by')
    @set_date('confirmed_date')
    def confirm(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    @set_date('done_date')
    def done(cls, checks):
        cls.set_sequence(checks)
        cls.manage_checkbook_closing(checks)

    @classmethod
    @ModelView.button
    @Workflow.transition('effective')
    @set_employee('effective_by')
    @set_date('effectivization_date')
    def effective(cls, checks):
        cls.check_effective_date(checks)
        cls.set_sequence(checks)
        cls.manage_checkbook_closing(checks)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    @set_employee('cancel_by')
    @set_date('cancel_date')
    def cancel(cls, checks):
        for check in checks:
            if not check.cancel_reason or check.cancel_reason.strip() == '':
                cls.raise_user_error('no_cancel_reason')

    @fields.depends('checkbook', 'number', 'responsable')
    def on_change_checkbook(self):
        if self.checkbook:
            self.responsable = self.checkbook.responsable
            available_number = self.checkbook.get_next_available_number()
            if available_number:
                self.number = available_number
            else:
                self.raise_user_error('no_available_numbers', {
                    'checkbook': self.checkbook.rec_name
                })
        else:
            self.number = None
            self.responsable = None

    @fields.depends('is_reconsignment', 'check_received', 'beneficiary',
        'amount')
    def on_change_is_reconsignment(self):
        if not self.is_reconsignment:
            self.check_received = None
            self.beneficiary = None
            self.amount = None

    @fields.depends('check_received', 'beneficiary', 'amount')
    def on_change_check_received(self):
        if self.check_received:
            check_drawn = self.check_received.check_drawn
            if check_drawn:
                self.beneficiary = check_drawn.beneficiary
                self.amount = check_drawn.amount

    @classmethod
    def get_responsable(cls, checks, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Checkbook = pool.get('checkbook.checkbook')

        tbl_check_drawn = cls.__table__()
        tbl_checkbook = Checkbook.__table__()

        result = defaultdict(lambda: None)
        check_drawns_ids = [c.id for c in checks]

        query = tbl_check_drawn.join(tbl_checkbook,
            condition=(tbl_checkbook.id == tbl_check_drawn.checkbook)
        ).select(
            tbl_check_drawn.id,
            tbl_checkbook.responsable,
            where=(tbl_check_drawn.id.in_(check_drawns_ids))
        )

        cursor.execute(*query)
        for row in cursor_dict(cursor):
            check_id = row['id']
            responsable_id = row['responsable']
            result[check_id] = responsable_id
        return {
            'responsable': result
        }


    @classmethod
    def set_sequence(cls, checks):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('checkbook.configuration.general')

        config = Config(1)
        sequence = config.check_drawn_sequence
        if not sequence:
            cls.raise_user_error('no_sequence')
        if not sequence.active:
            cls.raise_user_error('inactive_sequence')

        for check in checks:
            if check.sequence == '/' or not check.sequence:
                check.sequence = Sequence.get_id(sequence.id)
        cls.save(checks)

    @classmethod
    def manage_checkbook_closing(cls, checks):
        Checkbook = Pool().get('checkbook.checkbook')

        checkbooks = []
        for check in checks:
            checkbook = check.checkbook
            if checkbook.state != 'close':
                if checkbook not in checkbooks:
                    checkbooks.append(check.checkbook)

        if checkbooks:
            available_numbers = Checkbook.get_available_numbers(checkbooks)

            to_close_ids = []
            for checkbook_id, numbers in available_numbers.items():
                if len(numbers) == 0:
                    to_close_ids.append(checkbook_id)

            to_save = Checkbook.browse(to_close_ids)
            for checkbook in Checkbook.browse(to_close_ids):
                checkbook.state = 'close'
                to_save.append(checkbook)
            Checkbook.save(to_save)

    @classmethod
    def check_available_number(cls, checks):
        Checkbook = Pool().get('checkbook.checkbook')
        checkbooks = list(set([c.checkbook for c in checks]))
        used_numbers = Checkbook.get_used_numbers(checkbooks)

        for check in checks:
            checkbook = check.checkbook
            number = check.number

            # check that the number is in the correct range
            if not (checkbook.number_start <= number <= checkbook.number_end):
                cls.raise_user_error('number_out_of_range', {
                    'number': number,
                    'checkbook': checkbook.rec_name,
                    'range': (f'[{checkbook.number_start} y '
                              f'{checkbook.number_end}]')
                })

            # check if the number is being used
            if number in used_numbers[checkbook.id]:
                conflict_checks = cls.search([
                    ('id', '!=', check.id),
                    ('checkbook', '=', checkbook),
                    ('number', '=', number)
                ])
                if conflict_checks:
                    cls.raise_user_error('used_number', {
                        'number': number,
                        'checkbook': checkbook.rec_name,
                        'conflict_check_drawn': conflict_checks[0].rec_name,
                    })

    @classmethod
    def check_effective_date(cls, checks):
        for check in checks:
            if not check.effective_date:
                cls.raise_user_error('no_effective_date', {
                    'check': check.rec_name
                })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        number = -100000
        try:
            number = int(clause[2].replace('%', ''))
        except Exception:
            pass

        domain = [bool_op,
            ('number',) + ('=', number),
            ('checkbook',) + tuple(clause[1:]),
            ('sequence',) + tuple(clause[1:]),
            ('transmitter',) + tuple(clause[1:]),
            ('beneficiary',) + tuple(clause[1:]),
            ('state',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        seq = self.sequence.upper()
        num = self.number
        checkbook_seq = self.checkbook.sequence.upper()
        bank = self.checkbook.bank_account.bank.party.name.upper()
        amount = self.amount
        check_date = self.check_date
        state = self.state_translated.upper()
        return 'N°%s [%s %s %s] [$%s] [%s] [%s]' % (
            num, bank, checkbook_seq, seq, amount, check_date, state)


class CheckReceived(Workflow, ModelView, ModelSQL):
    'Check Received'
    __name__ = 'checkbook.check.received'
    _history = True

    _STATES = {
        'readonly': Eval('state') != 'draft'
    }
    _DEPENDS = ['state']

    bank = fields.Many2One('bank', 'Banco', states=_STATES, depends=_DEPENDS,
        required=True)
    number = fields.Integer('Número',
        domain=[
            ('number', '>', 0)
        ], states=_STATES, depends=_DEPENDS, required=True)
    transmitter = fields.Many2One('party.party', 'Emisor',
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS)
    beneficiary = fields.Many2One('party.party', 'Beneficiario',
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS)
    check_date = fields.Date('Fecha de cheque',
        domain=[
          If(Bool(Eval('is_postdated')),
             ('check_date', '>', Eval('created_date')),
             ('check_date', '<=', Eval('created_date')))
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS + ['is_postdated', 'check_date', 'created_date'])
    amount = fields.Numeric('Monto',
        domain=[
            ('amount', '>', 0)
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS, digits=DIGITS)
    effective_date = fields.Date('Fecha de efectivización',
        states={
            'invisible': ~Eval('state').in_(['done', 'effective']),
            'readonly': ~Eval('state').in_(['done']),
            'required': Eval('state').in_(['effective']),
        }, depends=_DEPENDS)
    is_postdated = fields.Boolean('Cheque posfechado?', states=_STATES,
        depends=_DEPENDS)
    reception_type = fields.Many2One('checkbook.check.received.type',
        'Tipo de recepción', states=_STATES, depends=_DEPENDS, required=True)
    description = fields.Text('Descripción', states=_STATES, depends=_DEPENDS)
    responsable = fields.Many2One('company.employee', 'Responsable',
        states=_STATES, depends=_DEPENDS, required=True)
    state = fields.Selection(CHECK_STATES, 'Estado',
        states={
            'readonly': True,
            'required': True
        })
    state_translated = state.translated('state')

    # Audit
    created_by = employee_field('Creado por')
    created_date = date_field('Se creó el')
    confirmed_by = employee_field('Confirmado por')
    confirmed_date = date_field('Se confirmó el')
    done_by = employee_field('Aprobado por')
    done_date = date_field('Se realizó el')
    effective_by = employee_field('Efectivizado por')
    effectivization_date = date_field('Se efectivizó el')
    cancel_by = employee_field('Anulado por')
    cancel_date = date_field('Se anuló el')
    cancel_reason = fields.Text('Razón de anulación',
        states={
            'readonly': ~(Eval('state', '').in_(['confirm', 'done'])),
        }, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(CheckReceived, cls).__setup__()
        cls._error_messages.update({
            'no_cancel_reason':('Para poder anular un registro, debe '
                'ingresar la razón en el campo "Razón anulación".'),
            'no_effective_date': ('Para efectivizar el cheque "%(check)s" es '
                'necesario especificar la fecha de efectivización.'),
            'same_number_and_bank': ('No pueden existir dos cheques recibidos '
                'con el mismo número del mismo banco.\n\nEl número '
                '"%(number)s" ya ha sido registrado en el cheque '
                '"%(conflict_check)s" con fecha %(conflict_check_date)s '
                'emitido por %(conflict_check_transmitter)s por un valor de '
                '$%(conflict_check_amount)s.'),
        })

        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
            ('confirm', 'cancel'),
            ('done', 'cancel'),
            ('done', 'return'),
            ('done', 'effective'),
        ))

        cls._buttons.update({
            'draft': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-undo',
                'depends': ['state']
            },
            'confirm': {
                'invisible': ~(Eval('state').in_(['draft'])),
                'icon': 'tryton-forward',
                'depends': ['state'],
            },
            'done': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-ok',
                'depends': ['state'],
            },
            'effective': {
                'invisible': ~(Eval('state').in_(['done'])),
                'icon': 'tryton-copy',
                'depends': ['state'],
            },
            'cancel': {
                'invisible': ~(Eval('state').in_(['confirm', 'done'])),
                'icon': 'tryton-clear',
                'depends': ['state'],
            },
        })
        cls._order.insert(0, ('check_date', 'DESC'))
        cls._order.insert(1, ('number', 'ASC'))
        cls._order.insert(2, ('transmitter', 'ASC'))

    @staticmethod
    def default_beneficiary():
        pool = Pool()
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company', None)
        if company_id:
            company = Company(company_id)
            return company.party.id
        return None

    @staticmethod
    def default_amount():
        return 0

    @staticmethod
    def default_check_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_responsable():
        return Transaction().context.get('employee')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_created_by():
        employee_id = Transaction().context.get('employee', None)
        return employee_id

    @staticmethod
    def default_created_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    def validate(cls, checks):
        CheckReceived.check_same_number_and_bank(checks)
        super(CheckReceived, cls).validate(checks)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    @set_employee('confirmed_by')
    @set_date('confirmed_date')
    def confirm(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    @set_date('done_date')
    def done(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('effective')
    @set_employee('effective_by')
    @set_date('effectivization_date')
    def effective(cls, checks):
        for check in checks:
            if not check.effective_date:
                cls.raise_user_error('no_effective_date', {
                    'check': check.rec_name
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    @set_employee('cancel_by')
    @set_date('cancel_date')
    def cancel(cls, checks):
        for check in checks:
            if not check.cancel_reason or check.cancel_reason.strip() == '':
                cls.raise_user_error('no_cancel_reason')

    @classmethod
    def check_same_number_and_bank(cls, checks):
        for check in checks:
            conflict_checks = cls.search([
                ('id', '!=', check.id),
                ('number', '=', check.number),
                ('bank', '=', check.bank)
            ])
            if conflict_checks:
                cls.raise_user_error('same_number_and_bank', {
                    'number': check.number,
                    'bank': check.bank,
                    'conflict_check': conflict_checks[0].rec_name,
                    'conflict_check_transmitter': (
                        conflict_checks[0].transmitter.rec_name),
                    'conflict_check_amount': (
                        conflict_checks[0].amount),
                    'conflict_check_date': (
                        conflict_checks[0].check_date),
                })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        number = -100000
        try:
            number = int(clause[2].replace('%', ''))
        except Exception:
            pass

        domain = [bool_op,
            ('number',) + ('=', number),
            ('bank',) + tuple(clause[1:]),
            ('sequence',) + tuple(clause[1:]),
            ('transmitter',) + tuple(clause[1:]),
            ('beneficiary',) + tuple(clause[1:]),
            ('state',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        num = self.number
        bank = self.bank.party.name.upper()
        amount = self.amount
        check_date = self.check_date
        state = self.state_translated.upper()
        return 'N°%s [%s] [$%s] [%s] [%s]' % (
            num, bank, amount, check_date, state)


class CheckReturned(Workflow, ModelView, ModelSQL):
    'Check Returned'
    __name__ = 'checkbook.check.returned'
    _history = True

    _STATES = {
        'readonly': Eval('state') != 'draft'
    }
    _DEPENDS = ['state']

    type_ = fields.Selection(CHECK_RETURNED_TYPES, 'Tipo', states=_STATES,
        depends=_DEPENDS, required=True)
    type_translated = type_.translated('type_')
    check_drawn = fields.Many2One('checkbook.check.drawn',
        'Cheque girado',
        domain=[
            If(Eval('state', '').in_(['draft', 'confirm']),
               ('state', '=', 'done'), ('state', '=', 'return'))
        ], states={
            'invisible': Eval('type_') != 'drawn',
            'readonly': Eval('state') != 'draft',
            'required': Eval('type_') == 'drawn',
        }, depends=['state', 'type_'])
    check_received = fields.Many2One('checkbook.check.received',
        'Cheque recibido',
        domain=[
            If(Eval('state', '').in_(['draft', 'confirm']),
               ('state', '=', 'done'), ('state', '=', 'return'))
        ], states={
            'invisible': Eval('type_') != 'received',
            'readonly': Eval('state') != 'draft',
            'required': Eval('type_') == 'received',
        }, depends=['state', 'type_'])
    reason_return = fields.Many2One('checkbook.check.returned.reason.type',
        'Motivo de devolución', states=_STATES, depends=_DEPENDS, required=True)
    observations = fields.Text('Observaciones', states=_STATES,
        depends=_DEPENDS)
    return_date = fields.Date('Fecha de devolución',
        states={
            'readonly': _STATES['readonly'],
            'required': True
        }, depends=_DEPENDS)
    apply_sanction = fields.Boolean('Aplicar sanción', states=_STATES,
        depends=_DEPENDS)
    amount_sanction = fields.Numeric('Monto de sanción',
        domain=[
            If(Bool(Eval('apply_sanction')), ('amount_sanction', '>', 0), ())
        ], states={
            'invisible': ~Bool(Eval('apply_sanction')),
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('apply_sanction'))
        }, depends=['state', 'apply_sanction'], digits=DIGITS)
    responsable = fields.Function(
        fields.Many2One('company.employee', 'Responsable'), 'get_responsable')
    state = fields.Selection(CHECK_RETURNED_STATES, 'Estado',
        states={
            'readonly': True,
            'required': True
        })
    state_translated = state.translated('state')

    # Entry note
    sanction_state = fields.Function(
        fields.Selection(CHECK_RETURNED_SANCTION_STATES, 'Estado de sanción'),
        'get_sanction_payments_info', searcher='search_sanction_state')
    sanction_state_translated = sanction_state.translated('sanction_state')
    amount_sanction_pending = fields.Function(
        fields.Numeric('Monto pendiente', digits=DIGITS),
        'get_sanction_payments_info')
    entry_notes = fields.Function(
        fields.One2Many('treasury.entry.note', None, 'Notas de ingreso'),
        'get_sanction_payments_info')

    # Info
    transmitter = fields.Function(
        fields.Many2One('party.party', 'Emisor'),
        'get_checks_info')
    beneficiary = fields.Function(
        fields.Many2One('party.party', 'Beneficiario'),
        'get_checks_info')
    amount = fields.Function(
        fields.Numeric('Monto', digits=DIGITS),
        'get_checks_info')

    # Audit
    created_by = employee_field('Creado por')
    created_date = date_field('Se creó el')
    confirmed_by = employee_field('Confirmado por')
    confirmed_date = date_field('Se confirmó el')
    done_by = employee_field('Aprobado por')
    done_date = date_field('Se aprobó el')

    @classmethod
    def view_attributes(cls):
        return super(CheckReturned, cls).view_attributes() + [(
            '//page[@id="page_entry_notes"]', 'states', {
                'invisible': ~(Bool(Eval('apply_sanction')) &
                        (Eval('state') == 'done')), })
        ]

    @classmethod
    def __setup__(cls):
        super(CheckReturned, cls).__setup__()
        cls._error_messages.update({})

        cls._transitions |= set((
            ('draft', 'confirm'),
            ('confirm', 'draft'),
            ('confirm', 'done'),
        ))

        cls._buttons.update({
            'draft': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-undo',
                'depends': ['state']
            },
            'confirm': {
                'invisible': ~(Eval('state').in_(['draft'])),
                'icon': 'tryton-forward',
                'depends': ['state'],
            },
            'done': {
                'invisible': ~(Eval('state').in_(['confirm'])),
                'icon': 'tryton-ok',
                'depends': ['state'],
            },
        })
        cls._order.insert(0, ('return_date', 'DESC'))
        cls._order.insert(1, ('type_', 'ASC'))
        cls._order.insert(2, ('check_drawn', 'ASC'))

    @staticmethod
    def default_type_():
        return 'drawn'

    @staticmethod
    def default_return_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_created_by():
        employee_id = Transaction().context.get('employee', None)
        return employee_id

    @staticmethod
    def default_created_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    @set_employee('confirmed_by')
    @set_date('confirmed_date')
    def confirm(cls, checks):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    @set_employee('done_by')
    @set_date('done_date')
    def done(cls, checks):
        cls.manage_checks_returns(checks)

    @fields.depends('apply_sanction', 'amount_sanction')
    def on_change_apply_sanction(self):
        if not self.apply_sanction:
            self.amount_sanction = None

    @fields.depends('reason_return', 'apply_sanction', 'amount_sanction',
        'type_', 'check_drawn', 'check_received')
    def on_change_reason_return(self):
        self.manage_sanction()

    @fields.depends('reason_return', 'apply_sanction', 'amount_sanction',
        'type_', 'check_drawn', 'check_received', 'transmitter', 'beneficiary',
        'amount', 'responsable')
    def on_change_check_drawn(self):
        self.manage_sanction()
        if self.check_drawn:
            info = CheckReturned.get_check_info(self)
            self.transmitter = info['transmitter']
            self.beneficiary = info['beneficiary']
            self.amount = info['amount']
            self.responsable = self.check_drawn.checkbook.responsable

    @fields.depends('reason_return', 'apply_sanction', 'amount_sanction',
        'type_', 'check_drawn', 'check_received', 'transmitter', 'beneficiary',
        'amount', 'responsable')
    def on_change_check_received(self):
        self.manage_sanction()
        if self.check_received:
            info = CheckReturned.get_check_info(self)
            self.transmitter = info['transmitter']
            self.beneficiary = info['beneficiary']
            self.amount = info['amount']
            self.responsable = self.check_received.responsable

    @classmethod
    def get_checks_info(cls, checks_returned, names=None):
        result_transmitter = defaultdict(lambda: None)
        result_beneficiary = defaultdict(lambda: None)
        result_amount = defaultdict(lambda: None)
        for cr in checks_returned:
            info = cls.get_check_info(cr)
            result_transmitter[cr.id] = info['transmitter']
            result_beneficiary[cr.id] = info['beneficiary']
            result_amount[cr.id] = info['amount']
        return {
            'transmitter': result_transmitter,
            'beneficiary': result_beneficiary,
            'amount': result_amount,
        }

    @classmethod
    def get_check_info(cls, check_returned):
        transmitter = None
        beneficiary = None
        amount = None
        check = None

        if check_returned.type_ == 'drawn':
            check = check_returned.check_drawn
        elif check_returned.type_ == 'received':
            check = check_returned.check_received

        if check:
            transmitter = check.transmitter
            beneficiary = check.beneficiary
            amount = check.amount
        return {
            'transmitter': transmitter.id,
            'beneficiary': beneficiary.id,
            'amount': amount,
        }

    @classmethod
    def get_sanction_payments_info(cls, checks_returned, names=None):
        pool = Pool()
        cursor = Transaction().connection.cursor()

        EntryNote = pool.get('treasury.entry.note')
        EntryNotePaymentMethod = pool.get('treasury.entry.note.payment.methods')
        tbl_entry_note = EntryNote.__table__()
        tbl_entry_payments = EntryNotePaymentMethod.__table__()
        tbl_check_returned = cls.__table__()

        ids = [cr.id for cr in checks_returned]
        result_entry_notes = defaultdict(lambda: [])
        result_sanction_states = defaultdict(lambda: 'no_sanction')
        result_payments = defaultdict(lambda: 0)
        result_pending = defaultdict(lambda: 0)

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(tbl_check_returned.id, sub_ids)
            query = tbl_check_returned.join(tbl_entry_note,
                condition=(
                    tbl_entry_note.origin ==
                        Concat('checkbook.check.returned,',
                            tbl_check_returned.id))
            ).join(tbl_entry_payments,
               condition=(
                    tbl_entry_note.id == tbl_entry_payments.entry_note)
            ).select(
                tbl_check_returned.id.as_('check_returned'),
                tbl_entry_note.id.as_('entry_note'),
                tbl_entry_payments.amount,
                where=((red_sql) &
                       (tbl_entry_note.state == 'done'))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                cr_id = row['check_returned']
                en_id = row['entry_note']
                payment = row['amount']
                if en_id not in result_entry_notes[cr_id]:
                    result_entry_notes[cr_id].append(en_id)
                result_payments[cr_id] += payment

        for cr in checks_returned:
            if cr.apply_sanction and cr.amount_sanction:
                paid = result_payments[cr.id]
                if cr.amount_sanction <= paid:
                    result_sanction_states[cr.id] = 'paid'
                else:
                    result_sanction_states[cr.id] = 'pending'
                    result_pending[cr.id] = (
                        cr.amount_sanction - result_payments[cr.id])
        return {
            'entry_notes': result_entry_notes,
            'sanction_state': result_sanction_states,
            'amount_sanction_pending': result_pending,
        }

    @classmethod
    def get_responsable(cls, checks_returned, names=None):
        result = defaultdict(lambda: None)
        for cr in checks_returned:
            responsable = None
            if cr.type_ == 'drawn':
                if cr.check_drawn and cr.check_drawn.checkbook:
                    responsable = cr.check_drawn.checkbook.responsable
            elif cr.type_ == 'received':
                if cr.check_received:
                    responsable = cr.check_received.responsable
            if responsable:
                result[cr.id] = responsable.id
        return {
            'responsable': result
        }

    def manage_sanction(self):
        if self.reason_return:
            apply_sanction = self.reason_return.apply_sanction
            percentage = self.reason_return.percentage_sanction
            if apply_sanction:
                check = None
                if self.type_ == 'drawn':
                    check = self.check_drawn
                elif self.type_ == 'received':
                    check = self.check_received

                amount = check.amount if check and check.amount else ZERO
                self.apply_sanction = True
                self.amount_sanction = amount * percentage / Decimal('100')
            else:
                self.apply_sanction = False
                self.amount_sanction = None
        else:
            self.apply_sanction = False
            self.amount_sanction = None

    @classmethod
    def manage_checks_returns(cls, checks_return):
        pool = Pool()
        CheckDrawn = pool.get('checkbook.check.drawn')
        CheckReceived = pool.get('checkbook.check.received')

        to_save_drawn = []
        to_save_received = []

        for cr in checks_return:
            if cr.type_ == 'drawn':
                check = cr.check_drawn
                if check:
                    check.state = 'return'
                    to_save_drawn.append(check)
            elif cr.type_ == 'received':
                check = cr.check_received
                if check:
                    check.state = 'return'
                    to_save_received.append(check)
        CheckDrawn.save(to_save_drawn)
        CheckReceived.save(to_save_received)

    @classmethod
    def search_sanction_state(cls, name, clause):
        ids = []
        checks_returned = cls.search([
            ('state', '=', 'done'),
            ('apply_sanction', '=', True),
            ('amount_sanction', '>', 0),
        ])
        data = cls.get_sanction_payments_info(checks_returned)
        sanction_states = data['sanction_state']
        for id, state in sanction_states.items():
            if state == 'pending':
                ids.append(id)
        return [('id', 'in', ids)]

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        number = -100000
        try:
            number = int(clause[2].replace('%', ''))
        except Exception:
            pass

        domain = [bool_op,
            ('type_',) + ('=', number),
            ('check_drawn',) + tuple(clause[1:]),
            ('check_received',) + tuple(clause[1:]),
            ('reason_return',) + tuple(clause[1:]),
            ('state',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        if self.type_:
            name = ''
            if self.type_ == 'drawn':
                check = self.check_drawn
                num = check.number
                bank = check.checkbook.bank_account.bank.party.name.upper()
                state = self.state_translated.upper()
                name = 'CHEQUE EMITIDO N°%s [%s] [%s]' % (num, bank, state)
            elif self.type_ == 'received':
                check = self.check_received
                num = check.number
                bank = check.bank.party.name.upper()
                state = self.state_translated.upper()
                name = 'CHEQUE RECIBIDO N°%s [%s] [%s]' % (num, bank, state)
            if name:
                return name
        return self.id


class CheckReceivedType(ModelView, ModelSQL):
    'Check Received Type'
    __name__ = 'checkbook.check.received.type'
    _history = True

    name = fields.Char('Nombre', required=True)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        domain = [bool_op,
            ('name',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        if self.name:
            return self.name
        return self.id


class CheckReturnedReasonType(ModelView, ModelSQL):
    'Check Returned Reason Type'
    __name__ = 'checkbook.check.returned.reason.type'
    _history = True

    name = fields.Char('Nombre', required=True)
    apply_sanction = fields.Boolean('Aplicar sanción')
    percentage_sanction = fields.Numeric('Porcentaje de sanción',
        domain=[
            If(Bool(Eval('apply_sanction')),
               ('percentage_sanction', '>', 0), ())
        ], states={
            'invisible': ~Bool(Eval('apply_sanction')),
            'required': Bool(Eval('apply_sanction'))
        }, depends=['state', 'apply_sanction'], digits=DIGITS)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'

        domain = [bool_op,
            ('name',) + tuple(clause[1:]),
        ]
        return domain

    def get_rec_name(self, name):
        if self.name:
            return self.name
        return self.id


class CheckConsultingContext(ModelView):
    'Check Consulting Context'
    __name__ = 'checkbook.check.consulting.context'

    type_ = fields.Selection(CHECK_RETURNED_TYPES + [(None, '')], 'Tipo')
    bank = fields.Many2One('bank', 'Banco')
    transmitter = fields.Many2One('party.party', 'Emisor')
    beneficiary = fields.Many2One('party.party', 'Beneficiario')
    state = fields.Selection(CHECK_STATES, 'Estado')


class CheckConsulting(ModelView, ModelSQL):
    'Check Consulting'
    __name__ = 'checkbook.check.consulting'

    _STATES = {
        'readonly': True
    }

    type_ = fields.Selection(CHECK_RETURNED_TYPES, 'Tipo', states=_STATES)
    type_translated = type_.translated('type_')
    checkbook = fields.Many2One('checkbook.checkbook', 'Chequera',
        states=_STATES)
    bank = fields.Many2One('bank', 'Banco', states=_STATES)
    number = fields.Integer('Número', states=_STATES)
    check_date = fields.Date('Fecha de cheque', states=_STATES)
    transmitter = fields.Many2One('party.party', 'Emisor', states=_STATES)
    beneficiary = fields.Many2One('party.party', 'Beneficiario', states=_STATES)
    responsable = fields.Many2One('company.employee', 'Responsable',
        states=_STATES)
    amount = fields.Numeric('Monto', states=_STATES, digits=DIGITS)
    effective_date = fields.Date('Fecha de efectivización', states=_STATES)
    description = fields.Text('Descripción', states=_STATES)
    reception_type = fields.Many2One('checkbook.check.received.type',
        'Tipo de recepción', states=_STATES)
    is_postdated = fields.Boolean('Cheque posfechado?', states=_STATES,
        help='Cheque posfechado?')
    is_reconsignment = fields.Boolean('Reconsignación?', states=_STATES,
        help='Cheque reconsignado?')
    check_received = fields.Many2One('checkbook.check.returned',
        'Cheque devuelto', states=_STATES)
    state = fields.Selection(CHECK_STATES, 'Estado', states=_STATES)
    state_translated = state.translated('state')

    @classmethod
    def __setup__(cls):
        super(CheckConsulting, cls).__setup__()
        cls._order.insert(0, ('check_date', 'DESC'))
        cls._order.insert(1, ('number', 'DESC'))
        cls._order.insert(2, ('bank', 'ASC'))

    @classmethod
    def table_query(cls):
        context = Transaction().context
        pool = Pool()
        Checkbook = pool.get('checkbook.checkbook')
        CheckDrawn = pool.get('checkbook.check.drawn')
        CheckReceived = pool.get('checkbook.check.received')
        CheckReceivedType = pool.get('checkbook.check.received.type')
        Bank = pool.get('bank')
        BankAccount = pool.get('bank.account')

        tbl_checkbook = Checkbook.__table__()
        tbl_check_drawn = CheckDrawn.__table__()
        tbl_check_received = CheckReceived.__table__()
        tbl_bank_account = BankAccount.__table__()

        # Context data
        type_ = context.get('type_', None)
        bank = context.get('bank', None)
        transmitter = context.get('transmitter', None)
        beneficiary = context.get('beneficiary', None)
        state = context.get('state', None)

        # Query check drawn
        query_check_drawn = tbl_check_drawn.join(tbl_checkbook,
            condition=(tbl_checkbook.id == tbl_check_drawn.checkbook)
        ).join(tbl_bank_account,
            condition=(tbl_bank_account.id == tbl_checkbook.bank_account)
        ).select(
            Literal('drawn').as_('type_'),
            tbl_check_drawn.checkbook,
            tbl_bank_account.bank,
            tbl_check_drawn.number,
            tbl_check_drawn.check_date,
            tbl_check_drawn.transmitter,
            tbl_check_drawn.beneficiary,
            tbl_checkbook.responsable,
            tbl_check_drawn.amount,
            tbl_check_drawn.effective_date,
            Literal('').as_('description'),
            Literal(None).as_('reception_type'),
            tbl_check_drawn.is_postdated,
            tbl_check_drawn.is_reconsignment,
            tbl_check_drawn.check_received,
            tbl_check_drawn.state,
        )

        # Query check received
        query_check_received = tbl_check_received.select(
            Literal('received').as_('type_'),
            Literal(None).as_('checkbook'),
            tbl_check_received.bank,
            tbl_check_received.number,
            tbl_check_received.check_date,
            tbl_check_received.transmitter,
            tbl_check_received.beneficiary,
            tbl_check_received.responsable,
            tbl_check_received.amount,
            tbl_check_received.effective_date,
            tbl_check_received.description,
            tbl_check_received.reception_type,
            tbl_check_received.is_postdated,
            Literal(None).as_('is_reconsignment'),
            Literal(None).as_('check_received'),
            tbl_check_received.state,
        )

        # Query union
        query_union = Union(query_check_drawn, query_check_received, all_=True)

        # Where clausula
        where = Literal(True)
        if type_:
            where &= (query_union.type_ == type_)
        if bank:
            where &= (query_union.bank == bank)
        if transmitter:
            where &= (query_union.transmitter == transmitter)
        if beneficiary:
            where &= (query_union.beneficiary == beneficiary)
        if state:
            where &= (query_union.state == state)

        # Final query
        query = query_union.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query_union.type_,
            query_union.checkbook,
            query_union.bank,
            query_union.number,
            query_union.check_date,
            query_union.transmitter,
            query_union.beneficiary,
            query_union.responsable,
            query_union.amount,
            query_union.effective_date,
            query_union.description,
            query_union.reception_type,
            query_union.is_postdated,
            query_union.is_reconsignment,
            query_union.check_received,
            query_union.state,
            where=where
        )
        return query