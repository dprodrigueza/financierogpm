from trytond.model import fields
from trytond.model import ModelView, ModelSQL, Workflow
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['FinancialInvestmentType','FinancialInvestment',
           'FinancialInvestmentLine']

class FinancialInvestmentType(ModelSQL, ModelView):
    ' Financial Investments Type'
    __name__ = 'financial.investment.type'
    name = fields.Char('Nombre', required=True)

    @classmethod
    def __setup__(cls):
        super(FinancialInvestmentType, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]

class FinancialInvestment(Workflow, ModelSQL, ModelView):
    'Financial Investment'
    __name__ = 'financial.investment'
    _states = {
        'readonly': (Eval('state') != 'draft')
    }
    _depends = ['state']
    _history = True;
    company = fields.Many2One('company.company', 'Empresa',
        select=True, readonly=True, required=True)
    responsable = fields.Many2One('company.employee', 'Responsable',
            required=True, states=_states, depends=_depends)
    type = fields.Many2One('financial.investment.type', 'Tipo', required=True,
                           states=_states, depends=_depends)
    amount = fields.Numeric('Capital', digits=(16, 2), required=True,
                            states=_states, depends=_depends)
    interest = fields.Numeric('Tasa Interés', digits=(16, 2), required=True,
                              states=_states, depends=_depends)
    start_date = fields.Date("Fecha Inicio", required=True,
                             states=_states, depends=_depends)
    end_date = fields.Date("Fecha Fin", required=True,
                           states=_states, depends=_depends)
    periodicity = fields.Selection([
        ('monthly', 'Mensual'),
        ('quarterly', 'Trimestral'),
        ('biyearly', 'Semestral'),
        ('yearly', 'Anual'),
    ],'Periodicidad', required=True,
        states=_states, depends=_depends)
    description = fields.Char('Descripción', required=True,
                            states=_states, depends=_depends)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('confirmed', 'Confirmado'),
        ('open', 'Abierto'),
        ('finished', 'Finalizado'),
        ('canceled', 'Cancelado'),], 'Estado', readonly=True, select=True,
        required=True)
    detail = fields.One2Many('financial.investment.line', 'investment',
                             'Detalle',required=True,  states={
            'readonly': ~(Eval('state').in_(['draft','open'])),
        }, depends=['state'])

    @classmethod
    def __setup__(cls):
        super(FinancialInvestment, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'open'),
            ('open', 'finished'),
            ('confirmed','draft'),
            ('draft', 'canceled'),
            ('confirmed', 'canceled'),
            ('open', 'canceled'),
        ))
        cls._buttons.update(
            {
                'confirm': {
                    'invisible':
                        ~(Eval('state').in_(['draft']))
                },

                'draft': {
                    'invisible': (
                        ~Eval('state').in_(['confirmed'])
                    )
                },

                'open': {
                    'invisible': (
                        ~Eval('state').in_(['confirmed'])
                    )
                },

                'finish': {
                    'invisible': (
                        ~Eval('state').in_(['open'])
                    )
                },

                'cancel': {
                    'invisible': (
                        ~Eval('state').in_(['draft', 'confirmed', 'open'])
                    )
                },
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_amount():
        return 0.00

    @staticmethod
    def default_interest():
        return 0.00

    @staticmethod
    def default_periodicity():
        return 'yearly'

    @classmethod
    def default_start_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_end_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, financial_investments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, financial_investments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, financial_investments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, financial_investments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, financial_investments):
        pass


class FinancialInvestmentLine(ModelSQL, ModelView):
    'Financial Investment Line'
    __name__ = 'financial.investment.line'
    _history = True;
    _states = {
        'readonly':
            ~Eval('_parent_investment', {}).get('state').in_(
                ['draft','open'])

    }
    _depends = ['investment']
    investment = fields.Many2One('financial.investment', 'Detalle',
        required=True, readonly=True,ondelete='CASCADE')
    date = fields.Date('Fecha',required=True, states=_states, depends=_depends)
    amount = fields.Numeric('Capital', digits=(16, 2),required=True,
                            states=_states, depends=_depends)
    interest= fields.Numeric('Interes Generado', digits=(16, 2), required=True,
                             states=_states, depends=_depends)
    state= fields.Selection(
        [
            ('paymented', 'Cobrado'),
            ('pending', 'Pendiente'),
        ], 'Estado', required=True, states=_states, depends=_depends)

    @classmethod
    def __setup__(cls):
        super(FinancialInvestmentLine, cls).__setup__()
        cls._order = [
            ('date', 'ASC'),
        ]
