from io import StringIO
from collections import defaultdict
import hashlib
import time
import zipfile
from sql import Null
try:
    COMPRESSION = zipfile.ZIP_DEFLATED
except Exception:
    COMPRESSION = zipfile.ZIP_STORED
from datetime import datetime
from decimal import Decimal

from trytond.wizard import (Wizard, StateView, Button, StateAction,
    StateTransition)
from trytond.model import fields, Workflow, ModelSQL, ModelView, Unique
from trytond.pyson import Eval, If, Bool, Id
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.hr_ec.loan import register_payment
from trytond.modules.public_ec.account import AutoMove
from trytond.modules.public_ec.account import RelatedMove
from trytond.modules.company.model import (CompanyMultiValueMixin,
    CompanyValueMixin)

_STATES = {
    'readonly': Eval('state') != 'draft'
    }
_STATES_LINE = {
    'readonly': True
}
_DEPENDS = ['state']

_DEFAULT_PAYMENT_METHOD = "-1"

__all__ = [
    'AccountPaymentSPI', 'AccountPaymentRequest',
    'PaymenAccountPaymentSPIConcept', 'Company',
    'PettyCash', 'PettyCashSequence', 'PettyCashRegistry',
    'PettyCashMoveStart', 'PettyCashMoveWizard',
    'PettyCashRegistryLine', 'EntryNoteType', 'EntryNotePaymentMethods',
    'EntryNote', 'EntryNotePayPoint', 'EntryNotePayMethod',
]


class AccountPaymentSPI(Workflow, ModelSQL, ModelView):
    'Account Payment SPI'
    __name__ = 'treasury.account.payment.spi'

    STATES = ['paid']

    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('confirmed', 'Confirmado'),
            ('generated', 'Generado'),
            ('paid', 'Pagado'),
            ('partially_paid', 'Pagado parcialmente'),
            ('cancel', 'Cancelado'),
        ], 'State', readonly=True)
    name = fields.Char('Descrición', required=True, states=_STATES,
        depends=_DEPENDS)
    number = fields.Char('Número', size=None, readonly=True, select=True)
    payment_date = fields.DateTime(
        'Fecha de pago', required=True, states=_STATES, depends=_DEPENDS)
    journal = fields.Many2One('bank.account', 'Cuenta bancaria', required=True,
        domain=[
            ('owners', 'where', [('id', '=', Eval('company_party'))])
            ], states=_STATES, depends=_DEPENDS + ['company_party'])
    company = fields.Many2One('company.company', 'Empresa', required=True,
        states={
            'readonly': Eval('state') != 'draft',
            },
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        depends=['state'])
    company_spi_name = fields.Function(
        fields.Char('Nombre asignado en el B. Central'),
        'on_change_with_company_spi_name')
    company_spi_city = fields.Function(
        fields.Char('Ciudad (SPI)'),
        'on_change_with_company_spi_city')
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal ',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True, states=_STATES)
    company_party = fields.Function(
        fields.Many2One('party.party', 'Empresa de tercero'),
        'on_change_with_company_party')
    is_direct = fields.Boolean('Transferencia directa', states=_STATES,
        depends=_DEPENDS)
    aux_date = fields.Date('Otra fecha de pago', states=_STATES,
        depends=_DEPENDS)
    zip_file = fields.Binary('Archivo SPI (.zip)', states=_STATES,
        depends=_DEPENDS + ['zip_file_name'],
        filename='zip_file_name', file_id="zip_path")
    zip_file_name = fields.Char('Zip SPI File Name')
    zip_path = fields.Char('Path location ZIP SPI')
    xls_file = fields.Binary('Archivo SPI (.xls)', states=_STATES,
        depends=_DEPENDS + ['xls_file_name'],
        filename='xls_file_name', file_id='xls_path')
    xls_file_name = fields.Char('XLS SPI File Name')
    xls_path = fields.Char('Path location XLS SPI')
    txt_file = fields.Binary('Archivo SPI (.txt)', states=_STATES,
        depends=_DEPENDS + ['txt_file_name'],
        filename='txt_file_name', file_id='txt_path')
    txt_file_name = fields.Char('TXT SPI File Name ')
    txt_path = fields.Char('Path location TXT SPI')
    dig = fields.Char('dig', size=256)
    spi_summary = fields.Function(fields.One2Many('bank', None, 'Resumen Pago',
        context={'spi': Eval('id', -1)}), 'get_summary')
    spi_cancel_summary = fields.Function(fields.One2Many('bank', None,
        'Resumen Negados', context={'spi': Eval('id', -1)}), 'get_cancel_summary')
    total = fields.Function(fields.Numeric('Total'),
        'on_change_with_total')
    total_cancel = fields.Function(fields.Numeric('Total Negados'),
        'on_change_with_total_cancel')
    total_to_pay = fields.Function(fields.Numeric('Total a pagar'),
        'on_change_with_total_to_pay')
    md5 = fields.Char('MD5')
    egress_concept = fields.Many2One('treasury.account.payment.spi.concept',
        'Concepto de egreso', help="Concepto de egreso por defecto",
        states={
            'readonly': ((_STATES['readonly']) |
                ~(Eval('payment_request') & Eval('lines'))),
            'required': True,
        }, depends=_DEPENDS + ['payment_request', 'lines'])
    payment_request = fields.Many2Many(
        'treasury.account.payment.request.header', 'spi', 'request',
        'Solicitud de pago',
        domain=[
            ('state', '=', 'approved'),
            ('bank_debit', '=', False),
            ['OR',
             ('spi', '=', None),
             ('spi.state', 'not in', ['paid', 'generated', 'partially_paid']),
             ('id', 'in', Eval('payment_request')),
             ]
        ],
        states={
            'readonly': _STATES['readonly'],
            'required': True,
        }, depends=['state'])
    lines = fields.One2Many(
        'treasury.account.payment.request.line.detail', 'spi',
        'Líneas de Solicitud',
        add_remove=[
            ('request', 'in', Eval('payment_request'))
        ], depends=['payment_request'], states={
            'required': True,
            'readonly': _STATES['readonly'] | ~Eval('payment_request'),
        })

    @classmethod
    def __setup__(cls):
        super(AccountPaymentSPI, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'generated'),
            ('generated', 'paid'),
            ('generated', 'partially_paid'),
            ('draft', 'cancel'),
            ('confirmed', 'cancel'),
            ('generated', 'cancel'),
            ('confirmed', 'draft'),
        ))
        cls._error_messages.update({
            'delete_draft': 'SPI payment "%s" must be in draft to be deleted.',
            'no_identifier': 'El tercero %(party)s no tiene identificador',
        })
        cls._buttons.update({
            'cancel_spi': {
                'invisible': ~Eval('state').in_(['draft', 'confirmed',
                    'generated']),
                'depends': ['state']
                },
            'set_draft_spi': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'depends': ['state']
                },
            'confirm_spi': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
                },
            'generate_spi': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'depends': ['state']
                },
            'pay_spi': {
                'invisible': ~Eval('state').in_(['generated']),
                'depends': ['state']
            },
            'apply_concept': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
        })
        cls._error_messages.update({
                'no_egress_concept': ('Falta concepto de egreso en la linea\n'
                'con tercero %(party)s y valor %(amount)s'),
                'no_user_bank': ('El empleado "%(user)s", no tiene configurado'
                    ' una cuenta bancaria.'),
                'no_bank_bic': ('El banco "%(bank)s" no tiene configurado el'
                    ' código BIC.'),
                'no_company_spi_name': ('La insitución no tiene un nombre '
                    'asignado por el Banco Central Configurado '),
                'no_company_spi_city': ('La insitución no tiene configurada '
                    'la ciudad para la transferencia SPI'),
                'not_sequence': ('No existe una secuencia SPI configurada, '
                    'en el ejercicio fiscal.'),
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_fiscalyear(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        Fiscalyear = pool.get('account.fiscalyear')
        today = Date.today()
        fiscalyear = Fiscalyear.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('state', '=', 'open'),
        ])
        if fiscalyear:
            return fiscalyear[0].id
        return None

    @staticmethod
    def default_payment_date():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_total_cancel():
        return 0

    @classmethod
    def get_summary(cls, spis, names):
        result = defaultdict(lambda: [])
        for spi in spis:
            for line in spi.lines:
                if line.state not in ['cancel', 'rejected']:
                    if line.bank and line.bank.id not in result[spi.id]:
                        result[spi.id].append(line.bank.id)
        return {'spi_summary': result}

    @classmethod
    def get_cancel_summary(cls, spis, names):
        result = defaultdict(lambda: [])
        for spi in spis:
            for line in spi.lines:
                if line.state in ['cancel', 'rejected']:
                    if line.bank and line.bank.id not in result[spi.id]:
                        result[spi.id].append(line.bank.id)
        return {'spi_cancel_summary': result}

    @fields.depends('lines')
    def on_change_with_total(self, name=None):
        if self.lines:
            result = 0
            for line in self.lines:
                if line.amount:
                    result += line.amount
            return result
        return 0

    @fields.depends('lines')
    def on_change_with_total_cancel(self, name=None):
        if self.lines:
            result = 0
            for line in self.lines:
                if line.state in ['cancel', 'rejected']:
                    if line.amount:
                        result += line.amount
            return result
        return 0

    @fields.depends('total_cancel', 'total')
    def on_change_with_total_to_pay(self, name=None):
        if self.total_cancel > 0 and self.total > 0:
            return self.total - self.total_cancel
        elif self.total_cancel == 0 and self.total:
            return self.total
        return 0

    @fields.depends('company')
    def on_change_with_company_spi_name(self, name=None):
        if self.company and self.company.spi_name:
            return self.company.spi_name
        return ''

    @fields.depends('company')
    def on_change_with_company_spi_city(self, name=None):
        if self.company and self.company.spi_city:
            return self.company.spi_city
        return ''

    def check_lines(self):
        for line in self.lines:
            if not line.party.tax_identifier:
                self.raise_user_error('no_identifier', {
                    'party': line.party.rec_name,
                })
        pass

    def generate_provider(self, path='SPI-SP_LB.TXT'):
        pool = Pool()

        #   ALONE TXT
        ReportTxt = pool.get('treasury.account.payment.spi.txt', type='report')
        data = {
            'model': self.__name__
        }
        ext, content, _, title = ReportTxt.execute([self.id], data)
        self.txt_file_name = "%s.%s" % (title, ext)
        self.txt_file = content.encode('utf-8')
        #   ZIP TXT
        ReportZipTxt = pool.get('treasury.account.payment.spi.zip.txt',
            type='report')
        ext_zip_txt, zip_txt_content, _, title_zip_txt = \
            ReportZipTxt.execute([self.id], data)
        ReportXls = pool.get('treasury.account.payment.spi.ods', type='report')
        ext, content, _, title = ReportXls.execute([self.id], data)
        self.xls_file_name = "%s.%s" % (title, ext)
        self.xls_file = content
        name = "%s.TXT" % ("SPI-SP_LB")
        digest_name = 'SPI-SP-LB.MD5'
        try:
            buf = StringIO.StringIO()
        except AttributeError:
            buf = StringIO()
        buf.write(zip_txt_content)
        title_zip_txt = title_zip_txt.replace(' ', '_')
        digest = '%s  %s\n' % (
            hashlib.md5(buf.getvalue().encode('utf-8')).hexdigest(),
            path)
        self.md5 = digest.split(' ')[0]
        self.save()
        file_digest = open(digest_name, 'w')
        file_digest.write(digest)
        file_digest.close()
        file_spi = open(name, 'w')
        file_spi.write(buf.getvalue())
        file_spi.close()
        zf_name = 'spi-sp_%s' % time.strftime('%Y-%m-%d')
        zf = zipfile.ZipFile(zf_name, mode='w')
        try:
            zf.write(name, compress_type=COMPRESSION)
            zf.write(digest_name)
            zf.filelist[1].create_system = 0
            zf.filelist[0].create_system = 0
        finally:
            zf.close()
        zf_tmp = open(zf_name, 'rb')
        try:
            zf_buf = StringIO.StringIO()
        except AttributeError:
            zf_buf = StringIO()
        zip_bytes = zf_tmp.read()
        buf.close()
        zf_buf.close()
        self.zip_file_name = "%s.%s" % (zf_name, 'ZIP')
        self.zip_file = fields.Binary.cast(zip_bytes)

    @fields.depends('company')
    def on_change_with_company_party(self, name=None):
        if self.company:
            return self.company.party.id

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm_spi(cls, transfers):
        for transfer in transfers:
            if not transfer.company.spi_name:
                cls.raise_user_error('no_company_spi_name', {})
            if not transfer.company.spi_city:
                cls.raise_user_error('no_company_spi_city', {})
            for line in transfer.lines:
                if not line.egress_concept:
                    cls.raise_user_error('no_egress_concept', {
                            'party': line.party.name,
                            'amount': line.amount,
                            })
                if not line.bank:
                    cls.raise_user_error('no_user_bank', {
                            'user': line.party.name
                            })
                if not line.bank.bic:
                    cls.raise_user_error('no_bank_bic', {
                                'bank': line.bank.party.name
                                })
            cls.save(transfers)

    @classmethod
    @ModelView.button
    @Workflow.transition('generated')
    def generate_spi(cls, transfers):
        cls.set_spi_number(transfers)
        for transfer in transfers:
            transfer.payment_date = datetime.now()
            transfer.generate_provider()
            for line in transfer.lines:
                line.state = 'paid'
                line.save()
        cls.save(transfers)

    @classmethod
    @ModelView.button
    @Workflow.transition('paid')
    def pay_spi(cls, transfers):
        pool = Pool()
        Loan = pool.get('company.employee.loan')
        Travel_expense = pool.get('company.travel.expense')
        types = ['account.move', 'account.invoice', 'account.move.posted']
        MoveLine = pool.get('account.move.line')
        partial = False
        lines_to_save = []
        for transfer in transfers:
            for line in transfer.lines:
                if line.request.type == 'company.employee.loan':
                    Loan.open([line.origin])
                elif line.request.type == 'payslip.advance':
                    Loan.open([line.origin.loan])
                elif line.request.type == 'company.travel.expense':
                   Travel_expense.authorize([line.origin])
                if line.state in ['rejected', 'cancel']:
                    partial = True
                if (str(line.origin).startswith('account.move.line')
                        and line.state in ['rejected', 'cancel']):
                    move_line = MoveLine.browse([line.origin.id])[0]
                    move_line.payment_origin = Null
                    lines_to_save.append(move_line)
                MoveLine.save(lines_to_save)
            if partial:
                transfer.state = 'partially_paid'
                transfer.save()

        """if self.spi.state == 'paid' and self.type == 'company.employee.loan':
            for line in self.spi.lines:
                loan = Loan.browse([line.origin.id])[0]
                loans_to_open.append(loan)
            Loan.open(loans_to_open)
        """
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def set_draft_spi(cls, transfers):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel_spi(cls, transfers):
        Request = Pool().get('treasury.account.payment.request')
        for transfer in transfers:
            for line in transfer.lines:
                line.state = 'pending'
                line.save()
                Request.remove_move_line_payment_origin(line.request)
        cls.save(transfers)

    @classmethod
    @ModelView.button
    def apply_concept(cls, transfers):
        for transfer in transfers:
            if transfer.lines and transfer.egress_concept:
                for line in transfer.lines:
                    line.egress_concept = transfer.egress_concept
                    line.save()

    @classmethod
    def set_spi_number(cls, transfers):
        '''
        Fill the spi_number field with the spi sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for transfer in transfers:
            if transfer.number:
                continue
            if not transfer.fiscalyear.transfer_sequence:
                cls.raise_user_error('not_sequence', {})
            transfer.number = Sequence.get_id(
                    transfer.fiscalyear.transfer_sequence.id)
            transfer.save()
        cls.save(transfers)

    @classmethod
    def remove_move_line_payment_origin(cls, transfer):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        for line in transfer.lines:
            move_line = MoveLine.browse([line.origin.id])[0]
            move_line.payment_origin = None
            MoveLine.save([move_line])

    @classmethod
    def delete(cls, transfer):
        for spi in transfer:
            if spi.state != 'draft':
                cls.raise_user_error('delete_draft', spi.rec_name)
        return super(AccountPaymentSPI, cls).delete(transfer)

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('number',) + tuple(clause[1:]),
                  ('name',) + tuple(clause[1:]),
                  ]
        return domain

    def get_rec_name(self, name):
        number = ''
        if self.number:
            number = f"{self.number} "
        return f"{number}{self.name}"


class AccountPaymentRequest(ModelSQL):
    'Account Payment General Roles'
    __name__ = 'treasury.account.payment.request.header'

    spi = fields.Many2One('treasury.account.payment.spi', 'spi')
    request = fields.Many2One('treasury.account.payment.request', 'request')


class PaymenAccountPaymentSPIConcept(ModelSQL, ModelView):
    'SPI Egress Concept'
    __name__ = 'treasury.account.payment.spi.concept'

    name = fields.Char('Name', required=True)
    code = fields.Char('Egress Conept Code', required=True)

    def get_rec_name(self, name):
        return f"{self.code} - {self.name}"

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('code',) + tuple(clause[1:]),
                  ('name',) + tuple(clause[1:]),
                  ]
        return domain


class Company(metaclass=PoolMeta):
    'company_company'
    __name__ = 'company.company'

    spi_name = fields.Char('Nombre asignado en el B. Central',
        states={
            'required': True
        })
    spi_city = fields.Char('Ciudad (SPI)',
        states={
            'required': True
        })


class PettyCash(ModelView, ModelSQL, CompanyMultiValueMixin):
    'Caja chica'
    __name__ = 'treasury.petty.cash'
    _history = True

    name = fields.Char('Nombre', size=None, required=True)
    responsible = fields.Many2One('company.employee', 'Responsable',
        required=True)
    amount = fields.Numeric('Monto', digits=(16, 2), required=True)
    sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Sequence", required=True,
        domain=[
            ('code', '=', 'treasury.petty.cash'),
            ('company', 'in', [
                Eval('context', {}).get('company', -1), None]),
        ]))
    sequences = fields.One2Many(
        'treasury.petty.cash.sequence', 'petty_cash', "Secuencias")
    request = fields.Many2One('treasury.account.payment.request', 'Solicitud',
        states={'invisible': True})

    def get_rec_name(self, name):
        return '%s - %s' % (self.name, self.responsible.party.name)

    @classmethod
    def default_sequence(cls, **pattern):
        return None


class PettyCashSequence(ModelSQL, CompanyValueMixin):
    "Secuencia - Caja chica"
    __name__ = 'treasury.petty.cash.sequence'
    petty_cash = fields.Many2One(
        'treasury.petty.cash', "Caja chica", ondelete='CASCADE', select=True)
    sequence = fields.Many2One(
        'ir.sequence', "Sequence",
        domain=[
            ('code', '=', 'treasury.petty.cash'),
            ('company', 'in', [Eval('company', -1), None]),
            ],
        depends=['company'])


class PettyCashRegistry(Workflow, ModelView, ModelSQL):
    'Registros de caja chica'
    __name__ = 'treasury.petty.cash.registry'
    _history = True

    company = fields.Many2One('company.company', 'Compañía', required=True,
        readonly=True,
        states={
            'invisible': True,
        },
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        depends=['state'])
    number = fields.Char('Número',
        states={
            'readonly': True
        }
    )
    petty_cash_invoice_receiver = fields.Selection(
        [
            (None, ''),
            ('custodian', 'Custodio'),
            ('company', 'Empresa'),
        ], "Receptor de facturas", readonly=True,
        states={
            'invisible': True,
        })
    name = fields.Char('Nombre', states=_STATES, depends=_DEPENDS,
        required=True)
    state = fields.Selection(
        [
            ('draft', 'Borrador'),
            ('open', 'Abierta'),
            ('validated', 'Validada'),
            ('closed', 'Cerrada')
        ], 'Estado', readonly=True)
    petty_cash = fields.Many2One('treasury.petty.cash', 'Caja chica',
        states=_STATES,
        domain=[
            'OR',
            ('responsible.id', '=', Eval('context', {}).get('employee', -1)),
            ('id', '=', Eval('petty_cash'))
        ],
        depends=_DEPENDS, required=True)
    petty_cash_party = fields.Function(
        fields.Many2One('party.party', 'Tercero caja chica',
        states={
            'invisible': True
        }),
        'on_change_with_petty_cash_party')
    date = fields.Date('Fecha', required=True, states=_STATES, depends=_DEPENDS)
    period = fields.Many2One('account.period', 'Periodo', required=True,
        domain=[
            ('company', '=', Eval('company', -1)),
            If(Eval('state') == 'closed',
                ('id', '=', Eval('period')),
                ('state', '=', 'open'),
            )
        ],
        states=_STATES, depends=_DEPENDS + ['company'], select=True)
    lines = fields.One2Many('treasury.petty.cash.registry.line',
        'petty_cash_registry', 'Registros',
        states={
            'invisible': Eval('petty_cash_invoice_receiver') == 'company',
            'readonly': ~(Eval('state') == 'open')
        }, depends=['state'], order=[('date', 'ASC')])
    invoices = fields.One2Many('account.invoice', 'petty_cash_registry',
        'Facturas', readonly=True,
        states={
            'invisible': Eval('petty_cash_invoice_receiver') != 'company',
        }
    )
    move = fields.Many2One('account.move', 'Asiento', readonly=True,
        domain=[
            ('company', '=', Eval('company', -1)),
        ],
        depends=['company'])
    total = fields.Function(fields.Numeric('Total', digits=(16, 2)),
        'on_change_with_total')

    balance = fields.Function(fields.Numeric('Saldo', digits=(16, 2)),
        'on_change_with_balance')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_number():
        return '/'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_period():
        pool = Pool()
        Period = pool.get('account.period')
        Date = pool.get('ir.date')
        periods = Period.search([
            ('start_date', '<=', Date.today()),
            ('end_date', '>=', Date.today())
        ])
        if periods:
            return periods[0].id
        return None

    @staticmethod
    def default_petty_cash_invoice_receiver():
        pool = Pool()
        Config = pool.get('treasury.petty.cash.configuration')
        config = Config(1)
        return config.get_multivalue('invoice_receiver')

    @staticmethod
    def default_total():
        return Decimal('0.00')

    @classmethod
    def __setup__(cls):
        super(PettyCashRegistry, cls).__setup__()
        cls._error_messages.update({
            'error_match_account': ('No se encontro la cuenta por pagar '
                'relacionada a la cuenta de gasto %(account)s.\n'
                'Reglas de facturas para cuentas por pagar'),
            'error_account': ('La cuenta contable en el registro %(number)s - '
                '%(description)s es obligatoria'),
            'error_amount': ('El registro de caja chica '
                 '"%(petty_cash_registry)s" tiene un monto total superior al '
                 'limite permitido en la caja chica "%(petty_cash)s".'
                 'Total: %(total)s, '
                 'Limite: %(petty_cash_amount)s'),
            'error_open_petty': ('Ya existe un registro de caja abierta para '
                 'la caja utlizada')
        })

        cls._transitions |= set((
            ('draft', 'open'),
            ('open', 'validated'),
            ('validated', 'closed'),
            ('open', 'draft'),
            ('validated', 'open'),
        ))
        cls._buttons.update({
            'open': {
                'invisible': ~(
                    (Eval('state').in_(['draft'])) |
                    (
                        Eval('state').in_(['validated']) &
                        Eval('context', {}).get('groups', []).contains(
                            Id('treasury', 'group_petty_cash_post'))
                    )
                ),
                'depends': ['state']
                },
            'validated': {
                'invisible': ~Eval('state').in_(['open']),
                'depends': ['state']
            },
            'closed': {
                'invisible': ~Eval('state').in_(['validated']),
                'depends': ['state']
                },
            'draft': {
                'invisible': Bool(Eval('lines')) | ~Eval('state').in_(['open']),
                'depends': ['state']
                },
        })

    @fields.depends('lines', 'petty_cash_invoice_receiver', 'invoices')
    def on_change_with_total(self, name=None):
        total = Decimal('0.00')
        if self.petty_cash_invoice_receiver == 'custodian':
            for line in self.lines:
                if line.amount:
                    total += line.amount
            return total
        else:
            for line in self.invoices:
                if line.total_amount:
                    total += line.total_amount
            return total

    @fields.depends('lines', 'petty_cash', 'total')
    def on_change_with_balance(self, name=None):
        if self.petty_cash and self.petty_cash.amount and self.total:
            return self.petty_cash.amount - self.total
        else:
            return Decimal('0.00')

    @fields.depends('petty_cash')
    def on_change_with_petty_cash_party(self, name=None):
        if self.petty_cash and self.petty_cash.responsible.party:
            return self.petty_cash.responsible.party.id
        return None

    @fields.depends('period', 'date')
    def on_change_with_date(self):
        date = self.date
        if date:
            if self.period and not (
                    self.period.start_date <= date <= self.period.end_date):
                date = self.period.start_date
            return date
        return date

    @classmethod
    def validate(cls, petties):
        super(PettyCashRegistry, cls).validate(petties)
        cls.check_petty_cash_registry_open(petties)
        for petty in petties:
            petty.check_amount()

    def check_amount(self):
        if (self.petty_cash.amount and self.total and
                self.total > self.petty_cash.amount):
            self.raise_user_error('error_amount', {
                'petty_cash_registry': self.rec_name,
                'total': self.total,
                'petty_cash': self.petty_cash.rec_name,
                'petty_cash_amount': self.petty_cash.amount,

            })

    @classmethod
    def check_petty_cash_registry_open(cls, petties):
        petty_cash_ids = [petty.petty_cash.id for petty in petties]
        result = cls.search_count([
            ('petty_cash', 'in', petty_cash_ids),
            ('state', '=', 'open')
        ])
        if result > 1:
            cls.raise_user_error('error_open_petty')

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, petties):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validated(cls, petties):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for petty in petties:
            if petty.number == '/':
                petty.number = Sequence.get_id(petty.petty_cash.sequence.id)
                petty.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def closed(cls, petties):
        pool = Pool()
        Account = pool.get('account.account')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        AccountPayableInvoiceRule = pool.get('account.payable.invoice.rule')
        CostCenter = pool.get('public.cost.center')
        cost_center, = CostCenter.search([])
        Config = pool.get('treasury.petty.cash.configuration')
        config = Config(1)
        moves = []
        for petty in petties:
            if petty.petty_cash_invoice_receiver == 'custodian':
                if petty.move:
                    move = petty.move
                    MoveLine.delete(move.lines)
                else:
                    move = Move(number="#")
                move.journal = config.petty_cash_journal
                move.period = petty.period.id
                move.date = petty.date
                move.description = petty.name
                # move.origin = petty # TODO: set origin on move
                move.company = petty.company
                move.type = 'financial'
                compromises = set()
                lines = []
                for line in petty.lines:
                    compromises.add(line.compromise)
                    move_line1 = MoveLine()
                    move_line1.account = line.account
                    move_line1.debit, move_line1.credit = line.amount, 0
                    move_line1.compromise = line.compromise
                    move_line1.budget = line.budget
                    move_line1.cost_center = cost_center
                    lines.append(move_line1)
                    if not line.account:
                        cls.raise_user_error('error_account', {
                            'number': line.number,
                            'description': line.description,
                        })
                    match_rule = AccountPayableInvoiceRule.get_match_account(
                        line.account, petty.petty_cash_party)
                    if not match_rule:
                        account_expense = Account(line.account)
                        cls.raise_user_error('error_match_account', {
                            'account': account_expense.rec_name,
                        })
                    move_line2 = MoveLine()
                    move_line2.account = match_rule.payable_account
                    move_line2.debit, move_line2.credit = 0, line.amount
                    if match_rule.payable_account.party_required:
                        move_line2.party = petty.petty_cash_party
                    move_line2.payable_budget = line.budget
                    lines.append(move_line2)
                move.compromises = compromises
                move.lines = lines
                petty.move = move
                moves.append(move)
        if moves:
            Move.save(moves)
        cls.save(petties)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, petties):
        pass


class PettyCashMoveStart(ModelView):
    'Petty Cash Move Start'
    __name__ = 'petty.cash.move.start'
    date = fields.Date('Fecha de Asiento', states={'required': True})
    period = fields.Many2One('account.period', 'Periodo',
        states={'required': True})
    description = fields.Text('Descripción', states={'required': True})
    pcash_registry = fields.One2Many('treasury.petty.cash.registry',
        'petty_cash', 'Reembolsos',
        domain=[('state', '=', 'validated')],
        states={'required': True})


class PettyCashMoveWizard(Wizard):
    'Petty Cash Move Wizard'
    __name__ = 'petty.cash.move.wizard'

    start = StateView(
        'petty.cash.move.start',
        'treasury.petty_cash_move_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Aceptar', 'enter', 'tryton-ok', default=True),
        ])

    enter = StateTransition()

    def transition_enter(self):
        pool = Pool()
        Account = pool.get('account.account')
        Move = pool.get('account.move')
        PettyCashRegistry = pool.get('treasury.petty.cash.registry')
        MoveLine = pool.get('account.move.line')
        AccountPayableInvoiceRule = pool.get('account.payable.invoice.rule')
        CostCenter = pool.get('public.cost.center')
        cost_center, = CostCenter.search([])
        Config = pool.get('treasury.petty.cash.configuration')
        config = Config(1)
        moves = []
        moves_dict = {}

        move = Move(number="#")
        move.journal = config.petty_cash_journal
        move.period = self.start.period
        move.date = self.start.date
        move.description = self.start.description
        move.type = 'financial'
        lines = []
        compromises = set()

        for petty in self.start.pcash_registry:
            if petty.petty_cash_invoice_receiver == 'company':
                continue
            if petty.move:
                move = petty.move
                MoveLine.delete(move.lines)
            for line in petty.lines:
                if (not line.account or not line.compromise
                    or not line.budget):
                    PettyCashRegistry.raise_user_error(
                        'Las líneas de reembolso deben ',
                        ' contener una cuenta, partida ',
                        ' y compromiso'
                    )

                if not moves_dict.get((line.account, line.compromise,
                    line.budget, petty.petty_cash_party)):
                    amount = 0
                else:
                    amount = moves_dict[(line.account, line.compromise,
                        line.budget, petty.petty_cash_party)]['debit']
                amount += line.amount
                moves_dict[(line.account, line.compromise,
                    line.budget, petty.petty_cash_party)] = {
                        'debit': amount
                    }
                compromises.add(line.compromise)
            petty.move = move
            petty.state = 'closed'
            petty.save()

        for key, value in moves_dict.items():
            move_line1 = MoveLine()
            move_line1.account = key[0]
            move_line1.debit = value.get('debit')
            move_line1.credit = 0
            move_line1.compromise = key[1]
            move_line1.budget = key[2]
            move_line1.cost_center = cost_center
            lines.append(move_line1)
            match_rule = AccountPayableInvoiceRule.get_match_account(
                key[0], key[3].id)
            if not match_rule:
                account_expense = Account(key[0])
                PettyCashRegistry.raise_user_error('error_match_account', {
                    'account': account_expense.rec_name,
                })
            move_line2 = MoveLine()
            move_line2.account = match_rule.payable_account
            move_line2.debit = 0
            move_line2.credit = value.get('debit')
            if match_rule.payable_account.party_required:
                move_line2.party = key[3]
            move_line2.payable_budget = key[2]
            lines.append(move_line2)
        move.compromises = compromises
        move.lines = lines
        moves.append(move)

        if moves:
            Move.save(moves)
        return 'end'


class PettyCashRegistryLine(ModelView, ModelSQL):
    'Linea de registro caja chica'
    __name__ = 'treasury.petty.cash.registry.line'
    _history = True

    _states = {
        'readonly': (Eval('_parent_petty_cash_registry', {}).get(
            'state', 'draft') == 'closed'),
        }
    _depends = ['petty_cash_registry']

    petty_cash_registry = fields.Many2One('treasury.petty.cash.registry',
        'Liquidación caja chica', required=True, readonly=True)
    date = fields.Date('Fecha de registro', required=True,
        states={
            'readonly': (_states['readonly'] |
                 (Eval('_parent_petty_cash_registry', {}).get(
                     'state', 'draft') == 'validated') |
                 ~((Eval('context', {}).get('groups', []).contains(
                     Id('treasury', 'group_petty_cash_responsible'))) &
                   (Eval('create_uid') == Eval('context', {}).get('_user'))))
        }, depends=_depends)
    number = fields.Char('No. Documento', required=True,
        states={
            'readonly': (_states['readonly'] |
                 (Eval('_parent_petty_cash_registry', {}).get(
                     'state', 'draft') == 'validated') |
                 ~((Eval('context', {}).get('groups', []).contains(
                     Id('treasury', 'group_petty_cash_responsible'))) &
                   (Eval('create_uid') == Eval('context', {}).get('_user'))))
        }, depends=_depends)
    description = fields.Char('Descripción', required=True,
        states={
            'readonly': (_states['readonly'] |
                 (Eval('_parent_petty_cash_registry', {}).get(
                     'state', 'draft') == 'validated') |
                 ~((Eval('context', {}).get('groups', []).contains(
                     Id('treasury', 'group_petty_cash_responsible'))) &
                   (Eval('create_uid') == Eval('context', {}).get('_user'))))
        }, depends=_depends)
    amount = fields.Numeric('Monto', digits=(16, 2), required=True,
        states={
            'readonly': (_states['readonly'] |
                 (Eval('_parent_petty_cash_registry', {}).get(
                     'state', 'draft') == 'validated') |
                 ~((Eval('context', {}).get('groups', []).contains(
                     Id('treasury', 'group_petty_cash_responsible'))) &
                   (Eval('create_uid') == Eval('context', {}).get('_user'))))
        },
        domain=[
            ('amount', '>', 0)
        ], depends=_depends)

    account = fields.Many2One('account.account', 'Cuenta', required=False,
        domain=[
            ('kind', '!=', 'view'),
            ['OR',
             ('start_date', '=', None),
             ('start_date', '<=', Eval('date', None)),
             ],
            ['OR',
             ('end_date', '=', None),
             ('end_date', '>=', Eval('date', None)),
             ],
        ],
        select=True, states=_states, depends=_depends + ['date'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        states=_states,
        domain=[
            ('state', '=', 'done'),
            ('party', '=', Eval('_parent_petty_cash_registry', {}
                ).get('petty_cash_party', -1)),
            ['OR',
             ('accrued_pending', '>', 0),
             ('executed_pending', '>', 0),
             ('id', '=', Eval('compromise'))
             ],
        ], depends=_depends)
    budget = fields.Many2One('public.budget', 'Partida',
        context={
            'move_line': True,
            'account': Eval('account'),
            'debit': Eval('amount'),
            'credit': 0,
            'compromise': Eval('compromise'),
            'period': Eval('_parent_petty_cash', {}).get('period', -1),
        },
        states=_states, depends=_depends
    )

    petty_cash_registry_state = fields.Function(fields.Selection([
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ], 'Estado liquidación caja chica'),
        'on_change_with_petty_cash_registry_state')

    @fields.depends('petty_cash_registry',
        '_parent_petty_cash_registry.state')
    def on_change_with_petty_cash_registry_state(self, name=None):
        if self.petty_cash_registry:
            return self.petty_cash_registry.state

    @fields.depends('petty_cash_registry', '_parent_petty_cash_registry.period',
        'date')
    def on_change_with_date(self):
        date = self.date
        if date:
            if self.petty_cash_registry.period and not (
                    self.petty_cash_registry.period.start_date <=
                    date <= self.petty_cash_registry.period.end_date):
                date = self.petty_cash_registry.period.start_date
        return date


class EntryNoteType(ModelSQL, ModelView):
    'Entry Note Type'
    __name__ = 'treasury.entry.note.type'

    name = fields.Char('Nombre', required=True)
    sequence = fields.Many2One(
        'ir.sequence', 'Secuencia', required=True,
        domain=[('code', '=', 'entry.note.type')])
    account = fields.Many2One('account.account', 'Cuenta Credito',
        domain=[('kind', '!=', 'view')])

    @classmethod
    def __setup__(cls):
        super(EntryNoteType, cls).__setup__()
        cls._order = [
            ('name', 'ASC'),
        ]


class EntryNotePaymentMethods(ModelSQL, ModelView):
    'Entry Note Payment Methods'
    __name__ = 'treasury.entry.note.payment.methods'
    _state = {'readonly': Bool(Eval('is_readonly'))}
    _depends = ['is_readonly']
    _history = True

    amount = fields.Numeric(
        'Cantidad', required=True, states=_state, depends=_depends)
    payment_methods = fields.Many2One(
        'siim.account.recaudacion.forma.pago', 'Método de pago', required=True,
        states=_state, depends=_depends)
    entry_note = fields.Many2One(
        'treasury.entry.note', 'Nota de ingreso', required=True, states=_state,
        depends=_depends)
    document_number = fields.Char(
        'Número de documento',
        states={
            'readonly': Bool(Eval('is_transfer')),
            'required': ~Bool(Eval('is_transfer')),
        }, depends=['is_transfer']
    )
    transfer_date = fields.Date(
        'Fecha de transferencia',
        states={
            'readonly': Bool(Eval('is_transfer')),
            'required': ~Bool(Eval('is_transfer')),
        }, depends=['is_transfer']
    )
    is_transfer = fields.Function(
        fields.Boolean('es transferencia'), 'on_change_with_is_transfer')
    is_readonly = fields.Function(
        fields.Boolean('es lectura'), 'on_change_with_is_readonly')

    @fields.depends('entry_note')
    def on_change_with_is_readonly(self, name=None):
        if self.entry_note and self.entry_note.state != 'draft':
            return True
        return False

    @fields.depends('payment_methods', 'entry_note')
    def on_change_with_is_transfer(self, name=None):
        if self.payment_methods and self.entry_note and (
                self.entry_note.state == 'draft') and (
                self.payment_methods.transferencia
                and self.payment_methods.transferencia == 1):
            return False
        return True


class EntryNote(Workflow, ModelSQL, ModelView, AutoMove, RelatedMove):
    'Entry Note'
    __name__ = 'treasury.entry.note'
    _states = {'readonly': Eval('state') != 'draft'}
    _depends = ['state']
    _history = True

    type = fields.Many2One(
        'treasury.entry.note.type', 'Tipo', required=True, states=_states,
        depends=_depends)
    number = fields.Char('Número', states={'readonly': True})
    company = fields.Many2One(
        'company.company', 'Compañia',
        states={
            'required': True,
            'readonly': True,
        })
    payment_methods = fields.One2Many(
        'treasury.entry.note.payment.methods', 'entry_note', 'Método de pago',
        required=True, states=_states, depends=_depends)
    party = fields.Many2One(
        'party.party', 'Tercero', required=True, states=_states,
        depends=_depends)
    amount = fields.Function(fields.Numeric(
        'Monto', digits=(16, 2), required=True), 'on_change_with_amount')
    concept = fields.Text('Concepto', states=_states, depends=_depends)
    register_date = fields.Date(
        'Fecha de registro', states={
            'required': True,
            'readonly': True,
        })
    effective_date = fields.Date(
        'Fecha efectiva', states={
            'required': True,
        })
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('done', 'Realizado'),
        ('confirmed', 'Confirmado'),
        ('cancel', 'Cancelado')], 'Estado', required=True, readonly=True)
    origin = fields.Reference(
        'Origen', selection='get_origin',
        domain=[
            If((Eval('origin_model') == 'checkbook.check.returned'),
               [('state', '=', 'done'), ('sanction_state', '=', 'pending')],
               [('state', '=', 'open')])
        ], states=_states, depends=_depends + ['origin_model'],
    )
    origin_model = fields.Function(
        fields.Char('Modelo origen'), 'on_change_with_origin_model')

    @classmethod
    def __register__(cls, module_name):
        super(EntryNote, cls).__register__(module_name)
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        sql_table = cls.__table__()
        table_h = cls.__table_handler__(module_name)

        if (table_h.column_exist('register_date') and
            table_h.column_exist('effective_date')):
            cursor.execute(*sql_table.update(
                columns=[sql_table.effective_date],
                values=[sql_table.register_date],
                where=sql_table.effective_date == Null))

    @classmethod
    def __setup__(cls):
        super(EntryNote, cls).__setup__()
        cls._order = [
            ('register_date', 'DESC'),
            ('party.name', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'confirmed'),
            ('confirmed', 'draft'),
            ('confirmed', 'cancel'),
            ('confirmed', 'done'),
        ))
        cls._buttons.update(
            {
                'draft': {
                    'invisible': ~(Eval('state').in_(['confirmed'])),
                },
                'done': {
                    'invisible': ~(Eval('state').in_(['confirmed'])),
                },
                'confirmed': {
                    'invisible': ~(Eval('state').in_(['draft'])),
                },
                'cancel': {
                    'invisible': ~(Eval('state').in_(['confirmed'])),
                },
                'post': {
                    'invisible': ~(Eval('state').in_(['done'])),
                },
            })
        cls._error_messages.update({
            'no_entry_note_type_account': ('No existe una cuenta configurada '
                'para el tipo de nota de ingreso: %(type)s'),
            'no_payment_method': ('No existe una cuenta configurada para el '
                'recaudador: %(user)s. Configurar cuenta para '
                'forma de pago: %(type)s o alguna por defecto.'
                'En Módulo: Puntos de Pago.'),
            'posted_move': ('La nota de ingreso ya cuenta con un asiento '
                'contabilizado.')
            })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        if Transaction().context.get('company'):
            return Transaction().context.get('company')

    @staticmethod
    def default_register_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_effective_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('payment_methods')
    def on_change_with_amount(self, name=None):
        amount = Decimal("0")
        if self.payment_methods:
            for payment_method in self.payment_methods:
                if payment_method.amount:
                    amount += payment_method.amount
        return amount

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, entry_notes):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, entry_notes):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for entry_note in entry_notes:
            if entry_note.number:
                continue
            entry_note.number = Sequence.get_id(entry_note.type.sequence.id)
        cls.save(entry_notes)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, entry_notes):
        for entry_note in entry_notes:
            if (entry_note.origin and entry_note.type and
                    entry_note.type.name and (
                        'ANTICIPO' in entry_note.type.name.upper())):
                register_payment(entry_note.amount, entry_note.origin.id)

    @classmethod
    @ModelView.button
    def post(cls, entry_notes):
        pool = Pool()
        EntryNotePayPoint = pool.get('entry.note.pay.point')
        user_ids = [en.create_uid for en in entry_notes]
        pay_points = EntryNotePayPoint.search([
            ('user', 'in', user_ids)
        ])
        user_pay_methods = {}
        for pp in pay_points:
            # Default account
            user_pay_methods[f"{pp.user.id}-{_DEFAULT_PAYMENT_METHOD}"] = (
                    pp.default_account)
            for apm in pp.account_pay_method:
                user_pay_methods[
                    f"{pp.user.id}-{apm.forma_pago.id}"] = apm.account

        for entry_note in entry_notes:
            pool = Pool()
            Account = pool.get('account.account')
            Move = pool.get('account.move')
            MoveLine = pool.get('account.move.line')
            Period = pool.get('account.period')
            TravelExpenseAccount = pool.get('travel.expense.account')
            travel_expense_account = TravelExpenseAccount.__table__()
            cursor = Transaction().connection.cursor()

            if entry_note.move and entry_note.move.state == 'posted':
                cls.raise_user_error('posted_move')
            if entry_note.move and entry_note.move.state == 'draft':
                MoveLine.delete(entry_note.move.lines)

            move_lines = []

            accounting_date = entry_note.effective_date

            period_id = Period.find(entry_note.company.id, date=accounting_date)

            move = entry_note.move if entry_note.move else Move(number="#")
            move.type = 'financial'
            move.description = 'N.I. %s: %s' % (
                entry_note.number, entry_note.concept)
            move.journal = entry_note.journal
            move.period = period_id
            move.date = entry_note.register_date
            move.origin = entry_note
            move.company = entry_note.company

            total = Decimal(0)
            entry_note_type = entry_note.type
            origin = entry_note.origin
            if origin is None or (
                    origin.__name__ != 'company.travel.expense'):
                if entry_note_type and entry_note_type.account is None:
                    cls.raise_user_error('no_entry_note_type_account', {
                        'type': entry_note_type.name,
                    })
                if len(user_pay_methods) == 0:
                    cls.raise_user_error('no_payment_method', {
                        'user': entry_note.create_uid.name,
                        'type': entry_note.type.name,

                    })
            if ((entry_note_type and entry_note.type.account and
                 (origin is None or
                  (origin.__name__ != 'company.travel.expense'))) or
                    (origin.__name__ == 'company.travel.expense')):
                credit_account = None
                account = None
                if (origin is None or (
                        origin.__name__ != 'company.travel.expense')):
                    credit_account = entry_note.type.account
                else:
                    query = travel_expense_account.select(
                        travel_expense_account.advance_account.as_(
                            'advance_account'),
                        where=((origin.travel_type ==
                                travel_expense_account.national_travel)
                               & (origin.spending_destiny ==
                                  travel_expense_account.spending_destiny)
                               & (travel_expense_account.kind_expense ==
                                  'advance')
                               )
                    )
                    cursor.execute(*query)
                    for row in cursor.fetchall():
                        if row:
                            credit_account = Account(row[0])
                        break

                for pm in entry_note.payment_methods:
                    if (origin is None or
                            (origin.__name__ != 'company.travel.expense')):
                        account = user_pay_methods.get(
                            f"{entry_note.create_uid.id}-{pm.payment_methods.id}")  # noqa
                        default_account = user_pay_methods.get(
                            f"{entry_note.create_uid.id}-{_DEFAULT_PAYMENT_METHOD}")  # noqa
                    else:
                        account = origin.payment_request.spi.journal.account
                    if account:
                        total += pm.amount
                        move_lines.extend(entry_note.get_move_line(move,
                            account, pm.amount, 0, party=entry_note.party))
                    elif default_account:  # Default account
                        total += pm.amount
                        move_lines.extend(entry_note.get_move_line(move,
                            default_account, pm.amount, 0,
                            party=entry_note.party))

                move_lines.extend(entry_note.get_move_line(move,
                    credit_account, 0, total, party=entry_note.party))

            move.lines = move_lines
            move.save()

            entry_note.move = move
            entry_note.save()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, entry_notes):
        pass

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__
        return None

    @classmethod
    def _get_origin(cls):
        return [
            'company.employee.loan',
            'company.travel.expense',
            'checkbook.check.returned'
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models),
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('party.name',) + tuple(clause[1:]),
                  ('type.name',) + tuple(clause[1:]),
                  ]
        return domain


class EntryNotePayPoint(ModelSQL, ModelView):
    'Entry Note Pay Point'
    __name__ = 'entry.note.pay.point'

    user = fields.Many2One('res.user', 'Recaudador', required=True)
    default_account = fields.Many2One('account.account', 'Cuenta Defecto',
        domain=[
            ('code', 'like', '111%'),
            ('kind', '!=', 'view'),
        ], required=True)
    account_pay_method = fields.One2Many('entry.note.pay.method',
        'pay_point', 'Metodos de Pago')

    @classmethod
    def __setup__(cls):
        super(EntryNotePayPoint, cls).__setup__()
        cls._order = [('user', 'ASC'), ]


class EntryNotePayMethod(ModelSQL, ModelView):
    'Entry Note Pay Method'
    __name__ = 'entry.note.pay.method'
    forma_pago = fields.Many2One('siim.account.recaudacion.forma.pago',
        'Forma Pago', required=True)
    account = fields.Many2One('account.account', 'Cuenta', required=True,
        domain=[('kind', '!=', 'view')])
    pay_point = fields.Many2One('entry.note.pay.point',
        'Punto de Pago', required=True)

    @classmethod
    def __setup__(cls):
        super(EntryNotePayMethod, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('forma_pago_uniq', Unique(t, t.pay_point, t.forma_pago),
            'La Forma de Pago debe ser unica por Recaudador.'),
        ]
