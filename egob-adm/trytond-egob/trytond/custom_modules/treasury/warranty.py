from datetime import datetime, timedelta
from io import BytesIO
from decimal import Decimal

from trytond.model import fields, Workflow, ModelSQL, ModelView
from trytond.pyson import Eval, Bool
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateTransition, Button, \
    StateAction
from trytond.tools import file_open

import pandas as pd


_STATES = {
    'readonly': (Eval('state') != 'draft') | Bool(Eval('renewed_warranty')),
    }
_DEPENDS = ['state', 'renewed_warranty']

__all__ = ['WorkWarranty', 'WorkWarrantyRenew', 'WorkWarrantyImportStart',
    'WorkWarrantyImport']


class WorkWarranty(Workflow, ModelSQL, ModelView):
    'Pólizas y Garantías'
    __name__ = 'work.warranty'
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('active', 'Active'),
            ('finished', 'Finished')
        ], 'State', readonly=True)
    folder = fields.Numeric(
        'Número de carpeta', states=_STATES, required=True, depends=_DEPENDS)
    renovation_days = fields.Numeric('Días de renovación', states={
            'readonly': (Eval('state') != 'draft'),
        }, depends=_DEPENDS, required=True)
    office = fields.Char(
        "Office", size=128, states=_STATES, required=True, depends=_DEPENDS)
    party = fields.Many2One(
        'party.party', 'Party', states=_STATES, required=True, depends=_DEPENDS)
    description = fields.Text(
        "Descripción", states=_STATES, required=True, depends=_DEPENDS)
    number = fields.Integer(
        "Number", states={
            'readonly': (Eval('state') != 'draft'),
        }, readonly=True, depends=_DEPENDS)
    renovation_lines = fields.One2Many('work.warranty', 'renewed_warranty',
        'Renovaciones de garantía', states={
            'readonly': True,
        }, depends=_DEPENDS)
    renewed_warranty = fields.Many2One('work.warranty',
        'Garantía renovada', states={
            'invisible': True,
        })
    insurance_carrier = fields.Many2One('party.party', 'Aseguradora',
        states={
            'readonly': (Eval('state') != 'draft'),
            'required': Eval('state') == 'active',
        }, depends=_DEPENDS)
    start_date = fields.Date(
        'Fecha inicio', states={
            'readonly': (Eval('state') != 'draft'),
        }, required=True, depends=_DEPENDS)
    end_date = fields.Date(
        'Fecha fin', states={
            'readonly': (Eval('state') != 'draft'),
        }, required=True, depends=_DEPENDS)
    percentage = fields.Numeric(
        'Porcentaje', states={
            'readonly': (Eval('state') != 'draft'),
        }, required=True,
        domain=[
            ('percentage', '>=', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ], depends=_DEPENDS)
    amount = fields.Numeric(
        'Monto', states={
            'readonly': (Eval('state') != 'draft'),
        }, required=True, depends=_DEPENDS)
    total = fields.Function(
        fields.Numeric('Total a garantizar', digits=(16, 2)),
        'on_change_with_total'
    )
    work = fields.Many2One(
        'project.work', 'Proyecto - Obra', states=_STATES, depends=_DEPENDS)
    account_debit = fields.Many2One('account.account', 'Cuenta de débito')
    account_credit = fields.Many2One('account.account', 'Cuenta de crédito')

    @classmethod
    def __setup__(cls):
        super(WorkWarranty, cls).__setup__()
        cls._order = [
            ('start_date', 'DESC'),
            ('party.name', 'ASC'),
            ('work', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'active'),
            ('active', 'finished'),
            ))
        cls._buttons.update({
            'activate': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
            'finish': {
                'invisible': ~Eval('state').in_(['active']),
                'depends': ['state']
            }
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_percentage():
        return Decimal("0")

    @staticmethod
    def default_amount():
        return Decimal("0")

    @classmethod
    @ModelView.button
    @Workflow.transition('active')
    def activate(cls, warranties):
        for warranty in warranties:
            cls.set_number(warranties)
        cls.save(warranties)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, warranties):
        pass

    @fields.depends('amount', 'percentage')
    def on_change_with_total(self, name=None):
        amount = Decimal("0")
        percentage = Decimal("0")
        if self.amount:
            amount = self.amount
        if self.percentage:
            percentage = self.percentage
        return (amount * percentage).quantize(Decimal("0.01"))

    @classmethod
    def set_number(cls, warranties):
        '''
        Fill the work.warranty number field with the work warranty sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('treasury.configuration')

        config = Config(1)
        for warranty in warranties:
            if warranty.number:
                continue
            warranty.number = Sequence.get_id(config.work_warranty_sequence.id)
        cls.save(warranties)

    def get_rec_name(self, name):
        work = '' if self.work is None else self.work.name
        return f"{self.number} - {work}"

    @classmethod
    def copy(cls, work_warranties, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.update({'number': None, 'renovation_lines': None})
        return super(WorkWarranty, cls).copy(work_warranties, default=default)


class WorkWarrantyRenew(Wizard):
    'Renovación de Polizas y Garantias'
    __name__ = 'work.warranty.renew'

    start = StateAction('treasury.act_work_warranty1')

    def do_start(self, action):
        pool = Pool()
        WorkWarranty = pool.get('work.warranty')
        transaction = Transaction()
        ids = transaction.context['active_ids']
        if len(ids) > 1:  # TODO: support multiples renews
            self.raise_user_error('multiples_renew_error', {
            })
        id = transaction.context['active_id']
        work_warranty = WorkWarranty(id)
        work_warranties_renew = WorkWarranty.copy([work_warranty],
           default={'renewed_warranty': work_warranty.id})
        data = {'res_id': [renew.id for renew in work_warranties_renew]}
        action['views'].reverse()
        return action, data


class WorkWarrantyImportStart(ModelView):
    'Work Warranty Import Start'
    __name__ = 'work.warranty.import.start'

    source_file = fields.Binary('Archivo fuente', required=True)
    template = fields.Binary('Plantilla de garantia en Excel', readonly=True,
        filename='template_filename', file_id='template_path')
    template_filename = fields.Char('WarrantyTemplate',
        states={
            'invisible': True,
        })
    template_path = fields.Char('Path location PDF', readonly=True)

    @staticmethod
    def default_template_path():
        return './data/warranty_template.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('warranty_template', 'xlsx')

    @staticmethod
    def default_template():
        path = 'treasury/data/warranty_template.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class WorkWarrantyImport(Wizard):
    'Work Warranty Import'
    __name__ = 'work.warranty.import'

    start = StateView('work.warranty.import.start',
        'treasury.work_warranty_import_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Import', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(WorkWarrantyImport, cls).__setup__()
        cls._error_messages.update({
            'empty_data': ('Registros encontrados en el archivo:%(field)s'),
            'format_error': ('El registro: ( %(field)s , %(value)s ), '
                'tiene errores de formato.'),
            'index_error': ('El campo: ( %(field)s, %(value)s ), '
                'no corresponde a ningun registro de la base.'),
            'date_error': ('La fecha: ( %(field)s, %(value)s ), '
                'tiene errores de escritura.'),
            })

    def transition_import_(self):
        pool = Pool()
        Account = pool.get('account.account')
        Party = pool.get('party.party')
        Warranty = pool.get('work.warranty')
        ProjectWork = pool.get('project.work')
        field_names = ['party', 'insurance_carrier', 'start_date', 'end_date',
            'percentage', 'amount', 'folder', 'renovation_days', 'office',
            'description', 'work', 'account_debit', 'account_credit',
            'renovation_lines']
        file_data = fields.Binary.cast(self.start.source_file)
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        df = df.fillna(False)
        to_save = []
        for row in df.itertuples():
            warranty = Warranty()
            for attr in dir(row):
                if attr in field_names:
                    field = getattr(row, attr, None)
                    if field:
                        try:
                            if attr in ['party', 'insurance_carrier']:
                                setattr(warranty, attr, Party.search([
                                    ('tax_identifier', '=', field)
                                    ])[0])
                            elif attr in ['start_date', 'end_date']:
                                temp_date = timedelta(days=field) + \
                                    datetime(1899, 12, 30)  # FIXME
                                setattr(warranty, attr, temp_date)
                            elif attr in ['percentage', 'amount', 'folder',
                                    'renovation_days']:
                                setattr(warranty, attr, float(field))
                            elif attr in ['account_debit', 'account_credit']:
                                account = Account.search([
                                    ('code', '=', field)
                                ])[0]
                                setattr(warranty, attr, account)
                            elif attr == 'renovation_lines':
                                if field == 'X':
                                    continue
                                else:
                                    r_ids = [int(x) for x in field.split(',')]
                                    setattr(warranty, attr,
                                        Warranty.search([
                                            ('number', 'in', r_ids)
                                        ]))
                            elif attr == 'work':
                                setattr(warranty, attr,
                                    ProjectWork.search([('id', '=', 1)])[0])
                            else:
                                setattr(warranty, attr, field)
                        except ValueError:
                            self.raise_user_error('format_error', {
                                    'field': attr,
                                    'value': str(field)
                                    })
                        except IndexError:
                            self.raise_user_error('index_error', {
                                    'field': attr,
                                    'value': str(field)
                                    })
                        except TypeError:
                            self.raise_user_error('date_error', {
                                    'field': attr,
                                    'value': str(field)
                                    })
                    else:
                        self.raise_user_error('empty_data', {'field': attr})
            to_save.append(warranty)
        Warranty.save(to_save)
        return 'end'
