# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.treasury.tests.test_treasury import suite
except ImportError:
    from .test_treasury import suite

__all__ = ['suite']
