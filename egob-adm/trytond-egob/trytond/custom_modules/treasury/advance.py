from collections import defaultdict

from sql import Literal, Cast
from sql.operators import Concat
from sql.functions import Substring, Position, CharLength


from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.pyson import Eval, If
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import reduce_ids

__all__ = ['Advance', 'AdvanceLine', 'Move']

_STATES = {
    'readonly': Eval('state') != 'draft'
}


class Advance(Workflow, ModelSQL, ModelView):
    'Advance'
    __name__ = 'treasury.advance'
    party = fields.Many2One('party.party', 'Tercero', select=True,
            states=_STATES)

    account = fields.Many2One('account.account', 'Cuenta contable',
        required=True, states=_STATES, domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ('kind', '!=', 'view'),
            ('bank_reconcile', '=', True)
            ])
    journal = fields.Many2One('account.journal', 'Libro diario', required=True,
            states=_STATES)
    period = fields.Many2One('account.period', 'Periodo', required=True,
        domain=[
            ('company', '=', Eval('company', -1)),
            ],
        states=_STATES, depends=['state', 'company'], select=True)
    reference = fields.Char('Referencia', states=_STATES)
    date = fields.Date('Fecha', states=_STATES, required=True)
    total_amount = fields.Function(fields.Numeric('Valor total',
        digits=(16, 2), readonly=True), 'on_change_with_total_amount')
    pending_amount = fields.Function(fields.Numeric('Valor pendiente',
        digits=(16, 2), states=_STATES), 'get_pending_amount')
    company = fields.Many2One('company.company', 'Empresa', required=True,
        readonly=True, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        depends=['state'])
    # devengado
    type = fields.Selection([
            ('works', 'Proyectos/Obras'),
            ('goods_services', 'Bienes y servicios'),
            ('travel_expense', 'Gastos por viaje'),
            ('salary', 'Salarios'),
    ], 'Type', required=True, states=_STATES)
    move_lines = fields.Function(fields.One2Many('account.move.line', None,
        'Líneas de asiento', states=_STATES), 'on_change_with_move_lines')
    advance_lines = fields.One2Many('treasury.advance.line', 'advance',
        'Líneas de anticipo', states=_STATES)
    description = fields.Text('Descripción')
    state = fields.Selection([
                ('draft', 'Borrador'),
                ('validated', 'Validado'),
                ('canceled', 'Cancelado'),
            ], 'State', readonly=True, required=True)
    post_state = fields.Function(fields.Selection(
        [
            (None, 'None'),
            ('draft', 'Borrador'),
            ('posted', 'Realizado'),
            ('invalid', 'Invalido'),
        ], 'Move State', readonly=True, required=True), 'get_move_state',
        searcher='search_post_state')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('advance_lines')
    def on_change_with_total_amount(self, name=None):
        if self.advance_lines:
            total = 0
            for line in self.advance_lines:
                if line.amount:
                    total += line.amount
            return total
        return 0

    @classmethod
    def __setup__(cls):
        super(Advance, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'validated'),
            ('validated', 'canceled'),
            ('validated', 'draft')
        ))
        cls._buttons.update({
            'validate': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
            'set_draft': {
                'invisible': ~Eval('state').in_(['validated']),
                'depends': ['state']
            },
            'cancel': {
                'invisible': ~Eval('state').in_(['validated']),
                'depends': ['state']
            }
        })
        cls._error_messages.update({
                'advance_move_posted': ('To cancel this advance this cannot'
                    ' have posted account movements related or you have to'
                    ' make a cancelation account move.'),
                'advance_move_line_related': ('This advance cannot have account'
                    ' move lines related if you want to cancel it.')
                }),

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate(cls, advances):
        for advance in advances:
            advance.generate_moves()
        cls.save(advances)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def set_draft(cls, advances):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, advances):
        for advance in advances:
            pool = Pool()
            Move = pool.get('account.move')
            table = Move.__table__()
            query = table.select(
                table.id,
                table.state,
                where=(table.origin == f"treasury.advance,{str(advance.id)}")
            )
            cursor = Transaction().connection.cursor()
            cursor.execute(*query)
            move_id, move_state = cursor.fetchone()
            if move_state == 'posted':
                query = table.select(
                    table.id,
                    where=(table.origin == f"account.move,{str(move_id)}")
                )
                cursor.execute(*query)
                cancelation_move = cursor.fetchone()
                if not cancelation_move:
                    advance.raise_user_error('advance_move_posted', {})
                else:
                    continue
            if len(advance.move_lines) > 0:
                advance.raise_user_error('advance_move_line_related', {})

    def generate_moves(self):
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Date = pool.get('ir.date')
        move_table = Move.__table__()
        query_move = move_table.select(
            move_table.id,
            where=(move_table.origin == f"treasury.advance,{str(self.id)}")
        )
        cursor = Transaction().connection.cursor()
        cursor.execute(*query_move)
        result = []
        for line in cursor.fetchall():
            result.append(line[0])
        # Ask if exist a move for this advance
        move = Move(number="#")
        if len(result) > 0:
            move = Move.browse(result)[0]
        else:
            move.journal = self.journal
            move.period = self.period
            move.type = 'financial'
            move.description = self.reference
            move.date = Date.today()
            move.origin = self
            move.company = self.company
        # Delete old lines
        self.remove_lines()
        move_lines = []
        # Create new debit lines
        for line in self.advance_lines:
            move_line = MoveLine()
            move_line.account = line.account
            move_line.debit = line.amount
            move_line.state = 'draft'
            move_line.credit = 0.00
            move_line.description = line.detail
            move_line.move = move
            move_lines.append(move_line)
        # Create credit lines
        move_line = MoveLine()
        move_line.account = self.account
        move_line.debit = 0.00
        move_line.credit = self.total_amount
        move_line.description = '/'
        # move_line.state = 'draft'
        move_line.move = move
        move_lines.append(move_line)
        # Set lines to move
        move.lines = move_lines
        moves = []
        moves.append(move)
        Move.save(moves)

    def remove_lines(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        if self.move_lines:
            MoveLine.delete(self.move_lines)

    def on_change_with_move_lines(self, name=None):
        result = []
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        move_table = Move.__table__()
        move_line_table = MoveLine.__table__()
        query_move = move_table.select(
            move_table.id,
            where=(move_table.origin == f"treasury.advance,{str(self.id)}")
        )
        query_move_line = move_line_table.select(
            move_line_table.id,
            where=(move_line_table.move.in_(query_move))
        )
        cursor.execute(*query_move_line)
        for line in cursor.fetchall():
            result.append(line[0])
        return result

    @classmethod
    def search_employee(cls, name, clause):
        pool = Pool()
        Move = pool.get('account.move')
        table = cls.__table__()
        move_table = Move.__table__()
        query = table.select(
            move_table.origin.id,
            where=move_table.origin != None
        )
        return [('id', 'in', query)]

    @classmethod
    def get_move_state(cls, advances, names):
        result = defaultdict(lambda: None)
        pool = Pool()
        Move = pool.get('account.move')
        cursor = Transaction().connection.cursor()
        move_table = Move.__table__()
        table = cls.__table__()
        advance_ids = [x.id for x in advances]
        where = reduce_ids(table.id, advance_ids)
        cursor.execute(*table.join(move_table,
            condition=(
                Concat(cls.__name__ + ',', table.id) == move_table.origin)
            ).select(table.id,
                move_table.state,
                where=where))
        for line in cursor.fetchall():
            result[line[0]] = line[1]
        return {'post_state': result}

    @classmethod
    def get_pending_amount(cls, advances, names):
        result = defaultdict(lambda: None)
        for advance in advances:
            result[advance.id] = advance.total_amount
        return {'pending_amount': result}

    @classmethod
    def search_post_state(cls, name, clause):
        pool = Pool()
        Move = pool.get('account.move')
        table = cls.__table__()
        move_table = Move.__table__()
        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]
        query = table.join(move_table,
            condition=(table.id == Cast(
                Substring(move_table.origin, Position(',', move_table.origin) +
                    Literal(1), CharLength(move_table.origin)),
                cls.id.sql_type().base))
                ).select(
                    table.id,
                    where=Operator(move_table.state, value))
        return [('id', 'in', query)]


class AdvanceLine(ModelSQL, ModelView):
    'Advance Line'
    __name__ = 'treasury.advance.line'
    advance = fields.Many2One('treasury.advance', 'Anticipo', required=True)
    amount = fields.Numeric('Amount', digits=(16, 2), required=True)
    account = fields.Many2One(
        'account.account', 'Cuenta contable', required=True)
    detail = fields.Function(fields.Char('Detalles', readonly=True),
        'get_advance_reference')

    def get_advance_reference(self, name=None):
        return self.advance.reference


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['treasury.advance']
