from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval

__all__ = ['Fiscalyear']


class Fiscalyear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'

    payment_sequence = fields.Many2One('ir.sequence',
        'Secuencia de orden de pago', required=True,
        domain=[
            ('code', '=', 'treasury.account.payment'),
            ['OR',
            ('company', '=', Eval('company')),
            ('company', '=', None)
            ]],
        context={
            'code': 'treasury.account.payment',
            'company': Eval('company'),
        }, depends=['company'])

    payment_request_sequence = fields.Many2One('ir.sequence',
        'Secuencia de Solicitud de pago', required=True,
        domain=[
            ('code', '=', 'treasury.account.payment.request'),
            ['OR',
            ('company', '=', Eval('company')),
            ('company', '=', None)
            ]],
        context={
            'code': 'treasury.account.payment.request',
            'company': Eval('company'),
        }, depends=['company'])
    payment_request_receipt_sequence = fields.Many2One('ir.sequence',
        'Secuencia Comprobante de pago',
        required=True,
        domain=[
           ('code', '=', 'treasury.account.payment.request'),
           ['OR',
            ('company', '=', Eval('company')),
            ('company', '=', None)
            ]],
        context={
           'code': 'treasury.account.payment.request',
           'company': Eval('company'),
        }, depends=['company'])
    transfer_sequence = fields.Many2One('ir.sequence',
        'Secuencia de transferencia SPI', required=True,
        domain=[
            ('code', '=', 'treasury.account.payment.spi'),
            ['OR',
            ('company', '=', Eval('company')),
            ('company', '=', None)
            ]],
        context={
            'code': 'treasury.account.payment.spi',
            'company': Eval('company'),
        }, depends=['company'])
