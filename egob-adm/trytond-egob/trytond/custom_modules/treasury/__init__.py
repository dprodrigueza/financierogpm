# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import account
from . import invoice
from . import fiscalyear
from . import advance
from . import treasury
from . import payment
from . import party
from . import bank
from . import warranty
from . import configuration
from . import report
from . import financial_investments
from . import hr
from . import hr_ec_payslip
from . import statement
from . import checkbook

__all__ = ['register']


def register():
    Pool.register(
        account.Account,
        account.Move,
        account.MoveLine,
        invoice.Invoice,
        invoice.InvoiceTracking,
        advance.Advance,
        advance.AdvanceLine,
        advance.Move,
        fiscalyear.Fiscalyear,
        hr.TravelExpense,
        hr.TravelExpenseAccount,
        hr.EmployeeLoan,
        hr_ec_payslip.ContractLiquidation,
        treasury.AccountPaymentSPI,
        treasury.PaymenAccountPaymentSPIConcept,
        treasury.AccountPaymentRequest,
        treasury.Company,
        treasury.PettyCash,
        treasury.PettyCashSequence,
        treasury.PettyCashRegistry,
        treasury.PettyCashRegistryLine,
        treasury.EntryNote,
        treasury.EntryNoteType,
        treasury.EntryNotePaymentMethods,
        treasury.EntryNotePayPoint,
        treasury.EntryNotePayMethod,
        treasury.PettyCashMoveStart,
        payment.Payment,
        payment.PaymentRelatedMoveLine,
        payment.PaymentType,
        payment.PaymentRequest,
        payment.PaymentRequestGenerateMoveStart,
        payment.PaymentRequestImportStart,
        payment.AccountPaymentRoles,
        payment.AccountPaymentAdvanceRole,
        payment.AccountPaymentRetiredRole,
        payment.AccountPaymentRoleRules,
        payment.AccountPaymentInvoice,
        payment.AccountPaymentMove,
        payment.AccountPaymentContractProcess,
        payment.AccountPaymentPartyLiquidation,
        payment.AccountPaymentTravelExpense,
        payment.PaymentRequestLineDetail,
        payment.RequestFillLinesStart,
        payment.CancelStart,
        configuration.Configuration,
        configuration.ConfigurationSequence,
        configuration.PettyCashConfiguration,
        configuration.PettyCashConfigurationInvoiceReceiver,
        configuration.PettyCashConfigurationJournal,
        party.Party,
        bank.BankAccount,
        bank.BankAccountNumber,
        bank.Bank,
        warranty.WorkWarranty,
        warranty.WorkWarrantyImportStart,
        financial_investments.FinancialInvestmentType,
        financial_investments.FinancialInvestment,
        financial_investments.FinancialInvestmentLine,
        statement.Statement,
        statement.StatementLine,
        statement.ImportStart,
        checkbook.CheckbookConfigurationGeneral,
        checkbook.Checkbook,
        checkbook.CheckDrawn,
        checkbook.CheckReceived,
        checkbook.CheckReturned,
        checkbook.CheckReceivedType,
        checkbook.CheckReturnedReasonType,
        checkbook.CheckConsultingContext,
        checkbook.CheckConsulting,
        module='treasury', type_='model')
    Pool.register(
        treasury.PettyCashMoveWizard,
        payment.RequestFillLines,
        warranty.WorkWarrantyRenew,
        warranty.WorkWarrantyImport,
        payment.PaymentRequestGenerateMove,
        payment.PaymentRequestImport,
        payment.CancelWizard,
        statement.Import,
        module='treasury', type_='wizard')
    Pool.register(
        report.AccountPaymentSPITXT,
        report.AccountPaymentSPIzipTXT,
        report.AccountPaymentSPIODS,
        report.WorkWarranty,
        report.WorkWarrantyExpiration,
        report.TreasuryAccountPayment,
        report.TreasuryAccountPaymentSpi,
        report.TreasuryAccountPaymentSpiSummary,
        report.PaymentRequest,
        report.EntryNote,
        report.CheckDrawn,
        report.CheckReceived,
        report.CheckReturned,
        report.CheckConsulting,
        report.AccountPaymentSPIGrouped,
        module='treasury', type_='report')
