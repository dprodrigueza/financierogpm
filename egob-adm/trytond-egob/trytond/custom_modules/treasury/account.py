from trytond.model import fields
from trytond.pyson import If, Bool, Eval
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids


__all__ = ['Account', 'Move', 'MoveLine']


class Account(metaclass=PoolMeta):
    __name__ = 'account.account'

    bank_account = fields.Many2One('bank.account', 'Cuenta bancaria',
        domain=[
            ('id', If(Bool(Eval('bank_reconcile')), '!=', '='), None),
        ],
        states={
            'required': Bool(Eval('bank_reconcile')),
            'readonly': ~Bool(Eval('bank_reconcile')),
        }, depends=['bank_reconcile'])


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + \
            ['treasury.account.payment.request']

    @classmethod
    def get_invoices(cls, moves, names):
        # call super method in order to get accrued relation with invoices
        # here we compute executed relation through payment request
        result = super(Move, cls).get_invoices(moves, names)['related_invoices']
        pool = Pool()
        table = cls.__table__()
        PaymentRequest = pool.get('treasury.account.payment.request')
        payment_request = PaymentRequest.__table__()
        PaymentRequestInv = pool.get('treasury.account.payment.request.invoice')
        payment_request_inv = PaymentRequestInv.__table__()
        ids = [m.id for m in moves]
        cursor = Transaction().connection.cursor()
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payment_request,
                condition=table.id == payment_request.move
            ).join(payment_request_inv,
                condition=payment_request.id == payment_request_inv.request
            ).select(
                table.id,
                payment_request_inv.invoice,
                where=red_sql,
            )
            cursor.execute(*query)
            for move_id, invoice_id in cursor.fetchall():
                result[move_id].append(invoice_id)
        return {
            'related_invoices': result
        }


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    payment_origin = fields.Reference('Origen de pago', [
        (None, 'None'),
        ('treasury.account.payment.spi', 'Transferencia SPI'),
        ('treasury.account.payment.request', 'Solicitud de pago')
    ])

    @classmethod
    def __setup__(cls):
        super(MoveLine, cls).__setup__()
        cls._check_modify_exclude.add('payment_origin')


class PartyLiquidation(metaclass=PoolMeta):
    __name__ = 'party.liquidation'

    requests = fields.Many2Many(
        'treasury.account.payment.request.party.liquidation',
        'liquidation', 'request', 'Liquidaciones',
        domain=[
            ('party', '=', Eval('party', -1)),
        ], depends=['party'])
