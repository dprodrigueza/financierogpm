from decimal import Decimal
from collections import defaultdict

from sql.aggregate import Count, Sum, Literal

from trytond.pool import Pool, PoolMeta
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pyson import Eval


__all__ = ['BankAccount', 'BankAccountNumber', 'Bank']

_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_DEPENDS = ['state']


class BankAccount(metaclass=PoolMeta):
    __name__ = 'bank.account'
    account_type = fields.Selection([
            ('savings', 'Ahorros'),
            ('current', 'Corriente'),
            ('card', 'Tarjeta Efectivo')
    ], 'Account Type', required=True)
    account_type_translated = account_type.translated('account_type')

    @classmethod
    def __setup__(cls):
        super(BankAccount, cls).__setup__()
        cls._error_messages.update({
            'delete_error': ('Para eliminar use la opción de remover el'
                ' registro "-"'),
        })

    @classmethod
    def default_owners(cls):
        context = Transaction().context
        if context.get('party'):
            return [context['party']]
        return []

    @classmethod
    def delete(cls, accounts):
        cls.raise_user_error('delete_error')


class BankAccountNumber(metaclass=PoolMeta):
    __name__ = 'bank.account.number'

    @staticmethod
    def default_type():
        return 'other'


class Bank(metaclass=PoolMeta):
    __name__ = 'bank'

    central_bank_code = fields.Char('Código Banco Central',
        help="Código asignado por el Banco Central para transferencias SPI.")
    payment_count = fields.Function(fields.Integer('Contador de pagos'),
        'get_bank_summary')
    payment_total = fields.Function(fields.Numeric('Total a pagar'),
        'get_bank_summary')
    payment_count_cancel = fields.Function(fields.Integer('Contador de negados'),
        'get_bank_summary')
    payment_total_cancel = fields.Function(fields.Numeric('Total Negados'),
        'get_bank_summary')

    @classmethod
    def get_bank_summary(cls, banks, names):
        count = defaultdict(lambda: 0)
        total = defaultdict(lambda: Decimal('0'))
        count_cancel = defaultdict(lambda: 0)
        total_cancel = defaultdict(lambda: Decimal('0'))
        pool = Pool()
        bank = cls.__table__()
        BankAccount = pool.get('bank.account')
        bank_account = BankAccount.__table__()
        SPILine = pool.get('treasury.account.payment.request.line.detail')
        spi_line = SPILine.__table__()
        TreasurSPI = pool.get('treasury.account.payment.spi')
        treasury_spi = TreasurSPI.__table__()
        context = Transaction().context
        if context.get('spi'):
            where = treasury_spi.id == context['spi']
            where_cancel = ((treasury_spi.id == context['spi']) &
                            spi_line.state.in_(['cancel', 'rejected']))
        else:
            where = Literal(True)
            where_cancel = Literal(True)
        query = bank.join(bank_account,
            condition=bank.id == bank_account.bank
        ).join(spi_line,
            condition=spi_line.bank_account == bank_account.id
        ).join(treasury_spi,
            condition=spi_line.spi == treasury_spi.id
        ).select(
            bank.id,
            Count(spi_line.id).as_('payment_count'),
            Sum(spi_line.amount).as_('payment_total'),
            where=where, group_by=[bank.id])
        query_cancel = bank.join(bank_account,
            condition=bank.id == bank_account.bank
        ).join(spi_line,
            condition=spi_line.bank_account == bank_account.id
        ).join(treasury_spi,
            condition=spi_line.spi == treasury_spi.id
        ).select(
            bank.id,
            Count(spi_line.id).as_('payment_count'),
            Sum(spi_line.amount).as_('payment_total'),
            where=where_cancel, group_by=[bank.id])
        cursor = Transaction().connection.cursor()
        cursor.execute(*query)
        for row in cursor.fetchall():
            count[row[0]] = row[1]
            total[row[0]] = row[2]
        cursor.execute(*query_cancel)
        for row in cursor.fetchall():
            count_cancel[row[0]] = row[1]
            total_cancel[row[0]] = row[2]
        return {
            'payment_count': count,
            'payment_total': total,
            'payment_count_cancel': count_cancel,
            'payment_total_cancel': total_cancel,
        }
