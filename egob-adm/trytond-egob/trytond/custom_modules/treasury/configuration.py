from trytond.model import ModelView, ModelSQL, ModelSingleton, fields, \
    ValueMixin
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)
from trytond.transaction import Transaction

__all__ = ['Configuration', 'ConfigurationSequence', 'PettyCashConfiguration',
    'PettyCashConfigurationInvoiceReceiver', 'PettyCashConfigurationJournal']

petty_cash_journal = fields.Many2One(
    'account.journal', "Diario caja Chica", required=True)

invoice_receivers = [
    (None, ''),
    ('custodian', 'Custodio'),
    ('company', 'Empresa'),
]


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Treasury Configuration'
    __name__ = 'treasury.configuration'

    payment_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Secuencia de pago", required=False,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'treasury.account.payment'),
            ]))
    spi_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', "Secuencia SPI", required=False,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'treasury.account.payment.spi'),
            ]))
    work_warranty_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia de garantía de trabajo', required=False,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'work.warranty'),
            ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'payment_sequence':
            return pool.get('treasury.configuration.sequence')
        if field == 'spi_sequence':
            return pool.get('treasury.configuration.sequence')
        if field == 'work_warranty_sequence':
            return pool.get('treasury.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_payment_sequence(cls, **pattern):
        return cls.multivalue_model(
            'payment_sequence').default_payment_sequence()

    @classmethod
    def default_spi_sequence(cls, **pattern):
        return cls.multivalue_model(
            'spi_sequence').default_spi_sequence()

    @classmethod
    def default_work_warranty_sequence(cls, **pattern):
        return cls.multivalue_model(
            'work_warranty_sequence').default_work_warranty_sequence()


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    'Treasury Configuration Sequence'
    __name__ = 'treasury.configuration.sequence'
    payment_sequence = fields.Many2One(
        'ir.sequence', "Secuencia de pago", required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'treasury.account.payment'),
            ],
        depends=['company'])
    spi_sequence = fields.Many2One(
        'ir.sequence', "Secuencia SPI", required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'treasury.account.payment.spi'),
            ],
        depends=['company'])
    work_warranty_sequence = fields.Many2One(
        'ir.sequence', "Secuencia de garantía de trabajo", required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'work.warranty'),
            ],
        depends=['company'])

    @classmethod
    def default_payment_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('treasury', 'sequence_payment')
        except KeyError:
            return None

    @classmethod
    def default_spi_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('treasury', 'sequence_spi')
        except KeyError:
            return None

    @classmethod
    def default_work_warranty_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('treasury', 'sequence_work_warranty')
        except KeyError:
            return None


class PettyCashConfiguration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Configuracion de cajas chicas'
    __name__ = 'treasury.petty.cash.configuration'

    invoice_receiver = fields.MultiValue(fields.Selection(
            invoice_receivers, "Receptor de facturas"))
    petty_cash_journal = fields.MultiValue(petty_cash_journal)
    account = fields.Many2One('account.account', 'Cuenta',
            states={'required': True},
            domain=[('code', 'like', '112.13%')])

    @classmethod
    def default_invoice_receiver(cls, **pattern):
        return cls.multivalue_model('invoice_receiver'
            ).default_invoice_receiver()


class PettyCashConfigurationInvoiceReceiver(ModelSQL, CompanyValueMixin):
    'Configuración de cajas chicas - Receptor de facturas'
    __name__ = 'treasury.petty.cash.configuration.invoice_receiver'
    petty_cash_configuration = fields.Many2One(
        'treasury.petty.cash.configuration', 'Configuración de cajas chicas',
        required=True, ondelete='CASCADE')
    invoice_receiver = fields.Selection(invoice_receivers,
        'Receptor de facturas', required=True)

    @classmethod
    def default_invoice_receiver(cls):
        return 'custodian'


class PettyCashConfigurationJournal(ModelSQL, ValueMixin):
    "Diario caja chica"
    __name__ = 'treasury.petty.cash.configuration.petty_cash_journal'
    petty_cash_journal = petty_cash_journal


