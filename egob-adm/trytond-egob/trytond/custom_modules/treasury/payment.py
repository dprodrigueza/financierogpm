from collections import defaultdict
from io import BytesIO
from decimal import Decimal
from datetime import datetime

from sql import Null, Union
from sql.conditionals import Coalesce
from sql.operators import Concat

from trytond.pool import Pool
from trytond.model import fields, Workflow, ModelSQL, ModelView
from trytond.pyson import If, Eval, Bool
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, Button, StateAction,
    StateTransition)
from trytond.tools import grouped_slice, reduce_ids, cursor_dict, file_open
from trytond.modules.public_ec.budget import employee_field, set_employee
from trytond.modules.public_ec.account import show_moves

from trytond import backend

# Use in case to view result of account.move generation
# from trytond.modules.public_ec.account import show_moves

import pandas as pd

_STATES = {
    'readonly': Eval('state') != 'draft',
    }
_STATES_LINE = {
    'readonly': True
}
_DEPENDS = ['state']

__all__ = ['Payment', 'PaymentRelatedMoveLine', 'PaymentType', 'PaymentRequest',
    'PaymentRequestGenerateMoveStart', 'PaymentRequestGenerateMove',
    'AccountPaymentPartyLiquidation', 'PaymentRequestLineDetail',
    'AccountPaymentRoles', 'AccountPaymentMove', 'AccountPaymentRetiredRole',
    'AccountPaymentContractProcess', 'AccountPaymentAdvanceRole',
    'AccountPaymentRoleRules', 'AccountPaymentInvoice', 'RequestFillLinesStart',
    'RequestFillLines', 'PaymentRequestImport', 'PaymentRequestImportStart',
    'AccountPaymentTravelExpense', 'CancelStart', 'CancelWizard', ]


class Payment(Workflow, ModelSQL, ModelView):
    'Payment'
    __name__ = 'treasury.account.payment'
    number = fields.Char('Number', readonly=True, select=True)
    company = fields.Many2One('company.company', 'Empresa', required=True,
        select=True, states=_STATES, domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        depends=_DEPENDS)
    party = fields.Many2One('party.party', 'Tercero', required=True,
        select=True, states=_STATES, depends=_DEPENDS)
    invoice = fields.Many2One('account.invoice', 'Factura', required=True,
        domain=[
            ('party', '=', Eval('party')),
        ], states=_STATES, depends=_DEPENDS + ['party'])
    invoice_payment_lines = fields.Function(
        fields.Many2Many('account.move.line', None, None,
        'Líneas de asiento de factura'),
        'on_change_with_invoice_payment_lines')
    applicant = fields.Many2One('company.employee', 'Solicitante',
        required=True,
        domain=[
            ('contract_company', '=', Eval('company'))
        ], states=_STATES, depends=_DEPENDS + ['company'])
    payment_type = fields.Many2One('account.payment.type', 'Tipo de pago',
        required=True, states=_STATES, depends=_DEPENDS)
    date = fields.Date('Date', required=True, states=_STATES, depends=_DEPENDS)
    spi = fields.Many2One('treasury.account.payment.spi', 'SPI', readonly=True)
    bank_account = fields.Many2One('bank.account', 'Cuenta bancaria',
        domain=[
            ('owners', 'in', [Eval('party')])
        ], states=_STATES, depends=_DEPENDS + ['state', 'party'])
    description = fields.Char('Descripción', states=_STATES, depends=_DEPENDS)
    moves = fields.Function(fields.Many2Many('account.move',
        None, None, 'Asiento contable'), 'get_moves', searcher='search_move')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('approved', 'Aprovado'),
        ('processing', 'Procesando'),
        ('succeeded', 'Completado'),
        ('failed', 'Fallido'),
        ], 'State', readonly=True, select=True)
    lines = fields.Many2Many(
        'treasury.account.payment-related-account.move.line',
        'payment', 'line', 'Líneas de asiento de pago',
        domain=[
            ('id', 'in', Eval('invoice_payment_lines', [])),
            ('move.company', '=', Eval('company', -1)),
            ['OR', ('credit', '<', 0), ('debit', '>', 0)],
            ('account.kind', 'in', ['payable', 'payable_previous']),
            ('party', 'in', [Eval('party', None), None]),
            ('move_state', '=', 'posted')
        ], states=_STATES, depends=_DEPENDS + ['invoice_payment_lines',
            'company', 'party'])
    amount = fields.Numeric('Valor', digits=(16, 2), required=True,
        domain=[
            ('amount', '<=', Eval('pending_amount')),
        ], states=_STATES, depends=_DEPENDS + ['pending_amount'])
    pending_amount = fields.Function(fields.Numeric('Valor pendiente',
        digits=(16, 2)), 'on_change_with_pending_amount')
    notes = fields.Text('Notas', depends=_DEPENDS)
    egress_concept = fields.Many2One('treasury.account.payment.spi.concept',
        'Concepto de egreso', states=_STATES, required=True,)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=['company'], required=True, states=_STATES)

    @classmethod
    def __setup__(cls):
        super(Payment, cls).__setup__()
        cls._order.insert(0, ('date', 'DESC'))
        cls._error_messages.update({
                'delete_draft': ('Payment "%s" must be in draft before '
                    'deletion.'),
                })
        cls._transitions |= set((
                ('draft', 'approved'),
                ('approved', 'processing'),
                ('processing', 'succeeded'),
                ('processing', 'failed'),
                ('approved', 'draft'),
                ('succeeded', 'failed'),
                ('failed', 'succeeded'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') != 'approved',
                    'icon': 'tryton-back',
                    'depends': ['state'],
                    },
                'approve': {
                    'invisible': Eval('state') != 'draft',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'succeed': {
                    'invisible': ~Eval('state').in_(
                        ['processing', 'failed']),
                    'icon': 'tryton-ok',
                    'depends': ['state'],
                    },
                'fail': {
                    'invisible': ~Eval('state').in_(
                        ['processing', 'succeeded']),
                    'icon': 'tryton-cancel',
                    'depends': ['state'],
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @fields.depends('party', 'invoice', 'bank_account')
    def on_change_party(self):
        if not self.party:
            self.invoice = None
            self.bank_account = None

    @classmethod
    def search_party(cls, name, clause):
        return [('party.' + clause[0],) + tuple(clause[1:])]

    @fields.depends('invoice')
    def on_change_with_invoice_payment_lines(self, name=None):
        result = []
        if self.invoice:
            for line in self.invoice.payment_lines:
                result.append(line.line.id)
        return result

    @classmethod
    def get_moves(cls, payments, names):
        pool = Pool()
        # payment = cls.__table__()
        Move = pool.get('account.move')
        account_move = Move.__table__()
        MoveLine = pool.get('account.move.line')
        account_move_line = MoveLine.__table__()
        PaymentMoveLine = pool.get(
            'treasury.account.payment-related-account.move.line')
        payment_move_line = PaymentMoveLine.__table__()
        cursor = Transaction().connection.cursor()
        moves = defaultdict(lambda: [])

        ids = [c.id for c in payments]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payment_move_line.id, sub_ids)
            query = payment_move_line.join(account_move_line,
                condition=account_move_line.id == payment_move_line.line
            ).join(account_move,
                condition=account_move.id == account_move_line.move
            ).select(
                payment_move_line.payment,
                account_move_line.move,
                where=red_sql,
            )
            cursor.execute(*query)
            for row in cursor.fetchall():
                moves[row[0]].append(row[1])
        return {
            'moves': moves,
        }

    @classmethod
    def search_move(cls, name, clause):
        return [('lines.' + clause[0],) + tuple(clause[1:])]

    @fields.depends('lines')
    def on_change_with_pending_amount(self, name=None):
        if self.lines:
            amount = 0
            for line in self.lines:
                amount += line.amount
            return amount
        return None

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, payments):
        pass

    @classmethod
    def set_number(cls, payments):
        '''
        Fill the number field with the payment sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for payment in payments:
            if payment.number:
                continue
            payment.number = Sequence.get_id(
                payment.fiscalyear.payment_sequence.id)
        cls.save(payments)

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    @set_employee('approved_by')
    def approve(cls, payments):
        pool = Pool()
        Date = pool.get('ir.date')
        for payment in payments:
            payment.approval_date = Date.today()
        cls.save(payments)
        cls.set_number(payments)

    @classmethod
    @Workflow.transition('processing')
    def process(cls, payments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('succeeded')
    def succeed(cls, payments):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('failed')
    def fail(cls, payments):
        pass

    @classmethod
    def delete(cls, payments):
        for payment in payments:
            if payment.state != 'draft':
                cls.raise_user_error('delete_draft', (payment.rec_name))
        super(Payment, cls).delete(payments)


class PaymentRelatedMoveLine(ModelSQL):
    'Payment - Moveline'
    __name__ = 'treasury.account.payment-related-account.move.line'
    _table = 'treasury_payment_moveline_rel'
    payment = fields.Many2One('treasury.account.payment', 'Pago',
            ondelete='CASCADE', select=True, required=True)
    line = fields.Many2One('account.move.line', 'Línea de asiento',
            ondelete='RESTRICT', select=True, required=True)


class PaymentType(ModelSQL, ModelView):
    'Account Payment Type'
    __name__ = 'account.payment.type'

    name = fields.Char('Name', required=True)
    description = fields.Char('Descripción', required=True)


class PaymentRequest(Workflow, ModelView, ModelSQL):
    'Payment Request'
    __name__ = 'treasury.account.payment.request'
    _history = True

    STATES = ['paid']

    receipt = fields.Char('Nro. Comprobante', readonly=True, select=True)
    bank_debit = fields.Boolean(
        'Débito bancario',
        states={
            'readonly': ~((Eval('state') == 'draft') |
                ((Eval('state') == 'approved') & ~Bool(Eval('move'))))
        }, depends=_DEPENDS)
    description = fields.Text('Descripción', required=True,
        states=_STATES, depends=_DEPENDS)
    attached_documentation = fields.Text('Documentos habilitantes',
        states=_STATES, depends=_DEPENDS)
    notes = fields.Text('Observaciones', states=_STATES, depends=_DEPENDS)
    company = fields.Many2One('company.company', 'Empresa', required=True,
        select=True, readonly=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], depends=_DEPENDS)
    applicant = fields.Many2One('company.employee', 'Solicitante',
        states={'readonly': Eval('state') != 'draft',
                'required': True
                },
        domain=[
            ('company', '=', Eval('company'))
        ], depends=_DEPENDS + ['company'])
    created_by = fields.Many2One('company.employee', 'Creado por',
        domain=[
            ('contract_company', '=', Eval('company'))
        ], required=True, readonly=True, depends=_DEPENDS + ['company'])
    party = fields.Many2One('party.party', 'Tercero', required=True,
        select=True, states=_STATES, depends=_DEPENDS)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('approved', 'Aprobado'),
        ('processing', 'Procesando'),
        ('denied', 'Revisión'),
        ('cancel', 'Cancelado'),
        ], 'State', readonly=True, select=True)
    state_translated = state.translated('state')
    request_date = fields.Date('Fecha Solicitud',
        required=True, states=_STATES, depends=_DEPENDS)
    done_date = fields.Date('Fecha Finalizado',
        states=_STATES, depends=_DEPENDS)
    payment_date = fields.Function(fields.DateTime('Fecha Pago'),
        'get_spi_data', searcher='search_spi_date')
    concept = fields.Text('Concepto', states=_STATES, depends=_DEPENDS)
    approval_date = fields.Date('Fecha de aprobación', readonly=True)
    approved_by = employee_field("Aprobado por")
    number = fields.Char('Número', readonly=True, select=True)
    fiscalyear = fields.Many2One('account.fiscalyear', 'Año fiscal',
        domain=[
            ('company', '=', Eval('company')),
        ], depends=_DEPENDS + ['company'], required=True, states=_STATES)
    type = fields.Selection(
        [
            ('payslip.payslip', 'Roles de pagos'),
            ('payslip.advance', 'Pago Quincenal'),
            ('payslip.retired', 'Pago Jubilados'),
            ('company.employee.loan', 'Anticipos de empleados'),
            ('contract.liquidation', 'Liquidación de haberes'),
            ('credit_card.creditcard', 'Carta crédito - Importación'),
            ('credit_card.liquidation.creditcard',
             'Carta crédito - Importación (Ajuste)'),
            ('vacation.liquidation', 'Liquidación de vacaciones'),
            ('account.invoice', 'Facturas'),
            ('party.liquidation', 'Liquidación de tercero'),
            ('account.move', 'Asiento Cartera'),
            ('account.move.posted', 'Asiento de pago'),
            ('purchase.contract.process', 'Pago Contratos'),
            ('company.travel.expense', 'Gastos de Viaje'),
            ('treasury.petty.cash', 'Apertura de caja chica'),
        ], 'Tipo', required=True, states=_STATES)
    spi = fields.Function(
        fields.Many2One('treasury.account.payment.spi', 'SPI'),
        'get_spi_data', searcher='search_spi')
    spi_state = fields.Function(
        fields.Selection('get_states', 'Estado SPI'),
        'get_spi_data', searcher='search_spi_state')
    payslip_general = fields.Many2Many(
        'treasury.account.payment.request.roles', 'request', 'general',
        'Rol de pagos',
        states={
            'invisible': (Eval('type') != 'payslip.payslip'),
            'required': ((Eval('type') == 'payslip.payslip') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('state', '=', 'done'),
            ('template_for_accumulated_law_benefits' ,'=', Eval('is_accummulated_benefits')),
            # ('move', '!=', None),
            # ('move.state', '=', 'posted'),
            ]
        , depends=['state', 'type', 'party',
                   'template_for_accumulated_law_benefits' ])
    payslip_general_advance = fields.Many2Many(
        'treasury.account.payment.request.advance',
        'request', 'general_advance', 'Rol de Quincena',
        states={
            'required': ((Eval('type') == 'payslip.advance') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'invisible': (Eval('type') != 'payslip.advance'),
            'readonly': Eval('state') != 'draft'
        }, depends=['state', 'type', 'party'])
    payslip_general_retired = fields.Many2Many(
        'treasury.account.payment.request.retired', 'request', 'retired',
        'Rol de jubilados',
        states={
            'invisible': (Eval('type') != 'payslip.retired'),
            'required': ((Eval('type') == 'payslip.retired') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('state', '=', 'done'),
            ('template_for_accumulated_law_benefits' ,'=', Eval('is_accummulated_benefits')),
        ], depends=['state', 'type', 'party',
                    'template_for_accumulated_law_benefits'])
    request_lines_payslips = fields.Function(
        fields.One2Many('payslip.payslip', None, 'Líneas de Roles',
            states={
                'invisible': True,
            }), 'on_change_with_request_lines_payslips')
    request_lines_payslips_advance = fields.Function(
        fields.One2Many('payslip.advance', None, 'Líneas SPI Quincena.',
            states={
                'invisible': True,
            }), 'on_change_with_request_lines_payslips_advance')
    request_lines_payslips_retired = fields.Function(
        fields.One2Many('payslip.retired', None, 'Líneas rol jubilados.',
            states={
                'invisible': True,
            }), 'on_change_with_request_lines_payslips_retired')
    invoice = fields.Many2Many(
        'treasury.account.payment.request.invoice', 'request', 'invoice',
        'Facturas',
        states={
            'required': ((Eval('type') == 'account.invoice') &
                         (~Eval('state').in_(['draft', 'cancel', 'paid']))),
            'invisible': ((Eval('type') != 'account.invoice') &
                          ((Eval('type') != 'purchase.contract.process') |
                          (Eval('contract_type') != 'payment')
                          )),
            'readonly': Eval('state') != 'draft'
        },
        domain=[
            ('state', 'in', ['posted']),
            ('id', 'not in', Eval('contract_invoice_ids', [])),
            If(Eval('party'),
               ('party', '=', Eval('party')),
               ()),
        ], depends=['state', 'type', 'party'])
    contract_invoice_ids = fields.Function(fields.One2Many('account.invoice',
            None, 'Facturas Contratos',
            states={
                'invisible': True
            }
        ), 'on_change_with_contract_invoice_ids')
    account_move_request = fields.Many2Many(
        'treasury.account.payment.request.move', 'request', 'move', 'Asientos',
        states={
            'required': ((Eval('type').in_(
                ['account.move', 'account.move.posted'])) &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'invisible': ~(Eval('type').in_(
                ['account.move', 'account.move.posted']))
        },
        domain=[
            If(Eval('type') == 'account.move',
               [
                ('origin', '=', None),
                ('state', '=', 'posted'),
                ('lines', 'where', [
                    ('party', '=', Eval('party', -1)),
                    ('reconciliation', '=', None),
                ])
               ],  # noqa
               []  # noqa
            ),
            If(Eval('type') == 'account.move.posted',
               [
                   ('origin', '=', None),
                   ('state', '=', 'posted'),
                   ('lines', 'where', [
                       ('party', '=', Eval('party', -1)),
                       ('debit', '!=', 0),
                   ])
               ],  # noqa
               []  # noqa
               ),

        ], depends=['state', 'type', 'party'])
    invoice_payment_move_lines = fields.Function(
        fields.One2Many('account.move.line', None,
            'Líneas de asiento de facturas',
            states={
                'invisible': True,
            }), 'on_change_with_invoice_payment_move_lines')
    move_lines = fields.Function(
        fields.One2Many('account.move.line', None,
            'Líneas de asiento',
            states={
                'invisible': True,
            }), 'on_change_with_move_lines')
    invoice_selectable_move_payment_lines = fields.Function(
        fields.One2Many('account.move.line', None,
            'Líneas seleccionables de lineas de asiento de facturas',
            states={
                'invisible': True,
            }),
        'on_change_with_invoice_selectable_move_payment_lines')
    contract_process = fields.Many2Many(
        'treasury.account.payment.request.contract.process',
        'request', 'contract', 'Pago Contratos',
        states={
            'required': ((Eval('type') == 'purchase.contract.process') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'invisible': (Eval('type') != 'purchase.contract.process'),
            'readonly': Eval('state') != 'draft'
        }, domain=[
            ('state', '=', 'open'),
            ('party', '=', Eval('party', -1)),
        ], depends=['state', 'type', 'party'])
    contract_payment_lines = fields.Function(
        fields.One2Many('purchase.contract.process.payment', None,
            'Líneas de pago',
            states={
                'invisible': True,
            }), 'on_change_with_contract_payment_lines')
    liquidations = fields.Many2Many(
        'treasury.account.payment.request.party.liquidation',
        'request', 'liquidation', 'Liquidaciones',
        states={
            'required': ((Eval('type') == 'party.liquidation') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'invisible': (Eval('type') != 'party.liquidation'),
            'readonly': Eval('state') != 'draft'
        }, domain=[
            ('state', '=', 'done'),
            ('party', '=', Eval('party')),
        ], depends=['state', 'type', 'party'])
    total = fields.Function(fields.Numeric('Total'), 'on_change_with_total')
    request_lines = fields.One2Many(
        'treasury.account.payment.request.line.detail', 'request',
        'Detalle de solicitud',
        states={
            'required': (~Eval('state').in_(['draft', 'cancel'])),
        }, depends=['state'])
    payment_type = fields.Selection(
        [
            ('employee', 'Empleados'),
            ('party', 'Terceros'),
        ], 'Tipo de pago',
        states={
            'required': (Eval('type').in_(
                ['payslip.payslip', 'payslip.retired'])),
            'invisible': ~(Eval('type').in_(
                ['payslip.payslip', 'payslip.retired'])),
            'readonly': Eval('state') != 'draft'
        })
    contract_type = fields.Selection(
        [
            ('payment', 'Pago'),
            ('advance', 'Anticipo'),
        ], 'Tipo de pago',
        states={
            'required': (Eval('type') == 'purchase.contract.process'),
            'invisible': (Eval('type') != 'purchase.contract.process'),
            'readonly': Eval('state') != 'draft'
        })
    items = fields.Many2Many(
        'treasury.account.payment.request.payslip_rule', 'request', 'rule',
        'Rubros',
        context={
            'type': Eval('type'),
            'payslip_general': Eval('payslip_general'),
            'payslip_retired': Eval('payslip_general_retired'),
        },
        states={
            'invisible': ~((Eval('payment_type') == 'party') &
                (Eval('type').in_(['payslip.payslip', 'payslip.retired']))),
            'readonly': Eval('state') != 'draft',
            'required': ((Eval('payment_type') == 'party') &
                         (~Eval('state').in_(['draft', 'cancel']))),
        },
        domain=[
            ('party', '!=', None),
            ('type_', '=', 'deduction'),
        ], depends=[
            'payment_type', 'payslip_general', 'state', 'type',
            'payslip_general_retired'])
    move = fields.Many2One('account.move', 'Asiento', readonly=True,
        domain=[
            ('company', '=', Eval('company', -1)),
        ], depends=['company'])
    move_state = fields.Function(
        fields.Selection(
            [
                ('draft', 'Borrador'),
                ('posted', 'Contabilizado'),
            ], 'Estado Asiento',
            states={
                'readonly': True,
                'invisible': Eval(
                    'type') == 'account.move.posted',
            }, depends=['type']),
        'get_move_state', searcher='search_move_state')

    party_details = fields.Function(fields.One2Many(
        'party.party', None, 'Detalle de Terceros',
        # states={
        #     'invisible': ~Bool(Eval('party_details')),
        # },
        context={
            'payment_request_id': [Eval('id', -1)],
        }
    ), 'get_parties')

    travel_expenses = fields.Many2Many(
        'treasury.account.payment.request.travel.expense',
        'request', 'expense', 'Gastos de Viaje',
        states={
            'required': ((Eval('type') == 'company.travel.expense') &
                         (~Eval('state').in_(['draft', 'cancel']))),
            'invisible': (Eval('type') != 'company.travel.expense'),
            'readonly': False
        }, domain=[
            ('state', '=', 'request'),
            ('employee.party', '=', Eval('party', -1)),
        ], depends=['state', 'type', 'party'])

    total_to_pay = fields.Numeric('Monto para pago parcial', digits=(16, 2),
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Eval('state') != 'draft',
        }, depends=['state'])

    moves_related_account_move_posted = fields.Function(fields.Selection([
        (None, ''),
        ('associated', 'Asientos asociados'),
        ], 'Estado asientos de pago',
        states={
            'readonly': True,
            'invisible': Eval(
                'type') != 'account.move.posted',
        }, depends=['type']),
        'get_moves_related_account_move_posted')
    is_new_beneficiary = fields.Boolean('Usar nuevo beneficiario', states={
        'readonly': Bool(Eval('request_lines')),
    }, depends=['request_lines'], help='Al seleccionar este check, tendra la '
        'posibilidad de seleccionar un nuevo beneficiario para realizar la '
        'transferencia SPI')
    new_beneficiary = fields.Many2One('party.party', 'Nuevo beneficiario',
        states={
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('is_new_beneficiary')),
            'invisible': Bool(~Eval('is_new_beneficiary')),
        }, depends=['is_new_beneficiary'], help='Los datos de este beneficiario'
        ' serán usados para el pago')
    is_accummulated_benefits = fields.Boolean('Pago de beneficios acumulados',
        states={
            'readonly': False,
            'invisible': ((Eval('type') != 'payslip.payslip')&(Eval('type') != 'payslip.retired')),
          }, depends=['type'])

    @classmethod
    def __setup__(cls):
        super(PaymentRequest, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'processing'),
                ('denied', 'draft'),
                ('processing', 'approved'),
                ('processing', 'denied'),
                ('processing', 'draft'),
                ('processing', 'cancel'),
                ('approved', 'cancel'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': ~Eval('state').in_(['processing', 'denied']),
                    'icon': 'tryton-back',
                    'depends': ['state'],
                    },
                'process': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'icon': 'tryton-ok',
                    'depends': ['state'],
                    },
                'approve': {
                    'invisible': ~Eval('state').in_(['processing']),
                    'icon': 'tryton-ok',
                    'depends': ['state'],
                    },
                'denny': {
                    'invisible': ~Eval('state').in_(['processing']),
                    'icon': 'tryton-cancel',
                    'depends': ['state'],
                    },
                'cancel': {
                    'invisible': ~Eval('state').in_(['approved']),
                    'icon': 'tryton-cancel',
                    'depends': ['state'],
                    },
                'fill_lines': {
                    'readonly': ~Eval('state').in_(['draft']),
                    'depends': ['state']
                    },
                'open_move_wizard': {
                    'invisible': (~Eval('state').in_(['approved'])
                                  | (Eval('move_state').in_(['posted'])
                                  )
                                  | (Eval('type').in_(['account.move.posted']))
                                  ),
                    'depends': ['state']
                    },
                'cancel_request': {
                    'invisible': ~Eval('state').in_([
                        'approved', 'denied', 'draft', 'processing']),
                    'icon': 'tryton-cancel',
                    'depends': ['state']
                },
                'post_account_move_period': {
                    'invisible': (~Eval('state').in_(['approved'])
                                  | (Eval('move_state').in_(['posted'])
                                  )
                                  | (~Eval('type').in_(['account.move.posted']))
                                  | (Eval('moves_related_account_move_posted'))
                                  ),
                    'depends': ['state'],
                },
                'apply_amount_to_pay': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'depends': ['state'],
                },
                })
        cls._error_messages.update({
            'account_error': 'La cuenta contable: %(account)s es de tipo vista',
            'party_without_identifier': ('El tercero "%(party)s", no tiene'
                ' RUC.'),
            'no_user_bank': ('El empleado "%(user)s", no tiene configurado'
                ' una cuenta bancaria.'),
            'no_bank_bic': ('El banco "%(bank)s" no tiene configurado el'
                ' código BIC.'),
            'not_sequence': ('No se ha definido una secuencia para '
                'el año fiscal.'),
            'empty_request_lines': 'La solicitud no contiene líneas',
            'spi_not_paid': 'Aun no se encuentra pagado el SPI.',
            'empty_loan_account': 'El tipo de prestamo "%(type)s" no tiene una '
                'cuenta contable configurada',
            'empty_advance_account': ('No existe cuenta de anticipo configurada'
                '\nSe puede configurar una cuenta general en configuración '
                'contable.\n'
                'También se puede agregar una cuenta específica para el '
                'proveedor: %(party)s'),
            'delete_draft': 'La solicitud %s solo se puede eliminar en borrador',  # noqa
            'contract_invoice_registered': ('Ya existe una solicitud de pago '
                'previamente relacionada a la factura: %(invoice)s,'
                'en la solicitud de pago: %(request)s.\n'
                'Se requiere contabilizarla.'),
            'payment_move_without_lines': 'La solicitud de pago no cuenta con '
                'líneas de pago aprobadas',
            'generate_move_without_lines': 'No se generaron líneas en el '
                'asiento de pago.',
            'cancel_spi': 'No puede cancelar una solicitud enlazada a un SPI'
                'en estado pagado o generado',
            'without_reclassification': ('Cuenta %(account)s sin configuración'
                ' de reclasificación.'),
            'without_budget_previous_year': ('No se ha seleccionado partida '
                'para generar el asiento de pago de años anteriores.'),
            'empty_loan_travel_account': ('No existe una cuenta de anticipo '
                'configurada para tipos de viajes:'
                ' "%(type)s", gasto destino: %(spending_destiny)s'),
            'invalid_execution_move_line': ('No se puede asignar las solicitud '
                'de pago al asiento "%(move)s" porque ĺa línea de pago no '
                'representa un pago, o la cuenta contable no está '
                'definido como cuenta de conciliación bancaria"\n'
                'Cuenta:%(account)s \nDebito: %(debit)s \nCredito: %(credit)s'),
            'amount_over_total': ('El monto a pagar es superior al monto total '
                'de las lineas de solicitud')
            })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_applicant():
        return Transaction().context.get('employee')

    @staticmethod
    def default_created_by():
        return Transaction().context.get('employee')

    @staticmethod
    def default_payment_type():
        return 'employee'

    @staticmethod
    def default_contract_type():
        return 'payment'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_request_date():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        Date = Pool().get('ir.date')
        today = Date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @staticmethod
    def default_bank_debit():
        return False

    @fields.depends('contract_process', 'type', 'contract_type')
    def on_change_with_invoice(self, name=None):
        payment_lines = []
        if self.type and self.contract_type:
            if (self.type == 'purchase.contract.process'
                    and self.contract_type == 'payment'):
                for contract in self.contract_process:
                    for payment in contract.payments:
                        if payment.origin and payment.origin.state == 'posted':
                            payment_lines.append(payment.origin.id)
        return payment_lines

    @fields.depends('request_lines')
    def on_change_with_total(self, name=None):
        if self.request_lines:
            result = Decimal("0.0")
            for line in self.request_lines:
                if line.amount:
                    result += line.amount
            return result
        return Decimal("0.0")

    @fields.depends('is_new_beneficiary', 'new_beneficiary')
    def on_change_is_new_beneficiary(self):
        self.new_beneficiary = None

    @fields.depends('invoice')
    def on_change_with_contract_invoice_ids(self, names=None):
        pool = Pool()
        payment_ids = []
        ContractPayment = pool.get('purchase.contract.process.payment')
        payments = ContractPayment.search([
            ('id', '>', 0)
        ])
        for invoice in payments:
            if invoice.origin:
                payment_ids.append(invoice.origin.id)
        # TODO: remove below line
        # payment_ids = [int(str(row.origin).split(',')[1]) for row in payments if row.origin]  # noqa

        return payment_ids

    @classmethod
    def get_spi_data(cls, p_requests, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Line = pool.get('treasury.account.payment.request.line.detail')
        table = cls.__table__()
        line = Line.__table__()
        SPI = pool.get('treasury.account.payment.spi')
        spi = SPI.__table__()
        spis = defaultdict(lambda: None)
        states = defaultdict(lambda: None)
        dates = defaultdict(lambda: None)
        ids = [p.id for p in p_requests]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(line,
                condition=table.id == line.request
            ).join(spi,
                condition=spi.id == line.spi
            ).select(
                table.id,
                line.spi,
                spi.state,
                spi.payment_date,
                where=red_sql,
            )
            cursor.execute(*query)
            for r_id, spi_id, state, pdate in cursor.fetchall():
                spis[r_id] = spi_id
                states[r_id] = state
                dates[r_id] = pdate
        return {
            'spi': spis,
            'spi_state': states,
            'payment_date': dates,
        }

    @classmethod
    def get_parties(cls, requests, names):
        pool = Pool()
        table = cls.__table__()
        cursor = Transaction().connection.cursor()
        requests_ = defaultdict(lambda: [])
        PaymentRequestLine = pool.get(
            'treasury.account.payment.request.line.detail')
        payment_request_line = PaymentRequestLine.__table__()
        ids = [r.id for r in requests]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payment_request_line,
               condition=table.id == payment_request_line.request
            ).select(
                table.id,
                payment_request_line.party,
                where=red_sql
            )
            cursor.execute(*query)
            for request_id, party in cursor.fetchall():
                requests_[request_id].append(party)
        return {
            'party_details': requests_,
        }

    @classmethod
    def get_moves_related_account_move_posted(cls, requests, names):
        result = defaultdict(lambda: None)
        for request in requests:
            for line in request.request_lines:
                if ((request.type == 'account.move.posted') and
                        line.origin.payment_origin):
                    if ((line.origin.payment_origin == request) and (
                            line.state == 'paid')):
                        result[request.id] = 'associated'
                        break
                else:
                    result[request.id] = None
        return {
            'moves_related_account_move_posted': result
        }

    @classmethod
    def search_spi(cls, name, clause):
        return [('request_lines.spi' + clause[0].lstrip(name),) +
            tuple(clause[1:])]

    @classmethod
    def search_spi_state(cls, name, clause):
        return [('request_lines.spi.state',) + tuple(clause[1:])]

    @classmethod
    def search_spi_date(cls, name, clause):
        return [('request_lines.spi.payment_date',) + tuple(clause[1:])]

    @staticmethod
    def get_states():
        pool = Pool()
        SPI = pool.get('treasury.account.payment.spi')
        return SPI.state.selection

    @classmethod
    def set_move_line_payment_origin(cls, transfer):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        for line in transfer.request_lines:
            move_line = MoveLine.browse([line.origin.id])[0]
            move_line.payment_origin = transfer
            MoveLine.save([move_line])

    @classmethod
    def set_request_petty_cash(cls, transfer):
        pool = Pool()
        Pcash = pool.get('treasury.petty.cash')
        for line in transfer.request_lines:
            pcash = None
            if str(line.origin).startswith('treasury.petty'):
                pcash = Pcash.browse([line.origin.id])[0]
                pcash.request = transfer
            if pcash:
                Pcash.save([pcash])

    @classmethod
    def remove_move_line_payment_origin(cls, transfer):
        types = ['account.move', 'account.invoice', 'account.move.posted']
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        lines_to_save = []
        if transfer.type in types:
            for line in transfer.request_lines:
                move_line = MoveLine.browse([line.origin.id])[0]
                move_line.payment_origin = Null
                lines_to_save.append(move_line)
            MoveLine.save(lines_to_save)

    @classmethod
    def remove_request_petty_cash(cls, transfer):
        pool = Pool()
        Pcash = pool.get('treasury.petty.cash')
        lines_to_save = []
        for line in transfer.request_lines:
            pcash = Pcash.browse([line.origin.id])[0]
            pcash.request = Null
            lines_to_save.append(pcash)
        Pcash.save(lines_to_save)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, p_requests):
        pass

    @classmethod
    @Workflow.transition('processing')
    def process(cls, p_requests):
        pool = Pool()
        RequestInvoiceLine = pool.get(
            'treasury.account.payment.request.invoice')
        types = ['account.move', 'account.invoice', 'account.move.posted']
        for request in p_requests:
            requestInvoiceLines = RequestInvoiceLine.search([
                ('invoice', 'in', [invoice.id for invoice in request.invoice]),
                ('request', '!=', request.id),
            ])
            for requestInvoiceLine in requestInvoiceLines:
                if (requestInvoiceLine.request.state != 'cancel'
                        and requestInvoiceLine.request.state != 'posted'
                        and requestInvoiceLine.request.move
                        and requestInvoiceLine.request.move.state == 'draft'):
                    cls.raise_user_error('contract_invoice_registered', {
                        'invoice': requestInvoiceLine.invoice.rec_name,
                        'request': requestInvoiceLine.request.number
                    })
            for line in request.request_lines:
                if not line.party.tax_identifier:
                    if line.party.relations[0]:
                        if not line.party.relations[0].to.tax_identifier.code:
                            cls.raise_user_error('party_without_identifier', {
                                'party': line.party.name
                            })
                    else:
                        cls.raise_user_error('party_without_identifier', {
                            'party': line.party.name
                        })
                if (line.bank and not line.bank_account.numbers
                        and request.bank_debit is False):
                    cls.raise_user_error('no_user_bank', {
                        'user': line.party.name
                    })
                if not line.bank and request.bank_debit is False:
                    cls.raise_user_error('no_user_bank', {
                        'user': line.party.name
                    })
                if (line.bank
                        and not line.bank.bic and request.bank_debit is False):
                    cls.raise_user_error('no_bank_bic', {
                        'bank': line.bank.party.name
                    })
            if request.type in types:
                cls.set_move_line_payment_origin(request)
            cls.set_request_petty_cash(request)
            cls.set_number(p_requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approve(cls, p_requests):
        cls.set_receipt(p_requests)

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, p_requests):
        for request in p_requests:
            if request.spi:
                if request.spi_state not in ['cancel', 'partially_paid']:
                    cls.raise_user_error('cancel_spi')
            for line in request.request_lines:
                line.state = 'cancel'
                line.save()
            cls.remove_move_line_payment_origin(request)
            cls.remove_request_petty_cash(request)

    @classmethod
    @ModelView.button
    @Workflow.transition('denied')
    def denny(cls, p_requests):
        pool = Pool()
        Loan = pool.get('company.employee.loan')
        for payment in p_requests:
            if payment.type == 'company.employee.loan':
                for line in payment.request_lines:
                    Loan.cancel([line.origin])

    @classmethod
    @ModelView.button_action('treasury.act_request_fill_lines')
    def fill_lines(cls, p_requests):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('post')
    def post(cls, p_requests):
        pass

    @classmethod
    @ModelView.button
    def post_account_move_period(cls, p_requests):
        pool = Pool()
        AccountMoveLine = pool.get('account.move.line')
        lines_to_modify = []
        for p_request in p_requests:
            for request_line in p_request.request_lines:
                if request_line.state != 'paid':
                    continue
                account_move_line = request_line.origin
                if account_move_line.payment_origin is None:
                    if (account_move_line.account.bank_reconcile):
                        account_move_line.payment_origin = p_request
                        lines_to_modify.append(account_move_line)
                    else:
                        cls.raise_user_error('invalid_execution_move_line', {
                            'move': account_move_line.move.number,
                            'account': account_move_line.account.code,
                            'debit': account_move_line.debit,
                            'credit': account_move_line.credit
                        })
        AccountMoveLine.save(lines_to_modify)

    @classmethod
    @ModelView.button
    def apply_amount_to_pay(cls, p_requests):
        pool = Pool()
        PaymentLine = pool.get('treasury.account.payment.request.line.detail')
        total_counted = 0

        lines_to_remove = []
        lines_to_save = []
        for p_request in p_requests:
            if p_request.total_to_pay > p_request.total:
                cls.raise_user_error('amount_over_total')
            pending = p_request.total_to_pay
            if p_request.total_to_pay:
                if p_request.total_to_pay > 0:
                    for line in p_request.request_lines:
                        if line.amount <= pending:
                            total_counted += line.amount
                            pending -= line.amount
                            continue
                        elif line.amount > pending:
                            line.amount = pending
                            pending = 0.0
                            lines_to_save.append(line)
                        if line.amount == 0.0:
                            lines_to_remove.append(line)
        PaymentLine.save(lines_to_save)
        PaymentLine.delete(lines_to_remove)

    @classmethod
    @ModelView.button_action('treasury.act_payment_request_generate_move')
    def open_move_wizard(cls, p_requests):
        pass

    @classmethod
    def delete(cls, p_requests):
        for request in p_requests:
            if request.state != 'draft':
                cls.raise_user_error('delete_draft', request.rec_name)
        return super(PaymentRequest, cls).delete(p_requests)

    def fill_contract_move_no_invoice(self, move, bank_account):
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        total_amount = Decimal("0.0")
        total_amount_aux = Decimal("0.0")
        contract_lines = []
        dict_contract_invoices = {}
        total_request_lines = Decimal("0.0")
        for request in self.request_lines:
            if request.state != 'paid':
                continue
            payment = request.origin
            total_request_lines += request.amount
            if payment.origin:
                if payment.origin.state == 'posted':
                    contract_lines.extend(payment.origin.lines_to_pay)
                    dict_contract_invoices[payment.origin.number] = str(
                        payment.origin.number)
        if len(contract_lines) == 0:
            self.raise_user_error('payment_move_without_lines')
        for line in contract_lines:
            if total_amount == total_request_lines:
                break
            oline = line
            if line.reconciliation:
                continue
            if oline.pending_amount == Decimal("0.0"):
                continue
            total_amount_aux += oline.pending_amount
            if total_amount_aux > total_request_lines:
                diff = total_amount_aux - total_request_lines
                pending_amount = oline.pending_amount - diff
            else:
                pending_amount = oline.pending_amount
            if pending_amount == 0:
                continue
            new_line = self.get_move_line(
                move,
                oline.account,
                pending_amount,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': line.party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            new_line.payable_move_line = oline
            if oline.parent:
                if oline.parent.compromise not in move.compromises:
                    move.compromises += (oline.parent.compromise,)
                new_line.compromise = oline.parent.compromise
                new_line.budget = oline.parent.budget
                new_line.cost_center = oline.parent.cost_center
            else:
                if oline.move.compromises:
                    if len(oline.move.compromises) == 1:
                        if oline.move.compromises[0] not in move.compromises:
                            move.compromises += (oline.move.compromises[0],)
                        new_line.compromise = oline.move.compromises[0]
                    if oline.payable_budget:
                        new_line.budget = oline.payable_budget
                        new_line.on_change_budget()
                    else:
                        if (new_line.compromise and (
                                len(new_line.compromise.budgets) == 1)):
                            new_line.budget = new_line.compromise.budgets[0]
                            new_line.on_change_budget()
            if not new_line.compromise or not new_line.budget:
                move.type = 'adjustment'
                new_line.adjustment = True
            move.lines += (new_line,)
            total_amount += pending_amount

        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        move.lines += (bank_line,)
        move.save()
        return move

    def fill_loan_move(self, move, bank_account):
        # cuenta de préstamo
        pool = Pool()
        EmployeeLoan = pool.get('company.employee.loan')
        EmployeeLoanTypeAccount = pool.get('company.employee.loan.type.account')
        TravelExpense = pool.get('company.travel.expense')
        TravelExpenseAccount = pool.get('travel.expense.account')
        PayslipAdvance = pool.get('payslip.advance')
        move.origin = self
        total_amount = Decimal("0.0")
        for line in self.request_lines:
            if line.state != 'paid':
                continue
            loan_account = None
            loan_type = None
            origin = line.origin
            if isinstance(origin, EmployeeLoan):
                loan_type = line.origin.type
            elif isinstance(origin, PayslipAdvance):
                loan_type = line.origin.loan.type

            if loan_type and loan_type.account_lines:
                template = line.origin.template
                account_line = EmployeeLoanTypeAccount.search([
                    ('payslip_template', '=', template),
                    ('loan_type', '=', loan_type)
                ])
                if account_line:
                    loan_account = account_line[0].account
            elif isinstance(origin, TravelExpense):
                account_line = TravelExpenseAccount.search([
                    ('kind_expense', '=', 'advance'),
                    ('national_travel', '=', line.origin.travel_type),
                    ('spending_destiny', '=', line.origin.spending_destiny)
                ])
                if account_line:
                    loan_account = account_line[0].advance_account
                else:
                    self.raise_user_error('empty_loan_travel_account', {
                        'type': line.origin.travel_type_translated,
                        'spending_destiny': line.origin.spending_destiny_translated,  # noqa
                    })
            else:
                loan_account = loan_type.account
            if loan_account is None:
                self.raise_user_error('empty_loan_account', {
                    'type': loan_type.name
                })
            new_line = self.get_move_line(
                move,
                loan_account,
                line.amount,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': line.party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (new_line,)
            total_amount += line.amount
        if len(move.lines) == 0:
            self.raise_user_error('generate_move_without_lines')
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        move.lines += (bank_line,)
        move.save()
        return move

    def fill_party_move(self, move, bank_account, budget=None, compromise=None):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        LiquidationLine = pool.get('party.liquidation.line')
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        total_amount = Decimal("0.0")
        for line in self.request_lines:
            if not self.bank_debit and line.state != 'paid':
                continue
            origin = line.origin
            if isinstance(origin, MoveLine):
                oline = line.origin
            elif isinstance(origin, LiquidationLine):
                oline = line.origin.move_line
            if oline.account.previous_year:
                Reclassification = pool.get('account.account.reclassification')
                reclassification = Reclassification.search([
                    ('company', '=', self.company.id),
                    ('fiscalyear', '=', self.fiscalyear.id),
                    ('account', '=', oline.account.id)
                ])
                if reclassification:
                    paccount = reclassification[0].reclassification_account
                else:
                    self.raise_user_error('without_reclassification', {
                        'account': oline.account.rec_name
                    })
                # Create accrued line
                if compromise is None:
                    if move.budget_impact_direct is False:
                        move.budget_impact_direct = True
                else:
                    move.compromises += (compromise,)
                if budget is None:
                    self.raise_user_error('without_budget_previous_year')
                accrued_line = MoveLine(
                    move=move,
                    account=oline.account,
                    debit=line.amount,
                    credit=0,
                    compromise=compromise,
                    budget=budget,
                    party=line.party,
                    cost_center=1
                )
                MoveLine.save([accrued_line])
                credit_line = MoveLine(
                    move=move,
                    account=paccount,
                    debit=0,
                    credit=line.amount,
                    party=line.party,
                    parent=accrued_line
                )
                MoveLine.save([credit_line])
                new_line = MoveLine(
                    move=move,
                    account=paccount,
                    debit=line.amount,
                    credit=0,
                    party=line.party,
                    compromise=compromise,
                    budget=budget,
                    cost_center=1,
                    payable_move_line=credit_line
                )
                MoveLine.save([new_line])
                move.lines += (accrued_line, credit_line, new_line,)
            else:
                new_line = self.get_move_line(
                    move,
                    oline.account,
                    line.amount,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': line.party,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                new_line.payable_move_line = oline
                if oline.parent:
                    if oline.parent.compromise not in move.compromises:
                        move.compromises += (oline.parent.compromise,)
                    new_line.compromise = oline.parent.compromise
                    new_line.budget = oline.parent.budget
                    new_line.cost_center = oline.parent.cost_center
                else:
                    if oline.move.compromises:
                        if len(oline.move.compromises) == 1:
                            if oline.move.compromises[0] not in move.compromises:  # noqa
                                move.compromises += (oline.move.compromises[0],)
                            new_line.compromise = oline.move.compromises[0]
                        if oline.payable_budget:
                            new_line.budget = oline.payable_budget
                            new_line.on_change_budget()
                        else:
                            if (new_line.compromise and (
                                    len(new_line.compromise.budgets) == 1)):
                                new_line.budget = new_line.compromise.budgets[0]
                                new_line.on_change_budget()
                if not new_line.compromise or not new_line.budget:
                    move.type = 'adjustment'
                    new_line.adjustment = True
                if new_line.budget and new_line.cost_center is None:
                    new_line.cost_center = 1
                move.lines += (new_line,)
            total_amount += line.amount
        if len(move.lines) == 0:
            self.raise_user_error('generate_move_without_lines')
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        move.lines += (bank_line,)
        move.save()
        return move

    def fill_advance_move(self, move, bank_account):
        # cuenta de préstamo
        pool = Pool()
        Configuration = pool.get('account.configuration')
        advance_account = None

        if self.party.advance_account:
            advance_account = self.party.advance_account
        else:
            account_config = Configuration(1)
            if account_config.advance_account:
                advance_account = account_config.advance_account

        if advance_account is None:
            self.raise_user_error('empty_advance_account', {
                'party': self.party.rec_name,
            })

        move.origin = self
        total_amount = Decimal("0.0")
        for line in self.request_lines:
            if line.state != 'paid':
                continue
            new_line = self.get_move_line(
                move,
                advance_account,
                line.amount,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': line.party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (new_line,)
            total_amount += line.amount
        if len(move.lines) == 0:
            self.raise_user_error('generate_move_without_lines')
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        move.lines += (bank_line,)
        move.save()
        return move

    def fill_petty_cash_move(self, move, bank_account):
        pool = Pool()
        Configuration = pool.get('treasury.petty.cash.configuration')
        advance_account = None

        account_config = Configuration(1)
        if account_config.account:
            advance_account = account_config.account

        if advance_account is None:
            self.raise_user_error('empty_advance_account', {
                'party': self.party.rec_name,
            })

        total_amount = Decimal("0.0")
        for line in self.request_lines:
            if line.state != 'paid':
                continue
            new_line = self.get_move_line(
                move,
                advance_account,
                line.amount,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': line.party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (new_line,)
            total_amount += line.amount
        if len(move.lines) == 0:
            self.raise_user_error('generate_move_without_lines')
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        move.lines += (bank_line,)
        move.save()
        return move

    def fill_liquidation_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Account = pool.get('account.account')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Liquidation = pool.get('contract.liquidation')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_line = MoveLine.__table__()
        liquidation = Liquidation.__table__()

        query_lines = request_line.select(
            request_line.request,
            request_line.party,
            request_line.origin,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, request_line.party,
                request_line.origin]
        )

        query = request.join(query_lines,
            condition=request.id == query_lines.request
        ).join(liquidation,
            condition=query_lines.origin == Concat(
                'contract.liquidation,', liquidation.id)
        ).join(move_t,
            condition=liquidation.move == move_t.id
        ).join(move_line,
            condition=(move_t.id == move_line.move)
        ).select(
            move_line.id,
            move_line.move,
            move_line.account,
            move_line.debit,
            move_line.credit,
            move_line.party,
            move_line.parent,
            move_line.payable_budget,
            move_line.payable_cost_center,
            move_line.compromise,
            liquidation.id.as_('liquidation'),
            where=(
                (request.id == self.id) & ((move_line.parent != Null) |
                (move_line.payable_budget != Null)) &
                (move_line.reconciliation == Null) &
                (move_t.state == 'posted') &
                (move_line.party == query_lines.party))
        )
        # cursor.execute(*query)

        liquidations = []
        LawBenefitXIII = pool.get(
            'contract.liquidation.law.benefits.xiii.lines')
        LawBenefitXIV = pool.get('contract.liquidation.law.benefits.xiv.lines')
        LawBenefit = pool.get('payslip.law.benefit')
        PaysplipPayslip = pool.get('payslip.payslip')
        PaysplipGeneral = pool.get('payslip.general')

        law_benefit_xiii = LawBenefitXIII.__table__()
        law_benefit_xiv = LawBenefitXIV.__table__()
        law_benefit = LawBenefit.__table__()
        move_tbl = Move.__table__()
        move_line = MoveLine.__table__()
        payslip_payslip = PaysplipPayslip.__table__()
        payslip_general = PaysplipGeneral.__table__()

        for line in self.request_lines:
            if line.state != 'paid':
                continue
            liquidations.append(line.origin.id)
        queryxiii = liquidation.join(law_benefit_xiii,
                   condition=liquidation.id == law_benefit_xiii.liquidation
            ).join(law_benefit,
                   condition=law_benefit_xiii.xiii_line == law_benefit.id
            ).join(payslip_payslip,
                   condition=law_benefit.generator_payslip == payslip_payslip.id
            ).join(payslip_general,
                   condition=payslip_payslip.general == payslip_general.id
            ).join(move_tbl,
                   condition=payslip_general.move_lb == move_tbl.id
            ).join(move_line,
                   condition=move_tbl.id == move_line.move,
            ).select(
                move_line.id.as_('move_line_id'),
                move_line.move.as_('move'),
                move_line.account.as_('move_line_account'),
                move_line.debit.as_('debit'),
                move_line.credit.as_('credit'),
                move_line.party.as_('party'),
                move_line.parent.as_('parent'),
                move_line.payable_budget.as_('payable_budget'),
                move_line.payable_cost_center.as_('payable_cost_center'),
                move_line.compromise.as_('compromise'),
                liquidation.id.as_('liquidation'),
                where=(
                    (move_tbl.state == 'posted') &
                    (move_line.id != Null) &
                    (liquidation.id.in_(liquidations)) &
                    (law_benefit.amount == move_line.credit) &
                    (move_line.party == self.party.id) &
                    (payslip_payslip.fiscalyear == self.fiscalyear.id)

                ),
            )
        # (liquidation.id in liquidations) &
        queryxiv = liquidation.join(law_benefit_xiv,
                   condition=liquidation.id == law_benefit_xiv.liquidation
            ).join(law_benefit,
                   condition=law_benefit_xiv.xiv_line == law_benefit.id
            ).join(payslip_payslip,
                   condition=law_benefit.generator_payslip == payslip_payslip.id
            ).join(payslip_general,
                   condition=payslip_payslip.general == payslip_general.id
            ).join(move_tbl,
                   condition=payslip_general.move_lb == move_tbl.id
            ).join(move_line,
                   condition=move_tbl.id == move_line.move,
            ).select(
                move_line.id.as_('move_line_id'),
                move_line.move.as_('move'),
                move_line.account.as_('move_line_account'),
                move_line.debit.as_('debit'),
                move_line.credit.as_('credit'),
                move_line.party.as_('party'),
                move_line.parent.as_('parent'),
                move_line.payable_budget.as_('payable_budget'),
                move_line.payable_cost_center.as_('payable_cost_center'),
                move_line.compromise.as_('compromise'),
                liquidation.id.as_('liquidation'),
                where=(
                    (move_tbl.state == 'posted') &
                    (move_line.id != Null) &
                    (liquidation.id.in_(liquidations)) &
                    (law_benefit.amount == move_line.credit) &
                    (move_line.party == self.party.id) &
                    (payslip_payslip.fiscalyear == self.fiscalyear.id)

                ),
            )
        union_query = Union(queryxiii, queryxiv, all_=True)
        query = Union(query, union_query, all_=True)

        cursor.execute(*query)
        total_amount = Decimal("0.0")
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        accounts = {}
        moves = {}
        for row in cursor_dict(cursor):
            mline = MoveLine(row['id'])
            pending_amount = Decimal(str(mline.pending_amount))
            if pending_amount > Decimal('0.0'):
                if not accounts.get(row['account']):
                    accounts[row['account']] = Account(row['account'])
                if not moves.get(row['move']):
                    moves[row['move']] = Move(row['move'])
                new_line = self.get_move_line(
                    move,
                    accounts[row['account']],
                    pending_amount,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': row['party'],
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                new_line.payable_move_line = mline
                if row['parent']:
                    oline = MoveLine(row['parent'])
                    if oline.compromise not in move.compromises:
                        move.compromises += (oline.compromise,)
                    new_line.compromise = oline.compromise
                    new_line.budget = oline.budget
                    new_line.cost_center = oline.cost_center
                else:
                    if moves[row['move']].compromises:
                        if len(moves[row['move']].compromises) == 1:
                            if (moves[row['move']].compromises[0] not in
                                    move.compromises):
                                move.compromises += (
                                    moves[row['move']].compromises[0],)
                            new_line.compromise = moves[row['move']].compromises[0]  # noqa
                        if row['payable_budget']:
                            new_line.budget = row['payable_budget']
                            new_line.on_change_budget()
                        else:
                            if (new_line.compromise and (
                                    len(new_line.compromise.budgets) == 1)):
                                new_line.budget = new_line.compromise.budgets[0]
                                new_line.on_change_budget()
                if not new_line.compromise or not new_line.budget:
                    move.type = 'adjustment'
                    new_line.adjustment = True
                move.lines += (new_line,)
                total_amount += pending_amount
        if total_amount > Decimal('0.0'):
            bank_line = self.get_move_line(
                move,
                bank_account,
                0,
                total_amount, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (bank_line,)
            show_moves([move])
            move.save()
            return move
        else:
            return None

    def fill_vacation_liquidation_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Account = pool.get('account.account')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        VacationLiquidation = pool.get('vacation.liquidation')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_line = MoveLine.__table__()
        vacation_liquidation = VacationLiquidation.__table__()

        query_lines = request_line.select(
            request_line.request,
            request_line.party,
            request_line.origin,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, request_line.party,
                request_line.origin]
        )

        query = request.join(query_lines,
            condition=request.id == query_lines.request
        ).join(vacation_liquidation,
            condition=query_lines.origin == Concat(
                'vacation.liquidation,', vacation_liquidation.id)
        ).join(move_t,
            condition=vacation_liquidation.move == move_t.id
        ).join(move_line,
            condition=(move_t.id == move_line.move)
        ).select(
            move_line.id,
            move_line.move,
            move_line.account,
            move_line.debit,
            move_line.credit,
            move_line.party,
            move_line.parent,
            move_line.payable_budget,
            move_line.payable_cost_center,
            move_line.compromise,
            vacation_liquidation.id.as_('liquidation'),
            where=(
                (request.id == self.id) & ((move_line.parent != Null) |
                (move_line.payable_budget != Null)) &
                (move_line.reconciliation == Null) &
                (move_t.state == 'posted') &
                (move_line.party == query_lines.party))
        )
        cursor.execute(*query)
        total_amount = Decimal("0.0")
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        accounts = {}
        moves = {}
        for row in cursor_dict(cursor):
            mline = MoveLine(row['id'])
            pending_amount = Decimal(str(mline.pending_amount))
            if pending_amount > Decimal('0.0'):
                if not accounts.get(row['account']):
                    accounts[row['account']] = Account(row['account'])
                if not moves.get(row['move']):
                    moves[row['move']] = Move(row['move'])
                new_line = self.get_move_line(
                    move,
                    accounts[row['account']],
                    pending_amount,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': row['party'],
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                new_line.payable_move_line = mline
                if row['parent']:
                    oline = MoveLine(row['parent'])
                    if oline.compromise not in move.compromises:
                        move.compromises += (oline.compromise,)
                    new_line.compromise = oline.compromise
                    new_line.budget = oline.budget
                    new_line.cost_center = oline.cost_center
                else:
                    if moves[row['move']].compromises:
                        if len(moves[row['move']].compromises) == 1:
                            if (moves[row['move']].compromises[0] not in
                                    move.compromises):
                                move.compromises += (
                                    moves[row['move']].compromises[0],)
                            new_line.compromise = moves[row['move']].compromises[0]  # noqa
                        if row['payable_budget']:
                            new_line.budget = row['payable_budget']
                            new_line.on_change_budget()
                        else:
                            if (new_line.compromise and (
                                    len(new_line.compromise.budgets) == 1)):
                                new_line.budget = new_line.compromise.budgets[0]
                                new_line.on_change_budget()
                if not new_line.compromise or not new_line.budget:
                    move.type = 'adjustment'
                    new_line.adjustment = True
                move.lines += (new_line,)
                total_amount += pending_amount
        if total_amount > Decimal('0.0'):
            bank_line = self.get_move_line(
                move,
                bank_account,
                0,
                total_amount, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (bank_line,)
            # show_moves([move])
            move.save()
            return move
        else:
            return None

    def fill_payslip_general_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Account = pool.get('account.account')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general')
        RequestPayslip = pool.get('treasury.account.payment.request.roles')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()

        query_lines = request_line.select(
            request_line.request,
            request_line.party,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, request_line.party]
        )

        query = request.join(query_lines,
            condition=request.id == query_lines.request
        ).join(payslip_request,
            condition=request.id == payslip_request.request
        ).join(payslip,
            condition=payslip_request.general == payslip.id
        ).join(move_t,
            condition=payslip.move == move_t.id
        ).join(move_line,
            condition=(move_t.id == move_line.move)
        ).select(
            move_line.id,
            move_line.move,
            move_line.account,
            move_line.debit,
            move_line.credit,
            move_line.party,
            move_line.parent,
            move_line.payable_budget,
            move_line.payable_cost_center,
            move_line.compromise,
            payslip.id.as_('general'),
            where=((request.id == self.id) & ((move_line.parent != Null) |
                (move_line.payable_budget != Null)) &
                (move_line.reconciliation == Null) &
                (move_t.state == 'posted') &
                (move_line.party == query_lines.party))
        )
        cursor.execute(*query)
        total_amount = Decimal("0.0")
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        # In order to do execution on payslip move.  First we need to
        # find accrued account.move.line from each account.move for every
        # payslip selected that contains the same party that request line
        accounts = {}
        moves = {}
        for row in cursor_dict(cursor):
            mline = MoveLine(row['id'])
            pending_amount = Decimal(str(mline.pending_amount))
            if pending_amount > Decimal('0.0'):
                if not accounts.get(row['account']):
                    accounts[row['account']] = Account(row['account'])
                if not moves.get(row['move']):
                    moves[row['move']] = Move(row['move'])
                new_line = self.get_move_line(
                    move,
                    accounts[row['account']],
                    pending_amount,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': row['party'],
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                new_line.payable_move_line = mline
                if row['parent']:
                    oline = MoveLine(row['parent'])
                    if oline.compromise not in move.compromises:
                        move.compromises += (oline.compromise,)
                    new_line.compromise = oline.compromise
                    new_line.budget = oline.budget
                    new_line.cost_center = oline.cost_center
                else:
                    if moves[row['move']].compromises:
                        if len(moves[row['move']].compromises) == 1:
                            if (moves[row['move']].compromises[0] not in
                                    move.compromises):
                                move.compromises += (
                                    moves[row['move']].compromises[0],)
                            new_line.compromise = moves[row['move']].compromises[0]  # noqa
                        if row['payable_budget']:
                            new_line.budget = row['payable_budget']
                            new_line.on_change_budget()
                        else:
                            if (new_line.compromise and (
                                    len(new_line.compromise.budgets) == 1)):
                                new_line.budget = new_line.compromise.budgets[0]
                                new_line.on_change_budget()
                if not new_line.compromise or not new_line.budget:
                    move.type = 'adjustment'
                    new_line.adjustment = True
                move.lines += (new_line,)
                total_amount += pending_amount
        if total_amount > Decimal('0.0'):
            bank_line = self.get_move_line(
                move,
                bank_account,
                0,
                total_amount, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (bank_line,)
            # show_moves([move])
            move.save()
            return move
        else:
            return None


    def fill_payslip_retired_accummulated_move_parties(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()

        Party = pool.get('party.party')
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        Employee = pool.get('company.employee')
        PayslipLine = pool.get('payslip.retired.line')
        PayslipPayslip = pool.get('payslip.retired')
        PayslipGeneral = pool.get('payslip.general.retired')
        AccountMove = pool.get('account.move')
        AccountMoveLine = pool.get('account.move.line')
        Budget = pool.get('public.budget')
        Account = pool.get('account.account')
        AccountClose = pool.get('account.account.close')
        AccountReclassification = pool.get('account.account.reclassification')

        party_party = Party.__table__()
        payslip_law_benefit = LawBenefit.__table__()
        period = Period.__table__()
        company_employee = Employee.__table__()
        payslip_payslip = PayslipPayslip.__table__()
        payslip_payslip_lb = PayslipPayslip.__table__()
        payslip_line = PayslipLine.__table__()
        payslip_payslip_gen = PayslipPayslip.__table__()
        payslip_general = PayslipGeneral.__table__()
        account_move = AccountMove.__table__()
        account_move_line_lxp = AccountMoveLine.__table__()
        account_move_line_ldev = AccountMoveLine.__table__()
        account_close = AccountClose.__table__()
        account_reclassification = AccountReclassification.__table__()

        Account = pool.get('account.account')
        CostCenter = pool.get('public.cost.center')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveComp = pool.get('account.move-allowed-budget.compromise')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general')
        RequestPayslip = pool.get('treasury.account.payment.request.retired')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_comp = MoveComp.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()
        cost_centers = CostCenter.search([])
        default_cost_center = None
        if len(cost_centers) > 0:
            default_cost_center = cost_centers[0]
        query_request_lines = request_line.select(
            request_line.request,
            request_line.amount,
            request_line.party,
            request_line.origin,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
        )
        cursor.execute(*query_request_lines)
        result_request_lines = cursor.fetchall()
        lines = defaultdict(lambda: [])
        for (request, amount, party, origin) in result_request_lines:
            lines[int(origin.split(',')[1])] = [request, amount, party, int(origin.split(',')[1])]
        lines_to_pay = payslip_line.join(payslip_payslip,
                                         condition=payslip_line.payslip == payslip_payslip.id
                                         ).join(payslip_law_benefit,
                                                condition=(payslip_law_benefit.payslip == payslip_payslip.id) &
                                                          (payslip_law_benefit.kind != 'monthlyse')
                                                ).join(period,
                                                       condition=payslip_law_benefit.period == period.id
                                                       ).join(company_employee,
                                                              condition=payslip_payslip.employee == company_employee.id
                                                              ).join(party_party,
                                                                     condition=company_employee.party == party_party.id
                                                                     ).join(payslip_payslip_lb,
                                                                            condition=payslip_law_benefit.generator_payslip == payslip_payslip_lb.id
                                                                            ).join(payslip_general,
                                                                                   condition=payslip_payslip_lb.general == payslip_general.id
                                                                                   ).join(account_move, type_='LEFT',
                                                                                          condition=((payslip_general.move_lb == account_move.id) &
                                                                                                     (account_move.state == 'posted'))
                                                                                          ).join(move_comp,type_='LEFT',
                                                                                                 condition=(move_comp.move == account_move.id)
                                                                                                 ).join(account_move_line_lxp, type_='LEFT',
                                                                                                        condition=((account_move_line_lxp.move == account_move.id) &
                                                                                                                   (payslip_law_benefit.payable_account == account_move_line_lxp.account) &
                                                                                                                   (payslip_law_benefit.amount == account_move_line_lxp.credit) &
                                                                                                                   (party_party.id == account_move_line_lxp.party) &
                                                                                                                   (account_move_line_lxp.reconciliation == Null)&
                                                                                                                   ((account_move_line_lxp.parent == '383789')|(account_move_line_lxp.parent == '384549'))),
                                                                                                        ).join(account_move_line_ldev, type_='LEFT',
                                                                                                               condition=account_move_line_lxp.parent == account_move_line_ldev.id
                                                                                                               ).join(account_close, type_='LEFT',
                                                                                                                      # condition=(account_move_line_lxp.account == account_close.account) &
                                                                                                                      condition=(payslip_law_benefit.payable_account == account_close.account) &
                                                                                                                                (account_close.fiscalyear == period.fiscalyear)
                                                                                                                      ).join(account_reclassification, type_='LEFT',
                                                                                                                             condition=(account_close.to_account == account_reclassification.account) &  # noqa
                                                                                                                                       (account_reclassification.fiscalyear == period.fiscalyear)
                                                                                                                             ).select(
            payslip_line.id.as_('payslip_line_id'),
            payslip_payslip.total_value.as_('total_value'),
            payslip_payslip.total_deduction.as_('total_deduction'),
            payslip_law_benefit.amount.as_('amount'),
            payslip_law_benefit.kind.as_('kind'),
            payslip_law_benefit.period.as_('period'),
            period.fiscalyear.as_('fiscalyear'),
            payslip_law_benefit.budget.as_('budget'),
            payslip_law_benefit.expense_account.as_('expense'),
            payslip_law_benefit.payable_account.as_('payable'),
            account_move_line_lxp.id.as_('move_lxp'),
            move_comp.compromise.as_('move_compromise'),
            account_move_line_lxp.account.as_('move_payable_account'),
            party_party.id.as_('party'),
            account_move_line_ldev.compromise.as_('compromise'),
            account_move_line_lxp.payable_budget.as_('budget_move'),
            account_close.to_account.as_('account_close'),
            Coalesce(account_reclassification.reclassification_account, -1).as_(
                'reclassification_account'),  # noqa
            # account_reclassification.reclassification_account.as_(
            #    'reclassification_account'),  # noqa
            where=(payslip_line.id.in_(list(lines.keys()))),
            order_by=[party_party.id.asc,
                      payslip_law_benefit.kind.asc,
                      period.fiscalyear.desc,
                      payslip_law_benefit.period.desc, ]
        )
        cursor.execute(*lines_to_pay)
        result = cursor.fetchall()
        account_ids_accummulate = []
        account_values = defaultdict(lambda: Decimal(0))

        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        lines_to_save = ()
        mld1 = ()
        mld2 = ()
        mlc = ()
        default_conf_account = {}
        paid_party = defaultdict(lambda: Decimal('0.00'))
        distinct_lines = lines_to_pay.select(
            lines_to_pay.expense,
            lines_to_pay.payable,
            lines_to_pay.move_payable_account,
            lines_to_pay.account_close,
            lines_to_pay.reclassification_account,
            # distinct=[
            #    lines_to_pay.expense,
            #    lines_to_pay.payable,
            #    lines_to_pay.move_payable_account,
            #    lines_to_pay.account_close,
            #    lines_to_pay.reclassification_account,
            # ]
        )
        cursor.execute(*distinct_lines)
        result_accounts = cursor.fetchall()

        for (expense, payable, move_payable_account, account_close,
             reclassification_account) in result_accounts:
            if expense is not None and expense != -1:
                account_ids_accummulate.append(expense)
            if payable is not None and payable != -1:
                account_ids_accummulate.append(payable)
            if move_payable_account is not None and move_payable_account != -1:
                account_ids_accummulate.append(move_payable_account)
            if account_close is not None and account_close != -1:
                account_ids_accummulate.append(account_close)
            if reclassification_account is not None and reclassification_account != -1:
                account_ids_accummulate.append(reclassification_account)

        for (payslip_line_id, total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp, move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:

            if default_conf_account.get(party) is None:
                default_conf_account[party] = {}
                default_conf_account[party]['expense'] = expense
                default_conf_account[party]['payable'] = payable
                if budget_move:
                    default_conf_account[party]['budget'] = budget_move
                else:
                    default_conf_account[party]['budget'] = budget
                if default_conf_account[party].get('account_close') is None:
                    default_conf_account[party]['account_close'] = account_close
                if default_conf_account[party].get('reclassification_account') is None:
                    default_conf_account[party]['reclassification_account'] = reclassification_account
            else:
                if default_conf_account[party].get('account_close') is None and account_close is not None:
                    default_conf_account[party]['account_close'] = account_close
                if default_conf_account[party].get('reclassification_account') == -1 and reclassification_account != -1:
                    default_conf_account[party]['reclassification_account'] = reclassification_account
            # if paid_party.get(party) is None:
            #    paid_party[party] = Decimal('0.00')

        for account in Account.browse(account_ids_accummulate):
            account_values[account.id] = account
        total_amount = Decimal(0.0)

        for (payslip_line_id, total_value, total_deduction, amount, kind, period,
             fiscalyear, budget, expense, payable, move_lxp, move_compromise,
             move_payable_account, party, compromise, budget_move, account_close,
             reclassification_account) in result:

            # if default_conf_account.get(party) is None:
            #    default_conf_account[party] = {}
            #    default_conf_account[party]['expense'] = expense
            #    default_conf_account[party]['payable'] = payable
            #    if budget_move:
            #        default_conf_account[party]['budget'] = budget_move
            #    else:
            #        default_conf_account[party]['budget'] = budget
            #    default_conf_account[party]['account_close'] = account_close
            #    default_conf_account[party][
            #        'reclassification_account'] = reclassification_account
            if paid_party[payslip_line_id] == lines.get(payslip_line_id)[1]:
                continue

            current_compromise = None
            current_budget = None

            if move_lxp is not None and fiscalyear == self.fiscalyear.id:  # Pay of move_line_declared law_benefit

                account_to_use = None
                if self.fiscalyear.id == fiscalyear:  # Current fiscalyear
                    if move_payable_account:
                        account_to_use = account_values[move_payable_account]
                    else:
                        account_to_use = account_values[payable]
                    if compromise:
                        current_compromise = compromise
                    else:
                        current_compromise = move_compromise
                    if budget_move:
                        current_budget = budget_move
                    else:
                        current_budget = budget

                else:
                    if account_close is None and default_conf_account[party]['account_close'] is None:
                        self.raise_user_error(
                            'account_close_missing', {
                                'move_payable_account': account_values[
                                    move_payable_account].rec_name
                            })
                    if reclassification_account is None and default_conf_account[party][
                        'reclassification_account'] is None:
                        self.raise_user_error('account_reclassification_missing', {
                            'account_close': account_values[account_close].rec_name
                        })

                    if reclassification_account is None:
                        account_to_use = account_values[default_conf_account[party]['reclassification_account']]
                    else:
                        account_to_use = account_values[reclassification_account]
                oline = MoveLine(move_lxp)
                amount_to_use = Decimal('0.0')
                if paid_party[payslip_line_id] + oline.pending_amount <= lines.get(payslip_line_id)[1]:
                    amount_to_use = oline.pending_amount
                    paid_party[payslip_line_id] += amount_to_use
                else:
                    diff = paid_party[
                               payslip_line_id] + oline.pending_amount - lines.get(payslip_line_id)[1]
                    amount_to_use = oline.pending_amount - diff
                    paid_party[payslip_line_id] += amount_to_use

                # party_to_use = lines.get(payslip_line_id)[2]
                party_to_use = party
                new_line = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': current_compromise,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': current_budget,
                        'payable_cost_center': None
                    },
                    None
                )

                if self.fiscalyear.id != fiscalyear:
                    new_line.budget = new_line.get_allowed_budgets()[0]

                else:
                    if budget_move:
                        new_line.budget = current_budget
                    else:
                        new_line.budget = budget

                if oline.parent is not None:
                    new_line.cost_center = oline.parent.cost_center
                else:
                    new_line.cost_center = default_cost_center

                if new_line.compromise not in move.compromises:
                    move.compromises += (new_line.compromise,)
                new_line.payable_move_line = move_lxp

                total_amount += amount_to_use
                lines_to_save += (new_line,)
            else:  # create lines of adjustment and without creation.
                if move.budget_impact_direct == False:
                    move.budget_impact_direct = True
                    move.save()

                budget = None
                account_to_use = None
                payable_acc = None
                account_to_use_id = None
                payable_acc_id = None
                budget_id = None
                confs = default_conf_account.get(party)
                if confs['expense'] is None:
                    continue
                if self.fiscalyear.id == fiscalyear:
                    account_to_use_id = confs['expense']
                    payable_acc_id = confs['payable']
                    budget_id = confs['budget']
                else:

                    if account_close is None and confs['account_close'] is None:
                        self.raise_user_error(
                            'account_close_missing', {
                                'move_payable_account': account_values[
                                    confs['payable']].rec_name
                            })
                    if (reclassification_account is None or reclassification_account == -1) and confs[
                        'reclassification_account'] is None:
                        self.raise_user_error('account_reclassification_missing', {
                            'account_close': confs['reclassification_account']
                        })

                    account_to_use_id = confs['account_close']
                    payable_acc_id = confs['reclassification_account']
                    budget_id = None
                account_to_use = account_values[account_to_use_id]
                payable_acc = account_values[payable_acc_id]

                if payable_acc_id is None or account_to_use_id is None:
                    continue
                if paid_party[payslip_line_id] + amount <= lines.get(payslip_line_id)[1]:
                    amount_to_use = amount
                    paid_party[payslip_line_id] += amount
                else:
                    diff = paid_party[payslip_line_id] + amount - lines.get(payslip_line_id)[1]
                    amount_to_use = amount - diff
                    paid_party[payslip_line_id] += amount_to_use

                party_to_use = lines.get(payslip_line_id)[2]
                # party_to_use = party
                line_dev = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': None,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                if budget_id is None:
                    line_dev.budget = line_dev.get_allowed_budgets()[0]
                else:
                    line_dev.budget = Budget(budget_id)

                line_dev.cost_center = default_cost_center
                # move.lines += (line_dev,)
                line_to_pay = self.get_move_line(
                    move,
                    payable_acc,
                    0,
                    amount_to_use, {
                        'compromise': None,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )

                # move.lines += (line_to_pay,)
                execute_line = self.get_move_line(
                    move,
                    payable_acc,
                    amount_to_use,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                execute_line.budget = line_dev.budget
                execute_line.cost_center = line_dev.cost_center
                line_to_pay.parent = line_dev
                execute_line.payable_move_line = line_to_pay
                mld1 += (line_dev,)
                mld2 += (execute_line,)
                mlc += (line_to_pay,)

                # move.lines += (execute_line,)
                total_amount += amount_to_use
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        # if move.budget_impact_direct:
        #    move.save()
        move.lines += lines_to_save
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        move.lines += (bank_line,)
        show_moves([move])
        move.save()
        return move


    def fill_payslip_accummulated_move_parties(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()

        Party = pool.get('party.party')
        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.law.benefit')
        Employee = pool.get('company.employee')
        PayslipLine = pool.get('payslip.line')
        PayslipPayslip = pool.get('payslip.payslip')
        PayslipGeneral = pool.get('payslip.general')
        AccountMove = pool.get('account.move')
        AccountMoveLine = pool.get('account.move.line')
        Budget = pool.get('public.budget')
        Account = pool.get('account.account')
        AccountClose = pool.get('account.account.close')
        AccountReclassification = pool.get('account.account.reclassification')

        party_party = Party.__table__()
        payslip_law_benefit = LawBenefit.__table__()
        period = Period.__table__()
        company_employee = Employee.__table__()
        payslip_payslip = PayslipPayslip.__table__()
        payslip_payslip_lb = PayslipPayslip.__table__()
        payslip_line = PayslipLine.__table__()
        payslip_payslip_gen = PayslipPayslip.__table__()
        payslip_general = PayslipGeneral.__table__()
        account_move = AccountMove.__table__()
        account_move_line_lxp = AccountMoveLine.__table__()
        account_move_line_ldev = AccountMoveLine.__table__()
        account_close = AccountClose.__table__()
        account_reclassification = AccountReclassification.__table__()

        Account = pool.get('account.account')
        CostCenter = pool.get('public.cost.center')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveComp = pool.get('account.move-allowed-budget.compromise')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general')
        RequestPayslip = pool.get('treasury.account.payment.request.roles')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_comp = MoveComp.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()
        cost_centers = CostCenter.search([])
        default_cost_center = None
        if len(cost_centers) > 0:
            default_cost_center = cost_centers[0]
        query_request_lines = request_line.select(
            request_line.request,
            request_line.amount,
            request_line.party,
            request_line.origin,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
        )
        cursor.execute(*query_request_lines)
        result_request_lines = cursor.fetchall()
        lines = defaultdict(lambda: [])
        for (request, amount, party, origin) in result_request_lines:
            lines[int(origin.split(',')[1])] = [request, amount, party, int(origin.split(',')[1])]
        lines_to_pay = payslip_line.join(payslip_payslip,
                condition=payslip_line.payslip == payslip_payslip.id
            ).join(payslip_law_benefit,
                condition=(payslip_law_benefit.payslip == payslip_payslip.id) &
                    (payslip_law_benefit.kind != 'monthlyse')
            ).join(period,
                condition=payslip_law_benefit.period == period.id
            ).join(company_employee,
                condition=payslip_payslip.employee == company_employee.id
            ).join(party_party,
                condition=company_employee.party == party_party.id
            ).join(payslip_payslip_lb,
                condition=payslip_law_benefit.generator_payslip == payslip_payslip_lb.id
            ).join(payslip_general,
                condition=payslip_payslip_lb.general == payslip_general.id
            ).join(account_move, type_='LEFT',
                condition=((payslip_general.move_lb == account_move.id) &
                    (account_move.state == 'posted'))
            ).join(move_comp,type_='LEFT',
                condition=(move_comp.move == account_move.id)
            ).join(account_move_line_lxp, type_='LEFT',
                condition=((account_move_line_lxp.move == account_move.id) &
                       (payslip_law_benefit.payable_account == account_move_line_lxp.account) &
                       (payslip_law_benefit.amount == account_move_line_lxp.credit) &
                       (party_party.id == account_move_line_lxp.party) &
                       (account_move_line_lxp.reconciliation == Null)),
            ).join(account_move_line_ldev, type_='LEFT',
                   condition=account_move_line_lxp.parent == account_move_line_ldev.id
            ).join(account_close, type_='LEFT',
                    # condition=(account_move_line_lxp.account == account_close.account) &
                    condition=(payslip_law_benefit.payable_account == account_close.account) &
                        (account_close.fiscalyear == period.fiscalyear)
            ).join(account_reclassification, type_='LEFT',
                condition=(account_close.to_account == account_reclassification.account) &  # noqa
                    (account_reclassification.fiscalyear == period.fiscalyear)
            ).select(
                payslip_line.id.as_('payslip_line_id'),
                payslip_payslip.total_value.as_('total_value'),
                payslip_payslip.total_deduction.as_('total_deduction'),
                payslip_law_benefit.amount.as_('amount'),
                payslip_law_benefit.kind.as_('kind'),
                payslip_law_benefit.period.as_('period'),
                period.fiscalyear.as_('fiscalyear'),
                payslip_law_benefit.budget.as_('budget'),
                payslip_law_benefit.expense_account.as_('expense'),
                payslip_law_benefit.payable_account.as_('payable'),
                account_move_line_lxp.id.as_('move_lxp'),
                move_comp.compromise.as_('move_compromise'),
                account_move_line_lxp.account.as_('move_payable_account'),
                party_party.id.as_('party'),
                account_move_line_ldev.compromise.as_('compromise'),
                account_move_line_lxp.payable_budget.as_('budget_move'),
                account_close.to_account.as_('account_close'),
            Coalesce(account_reclassification.reclassification_account, -1).as_(
                'reclassification_account'),  # noqa
            # account_reclassification.reclassification_account.as_(
            #    'reclassification_account'),  # noqa
            where=(payslip_line.id.in_(list(lines.keys()))),
            order_by=[party_party.id.asc,
                      payslip_law_benefit.kind.asc,
                      period.fiscalyear.desc,
                      payslip_law_benefit.period.desc, ]
        )
        cursor.execute(*lines_to_pay)
        result = cursor.fetchall()
        account_ids_accummulate = []
        account_values = defaultdict(lambda: Decimal(0))

        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        lines_to_save = ()
        mld1 = ()
        mld2 = ()
        mlc = ()
        default_conf_account = {}
        paid_party = defaultdict(lambda: Decimal('0.00'))
        distinct_lines = lines_to_pay.select(
            lines_to_pay.expense,
            lines_to_pay.payable,
            lines_to_pay.move_payable_account,
            lines_to_pay.account_close,
            lines_to_pay.reclassification_account,
            # distinct=[
            #    lines_to_pay.expense,
            #    lines_to_pay.payable,
            #    lines_to_pay.move_payable_account,
            #    lines_to_pay.account_close,
            #    lines_to_pay.reclassification_account,
            # ]
        )
        cursor.execute(*distinct_lines)
        result_accounts = cursor.fetchall()

        for (expense, payable, move_payable_account, account_close,
             reclassification_account) in result_accounts:
            if expense is not None and expense != -1:
                account_ids_accummulate.append(expense)
            if payable is not None and payable != -1:
                account_ids_accummulate.append(payable)
            if move_payable_account is not None and move_payable_account != -1:
                account_ids_accummulate.append(move_payable_account)
            if account_close is not None and account_close != -1:
                account_ids_accummulate.append(account_close)
            if reclassification_account is not None and reclassification_account != -1:
                account_ids_accummulate.append(reclassification_account)

        for (payslip_line_id, total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp, move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:

            if default_conf_account.get(party) is None:
                default_conf_account[party] = {}
                default_conf_account[party]['expense'] = expense
                default_conf_account[party]['payable'] = payable
                if budget_move:
                    default_conf_account[party]['budget'] = budget_move
                else:
                    default_conf_account[party]['budget'] = budget
                if default_conf_account[party].get('account_close') is None:
                    default_conf_account[party]['account_close'] = account_close
                if default_conf_account[party].get('reclassification_account') is None:
                    default_conf_account[party]['reclassification_account'] = reclassification_account
            else:
                if default_conf_account[party].get('account_close') is None and account_close is not None:
                    default_conf_account[party]['account_close'] = account_close
                if default_conf_account[party].get('reclassification_account') == -1 and reclassification_account != -1:
                    default_conf_account[party]['reclassification_account'] = reclassification_account
            # if paid_party.get(party) is None:
            #    paid_party[party] = Decimal('0.00')

        for account in Account.browse(account_ids_accummulate):
            account_values[account.id] = account
        total_amount = Decimal(0.0)

        for (payslip_line_id, total_value, total_deduction, amount, kind, period,
             fiscalyear, budget, expense, payable, move_lxp, move_compromise,
             move_payable_account, party, compromise, budget_move, account_close,
             reclassification_account) in result:

            # if default_conf_account.get(party) is None:
            #    default_conf_account[party] = {}
            #    default_conf_account[party]['expense'] = expense
            #    default_conf_account[party]['payable'] = payable
            #    if budget_move:
            #        default_conf_account[party]['budget'] = budget_move
            #    else:
            #        default_conf_account[party]['budget'] = budget
            #    default_conf_account[party]['account_close'] = account_close
            #    default_conf_account[party][
            #        'reclassification_account'] = reclassification_account
            if paid_party[payslip_line_id] == lines.get(payslip_line_id)[1]:
                continue

            current_compromise = None
            current_budget = None

            if move_lxp is not None and fiscalyear == self.fiscalyear.id:  # Pay of move_line_declared law_benefit

                account_to_use = None
                if self.fiscalyear.id == fiscalyear:  # Current fiscalyear
                    if move_payable_account:
                        account_to_use = account_values[move_payable_account]
                    else:
                        account_to_use = account_values[payable]
                    if compromise:
                        current_compromise = compromise
                    else:
                        current_compromise = move_compromise
                    if budget_move:
                        current_budget = budget_move
                    else:
                        current_budget = budget

                else:
                    if account_close is None and default_conf_account[party]['account_close'] is None:
                        self.raise_user_error(
                            'account_close_missing', {
                                'move_payable_account': account_values[
                                    move_payable_account].rec_name
                            })
                    if reclassification_account is None and default_conf_account[party][
                        'reclassification_account'] is None:
                        self.raise_user_error('account_reclassification_missing', {
                            'account_close': account_values[account_close].rec_name
                        })

                    if reclassification_account is None:
                        account_to_use = account_values[default_conf_account[party]['reclassification_account']]
                    else:
                        account_to_use = account_values[reclassification_account]
                oline = MoveLine(move_lxp)
                amount_to_use = Decimal('0.0')
                if paid_party[payslip_line_id] + oline.pending_amount <= lines.get(payslip_line_id)[1]:
                    amount_to_use = oline.pending_amount
                    paid_party[payslip_line_id] += amount_to_use
                else:
                    diff = paid_party[
                               payslip_line_id] + oline.pending_amount - lines.get(payslip_line_id)[1]
                    amount_to_use = oline.pending_amount - diff
                    paid_party[payslip_line_id] += amount_to_use

                # party_to_use = lines.get(payslip_line_id)[2]
                party_to_use = party
                new_line = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': current_compromise,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': current_budget,
                        'payable_cost_center': None
                    },
                    None
                )

                if self.fiscalyear.id != fiscalyear:
                    new_line.budget = new_line.get_allowed_budgets()[0]

                else:
                    if budget_move:
                        new_line.budget = current_budget
                    else:
                        new_line.budget = budget

                if oline.parent is not None:
                    new_line.cost_center = oline.parent.cost_center
                else:
                    new_line.cost_center = default_cost_center

                if new_line.compromise not in move.compromises:
                    move.compromises += (new_line.compromise,)
                new_line.payable_move_line = move_lxp

                total_amount += amount_to_use
                lines_to_save += (new_line,)
            else:  # create lines of adjustment and without creation.
                if move.budget_impact_direct == False:
                    move.budget_impact_direct = True
                    move.save()

                budget = None
                account_to_use = None
                payable_acc = None
                account_to_use_id = None
                payable_acc_id = None
                budget_id = None
                confs = default_conf_account.get(party)
                if confs['expense'] is None:
                    continue
                if self.fiscalyear.id == fiscalyear:
                    account_to_use_id = confs['expense']
                    payable_acc_id = confs['payable']
                    budget_id = confs['budget']
                else:

                    if account_close is None and confs['account_close'] is None:
                        self.raise_user_error(
                            'account_close_missing', {
                                'move_payable_account': account_values[
                                    confs['payable']].rec_name
                            })
                    if (reclassification_account is None or reclassification_account == -1) and confs[
                        'reclassification_account'] is None:
                        self.raise_user_error('account_reclassification_missing', {
                            'account_close': confs['reclassification_account']
                        })

                    account_to_use_id = confs['account_close']
                    payable_acc_id = confs['reclassification_account']
                    budget_id = None
                account_to_use = account_values[account_to_use_id]
                payable_acc = account_values[payable_acc_id]

                if payable_acc_id is None or account_to_use_id is None:
                    continue
                if paid_party[payslip_line_id] + amount <= lines.get(payslip_line_id)[1]:
                    amount_to_use = amount
                    paid_party[payslip_line_id] += amount
                else:
                    diff = paid_party[payslip_line_id] + amount - lines.get(payslip_line_id)[1]
                    amount_to_use = amount - diff
                    paid_party[payslip_line_id] += amount_to_use

                party_to_use = lines.get(payslip_line_id)[2]
                # party_to_use = party
                line_dev = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': None,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                if budget_id is None:
                    line_dev.budget = line_dev.get_allowed_budgets()[0]
                else:
                    line_dev.budget = Budget(budget_id)

                line_dev.cost_center = default_cost_center
                # move.lines += (line_dev,)
                line_to_pay = self.get_move_line(
                    move,
                    payable_acc,
                    0,
                    amount_to_use, {
                        'compromise': None,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )

                # move.lines += (line_to_pay,)
                execute_line = self.get_move_line(
                    move,
                    payable_acc,
                    amount_to_use,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': party_to_use,
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                execute_line.budget = line_dev.budget
                execute_line.cost_center = line_dev.cost_center
                line_to_pay.parent = line_dev
                execute_line.payable_move_line = line_to_pay
                mld1 += (line_dev,)
                mld2 += (execute_line,)
                mlc += (line_to_pay,)

                # move.lines += (execute_line,)
                total_amount += amount_to_use
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        # if move.budget_impact_direct:
        #    move.save()
        move.lines += lines_to_save
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        move.lines += (bank_line,)
        show_moves([move])
        move.save()
        return move

    def fill_payslip_accummulated_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()

        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.law.benefit')
        Employee= pool.get('company.employee')
        PayslipPayslip = pool.get('payslip.payslip')
        PayslipGeneral = pool.get('payslip.general')
        AccountMove = pool.get('account.move')
        AccountMoveLine = pool.get('account.move.line')
        Budget = pool.get('public.budget')
        Account = pool.get('account.account')
        AccountClose = pool.get('account.account.close')
        AccountReclassification = pool.get('account.account.reclassification')

        payslip_law_benefit = LawBenefit.__table__()
        period = Period.__table__()
        company_employee = Employee.__table__()
        payslip_payslip = PayslipPayslip.__table__()
        payslip_payslip_gen = PayslipPayslip.__table__()
        payslip_general = PayslipGeneral.__table__()
        account_move = AccountMove.__table__()
        account_move_line_lxp = AccountMoveLine.__table__()
        account_move_line_ldev = AccountMoveLine.__table__()
        account_close = AccountClose.__table__()
        account_reclassification = AccountReclassification.__table__()

        Account = pool.get('account.account')
        CostCenter = pool.get('public.cost.center')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveComp = pool.get('account.move-allowed-budget.compromise')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general')
        RequestPayslip = pool.get('treasury.account.payment.request.roles')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_comp = MoveComp.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()
        cost_centers = CostCenter.search([])
        default_cost_center = None
        if len(cost_centers) > 0:
            default_cost_center = cost_centers[0]

        query_lines = request_line.join(company_employee,
                condition=request_line.party == company_employee.party
            ).select(
                request_line.request,
                company_employee.id,
                request_line.party,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, company_employee.id,
                    request_line.party]
        )

        lines_to_pay = request.join(query_lines,
            condition=request.id == query_lines.request
        ).join(payslip_request,
            condition=request.id == payslip_request.request
        ).join(payslip_payslip,
            condition=(payslip_payslip.general == payslip_request.general) &
                      (payslip_payslip.employee == query_lines.id)
        ).join(payslip_law_benefit,
            condition=(payslip_law_benefit.payslip == payslip_payslip.id) &
                      (payslip_payslip.employee == query_lines.id)
        ).join(period,
            condition=(payslip_law_benefit.period == period.id)
        ).join(payslip_payslip_gen,
            condition=(payslip_law_benefit.generator_payslip == payslip_payslip_gen.id)
        ).join(payslip_general,
            condition=(payslip_payslip_gen.general == payslip_general.id)
        ).join(account_move,type_='LEFT',
            condition=((payslip_general.move_lb == account_move.id) &
                       (account_move.state == 'posted'))
        ).join(move_comp, type_='LEFT',
               condition=(move_comp.move== account_move.id)
        ).join(account_move_line_lxp,type_='LEFT',
            condition=((account_move_line_lxp.move == account_move.id) &
                (payslip_law_benefit.payable_account == account_move_line_lxp.account) &
                (payslip_law_benefit.amount == account_move_line_lxp.credit) &
                (query_lines.party == account_move_line_lxp.party) &
                (account_move_line_lxp.reconciliation == Null)),
        ).join(account_move_line_ldev, type_='LEFT',
            condition = account_move_line_lxp.parent == account_move_line_ldev.id
        ).join(account_close, type_='LEFT',
            # condition=(account_move_line_lxp.account == account_close.account) &
            condition=(payslip_law_benefit.payable_account == account_close.account) &
                (account_close.fiscalyear == period.fiscalyear)
        ).join(account_reclassification, type_='LEFT',
            condition=(account_close.to_account == account_reclassification.account) &  # noqa
                (account_reclassification.fiscalyear == period.fiscalyear)
        ).select(
            payslip_payslip.total_value.as_('total_value'),
            payslip_payslip.total_deduction.as_('total_deduction'),
            payslip_law_benefit.amount.as_('amount'),
            payslip_law_benefit.kind.as_('kind'),
            payslip_law_benefit.period.as_('period'),
            period.fiscalyear.as_('fiscalyear'),
            payslip_law_benefit.budget.as_('budget'),
            payslip_law_benefit.expense_account.as_('expense'),
            payslip_law_benefit.payable_account.as_('payable'),
            account_move_line_lxp.id.as_('move_lxp'),
            move_comp.compromise.as_('move_compromise'),
            account_move_line_lxp.account.as_('move_payable_account'),
            query_lines.party.as_('party'),
            account_move_line_ldev.compromise.as_('compromise'),
            account_move_line_lxp.payable_budget.as_('budget_move'),
            account_close.to_account.as_('account_close'),
            account_reclassification.reclassification_account.as_(
                'reclassification_account'),  # noqa
            where= ((request.id == self.id) & (payslip_law_benefit.amount > 0)),
            order_by=[query_lines.party.asc,
                      payslip_law_benefit.kind.asc,
                      period.fiscalyear.desc,
                      payslip_law_benefit.period.desc,]
        )
        cursor.execute(*lines_to_pay)
        result = cursor.fetchall()
        account_ids_accummulate = []
        account_values = defaultdict(lambda: Decimal(0))

        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        lines_to_save = ()
        mld1 = ()
        mld2 = ()
        mlc = ()
        default_conf_account = {}
        paid_party = defaultdict(lambda: 0)
        for (total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp, move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:
            if expense is not None and expense != -1:
                account_ids_accummulate.append(expense)
            if payable is not None and payable != -1:
                account_ids_accummulate.append(payable)
            if move_payable_account is not None and move_payable_account != -1:
                account_ids_accummulate.append(move_payable_account)
            if account_close is not None and account_close != -1:
                account_ids_accummulate.append(account_close)
            if reclassification_account is not None and reclassification_account != -1:
                account_ids_accummulate.append(reclassification_account)
            if default_conf_account.get(party) is None:
                default_conf_account[party] = {}
                default_conf_account[party]['expense'] = expense
                default_conf_account[party]['payable'] = payable
                if budget_move:
                    default_conf_account[party]['budget'] = budget_move
                else:
                    default_conf_account[party]['budget'] = budget
                default_conf_account[party]['account_close'] = account_close
                default_conf_account[party]['reclassification_account'] = reclassification_account
            if paid_party.get(party) is None:
                paid_party[party] = Decimal('0.00')


        for account in Account.browse(account_ids_accummulate):
            account_values[account.id] = account
        total_amount = Decimal(0.0)

        dev_lines_to_create = defaultdict(lambda: {})
        execute_lines_to_create = defaultdict(lambda: {})
        payable_lines_to_create = defaultdict(lambda: {})

        for (total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp,move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:
            if paid_party[party] == total_value:
                continue

            current_compromise = None
            current_budget = None
            
            if move_lxp is not None:    #Pay of move_line_declared law_benefit

                account_to_use = None
                if self.fiscalyear.id == fiscalyear: #Current fiscalyear
                    if move_payable_account:

                        account_to_use = account_values[move_payable_account]
                    else:
                        account_to_use = account_values[payable]
                    if compromise:
                        current_compromise = compromise
                    else:
                        current_compromise = move_compromise
                    if budget_move:
                        current_budget = budget_move
                    else:
                        current_budget = budget

                else:
                    account_to_use = reclassification_account
                oline = MoveLine(move_lxp)
                amount_to_use = Decimal('0.0')
                if paid_party[party] + oline.pending_amount <= total_value:
                    amount_to_use = oline.pending_amount
                    paid_party[party] += amount_to_use
                else:
                    diff = paid_party[party] + oline.pending_amount - total_value
                    amount_to_use = oline.pending_amount - diff
                    paid_party[party] += amount_to_use

                new_line = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': current_compromise,
                        'cost_center': None,
                        'party': party,
                        'payable_budget': current_budget,
                        'payable_cost_center': None
                    },
                    None
                )

                if self.fiscalyear.id != fiscalyear:
                    new_line.budget = new_line.get_allowed_budgets()[0]

                else:
                    if budget_move:
                        new_line.budget = current_budget
                    else:
                        new_line.budget = budget

                if oline.parent is not None:
                    new_line.cost_center = oline.parent.cost_center
                else:
                    new_line.cost_center = default_cost_center

                if new_line.compromise not in move.compromises:
                    move.compromises += (new_line.compromise,)
                new_line.payable_move_line = move_lxp

                total_amount += amount_to_use
                lines_to_save += (new_line,)
            else:   #create lines of adjustment and without creation.
                move.budget_impact_direct = True

                budget = None
                account_to_use = None
                payable_acc = None
                account_to_use_id = None
                payable_acc_id = None
                budget_id = None
                confs = default_conf_account.get(party)

                account_to_use_id = confs['expense']
                payable_acc_id = confs['payable']
                budget_id = confs['budget']
                account_to_use = account_values[account_to_use_id]
                payable_acc = account_values[payable_acc_id]

                if payable_acc_id is None or account_to_use_id is None:
                    continue
                if paid_party[party] + amount <= total_value:
                    amount_to_use = amount
                    paid_party[party] += amount
                else:
                    diff = paid_party[party] + amount - total_value
                    amount_to_use = amount - diff
                    paid_party[party] += amount_to_use

                dev_key = None

                if budget_id is None:
                    dev_key = '%s-%s' % (account_to_use_id, '')
                else:
                    dev_key = '%s-%s' % (account_to_use_id, budget_id)

                if dev_lines_to_create.get(dev_key) is None:
                    dev_lines_to_create[dev_key] = {}
                    dev_lines_to_create[dev_key]['account'] = account_to_use
                    dev_lines_to_create[dev_key]['amount'] = amount_to_use
                    dev_lines_to_create[dev_key]['budget_id'] = budget_id
                else:
                    dev_lines_to_create[dev_key]['amount'] += amount_to_use

                payable_key = '%s-%s' % (party, payable_acc_id)
                if payable_lines_to_create.get(payable_key) is None:
                    payable_lines_to_create[payable_key] = {}
                    payable_lines_to_create[payable_key]['account'] = payable_acc
                    payable_lines_to_create[payable_key]['amount'] = amount_to_use
                    payable_lines_to_create[payable_key]['debit_line_key'] = dev_key
                    payable_lines_to_create[payable_key]['party'] = party
                    payable_lines_to_create[payable_key]['payable_lines'] = {}
                else:
                    payable_lines_to_create[payable_key]['amount'] += amount_to_use

        for k,v in dev_lines_to_create.items():
            account_to_use = v.get('account')
            budget_id = v.get('budget_id')
            amount_to_use = v.get('amount')

            line_dev = self.get_move_line(
                move,
                account_to_use,
                amount_to_use,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            if budget_id is None:
                line_dev.budget = line_dev.get_allowed_budgets()[0]
            else:
                line_dev.budget = Budget(budget_id)
            line_dev.cost_center = default_cost_center
            dev_lines_to_create[k]['budget'] = line_dev.budget
            dev_lines_to_create[k]['cost_center'] = line_dev.cost_center
            dev_lines_to_create[k]['line_debit'] = line_dev
            # move.lines += (line_dev,)
            mld1 += (line_dev,)
        for k, v in payable_lines_to_create.items():
            payable_acc = v.get('account')
            amount_to_use = v.get('amount')
            party = v.get('party')
            line_dev_key = v.get('debit_line_key')
            line_dev = dev_lines_to_create.get(line_dev_key).get('line_debit')

            line_to_pay = self.get_move_line(
                move,
                payable_acc,
                0,
                amount_to_use, {
                    'compromise': None,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )

            # move.lines += (line_to_pay,)
            execute_line = self.get_move_line(
                move,
                payable_acc,
                amount_to_use,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            execute_line.budget = line_dev.budget
            execute_line.cost_center = line_dev.cost_center
            line_to_pay.parent = line_dev
            execute_line.payable_move_line = line_to_pay

            # mld1 += (line_dev,)
            mld2 += (execute_line,)
            mlc += (line_to_pay,)

            # move.lines += (execute_line,)
            total_amount += amount_to_use
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        if move.budget_impact_direct:
            move.save()
        move.lines += lines_to_save
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        move.lines += (bank_line,)
        # show_moves([move])
        move.save()
        return move

    def fill_payslip_retired_general_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Account = pool.get('account.account')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general.retired')
        RequestPayslip = pool.get('treasury.account.payment.request.retired')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()

        query_lines = request_line.select(
            request_line.request,
            request_line.party,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, request_line.party]
        )

        query = request.join(query_lines,
                             condition=request.id == query_lines.request
                             ).join(payslip_request,
                                    condition=request.id == payslip_request.request
                                    ).join(payslip,
                                           condition=payslip_request.retired == payslip.id
                                           ).join(move_t,
                                                  condition=payslip.move == move_t.id
                                                  ).join(move_line,
                                                         condition=(move_t.id == move_line.move)
                                                         ).select(
            move_line.id,
            move_line.move,
            move_line.account,
            move_line.debit,
            move_line.credit,
            move_line.party,
            move_line.parent,
            move_line.payable_budget,
            move_line.payable_cost_center,
            move_line.compromise,
            payslip.id.as_('general'),
            where=((request.id == self.id) & ((move_line.parent != Null) |
                                              (move_line.payable_budget != Null)) &
                   (move_line.reconciliation == Null) &
                   (move_t.state == 'posted') &
                   (move_line.party == query_lines.party))
        )
        cursor.execute(*query)
        total_amount = Decimal("0.0")
        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        # In order to do execution on payslip move.  First we need to
        # find accrued account.move.line from each account.move for every
        # payslip selected that contains the same party that request line
        accounts = {}
        moves = {}
        for row in cursor_dict(cursor):
            mline = MoveLine(row['id'])
            pending_amount = Decimal(str(mline.pending_amount))
            if pending_amount > Decimal('0.0'):
                if not accounts.get(row['account']):
                    accounts[row['account']] = Account(row['account'])
                if not moves.get(row['move']):
                    moves[row['move']] = Move(row['move'])
                new_line = self.get_move_line(
                    move,
                    accounts[row['account']],
                    pending_amount,
                    0, {
                        'compromise': None,
                        'cost_center': None,
                        'party': row['party'],
                        'payable_budget': None,
                        'payable_cost_center': None
                    },
                    None
                )
                new_line.payable_move_line = mline
                if row['parent']:
                    oline = MoveLine(row['parent'])
                    if oline.compromise not in move.compromises:
                        move.compromises += (oline.compromise,)
                    new_line.compromise = oline.compromise
                    new_line.budget = oline.budget
                    new_line.cost_center = oline.cost_center
                else:
                    if moves[row['move']].compromises:
                        if len(moves[row['move']].compromises) == 1:
                            if (moves[row['move']].compromises[0] not in
                                    move.compromises):
                                move.compromises += (
                                    moves[row['move']].compromises[0],)
                            new_line.compromise = moves[row['move']].compromises[0]  # noqa
                        if row['payable_budget']:
                            new_line.budget = row['payable_budget']
                            new_line.on_change_budget()
                        else:
                            if (new_line.compromise and (
                                    len(new_line.compromise.budgets) == 1)):
                                new_line.budget = new_line.compromise.budgets[0]
                                new_line.on_change_budget()
                if not new_line.compromise or not new_line.budget:
                    move.type = 'adjustment'
                    new_line.adjustment = True
                move.lines += (new_line,)
                total_amount += pending_amount
        if total_amount > Decimal('0.0'):
            bank_line = self.get_move_line(
                move,
                bank_account,
                0,
                total_amount, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            move.lines += (bank_line,)
            # show_moves([move])
            move.save()
            return move
        else:
            return None

    def fill_payslip_retired_accummulated_move(self, move, bank_account):
        cursor = Transaction().connection.cursor()
        pool = Pool()

        Period = pool.get('account.period')
        LawBenefit = pool.get('payslip.retired.law.benefit')
        Employee= pool.get('company.employee')
        PayslipPayslip = pool.get('payslip.retired')
        PayslipGeneral = pool.get('payslip.general.retired')
        AccountMove = pool.get('account.move')
        AccountMoveLine = pool.get('account.move.line')
        Budget = pool.get('public.budget')
        Account = pool.get('account.account')
        AccountClose = pool.get('account.account.close')
        AccountReclassification = pool.get('account.account.reclassification')

        payslip_law_benefit = LawBenefit.__table__()
        period = Period.__table__()
        company_employee = Employee.__table__()
        payslip_payslip = PayslipPayslip.__table__()
        payslip_payslip_gen = PayslipPayslip.__table__()
        payslip_general = PayslipGeneral.__table__()
        account_move = AccountMove.__table__()
        account_move_line_lxp = AccountMoveLine.__table__()
        account_move_line_ldev = AccountMoveLine.__table__()
        account_close = AccountClose.__table__()
        account_reclassification = AccountReclassification.__table__()

        Account = pool.get('account.account')
        CostCenter = pool.get('public.cost.center')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        Move = pool.get('account.move')
        MoveComp = pool.get('account.move-allowed-budget.compromise')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.general.retired')
        RequestPayslip = pool.get('treasury.account.payment.request.retired')
        request = self.__class__.__table__()
        request_line = RequestLine.__table__()
        move_t = Move.__table__()
        move_comp = MoveComp.__table__()
        move_line = MoveLine.__table__()
        payslip = Payslip.__table__()
        payslip_request = RequestPayslip.__table__()
        cost_centers = CostCenter.search([])
        default_cost_center = None
        if len(cost_centers) > 0:
            default_cost_center = cost_centers[0]

        query_lines = request_line.join(company_employee,
                                        condition=request_line.party == company_employee.party
                                        ).select(
            request_line.request,
            company_employee.id,
            request_line.party,
            where=(request_line.request == self.id) &
                  (request_line.state == 'paid'),
            group_by=[request_line.request, company_employee.id,
                      request_line.party]
        )

        lines_to_pay = request.join(query_lines,
                                    condition=request.id == query_lines.request
                                    ).join(payslip_request,
                                           condition=request.id == payslip_request.request
                                           ).join(payslip_payslip,
                                                  condition=(payslip_payslip.general == payslip_request.retired) &
                                                            (payslip_payslip.employee == query_lines.id)
                                                  ).join(payslip_law_benefit,
                                                         condition=(payslip_law_benefit.payslip == payslip_payslip.id) &
                                                                   (payslip_payslip.employee == query_lines.id)
                                                         ).join(period,
                                                                condition=(payslip_law_benefit.period == period.id)
                                                                ).join(payslip_payslip_gen,
                                                                       condition=(payslip_law_benefit.generator_payslip == payslip_payslip_gen.id)
                                                                       ).join(payslip_general,
                                                                              condition=(payslip_payslip_gen.general == payslip_general.id)
                                                                              ).join(account_move,type_='LEFT',
                                                                                     condition=((payslip_general.move_lb == account_move.id) &
                                                                                                (account_move.state == 'posted'))
                                                                                     ).join(move_comp, type_='LEFT',
                                                                                            condition=(move_comp.move== account_move.id)
                                                                                            ).join(account_move_line_lxp,type_='LEFT',
                                                                                                   condition=((account_move_line_lxp.move == account_move.id) &
                                                                                                              (payslip_law_benefit.payable_account == account_move_line_lxp.account) &
                                                                                                              (payslip_law_benefit.amount == account_move_line_lxp.credit) &
                                                                                                              (query_lines.party == account_move_line_lxp.party) &
                                                                                                              (account_move_line_lxp.reconciliation == Null)&
                                                                                                              ((account_move_line_lxp.parent == '383789')|(account_move_line_lxp.parent == '384549'))),
                                                                                                   ).join(account_move_line_ldev, type_='LEFT',
                                                                                                          condition = account_move_line_lxp.parent == account_move_line_ldev.id
                                                                                                          ).join(account_close, type_='LEFT',
                                                                                                                 # condition=(account_move_line_lxp.account == account_close.account) &
                                                                                                                 condition=(payslip_law_benefit.payable_account == account_close.account) &
                                                                                                                           (account_close.fiscalyear == period.fiscalyear)
                                                                                                                 ).join(account_reclassification, type_='LEFT',
                                                                                                                        condition=(account_close.to_account == account_reclassification.account) &  # noqa
                                                                                                                                  (account_reclassification.fiscalyear == period.fiscalyear)
                                                                                                                        ).select(
            payslip_payslip.total_value.as_('total_value'),
            payslip_payslip.total_deduction.as_('total_deduction'),
            payslip_law_benefit.amount.as_('amount'),
            payslip_law_benefit.kind.as_('kind'),
            payslip_law_benefit.period.as_('period'),
            period.fiscalyear.as_('fiscalyear'),
            payslip_law_benefit.budget.as_('budget'),
            payslip_law_benefit.expense_account.as_('expense'),
            payslip_law_benefit.payable_account.as_('payable'),
            account_move_line_lxp.id.as_('move_lxp'),
            move_comp.compromise.as_('move_compromise'),
            account_move_line_lxp.account.as_('move_payable_account'),
            query_lines.party.as_('party'),
            account_move_line_ldev.compromise.as_('compromise'),
            account_move_line_lxp.payable_budget.as_('budget_move'),
            account_close.to_account.as_('account_close'),
            account_reclassification.reclassification_account.as_(
                'reclassification_account'),  # noqa
            where= ((request.id == self.id) & (payslip_law_benefit.amount > 0)),
            order_by=[query_lines.party.asc,
                      payslip_law_benefit.kind.asc,
                      period.fiscalyear.desc,
                      payslip_law_benefit.period.desc,]
        )
        cursor.execute(*lines_to_pay)
        result = cursor.fetchall()
        account_ids_accummulate = []
        account_values = defaultdict(lambda: Decimal(0))

        move.origin = self
        move.compromises = []
        move.budget_impact_direct = False
        lines_to_save = ()
        mld1 = ()
        mld2 = ()
        mlc = ()
        default_conf_account = {}
        paid_party = defaultdict(lambda: 0)
        for (total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp, move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:
            if expense is not None and expense != -1:
                account_ids_accummulate.append(expense)
            if payable is not None and payable != -1:
                account_ids_accummulate.append(payable)
            if move_payable_account is not None and move_payable_account != -1:
                account_ids_accummulate.append(move_payable_account)
            if account_close is not None and account_close != -1:
                account_ids_accummulate.append(account_close)
            if reclassification_account is not None and reclassification_account != -1:
                account_ids_accummulate.append(reclassification_account)
            if default_conf_account.get(party) is None:
                default_conf_account[party] = {}
                default_conf_account[party]['expense'] = expense
                default_conf_account[party]['payable'] = payable
                if budget_move:
                    default_conf_account[party]['budget'] = budget_move
                else:
                    default_conf_account[party]['budget'] = budget
                default_conf_account[party]['account_close'] = account_close
                default_conf_account[party]['reclassification_account'] = reclassification_account
            if paid_party.get(party) is None:
                paid_party[party] = Decimal('0.00')


        for account in Account.browse(account_ids_accummulate):
            account_values[account.id] = account
        total_amount = Decimal(0.0)

        dev_lines_to_create = defaultdict(lambda: {})
        execute_lines_to_create = defaultdict(lambda: {})
        payable_lines_to_create = defaultdict(lambda: {})

        for (total_value, total_deduction, amount, kind, period, fiscalyear, budget,
             expense, payable, move_lxp,move_compromise, move_payable_account,
             party, compromise, budget_move, account_close,
             reclassification_account) in result:
            if paid_party[party] == total_value:
                continue

            current_compromise = None
            current_budget = None

            if move_lxp is not None:    #Pay of move_line_declared law_benefit

                account_to_use = None
                if self.fiscalyear.id == fiscalyear: #Current fiscalyear
                    if move_payable_account:

                        account_to_use = account_values[move_payable_account]
                    else:
                        account_to_use = account_values[payable]
                    if compromise:
                        current_compromise = compromise
                    else:
                        current_compromise = move_compromise
                    if budget_move:
                        current_budget = budget_move
                    else:
                        current_budget = budget

                else:
                    account_to_use = reclassification_account
                oline = MoveLine(move_lxp)
                amount_to_use = Decimal('0.0')
                if paid_party[party] + oline.pending_amount <= total_value:
                    amount_to_use = oline.pending_amount
                    paid_party[party] += amount_to_use
                else:
                    diff = paid_party[party] + oline.pending_amount - total_value
                    amount_to_use = oline.pending_amount - diff
                    paid_party[party] += amount_to_use

                new_line = self.get_move_line(
                    move,
                    account_to_use,
                    amount_to_use,
                    0, {
                        'compromise': current_compromise,
                        'cost_center': None,
                        'party': party,
                        'payable_budget': current_budget,
                        'payable_cost_center': None
                    },
                    None
                )

                if self.fiscalyear.id != fiscalyear:
                    new_line.budget = new_line.get_allowed_budgets()[0]

                else:
                    if budget_move:
                        new_line.budget = current_budget
                    else:
                        new_line.budget = budget

                if oline.parent is not None:
                    new_line.cost_center = oline.parent.cost_center
                else:
                    new_line.cost_center = default_cost_center

                if new_line.compromise not in move.compromises:
                    move.compromises += (new_line.compromise,)
                new_line.payable_move_line = move_lxp

                total_amount += amount_to_use
                lines_to_save += (new_line,)
            else:   #create lines of adjustment and without creation.
                move.budget_impact_direct = True

                budget = None
                account_to_use = None
                payable_acc = None
                account_to_use_id = None
                payable_acc_id = None
                budget_id = None
                confs = default_conf_account.get(party)

                account_to_use_id = confs['expense']
                payable_acc_id = confs['payable']
                budget_id = confs['budget']
                account_to_use = account_values[account_to_use_id]
                payable_acc = account_values[payable_acc_id]

                if payable_acc_id is None or account_to_use_id is None:
                    continue
                if paid_party[party] + amount <= total_value:
                    amount_to_use = amount
                    paid_party[party] += amount
                else:
                    diff = paid_party[party] + amount - total_value
                    amount_to_use = amount - diff
                    paid_party[party] += amount_to_use

                dev_key = None

                if budget_id is None:
                    dev_key = '%s-%s' % (account_to_use_id, '')
                else:
                    dev_key = '%s-%s' % (account_to_use_id, budget_id)

                if dev_lines_to_create.get(dev_key) is None:
                    dev_lines_to_create[dev_key] = {}
                    dev_lines_to_create[dev_key]['account'] = account_to_use
                    dev_lines_to_create[dev_key]['amount'] = amount_to_use
                    dev_lines_to_create[dev_key]['budget_id'] = budget_id
                else:
                    dev_lines_to_create[dev_key]['amount'] += amount_to_use

                payable_key = '%s-%s' % (party, payable_acc_id)
                if payable_lines_to_create.get(payable_key) is None:
                    payable_lines_to_create[payable_key] = {}
                    payable_lines_to_create[payable_key]['account'] = payable_acc
                    payable_lines_to_create[payable_key]['amount'] = amount_to_use
                    payable_lines_to_create[payable_key]['debit_line_key'] = dev_key
                    payable_lines_to_create[payable_key]['party'] = party
                    payable_lines_to_create[payable_key]['payable_lines'] = {}
                else:
                    payable_lines_to_create[payable_key]['amount'] += amount_to_use

        for k,v in dev_lines_to_create.items():
            account_to_use = v.get('account')
            budget_id = v.get('budget_id')
            amount_to_use = v.get('amount')

            line_dev = self.get_move_line(
                move,
                account_to_use,
                amount_to_use,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': None,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            if budget_id is None:
                line_dev.budget = line_dev.get_allowed_budgets()[0]
            else:
                line_dev.budget = Budget(budget_id)
            line_dev.cost_center = default_cost_center
            dev_lines_to_create[k]['budget'] = line_dev.budget
            dev_lines_to_create[k]['cost_center'] = line_dev.cost_center
            dev_lines_to_create[k]['line_debit'] = line_dev
            # move.lines += (line_dev,)
            mld1 += (line_dev,)
        for k, v in payable_lines_to_create.items():
            payable_acc = v.get('account')
            amount_to_use = v.get('amount')
            party = v.get('party')
            line_dev_key = v.get('debit_line_key')
            line_dev = dev_lines_to_create.get(line_dev_key).get('line_debit')

            line_to_pay = self.get_move_line(
                move,
                payable_acc,
                0,
                amount_to_use, {
                    'compromise': None,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )

            # move.lines += (line_to_pay,)
            execute_line = self.get_move_line(
                move,
                payable_acc,
                amount_to_use,
                0, {
                    'compromise': None,
                    'cost_center': None,
                    'party': party,
                    'payable_budget': None,
                    'payable_cost_center': None
                },
                None
            )
            execute_line.budget = line_dev.budget
            execute_line.cost_center = line_dev.cost_center
            line_to_pay.parent = line_dev
            execute_line.payable_move_line = line_to_pay

            # mld1 += (line_dev,)
            mld2 += (execute_line,)
            mlc += (line_to_pay,)

            # move.lines += (execute_line,)
            total_amount += amount_to_use
        bank_line = self.get_move_line(
            move,
            bank_account,
            0,
            total_amount, {
                'compromise': None,
                'cost_center': None,
                'party': None,
                'payable_budget': None,
                'payable_cost_center': None
            },
            None
        )
        if move.budget_impact_direct:
            move.save()
        move.lines += lines_to_save
        move.lines += mld1
        move.lines += mlc
        move.lines += mld2

        AccountMoveLine.save(mld1)
        AccountMoveLine.save(mlc)
        AccountMoveLine.save(mld2)

        move.lines += (bank_line,)
        # show_moves([move])
        move.save()
        return move

    def get_move_line(self, move, account, debit=0, credit=0, expense_data={},
            budget=None):
        if debit == 0 and credit == 0:
            return None
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        # TODO: remove non used compromise
        # Compromise = pool.get('public.budget.compromise')
        # CREAR ACCOUNT_ERROR!!!!
        Party = pool.get('party.party')
        if account.kind == 'view':
            self.raise_user_error('account_error', {
                'account': account.rec_name
            })

        def add_line(move, account, debit, credit, budget=None,
                     cost_center=None):
            line = MoveLine()
            line.move = move
            line.type = 'financial'
            line.account, line.debit, line.credit = account, debit, credit
            if expense_data['party'] is not None:
                line.party = Party(expense_data['party'])
            line.compromise = expense_data.get('compromise')
            if line.account.party_required and not line.party:
                if account.default_party:
                    line.party = account.default_party
                elif line.compromise is not None:
                    line.party = line.compromise.party
            line.budget = budget
            line.cost_center = cost_center
            if account.kind in ['payable', 'payable_previous'] and credit > 0:
                line.payable_budget = expense_data.get('payable_budget')
                line.payable_cost_center = expense_data.get(
                    'payable_cost_center')

            return line

        line = add_line(move, account, debit, credit, budget)
        return line

    @classmethod
    def get_move_state(cls, payslip_generals, name):
        ids = [pg.id for pg in payslip_generals]

        cursor = Transaction().connection.cursor()
        Move = Pool().get('account.move')
        move = Move.__table__()
        payslip_general = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(payslip_general.id, sub_ids)
            query = payslip_general.join(move, 'LEFT',
                condition=(move.id == payslip_general.move)
            ).select(
                payslip_general.id,
                move.state,
                where=red_sql
            )
            cursor.execute(*query)
            result.update(dict(cursor.fetchall()))

        return result

    @classmethod
    def search_move_state(cls, name, clause):
        return [('move.state',) + tuple(clause[1:])]

    @classmethod
    def set_number(cls, payments):
        '''
        Fill the number field with the payment sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for payment in payments:
            if payment.number:
                continue
            if not payment.fiscalyear.payment_request_sequence:
                payment.raise_user_error('not_sequence', {})
            payment.number = Sequence.get_id(
                    payment.fiscalyear.payment_request_sequence.id)
        cls.save(payments)

    @classmethod
    def set_receipt(cls, payments):
        '''
        Fill the number field with the receipt sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        for payment in payments:
            if payment.receipt:
                continue
            if not payment.fiscalyear.payment_request_receipt_sequence:
                payment.raise_user_error('not_sequence', {})
            payment.receipt = Sequence.get_id(
                payment.fiscalyear.payment_request_receipt_sequence.id)
        cls.save(payments)

    @fields.depends('request_lines', 'type', 'payment_type')
    def on_change_with_request_lines_payslips(self, name=None):
        payslips_list = []
        if self.type == 'payslip.payslip' and self.payment_type == 'employee':
            for line in self.request_lines:
                if line.origin:
                    payslips_list.append(line.origin.id)
        return payslips_list

    @fields.depends('request_lines', 'type')
    def on_change_with_request_lines_payslips_advance(self, name=None):
        payslips_list = []
        if self.type == 'payslip.advance':
            for line in self.request_lines:
                if line.origin:
                    payslips_list.append(line.origin.id)
        return payslips_list

    @fields.depends('request_lines', 'type', 'payment_type')
    def on_change_with_request_lines_payslips_retired(self, name=None):
        payslips_list = []
        if self.type == 'payslip.retired' and self.payment_type == 'employee':
            for line in self.request_lines:
                if line.origin:
                    payslips_list.append(line.origin.id)
        return payslips_list

    @fields.depends('request_lines', 'type', 'contract_type')
    def on_change_with_invoice_payment_move_lines(self, name=None):
        payments_move_list = []
        if (self.type == 'account.invoice' or (
                self.type == 'purchase.contract.process' and
                self.contract_type == 'payment')):
            for line in self.request_lines:
                if line.origin:
                    payments_move_list.append(line.origin.id)
        return payments_move_list

    @fields.depends('type', 'account_move_request', 'party')
    def on_change_with_move_lines(self, name=None):
        lines_to_search = []
        for move_req in self.account_move_request:
            for line in move_req.lines:
                lines_to_search.append(str(line))
        pool = Pool()
        Linedetail = pool.get('treasury.account.payment.request.line.detail')
        result = Linedetail.search([
            ('origin', 'in', lines_to_search),
            ('state', 'not in', ['cancel', 'rejected']),
            ('request', '!=', None)
        ])
        #lines_found = [line.origin for line in result]
        lines_found = []
        for line in result:
            if line.amount == line.pending_amount:
                lines_found.append(line.origin)
        payments_move_list = []
        for move_req in self.account_move_request:
            for line in move_req.lines:
                if line in lines_found:
                    continue
                #if line.payment_origin is Null:
                if self.type == 'account.move':
                    if ((line.account.budget_debit is None) and (
                            line.account.budget_credit is None)):
                        continue
                    if line.party and line.credit > 0:
                        if line.party == self.party:
                            payments_move_list.append(line.id)
                elif self.type == 'account.move.posted':
                    if line.account.bank_reconcile:
                        payments_move_list.append(line.id)
        return payments_move_list

    @fields.depends('invoice', 'type', 'request_lines', 'contract_type')
    def on_change_with_invoice_selectable_move_payment_lines(self, name=None):
        selectable = []
        selected_invoices = []
        for request_line in self.request_lines:
            if request_line.origin:
                selected_invoices.append(request_line.origin.id)
        if self.type == 'account.invoice':
            for invoice in self.invoice:
                for line in invoice.lines_to_pay:
                    if (#not line.payment_origin and
                            line.pending_amount > Decimal("0.0") and
                            line.reconciliation is None and
                            line.party):
                        selectable.append(line.id)
        elif self.type == 'purchase.contract.process' and self.contract_type:
            if self.contract_type == 'payment':
                for invoice in self.invoice:
                    for line in invoice.lines_to_pay:
                        if (not line.payment_origin and
                                line.pending_amount > Decimal("0.0") and
                            line.reconciliation is None and
                            line.party):
                            if line.id not in selected_invoices:
                                selectable.append(line.id)
        return selectable

    @fields.depends('contract_process', 'type')
    def on_change_with_contract_payment_lines(self, name=None):
        payment_lines = []
        if self.type == 'purchase.contract.process':
            for line in self.request_lines:
                if line.origin:
                    payment_lines.append(line.origin.id)
        return payment_lines

    @fields.depends('spi')
    def on_change_with_payment_date(self, name=None):
        if self.spi:
            if self.spi.payment_date:
                return self.spi.payment_date
        return None

    def get_rec_name(self, name):
        return f"{self.number} - {self.party.name}"

    @classmethod
    @ModelView.button_action('treasury.act_cancel_request')
    def cancel_request(cls, works):
        pass

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        domain = [bool_op,
                  ('number',) + tuple(clause[1:]),
                  ]
        return domain


class PaymentRequestGenerateMoveStart(ModelView):
    'Payment Request Generate Move Start'
    __name__ = 'payment.request.generate.move.start'

    company = fields.Many2One('company.company', 'Empresa',
        states={'invisible': True})
    company_party = fields.Function(fields.Many2One('party.party', 'Tercero',
        states={'invisible': True}), 'on_change_with_company_party')
    spi = fields.Many2One('treasury.account.payment.spi', 'SPI',
        states={'invisible': True})
    bank_debit = fields.Boolean('Débito bancario')
    bank_account = fields.Many2One('bank.account', 'Cuenta de banco',
        states={
            'readonly': Bool(Eval('spi'))
        },
        domain=[
            ('owners', 'in', [Eval('company_party')]),
        ],
        depends=['spi', 'company_party'])
    move = fields.Many2One('account.move', 'Asiento contable',
        help='Se puede usar para generar un asiento ya creado anteriormente ',
        readonly=True)
    journal = fields.Many2One('account.journal', 'Diario', required=True,
        states={
            'readonly': (Eval('move') & (Eval('move_state') != 'invalid'))
        }, depends=['move', 'move_state'])
    move_date = fields.Date('Fecha asiento', required=True,
        help='Fecha para el asiento de liquidación')
    description = fields.Char('Descripción', required=True)
    move_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('posted', 'Contabilizado'),
            ('invalid', 'Cancelado'),
        ], 'Estado Asiento'), 'on_change_with_move_state')
    previous_year = fields.Boolean('Año anterior', states={'invisible': True})
    poa = fields.Many2One('public.planning.unit', 'POA',
        states={'invisible': True})
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        states={
            'readonly': ~Bool(Eval('previous_year')),
        }, depends=['previous_year'],
        help='Opcional, en caso de no necesitar la generación automática de '
        'un compromiso.')
    budget = fields.Many2One('public.budget', 'Partida',
        domain=[
            ('type', '=', 'expense'),
            ('activity.parent', 'child_of', Eval('poa')),
            ['OR',
                ('code', 'like', '97%'),
                ('code', 'like', '98%'),
            ]
        ],
        states={
            'readonly': ~Bool(Eval('previous_year')),
            'required': Bool(Eval('previous_year')),
        }, depends=['previous_year', 'poa'],
        help='Partida ha ser utilizada en caso de pagos de años anteriores')

    @classmethod
    def default_move_date(cls):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        context = Transaction().context
        request = Request(context['active_id'])
        return request.payment_date

    @classmethod
    def default_description(cls):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        context = Transaction().context
        request = Request(context['active_id'])
        return request.description

    @fields.depends('move')
    def on_change_with_move_state(self, name=None):
        if self.move:
            return self.move.state
        return None

    @fields.depends('move', 'move_date', 'journal', 'description')
    def on_change_move(self):
        if self.move and self.move.state != 'invalid':
            self.journal = self.move.journal
            self.move_date = self.move.date
            self.description = self.move.description

    @fields.depends('company')
    def on_change_with_company_party(self):
        if self.company and self.company.party:
            return self.company.party.id
        return None

    @fields.depends('spi')
    def on_change_spi(self):
        if self.spi:
            self.bank_account = self.spi.journal


class CancelStart(ModelView):
    'Cancel'
    __name__ = 'cancel.payment.request.start'
    date = fields.DateTime('Fecha de evaluación', states={'readonly': True})
    description = fields.Text('Descripción/Observaciones',
        states={
            'required': True
        })

    @staticmethod
    def default_date():
        return datetime.now()


class CancelWizard(Wizard):
    'CancelWizard'
    __name__ = 'payment.request.cancel.wizard'

    start = StateView(
        'cancel.payment.request.start',
        'treasury.cancel_payment_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Aceptar', 'enter', 'tryton-ok', default=True),
        ])

    enter = StateTransition()

    def transition_enter(self):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        context = Transaction().context
        request = Request(context['active_id'])
        Employee = Pool().get('company.employee')
        employees = []
        user_id = Transaction().context.get('employee')
        if user_id:
            employees = Employee.search([
                ('id', '=', user_id)
            ])
        employee = employees[0].party.name if employees else None
        if request.notes:
            request.notes += (' Obsv. Anulación: ' + self.start.description +
                ' | ' + str(self.start.date) + ' - ' + employee)
        else:
            request.notes = (self.start.description
                + ' | ' + str(self.start.date) + ' | ' + employee)
        if request.spi:
            if request.spi_state not in ['cancel', 'partially_paid']:
                Request.raise_user_error('cancel_spi')
        for line in request.request_lines:
            line.state = 'cancel'
            line.save()
        Request.remove_move_line_payment_origin(request)
        request.state = 'cancel'
        request.save()
        return 'end'


class PaymentRequestGenerateMove(Wizard):
    'Payment Request Generate Move'
    __name__ = 'payment.request.generate.move'

    start = StateView('payment.request.generate.move.start',
        'treasury.payment_request_generate_move_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'handle_', 'tryton-ok', default=True),
            ])
    handle_ = StateTransition()
    open_ = StateAction('account.act_move_form')

    @classmethod
    def __setup__(cls):
        super(PaymentRequestGenerateMove, cls).__setup__()
        cls._error_messages.update({
            'no_move': 'No se generó ningún asiento.',
            'no_bank_account': ('No existe cuenta contable relacionada con la '
                'cuenta de banco %(bank_account)s'),
        })

    def default_start(self, fields):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        POA = pool.get('public.planning.unit')
        context = Transaction().context
        request = Request(context['active_id'])
        previous_year = False
        if request.type == 'party.liquidation':
            for liq in request.liquidations:
                if liq.previous_year:
                    previous_year = True
        poa = POA.search([
            ('type', '=', 'poa'),
            ('start_date', '>=', request.fiscalyear.start_date),
            ('end_date', '>=', request.fiscalyear.end_date),
        ])
        data = {
            'move': request.move.id if request.move else None,
            'spi': request.spi.id if request.spi else None,
            'bank_debit': request.bank_debit,
            'company': request.company.id if request.company else None,
            'previous_year': previous_year,
            'poa': poa[0].id,
        }
        return data

    def create_update_move(self, move_=None):
        pool = Pool()
        Move = pool.get('account.move')
        Line = pool.get('account.move.line')
        Period = pool.get('account.period')
        if move_:
            move = move_
            Line.delete(move.lines)
        else:
            move = Move()
            move.number = "#"
        move.type = 'financial'
        move.journal = self.start.journal
        move.description = self.start.description
        move.period, = Period.search([
            ('start_date', '<=', self.start.move_date),
            ('end_date', '>=', self.start.move_date),
        ])
        move.date = self.start.move_date
        move.compromises = []
        move.lines = []
        move.budget_impact_direct = False
        return move

    def transition_handle_(self):
        pool = Pool()
        Account = pool.get('account.account')
        Request = pool.get('treasury.account.payment.request')
        context = Transaction().context
        request = Request(context['active_id'])
        move = self.create_update_move(self.start.move)
        if request.request_lines:
            bank_account = Account.search([
                ('bank_account', '=', self.start.bank_account)
            ])
            if bank_account:
                bank_account = bank_account[0]
            else:
                self.raise_user_error('no_bank_account', {
                    'bank_account': self.start.bank_account.rec_name,
                })

            current_move = None
            pool = Pool()
            Payment = pool.get('purchase.contract.process.payment')

            if request.type == 'company.employee.loan':
                current_move = request.fill_loan_move(move, bank_account)
            elif request.type == 'contract.liquidation':
                current_move = request.fill_liquidation_move(move, bank_account)
            elif request.type == 'credit_card.creditcard':
                pass
                # current_move=request.fill_liquidation_move(move, bank_account)
            elif request.type == 'credit_card.liquidation.creditcard':
                pass
                # current_move=request.fill_liquidation_move(move, bank_account)
            elif request.type == 'vacation.liquidation':
                current_move = request.fill_vacation_liquidation_move(
                    move, bank_account)
            elif request.type == 'account.invoice':
                current_move = request.fill_party_move(move, bank_account)
            elif request.type == 'party.liquidation':
                current_move = request.fill_party_move(move, bank_account,
                   compromise=self.start.compromise, budget=self.start.budget)
            elif request.type == 'payslip.advance':
                current_move = request.fill_loan_move(move, bank_account)
            elif request.type == 'purchase.contract.process':
                if request.contract_type == 'payment' and isinstance(
                        request.request_lines[0].origin, Payment):
                    current_move = request.fill_contract_move_no_invoice(
                        move, bank_account)
                elif request.contract_type == 'advance':
                    current_move = request.fill_advance_move(move, bank_account)
                else:
                    current_move = request.fill_party_move(move, bank_account)
            elif request.type == 'payslip.payslip':
                if request.is_accummulated_benefits:
                    if request.payment_type == 'party':
                        current_move = request.fill_payslip_accummulated_move_parties(
                            move, bank_account)
                    else:
                        current_move = request.fill_payslip_accummulated_move(
                            move, bank_account)
                else:
                    current_move = request.fill_payslip_general_move(
                        move, bank_account)
            elif request.type == 'payslip.retired':
                if request.is_accummulated_benefits:
                    if request.payment_type == 'party':
                        current_move = request.fill_payslip_retired_accummulated_move_parties(
                            move, bank_account)
                    else:
                        current_move = request.fill_payslip_retired_accummulated_move(
                            move, bank_account)
                else:
                    current_move = request.fill_payslip_retired_general_move(
                        move, bank_account)
            elif request.type == 'account.move':
                current_move = request.fill_party_move(move, bank_account)
            elif request.type == 'company.travel.expense':
                current_move = request.fill_loan_move(move, bank_account)
            elif request.type == 'treasury.petty.cash':
                current_move = request.fill_petty_cash_move(move, bank_account)
            request.move = current_move
            request.save()
            self.start.move = current_move
            return "open_"
        else:
            request.raise_user_error('empty_request_lines')

    def do_open_(self, action):
        if self.start.move:
            data = {'res_id': [self.start.move.id]}
            action['views'].reverse()
            return action, data
        else:
            self.raise_user_error('no_move')


class AccountPaymentRoles(ModelSQL, ModelView):
    'Account Payment General Roles'
    __name__ = 'treasury.account.payment.request.roles'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    general = fields.Many2One('payslip.general', 'general')


class AccountPaymentRoleRules(ModelSQL):
    'Account Payment Rules'
    __name__ = 'treasury.account.payment.request.payslip_rule'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    rule = fields.Many2One('payslip.rule', 'rule')


class AccountPaymentAdvanceRole(ModelSQL):
    'Account Payment Advance Roles'
    __name__ = 'treasury.account.payment.request.advance'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    general_advance = fields.Many2One(
        'payslip.general.advance', 'general_advance')


class AccountPaymentRetiredRole(ModelSQL):
    'Retired Payslip Payment'
    __name__ = 'treasury.account.payment.request.retired'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    retired = fields.Many2One(
        'payslip.general.retired', 'general_retired')


class AccountPaymentInvoice(ModelSQL):
    'Account Payment Invoice'
    __name__ = 'treasury.account.payment.request.invoice'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    invoice = fields.Many2One('account.invoice', 'Factura')


class AccountPaymentMove(ModelSQL, ModelView):
    'Account Payment Account Move'
    __name__ = 'treasury.account.payment.request.move'

    request = fields.Many2One('treasury.account.payment.request', 'request')
    move = fields.Many2One('account.move', 'Asiento')


class AccountPaymentContractProcess(ModelSQL):
    'Account Payment Contract Payment'
    __name__ = 'treasury.account.payment.request.contract.process'

    request = fields.Many2One('treasury.account.payment.request', 'request')


class AccountPaymentPartyLiquidation(ModelSQL):
    'Account Payment Party Liquidation'
    __name__ = 'treasury.account.payment.request.party.liquidation'

    request = fields.Many2One('treasury.account.payment.request', 'Solicitud')
    liquidation = fields.Many2One('party.liquidation', 'Liquidación de tercero')


class AccountPaymentTravelExpense(ModelSQL):
    'Account Payment Travel Expense'
    __name__ = 'treasury.account.payment.request.travel.expense'
    request = fields.Many2One('treasury.account.payment.request', 'request')
    expense = fields.Many2One('company.travel.expense', 'expense')


class PaymentRequestLineDetail(Workflow, ModelView, ModelSQL):
    'Payment Request Line Detail'
    __name__ = 'treasury.account.payment.request.line.detail'
    _history = True

    initial_amount = fields.Numeric('Inicial', digits=(16, 2),
        states={'readonly': True},
        domain=[
            ('initial_amount', '>', Decimal("0.0")),
        ])
    pending_amount = fields.Numeric('Pendiente', digits=(16, 2),
        states={'readonly': True},
        domain=[
            ('pending_amount', '<=', Eval('initial_amount')),
        ], depends=['pending_amount', 'request', 'initial_amount'])
    amount = fields.Numeric('Valor', digits=(16, 2),
        states={
            'readonly': ~(Eval('_parent_request', {}).get('state') == 'draft'),
        },
        domain=[
            ('amount', '<=', Eval('pending_amount')),
        ], depends=['pending_amount', 'request'])
    egress_concept = fields.Many2One('treasury.account.payment.spi.concept',
        'Código de concepto de egreso',
        states={
            'readonly': Bool(
                Eval('_parent_request', {}).get(
                    'state') != 'draft')
        })
    state = fields.Selection(
        [
            ('pending', 'Pendiente'),
            ('paid', 'Pagado'),
            ('rejected', 'Negado'),
            ('cancel', 'Cancelado')
        ], 'Estado',
        states={
            'readonly': Bool(
                Eval('_parent_request', {}).get(
                    'state') != 'generated')
        }
    )
    state_translated = state.translated('state')
    bank_account = fields.Many2One(
        'bank.account', 'Cuenta bancaria', states=_STATES_LINE)
    spi = fields.Many2One(
        'treasury.account.payment.spi', 'SPI', states=_STATES_LINE)
    request = fields.Many2One(
        'treasury.account.payment.request', 'Solicitud', states=_STATES_LINE)
    party = fields.Many2One('party.party', 'Tercero', states=_STATES_LINE)
    bank = fields.Function(fields.Many2One('bank', 'Bank'), 'get_data')
    description = fields.Char('Descripción', states=_STATES_LINE)
    origin = fields.Reference('Origen',
        [
            ('payslip.payslip', 'Individual Payslip'),
            ('payslip.retired', 'Línea rol jubilado'),
            ('account.move.line', 'Invoice Payment Line'),
            ('company.employee.loan', 'employee Loan'),
            ('contract.liquidation', 'Liquidación haberes'),
            ('credit_card.creditcard', 'Carta crédito - Importación'),
            ('credit_card.liquidation.creditcard',
             'Carta crédito - Importación (Ajuste)'),
            ('vacation.liquidation', 'Liquidación vacaciones'),
            ('payslip.advance', 'payslip advance'),
            ('payslip.line', 'Lineas de Rubros'),
            ('payslip.retired.line', 'Lineas de Rubros Retirados'),
            ('payslip.rule', 'Rubros'),
            ('purchase.contract.process.payment', 'Lineas de Contrato'),
            ('party.liquidation.line', 'Liquidación de tercero'),
            ('company.travel.expense', 'Gastos de Viaje'),
            ('treasury.petty.cash', 'Caja Chica'),
        ],
        domain=[
            If(((Eval('origin_model') == 'payslip.payslip') &
                (Eval('_parent_request', {}).get(
                    'payment_type') == 'employee')),  # noqa
               [
                   ('general', '=', Eval(
                       '_parent_request', {}).get('payslip_general', [])
                   ),
                   ('id', 'not in', Eval(
                       '_parent_request', {}).get('request_lines_payslips', [])
                   ),
               ],  # noqa
               []),
            If(Eval('origin_model') == 'party.liquidation.line',
               [
                   ('liquidation.party', '=', Eval(
                       '_parent_request', {}).get('party', [])
                    ),
               ],  # noqa
               []),
            If(Eval('origin_model') == 'payslip.advance',
               [
                   ('general', '=', Eval(
                       '_parent_request', {}).get('payslip_general_advance', [])
                    ),
                   ('id', 'not in', Eval(
                       '_parent_request', {}).get(
                           'request_lines_payslips_advance', [])
                    ),
               ],  # noqa
               []),
            If(((Eval('origin_model') == 'payslip.retired') &
                (Eval('_parent_request', {}).get(
                    'payment_type') == 'employee')),
               [
                   ('general', '=', Eval(
                       '_parent_request', {}).get('payslip_general_retired', [])
                    ),
                   ('id', 'not in', Eval(
                       '_parent_request', {}).get(
                       'request_lines_payslips_retired', [])
                    ),
               ],  # noqa
               []),
            If(Eval('origin_model') == 'account.move.line',
               [
                   ('id', 'not in', Eval('_parent_request', {}).get(
                       'invoice_payment_move_lines', [])),
                   ('id', 'in', Eval('_parent_request', {}).get(
                       'invoice_selectable_move_payment_lines', [])),

               ], [])  # noqa
        ],
        states={
            'readonly': ~(Eval('_parent_request', {}).get('state') == 'draft'),
            'required': True,
        }, depends=['spi', 'origin_model', 'request'])
    origin_model = fields.Function(
        fields.Char('Modelo origen'), 'on_change_with_origin_model')
    identifier = fields.Function(
        fields.Char('Identificador'), 'on_change_with_identifier')
    identifier_info = fields.Char('Identificador', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(PaymentRequestLineDetail, cls).__setup__()
        cls._error_messages.update({
            'already_origin': ('The origin "%(origin)s" already '
                'exist in this request.'),
            'employee_bank': ('The employee "%(employee)s" does not have a'
                ' bank account configured.'),
            'party_bank': ('El tercero "%(party)s" no tiene una '
                ' cuenta de banco configurada.'),
            'origin_type_error': ('The selected type "%(origin_model)s" '
                ' is not the same that request type.'),
        })
        cls._buttons.update({
            'unpaid': {
                'invisible': (
                    Eval('_parent_spi', {}).get('state', '') != 'generated'),
            },
        })
        cls._order.insert(0, ('party', 'ASC'))
        cls._order.insert(1, ('amount', 'ASC'))

    @classmethod
    def __register__(cls, module_name):
        super(PaymentRequestLineDetail, cls).__register__(module_name)
        TableHandler = backend.get('TableHandler')
        cursor = Transaction().connection.cursor()
        table = TableHandler(cls, module_name)
        detail = cls.__table__()
        initial_amount_exists = table.column_exist('initial_amount')
        if initial_amount_exists:
            cursor.execute(*detail.update(
                columns=[detail.initial_amount, detail.pending_amount],
                values=[detail.amount, detail.amount],
                where=detail.initial_amount == Null))

    @classmethod
    def delete(cls, lines):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Pcash = pool.get('treasury.petty.cash')
        move_line_to_save = []
        pcash_to_save = []
        for line in lines:
            if isinstance(line.origin, MoveLine):
                move_line = line.origin
                move_line.payment_origin = None
                move_line_to_save.append(move_line)
            elif isinstance(line.origin, Pcash):
                pcash = line.origin
                pcash.request = Null
                pcash_to_save.append(pcash)
        MoveLine.save(move_line_to_save)
        Pcash.save(pcash_to_save)
        super(PaymentRequestLineDetail, cls).delete(lines)

    @classmethod
    @ModelView.button
    def unpaid(cls, lines):
        for line in lines:
            if line.state == 'rejected':
                line.state = 'paid'
            elif line.state == 'paid':
                line.state = 'rejected'
        cls.save(lines)

    @classmethod
    def get_origin_models(cls):
        return cls.fields_get(['origin'])['origin']['selection']

    @fields.depends('origin')
    def on_change_with_origin_model(self, name=None):
        if self.origin:
            return self.origin.__name__
        return None

    @staticmethod
    def default_state():
        return 'pending'

    @staticmethod
    def default_amount():
        return Decimal('0.0')

    @staticmethod
    def _get_origin():
        return [
            'treasury.account.payment',
            'payslip.payslip',
            'payslip.retired',
            'account.move.line',
            'company.employee.loan',
            'contract.liquidation',
            'credit_card.creditcard',
            'credit_card.liquidation.creditcard',
            'vacation.liquidation',
            'payslip.advance',
            'payslip.line',
            'party.liquidation.line',
            'treasury.petty.cash',
        ]

    @classmethod
    def get_origin(cls):
        IrModel = Pool().get('ir.model')
        models = cls._get_origin()
        models = IrModel.search([
            ('model', 'in', models)
        ])
        return [(None, '')] + [(m.model, m.name) for m in models]

    @fields.depends('origin', 'party', 'bank_account', 'description', 'amount',
        'egress_concept', 'spi', 'request', 'origin_model',
        '_parent_request.description')
    def on_change_origin(self):
        pool = Pool()
        AccountPayment = pool.get('treasury.account.payment')
        MoveLine = pool.get('account.move.line')
        Payslip = pool.get('payslip.payslip')
        Payslip_advance = pool.get('payslip.advance')
        PayslipRetired = pool.get('payslip.retired')
        employeeLoan = pool.get('company.employee.loan')
        ContractLiquidation = pool.get('contract.liquidation')
        CreditCard = pool.get('credit_card.creditcard')
        LiquidationCreditCard = pool.get('credit_card.liquidation.creditcard')
        VacationLiquidation = pool.get('vacation.liquidation')
        EgressConcept = pool.get('treasury.account.payment.spi.concept')
        partyPayment = pool.get('payslip.line')
        PartyPaymentRetired = pool.get('payslip.retired.line')
        ContractPayment = pool.get('purchase.contract.process.payment')
        LiquidationLine = pool.get('party.liquidation.line')
        TravelExpense = pool.get('company.travel.expense')
        PettyCash = pool.get('treasury.petty.cash')
        if self.origin:
            if self.origin.id != -1:
                # asking if that origin was already selected
                already_dict = {}
                # for request_line in self.request.spi_lines:
                #     already_dict[request_line.id] = request_line.origin.id
                if self.origin.id in already_dict.values():
                    self.raise_user_error('already_origin', {
                        'origin': self.origin.rec_name
                        })
                    return 'end'
                    # ACCOUNT PAYMENTS
                if isinstance(self.origin, AccountPayment):
                    self.party = self.origin.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = self.origin.description
                    self.amount = self.origin.amount
                    self.on_change_amount()
                    self.egress_concept = None
                # party Payment
                if (isinstance(self.origin, partyPayment) or
                    isinstance(self.origin, PartyPaymentRetired)):
                    self.party = self.origin.rule.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = self.origin.payslip_template.rec_name
                    self.amount = self.origin.amount
                    self.on_change_amount()
                    self.egress_concept = EgressConcept.search(
                        [('code', '=', '401011')])[0]
                # PAYSLIPS
                elif (isinstance(self.origin, Payslip) or
                      isinstance(self.origin, Payslip_advance) or
                      isinstance(self.origin, PayslipRetired)):
                    if not hasattr(self.origin, 'employee'):
                        return
                    self.party = self.origin.employee.party
                    if isinstance(self.origin, PayslipRetired) and (
                            self.origin.retired.party):
                        self.party = self.origin.retired.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = self.origin.template.name + ' - ' + \
                        self.origin.period.name
                    self.amount = self.origin.total_value
                    self.on_change_amount()
                    self.egress_concept = None
                # INVOICE PAYMENT LINES
                elif (isinstance(self.origin, MoveLine) and
                      (self.request.type == 'account.invoice' or
                      self.request.type == 'purchase.contract.process')):
                    self.party = self.origin.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    if self.origin.origin_:
                        self.description = self.origin.origin_.description
                    else:
                        self.description = self.origin.move.description
                    self.initial_amount = self.origin.credit
                    self.pending_amount = self.origin.pending_amount
                    self.amount = self.origin.pending_amount
                    self.on_change_amount()
                elif (isinstance(self.origin, MoveLine) and
                      self.request.type == 'account.move'):
                    self.party = self.origin.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = self.origin.move.description
                    self.initial_amount = self.origin.credit
                    self.pending_amount = self.origin.pending_amount
                    self.amount = self.origin.pending_amount
                    self.on_change_amount()
                elif (isinstance(self.origin, MoveLine) and
                      self.request.type == 'account.move.posted'):
                    self.party = self.request.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = self.origin.move.description
                    self.initial_amount = self.origin.credit
                    self.pending_amount = self.origin.pending_amount
                    self.amount = self.origin.pending_amount
                    self.on_change_amount()
                elif isinstance(self.origin, employeeLoan):
                    self.party = self.origin.employee.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = 'Anticipo a empleado'
                    self.egress_concept = None
                    self.amount = self.origin.total_amount
                    self.on_change_amount()
                elif isinstance(self.origin, ContractLiquidation):
                    self.party = self.origin.contract.employee.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = (f"Liquidacion de haberes por: "
                                f"{self.origin.reason_liquidation.name}")
                    self.egress_concept = None
                    self.amount = self.origin.total_value
                    self.on_change_amount()
                elif isinstance(self.origin, CreditCard):
                    self.party = self.origin.provider
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = (
                        f"Abono carta de crédito para importación")
                    self.egress_concept = None
                    self.amount = self.origin.amount
                    self.on_change_amount()
                elif isinstance(self.origin, LiquidationCreditCard):
                    self.party = self.origin.provider
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = (
                        f"Ajuste carta de crédito para importación")
                    self.egress_concept = None
                    self.amount = self.origin.adjustment_change_currency
                    self.on_change_amount()
                elif isinstance(self.origin, VacationLiquidation):
                    self.party = self.origin.contract.employee.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = (f"Liquidacion de vacaciones")
                    self.egress_concept = None
                    self.amount = self.origin.amount_pay
                    self.on_change_amount()
                elif isinstance(self.origin, ContractPayment):
                    self.party = self.origin.process.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = 'Pago a proveedores'
                    self.amount = self.origin.total
                    self.on_change_amount()
                    self.egress_concept = None
                elif isinstance(self.origin, LiquidationLine):
                    self.party = self.origin.liquidation.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = 'Liquidación de tercero'
                    self.amount = self.origin.to_pay_amount
                    self.on_change_amount()
                    self.egress_concept = None
                elif isinstance(self.origin, TravelExpense):
                    self.party = self.origin.employee.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = 'Gasto de viaje'
                    self.amount = self.origin.amount_receive
                    self.on_change_amount()
                # PETTY CASH
                elif isinstance(self.origin, PettyCash):
                    self.party = self.origin.responsible.party
                    if self.request.is_new_beneficiary:
                        self.party = self.request.new_beneficiary
                    self.on_change_party()
                    self.description = 'Apertura de caja chica'
                    self.amount = self.origin.amount
                    self.on_change_amount()

    @classmethod
    def get_data(cls, lines, names):
        banks = defaultdict(lambda: None)
        for line in lines:
            if line.bank_account and line.bank_account.bank:
                banks[line.id] = line.bank_account.bank.id
        return {
            'bank': banks,
        }

    @fields.depends('bank_account')
    def on_change_with_bank(self, name=None):
        if self.bank_account and self.bank_account.bank:
            return self.bank_account.bank.id

    @fields.depends('party', 'bank_account', 'request',
        '_parent_request.bank_debit', 'identifier_info')
    def on_change_party(self):
        if self.party:
            if self.party.bank_accounts:
                self.bank_account = self.party.bank_accounts[0]
                if self.party.tax_identifier:
                    self.identifier_info = self.party.tax_identifier.code
                elif self.party.relations and self.party.relations[0]:
                    self.identifier_info = self.party.relations[0].to.tax_identifier.code  # noqa
            else:
                if self.request.bank_debit is False:
                    self.raise_user_error('party_bank', {
                        'party': self.party.rec_name
                    })

    @fields.depends('initial_amount', 'pending_amount', 'amount')
    def on_change_amount(self):
        if self.amount:
            if not self.initial_amount:
                self.initial_amount = self.amount
            if not self.pending_amount:
                self.pending_amount = self.amount

    @fields.depends('party')
    def on_change_with_identifier(self, name=None):
        if self.party:
            if self.party.tax_identifier:
                return self.party.tax_identifier.code
            elif self.party.relations and self.party.relations[0]:
                return self.party.relations[0].to.tax_identifier.code
        return None

    def get_rec_name(self, name):
        if self.request:
            return (f"{self.request.number} {self.party.rec_name} "
                    f"[{self.state_translated.upper()}]")
        else:
            return self.id


class RequestFillLinesStart(ModelView):
    'Request Fill Lines Start'
    __name__ = 'request.fill.lines.start'

    # PAY ORDERS
    account_payments = fields.Many2Many(
        'treasury.account.payment', None, None, 'Pagos',
        domain=[
            ('state', '=', 'approved')
        ],
        states={
            'invisible': ~(Eval('type') == 'treasury.account.payment')
        }, depends=['type'])
    # PAYSLIPS
    payslips = fields.Many2Many(
        'payslip.payslip', None, None, 'Roles individuales',
        states={
            'invisible': ~(Eval('type') == 'payslip.payslip')
        }, domain=[
            ('general', 'in', Eval('payslip_general')),
            ('id', 'not in', Eval('request_lines_payslips')),
            If(Eval('payment_type') == 'employee',
               ('total_value', '>', 0),
               ()),
            If(Bool(Eval('items')),
               ('id', 'in', Eval('payslip_to_pay_rule')),
               ())
        ], depends=['type', 'payslip_general', 'request_lines_payslips',
            'payment_type', 'payslip_to_pay_rule', 'items'])
    payslips_advance = fields.Many2Many(
        'payslip.advance', None, None, 'Roles quincenales individuales',
        states={
            'invisible': ~(Eval('type') == 'payslip.advance')
        }, domain=[
            ('general', 'in', Eval('payslip_general_advance')),
            ('id', 'not in', Eval('request_lines_payslips_advance')),
        ], depends=['type', 'payslip_general_advance',
            'request_lines_payslips_advance'])
    payslips_retired = fields.Many2Many(
        'payslip.retired', None, None, 'Roles Jubilados',
        states={
            'invisible': ~(Eval('type') == 'payslip.retired')
        }, domain=[
            ('general', 'in', Eval('payslip_general_retired')),
            ('id', 'not in', Eval('request_lines_payslips_retired')),
            If(Eval('payment_type') == 'employee',
               ('total_value', '>', 0),
               ()),
            If(Bool(Eval('items')),
               ('id', 'in', Eval('payslip_retired_to_pay_rule')),
               ())
        ], depends=[
            'type', 'payslip_general_retired', 'request_lines_payslips_retired',
            'payment_type', 'items', 'payslip_retired_to_pay_rule'])
    payslip_general = fields.Many2Many(
        'payslip.general', None, None,
        'Rol de pagos',
        domain=[
            ('state', '=', 'done')
        ],
        states={
            'required': (Eval('type') == 'payslip.payslip'),
            'invisible': True
        })
    payslip_general_advance = fields.Many2Many(
        'payslip.advance',
        None, None, 'Rol de Quincena',
        domain=[
            ('account', 'in', Eval('account_move_request'))
        ],
        states={
            'required': (Eval('type') == 'payslip.advance'),
            'invisible': True
        })
    payslip_general_retired = fields.Many2Many(
        'payslip.retired',
        None, None, 'Rol de Jubilados',
        domain=[
            ('state', '=', 'done')
        ],
        states={
            'required': (Eval('type') == 'payslip.retired'),
            'invisible': True
        })
    request_lines_payslips = fields.One2Many('payslip.payslip', None,
        'Líneas de Roles',
        states={
            'invisible': True,
        })
    request_lines_payslips_advance = fields.One2Many('payslip.advance', None,
        'Lineas de Quinceca',
        states={
            'invisible': True,
        })
    request_lines_payslips_retired = fields.One2Many('payslip.retired', None,
        'Lineas rol Jubilados',
        states={
            'invisible': True,
        })
    # INVOICES PAYMENT LINES
    invoice_payment_lines = fields.Many2Many(
        'account.move.line', None, None, 'Lineas de pago de factura',
        states={
            'invisible': (~(Eval('type') == 'account.invoice') &
                         ~((Eval('type') == 'purchase.contract.process') &
                         (Eval('contract_type') == 'payment')
                         ))
        }, domain=[
            # ('id', 'not in', Eval('invoice_payment_move_lines')),
            ('id', 'in', Eval('invoice_selectable_move_payment_lines')),
            ('reconciliation', '=', None)
            # ('move.state', '=', 'posted'),
        ], depends=['type', 'invoice_payment_move_lines',
        'invoice_selectable_move_payment_lines'])
    invoice_selectable_move_payment_lines = fields.One2Many(
        'account.move.line', None,
        'Líneas seleccionables de lineas de asiento de factura',
        states={'invisible': True})
    # TODO: revisar None
    invoice_payment_move_lines = fields.One2Many('account.move.line', None,
        'Líneas de pago de facturas', states={'invisible': True})
    account_move_lines = fields.Many2Many(
        'account.move.line', None, None, 'Líneas de Asientos',
        states={
            'invisible': ~(Eval('type').in_(
                ['account.move', 'account.move.posted']))
        },
        domain=[
            ('id', 'in', Eval('move_lines'))],
        depends=[
            'type', 'account_move', 'move_lines'])
    account_move = fields.Many2Many(
        'account.move', None, None,
        'Lineas de pago de asiento',
        states={
            'required': (Eval('type') == 'account.move'),
            'invisible': True
        })
    move_lines = fields.One2Many('account.move.line', None,
        'Líneas de asiento',
        states={
            'invisible': True,
        })
    # employee LOAN
    employee_loans = fields.Many2Many(
        'company.employee.loan', None, None, 'Anticipos a empleados',
        domain=[('id', 'in', Eval('pending_loan', [-1]))],
        states={
            'invisible': ~(Eval('type') == 'company.employee.loan')
        }, depends=['type', 'pending_loan'])
    pending_loan = fields.Function(
        fields.One2Many('company.employee.loan', None, 'Anticipos pendientes'),
        'on_change_with_pending_loan')
    # Contract liquidation employees
    contract_liquidations = fields.Many2Many(
        'contract.liquidation', None, None, 'liquidción de haberes',
        domain=[('id', 'in', Eval('pending_contract_liquidation', [-1]))],
        states={
            'invisible': ~(Eval('type') == 'contract.liquidation')
        }, depends=['type', 'pending_contract_liquidation'])
    pending_contract_liquidation = fields.Function(
        fields.One2Many('contract.liquidation', None, 'Liquidaciones de '
        'contratos pendientes'), 'on_change_with_pending_contract_liquidation')
    # Credit Card Importation
    credit_card_importation = fields.Many2Many(
        'credit_card.creditcard', None, None, 'Carta crédito importación',
        domain=[('id', 'in', Eval('pending_credit_card_importation', [-1]))],
        states={
            'invisible': ~(Eval('type') == 'credit_card.creditcard')
        }, depends=['type', 'pending_credit_card_importation'])
    pending_credit_card_importation = fields.Function(
        fields.One2Many('credit_card.creditcard', None, 'Cartas de cŕedito'),
        'on_change_with_pending_credit_card_importation')
    # Liquidation Credit Card Importation
    liquidation_credit_card_importation = fields.Many2Many(
        'credit_card.liquidation.creditcard', None, None,
        'Liquidación carta crédito importación',
        domain=[('id', 'in', Eval(
            'liquidation_pending_credit_card_importation', [-1]))],
        states={
            'invisible': ~(Eval('type') == 'credit_card.liquidation.creditcard')
        }, depends=['type', 'liquidation_pending_credit_card_importation'])
    liquidation_pending_credit_card_importation = fields.Function(
        fields.One2Many('credit_card.liquidation.creditcard', None,
        'Liquidation cartas de cŕedito'),
        'on_change_with_liquidation_pending_credit_card_importation')
    # Vacation liquidation employees
    vacation_liquidations = fields.Many2Many(
        'vacation.liquidation', None, None, 'liquidción de vacaciones',
        domain=[('id', 'in', Eval('pending_vacation_liquidation', [-1]))],
        states={
            'invisible': ~(Eval('type') == 'vacation.liquidation')
        }, depends=['type', 'pending_vacation_liquidation'])
    pending_vacation_liquidation = fields.Function(
        fields.One2Many('vacation.liquidation', None, 'Liquidaciones de '
        'contratos pendientes'), 'on_change_with_pending_vacation_liquidation')

    type = fields.Selection(
        [
            ('payslip.payslip', 'Roles individuales'),
            ('payslip.advance', 'Pago Quincenal'),
            ('payslip.retired', 'Pago Jubilados'),
            ('company.employee.loan', 'Anticipo a empleados'),
            ('contract.liquidation', 'liquidación de haberes'),
            ('credit_card.creditcard', 'Carta de crédito importación'),
            ('credit_card.liquidation.creditcard',
             'Carta de crédito importación (Ajuste)'),
            ('vacation.liquidation', 'liquidación de vacaciones'),
            ('account.invoice', 'Líneas de pago de factura'),
            ('party.liquidation', 'Liquidación de tercero'),
            ('account.move', 'Asiento Cartera'),
            ('account.move.posted', 'Asiento de pago'),
            ('purchase.contract.process', 'Pago Contrato'),
            ('company.travel.expense', 'Gastos de viaje'),
            ('treasury.petty.cash', 'Caja chica'),
        ], 'Tipo', required=True, states={'invisible': True})
    payment_lines_state = fields.Selection(
        [
            ('pending', 'Pendiente'),
            ('paid', 'Pagado'),
            ('rejected', 'Negado'),
            ('cancel', 'Cancelado')
        ], 'Estado',
        states={
            'invisible': ~(Eval('type') == 'account.invoice')
        }, depends=['type'])
    # CONTRACT PAYMENT
    contract_payments = fields.Many2Many(
        'purchase.contract.process.payment', None, None, 'Pagos',
        states={
            'invisible': ~((Eval('type') == 'purchase.contract.process') &
                           (Eval('contract_type') == 'advance'))
        }, domain=[
            ('process', 'in', Eval('contract_process')),
            ('id', 'not in', Eval('contract_payment_lines')),
            ('id', 'in', Eval('pending_payment_lines')),
            ('process.state', '=', 'open'),
        ], depends=['type', 'contract_process', 'contract_payment_lines',
                    'pending_payment_lines', 'contract_type'])
    contract_process = fields.Many2Many(
        'purchase.contract.process', None, None, 'Contratos',
        states={
            'required': (Eval('type') == 'purchase.contract.process'),
            'invisible': True
        })
    contract_payment_lines = fields.One2Many(
        'purchase.contract.process.payment', None, 'Líneas de Pago',
        states={
            'invisible': True,
        })
    pending_payment_lines = fields.Function(
        fields.One2Many('purchase.contract.process.payment', None,
        'Anticipos pendientes'), 'on_change_with_pending_payment_lines')
    payment_type = fields.Selection(
        [
            ('party', 'Terceros'),
            ('employee', 'Empleados'),
        ], 'Tipo de pago', sort=False,
        states={
            'invisible': True,
        })
    contract_type = fields.Selection(
        [
            ('payment', 'Pago'),
            ('advance', 'Anticipo'),
        ], 'Tipo de pago',
        states={
            'invisible': True,
        })

    travel_expense = fields.Many2Many(
        'company.travel.expense', None, None, 'Gastos de Viaje',
        domain=[('id', 'in', Eval('pending_travel_expense', [-1]))],
        states={
             'invisible': ~(Eval('type') == 'company.travel.expense')
        }, depends=['type', 'pending_travel_expense'])

    pending_travel_expense = fields.Function(
        fields.One2Many('company.travel.expense', None,
            'Gastos de viaje pendientes'),
        'on_change_with_pending_travel_expense')
    petty_cash = fields.Many2Many(
        'treasury.petty.cash', None, None, 'Caja Chica',
        domain=[('request', '=', None)],
        states={
             'invisible': ~(Eval('type') == 'treasury.petty.cash')
        }, depends=['type'])
    party = fields.Many2One('party.party', 'Tercero',
        states={
            'invisible': True
        })
    payslip_to_pay_rule = fields.Function(
        fields.One2Many('payslip.payslip', None, 'Líneas pendientes'),
        'on_change_with_payslip_to_pay_rule')
    payslip_retired_to_pay_rule = fields.Function(
        fields.One2Many('payslip.retired', None, 'Líneas pendientes'),
        'on_change_with_payslip_retired_to_pay_rule')
    items = fields.Function(fields.Boolean('Rubros',
        states={
            'invisible': True
        }), 'on_change_with_items')

    @staticmethod
    def default_payment_lines_state():
        return 'pending'

    @fields.depends('payment_type')
    def on_change_with_items(self, name=None):
        pool = Pool()
        if self.payment_type == 'party':
            Request = pool.get('treasury.account.payment.request')
            context = Transaction().context
            request = Request(context['active_id'])
            if request.items:
                return True
        else:
            return False

    @fields.depends('id')
    def on_change_with_pending_liquidations(self, name=None):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        Liquidation = pool.get('party.liquidation')
        context = Transaction().context
        request = Request(context['active_id'])
        pending_liquidations = Liquidation.search([
            ('party', '=', request.party),
            ('fiscalyear', '=', request.fiscalyear),
            ['OR',
             ('requests', '=', None),
             ('requests', 'where', [
                 ('state', '!=', 'approved'),
             ])
            ]
        ])
        return [p.id for p in pending_liquidations]

    @fields.depends('type', 'employee_loans')
    def on_change_with_pending_loan(self, names=None):
        if self.type != 'company.employee.loan':
            return []
        pool = Pool()
        ReuestLines = pool.get('treasury.account.payment.request.line.detail')
        request_lines = ReuestLines.search_read([
            ('origin', 'like', 'company.employee.loan,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        loan_ids = [int(row['origin'].split(',')[1]) for row in request_lines]
        Loan = pool.get('company.employee.loan')
        loans = Loan.search([
            ('state', '=', 'open'),
            ('id', 'not in', loan_ids),
        ])
        ids_loans_paid = []
        for loan in loans:
            if loan.total_amount != loan.pending_amount:
                ids_loans_paid.append(loan.id)
        loans = Loan.search_read([
            ('state', '=', 'request'),
            ('type.is_quincenal_loan', '=', False),
            ('id', 'not in', loan_ids),
            ('id', 'not in', ids_loans_paid),
        ], fields_names=['id'])

        return [row['id'] for row in loans]

    @fields.depends('type', 'party', 'contract_liquidations')
    def on_change_with_pending_contract_liquidation(self, names=None):
        liquidations = []
        if self.type != 'contract.liquidation':
            return liquidations
        pool = Pool()
        RequestLines = pool.get('treasury.account.payment.request.line.detail')
        request_lines = RequestLines.search_read([
            ('origin', 'like', 'contract.liquidation,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        liquid_ids = [int(row['origin'].split(',')[1]) for row in request_lines]
        ContractLiquidation = pool.get('contract.liquidation')
        liquidations = ContractLiquidation.search_read([
            ('state', '=', 'done'),
            ('party', '=', self.party.id),
            ('id', 'not in', liquid_ids),
        ], fields_names=['id'])

        return [row['id'] for row in liquidations]

    @fields.depends('party', 'credit_card_importation')
    def on_change_with_pending_credit_card_importation(self, names=None):
        pool = Pool()
        RequestLines = pool.get('treasury.account.payment.request.line.detail')
        request_lines = RequestLines.search_read([
            ('origin', 'like', 'credit_card.creditcard,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        cards_ids = [int(row['origin'].split(',')[1]) for row in request_lines]
        CreditCard = pool.get('credit_card.creditcard')
        creditcards = CreditCard.search_read([
            ('state', '=', 'done'),
            ('provider', '=', self.party.id),
            ('id', 'not in', cards_ids),
        ], fields_names=['id'])

        return [row['id'] for row in creditcards]

    @fields.depends('party', 'liquidation_credit_card_importation')
    def on_change_with_liquidation_pending_credit_card_importation(
            self, names=None):
        pool = Pool()
        RequestLines = pool.get('treasury.account.payment.request.line.detail')
        request_lines = RequestLines.search_read([
            ('origin', 'like', 'credit_card.liquidation.creditcard,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        liquidtions_cards_ids = [
            int(row['origin'].split(',')[1]) for row in request_lines]
        LiquidationCreditCard = pool.get('credit_card.liquidation.creditcard')
        liquidations_creditcards = LiquidationCreditCard.search_read([
            ('state', '=', 'liquidated'),
            ('credit_card.provider', '=', self.party.id),
            ('id', 'not in', liquidtions_cards_ids),
        ], fields_names=['id'])

        return [row['id'] for row in liquidations_creditcards]

    @fields.depends('type', 'vacation_liquidations')
    def on_change_with_pending_vacation_liquidation(self, names=None):
        pool = Pool()
        RequestLines = pool.get('treasury.account.payment.request.line.detail')
        request_lines = RequestLines.search_read([
            ('origin', 'like', 'vacation.liquidation,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        liquid_ids = [int(row['origin'].split(',')[1]) for row in request_lines]
        VacationLiquidation = pool.get('vacation.liquidation')
        liquidations = VacationLiquidation.search_read([
            ('state', '=', 'done'),
            ('id', 'not in', liquid_ids),
        ], fields_names=['id'])

        return [row['id'] for row in liquidations]

    @fields.depends('party', 'request_lines')
    def on_change_with_pending_travel_expense(self, names=None):
        pool = Pool()
        ReuestLines = pool.get('treasury.account.payment.request.line.detail')
        party = self.party
        request_lines = []
        if party is not None:
            request_lines = ReuestLines.search_read([
                ('origin', 'like', 'company.travel.expense,%'),
                ('request.state', '=', 'request'),
                ('party', '=', self.party.id),
            ], fields_names=['origin'])
        loan_ids = [int(row['origin'].split(',')[1]) for row in request_lines]
        TravelExpense = pool.get('company.travel.expense')
        travel_expenses = TravelExpense.search([
            ('state', '=', 'request'),
            ('character', '=', 'planned'),
            ('id', 'not in', loan_ids),
        ])
        travel_expenses = TravelExpense.search_read([
            ('state', '=', 'request'),
            ('id', 'not in', loan_ids),
        ], fields_names=['id'])

        return [row['id'] for row in travel_expenses]

    @fields.depends('payment_type', 'items')
    def on_change_with_payslip_to_pay_rule(self, names=None):
        pool = Pool()
        Payslips = pool.get('payslip.payslip')
        PLines = pool.get('payslip.line')
        Request = pool.get('treasury.account.payment.request')
        items = []
        context = Transaction().context
        request = Request(context['active_id'])
        if self.payment_type == 'party' and request.items:
            for item in request.items:
                items.append(item.id)
        lines = PLines.search_read(
            [('rule', 'in', items)], fields_names=['id'])
        lines_aux = [l.get('id') for l in lines]
        payslips = Payslips.search_read(
             [('lines', 'in', lines_aux)], fields_names=['id'])

        return [row['id'] for row in payslips]

    @fields.depends('payment_type', 'items')
    def on_change_with_payslip_retired_to_pay_rule(self, names=None):
        pool = Pool()
        Payslips = pool.get('payslip.retired')
        PLines = pool.get('payslip.retired.line')
        Request = pool.get('treasury.account.payment.request')
        items = []
        context = Transaction().context
        request = Request(context['active_id'])
        if self.payment_type == 'party' and request.items:
            for item in request.items:
                items.append(item.id)
        lines = PLines.search_read(
            [('rule', 'in', items)], fields_names=['id'])
        lines_aux = [l.get('id') for l in lines]
        payslips = Payslips.search_read(
             [('lines', 'in', lines_aux)], fields_names=['id'])

        return [row['id'] for row in payslips]

    @fields.depends('type', 'contract_payments', 'contract_type')
    def on_change_with_pending_payment_lines(self, names=None):
        pool = Pool()
        RequestLines = pool.get('treasury.account.payment.request.line.detail')
        AccountInvoice = pool.get('account.invoice')
        request_lines = RequestLines.search_read([
            ('origin', 'like', 'purchase.contract.process.payment,%'),
            ('request.state', '!=', 'cancel'),
        ], fields_names=['origin'])
        payment_ids = [
            int(row['origin'].split(',')[1]) for row in request_lines]
        PaymentRequest = pool.get('purchase.contract.process.payment')
        if self.contract_type == 'payment':
            paymentRequests = PaymentRequest.search_read([
                ('type', '=', 'payment'),
                ('id', 'not in', payment_ids),
                ('origin', 'like', 'account.invoice%'),
            ], fields_names=['id', 'origin'])
            lines_to_pay = []
            for row in paymentRequests:
                if not row['origin']:
                    continue
                invoice = AccountInvoice(int(row['origin'].split(',')[1]))
                if invoice.state not in ['cancel', 'draft', 'validated']:
                    lines_to_pay.append(row)
            return [row['id'] for row in lines_to_pay]
        else:
            paymentRequests = PaymentRequest.search_read([
                ('type', '=', 'advance'),
                ('id', 'not in', payment_ids),
            ], fields_names=['id'])
            return [row['id'] for row in paymentRequests]


class RequestFillLines(Wizard):
    'Request Fill Lines'
    __name__ = 'request.fill.lines'

    start = StateView('request.fill.lines.start',
        'treasury.request_fill_lines_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'handle', 'tryton-ok', default=True),
        ])
    handle = StateTransition()

    def default_start(self, fields):
        Request = Pool().get('treasury.account.payment.request')
        request = Request(Transaction().context['active_id'])
        if (request.type == 'treasury.account.payment' or
                request.type == 'company.employee.loan'):
            return {
                'type': request.type
            }
        elif request.type == 'payslip.payslip':
            return {
                'type': request.type,
                'payment_type': request.payment_type,
                'payslip_general': [x.id for x in request.payslip_general],
                'request_lines_payslips':
                    [x.id for x in request.request_lines_payslips]
            }
        elif request.type == 'payslip.advance':
            return {
                'type': request.type,
                'payment_type': request.payment_type,
                'payslip_general_advance':
                    [x.id for x in request.payslip_general_advance],
                'request_lines_payslips_advance':
                    [x.id for x in request.request_lines_payslips_advance]
            }
        elif request.type == 'payslip.retired':
            return {
                'type': request.type,
                'payment_type': request.payment_type,
                'payslip_general_retired':
                    [x.id for x in request.payslip_general_retired],
                'request_lines_payslips_retired':
                    [x.id for x in request.request_lines_payslips_retired]
            }
        elif request.type == 'account.invoice':
            return{
                'type': request.type,
                'invoice_payment_lines': [
                    x.id for x in request.invoice_selectable_move_payment_lines],  # noqa
                'invoice_selectable_move_payment_lines': [
                    x.id for x in request.invoice_selectable_move_payment_lines],  # noqa
                'invoice_payment_move_lines':
                    [x.id for x in request.invoice_payment_move_lines]
            }
        elif request.type == 'account.move':
            return{
                'type': request.type,
                'move_lines':
                    [x.id for x in request.move_lines]
            }
        elif request.type == 'account.move.posted':
            return{
                'type': request.type,
                'move_lines':
                    [x.id for x in request.move_lines]
            }
        elif request.type == 'purchase.contract.process':
            return {
                'type': request.type,
                'contract_type': request.contract_type,
                'contract_process':
                    [x.id for x in request.contract_process],
                'contract_payment_lines':
                    [x.id for x in request.contract_payment_lines],
                'invoice_selectable_move_payment_lines': [
                    x.id for x in request.invoice_selectable_move_payment_lines],  # noqa
                'invoice_payment_move_lines':
                    [x.id for x in request.invoice_payment_move_lines]
            }
        elif request.type == 'party.liquidation':
            return {
                'type': request.type,
            }
        elif request.type == 'company.travel.expense':
            pool = Pool()
            Linedetail = pool.get(
                'treasury.account.payment.request.line.detail')
            result = Linedetail.search([
                ('origin', 'in', [str(x) for x in request.travel_expenses]),
                ('state', 'not in', ['cancel', 'rejected'])
            ])
            lines_found = [line.origin for line in result]
            result = []
            for expense in request.travel_expenses:
                if expense in lines_found:
                    continue
                result.append(expense)
            return {
                'type': request.type,
                'party': request.party.id,
                'pending_travel_expense': [x.id for x in result]
            }
        elif request.type == 'contract.liquidation':
            return {
                'type': request.type,
                'party': request.party.id,
            }
        elif request.type == 'treasury.petty.cash':
            return {
                'type': request.type,
                'party': request.party.id,
            }

        elif request.type == 'vacation.liquidation':
            return {
                'type': request.type,
                'party': request.party.id,
            }
        elif request.type == 'credit_card.creditcard':
            return {
                'type': request.type,
                'party': request.party.id,
            }
        elif request.type == 'credit_card.liquidation.creditcard':
            return {
                'type': request.type,
                'party': request.party.id,
            }

    def transition_handle(self):
        pool = Pool()
        Request = Pool().get('treasury.account.payment.request')
        RequestLine = pool.get('treasury.account.payment.request.line.detail')
        PayslipLine = pool.get('payslip.line')
        PayslipRetiredLine = pool.get('payslip.retired.line')
        to_create = []
        request = Request(Transaction().context['active_id'])
        if self.start.type == 'treasury.account.payment':
            for payment in self.start.account_payments:
                line = RequestLine()
                line.origin = payment
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'payslip.payslip':
            rule_origins = {}
            for payslip in self.start.payslips:
                if request.payment_type == 'employee':
                    line = RequestLine()
                    line.request = request
                    line.origin = payslip
                    line.on_change_origin()
                    to_create.append(line)
                elif request.payment_type == 'party':
                    # search lineas de rol de los rubros de spi
                    role_lines = PayslipLine.search([
                        ('payslip', '=', payslip),
                        ('rule', 'in', request.items)
                    ])
                    if role_lines:
                        # TODO: remove print
                        print('empleado: ',
                              payslip.period.rec_name, '-',
                              payslip.employee.party.name, '-', len(role_lines))
                    for rl in role_lines:
                        line = RequestLine()
                        line.request = request
                        line.origin = rl
                        rule_origins[rl.rule.id] = payslip
                        line.on_change_origin()
                        to_create.append(line)
            if self.start.payment_type == 'party':
                for rule in request.items:
                    if rule.additional_value > 0:
                        pool = Pool()
                        EgressConcept = pool.get(
                            'treasury.account.payment.spi.concept')
                        line = RequestLine()
                        line.party = rule.party
                        line.request = request
                        line.origin = rule_origins.get(rule.id)
                        line.bank_account = rule.party.bank_accounts[0]
                        line.description = (line.origin.template.name + ' - ' +
                            line.origin.period.name)
                        line.amount = rule.additional_value
                        line.egress_concept = EgressConcept.search(
                                [('code', '=', '401011')])[0]
                        line.on_change_amount()
                        line.save()
                        list_temp = list(request.request_lines)
                        list_temp.append(line)
                        request.request_lines = tuple(list_temp)
        elif self.start.type == 'payslip.advance':
            for payslip in self.start.payslips_advance:
                line = RequestLine()
                line.request = request
                line.origin = payslip
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'payslip.retired':
            rule_origins = {}
            for payslip in self.start.payslips_retired:
                if request.payment_type == 'employee':
                    line = RequestLine()
                    line.request = request
                    line.origin = payslip
                    line.on_change_origin()
                    to_create.append(line)
                elif request.payment_type == 'party':
                    role_lines = PayslipRetiredLine.search([
                        ('payslip', '=', payslip),
                        ('rule', 'in', request.items)
                    ])
                    for rl in role_lines:
                        line = RequestLine()
                        line.request = request
                        line.origin = rl
                        rule_origins[rl.rule.id] = payslip
                        line.on_change_origin()
                        to_create.append(line)
            if self.start.payment_type == 'party':
                for rule in request.items:
                    if rule.additional_value > 0:
                        pool = Pool()
                        EgressConcept = pool.get(
                            'treasury.account.payment.spi.concept')
                        line = RequestLine()
                        line.party = rule.party
                        line.request = request
                        line.origin = rule_origins.get(rule.id)
                        line.bank_account = rule.party.bank_accounts[0]
                        line.description = (line.origin.template.name + ' - ' +
                                            line.origin.period.name)
                        line.amount = rule.additional_value
                        line.egress_concept = EgressConcept.search(
                            [('code', '=', '401011')])[0]
                        line.on_change_amount()
                        line.save()
                        list_temp = list(request.request_lines)
                        list_temp.append(line)
                        request.request_lines = tuple(list_temp)
        elif (self.start.type == 'account.invoice' or
                (self.start.type == 'purchase.contract.process' and
                request.contract_type == 'payment')):
            for move_line in self.start.invoice_payment_lines:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'account.move.line':
            for move_line in self.start.move_lines:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'company.employee.loan':
            for loan in self.start.employee_loans:
                line = RequestLine()
                line.request = request
                line.origin = loan
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'contract.liquidation':
            for liquidation in self.start.contract_liquidations:
                line = RequestLine()
                line.request = request
                line.origin = liquidation
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'credit_card.creditcard':
            for card in self.start.credit_card_importation:
                line = RequestLine()
                line.request = request
                line.origin = card
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'credit_card.liquidation.creditcard':
            for liqui_card in self.start.liquidation_credit_card_importation:
                line = RequestLine()
                line.request = request
                line.origin = liqui_card
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'vacation.liquidation':
            for liquidation in self.start.vacation_liquidations:
                line = RequestLine()
                line.request = request
                line.origin = liquidation
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'account.move':
            for move_line in self.start.account_move_lines:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'account.move.posted':
            for move_line in self.start.account_move_lines:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'purchase.contract.process':
            for move_line in self.start.contract_payments:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        elif self.start.type == 'party.liquidation':
            for liquidation in request.liquidations:
                for liq_line in liquidation.lines:
                    if liq_line.to_pay_amount > Decimal("0.0"):
                        line = RequestLine()
                        line.request = request
                        line.origin = liq_line
                        line.on_change_origin()
                        to_create.append(line)
        elif self.start.type == 'company.travel.expense':
            RequestTravelExpense = pool.get(
                'treasury.account.payment.request.travel.expense')

            request_travel_expense_to_create = []
            for travel_expense in self.start.travel_expense:

                line = RequestLine()
                line.request = request
                line.origin = travel_expense
                line.on_change_origin()
                to_create.append(line)
                request_travel_expense = RequestTravelExpense()
                request_travel_expense.request = request
                request_travel_expense.expense = travel_expense
                request_travel_expense_to_create.append(request_travel_expense)
            RequestTravelExpense.save(request_travel_expense_to_create)
        elif self.start.type == 'treasury.petty.cash':
            for move_line in self.start.petty_cash:
                line = RequestLine()
                line.request = request
                line.origin = move_line
                line.on_change_origin()
                to_create.append(line)
        RequestLine.save(to_create)
        return 'end'

    def get_rec_name(self, name):
        return f"{self.number} - {self.party.name}"


class PaymentRequestImportStart(ModelView):
    'Payment Request Import Start'
    __name__ = 'treasury.account.payment.request.import.start'

    source_file = fields.Binary('Source File', required=True)
    template = fields.Binary('Payment Request Excel file template',
        filename='template_filename', file_id='template_path', readonly=True)
    template_filename = fields.Char('RequestTemplate',
        states={
            'invisible': True,
        })
    template_path = fields.Char('Path location PDF', readonly=True)

    @staticmethod
    def default_template_path():
        return './data/payment_request_template.xlsx'

    @staticmethod
    def default_template_filename():
        return "%s.%s" % ('payment_request_template', 'xlsx')

    @staticmethod
    def default_template():
        path = 'treasury/data/payment_request_template.xlsx'
        with file_open(path, 'rb') as f:
            template_file = f.read()
        return template_file


class PaymentRequestImport(Wizard):
    'Payment Request Import'
    __name__ = 'treasury.account.payment.request.import'

    start = StateView('treasury.account.payment.request.import.start',
        'treasury.payment_request_import_start_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('Importar', 'import_', 'tryton-executable'),
        ])
    import_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PaymentRequestImport, cls).__setup__()
        cls._error_messages.update({
            'empty_data': ('Se re an regrestrado los registros %(field)s en el'
                'archivo'),
            'format_error': ('El campo: ( %(field)s , %(value)s ) , tiene un '
                'error de formato.'),
            'index_error': ('El campo: ( %(field)s, %(value)s ), no coincide '
                'con ningún registro.'),
            })

    def transition_import_(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        Party = pool.get('party.party')
        PaymentRequest = pool.get('treasury.account.payment.request')
        field_names = ['party', 'applicant', 'amount', 'concept']
        file_data = fields.Binary.cast(self.start.source_file)
        df = pd.read_excel(
            BytesIO(file_data), names=field_names, dtype='object', header=0)
        df = df.fillna(False)
        to_save = []
        for row in df.itertuples():
            request = PaymentRequest()
            for attr in dir(row):
                if attr in field_names:
                    field = getattr(row, attr, None)
                    if field:
                        try:
                            if attr == 'party':
                                setattr(request, attr, Party.search([
                                    ('tax_identifier', '=', field)
                                    ])[0])
                            elif attr == 'applicant':
                                setattr(request, attr, Employee.search(
                                        [('identifier', '=', str(field))])[0]
                                    )
                            elif attr == 'amount':
                                setattr(request, attr, '%.2f' % float(field))
                            else:
                                setattr(request, attr, field)
                        except ValueError:
                            self.raise_user_error('format_error', {
                                'field': attr,
                                'value': str(field)
                                })
                        except IndexError:
                            self.raise_user_error('index_error', {
                                'field': attr,
                                'value': str(field)
                                })
                    else:
                        self.raise_user_error('empty_data', {'field': attr})
            to_save.append(request)
        PaymentRequest.save(to_save)
        return 'end'
