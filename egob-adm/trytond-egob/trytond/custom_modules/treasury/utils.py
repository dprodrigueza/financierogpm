from sql.aggregate import Aggregate


class JsonAgg(Aggregate):
    __slots__ = ()
    _sql = 'JSON_AGG'
