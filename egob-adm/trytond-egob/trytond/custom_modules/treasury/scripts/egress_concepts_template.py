#!/usr/bin/env python

import sys
import csv

sys.stdout.write('<?xml version="1.0"?>\n')
sys.stdout.write('<tryton>\n')
sys.stdout.write('    <data>\n')

with open('egress_concepts_template.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    plan = ''
    for row in reader:
        record = '''
        <record model="treasury.account.payment.spi.concept" id="treasury_account_payment_spi_concept_%s">
            <field name="code">%s</field>
            <field name="name">%s</field>
        </record>\n''' % (row['code'],
                          row['code'],
                          row['name']
                          )
        sys.stdout.write(record)
sys.stdout.write('    </data>\n')
sys.stdout.write('</tryton>\n')
