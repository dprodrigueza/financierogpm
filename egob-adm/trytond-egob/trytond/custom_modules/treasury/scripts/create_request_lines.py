from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from proteus import config, Model

import pandas as pd
# import pysnooper


# @pysnooper.snoop(depth=2, prefix='ZZZ ')
def create_lines(database, trytond_config, filename):
    config.set_trytond(database, config_file=trytond_config)
    Party = Model.get('party.party')
    Request = Model.get('treasury.account.payment.request')
    RequestLine = Model.get('treasury.account.payment.request.line.detail')
    request = Request(564)
    df = pd.read_excel(filename, encoding='utf-8', dtype='object')
    df = df.astype(object).where(pd.notnull(df), None)
    for row in df.itertuples():
        line = RequestLine()
        line.request = request
        line.party = Party(row.party)
        line.bank_account = line.party.bank_accounts[0]
        line.initial_amount = line.debit
        line.pending_amount = line.debit
        line.amount = line.debit
        line.save()


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--database', dest='database', required=True,
        help='database')
    parser.add_argument('--config-file', dest='config_file', required=True,
        help='trytond config file')
    parser.add_argument('--filename', dest='filename', required=True,
        help='Filename')
    options = parser.parse_args()
    create_lines(options.database, options.config_file, options.filename)
