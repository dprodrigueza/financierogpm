from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.model import fields
from collections import defaultdict
from trytond.transaction import Transaction
from sql.aggregate import Sum
from trytond.tools import reduce_ids, grouped_slice
__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    pending = fields.Function(
        fields.Numeric('Pendiente', digits=(16, 2)), 'get_request_amounts')
    to_pay = fields.Function(
        fields.Numeric('A Pagar', digits=(16, 2)), 'get_request_amounts')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.bank_accounts.context.update({
            'party': Eval('id')
        })
        cls.bank_accounts.depends += ['id']

    @classmethod
    def get_request_amounts(cls, parties, names):
        context = Transaction().context
        payment_id = context['payment_request_id']
        pool = Pool()
        table = cls.__table__()
        cursor = Transaction().connection.cursor()

        pendings = defaultdict(lambda: 0)
        to_paids = defaultdict(lambda: 0)
        PaymentRequestLine = pool.get(
            'treasury.account.payment.request.line.detail')
        payment_request_line = PaymentRequestLine.__table__()
        PaymentRequest = pool.get('treasury.account.payment.request')
        payment_request = PaymentRequest.__table__()

        ids = [b.id for b in parties]
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(payment_request_line,
                condition=table.id == payment_request_line.party
            ).join(payment_request,
                condition=payment_request.id == payment_request_line.request
            ).select(
                table.id,
                Sum(payment_request_line.pending_amount).as_("pending_amount"),
                Sum(payment_request_line.amount).as_("amount"),
                where=red_sql & (payment_request.id == payment_id),
                group_by=[table.id]
            )
            cursor.execute(*query)
            for party_id, pending_amount, amount in cursor.fetchall():
                pendings[party_id] = pending_amount
                to_paids[party_id] = amount
        return {
            'pending': pendings,
            'to_pay': to_paids,
        }
