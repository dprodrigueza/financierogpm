from trytond.modules.company import CompanyReport
from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.pool import Pool
from trytond.transaction import Transaction
from unidecode import unidecode

__all__ = [
    'SPIReportMixin', 'AccountPaymentSPITXT', 'WorkWarranty',
    'WorkWarrantyExpiration',
    'AccountPaymentSPIODS', 'AccountPaymentSPIGrouped', 'TreasuryAccountPayment', 'EntryNote',
    'PaymentRequest', 'CheckDrawn', 'CheckReceived', 'CheckReturned',
    'CheckConsulting'
]


class SPIReportMixin(object):
    'SPI Report Mixin'

    @classmethod
    def get_context(cls, records, data):
        method = cls._get_lines
        for record in records:
            for line in record.lines:
                if (line.request.payment_type == 'party' or
                        line.request.type == 'account.invoice'):
                    method = cls._get_grouped_party
        report_context = super(
            SPIReportMixin, cls).get_context(records, data)
        report_context['spi_lines'] = method
        report_context['spi_grouped_lines'] = cls._get_grouped_lines
        report_context['spi_grouped'] = cls._get_lines_grouped
        report_context['mount_value'] = cls.mount_value
        report_context['spi_party_lines'] = cls._get_party_lines
        return report_context

    @classmethod
    def _normalizar_campo(cls, campo, size, trim=True, decode=True):
        # cortamos la cadena
        if len(campo) > size:
            campo = campo[:size]
        # Remplazamos tildes, eñes, etc
        if decode:
            campo = unidecode(campo)
        # Eliminamos espacios
        if trim:
            campo = campo.strip()
        # Retornamos el campo sin comas
        return campo.replace(",", '')

    @classmethod
    def _get_lines(cls, spi):
        data2 = {}
        for line in spi.lines:
            # 0 -> IDENTIFIER
            # 1 -> PARTY NAME
            # 2 -> ACCOUNT NUMBER
            # 3 -> AMOUNT
            # 4 -> PROVIDER ACCOUNT BANK BIC
            # 5 -> ACCOUNT_TYPE
            # 6 -> SPI NUMBER
            # 7 -> PAYMENT_DESCRIPTION
            # 8 -> BANK-BIC
            # 9 -> EGRESS-CONCEPT
            # 10 -> STATE
            bank_number = ''
            if line.bank_account:
                bank_number = line.bank_account.numbers[0].number
            if line.identifier_info:
                identifier = line.identifier_info
            else:
                identifier = line.identifier
            if not data2.get(line.id):
                data2.update({
                    line.id: [
                        int(str(line.request.receipt).split('-')[2]),
                        cls._normalizar_campo(line.party.name, size=28),
                        bank_number,
                        0,
                        line.bank.bic,
                        line.bank_account.account_type,
                        spi.number,
                        cls._normalizar_campo(str(line.description), size=79),
                        line.bank.bic,
                        line.egress_concept.code,
                        identifier,
                        line.state_translated
                        ]})
            data2[line.id][3] += line.amount
        data = {}
        for payment_id, values in data2.items():
            bank_bic = "%08d" % (int(values[4]))
            account_type = None
            if values[5] == 'card':
                account_type = '5'
            if values[5] == 'savings':
                account_type = '2'
            if values[5] == 'current':
                account_type = '1'
            data.update({
                payment_id: [
                    values[0],
                    values[1],
                    str(values[2]),
                    str(values[3]),
                    str(bank_bic),
                    account_type,
                    values[6],
                    values[7],
                    values[8],
                    values[9],
                    values[10],
                    values[11]
                    ]
            })
        return data

    @classmethod
    def _get_lines_grouped(cls, spi):
        data2 = {}
        for line in spi.lines:
            # 0 -> IDENTIFIER
            # 1 -> PARTY NAME
            # 2 -> ACCOUNT NUMBER
            # 3 -> AMOUNT
            # 4 -> PROVIDER ACCOUNT BANK BIC
            # 5 -> ACCOUNT_TYPE
            # 6 -> SPI NUMBER
            # 7 -> PAYMENT_DESCRIPTION
            # 8 -> BANK-BIC
            # 9 -> EGRESS-CONCEPT
            # 10 -> STATE
            bank_number = ''
            if line.identifier_info:
                identifier = line.identifier_info
            else:
                identifier = line.identifier

            if not data2.get(line.bank_account.numbers[0].number):
                data2.update({
                    line.bank_account.numbers[0].number: [
                        int(str(line.request.receipt).split('-')[2]),
                        cls._normalizar_campo(line.party.name, size=28),
                        line.bank_account.numbers[0].number,
                        0,
                        line.bank.bic,
                        line.bank_account.account_type,
                        spi.number,
                        cls._normalizar_campo(str(line.description), size=79),
                        line.bank.bic,
                        line.egress_concept.code,
                        identifier,
                        line.state_translated
                    ]})
            data2[line.bank_account.numbers[0].number][3] += line.amount
        data = {}
        for payment_id, values in data2.items():
            bank_bic = "%08d" % (int(values[4]))
            account_type = None
            if values[5] == 'card':
                account_type = '5'
            if values[5] == 'savings':
                account_type = '2'
            if values[5] == 'current':
                account_type = '1'
            data.update({
                payment_id: [
                    values[0],
                    values[1],
                    str(values[2]),
                    str(values[3]),
                    str(bank_bic),
                    account_type,
                    values[6],
                    values[7],
                    values[8],
                    values[9],
                    values[10],
                    values[11]
                ]
            })
        return data

    @classmethod
    def _get_grouped_lines(cls, spi):
        data2 = {}
        for line in spi.lines:
            # 0 -> IDENTIFIER
            # 1 -> PARTY NAME
            # 2 -> ACCOUNT NUMBER
            # 3 -> AMOUNT
            # 4 -> PROVIDER ACCOUNT BANK BI
            # 5 -> UNKNOW VALUE
            if line.identifier_info:
                identifier = line.identifier_info
            else:
                identifier = line.identifier
            if not data2.get(line.bank_account.numbers[0].number):
                data2.update({
                    line.bank_account.numbers[0].number: [
                        identifier,
                        cls._normalizar_campo(line.party.name, size=28),
                        line.bank_account.numbers[0].number,
                        0,
                        line.bank.bic,
                        1,
                    ]})
            data2[line.bank_account.numbers[0].number][3] += line.amount
        data = {}
        for payment_id, values in data2.items():
            data.update({
                payment_id: [
                    values[0],
                    values[1],
                    str(values[2]),
                    str(values[3]),
                    str(values[4]),
                    str(values[5])
                    ]
                })
        return data

    @classmethod
    def _get_grouped_party(cls, spi):
        data2 = {}
        for line in spi.lines:
            # 0 -> REQUEST NUMBER
            # 1 -> PARTY NAME
            # 2 -> ACCOUNT NUMBER
            # 3 -> AMOUNT
            # 4 -> PROVIDER ACCOUNT BANK BIC
            # 5 -> ACCOUNT_TYPE
            # 6 -> SPI NUMBER
            # 7 -> PAYMENT_DESCRIPTION
            # 8 -> BANK-BIC
            # 9 -> EGRESS-CONCEPT
            bank_number = ''
            a_type = ''
            if line.bank_account:
                bank_number = line.bank_account.numbers[0].number
                a_type = line.bank_account.account_type
            description = line.description
            bank_bic = 0
            if line.bank:
                bank_bic = line.bank.bic
            if line.request.type == 'payslip.payslip':
                key = bank_number
            if line.request.type == 'account.invoice':
                key = line.origin.move if line.origin else line.id
            if line.request.type not in ('account.invoice', 'payslip.payslip'):
                key = line.id
            e_code = line.egress_concept.code if line.egress_concept else ''
            if line.identifier_info:
                identifier = line.identifier_info
            else:
                identifier = line.identifier
            if not data2.get(key):
                data2.update({
                    key: [
                        int(str(line.request.receipt).split('-')[2]),
                        cls._normalizar_campo(line.party.name, size=28),
                        bank_number,
                        0,
                        bank_bic,
                        a_type,
                        spi.number,
                        cls._normalizar_campo(str(description), size=79),
                        bank_bic,
                        e_code,
                        identifier,
                        line.state_translated,
                    ]})
            data2[key][3] += line.amount
        data = {}
        for payment_id, values in data2.items():
            bank_bic = "%08d" % (int(values[4]))
            account_type = None
            if values[5] == 'card':
                account_type = '5'
            if values[5] == 'savings':
                account_type = '2'
            if values[5] == 'current':
                account_type = '1'
            data.update({
                payment_id: [
                    values[0],
                    values[1],
                    str(values[2]),
                    str(values[3]),
                    str(bank_bic),
                    account_type,
                    values[6],
                    values[7],
                    values[8],
                    values[9],
                    values[10],
                    values[11]
                ]
            })
        return data

    @classmethod
    def _get_party_lines(cls, spi):
        data2 = {}
        for line in spi.lines:
            # 0 -> IDENTIFIER
            # 1 -> EMPLOYEE NAME
            # 3 -> AMOUNT
            # 4 -> TOTAL
            vals = []
            pool = Pool()
            PayslipRule = pool.get('payslip.line')

            if isinstance(line.origin, PayslipRule):
                vals = [
                    spi.company.rec_name,
                    'Pago Comisiones',
                    line.amount
                ]
            else:
                continue
                # vals = [
                #     line.origin.payslip_employee.party.tax_identifier.code,
                #     line.origin.payslip_employee.party.name,
                #     line.origin.amount
                # ]
            if line.party.name in data2:
                data2[line.party.name].append(vals)
            else:
                data2[line.party.name] = [vals]

        data = {}
        for payment_id, values in data2.items():
            values = sorted(values, key=lambda x: x[1])
            for value in values:
                vals = [
                        value[0],
                        value[1][0:28],
                        str(value[2])
                ]
                if payment_id in data:
                    data[payment_id].append(vals)
                else:
                    data[payment_id] = [vals]
        return data

    @classmethod
    def mount_value(cls, mount):
        val1 = "%.2f" % (mount)
        val2 = val1.replace(".", '')
        str_val_aux = str("%09d" % int(val2))
        str_val = str_val_aux[:-2] + '.' + str_val_aux[-2:]
        return str_val


class AccountPaymentSPITXT(SPIReportMixin, CompanyReport):
    __name__ = 'treasury.account.payment.spi.txt'


class AccountPaymentSPIGrouped(SPIReportMixin, CompanyReportSignature):
    __name__ = 'treasury.account.payment.spi.grouped'


class AccountPaymentSPIODS(SPIReportMixin, CompanyReportSignature):
    __name__ = 'treasury.account.payment.spi.ods'


class AccountPaymentSPIzipTXT(SPIReportMixin, CompanyReport):
    __name__ = 'treasury.account.payment.spi.zip.txt'


class WorkWarranty(CompanyReportSignature):
    'Work Warranty Report'
    __name__ = 'work.warranty.report'

class WorkWarrantyExpiration(CompanyReportSignature):
    'Work Warranty Expiration Report'
    __name__ = 'work.warranty.expiration.report'

class TreasuryAccountPayment (CompanyReport):
    __name__ = 'treasury.account.payment.fodt'


class TreasuryAccountPaymentSpi (CompanyReportSignature):
    __name__ = 'treasury.account.payment.spi.fodt'


class TreasuryAccountPaymentSpiSummary (CompanyReportSignature):
    __name__ = 'treasury.account.payment.spi_summary.fodt'


class EntryNote(CompanyReportSignature):
    __name__ = 'treasury.entry.note'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(EntryNote, cls).get_context(
            records, data)
        context = Transaction().context
        employee_id = context.get('employee')
        if employee_id:
            pool = Pool()
            Employee = pool.get('company.employee')
            employee, = Employee.browse([employee_id])
            report_context['user_print'] = employee.party.name
        else:
            report_context['user_print'] = "Configurar usuario"
        return report_context


class PaymentRequest(CompanyReportSignature):
    __name__ = 'treasury.account.payment.request'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PaymentRequest, cls).get_context(
            records, data)
        condition = False
        for payment in records:
            for line in payment.request_lines:
                if line.amount < line.pending_amount:
                    condition = True
                    break

        report_context['condition'] = condition
        report_context['contract_lines'] = cls._get_contract_payment
        report_context['notes'] = cls._get_notes
        return report_context

    @classmethod
    def _get_contract_payment(cls, records):
        ContractPayment = Pool().get('purchase.contract.process.payment')
        Invoice = Pool().get('account.invoice')
        AccountLine = Pool().get('account.move.line')
        PaymentLines = Pool().get('account.invoice-account.move.line')
        is_account = False
        deduction = 0
        total_payment = 0
        account_values = []
        result = {}
        lines = []
        for line in records.request_lines:
            if str(line.origin).startswith('account.move.line'):
                is_account = True
                cpayment = Invoice.search([
                    'id', '=', str(line.origin.origin_).split(',')[1]
                ])
                payment_lines = PaymentLines.search([
                    'invoice', '=', line.origin.origin_
                ])
                if payment_lines:
                    lines = [
                        str(move.line).split(',')[1] for move in payment_lines]
                account_values = AccountLine.search([
                    'id', 'in', lines
                ])
                account_amount = line.origin.credit
                total_payment += line.amount
                result[line.origin.id] = [
                    0,
                    account_amount,
                    0,
                    line.amount
                ]
            else:
                cpayment = ContractPayment.search([
                    'id', '=', str(line.origin).split(',')[1]
                ])
        if account_values:
            for account in account_values:
                deduction += account.debit
        result[line.origin.id][0] = deduction
        # total_payment = total_payment - deduction

        return [cpayment, is_account, result, total_payment]
    
    @classmethod
    def _get_notes(cls, records):
        pool = Pool()
        Notes = pool.get('ir.note')
        Request = pool.get('treasury.account.payment.request')
        data = {}

        notes = Notes.search([
            'resource', 'like', 'treasury.account.payment.request,%'
        ])
        for note in notes:
            if note.resource:
                id = note.resource.id
                request, = Request.search(['id', '=', id])
                data[note.id] = [
                    str(note.create_date).split(' ')[0],
                    note.message,
                    str(request.request_date).split(' ')[0],
                    request.number
                ]
        return data


class CheckDrawn(CompanyReportSignature):
    __name__ = 'checkbook.check.drawn'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(CheckDrawn, cls).get_context(
            records, data)
        return report_context


class CheckReceived(CompanyReportSignature):
    __name__ = 'checkbook.check.received'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(CheckReceived, cls).get_context(
            records, data)
        return report_context


class CheckReturned(CompanyReportSignature):
    __name__ = 'checkbook.check.returned'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(CheckReturned, cls).get_context(
            records, data)
        return report_context


class CheckConsulting(CompanyReportSignature):
    __name__ = 'checkbook.check.consulting'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(CheckConsulting, cls).get_context(
            records, data)
        return report_context
