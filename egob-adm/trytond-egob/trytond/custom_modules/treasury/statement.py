import os
import re
import tempfile
import pandas as pd
from datetime import datetime
from subprocess import Popen, PIPE
from io import BytesIO
from decimal import Decimal

from sql import Null
from sql.aggregate import Sum
from sql.functions import Extract
from sql.operators import Concat

from .utils import JsonAgg

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.tools import reduce_ids
from trytond.transaction import Transaction
from trytond.modules.account_bank_statement.statement import _STATES

__all__ = ['Statement', 'StatementLine', 'ImportStart', 'Import']


class Statement(metaclass=PoolMeta):
    __name__ = 'account.bank.statement'

    @classmethod
    def __setup__(cls):
        super(Statement, cls).__setup__()
        cls._error_messages.update({
            'conciliation_spi_multiple_matches': ('El SPI: %(spi_number)s con '
                'el concepto de egreso: "%(spi_concept)s" debera ser '
                'conciliado manualmente debido a que coincide con '
                'las siguientes lineas del extracto bancario:\n'
                '%(blines)s\n'),
        })

    @classmethod
    def search_reconciliations(cls, statements):
        cls.spi_reconciliations(statements)
        super(Statement, cls).search_reconciliations(statements)

    @classmethod
    def spi_reconciliations(cls, statements):
        pool = Pool()
        Concept = pool.get('treasury.account.payment.spi.concept')
        StatementLine = pool.get('account.bank.statement.line')
        Reconciliation = pool.get(
            'account.move.line-account.bank.statement.line')
        cursor = Transaction().connection.cursor()
        query_bank_lines = cls.query_bank_lines(statements)
        query_spi = cls.query_spi()
        query_conciliation = query_bank_lines.join(query_spi,
            condition=(
                (query_spi.spi_amount*-1 == query_bank_lines.amount) &
                (query_spi.concept_id == query_bank_lines.concept_id) &
                (query_spi.year == query_bank_lines.year) &
                (query_spi.month == query_bank_lines.month) &
                (query_spi.account == query_bank_lines.account)
            )
        ).select(
            query_bank_lines.id.as_('bline'),
            query_spi.aml_ids,
            query_spi.spi_number,
            query_spi.concept_id,
        )

        query_bline_agg = query_conciliation.select(
            JsonAgg(query_conciliation.bline).as_('blines'),
            Concat(query_conciliation.aml_ids, '').cast('JSON').as_('aml_ids'),
            query_conciliation.spi_number,
            query_conciliation.concept_id,
            group_by=[
                Concat(query_conciliation.aml_ids, ''),
                query_conciliation.spi_number,
                query_conciliation.concept_id,
            ]
        )

        cursor.execute(*query_bline_agg)
        query_result = cursor.fetchall()
        to_save = []
        for blines, aml_ids, spi_number, concept_id in query_result:
            if len(blines) == 1:
                reconciliation = Reconciliation()
                reconciliation.bank_lines = blines
                reconciliation.move_lines = aml_ids
                to_save.append(reconciliation)
            else:
                concept = Concept(concept_id)
                blines_message = ''
                for bline in blines:
                    statement_line = StatementLine(bline)
                    blines_message = (blines_message +
                        statement_line.date.strftime("%Y:%m:%d") + ' - "' +
                        statement_line.description + '": ' +
                        str(statement_line.amount) + '\n')
                cls.raise_user_warning('conciliation_spi_multiple_matches',
                    'conciliation_spi_multiple_matches', {
                        'spi_number': spi_number,
                        'spi_concept': concept.rec_name,
                        'blines': blines_message
                    })
        Reconciliation.save(to_save)

    @classmethod
    def query_bank_lines(cls, statements):
        pool = Pool()
        ids = [s_.id for s_ in statements]
        StatementLine = pool.get('account.bank.statement.line')
        statement_line = StatementLine.__table__()
        statement = cls.__table__()
        red_sql = reduce_ids(statement.id, ids)
        query = statement_line.join(statement,
            condition=(statement.id == statement_line.statement)).select(
            statement_line.id,
            statement_line.date_utc,
            statement_line.description,
            statement_line.amount,
            statement_line.egress_concept.as_('concept_id'),
            Extract('year', statement.start_date).as_('year'),
            Extract('month', statement.start_date).as_('month'),
            statement.account,
            where=(
                red_sql &
                (statement_line.bank_reconciliation == Null) &
                (statement_line.egress_concept != Null))
        )
        return query

    @classmethod
    def query_spi(cls):
        pool = Pool()
        Request = pool.get('treasury.account.payment.request')
        request = Request.__table__()
        Line = pool.get('treasury.account.payment.request.line.detail')
        line = Line.__table__()
        Spi = pool.get('treasury.account.payment.spi')
        spi = Spi.__table__()
        Am = pool.get('account.move')
        am = Am.__table__()
        Aml = pool.get('account.move.line')
        aml = Aml.__table__()
        Aa = pool.get('account.account')
        aa = Aa.__table__()

        # query1: sum spi_line.amount
        query1 = request.join(line,
            condition=(line.request == request.id)
        ).join(spi,
            condition=(spi.id == line.spi)
        ).join(am,
            condition=(am.id == request.move)
        ).join(aml,
            condition=(aml.move == am.id)
        ).join(aa,
            condition=(aa.id == aml.account)
        ).select(
            spi.id.as_('spi_id'),
            spi.number.as_('spi_number'),
            spi.payment_date,
            Sum(line.amount).as_('amount'),
            line.egress_concept.as_('concept_id'),
            aml.credit,
            aml.id.as_('aml_id'),
            am.date.as_('move_date'),
            aml.account,
            where=(
                (aa.bank_reconcile == True) &
                (am.state == 'posted') &
                (aml.bank_reconciliation == Null)
            ),
            group_by=[
                spi.id, spi.number, spi.payment_date, line.egress_concept,
                aml.credit, aml.id, am.date, aml.account]
        )

        # query: sum spi_amount, sum credit, json_agg aml_ids
        query = query1.select(
            query1.spi_id,
            query1.payment_date,
            Sum(query1.amount).as_('spi_amount'),
            query1.spi_number,
            query1.concept_id,
            Sum(query1.credit).as_('credit'),
            JsonAgg(query1.aml_id).as_('aml_ids'),
            query1.move_date,
            Extract('year', query1.move_date).as_('year'),
            Extract('month', query1.move_date).as_('month'),
            query1.account,
            group_by=[query1.spi_id, query1.spi_number, query1.payment_date,
                query1.concept_id, query1.move_date, query1.account],
            having=(Sum(query1.amount) == Sum(query1.credit))
        )
        return query


class StatementLine(metaclass=PoolMeta):
    'Bank Statement Line'
    __name__ = 'account.bank.statement.line'

    egress_concept = fields.Many2One('treasury.account.payment.spi.concept',
        'Concepto de egreso', states=_STATES)
    collection_date = fields.Date('Fecha de recaudación',
        states=_STATES)
    observations = fields.Char('Observaciones',
        states=_STATES)


class ImportStart(metaclass=PoolMeta):
    'Import Start'
    __name__ = 'account.bank.statement.import.start'

    @classmethod
    def __setup__(cls):
        super(ImportStart, cls).__setup__()
        cls.type.selection.append(('pdf_bce', 'PDF (BANCO CENTRAL DE ECUADOR)'))


class Import(metaclass=PoolMeta):
    'Import'
    __name__ = 'account.bank.statement.import'

    @classmethod
    def __setup__(cls):
        super(Import, cls).__setup__()
        cls._error_messages.update({
            'error_file_bce': ('El archivo no cumple con la estructura para la '
                'importación'),
            'error_start_date': 'Las fechas iniciales no coinciden',
            'error_end_date': 'Las fechas finales no coinciden',
            'error_debits': ('No se puedo validar los debitos del extracto '
                 'bancario, el archivo debe ser importado de forma manual'),
            'error_credits': ('No se puedo validar los creditos del extracto '
                 'bancario, el archivo debe ser importado de forma manual'),
        })

    def process(self, statement):
        super(Import, self).process(statement)
        if self.start.type != 'pdf_bce':
            return
        pool = Pool()
        BankStatementLine = pool.get('account.bank.statement.line')
        pdf_file = BytesIO(self.start.import_file)
        tmp_file = tempfile.NamedTemporaryFile()
        tmp_file.write(pdf_file.getvalue())
        df_detail = self._process_bce(tmp_file, type='detail')
        df_total = self._process_bce(tmp_file, type='total')
        df_range = self._process_bce(tmp_file, type='range')
        tmp_file.close()
        if len(df_range) == 0:
            self.raise_user_error('error_file_bce')
        for row in df_range.itertuples():
            start_date = datetime.strptime(row.DESDE, '%m-%d-%Y').date()
            end_date = datetime.strptime(row.HASTA, '%m-%d-%Y').date()
            if start_date != statement.start_date:
                self.raise_user_error('error_start_date', {
                    'start_date_bce': start_date,
                    'start_date_statement': statement.start_date,
                })
            if end_date != statement.end_date:
                self.raise_user_error('error_end_date', {
                    'end_date_bce': end_date,
                    'end_date_statement': statement.end_date,
                })
        lines = []
        sum_debits = Decimal('0.0')
        sum_credits = Decimal('0.0')
        for row in df_detail.itertuples():
            line = BankStatementLine()
            line.statement = statement
            line.date = self.string_to_date(
                f'{row.FECHA}/{str(statement.start_date.year)}')
            line.description = row.REFERENCIA
            if row.DEBITOS:
                line.amount = self.string_to_number(row.DEBITOS) * -1
                sum_debits += self.string_to_number(row.DEBITOS)
            elif row.CREDITOS:
                line.amount = self.string_to_number(row.CREDITOS)
                sum_credits += self.string_to_number(row.CREDITOS)
            if row.AUX1:
                self._process_bce_aux(line, row.AUX1)
            if row.AUX2:
                self._process_bce_aux(line, row.AUX2)
            if row.AUX3:
                self._process_bce_aux(line, row.AUX3)
            lines.append(line)
        for row in df_total.itertuples():
            total_debit = self.string_to_number(row.DEBITO)
            if total_debit != sum_debits:
                self.raise_user_error('error_debits', {
                    'total_debit': total_debit,
                    'sum_debits': sum_debits,
                })
            total_credit = self.string_to_number(row.CREDITO)
            if total_credit != sum_credits:
                self.raise_user_error('error_credits', {
                    'total_credit': total_credit,
                    'sum_credits': sum_credits,
                })
        BankStatementLine.save(lines)

    def _process_bce(self, file, type):
        command = (f'sh {os.getcwd()}'
                   f'/trytond-egob/trytond/modules/treasury'
                   f'/scripts/import_bank_statement_bce.sh -{type} '
                   f'{file.name}')
        # TODO raise_user_error when import_bank_statement_bce.sh not found
        process = Popen(command, shell=True, stdin=PIPE, stdout=PIPE)
        output = process.communicate()[0]
        keys = {
            'detail': [
                'FECHA', 'HORA', 'OFI', 'COMPROBANTE', 'DOCUMENTO',
                'REFERENCIA', 'DEBITOS', 'CREDITOS', 'SALDO', 'CONTRACUENTA',
                'AUX1', 'AUX2', 'AUX3', 'AUX4', 'AUX5'],
            'total': [
                'CUENTA', 'DEBITO', 'CREDITO', 'SALDO'],
            'range': [
                'DESDE', 'HASTA'],
        }
        df = pd.read_csv(BytesIO(output), delimiter='|', names=keys[type],
            dtype='object', header=None)
        df = df.astype(object).where(pd.notnull(df), None)
        return df

    def _process_bce_aux(self, line, aux):
        if (len(aux[0:6]) and
                re.search('[0-9]{0,6}', aux[0:6]).group(0) not in ''):
            pool = Pool()
            SpiConcept = pool.get('treasury.account.payment.spi.concept')
            concept_code = str(int(aux.split(' ')[0]))
            egress_concept = SpiConcept.search([
                ('code', '=', concept_code)
            ])
            if egress_concept:
                line.egress_concept = egress_concept[0]
        elif len(aux) >= 21 and aux[0:21] == 'Fecha de Recaudacion:':
            line.collection_date = self.string_to_date(
                aux.replace('Fecha de Recaudacion: ', ''))
        elif len(aux) >= 4 and aux[0:4] == 'Obs:':
            line.observations = aux.replace('Obs:', '')
