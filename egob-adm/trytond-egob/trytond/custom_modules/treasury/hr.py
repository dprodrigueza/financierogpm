from collections import defaultdict

from trytond.model import fields, ModelView, Workflow
from trytond.pyson import Eval, Bool
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids, cursor_dict

__all__ = ['TravelExpense', 'TravelExpenseAccount', 'EmployeeLoan']


class TravelExpense(metaclass=PoolMeta):
    'Company - Employee - Travel - Expense'
    __name__ = 'company.travel.expense'

    entry_note = fields.Many2One('treasury.entry.note', 'Nota de Ingreso',
             states={
             'readonly': True#(~Eval('state').in_(['liquidate']))
             })
    entry_note_state = fields.Function(
        fields.Selection([
            ('draft', 'Borrador'),
            ('done', 'Realizado'),
            ('confirmed', 'Confirmado'),
            ('cancel', 'Cancelado'),
        ], 'Estado Nota de ingreso', states={
            'invisible': (~Eval('state').in_(
                ['liquidate'])
                          ),
        }), 'get_entry_note_state',
        searcher='search_entry_note_state')

    @classmethod
    def search_entry_note_state(cls, name, clause):
        return [('entry_note.state',) + tuple(clause[1:])]

    @classmethod
    def get_entry_note_state(cls, entry_notes, name):
        ids = [en.id for en in entry_notes]

        cursor = Transaction().connection.cursor()
        EntryNote = Pool().get('treasury.entry.note')
        entry_note = EntryNote.__table__()
        travel_expense = cls.__table__()
        result = {}

        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(travel_expense.id, sub_ids)
            query = travel_expense.join(entry_note, 'LEFT',
                condition=(entry_note.id == travel_expense.entry_note)
                ).select(
                travel_expense.id,
                entry_note.state,
                where=red_sql
            )
            cursor.execute(*query)

            result.update(dict(cursor.fetchall()))

        return result

    @classmethod
    @ModelView.button
    #@Workflow.transition('posted')
    def post_travel(cls, expenses):
        super(TravelExpense, cls).post_travel(expenses)

        pool = Pool()
        EntryNote = pool.get('treasury.entry.note')
        EntryNoteType = pool.get('treasury.entry.note.type')
        PaymentMethods = pool.get('treasury.entry.note.payment.methods')
        SiimPaymentmethods = pool.get('siim.account.recaudacion.forma.pago')
        TravelExpenses = pool.get('company.travel.expense')
        TravelExpenseAccount = pool.get('travel.expense.account')


        travel_expense_account = TravelExpenseAccount.__table__()
        cursor = Transaction().connection.cursor()


        entry_notes_to_save = []
        travel_expenses_to_save = []


        for travel_expense in expenses:
            new_entry_note = None
            if travel_expense.amount_return <= 0:
                continue
            if travel_expense.entry_note:
                new_entry_note = travel_expense.entry_note
                PaymentMethods.delete(travel_expense.entry_note.payment_methods)
                EntryNote(travel_expense.entry_note)
            else:
                new_entry_note = EntryNote()

            query = travel_expense_account.select(
                travel_expense_account.entry_note_type.as_('entry_note_type'),
                travel_expense_account.payment_method.as_('payment_method'),
                where=((travel_expense.travel_type ==
                            travel_expense_account.national_travel)
                       & (travel_expense.spending_destiny ==
                            travel_expense_account.spending_destiny
                          )
                       & (travel_expense_account.kind_expense == 'advance')
                       ))
            cursor.execute(*query)

            conf_entry_note_type = None
            conf_payment_method = None

            for row in cursor.fetchall():
                if row:
                    conf_entry_note_type =  EntryNoteType(row[0])
                    conf_payment_method = SiimPaymentmethods(row[1])
                break


            payment_method = PaymentMethods()
            payment_method.payment_methods = conf_payment_method
            payment_method.amount = travel_expense.amount_return

            new_entry_note.type = conf_entry_note_type
            new_entry_note.party = travel_expense.employee.party
            new_entry_note.concept = (
                f"Devolución por gastos de viaje por el valor de : "
                f"${str(travel_expense.amount_return)}")
            new_entry_note.payment_methods = [payment_method]
            new_entry_note.origin = travel_expense
            travel_expense.entry_note = new_entry_note

            entry_notes_to_save.append(new_entry_note)
            travel_expenses_to_save.append(travel_expense)

        if travel_expenses_to_save:
            TravelExpenses.save(travel_expenses_to_save)
        if entry_notes_to_save:
            EntryNote.save(entry_notes_to_save)

        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, expenses):
        pool = Pool()
        EntryNote = pool.get('treasury.entry.note')
        EntryNoteType = pool.get('treasury.entry.note.type')
        PaymentMethods = pool.get('treasury.entry.note.payment.methods')
        SiimPaymentmethods = pool.get('siim.account.recaudacion.forma.pago')
        TravelExpenses = pool.get('company.travel.expense')
        TravelExpenseAccount = pool.get('travel.expense.account')

        travel_expense_account = TravelExpenseAccount.__table__()
        cursor = Transaction().connection.cursor()

        entry_notes_to_save = []
        travel_expenses_to_save = []

        for travel_expense in expenses:
            pool = Pool()
            PaymentRequestTravelExpense = pool.get(
                'treasury.account.payment.request.travel.expense')
            if travel_expense.state == 'request':
                results = PaymentRequestTravelExpense.search([
                    ('expense', '=', travel_expense),
                ])
                for result in results:
                    if result.request.state != 'cancel':
                        cls.raise_user_error('payment_request_exists')
            if (travel_expense.state == 'authorize' and
                    travel_expense.character == 'planned'):
                new_entry_note = None
                if travel_expense.amount_return <= 0:
                    continue
                if travel_expense.entry_note:
                    new_entry_note = travel_expense.entry_note
                    PaymentMethods.delete(
                        travel_expense.entry_note.payment_methods)
                    EntryNote(travel_expense.entry_note)
                else:
                    new_entry_note = EntryNote()

                query = travel_expense_account.select(
                    travel_expense_account.entry_note_type.as_('entry_note_type'), #noqa
                    travel_expense_account.payment_method.as_('payment_method'),
                    where=((travel_expense.travel_type ==
                                travel_expense_account.national_travel)
                           & (travel_expense.spending_destiny ==
                                travel_expense_account.spending_destiny
                              )
                           & (travel_expense_account.kind_expense == 'advance')
                           ))
                cursor.execute(*query)

                conf_entry_note_type = None
                conf_payment_method = None

                for row in cursor.fetchall():
                    if row:
                        conf_entry_note_type = EntryNoteType(row[0])
                        conf_payment_method = SiimPaymentmethods(row[1])
                    break

                payment_method = PaymentMethods()
                payment_method.payment_methods = conf_payment_method
                payment_method.amount = travel_expense.amount_return

                if conf_entry_note_type is None:
                    cls.raise_user_error('empty_entry_note_type')
                new_entry_note.type = conf_entry_note_type

                new_entry_note.party = travel_expense.employee.party
                new_entry_note.concept = (
                    f"Devolución por gastos de viaje por el valor de : "
                    f"${str(travel_expense.amount_return)}")
                new_entry_note.payment_methods = [payment_method]
                new_entry_note.origin = travel_expense
                travel_expense.entry_note = new_entry_note

                entry_notes_to_save.append(new_entry_note)
                travel_expenses_to_save.append(travel_expense)

        if travel_expenses_to_save:
            TravelExpenses.save(travel_expenses_to_save)
        if entry_notes_to_save:
            EntryNote.save(entry_notes_to_save)


class TravelExpenseAccount(metaclass=PoolMeta):
    'Company - Employee - Travel - Expense'
    __name__ = 'travel.expense.account'

    entry_note_type = fields.Many2One('treasury.entry.note.type',
        'Tipo de Nota de Ingreso',
        states={
          'required': Eval('kind_expense') == 'advance',
          'invisible': Eval('kind_expense') != 'advance'},
        domain=[])
    payment_method = fields.Many2One('siim.account.recaudacion.forma.pago',
        'Forma de Pago',
        states={
          'required': Eval('kind_expense') == 'advance',
          'invisible': Eval('kind_expense') != 'advance'},
        domain=[])


class EmployeeLoan(metaclass=PoolMeta):
    __name__ = 'company.employee.loan'

    payment_request_line_detail = fields.Function(fields.Many2One(
        'treasury.account.payment.request.line.detail',
        'Detalle solicitud de pago',
        states={
            'invisible': ~Bool(Eval('state').in_(['request', 'open', 'paid']))
        }, depends=['state']),
        'get_payment_request_line_detail')

    @classmethod
    def __setup__(cls):
        super(EmployeeLoan, cls).__setup__()
        cls._error_messages.update({
            'used_on_payment_request': ('No puede cancelar este registro '
                'debido a que ya ha sido asociado a la solicitud de pago '
                'número %(payment_request_number)s [%(payment_request_state)s] '
                'con transferencia en estado %(payment_request_line_state)s.'),
        })

    @classmethod
    def cancel(cls, loans):
        for loan in loans:
            if loan.payment_request_line_detail:
                payment_request_line_detail = loan.payment_request_line_detail
                payment_request = payment_request_line_detail.request
                cls.raise_user_error('used_on_payment_request', {
                    'payment_request_number': payment_request.number,
                    'payment_request_state':
                        payment_request.state_translated.upper(),
                    'payment_request_line_state':
                        payment_request_line_detail.state_translated.upper(),
                })
        super(EmployeeLoan, cls).cancel(loans)

    @classmethod
    def get_payment_request_line_detail(cls, loans, names=None):
        result = defaultdict(lambda: None)
        res1 = cls.get_data_for_employee_loan()
        res2 = cls.get_data_for_payslip_advance()
        result.update(res1)
        result.update(res2)
        return {
            'payment_request_line_detail': result
        }

    @classmethod
    def get_data_for_employee_loan(cls):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PaymentRequest = pool.get('treasury.account.payment.request')
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')

        tbl_payment_request = PaymentRequest.__table__()
        tbl_payment_request_line_detail = PaymentRequestLineDetail.__table__()

        result = defaultdict(lambda: None)

        # Query for company.employee.loan payment request type
        query = tbl_payment_request_line_detail.join(tbl_payment_request,
                condition=(tbl_payment_request.id ==
                    tbl_payment_request_line_detail.request)
        ).select(
            tbl_payment_request_line_detail.id,
            tbl_payment_request_line_detail.origin,
            where=((tbl_payment_request_line_detail.origin.like(
                'company.employee.loan,%')) &
                   ~(tbl_payment_request.state.in_(['draft', 'cancel'])))
        )

        cursor.execute(*query)
        for row in cursor_dict(cursor):
            origin = row['origin']
            if origin:
                try:
                    request_id = int(row['id'])
                    loan_id = int(origin.split(',')[1])
                    result[loan_id] = request_id
                except:
                    pass
        return result

    @classmethod
    def get_data_for_payslip_advance(cls):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        PayslipAdvance = pool.get('payslip.advance')
        PaymentRequest = pool.get('treasury.account.payment.request')
        PaymentRequestLineDetail = pool.get(
            'treasury.account.payment.request.line.detail')

        tbl_payslip_advance = PayslipAdvance.__table__()
        tbl_payment_request = PaymentRequest.__table__()
        tbl_payment_request_line_detail = PaymentRequestLineDetail.__table__()

        result = defaultdict(lambda: None)
        advance_data = defaultdict(lambda: {})

        # Query for payslip.advance payment request type
        query = tbl_payment_request_line_detail.join(tbl_payment_request,
                condition=(tbl_payment_request.id ==
                    tbl_payment_request_line_detail.request)
        ).select(
            tbl_payment_request_line_detail.id,
            tbl_payment_request_line_detail.origin,
            where=((tbl_payment_request_line_detail.origin.like(
                'payslip.advance,%')) &
                   ~(tbl_payment_request.state.in_(['draft', 'cancel'])))
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            origin = row['origin']
            if origin:
                try:
                    request_id = int(row['id'])
                    advance_id = int(origin.split(',')[1])
                    advance_data.update({advance_id: request_id})
                except:
                    pass

        # Find loans on payslip advance
        advance_ids = list(advance_data.keys())
        if advance_ids:
            query = tbl_payslip_advance.select(
                tbl_payslip_advance.id,
                tbl_payslip_advance.loan,
                where=(tbl_payslip_advance.id.in_(advance_ids))
            )
            cursor.execute(*query)
            for row in cursor_dict(cursor):
                advance_id = row['id']
                loan_id = row['loan']
                request_id = advance_data.get(advance_id, None)
                if request_id and loan_id:
                    result[loan_id] = request_id
        return result
