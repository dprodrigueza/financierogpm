from collections import defaultdict

from trytond.model import fields, ModelView, Workflow
from trytond.pyson import Eval, Bool
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids, cursor_dict

__all__ = ['ContractLiquidation']

class ContractLiquidation(metaclass=PoolMeta):
    'Liquidation'
    __name__ = 'contract.liquidation'

    party = fields.Function(fields.Many2One('party.party', 'Tercero',
        states={
            'invisible': True,
        }), 'get_party', searcher='search_party')

    @classmethod
    def get_party(cls, liquidations, name):
        cursor = Transaction().connection.cursor()
        ContractLiquidation = Pool().get('contract.liquidation')
        Contract = Pool().get('company.contract')
        Employee = Pool().get('company.employee')
        Party = Pool().get('party.party')
        contract_liquidation = ContractLiquidation.__table__()
        contract = Contract.__table__()
        employee = Employee.__table__()
        party = Party.__table__()

        result = {}

        for sub_ids in grouped_slice(liquidations):
            red_sql = reduce_ids(contract_liquidation.id, sub_ids)
            query = contract_liquidation.join(contract,
                    condition=(contract_liquidation.contract == contract.id)
                ).join(employee,
                    condition=(contract.employee == employee.id)
                ).join(party,
                    condition=(employee.party == party.id)
                ).select(
                    contract_liquidation.id,
                    party.id,
                where=red_sql
                )
            cursor.execute(*query)
            result.update(dict(cursor.fetchall()))
        return result

    @classmethod
    def search_party(cls, name, clause):
        return [('contract.employee.party.id',) + tuple(clause[1:])]