from collections import defaultdict
from decimal import Decimal
from datetime import datetime

from sql import Literal, Window
from sql.operators import Concat
from sql.functions import CurrentTimestamp, RowNumber

from trytond.model import ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import If, Bool, Eval

__all__ = [
    'Invoice', 'InvoiceTracking'
]


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    petty_cash_invoice = fields.Boolean('Factura Caja chica',
        states={
            'invisible': ~(Eval('petty_cash_invoice_receiver').in_(
                ['company']) | Bool(Eval('petty_cash_invoice'))),
            'readonly': Eval('state') != 'draft',
        }, depends=['state'])

    petty_cash_registry = fields.Many2One('treasury.petty.cash.registry',
        'Caja chica',
        states={
            'invisible': ~Bool(Eval('petty_cash_invoice')),
            'readonly': Eval('state') != 'draft',
            'required': Bool(Eval('petty_cash_invoice'))
        },
        domain=[
            ('company', '=', Eval('company', -1)),
            ('petty_cash_invoice_receiver', '=', 'company'),
            If(Eval('state') != 'draft',
                ('id', '=', Eval('petty_cash_registry')),
                ('state', '=', 'open'),
               )
        ], depends=['petty_cash_invoice', 'company', 'state'])
    petty_cash_invoice_receiver = fields.Function(
        fields.Selection([
            (None, ''),
            ('custodian', 'Custodio'),
            ('company', 'Empresa')
        ], 'Receptor de facturas', readonly=True,
            states={
                'invisible': True
            }
        ), 'get_petty_cash_invoice_receiver')

    @staticmethod
    def default_petty_cash_invoice_receiver():
        pool = Pool()
        Config = pool.get('treasury.petty.cash.configuration')
        config = Config(1)
        invoice_receiver = config.get_multivalue('invoice_receiver')
        return invoice_receiver

    def get_petty_cash_invoice_receiver(self, name):
        pool = Pool()
        Config = pool.get('treasury.petty.cash.configuration')
        config = Config(1)
        invoice_receiver = config.get_multivalue('invoice_receiver')
        return invoice_receiver

    @classmethod
    def view_attributes(cls):
        return super(Invoice, cls).view_attributes() + [
            ('//page[@id="petty_cash_page"]', 'states', {
                'invisible': ~(Eval('petty_cash_invoice_receiver').in_(
                    ['company']) | Bool(Eval('petty_cash_invoice'))),
                    })]

    @fields.depends('party', 'petty_cash_invoice')
    def on_change_with_retention_party(self, name=None):
        if self.petty_cash_invoice:
            return None
        return super(Invoice, self).on_change_with_retention_party(name)


class InvoiceTracking(ModelView, ModelSQL):
    'Invoice Tracking'
    __name__ = 'account.invoice.tracking'

    invoice = fields.Many2One(
        'account.invoice', 'Numero Fac.', help='Número de factura')
    invoice_date = fields.Date(
        'Fecha Fac.', help='Fecha de factura'
    )
    invoice_number = fields.Char(
        'Referencia Fac.', help='Referencia de la factura'
    )
    invoice_party = fields.Many2One(
        'party.party', 'Tercero Fac.', help='Tercero de la factura'
    )
    invoice_state = fields.Selection(
        [
            (None, ''),
            ('draft', 'Borrador'),
            ('validated', 'Validado'),
            ('posted', 'Contabilizado'),
            ('paid', 'Pagado'),
            ('cancel', 'Cancelado'),
        ], 'Estado Fac.', help='Estado de la factura'
    )
    retention = fields.Function(
        fields.Many2One('account.tax.retention', 'Retencion'),
        'on_change_with_retention', searcher='search_data')
    retention_state = fields.Function(fields.Selection(
        [
            (None, ''),
            ('draft', 'Borrador'),
            ('done', 'Realizado'),
            ('cancel', 'Cancelado')
        ], 'Estado Ret.', help='Estado de la retención'),
        'on_change_with_retention_state', searcher='search_data')
    retention_authorization_date = fields.Function(fields.Date(
        'Fecha Aut.', help='Fecha de autorización de la retención'),
        'on_change_with_retention_authorization_date', searcher='search_data'
    )
    retention_authorization_state = fields.Function(fields.Selection(
        [
            (None, ''),
            ('PPR', 'Procesando'),
            ('AUT', 'Autorizado'),
            ('NAT', 'No Autorizado')
        ],
        'Autorizacion Ret.', help='Autorización de la retención'),
        'on_change_with_retention_authorization_state', searcher='search_data'
    )
    move = fields.Function(fields.Many2One(
        'account.move', 'Asiento contable'),
        'on_change_with_move'
    )
    move_state = fields.Function(fields.Selection(
            [
                (None, ''),
                ('draft', 'Borrador'),
                ('invalid', 'Invalida'),
                ('posted', 'Contabilizado'),
            ], 'Estado Con.', help='Estado de contabilización'),
            'on_change_with_move_state', searcher='search_data'
    )
    move_date = fields.Function(fields.Date(
        'Fecha Con.', help='Fecha de contabilización'),
        'on_change_with_move_date', searcher='search_data'
    )
    payment_amount = fields.Function(fields.Numeric(
        'Monto a pagar', digits=(16,2)),
        'on_change_with_payment_amount'
    )
    deduction = fields.Function(fields.Numeric(
        'Deduccion', digits=(16,2)),
        'on_change_with_deduction')
    to_paid = fields.Function(fields.Numeric(
        'Pendiente por pagar', digits=(16,2)),
        'on_change_with_to_paid'
    )
    request = fields.Function(fields.Many2One(
        'treasury.account.payment.request', 'Solicitud',
        help='Solicitud de pago'),
        'on_change_with_request', searcher='search_data'
    )
    request_state = fields.Function(fields.Selection(
        [
            (None, ''),
            ('draft', 'Borrador'),
            ('approved', 'Aprobado'),
            ('processing', 'Procesando'),
            ('denied', 'Revisión'),
            ('cancel', 'Cancelado'),
        ], 'Estado Sol.', help='Estado de solicitud de pago'),
        'on_change_with_request_state', searcher='search_data'
    )
    request_date = fields.Function(fields.Date(
        'Fecha Sol.', help='Fecha de solicitud de pago'),
        'on_change_with_request_date', searcher='search_data'
    )
    payment = fields.Function(fields.Many2One(
        'treasury.account.payment.spi', 'Pago'),
        'on_change_with_payment', searcher='search_data'
    )
    payment_state = fields.Function(fields.Selection(
        [
            (None, ''),
            ('draft', 'Borrador'),
            ('confirmed', 'Confirmado'),
            ('generated', 'Generado'),
            ('paid', 'Pagado'),
            ('partially_paid', 'Pagado parcialmente'),
            ('cancel', 'Cancelado'),
        ], 'Estado Pago'),
        'on_change_with_payment_state', searcher='search_data'
    )
    payment_date = fields.Function(fields.Date(
        'Fecha Pago'), 'on_change_with_payment_date', searcher='search_data')

    @classmethod
    def __setup__(cls):
        super(InvoiceTracking, cls).__setup__()
        cls._order = [
            ('invoice_number', 'ASC'),
            ('invoice_date', 'DESC'),
            ('invoice_party', 'ASC'),
        ]
        # cls.order_retention = cls._order_field_model(
        #     'account.tax.retention', 'number.number')
        # cls.order_move_line_state = cls._order_field_model(
        #     'account.move.line', 'move.state')
        # cls.order_move_line_account = cls._order_field_model(
        #     'account.move.line', 'account.name')
        # cls.order_request_number = cls._order_field_model(
        #     'treasury.account.payment.request.line.detail', 'request.number')
        # cls.order_request_state = cls._order_field_model(
        #     'treasury.account.payment.request.line.detail', 'request.state')
        # cls.order_request_party = cls._order_field_model(
        #     'treasury.account.payment.request.line.detail', 'party.name')

    @classmethod
    def _order_field_model(cls, model, name):
        def order_field(tables):
            Model = Pool().get(model)
            tbl_model = Model.__table__()
            fname, _, oexpr = name.partition('.')
            field = Model._fields[fname]
            table, _ = tables[None]
            model_tables = tables.get('move_line')
            if model_tables is None:
                model_tables = {
                    None: ((tbl_model, tbl_model.id == table.move_line)
                           if 'move.line' in model else
                           (tbl_model, tbl_model.id == table.request_detail) )
                }
                if 'move.line' in model:
                    tables['move_line'] = model_tables
                if 'request.line' in model:
                    tables['request_detail'] = model_tables
            return field.convert_order(name, model_tables, Model)
        return staticmethod(order_field)

    @fields.depends('id', 'invoice_party')
    def on_change_with_move(self, name=None):
        Move = Pool().get('account.move')
        MoveLine = Pool().get('account.move.line')
        move_lines = MoveLine.search_read([
            ('origin_', '=', f"account.invoice,{self.id}"),
            ('credit', '>', Decimal("0")),
            ('party', '=', self.invoice_party),
        ], fields_names=['move'])
        if move_lines:
            ids = [l['move'] for l in move_lines]
            moves = Move.search_read([
                ('id', 'in', ids),
            ], order=[('post_date', 'DESC NULLS LAST')], fields_names=['id'])
            if moves:
                return moves[0]['id']
        return None

    @classmethod
    def search_data(cls, name, clause):
        domain = tuple(clause[1:])
        pool = Pool()
        ids = []
        if 'retention' in name:
            _field = name[10:]
            if not _field:
                _field = 'number'
            if 'date' in _field:
                domain = (
                    domain[:1][0] ,
                    (datetime(domain[1:][0].year, domain[1:][0].month,
                              domain[1:][0].day)))
            Retention = pool.get('account.tax.retention')
            retentions = Retention.search_read([
                ((_field,) + domain)
            ], fields_names=['invoice'])
            ids = [r['invoice'] for r in retentions]
        if 'move' in name:
            _field = name[5:]
            if not _field:
                _field = 'number'
            if 'date' in _field:
                _field = 'post_date'
            Move = pool.get('account.move')
            moves = Move.search_read([
                ((_field,) + domain),
                ('origin', 'ilike', '%account.invoice,%')
            ], fields_names=['origin'])
            ids = [r['origin'][16:] for r in moves]
        if 'request' in name:
            _field = name[8:]
            if not _field:
                _field = 'number'
            if 'date' in _field:
                _field = 'request_date'
            Request = pool.get('treasury.account.payment.request')
            LineDetail = pool.get('treasury.account.payment.request.line.detail')
            Move = pool.get('account.move')
            requests = Request.search_read([
                ((_field,) + domain)
            ], fields_names=['id'])
            request_ids = [r['id'] for r in requests]
            line_details = LineDetail.search_read([
                ('request', 'in', request_ids),
                ('origin', 'ilike', '%account.move.line,%')
            ], fields_names=['origin'])
            move_lines_ids = [l['origin'][18:] for l in line_details]
            moves = Move.search_read([
                ('lines', 'in', move_lines_ids),
                ('origin', 'ilike', '%account.invoice,%')
            ], fields_names=['origin'])
            ids = [r['origin'][16:] for r in moves]
        if 'payment' in name:
            _field = name[8:]
            if not _field:
                _field = 'number'
            if 'date' in _field:
                _field = 'payment_date'
                domain = (
                    domain[:1][0] ,
                    (datetime(domain[1:][0].year, domain[1:][0].month,
                              domain[1:][0].day)))
            Payment = pool.get('treasury.account.payment.spi')
            LineDetail = pool.get('treasury.account.payment.request.line.detail')
            Move = pool.get('account.move')
            payments = Payment.search_read([
                ((_field,) + domain)
            ], fields_names=['id'])
            payment_ids = [r['id'] for r in payments]
            line_details = LineDetail.search_read([
                ('spi', 'in', payment_ids),
                ('origin', 'ilike', '%account.move.line,%')
            ], fields_names=['origin'])
            move_lines_ids = [l['origin'][18:] for l in line_details]
            moves = Move.search_read([
                ('lines', 'in', move_lines_ids),
                ('origin', 'ilike', '%account.invoice,%')
            ], fields_names=['origin'])
            ids = [r['origin'][16:] for r in moves]
        return [('id', 'in', ids)]

    @fields.depends('id')
    def on_change_with_retention(self, name=None):
        Retention = Pool().get('account.tax.retention')
        retentions = Retention.search_read([
            ('invoice', '=', self.id),
            ('state', '!=', 'cancel'),
        ], order=[('issue_date', 'DESC')], fields_names=['id'])
        if retentions:
            return retentions[0]['id']
        return None

    @fields.depends('retention')
    def on_change_with_retention_state(self, name=None):
        if self.retention:
            return self.retention.state
        return None

    @fields.depends('retention')
    def on_change_with_retention_authorization_state(self, name=None):
        if self.retention:
            return self.retention.authorization_state
        return None

    @fields.depends('retention')
    def on_change_with_retention_authorization_date(self, name=None):
        if self.retention and self.retention.authorization_date:
            return self.retention.authorization_date.date()
        return None

    @fields.depends('move')
    def on_change_with_move_state(self, name=None):
        if self.move:
            return self.move.state
        return None

    @fields.depends('move')
    def on_change_with_move_date(self, name=None):
        if self.move:
            return self.move.post_date
        return None

    @fields.depends('id')
    def on_change_with_to_paid(self, name=None):
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(self.id)
        return invoice.amount_to_pay

    @fields.depends('id')
    def on_change_with_payment_amount(self, name=None):
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(self.id)
        return invoice.payable_amount

    @fields.depends('id')
    def on_change_with_deduction(self, name=None):
        Invoice = Pool().get('account.invoice')
        invoice = Invoice(self.id)
        deduction = Decimal("0.00")
        for row in invoice.payment_lines:
            deduction += (row.debit - row.credit).quantize(Decimal("0.01"))
        return deduction

    @fields.depends('move', 'id')
    def on_change_with_request(self, name=None):
        if self.move:
            ids = [f"account.move.line,{m.id}" for m in self.move.lines]
            pool = Pool()
            Payment = pool.get('purchase.contract.process.payment')
            Request = pool.get('treasury.account.payment.request')
            LineDetail =pool.get('treasury.account.payment.request.line.detail')
            line_details = LineDetail.search_read([
                ('origin', 'in', ids),
                ('request', '!=', None),
            ], fields_names=['request'])
            if line_details:
                requests = Request.search_read([
                    ('id', 'in', [x['request'] for x in line_details]),
                    ('state', '!=', 'cancel'),
                ], order=[('request_date', 'DESC')], fields_names=['id'])
                if requests:
                    return requests[0]['id']
            else:
                payments = Payment.search_read([
                    ('origin', '=', f"account.invoice,{self.id}"),
                    ('state', '=', 'valid')
                ], fields_names=['id'])
                if payments:
                    ids = [f"purchase.contract.process.payment,{m['id']}" for m in payments]
                    line_details = LineDetail.search_read([
                        ('origin', 'in', ids),
                        ('request', '!=', None),
                    ], fields_names=['request'])
                    if line_details:
                        requests = Request.search_read([
                            ('id', 'in', [x['request'] for x in line_details]),
                            ('state', '!=', 'cancel'),
                        ], order=[('request_date', 'DESC')], fields_names=['id'])
                        if requests:
                            return requests[0]['id']
        return None

    @fields.depends('request')
    def on_change_with_request_state(self, name=None):
        if self.request:
            return self.request.state
        return None

    @fields.depends('request')
    def on_change_with_request_date(self, name=None):
        if self.request:
            return self.request.request_date
        return None

    @fields.depends('request')
    def on_change_with_payment(self, name=None):
        if self.request and self.request.spi:
            return self.request.spi.id
        return None

    @fields.depends('payment')
    def on_change_with_payment_state(self, name=None):
        if self.payment:
            return self.payment.state
        return None

    @fields.depends('payment')
    def on_change_with_payment_date(self, name=None):
        if self.payment and self.payment.payment_date:
            return self.payment.payment_date.date()
        return None

    @classmethod
    def table_query(cls):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        invoice = Invoice.__table__ ()
        query = invoice.select(
            invoice.id,
            invoice.id.as_('invoice'),
            invoice.invoice_date,
            invoice.number.as_('invoice_number'),
            invoice.state.as_('invoice_state'),
            invoice.party.as_('invoice_party'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            where=(invoice.type=='in') & (~invoice.state.in_(['draft', 'cancel']))
        )
        return query
