from sql.aggregate import Sum
from sql.operators import Concat
from sql import Null
from decimal import Decimal

from trytond.modules.product import price_digits
from trytond.model import ModelView, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.wizard import (Wizard, StateView, StateTransition,
    Button)

from trytond.modules.account_invoice import InvoiceLine as InvLine

__all__ = ['Invoice', 'InvoiceLine', 'CreateInvoiceStart', 'CreateInvoice']


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._error_messages.update({
            'invoice_wrong_qty': ('Subtotal incorrecto en la factura '
                '%(invoice)s con respecto a los ingresos a bodega" '
                'en los productos:\n%(products)s'),
        })
        cls._buttons.update({
            'update_move_price': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state']
            },
        })

    @classmethod
    @ModelView.button
    def update_move_price(cls, invoices):
        to_update = []
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        Move = pool.get('stock.move')
        configuration = Configuration(1)
        for invoice in invoices:
            if invoice.lines:
                for line in invoice.lines:
                    if line.origin and isinstance(line.origin, Move):
                        if configuration.unit_price_with_tax:
                            Tax = pool.get('account.tax')
                            taxes = Tax.compute(
                                line.taxes, line.unit_price, 1)
                            if taxes:
                                total = 0
                                for tax in taxes:
                                    total += tax['amount'] + tax['base']
                                price = Decimal(total /
                                    Decimal(1)).quantize(
                                    Decimal(10) ** -price_digits[1])
                                line.origin.base_unit_price = round(
                                    line.unit_price, 4)
                                line.origin.unit_price = price
                                line.origin.tax_rate = line.taxes[0].rate
                                to_update.append(line.origin)
                            else:
                                line.origin.base_unit_price = line.unit_price
                                line.origin.unit_price = line.unit_price
                                to_update.append(line.origin)
        Move.save(to_update)

    @classmethod
    def validate_invoice(cls, invoices):
        super(Invoice, cls).validate_invoice(invoices)
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Move = pool.get('stock.move')
        Line = pool.get('account.invoice.line')
        move = Move.__table__()
        invoice_ = cls.__table__()
        line = Line.__table__()
        Product = pool.get('product.product')
        Configuration = pool.get('purchase.configuration')
        config = Configuration(1)
        if config.unit_price_with_tax:
            price_to_use = move.base_unit_price
        else:
            price_to_use = move.unit_price
        for invoice in invoices:

            query_sum_invoice = line.join(invoice_,
                condition=invoice_.id == line.invoice
            ).select(
                line.id,
                line.origin,
                line.product,
                Sum(line.quantity).as_('invoice_quantity'),
                Sum((line.quantity * line.unit_price)).as_('subtotal'),
                where=(invoice_.id == invoice.id) & (
                    line.origin.like('stock.move,%')),
                group_by=[line.id, line.origin, line.product])

            query_sum_stock = line.join(invoice_,
                condition=invoice_.id == line.invoice
            ).join(move,
                condition=(Concat('stock.move,', move.id) == line.origin)
            ).select(
                line.id,
                line.product,
                Sum(move.quantity).as_('stock_quantity'),
                Sum(move.quantity * price_to_use).as_('subtotal'),
                line.origin,
                where=(invoice_.id == invoice.id) & (
                    line.origin.like('stock.move,%')),
                group_by=[line.id, line.product])

            query = query_sum_invoice.join(query_sum_stock,
                condition=(
                    query_sum_invoice.origin == query_sum_stock.origin)
            ).select(
                query_sum_invoice.id,
                query_sum_invoice.product,
                query_sum_invoice.subtotal,
                query_sum_stock.subtotal,
                where=((query_sum_invoice.invoice_quantity !=
                       query_sum_stock.stock_quantity) | (
                    query_sum_invoice.subtotal != query_sum_stock.subtotal) |
                      (query_sum_stock.subtotal == Null))
            )
            cursor.execute(*query)

            product_str = ""
            for line_id, product_id, tot_inv, tot_stock, in cursor.fetchall():
                diff = abs(Decimal(tot_inv) - Decimal(tot_stock))
                if diff >= Decimal('0.01'):
                    line = Line(line_id)
                    product = Product(product_id)
                    product_str += f"({product.code}) {product.name} "
                    product_str += f"Cant: {line.quantity}\n"
                    cls.raise_user_error('invoice_wrong_qty', {
                        'invoice': invoice.rec_name,
                        'products': product_str,
                    })

    @classmethod
    def post(cls, invoices):
        super(Invoice, cls).post(invoices)
        pool = Pool()
        Move = pool.get('stock.move')
        MoveAccount = pool.get('stock.move.account')
        to_save = []
        for invoice in invoices:
            for line in invoice.lines:
                if line.origin and isinstance(line.origin, Move):
                    if line.origin.accounts:
                        MoveAccount.delete(line.origin.accounts)
                    for line_account in line.accounts:
                        move_account = MoveAccount()
                        move_account.move = line.origin
                        move_account.account = line.account
                        move_account.budget = line.budget
                        move_account.quantity = Decimal(line.quantity).quantize(
                            Decimal('0.01'))
                        move_account.amount = line.amount
                        to_save.append(move_account)
        if to_save:
            MoveAccount.save(to_save)


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def delete(cls, items):
        origins = []
        shipments = []
        for item in items:
            if str(item.origin).startswith('stock.move'):
                s_in = str(item.origin.shipment).split(',')[1]
                if s_in not in origins:
                    origins.append(s_in)
        if origins:
            ShipmentIn = Pool().get('stock.shipment.in')
            shipmentIn = ShipmentIn.search(['id', 'in', origins])
            for shipment in shipmentIn:
                shipment.invoice = None
                shipments.append(shipment)
            ShipmentIn.save(shipments)
        super(InvoiceLine, cls).delete(items)

    @staticmethod
    def _account_domain(type_):
        type_aux = InvLine._account_domain(type_)
        return type_aux + ['other']

    @classmethod
    def _get_origin(cls):
        origin = super(InvoiceLine, cls)._get_origin()
        return origin + ['stock.move', 'public.budget.compromise.line']


class CreateInvoiceStart(ModelView):
    'Create Invoice Start'
    __name__ = 'wizard.create.invoice.start'
    invoice = fields.Many2One('account.invoice', 'Invoice', required=True)
    invoice_party = fields.Function(
        fields.Many2One('party.party', 'Party'), 'on_change_with_invoice_party')
    invoice_company = fields.Function(
        fields.Many2One('company.company', 'Company'),
        'on_change_with_invoice_company')
    invoice_compromise = fields.Function(
        fields.Many2One('public.budget.compromise', 'Compromiso'),
        'on_change_with_invoice_compromise')
    type_ = fields.Selection(
        [
            ('purchases', 'Compras'),
            ('shipments', 'Ingresos a bodega'),
            ('compromise', 'Compromiso')
        ], 'Create from', required=True)
    purchases = fields.Many2Many('purchase.purchase', None, None, 'Purchases',
        states={
            'invisible': Eval('type_') != 'purchases',
            'required': Eval('type_') == 'purchases',
        },
        domain=[
            ('party', '=', Eval('invoice_party')),
            ('company', '=', Eval('invoice_company')),
        ], depends=['type_', 'invoice_party', 'invoice_company'])
    shipments = fields.Many2Many('stock.shipment.in', None, None, 'Shipments',
        states={
            'invisible': (Eval('type_') != 'shipments'),
            'required': Eval('type_') == 'shipments',
        },
        domain=[
            ('supplier', '=', Eval('invoice_party')),
            ('company', '=', Eval('invoice_company')),
            ('state', '=', 'done'),
            ('invoice', '=', None),
        ], depends=['type_', 'invoice_party', 'invoice_company', 'invoice'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        states={
            'invisible': Eval('type_') != 'compromise',
            'required': Eval('type_') == 'compromise',
        },
        domain=[
            ('id', '=', Eval('invoice_compromise')),
            ('party', '=', Eval('invoice_party')),
            ('company', '=', Eval('invoice_company')),
            ('state', '=', 'done'),
        ], depends=['type_', 'invoice_party', 'invoice_company'])

    @classmethod
    def default_invoice(cls):
        return Transaction().context['active_id']

    @fields.depends('invoice')
    def on_change_with_invoice_party(self, name=None):
        if self.invoice:
            return self.invoice.party.id
        return None

    @fields.depends('invoice')
    def on_change_with_invoice_company(self, name=None):
        if self.invoice:
            return self.invoice.company.id
        return None

    @fields.depends('invoice')
    def on_change_with_invoice_compromise(self, name=None):
        if self.invoice and self.invoice.compromise:
            return self.invoice.compromise.id
        return None


class CreateInvoice(Wizard):
    'Create Invoice'
    __name__ = 'wizard.create.invoice'

    start = StateView('wizard.create.invoice.start',
    'public_ec_stock.wizard_create_invoice_view_form', [
        Button('Cancel', 'end', 'tryton-cancel'),
        Button('OK', 'handle', 'tryton-ok', default=True),
    ])
    handle = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CreateInvoice, cls).__setup__()
        cls._error_messages.update({
            'without_budget': ('No se puede obtener partida para el  '
                'producto %(product)s. Revise que la factura tenga '
                'asociada una certificación o compromiso, o que la '
                'categoría del producto tenga configuración de partidas'),
            'no_account_for_budget': ('No existe una cuenta contable '
                'relacionada con la partida %(budget)s, debe crear la '
                'cuenta en su catálogo de cuentas.'),
        })

    def from_purchase(self, invoice):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Account = pool.get('account.account')
        AccountLine = pool.get('account.invoice.line.account')
        # LineTax = pool.get('account.invoice.line-account.tax')
        lines = []
        # to_save_taxes = []
        # TODO: Determinar si es necesario diccionario de impuestos.
        for purchase in self.start.purchases:
            invoice.certificate = purchase.certificate
            invoice.save()
            for pline in purchase.lines:
                line = InvoiceLine()
                to_save_accounts = []
                for budget in pline.budgets:
                    account_line = AccountLine(
                        line=line,
                        budget=budget.budget,
                        quantity=budget.quantity,
                        amount=budget.amount
                    )
                    to_save_accounts.append(account_line)
                    line.budget = budget.budget
                    accounts = Account.search([
                        ('budget_debit', 'parent_of', line.budget.id, 'parent'),
                        ('kind', '!=', 'view')
                    ])
                    if accounts and len(accounts) == 1:
                        account_line.account = accounts[0]
                        line.account = accounts[0]
                allowed_cost_centers = line.get_allowed_cost_centers()
                if len(allowed_cost_centers) == 1:
                    line.cost_center = allowed_cost_centers[0].id
                line.on_change_with_cost_center()
                line.invoice = invoice
                line.product = pline.product
                line.quantity = 1
                line.unit = pline.unit
                line.unit_price = pline.unit_price
                line.taxes = pline.taxes
                line.description = pline.description
                line.origin = pline
                line.accounts = to_save_accounts
                lines.append(line)
        return {'lines': lines, 'taxes': []}

    def from_compromise(self, invoice):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Account = pool.get('account.account')
        Tax = pool.get('account.tax')
        lines = []
        for cline in self.start.compromise.lines:
            line = InvoiceLine()
            line.invoice = invoice
            line.product = None
            line.origin = f"public.budget.compromise.line,{cline.id}"
            line.budget = cline.certificate_line.budget
            line.cost_center = line.on_change_with_cost_center()
            accounts = Account.search([
                ('budget_debit', 'where', [
                    ('code', '=', line.budget.parent.code),
                ]),
                ('kind', '!=', 'view')
            ], order=[('code', 'ASC')])
            if accounts:
                line.account = accounts[0]
            line.quantity = 1
            line.unit_price = cline.untaxed_amount
            line.taxes = []
            if cline.taxed_amount:
                # TODO: use tax as config parameter
                taxes = Tax.search([
                    ('name', 'like', '%IVA 12%'),
                ])
                if taxes:
                    line.taxes = [taxes[0]]
            lines.append(line)
        return {'lines': lines, 'taxes': []}

    def from_shipment(self, invoice):
        # TODO: prevalidate compromise
        if invoice.state == 'draft':
            lines = []
            pool = Pool()
            Account = pool.get('account.account')
            Tax = pool.get('account.tax')
            Budget = pool.get('public.budget')
            InvoiceLine = pool.get('account.invoice.line')
            InvoiceLineAccount = pool.get('account.invoice.line.account')
            LineTax = pool.get('account.invoice.line-account.tax')
            Config = pool.get('purchase.configuration')
            config = Config(1)
            to_save_taxes = []
            for shipment in self.start.shipments:
                for sline in shipment.incoming_moves:
                    unit_price = Decimal(
                        sline.base_unit_price if config.unit_price_with_tax
                        else sline.unit_price).quantize(Decimal('0.0001'))
                    line = InvoiceLine()
                    line.invoice = invoice
                    if sline.origin:
                        line.origin = sline.origin
                    else:
                        line.origin = f"stock.move,{sline.id}"
                    line.product = sline.product
                    line.budget = None
                    _accounts = []
                    if sline.origin and (
                        str(sline.origin) == f"purchase.line,{sline.origin.id}"
                    ):
                        for budget in sline.origin.budgets:
                            nAILA = InvoiceLineAccount(
                                line=line,
                                budget=budget.budget,
                                amount=budget.amount,
                                quantity=Decimal(budget.quantity).quantize(
                                    Decimal('0.0001'))
                            )
                            _accounts.append(nAILA)
                        for tax in sline.origin.taxes:
                            nTax = LineTax(
                                line=line,
                                tax=tax
                            )
                            to_save_taxes.append(nTax)
                        invoice.certificate = sline.origin.purchase.certificate
                    else:
                        for account in sline.accounts:
                            nAILA = InvoiceLineAccount(
                                line=line,
                                amount=account.amount,
                                quantity=account.quantity.quantity(
                                    Decimal('0.0001'))
                            )
                            _accounts.append(nAILA)
                        if not sline.accounts:
                            nAILA = InvoiceLineAccount(
                                line=line,
                                amount=(
                                    Decimal(sline.quantity).quantize(
                                        Decimal('0.00')) * unit_price
                                ).quantize(Decimal('0.0001')),
                                quantity=Decimal(sline.quantity
                                        ).quantize(Decimal('.00'))
                            )
                            _accounts.append(nAILA)
                        for tax in sline.taxes:
                            nTax = LineTax(
                                line=line,
                                tax=tax
                            )
                            to_save_taxes.append(nTax)
                    line.accounts = _accounts
                    line.quantity = Decimal(sline.quantity
                                    ).quantize(Decimal('.00'))
                    line.unit = sline.uom
                    line.unit_price = unit_price

                    # TODO: adapt this method to accept multiple budgets
                    if sline.budget:
                        if sline.budget.kind == 'other':
                            line.budget = sline.budget
                        else:
                            budget_object = None
                            if invoice.certificate:
                                budget_object = invoice.certificate
                            if invoice.compromise:
                                budget_object = invoice.compromise
                            if budget_object:
                                domain = [
                                    ('id', 'in', [
                                        b.id for b in budget_object.budgets])
                                ]
                            else:
                                domain = [
                                    ('parent', 'child_of',
                                        sline.product.account_category.budgets,
                                        'parent'),
                                    ('kind', '=', 'other'),
                                    ('activity.start_date', '<=',
                                     sline.effective_date),
                                    ('activity.end_date', '>=',
                                     sline.effective_date),
                                ]
                            budgets = Budget.search(domain)
                            if budgets and line.budget is None:
                                line.budget = budgets[0]
                        if line.budget:
                            line.cost_center = line.on_change_with_cost_center()
                        else:
                            self.raise_user_error('without_budget', {
                                'product': sline.product.rec_name,
                            })
                        accounts = Account.search([
                            ('budget_debit', 'where', [
                                ('parent', 'parent_of', line.budget),
                            ]),
                            ('kind', 'in', ['other', 'expense']),
                        ], order=[('code', 'ASC')])
                        if accounts:
                            line.account = accounts[0]
                        else:
                            self.raise_user_error('no_account_for_budget', {
                                'budget': line.budget.rec_name,
                            })

                    if (line.product.account_category and
                            line.product.account_category.supplier_taxes):
                        line.taxes = (
                            line.product.account_category.supplier_taxes)
                    line.quantity = Decimal(sline.quantity
                                    ).quantize(Decimal('.00'))
                    line.unit = sline.uom
                    if sline.tax_rate:
                        line.unit_price = unit_price
                        taxes = Tax.search([
                            ('rate', '=', sline.tax_rate),
                            ('group', '=', 'IVA'),
                        ])
                        if taxes:
                            line.taxes = taxes
                    else:
                        line.unit_price = unit_price
                    lines.append(line)
                if shipment.certificate:
                    invoice.certificate = shipment.certificate
                    invoice.save()
            shipment.invoice = invoice
            shipment.save()
        return {'lines': lines, 'taxes': to_save_taxes}

    def transition_handle(self):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        LineTax = pool.get('account.invoice.line-account.tax')
        invoice = self.start.invoice
        lines = []
        if self.start.type_ == 'purchases':
            lines = self.from_purchase(invoice)
        elif self.start.type_ == 'compromise':
            lines = self.from_compromise(invoice)
        elif self.start.type_ == 'shipments':
            lines = self.from_shipment(invoice)

        InvoiceLine.save(lines['lines'])
        LineTax.save(lines['taxes'])
        invoice.on_change_taxes()
        invoice.save()
        return 'end'
