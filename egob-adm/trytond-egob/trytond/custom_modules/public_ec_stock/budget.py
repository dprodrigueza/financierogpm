from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['PublicBudget']


class PublicBudget(metaclass=PoolMeta):
    __name__ = 'public.budget'

    @classmethod
    def search_from_stock_move(cls, context):
        pool = Pool()
        Date = pool.get('ir.date')
        POA = pool.get('public.planning.unit')
        Product = pool.get('product.product')
        # Product = pool.get('product.product')
        Certificate = pool.get('public.budget.certificate')
        new_domain = []
        poa = None
        if context.get('effective_date'):
            date = context['effective_date']
        else:
            date = Date.today()

        poa, = POA.search_read([
            ('start_date', '<=', date),
            ('end_date', '>=', date),
            ('type', '=', 'poa')
        ], fields_names=['id'])

        if context.get('product'):
            if context.get('certificate') is None:
                product = Product(context['product'])
                if product.account_category.budgets:
                    new_domain += [
                        ('id', 'in', [
                            b.id for b in product.account_category.budgets]),
                    ]
        if context.get('certificate'):
            certificate = Certificate(context['certificate'])
            new_domain += [
                ('id', 'in', [b.id for b in certificate.budgets])
            ]
        return new_domain

    @classmethod
    def search(cls, domain, offset=0, limit=None, order=None, count=False,
            query=False):
        context = Transaction().context
        if context.get('stock_move'):
            new_domain = cls.search_from_stock_move(context)
            if new_domain:
                domain += new_domain

        return super(PublicBudget, cls).search(domain, offset=offset,
            limit=limit, order=order, count=count, query=query)
