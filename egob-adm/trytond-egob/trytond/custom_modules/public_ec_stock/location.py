from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta

__all__ = ['Location', 'LocationDomain']


class Location(metaclass=PoolMeta):
    __name__ = "stock.location"

    from_domain = fields.Many2Many(
        'stock.location-domain-stock.location', 'from_location', 'to_location',
        'Restricciones de', states={
            'invisible': True
        })
    to_domain = fields.Many2Many(
        'stock.location-domain-stock.location', 'to_location', 'from_location',
        'Restricciones para', states={
            'invisible': True
        })
    is_consumable = fields.Boolean('De consumo')


class LocationDomain(ModelSQL):
    "Stock Location Domain"
    __name__ = 'stock.location-domain-stock.location'

    from_location = fields.Many2One(
        'stock.location', 'From Location', required=True)
    to_location = fields.Many2One(
        'stock.location', 'To Location', required=True)
