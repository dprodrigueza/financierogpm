from trytond.model import fields
from trytond.pyson import Bool, Eval, If
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta

__all__ = ['Purchase', 'PurchaseLine']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    poa = fields.Many2One('public.planning.unit', 'POA',
        domain=[
            ('type', '=', 'poa'),
            ('company', '=', Eval('company'))
        ], depends=['company'], required=True)
    certificate = fields.Many2One('public.budget.certificate', 'Certificado',
        states={
            'readonly': (~Eval('state').in_(['draft']) | ~Bool(Eval('poa'))),
            'required': ~Eval('state').in_(['draft']),
        },
        domain=[
            ('poa', '=', Eval('poa')),
            ('state', '=', 'done'),
        ], depends=['poa', 'state'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromise',
        states={
            'readonly': (~Eval('state').in_(['draft']) | ~Bool(Eval('poa'))),
        },
        domain=[
            ('certificate', '=', Eval('certificate')),
            ('state', '=', 'done'),
            ('party', '=', Eval('party')),
            If(Bool(Eval('compromise')),
                [
                    'OR',
                    # TODO: add available_without_purchase function field
                    # on PublicBudgetCompromise model
                    # ('available_without_purchase', '>', Eval('total_amount')),
                    ('id', '=', Eval('compromise'))],
                [])
        ], depends=['certificate', 'party', 'state'])

    def _get_invoice_purchase(self):
        invoice = super(Purchase, self)._get_invoice_purchase()
        invoice.poa = self.poa
        invoice.certificate = self.certificate
        if self.compromise:
            invoice.compromise = self.compromise
        return invoice


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    budget = fields.Many2One('public.budget', 'Budget',
        states={
            'readonly': Eval('purchase_state').in_(
                ['confirmed', 'processing', 'done', 'cancel']),
            'required': Eval('purchase_state').in_(
                ['confirmed', 'processing', 'done', 'cancel'])
        },
        domain=[
            ('id', 'in', Eval('budgets_allowed')),
        ], depends=['purchase_state', 'budgets_allowed'],
    )
    budgets_allowed = fields.Function(
        fields.Many2Many('public.budget', None, None, 'Budgets allowed',
            states={
            }), 'on_change_with_budgets_allowed')
    account = fields.Many2One('account.account', 'Account',
        states={
            'readonly': Eval('purchase_state').in_(
                ['confirmed', 'processing', 'done', 'cancel']) | ~Bool(
                    Eval('budget')),
            'required': Eval('purchase_state').in_(
                ['confirmed', 'processing', 'done', 'cancel'])
        },
        domain=[
            ('kind', 'not in', ['view', 'payable']),
            ('budget_debit', 'parent_of', [Eval('budget')], 'parent')
        ], depends=['budget']
    )

    @fields.depends('account', 'purchase', '_parent_purchase.compromise',
        '_parent_purchase.certificate')
    def on_change_with_budgets_allowed(self, name=None):
        budgets = []
        if self.purchase:
            if self.purchase.compromise:
                for b in self.purchase.compromise.budgets:
                    budgets.append(b.id)
            elif self.purchase.certificate:
                for b in self.purchase.certificate.budgets:
                    budgets.append(b.id)
        return budgets

    def get_invoice_line(self):
        'Return a list of invoice line for purchase line'
        # Override method dont call super function
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')

        invoice_line = InvoiceLine()
        invoice_line.type = self.type
        invoice_line.description = self.description
        invoice_line.note = self.note
        invoice_line.origin = self
        if self.type != 'line':
            if self._get_invoice_not_line():
                return [invoice_line]
            else:
                return []

        quantity = (self._get_invoice_line_quantity()
            - self._get_invoiced_quantity())

        if self.unit:
            quantity = self.unit.round(quantity)
        invoice_line.quantity = quantity

        if not invoice_line.quantity:
            return []

        invoice_line.unit = self.unit
        invoice_line.product = self.product
        invoice_line.unit_price = self.unit_price
        invoice_line.taxes = self.taxes
        invoice_line.invoice_type = 'in'

        invoice_line.budget = self.budget
        invoice_line.account = self.account

        return [invoice_line]

    @fields.depends('account')
    def on_change_product(self):
        super(PurchaseLine, self).on_change_product()

    def _get_tax_rule_pattern(self):
        Date = Pool().get('ir.date')
        date = Date.today()
        if self.purchase and self.purchase.purchase_date:
            date = self.purchase.purchase_date
        default_pattern = super(PurchaseLine, self)._get_tax_rule_pattern()
        with Transaction().set_context(date=date):
            if self.account:
                default_pattern.update({'account': self.account.id})
        return default_pattern

    def get_move(self, move_type):
        move = super(PurchaseLine, self).get_move(move_type)
        if move:
            if not self.budgets:
                self.raise_user_error('error_without_budget', {
                    'product': move.product.rec_name,
                })
        return move
