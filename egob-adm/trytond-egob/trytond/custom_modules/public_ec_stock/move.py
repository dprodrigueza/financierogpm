import datetime

from sql import Null
from sql.operators import Concat

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Eval, If, Bool
from trytond.model import ModelView, ModelSQL
from trytond import backend

from trytond.modules.stock.move import STATES as MOVE_STATES
from trytond.modules.stock.move import DEPENDS as MOVE_DEPENDS

__all__ = ['Move', 'MoveAccount']


class MoveAccount(ModelSQL, ModelView):
    'Move Account'
    __name__ = 'stock.move.account'

    move = fields.Many2One('stock.move', 'Movimiento', ondelete='CASCADE',
        states={
            'readonly': True,
            'required': True
        })
    account = fields.Many2One('account.account', 'Cuenta',
        domain=[
            ('kind', 'not in', ['view', 'payable', 'payable_previous']),
            If(Bool(Eval('budget')),
                ('budget_debit', 'parent_of', Eval('budget'), 'parent'),
                ()
            )
        ], depends=['company', 'budget'])
    product = fields.Function(fields.Many2One("product.product", "Product"),
        'on_change_with_product')
    # TODO: update when changed in quantity "ALBARAN"
    quantity = fields.Numeric(
        'Cantidad', digits=(16, 2),
        domain=[
            ('quantity', '>', 0)
        ],
        states={
            'readonly': True,
            'required': True,
        }, depends=['move'])
    stock = fields.Function(
        fields.Numeric('Stock', digits=(16, 4)), 'on_change_with_stock')
    amount = amount = fields.Numeric('Valor', digits=(16, 4))

    @classmethod
    def __register__(cls, module_name):
        super(MoveAccount, cls).__register__(module_name)
        pool = Pool()
        move_account = cls.__table__()
        TableHandler = backend.get('TableHandler')
        cursor = Transaction().connection.cursor()
        exist = TableHandler.table_exist(cls._table)
        InvoiceLine = pool.get('account.invoice.line')
        table = InvoiceLine.__table__()
        Invoice = pool.get('account.invoice')
        invoice = Invoice.__table__()
        Move = pool.get('stock.move')
        move = Move.__table__()

        super(MoveAccount, cls).__register__(module_name)
        if exist:
            query_select = table.join(invoice,
                condition=invoice.id == table.invoice
            ).join(move,
                condition=table.origin == Concat('stock.move,', move.id)
            ).join(move_account, type_='LEFT',
                condition=move.id == move_account.move
            ).select(
                move.id,
                table.account,
                table.quantity,
                (table.unit_price * table.quantity).as_('amount'),
                where=((invoice.state == 'posted') &
                    (move_account.move == Null) & ((table.account != Null) |
                    (table.budget != Null))))
            query_insert = move_account.insert(
                [move_account.move, move_account.account,
                move_account.quantity, move_account.amount], query_select)
            cursor.execute(*query_insert)

    @fields.depends('move', '_parent_move.accounts')
    def on_change_move(self):
        if self.move:
            self.product = self.move.product

    @fields.depends('_parent_move_from_location', '_parent_move_product',
        '_parent_move_effective_date', 'account', 'product')
    def on_change_with_stock(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        stock_date_end = datetime.date.today()
        result = 0
        if self.product and self.move.from_location:
            if not self.account:
                pass
                #TODO:corregir este metodo
                #result = super(MoveAccount, self).on_change_with_stock(name)
            else:
                grouping_filter = ([self.product.id], [self.account.id])
                locations = [self.move.from_location.id]
                if self.move.effective_date:
                    stock_date_end = self.move.effective_date
                with Transaction().set_context(locations=locations,
                        stock_date_end=stock_date_end):
                    stock_data = Product.products_by_location(locations,
                        grouping=('product', 'account'),
                        grouping_filter=grouping_filter)
                    key = (self.move.from_location.id, self.product.id,
                        self.account.id)
                    if stock_data.get(key):
                        result = stock_data[key]
        return result

    @fields.depends('move', '_parent_move.accounts')
    def on_change_with_product(self, name=None):
        if self.move:
            return self.move.product.id


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'
    _history = True

    budget = fields.Many2One('public.budget', 'Partida',
        context={
            'stock_move': True,
            'product': Eval('product'),
            'certificate': Eval('_parent_shipment', {}).get('certificate', None)
        },
        states={
            'readonly': MOVE_STATES['readonly'],
            'required': False,
        }, depends=MOVE_DEPENDS + ['product', 'shipment'])
    account = fields.Many2One('account.account', 'Cuenta',
        domain=[
            ('budget_debit', 'parent_of', Eval('budget'), 'parent')
        ], depends=['budget'])
    accounts = fields.One2Many(
        'stock.move.account', 'move', 'Detalle Presupuestario',
        states={
            'readonly': MOVE_STATES['readonly'],
            'required': False,
        }, depends=MOVE_DEPENDS + ['product', 'shipment'])

    @fields.depends('from_location', 'product', 'effective_date', 'account')
    def on_change_with_stock(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        stock_date_end = datetime.date.today()
        result = 0
        if self.product and self.from_location:
            if not self.account:
                result = super(Move, self).on_change_with_stock(name)
            else:
                grouping_filter = ([self.product.id], [self.account.id])
                locations = [self.from_location.id]
                if self.effective_date:
                    stock_date_end = self.effective_date
                with Transaction().set_context(locations=locations,
                        stock_date_end=stock_date_end):
                    stock_data = Product.products_by_location(locations,
                        grouping=('product', 'account'),
                        grouping_filter=grouping_filter)
                    key = (self.from_location.id, self.product.id,
                        self.account.id)
                    if stock_data.get(key):
                        result = stock_data[key]
        return result
