from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.modules.stock_ec.product import ProductListReport
from decimal import Decimal

__all__ = ['Purchase', 'ProductsByCategory']


class Purchase(CompanyReportSignature):
    __name__ = 'purchase.purchase'


class ProductsByCategory(ProductListReport):
    __name__ = 'products.by.category.report'

    @classmethod
    def get_context(cls, records, data):
        context = super(ProductsByCategory, cls).get_context(
            records, data)
        context['categories'] = cls.grouped_categories
        return context

    @classmethod
    def grouped_categories(cls, records):
        data = {}
        for product in records:
            account = product.account_category
            if account.related_accounts:
                if not data.get(account.related_accounts[0].code):
                    data.update({
                        account.related_accounts[0].code: [
                            account.name,
                            0, ]})
                data[account.related_accounts[0].code][1] += \
                    Decimal(str(product.quantity)) * product.cost_price
        return data