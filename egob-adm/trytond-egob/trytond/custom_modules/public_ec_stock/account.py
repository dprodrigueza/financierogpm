import datetime

from sql import Window, With, Values, Literal
from sql.functions import RowNumber, CurrentTimestamp

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.transaction import Transaction

from trytond.tools import grouped_slice

__all__ = ['ProductByAccountContext', 'ProductByAccount']


class ProductByAccountContext(ModelView):
    "Product By Cost Center Context"
    __name__ = 'product.by.cost.center.context'

    stock_date_end = fields.Date('Date', required=True)
    locations = fields.Many2Many(
        'stock.location', None, None, 'Locations', required=True)

    @staticmethod
    def default_stock_date_end():
        pool = Pool()
        Date = pool.get('ir.date')
        return Date.today()

    @classmethod
    def default_locations(cls):
        context = Transaction().context
        if context.get('locations'):
            return context['locations']


class ProductByAccount(ModelSQL, ModelView):
    "Product By Cost Center"
    __name__ = 'product.by.cost.center'

    product = fields.Many2One('product.product', 'Producto')
    location = fields.Many2One('stock.location', 'Ubicación')
    account = fields.Many2One('account.account', 'Cuenta')
    quantity = fields.Float('Quantity')
    # forecast_quantity = fields.Float('Forecast Quantity')

    @classmethod
    def get_quantity(cls, accounts, name):
        pool = Pool()
        Product = pool.get('product.product')
        Location = pool.get('stock.location')
        Date_ = pool.get('ir.date')
        trans_context = Transaction().context

        if Transaction().context.get('locations'):
            locations = Location.search([
                ('id', 'in', Transaction().context.get('locations', [])),
            ])
        else:
            locations = Location.search([])

        context = {}
        if (name == 'quantity'
                and (trans_context.get('stock_date_end', datetime.date.max)
                    > Date_.today())):
            context['stock_date_end'] = Date_.today()

        if name == 'forecast_quantity':
            context['forecast'] = True
            if not trans_context.get('stock_date_end'):
                context['stock_date_end'] = datetime.date.max

        grouping = ('product',)
        grouping_filter = ([],)
        if trans_context.get('product') is not None:
            grouping_filter = ([trans_context['product']],)
        elif trans_context.get('product_template') is not None:
            grouping = ('product.template',)
            grouping_filter = ([trans_context['product_template']],)

        pbl = {}
        for sub_account in grouped_slice(accounts):
            acc_ids = ([a.id for a in sub_account],)
            grouping += ('account',)
            grouping_filter += acc_ids
            with Transaction().set_context(context):
                pbl.update(Product.products_by_location(
                        [l.id for l in locations],
                        grouping=grouping,
                        grouping_filter=grouping_filter,
                        with_childs=trans_context.get('with_childs', True)))
        return pbl

    @classmethod
    def table_query(cls):
        pool = Pool()
        Location = pool.get('stock.location')
        location_table = Location.__table__()
        Account = pool.get('account.account')
        accounts = Account.search([])
        values = cls.get_quantity(accounts, 'quantity')
        report_fields = ['location', 'product', 'account', 'quantity']
        report_values = []
        for key, value in values.items():
            location, product, account = key
            report_values.append([location, product, account, value])
        if not report_values:
            report_values.append([0, 0, 0, 0])
        report_data = With()
        report_data.columns = report_fields
        report_data.query = Values(report_values)
        table = report_data.select(with_=[report_data])
        query = table.join(location_table,
                condition=table.location == location_table.id
        ).select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            table.location,
            table.product,
            table.account,
            table.quantity,
            where=(table.quantity != 0) & (location_table.type == 'storage')
        )
        return query
