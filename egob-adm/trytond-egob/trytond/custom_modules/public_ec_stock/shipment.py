from collections import defaultdict
from decimal import Decimal

from trytond.model import ModelView, fields, ModelSQL
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.exceptions import UserError

from sql import Window, Literal, Union
from sql.functions import RowNumber, CurrentTimestamp

__all__ = ['ShipmentIn']


class ShipmentIn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.in'

    certificate = fields.Many2One('public.budget.certificate',
        'Certificación',
        states={
            'required': False,
            'readonly': Eval('state') != 'draft',
        },
        domain=[
            ('state', '=', 'done'),
        ], depends=['supplier', 'state'])
    compromise = fields.Many2One('public.budget.compromise', 'Compromiso',
        states={
            'required': False,
            'readonly': Eval('state') != 'draft',
        },
        domain=[
            ('party', '=', Eval('supplier')),
            ('state', '=', 'done'),
        ], depends=['supplier', 'state'])

    @classmethod
    def __setup__(cls):
        super(ShipmentIn, cls).__setup__()
        cls._buttons.update({
            'update_from_purchase': {
                'invisible': ~Eval('state').in_(['draft']),
            },
        })
        cls._error_messages.update({
                'no_location_for_account': ('There is no location defined for '
                    'account "%(account)s" on warehourse "%(warehouse)s"'),
                'no_account_on_origin': ('There is no account defined for '
                    '"%(origin)s" on purchase "%(purchase)s"'),
                'no_budget_on_origin': ('No hay partida definida '
                    'para el origen "%(origin)s" en la compra "%(purchase)s"'),
                })

    @classmethod
    @ModelView.button
    def update_from_purchase(cls, shipments):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        Move = pool.get('stock.move')
        to_save = []
        for shipment in shipments:
            for move in shipment.incoming_moves:
                if move.origin and isinstance(move.origin, PurchaseLine):
                    move.product = move.origin.product
                    move.quantity = move.origin.quantity
                    move.base_unit_price = move.origin.unit_price
                    move.on_change_base_unit_price()
                    move.budget = move.origin.budget
                    to_save.append(move)
        Move.save(to_save)

    @classmethod
    def _get_inventory_moves(cls, incoming_move):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')
        move = super(ShipmentIn, cls)._get_inventory_moves(incoming_move)
        if move:
            if incoming_move.origin:
                if isinstance(incoming_move.origin, PurchaseLine):
                    if not incoming_move.origin.budgets:
                        cls.raise_user_error('no_budget_on_origin', {
                            'origin': incoming_move.origin.rec_name,
                            'purchase': incoming_move.origin.purchase.rec_name,
                        })
                elif (not getattr(move, 'budget', False)
                      and incoming_move.budget):
                    move.budget = incoming_move.budget
        return move


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    move = fields.Many2One('account.move', 'Asiento contable', states={
        'readonly': True,
        'invisible': ~Bool(Eval('is_to_location_consumable'))
    }, depends=['is_to_location_consumable'])
    move_state = fields.Function(fields.Char('Estado de asiento contable',
        states={
            'invisible': True
        }),
        'on_change_with_move_state')
    is_to_location_consumable = fields.Function(
        fields.Boolean('Bodega Consumible',
        states={
            'invisible': True
        }),
        'on_change_with_is_to_location_consumable')

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()
        cls._buttons.update({
            'post_shipment_moves': {
                'invisible': ~Bool(Eval('is_to_location_consumable')) |
                    ~(Eval('move_state') != 'posted')
            },
        })
        cls.planned_date.states['invisible'] = (True)
        cls.effective_start_date.states['invisible'] = (True)
        cls.planned_start_date.states['invisible'] = (True)

    @fields.depends('move')
    def on_change_with_move_state(self, name=None):
        if self.move:
            return self.move.state
        else:
            return ''

    @fields.depends('to_location')
    def on_change_with_is_to_location_consumable(self, name=None):
        if self.to_location:
            return self.to_location.is_consumable
        else:
            return ''

    @classmethod
    @ModelView.button_action('public_ec_stock.act_post_shipment_moves')
    def post_shipment_moves(cls, works):
        pass


class PostShipmentMovesStart(ModelView):
    'Post Shipment Moves Start'
    __name__ = 'stock.shipment.post.moves.start'

    shipment = fields.Many2One('stock.shipment.internal', 'Albaran',
        states={'readonly': True})
    journal = fields.Many2One('account.journal', 'Libro diario',
        states={'required': True})
    period = fields.Many2One('account.period', 'Periodo',
        states={'readonly': True})
    lines = fields.One2Many('stock.shipment.post.moves.line.start',
        'post', 'Líneas', required=True)

    @classmethod
    def __setup__(cls):
        super(PostShipmentMovesStart, cls).__setup__()
        cls._error_messages.update({
            'lower_mileage': ('El recorrido o las horas m. ingresadas '
                'es menor al recorrido o horas m. actuales del '
                'vehículo: %s.')
            })

    @staticmethod
    def default_shipment():
        return Transaction().context['active_id']

    @staticmethod
    def default_period():
        Shipment = Pool().get('stock.shipment.internal')
        Period = Pool().get('account.period')
        shipment = Shipment(Transaction().context['active_id'])
        return Period.search([
            ('start_date','<=',shipment.effective_date),
            ('end_date','>=',shipment.effective_date)])[0].id

    @fields.depends('shipment', 'lines')
    def on_change_shipment(self, name=None):
        Shipment = Pool().get('stock.shipment.internal')
        Product = Pool().get('product.product')
        shipment = Shipment(Transaction().context['active_id'])
        Line = Pool().get('stock.shipment.post.moves.line.start')
        Account = Pool().get('account.account')
        lines = []
        for move in shipment.moves:
            line = Line()
            line.product = Product(move.product.id)
            line.amount = move.subtotal
            lines.append(line)
        self.lines = lines

        for line in self.lines:
            debit_account_ids = line.on_change_with_dselectable()
            if debit_account_ids:
                line.account_debit = Account(debit_account_ids[0])
                line.dselectable = debit_account_ids
            else:
                raise UserError('El producto ' + line.product.name +
                    ' tiene una categoría que no cuenta con configuración ' +
                    'de cuentas de consumo')
            credit_account_ids = line.on_change_with_cselectable()
            if credit_account_ids:
                line.account_credit = Account(credit_account_ids[0])
                line.cselectable = credit_account_ids
            else:
                raise UserError('El producto ' + line.product.name +
                  ' no tiene cuentas disponibles para el crédito')


class PostShipmentMovesLineStart(ModelView):
    'Post Shipment Moves Start'
    __name__ = 'stock.shipment.post.moves.line.start'

    post = fields.Many2One('stock.shipment.post.moves.start', 'Contabilización')
    product = fields.Many2One('product.product', 'Producto', states={
        'required': True,
        # 'readonly': True
    })
    account_debit = fields.Many2One('account.account', 'Cuenta débito', domain=[
            ('id', 'in', Eval('dselectable'))
        ], depends=['dselectable'])
    dselectable = fields.Function(
        fields.One2Many('account.account', None,
            'Cuentas'), 'on_change_with_dselectable')
    account_credit = fields.Many2One('account.account', 'Cuenta crédito', domain=[
            ('id', 'in', Eval('cselectable'))
        ], depends=['cselectable'])
    cselectable = fields.Function(
        fields.One2Many('account.account', None,
            'Cuentas'),'on_change_with_cselectable')
    amount = fields.Numeric('Total',states={
        'readonly': True
    })

    @fields.depends('product', 'account_debit')
    def on_change_with_dselectable(self, name=None):
        if self.product:
            if self.product.account_category:
                return [a.id for a in self.product.account_category.related_accounts]
        return []

    @fields.depends('product', 'account_credit')
    def on_change_with_cselectable(self, name=None):
        pool = Pool()
        StockMoveAccounts = pool.get('stock.move.account.product.query')
        params = {
            'product': self.product.id,
        }
        account_ids = []
        with Transaction().set_context(**params):
            table_query = StockMoveAccounts.table_query()
            cursor = Transaction().connection.cursor()
            cursor.execute(*table_query)
            for row in cursor.fetchall():
                account_ids.append(row[5])
        return account_ids




class PostShipmentMoves(Wizard):
    'Post Shipment Moves'
    __name__ = 'stock.shipment.post.moves'

    start = StateView('stock.shipment.post.moves.start',
        'public_ec_stock.post_shipment_moves_view_form', [
            Button('Cancelar', 'end', 'tryton-cancel'),
            Button('OK', 'post', 'tryton-ok', default=True),
            ])
    post = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PostShipmentMoves, cls).__setup__()
        cls._error_messages.update({
            'lower_mileage': ('El recorrido o las horas m. ingresadas '
                'es menor al recorrido o horas m. actuales del '
                'vehículo: %s.')
            })

    def transition_post(self):
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Shipment = pool.get('stock.shipment.internal')
        move = Move()
        if self.start.shipment.move:
            move = self.start.shipment.move
            MoveLine.delete(move.lines)
        move.journal = self.start.journal
        move.period = self.start.period
        move.type = 'adjustment'
        move.date = self.start.shipment.effective_date
        move.description = 'Movimiento #' + self.start.shipment.number + \
            ' desde ubicación ' + \
            self.start.shipment.from_location.name + \
            ' hacia la ubicación ' + self.start.shipment.to_location.name + \
            ' solicitada por el empleado ' + \
            self.start.shipment.employee.party.name + \
            ' del departamento ' + self.start.shipment.department.name
        move.origin = 'stock.shipment.internal,'+str(self.start.shipment.id)
        move.lines = []
        # amount = company.currency.round(Decimal(str(rv[2])))
        for line in self.start.lines:
            line_debit = MoveLine()
            line_debit.account = line.account_debit
            line_debit.debit = Decimal(line.amount).quantize(Decimal('.01'))
            line_debit.adjustment  = True
            move.lines += (line_debit,)


            line_credit = MoveLine()
            line_credit.account = line.account_credit
            line_credit.credit = Decimal(line.amount).quantize(Decimal('.01'))
            line_credit.adjustment  = True
            move.lines += (line_credit,)
        Move.save([move])
        self.start.shipment.move = move
        Shipment.save([self.start.shipment])
        return 'end'


class StockMoveAccountProductQuery(ModelSQL, ModelView):
    'Stock Move Account Product Query'
    __name__ = 'stock.move.account.product.query'

    account = fields.Many2One('account.account', 'Cuenta',
        readonly=True)

    @staticmethod
    def table_query():
        context = Transaction().context
        pool = Pool()
        MoveAccount = pool.get('stock.move.account')
        move_account = MoveAccount.__table__()
        Move = pool.get('stock.move')
        move = Move.__table__()
        Account = pool.get('account.account')
        account = Account.__table__()
        Product = pool.get('product.product')
        product = Product.__table__()
        where = (product.id == context.get('product'))

        query = move_account.join(move,
            condition=move_account.move ==  move.id
        ).join(account,
            condition=move_account.account == account.id
        ).join(product,
            condition=move.product == product.id
        ).select(
            account.id.as_('account'),
            where=where)
        return query.select(
            RowNumber(window=Window([])).as_('id'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(0).as_('write_uid'),
            CurrentTimestamp().as_('write_date'),
            query.account,
        )
