from trytond.pool import Pool

from . import location
from . import invoice
from . import purchase
from . import move
from . import shipment
from . import account
from . import product
from . import budget
from . import report

__all__ = ['register']


def register():
    Pool.register(
        invoice.Invoice,
        invoice.InvoiceLine,
        invoice.CreateInvoiceStart,
        location.Location,
        location.LocationDomain,
        purchase.Purchase,
        purchase.PurchaseLine,
        move.Move,
        move.MoveAccount,
        shipment.ShipmentIn,
        shipment.ShipmentInternal,
        shipment.PostShipmentMovesStart,
        shipment.PostShipmentMovesLineStart,
        shipment.StockMoveAccountProductQuery,
        account.ProductByAccountContext,
        account.ProductByAccount,
        product.Category,
        product.CategoryBudget,
        product.RelatedAccount,
        budget.PublicBudget,
        module='public_ec_stock', type_='model')
    Pool.register(
        invoice.CreateInvoice,
        shipment.PostShipmentMoves,
        module='public_ec_stock', type_='wizard')
    Pool.register(
        report.Purchase,
        report.ProductsByCategory,
        module='public_ec_stock', type_='report')
