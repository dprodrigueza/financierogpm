from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool

__all__ = ['Category', 'CategoryBudget', 'RelatedAccount']


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    budgets = fields.Many2Many(
        'product.category.budget', 'category', 'budget', 'Partidas',
        domain=[
            ('template', '!=', None),
        ])
    related_accounts = fields.Many2Many('product.category.account.account',
        'category', 'account', 'Cuentas de consumo', domain=[
            ('kind', '=', 'expense'),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ])
    

    @classmethod
    def view_attributes(cls):
        return super(Category, cls).view_attributes() + [(
            '//page[@id="page_budgets"]', 'states', {
                'invisible':  ~Eval('accounting', False),
            }),
            ('//page[@id="related_accounts"]', 'states', {
                'invisible':  ~Eval('accounting', False),
            }),
        ]

class CategoryBudget(ModelSQL):
    'Category Budget'
    __name__ = 'product.category.budget'

    category = fields.Many2One('product.category', 'Categoría', required=True)
    budget = fields.Many2One('public.budget', 'Partida', required=True)


class RelatedAccount(ModelSQL):
    'Related Account'
    __name__ = 'product.category.account.account'

    category = fields.Many2One('product.category', 'Categoría', required=True)
    account = fields.Many2One('account.account', 'Cuenta', domain=[
            ('kind', '=', 'expense'),
            ('company', '=', Eval('context', {}).get('company', -1)),
        ], required=True)
