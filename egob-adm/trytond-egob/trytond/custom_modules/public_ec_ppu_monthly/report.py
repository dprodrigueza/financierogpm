from collections import defaultdict
from decimal import Decimal
from sql.conditionals import Coalesce, Case

from trytond.pool import Pool

from trytond.modules.hr_ec.company import CompanyReportSignature
from trytond.transaction import Transaction
from trytond.tools import reduce_ids, grouped_slice, cursor_dict

__all__ = ['MothlyReport']

class MothlyReport(CompanyReportSignature):
    __name__ = 'public.planning.unit.month'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MothlyReport, cls).get_context(records, data)
        poa_data = cls.get_data()
        headers = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre',
            'Diciembre', 'Total']
        months = cls.get_months(poa_data)
        report_context['headers'] = headers
        report_context['poa_data'] = poa_data
        report_context['months'] = months
        return report_context

    @classmethod
    def get_data(cls):
        pool = Pool()
        PPU = pool.get('public.planning.unit')
        USER = pool.get('res.user')
        PB = pool.get('public.budget')
        PBP = pool.get('public.budget.planning')
        cursor = Transaction().connection.cursor()
        result = []

        ppu_table = PPU.__table__()
        user_table = USER.__table__()

        pb_table = PB.__table__()
        pbp_table = PBP.__table__()
        query = ppu_table.join(
            user_table,
            condition=user_table.id == ppu_table.create_uid
        ).join(
            pbp_table, type_='LEFT',
            condition=pbp_table.activity == ppu_table.id
        ).join(
            pb_table, type_='LEFT',
            condition=pb_table.id == pbp_table.budget_parent
        ).select(
            ppu_table.id,
            user_table.name.as_('user'),
            pbp_table.reference,
            pbp_table.initial_amount,
            pb_table.code.as_('budget_code'),
            pb_table.name.as_('budget'),
            Case(
                (pb_table.type == 'expense', 'Gastos'),
                (pb_table.type == 'revenue', 'Ingresos'),
                (pb_table.type == 'finance', 'Aplica Finanzas'),
                (pb_table.type == 'other', 'Otro')).as_('type'),
            ppu_table.code.as_('code'),
            ppu_table.name.as_('activity'),
            where=(ppu_table.type == 'activity') &
                  (ppu_table.code.like('2020.%')),
            order_by=[ppu_table.code]
        )
        cursor.execute(*query)
        for row in cursor_dict(cursor):
            data = {
                'id': row['id'],
                'user': row['user'],
                'reference': row['reference'],
                'initial_amount': row['initial_amount'],
                'budget_code': row['budget_code'],
                'budget': row['budget'],
                'budget_type': row['type'],
                'code': row['code'],
                'level': len(row['code'].split(".")),
                'activity': row['activity'],
            }
            result.append(data)
        return result

    @classmethod
    def get_months(cls, activities):
        ids = []
        data_months = defaultdict(lambda: 0)
        PPU = Pool().get('public.planning.unit')
        for activity in activities:
            if activity['id'] not in ids:
                ids.append(activity['id'])     
        for id in ids:
            activity, = PPU.search([
                    ('id', '=', id)
                ])

            months = {
                'Enero': activity.m_01,
                'Febrero': activity.m_02,
                'Marzo': activity.m_03, 
                'Abril': activity.m_04,
                'Mayo': activity.m_05,
                'Junio': activity.m_06,
                'Julio': activity.m_07, 
                'Agosto': activity.m_08, 
                'Septiembre': activity.m_09, 
                'Octubre': activity.m_10, 
                'Noviembre': activity.m_11,
                'Diciembre': activity.m_12,
                'Total': activity.m_total
            }
            data_months[id] = months
        return data_months