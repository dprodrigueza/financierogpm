# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class PublicEcPpuMonthlyTestCase(ModuleTestCase):
    'Test Public Ec Ppu Monthly module'
    module = 'public_ec_ppu_monthly'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PublicEcPpuMonthlyTestCase))
    return suite
