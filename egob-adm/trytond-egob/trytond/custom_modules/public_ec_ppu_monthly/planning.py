from decimal import Decimal
from collections import defaultdict

from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Bool, Eval
from trytond.tools import grouped_slice, reduce_ids, cursor_dict

__all__ = ['PublicPlanningUnit']


class PublicPlanningUnit(metaclass=PoolMeta):
    __name__ = 'public.planning.unit'

    start_month = fields.Function(
        fields.Integer('Mes Inicial', depends=['start_date', 'end_date']),
    'on_change_with_start_month')
    end_month = fields.Function(
        fields.Integer('Mes Final', depends=['start_date', 'end_date']),
        'on_change_with_end_month')

    # Planning Month Values
    annual = fields.Boolean('Ingresar valor anual')
    m_01 = fields.Numeric('Enero', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 1) &
                 (Eval('end_month', 0) >= 1)))
        }, depends=['annual', 'start_month', 'end_month', 'start_date',
                    'end_date'])
    m_02 = fields.Numeric('Febrero', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 2) &
                 (Eval('end_month', 0) >= 2)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_03 = fields.Numeric('Marzo', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 3) &
                 (Eval('end_month', 0) >= 3)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_04 = fields.Numeric('Abril', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 4) &
                 (Eval('end_month', 0) >= 4)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_05 = fields.Numeric('Mayo', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 5) &
                 (Eval('end_month', 0) >= 5)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_06 = fields.Numeric('Junio', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 6) &
                 (Eval('end_month', 0) >= 6)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_07 = fields.Numeric('Julio', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 7) &
                 (Eval('end_month', 0) >= 7)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_08 = fields.Numeric('Agosto', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 8) &
                 (Eval('end_month', 0) >= 8)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_09 = fields.Numeric('Septiembre', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 9) &
                 (Eval('end_month', 0) >= 9)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_10 = fields.Numeric('Octubre', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 10) &
                 (Eval('end_month', 0) >= 10)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_11 = fields.Numeric('Noviembre', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 11) &
                 (Eval('end_month', 0) >= 11)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_12 = fields.Numeric('Diciembre', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(~Bool(Eval('annual')) &
                ((Eval('start_month', 0) <= 12) &
                 (Eval('end_month', 0) >= 12)))
        }, depends=['annual', 'start_month', 'end_month'])
    m_total = fields.Numeric('Total', digits=(16, 2),
        states={
            'invisible': ~(Eval('type') == 'activity'),
            'readonly': ~(Bool(Eval('annual'))),
        })
    p_01 = fields.Function(fields.Numeric('Enero', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_02 = fields.Function(fields.Numeric('Febrero', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_03 = fields.Function(fields.Numeric('Marzo', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_04 = fields.Function(fields.Numeric('Abril', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_05 = fields.Function(fields.Numeric('Mayo', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_06 = fields.Function(fields.Numeric('Junio', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_07 = fields.Function(fields.Numeric('Julio', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_08 = fields.Function(fields.Numeric('Agosto', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_09 = fields.Function(fields.Numeric('Septiembre', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_10 = fields.Function(fields.Numeric('Octubre', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_11 = fields.Function(fields.Numeric('Noviembre', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_12 = fields.Function(fields.Numeric('Diciembre', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')
    p_total = fields.Function(fields.Numeric('Total', digits=(16, 2),
        states={
            'invisible': (Eval('type') == 'activity')
        }), 'get_month_values')

    @staticmethod
    def default_m_01():
        return Decimal('0.0')

    @staticmethod
    def default_m_02():
        return Decimal('0.0')

    @staticmethod
    def default_m_03():
        return Decimal('0.0')

    @staticmethod
    def default_m_04():
        return Decimal('0.0')

    @staticmethod
    def default_m_05():
        return Decimal('0.0')

    @staticmethod
    def default_m_06():
        return Decimal('0.0')

    @staticmethod
    def default_m_07():
        return Decimal('0.0')

    @staticmethod
    def default_m_08():
        return Decimal('0.0')

    @staticmethod
    def default_m_09():
        return Decimal('0.0')

    @staticmethod
    def default_m_10():
        return Decimal('0.0')

    @staticmethod
    def default_m_11():
        return Decimal('0.0')

    @staticmethod
    def default_m_12():
        return Decimal('0.0')

    @staticmethod
    def default_m_total():
        return Decimal('0.0')

    @fields.depends('start_date')
    def on_change_with_start_month(self, name=None):
        if self.start_date:
            return self.start_date.month
        return 0

    @fields.depends('end_date')
    def on_change_with_end_month(self, name=None):
        if self.end_date:
            return self.end_date.month
        return 0

    @fields.depends('m_01', 'm_02', 'm_03', 'm_04', 'm_05', 'ḿ_06', 'm_07',
        'm_08', 'm_09', 'm_10', 'm_11', 'm_12', 'm_total', 'start_date',
        'annual')
    def on_change_start_date(self):
        super(PublicPlanningUnit, self).on_change_start_date()
        if self.start_date:
            month = self.start_date.month
            for m in range(1, month):
                m_ = 'm_' + str(m).zfill(2)
                setattr(self, m_, Decimal('0.0'))
            if self.annual:
                self.on_change_m_total()
            else:
                self.m_total = self.on_change_with_m_total()

    @fields.depends('m_01', 'm_02', 'm_03', 'm_04', 'm_05', 'ḿ_06', 'm_07',
        'm_08', 'm_09', 'm_10', 'm_11', 'm_12', 'm_total', 'start_date',
        'annual')
    def on_change_end_date(self):
        super(PublicPlanningUnit, self).on_change_end_date()
        if self.end_date:
            month = self.end_date.month
            for m in range(month + 1, 12 + 1):
                m_ = 'm_' + str(m).zfill(2)
                setattr(self, m_, Decimal('0.0'))
            if self.annual:
                self.on_change_m_total()
            else:
                self.m_total = self.on_change_with_m_total()

    @fields.depends('parent', '_parent_parent.children_number')
    def on_change_parent(self):
        super(PublicPlanningUnit, self).on_change_parent()
        self.start_month = self.on_change_with_start_month()
        self.end_month = self.on_change_with_end_month()

    @fields.depends('m_01', 'm_02', 'm_03', 'm_04', 'm_05', 'm_06', 'm_07',
        'm_08', 'm_09', 'm_10', 'm_11', 'm_12', 'annual')
    def on_change_with_m_total(self):
        if not self.annual:
            value = Decimal('0.0')

            value += (self.if_exists(self.m_01) + self.if_exists(self.m_02) +
                      self.if_exists(self.m_03) + self.if_exists(self.m_04) +
                      self.if_exists(self.m_05) + self.if_exists(self.m_06) +
                      self.if_exists(self.m_07) + self.if_exists(self.m_08) +
                      self.if_exists(self.m_09) + self.if_exists(self.m_10) +
                      self.if_exists(self.m_11) + self.if_exists(self.m_12))

            return value

    def if_exists(self, value):
        if value is None:
            return Decimal('0.0')
        else:
            return value

    @fields.depends('m_01', 'm_02', 'm_03', 'm_04', 'm_05', 'ḿ_06', 'm_07',
        'm_08', 'm_09', 'm_10', 'm_11', 'm_12', 'm_total', 'annual',
        'start_date', 'end_date')
    def on_change_m_total(self):
        if self.annual and self.m_total:
            if self.start_date and self.end_date:
                start_month = self.start_date.month
                end_month = self.end_date.month
                total_months = end_month - (start_month - 1)
                sum_value = Decimal('0.0')
                month_value = (self.m_total / Decimal(str(total_months))
                    ).quantize(Decimal('0.01'))
                for m in range(start_month, end_month):
                    m_ = 'm_' + str(m).zfill(2)
                    sum_value += month_value
                    setattr(self, m_, month_value)
                setattr(self, 'm_' + str(end_month).zfill(2),
                    self.m_total - sum_value)

    @classmethod
    def get_month_values(cls, ppus, names):
        cursor = Transaction().connection.cursor()
        table_a = cls.__table__()
        table_c = cls.__table__()
        suffix = ['_01', '_02', '_03', '_04', '_05', '_06', '_07', '_08',
            '_09', '_10', '_11', '_12', '_total']
        result = {}
        columns = [table_a.id]
        for suff in suffix:
            result['p' + suff] = defaultdict(lambda: Decimal('0.0'))
            columns.append(
                Sum(Coalesce(getattr(table_c, 'm' + suff), 0)).as_('p' + suff))
        poas = []
        ids = []
        for p in ppus:
            if p.type != 'poa':
                ids.append(p.id)
            else:
                poas.append(p)
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table_a.id, sub_ids)
            cursor.execute(*table_a.join(table_c,
                condition=(table_c.left >= table_a.left)
                    & (table_c.right <= table_a.right)
            ).select(
                *columns,
                where=red_sql & (table_c.active == True),
                group_by=table_a.id))
            for line in cursor_dict(cursor):
                for suff in suffix:
                    result['p' + suff][line['id']] = line['p' + suff]
        for poa in poas:
            data = cls.get_month_values(poa.children, names=[])
            for child in poa.children:
                if child.type != 'funding_program':
                    for suff in suffix:
                        data['p' + suff][child.id] = (
                            data['p' + suff][child.id] * -1)
            for suff in suffix:
                result['p' + suff][poa.id] = sum(
                    [val for key, val in data['p' + suff].items()])
        return result
