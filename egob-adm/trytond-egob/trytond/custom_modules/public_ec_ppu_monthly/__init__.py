from trytond.pool import Pool
from . import planning
from . import report

__all__ = ['register']


def register():
    Pool.register(
        planning.PublicPlanningUnit,
        module='public_ec_ppu_monthly', type_='model')
    Pool.register(
        module='public_ec_ppu_monthly', type_='wizard')
    Pool.register(
        report.MothlyReport,
        module='public_ec_ppu_monthly', type_='report')
