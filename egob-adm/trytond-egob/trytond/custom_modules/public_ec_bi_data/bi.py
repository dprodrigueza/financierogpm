from pathlib import Path
import csv
import collections

from trytond.config import config
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

import pandas as pd

__all__ = ['PublicBudgetCard']


def sort_fields(data=[]):
    '''
    :param data: must be a list of dictionaries, for example:
        [{key: value}, {key: value}]
    :return: a list of sorted dictionaries
    '''
    _data = []
    for d in data:
        d = collections.OrderedDict(sorted(d.items()))
        _data.append(d)
    return _data


class PublicBudgetCard(metaclass=PoolMeta):
    __name__ = 'public.budget.card'

    @classmethod
    def generate_file(cls, context, path_name, filename):
        fields = [fname for fname in cls._fields.keys()]
        company = Transaction().context.get('company')
        path_name = f"{path_name}/{company}/budget/"
        path = Path(path_name)
        path.mkdir(parents=True, exist_ok=True)
        filename = f"{str(path)}/{filename}.csv"
        with Transaction().set_context(context):
            data = cls.search_read([
                ('kind', '=', 'other'),
            ], fields_names=fields)
            data = sort_fields(data)
            data_frame = pd.DataFrame.from_dict(data)
            data_frame.reindex(columns=sorted(data_frame.columns))
            data_frame.to_csv(filename, sep='|', encoding='utf-8', index=False,
                quoting=csv.QUOTE_NONNUMERIC)

    @classmethod
    def generate_bi_data(cls):
        pool = Pool()
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        Fiscalyear = pool.get('account.fiscalyear')
        today = Date.today()
        bi_path = config.get('siim', 'bi_data_path')
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])
        if bi_path and poa:
            fiscalyear, = Fiscalyear.search([
                ('start_date', '<=', today),
                ('end_date', '>=', today),
            ])
            context = {
                'poa': poa[0].id,
                'start_date': fiscalyear.start_date,
            }
            for period in fiscalyear.periods:
                # Exclude start and close periods
                if period.start_date != period.end_date:
                    context.update({
                        'end_date': period.end_date,
                    })
                    cls.generate_file(context, bi_path, period.name)
            context.update({
                'end_date': fiscalyear.end_date,
            })
            cls.generate_file(context, bi_path, fiscalyear.name)
