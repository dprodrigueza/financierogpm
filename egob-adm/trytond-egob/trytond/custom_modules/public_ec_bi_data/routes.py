import simplejson as json
from simpleeval import EvalWithCompoundTypes
import pendulum

from trytond.transaction import Transaction
from trytond.wsgi import app
from trytond.protocols.wrappers import (with_pool, with_transaction,
    user_application)

bi_application = user_application('bi')


@app.route('/<database_name>/bi/indicator/', methods=['GET'])
@with_pool
@with_transaction()
@bi_application
def indicator(request, pool):
    Indicator = pool.get('general.indicator')
    Date = pool.get('ir.date')
    codes = []
    evaluator = EvalWithCompoundTypes()
    company = None
    if request.headers.get('codes'):
        codes = evaluator.eval(request.headers['codes'])
    data = {}
    if request.headers.get('eval_date'):
        eval_date = pendulum.parse(request.headers['eval_date']).date()
    else:
        eval_date = Date.today()
    if request.headers.get('company'):
        company = evaluator.eval(request.headers['company'])
    with Transaction().set_context(eval_date=eval_date, company=company):
        data = Indicator.search_read([
            ('code', 'in', codes),
        ], fields_names=['id', 'code', 'result'])
    data = json.dumps(data).replace('"', '\'')
    return data
