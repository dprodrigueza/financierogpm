from trytond.pool import Pool

from . import bi
from . import user
from . import routes

__all__ = ['register', 'routes']


def register():
    Pool.register(
        bi.PublicBudgetCard,
        user.UserApplication,
        module='public_ec_bi_data', type_='model')
    Pool.register(
        module='public_ec_bi_data', type_='wizard')
    Pool.register(
        module='public_ec_bi_data', type_='report')
