from collections import defaultdict
from decimal import Decimal

from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction


__all__ = [
    'PublicBudgetPlanningAnalyticAccount'
]


class PublicBudgetPlanningAnalyticAccount(ModelSQL, ModelView):
    'Public Budget Planning Analytic Account'
    __name__ = 'public.budget.planning.analytic_account'

    poa = fields.Many2One(
        'public.planning.unit', 'POA',
        domain=[
            ('type', '=', 'poa'),
            ('company', '=', Eval('context', {}).get('company', -1))
        ], states={
            'readonly': True,
            'required': True,
        }
    )
    plannings = fields.Function(
        fields.One2Many('public.budget.planning', None, 'Planificaiciones'),
        'on_change_with_plannings'
    )
    planning = fields.Many2One(
        'public.budget.planning', 'Planificacion',
        states={
            'required': True,
        },
        domain=[
            ('id', 'in', Eval('plannings')),
        ], depends=['plannings']
    )
    budget = fields.Function(
        fields.Many2One('public.budget', 'Partida'),
        'on_change_with_budget'
    )
    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica',
        states={
            'required': True,
        },
        domain=[
            ('type', 'not in', ['view', 'distribution', 'root']),
            ('state', '=', 'opened'),
        ]
    )


    @classmethod
    def __setup__(cls):
        super(PublicBudgetPlanningAnalyticAccount, cls).__setup__()
        cls._order = [
            ('poa', 'ASC'),
            ('planning', 'ASC')
        ]

    @staticmethod
    def default_poa():
        pool = Pool()
        POA = pool.get('public.planning.unit')
        Date = pool.get('ir.date')
        today = Date.today()
        poa = POA.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today),
            ('type', '=', 'poa'),
        ])[0]
        return poa.id

    @fields.depends('poa')
    def on_change_with_plannings(self, name=None):
        if self.poa:
            Planning = Pool().get('public.budget.planning')
            plannings = Planning.search_read([
                ('activity.type', '=', 'activity'),
                ('activity.parent', 'child_of', self.poa.id),
            ], fields_names=['id'])
            if plannings:
                return [p['id'] for p in plannings]
        return []

    @fields.depends('planning')
    def on_change_with_budget(self, name=None):
        if self.planning:
            return self.planning.budget.id
        return None