# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class AnalyticAccountEcTestCase(ModuleTestCase):
    'Test Analytic Account Ec module'
    module = 'analytic_account_ec'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            AnalyticAccountEcTestCase))
    return suite
