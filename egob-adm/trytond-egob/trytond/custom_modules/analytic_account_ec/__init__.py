# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import line
from . import account
from . import configuration
from . import asset
from . import account_card
from . import planning
from . import budget
from . import report

__all__ = ['register']


def register():
    Pool.register(
        account.Account,
        account.AccountCardContext,
        account.AnalyticAccount,
        account.AccountTemplate,
        account.AccountTemplateLine,
        account.AnalyticPeriod,
        account.FiscalYear,
        account.AccountTemplateBudget,
        account.AccountTemplateAccount,
        account.GeneralLedgerAccountContext,
        account.GeneralLedgerAccount,
        account.EnterCostCenterToAnalyticAccountStart,
        account.AnalyticAccountGeneral,
        account.AnalyticAccountGeneralLine,
        account.AnalyticAccountGeneralComplete,
        account.AnalyticAccountGeneralCompleteLine,
        account.AnalyticAccountContext,
        account.AnalyticAccountContextLine,
        account.GeneralLedgerLineContext,
        account_card.AccountCardGenerateStart,
        account_card.AnalyticAccountCard,
        account_card.FinancialReportGenerateESFStart,
        account_card.FinancialReportGenerateStart,
        configuration.Configuration,
        configuration.AccountConfigurationCodePattern,
        line.Line,
        line.Move,
        line.MoveLine,
        line.MoveLineAccountTemplate,
        line.MoveLineAccountTemplateLine,
        line.EnterMoveAnalyticAccountStart,
        asset.Asset,
        asset.AnalyticAsset,
        planning.PublicBudgetPlanningAnalyticAccount,
        budget.PublicBudgetReformLine,
        budget.PublicBudgetCertificateLine,
        module='analytic_account_ec', type_='model')
    Pool.register(
        account.EnterCostCenterToAnalyticAccount,
        account_card.AccountCardGenerate,
        account_card.FinancialReportESF,
        account_card.FinancialReportERE,
        account_card.FinancialReportEFE,
        line.EnterMoveAnalyticAccount,
        module='analytic_account_ec', type_='wizard')
    Pool.register(
        report.AccountCardAnalytic,
        report.AccountAnalyticFinancialESF,
        report.AnalyticFinancialReportERE,
        report.AnalyticFinancialReportEFE,
        module='analytic_account_ec', type_='report')
