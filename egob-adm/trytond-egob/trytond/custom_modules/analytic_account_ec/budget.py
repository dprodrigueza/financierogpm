from collections import defaultdict
from decimal import Decimal

from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction


__all__ = [
    'PublicBudgetReformLine'
]


class PublicBudgetReformLine(metaclass=PoolMeta):
    __name__ = 'public.budget.reform.line'

    from_accounts = fields.Function(
        fields.One2Many('analytic_account.account', None, 'Cuentas origenes'),
        'on_change_with_from_accounts'
    )
    from_analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica origen',
        domain=[
            ('id', 'in', Eval('from_accounts')),
        ],
        states={
            'readonly': (Eval('reform_state') != 'draft') | (
                    Eval('reform_type') == 'increase'),
            'invisible': ~(Eval('type') == 'line'),
            'required': Bool(Eval('from_accounts')) & (Eval('type') == 'line')
        }, depends=['reform_state', 'reform_type', 'type', 'from_accounts']
    )
    to_accounts = fields.Function(
        fields.One2Many('analytic_account.account', None, 'Cuentas destino'),
        'on_change_with_to_accounts'
    )
    to_analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica destino',
        domain=[
            ('id', 'in', Eval('to_accounts')),
        ],
        states={
            'readonly': (Eval('reform_state') != 'draft') | (
                    Eval('reform_type') == 'decrease'),
            'invisible': ~(Eval('type') == 'line'),
            'required': Bool(Eval('to_accounts')) & (Eval('type') == 'line')
        }, depends=['reform_state', 'reform_type', 'type', 'to_accounts']
    )

    @fields.depends('from_budget', 'reform')
    def on_change_with_from_accounts(self, name=None):
        if self.from_budget and self.reform:
            return self.get_analytic_accounts(
                self.reform.poa.id, self.from_budget)
        return []

    @fields.depends('to_budget', 'reform')
    def on_change_with_to_accounts(self, name=None):
        if self.to_budget and self.reform:
            return self.get_analytic_accounts(
                self.reform.poa.id, self.to_budget)
        return []

    def get_analytic_accounts(self, poa, budget):
        pool = Pool()
        PlanningAnalyticAccount = pool.get(
            'public.budget.planning.analytic_account')
        planning_analytic_accounts = PlanningAnalyticAccount.search([
            ('poa', '=', poa),
        ])
        if planning_analytic_accounts:
            return [x.analytic_account.id
                    for x in planning_analytic_accounts if x.budget == budget]
        return []


class PublicBudgetCertificateLine(metaclass=PoolMeta):
    __name__ = 'public.budget.certificate.line'

    accounts = fields.Function(
        fields.One2Many('analytic_account.account', None, 'Cuentas origenes'),
        'on_change_with_accounts'
    )
    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica origen',
        domain=[
            ('id', 'in', Eval('accounts')),
        ],
        states={
            'readonly': Eval('certificate_state') != 'draft',
            'invisible': ~(Eval('type') == 'line'),
            'required': Bool(Eval('accounts')) & (Eval('type') == 'line')
        }, depends=['certificate_state', 'type', 'accounts']
    )

    @fields.depends('budget', 'certificate')
    def on_change_with_accounts(self, name=None):
        if self.budget and self.certificate:
            pool = Pool()
            PlanningAnalyticAccount = pool.get(
                'public.budget.planning.analytic_account')
            planning_analytic_accounts = PlanningAnalyticAccount.search([
                ('poa', '=', self.certificate.poa.id),
            ])
            if planning_analytic_accounts:
                return [x.analytic_account.id
                        for x in planning_analytic_accounts
                        if x.budget == self.budget]
        return []