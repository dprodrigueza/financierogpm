from trytond.pool import PoolMeta, Pool
from trytond.model import fields

__all__ = [
    'Configuration', 'AccountConfigurationCodePattern'
]


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    code_pattern_analytic_account = fields.MultiValue(
        fields.Char("Patrón de código analítica"))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'code_pattern_analytic_account':
            return pool.get('account.configuration.code_pattern')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_code_pattern_analytic_account(cls, **pattern):
        return cls.multivalue_model('code_pattern_analytic_account').default_code_pattern_analytic_account()


class AccountConfigurationCodePattern(metaclass=PoolMeta):
    __name__ = 'account.configuration.code_pattern'

    code_pattern_analytic_account = fields.Char("Codido de patron de analitica")

    @staticmethod
    def default_code_pattern_analytic_account():
        return '^[0-9]{3,}([.][0-9]{2,})*'