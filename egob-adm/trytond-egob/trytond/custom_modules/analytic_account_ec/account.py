import re
from decimal import Decimal
from collections import defaultdict
from functools import reduce

from sql import Column
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields, DeactivableMixin, \
    Workflow
from trytond.pyson import Eval, If
from trytond.wizard import Wizard, StateView, StateTransition, Button

__all__ = [
    'AnalyticAccount', 'AccountTemplate', 'AccountTemplateLine', 'Account',
    'AnalyticPeriod', 'FiscalYear', 'AccountTemplateAccount',
    'AccountTemplateBudget', 'GeneralLedgerAccountContext',
    'GeneralLedgerAccount', 'EnterCostCenterToAnalyticAccount',
    'EnterCostCenterToAnalyticAccountStart', 'GeneralLedgerLineContext',
    'AnalyticAccountContext', 'AnalyticAccountContextLine',
    'AnalyticAccountGeneral', 'AnalyticAccountGeneralLine',
    'AnalyticAccountGeneralComplete', 'AnalyticAccountGeneralCompleteLine',
    'AccountCardContext'
]


class Account(metaclass=PoolMeta):
    __name__ = 'account.account'

    analytic_account = fields.Boolean('Necesita contabilidad analitica?')

    @staticmethod
    def default_analytic_account():
        return False


class AnalyticAccount(Workflow, metaclass=PoolMeta):
    __name__ = 'analytic_account.account'

    _history = True

    balance_by_account = fields.Function(
        fields.Numeric('Balance'),
        'on_change_with_balance_by_account'
    )
    debit_by_account = fields.Function(
        fields.Numeric('Debito'),
        'get_debit_credit_by_account'
    )
    credit_by_account = fields.Function(
        fields.Numeric('Credito'),
        'get_debit_credit_by_account'
    )
    accrued_amount = fields.Function(
        fields.Numeric('Monto devengado'),
        'get_amounts'
    )
    executed_amount = fields.Function(
        fields.Numeric('Monto ejecutado'),
        'get_amounts'
    )

    @classmethod
    def __setup__(cls):
        super(AnalyticAccount, cls).__setup__()
        cls.state.states['readonly'] = True
        cls.name.states['readonly'] = Eval('state') != 'draft'
        cls.name.depends.append('state')
        cls.code.states['readonly'] = Eval('state') != 'draft'
        cls.code.depends.append('state')
        cls.type.states['readonly'] = Eval('state') != 'draft'
        cls.type.depends.append('state')
        cls.root.states['readonly'] = Eval('state') != 'draft'
        cls.root.depends.append('state')
        cls.parent.states['readonly'] = Eval('state') != 'draft'
        cls.parent.depends.append('state')
        cls.childs.states['readonly'] = Eval('state') != 'draft'
        cls.childs.depends.append('state')
        cls.company.states['readonly'] = Eval('state') != 'draft'
        cls.company.depends.append('state')
        cls.display_balance.states['readonly'] = Eval('state') != 'draft'
        cls.display_balance.depends.append('state')
        cls.mandatory.states['readonly'] = Eval('state') != 'draft'
        cls.mandatory.depends.append('state')
        cls.distributions.states['readonly'] = Eval('state') != 'draft'
        cls.distributions.depends.append('state')
        cls.distribution_parents.states['readonly'] = Eval('state') != 'draft'
        cls.distribution_parents.depends.append('state')

        cls._error_messages.update({
            'parent_code_not_related': (
                'El código %(code)s debe iniciar con %(parent_code)s'),
            'code_format_invalid': (
                'El código %(code)s no coincide con el '
                'formato %(parent_code)sXX'),
            'not_code_pattern': (
                'No se encuentra definido el patron del código para la '
                'validación')
        })
        cls._transitions |= set((
            ('draft', 'opened'),
            ('opened', 'draft'),
            ('opened', 'closed'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~(Eval('state').in_(['opened'])),
            },
            'opened': {
                'invisible': ~(Eval('state').in_(['draft'])),
            },
            'closed': {
                'invisible': ~(Eval('state').in_(['opened'])),
            },
        })


    @classmethod
    def validate(cls, accounts):
        super(AnalyticAccount, cls).validate(accounts)
        cls.check_codes(accounts)

    @classmethod
    def check_codes(cls, accounts):
        pool = Pool()
        Configuration = pool.get('account.configuration')
        config = Configuration(1)
        if not config.code_pattern_analytic_account:
            cls.raise_user_error('not_code_pattern')

        def is_valid_format(code, parent_code):
            pattern = config.code_pattern_analytic_account
            if ((len(code.split('.')) != (len(parent_code.split('.')) + 1)) or
                    (len(code.split('.')[len(code.split('.')) - 1]) == 0) or
                    (re.search(pattern, code).group(0) != code)
            ):
                return False
            return True

        for account in accounts:
            if account.parent and account.code.find(account.parent.code + '.') != 0:
                cls.raise_user_error('parent_code_not_related', {
                    'code': account.code,
                    'parent_code': account.parent.code + '.',
                })
            if account.parent and not is_valid_format(account.code, account.parent.code):
                cls.raise_user_error('code_format_invalid', {
                    'code': account.code,
                    'parent_code': account.parent.code + '.',
                })

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, accounts):
        childs_of = []
        for account in accounts:
            childs_of += cls.search([
                ('parent', '=', account.id),
            ])
        if childs_of:
            cls.draft(childs_of)

    @classmethod
    @ModelView.button
    @Workflow.transition('opened')
    def opened(cls, accounts):
        childs_of = []
        for account in accounts:
            childs_of += cls.search([
                ('parent', '=', account.id),
            ])
        if childs_of:
            cls.opened(childs_of)

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def closed(cls, accounts):
        childs_of = []
        for account in accounts:
            childs_of += cls.search([
                ('parent', '=', account.id),
            ])
        if childs_of:
            cls.closed(childs_of)

    @fields.depends('debit_by_account', 'credit_by_account', 'display_balance')
    def on_change_with_balance_by_account(self, name=None):
        debit = (Decimal("0")
                 if self.debit_by_account is None else self.debit_by_account)
        credit = (Decimal("0")
                 if self.credit_by_account is None else self.credit_by_account)
        if self.display_balance == 'debit-credit':
            _balance = (debit - credit).quantize(Decimal("0.01"))
        else:
            _balance = (credit - debit).quantize(Decimal("0.01"))
        return _balance

    @classmethod
    def get_amounts(cls, accounts, names=None):
        accrued = defaultdict(lambda : Decimal("0"))
        executed = defaultdict(lambda : Decimal("0"))
        context = Transaction().context
        _domain = ('move_line.move.state', '=', 'posted'),
        if context.get('start_date'):
            _domain += ('date', '>=', context.get('start_date')),
        if context.get('end_date'):
            _domain += ('date', '<=', context.get('end_date')),
        if context.get('analytic_account'):
            _domain += ('account' , '=', context['analytic_account']),
        Line = Pool().get('analytic_account.line')
        lines = Line.search_read([_domain], fields_names=[
            'account', 'debit', 'credit', 'move_line.budget_operation',
            'move_line.account', 'move_line.budget'])
        if context.get('kind') and context.get('analytic_account') and (
                context.get('kind') == 'budget'):
            variable = 'move_line.budget'
        elif context.get('kind') and context.get('analytic_account') and (
                context.get('kind') == 'account'):
            variable = 'move_line.account'
        else:
            variable = 'account'
        for line in lines:
            if line['move_line.budget_operation'] == 'acc':
                if line.get(variable):
                    accrued[line[variable]] += (
                            line['debit'] + line['credit']).quantize(
                        Decimal("0.01"))
            if line['move_line.budget_operation'] == 'exe':
                if line.get(variable):
                    executed[line['account']] += (
                            line['debit'] + line['credit']).quantize(
                        Decimal("0.01"))
        return {
            'accrued_amount': accrued,
            'executed_amount': executed,
        }

    @classmethod
    def get_debit_credit_by_account(cls, accounts, names=None):
        value = defaultdict(lambda : Decimal("0"))
        context = Transaction().context
        Line = Pool().get('analytic_account.line')
        lines = Line.search_read([
            ('move_line.move.state', '=', 'posted'),
            ('move_line.account', '=', context.get('account_id')),
            ('company', '=', context.get('company')),
            ('account', '=', context.get('analytic_account_id')),
            ('date', '>=', context.get('start_date')),
            ('date', '<=', context.get('end_date')),
        ], fields_names=['debit', 'credit'])
        fname = ''
        field_name = ''
        for test in ['debit', 'credit']:
            for name in names:
                if test in name:
                    fname = name[:len(test)]
                    field_name = name
                    break
        value[context.get('analytic_account_id')] = reduce(
            lambda a,b: a+b, [x[fname] for x in lines])
        return {
            field_name: value,
        }

    @classmethod
    def get_balance(cls, accounts, name):
        pool = Pool()
        Line = pool.get('analytic_account.line')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        line = Line.__table__()
        move_line = MoveLine.__table__()
        move = Move.__table__()

        ids = [a.id for a in accounts]
        childs = cls.search([('parent', 'child_of', ids)])
        all_ids = list({}.fromkeys(ids + [c.id for c in childs]).keys())

        id2account = {}
        all_accounts = cls.browse(all_ids)
        for account in all_accounts:
            id2account[account.id] = account

        line_query = Line.query_get(line)
        cursor.execute(*table.join(
            line, 'LEFT',
            condition=table.id == line.account
            ).join(move_line, 'LEFT',
            condition=move_line.id == line.move_line
            ).join(move, 'LEFT',
            condition=move.id == move_line.move
            ).select(table.id,
            Sum(Coalesce(line.debit, 0) - Coalesce(line.credit, 0)),
            where=(table.type != 'view')
                 & (move.state == 'posted')
                 & table.id.in_(all_ids)
                 & (table.active == True) & line_query,
            group_by=table.id))
        account_sum = defaultdict(Decimal)
        for account_id, value in cursor.fetchall():
            account_sum.setdefault(account_id, Decimal('0.0'))
            # SQLite uses float for SUM
            if not isinstance(value, Decimal):
                value = Decimal(str(value))
            account_sum[account_id] += value

        balances = {}
        for account in accounts:
            balance = Decimal()
            childs = cls.search([
                ('parent', 'child_of', [account.id]),
            ])
            for child in childs:
                balance += account_sum[child.id]
            if account.display_balance == 'credit-debit' and balance:
                balance *= -1
            exp = Decimal(str(10.0 ** -account.currency_digits))
            balances[account.id] = balance.quantize(exp)
        return balances

    @classmethod
    def get_credit_debit(cls, accounts, names):
        pool = Pool()
        Line = pool.get('analytic_account.line')
        MoveLine = pool.get('account.move.line')
        Move = pool.get('account.move')
        cursor = Transaction().connection.cursor()
        table = cls.__table__()
        line = Line.__table__()
        move = Move.__table__()
        move_line = MoveLine.__table__()

        result = {}
        ids = [a.id for a in accounts]
        for name in names:
            if name not in ('credit', 'debit'):
                raise Exception('Bad argument')
            result[name] = {}.fromkeys(ids, Decimal('0.0'))

        id2account = {}
        for account in accounts:
            id2account[account.id] = account

        line_query = Line.query_get(line)
        columns = [table.id]
        for name in names:
            columns.append(Sum(Coalesce(Column(line, name), 0)))
        cursor.execute(*table.join(line, 'LEFT',
            condition=table.id == line.account
            ).join(move_line, 'LEFT',
            condition=move_line.id == line.move_line
            ).join(move, 'LEFT',
            condition=move.id == move_line.move
            ).select(*columns,
            where=(table.type != 'view')
                & (move.state == 'posted')
                & table.id.in_(ids)
                & (table.active == True) & line_query,
            group_by=table.id))

        for row in cursor.fetchall():
            account_id = row[0]
            for i, name in enumerate(names, 1):
                value = row[i]
                # SQLite uses float for SUM
                if not isinstance(value, Decimal):
                    value = Decimal(str(value))
                result[name][account_id] += value
        for account in accounts:
            for name in names:
                exp = Decimal(str(10.0 ** -account.currency_digits))
                result[name][account.id] = (
                    result[name][account.id].quantize(exp))
        return result


class AccountTemplate(ModelSQL, ModelView, DeactivableMixin):
    'Account Template'
    __name__ = 'analytic_account.template'

    _history = True

    name = fields.Char('Nombre', required=True)
    lines = fields.One2Many(
        'analytic_account.template.line', 'template', 'Detalle')
    total = fields.Function(fields.Numeric(
        'Total'), 'on_change_with_total'
    )
    accounts = fields.Many2Many(
        'analytic_account.template.account', 'template', 'account', 'Cuentas')
    budgets = fields.Many2Many(
        'analytic_account.template.budget', 'template', 'budget', 'Partidas')


    @classmethod
    def __setup__(cls):
        super(AccountTemplate, cls).__setup__()
        cls._order = [
            ('name', 'ASC')
        ]
        cls._error_messages.update({
            'hundred_percent': (
                'La suma de los porcentajes de las lineas debe estar entre 1 '
                'y 100.'),
        })

    @classmethod
    def validate(cls, templates):
        percentage = Decimal("0")
        for template in templates:
            for line in template.lines:
                percentage += line.percentage
        if percentage == Decimal("0") or percentage > Decimal("1"):
            cls.raise_user_error('hundred_percent')

    @fields.depends('lines')
    def on_change_with_total(self, name=None):
        total = Decimal("0")
        for line in self.lines:
            total += line.percentage
        return total


class AccountTemplateLine(ModelSQL, ModelView):
    'Account Template Line'
    __name__ = 'analytic_account.template.line'

    _history = True

    template = fields.Many2One(
        'analytic_account.template', 'Plantilla', required=True,
        ondelete='CASCADE'
    )
    percentage = fields.Numeric(
        'Porcentaje', domain=[
            ('percentage', '>=', Decimal("0")),
            ('percentage', '<=', Decimal("1")),
        ], required=True,
    )
    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica', required=True,
        domain=[
            ('state', '=', 'opened'),
            ('type', '=', 'normal'),
        ])

    @classmethod
    def __setup__(cls):
        super(AccountTemplateLine, cls).__setup__()
        cls._order = [
            ('percentage', 'DESC'),
            ('analytic_account', 'ASC')
        ]

    @staticmethod
    def default_percentage():
        return Decimal("0")

    @classmethod
    def get_match_rules(cls, template_id):
        match = cls.search([
            ('template', '=', int(template_id)),
        ])
        if not match:
            match = cls.search([
                ('template', '=', int(template_id)),
            ])
        return match


class AccountTemplateAccount(ModelSQL, ModelView):
    'Account Template Account'
    __name__ = 'analytic_account.template.account'

    account = fields.Many2One('account.account', 'Cuenta', required=True)
    template = fields.Many2One(
        'analytic_account.template', 'Plantilla', required=True)


class AccountTemplateBudget(ModelSQL, ModelView):
    'Account Template Budget'
    __name__ = 'analytic_account.template.budget'

    budget = fields.Many2One('public.budget', 'Partida', required=True)
    template = fields.Many2One(
        'analytic_account.template', 'Plantilla', required=True)


class AnalyticPeriod(Workflow, ModelSQL, ModelView):
    'Analytic Period'
    __name__ = 'analytic_account.period'

    _states = {
        'readonly': Eval('state')=='close',
    }
    _depends = ['state']

    state = fields.Selection([
        ('open', 'Abierto'),
        ('close', 'Cerrado'),
    ], 'Estado', required=True, states={'readonly': True})
    company = fields.Many2One(
        'company.company', 'Compañia', required=True, states={'readonly': True})
    name = fields.Char(
        'Nombre', required=True, states=_states, depends=_depends)
    start_date = fields.Date(
        'Fecha inicio', states=_states, depends=_depends+['end_date'],
        domain=[
            ('start_date', '<=', Eval('end_date'))
        ], required=True)
    end_date = fields.Date(
        'Fecha fin', states=_states, depends=_depends+['start_date'],
        domain=[
            ('end_date', '>=', Eval('start_date'))
        ], required=True)
    fiscalyear = fields.Many2One(
        'account.fiscalyear', 'Año fiscal', required=True,
        states={'readonly': False})

    @classmethod
    def __setup__(cls):
        super(AnalyticPeriod, cls).__setup__()
        cls._order = [
            ('company', 'ASC'),
            ('fiscalyear', 'DESC'),
            ('start_date', 'ASC'),
        ]
        cls._transitions |= set((
            ('open', 'close'),
            ('close', 'open'),
        ))
        cls._buttons.update({
            'open': {
                'invisible': ~(Eval('state').in_(['close'])),
            },
            'close': {
                'invisible': ~(Eval('state').in_(['open'])),
            },
        })
        cls._error_messages.update({
            'period_overlaps': (
                'El periodo %(period)s ya existe con las fechas desde %(start)s'
                ' hasta %(end)s')
        })

    @classmethod
    def validate(cls, periods):
        for period in periods:
            if period.state != 'close':
                period.check_period_validity()

    @staticmethod
    def default_state():
        return 'open'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_fiscalyear():
        Fiscalyear = Pool().get('account.fiscalyear')
        Date = Pool().get('ir.date')
        today = Date.today()
        fiscalyears = Fiscalyear.search([
            ('start_date', '<=', today),
            ('end_date', '>=', today)
        ])
        if fiscalyears:
            return fiscalyears[0].id
        return None

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, periods):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('close')
    def close(cls, periods):
        pass

    def check_period_validity(self):
        transaction = Transaction()
        connection = transaction.connection
        transaction.database.lock(connection, self._table)
        table = self.__table__()
        cursor = connection.cursor()
        where = ((((table.start_date <= self.start_date)
                   & (table.end_date >= self.start_date))
                  | ((table.start_date <= self.end_date)
                     & (table.end_date >= self.end_date))
                  | ((table.start_date >= self.start_date)
                     & (table.end_date <= self.end_date)))
                 & (table.state.in_(['open']))
                 & (table.id != self.id))
        cursor.execute(*table.select(table.id, where=where))
        period_id = cursor.fetchone()
        if period_id:
            overlapping_period = self.__class__(period_id[0])
            self.raise_user_error('period_overlaps', {
                'period': overlapping_period.name.upper(),
                'start': overlapping_period.start_date,
                'end': overlapping_period.end_date,
            })


class FiscalYear(metaclass=PoolMeta):
    __name__ = 'account.fiscalyear'

    analytic_periods = fields.One2Many(
        'analytic_account.period', 'fiscalyear', 'Periodos analiticos',
        states={
            'readonly': Eval('state')!='open',
        }, depends=['state'])


class GeneralLedgerAccountContext(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.account.context'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica',
        domain=[
            ('state', '=', 'opened'),
            ('type', '!=', 'root'),
            ('active', '=', True),
        ])

    @classmethod
    def default_from_date(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        fiscalyear = context.get('fiscalyear')
        if fiscalyear:
            fiscalyear = FiscalYear(fiscalyear).start_date
        return fiscalyear

    @classmethod
    def default_to_date(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        fiscalyear = context.get('fiscalyear')
        if fiscalyear:
            fiscalyear = FiscalYear(fiscalyear).end_date
        return fiscalyear

    @fields.depends('fiscalyear', 'start_period', 'end_period')
    def on_change_fiscalyear(self):
        if (self.start_period
                and self.start_period.fiscalyear != self.fiscalyear):
            self.start_period = None
        if (self.end_period
                and self.end_period.fiscalyear != self.fiscalyear):
            self.end_period = None
        if self.fiscalyear:
            self.from_date = self.fiscalyear.start_date
            self.to_date = self.fiscalyear.end_date
        else:
            self.from_date = None
            self.to_date = None

    @fields.depends('fiscalyear', 'start_period')
    def on_change_start_period(self):
        if self.start_period:
            self.from_date = self.start_period.start_date
        if self.start_period is None and self.fiscalyear:
            self.from_date = self.fiscalyear.start_date

    @fields.depends('fiscalyear', 'end_period')
    def on_change_end_period(self):
        if self.end_period:
            self.to_date = self.end_period.end_date
        if self.end_period is None and self.fiscalyear:
            self.to_date = self.fiscalyear.end_date

class GeneralLedgerAccount(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.account'

    @classmethod
    def table_query(cls):
        query = super(GeneralLedgerAccount, cls).table_query()
        context = Transaction().context
        if context.get('analytic_account'):
            pool = Pool()
            Line = pool.get('analytic_account.line')
            Move = pool.get('account.move')
            MoveLine = pool.get('account.move.line')
            move = Move.__table__()
            move_line = MoveLine.__table__()
            line = Line.__table__()
            where = Line.query_get(line)
            query_all = move.join(
                move_line, condition=move.id == move_line.move
            ).join(
                line, condition=line.move_line == move_line.id
            ).select(
                move_line.account,
                where=where & (move.state=='posted'),
            )
            return query.select(where=query.id.in_(query_all))
        return query

    # @classmethod
    # def get_debit_credit(cls, records, name):
    #     debit_credit = super(
    #         GeneralLedgerAccount, cls).get_debit_credit(records, name)
    #     context = Transaction().context
    #     if context.get('analytic_account'):
    #         pool = Pool()
    #         Line = pool.get('analytic_account.line')
    #         account_id = [x.id for x in records]
    #         conditional = [
    #             ('move_line.account', 'in', account_id),
    #             ('move_line.move.state', '=', 'posted')
    #         ]
    #         if context.get('from_date'):
    #             conditional.append(('date', '>=', context.get('from_date')))
    #         if context.get('to_date'):
    #             conditional.append(('date', '<=', context.get('to_date')))
    #         start_period_ids = cls.get_period_ids('start_%s' % name)
    #         end_period_ids = cls.get_period_ids('end_%s' % name)
    #         periods_ids = list(
    #             set(end_period_ids).difference(set(start_period_ids)))
    #         with Transaction().set_context(periods=periods_ids):
    #             lines = Line.search(conditional)
    #         values = dict.fromkeys([x.id for x in records], Decimal("0"))
    #         for line in lines:
    #             values[line.move_line.account.id] += getattr(line, name)
    #         return {a: values.get(a) for a in values}
    #     return debit_credit

    @classmethod
    def get_account(cls, records, name):
        context = Transaction().context
        if context.get('analytic_account') and 'balance' in name:
            values = dict.fromkeys([x.id for x in records], Decimal("0"))
            pool = Pool()
            Line = pool.get('analytic_account.line')
            FiscalYear = pool.get('account.fiscalyear')
            account_ids = [x.id for x in records]
            if context.get('from_date'):
                from_date = context.get('from_date')
            else:
                from_date = FiscalYear(context.get('fiscalyear')).start_date
            lines = Line.search([
                ('move_line.move.state', '=', 'posted'),
                ('move_line.account', 'in', account_ids),
                ('company', '=', context.get('company')),
                ('account', '=', context.get('analytic_account')),
                ('date', '<', from_date),
            ])
            for line in lines:
                values[line.move_line.account.id] += line.debit - line.credit
            return {
                a: values.get(a) for a in values
            }
        return super(GeneralLedgerAccount, cls).get_account(records, name)


class EnterCostCenterToAnalyticAccountStart(ModelView):
    'Enter Cost Center To Analytic Account Start'
    __name__ = 'enter.cost.center.to.analytic.account.start'

    accounts = fields.Boolean(
        'Migrar centros de costos?', states={'readonly': True})
    moves = fields.Boolean(
        'Migrar asientos contables?', states={'readonly': True})


class EnterCostCenterToAnalyticAccount(Wizard):
    'Enter Cost Center To Analytic Account'
    __name__ = 'enter.cost.center.to.analytic.account'

    start = StateView(
        'enter.cost.center.to.analytic.account.start',
        'analytic_account_ec.enter_cost_center_analytic_account_start_view_form',
        [
            Button('Cuentas analiticas', 'accounts', 'tryton-ok'),
            Button('Asientos contables', 'moves', 'tryton-ok'),
            Button('Finalizar', 'end', 'tryton-cancel', default=True),
        ]
    )

    accounts = StateTransition()
    moves = StateTransition()


    def default_start(self, fields):
        pool = Pool()
        AnalyticLine = pool.get('analytic_account.line')
        MoveLine = pool.get('account.move.line')
        move_lines = MoveLine.search([
            ('move.state', '=', 'posted'),
            ('cost_center', '!=', None),
        ])
        analytic_lines = AnalyticLine.search([
            ('move_line', 'in', [m.id for m in move_lines])
        ])
        CostCenter = pool.get('public.cost.center')
        AnalyticAccount = pool.get('analytic_account.account')
        cost_centers = CostCenter.search([])
        analytic_accounts = AnalyticAccount.search([
            ('name', 'in', [c.name for c in cost_centers]),
        ])
        defaults = {}
        defaults['accounts'] = True if analytic_accounts else getattr(
            self.start, 'accounts', False)
        defaults['moves'] = True if analytic_lines else getattr(
            self.start, 'moves', False)
        return defaults

    def transition_accounts(self):
        if self.start.accounts:
            return 'start'
        pool = Pool()
        CostCenter = pool.get('public.cost.center')
        AnalyticAccount = pool.get('analytic_account.account')
        cost_centers = CostCenter.search([])
        parents = defaultdict(lambda : None)
        for row in cost_centers:
            try:
                name = row.name.strip()
                code = row.code.strip()
                new = AnalyticAccount(
                    name=name,
                    code=code,
                    state='draft',
                    type='normal',
                    display_balance='debit-credit',
                )
                if len(new.code) == 3:
                    new.type = 'root'
                if row.parent:
                    new.parent = parents[row.parent.code.strip()]
                    new.root = parents[code[:3]]
                parents[code] = new
                new.save()
            except Exception:
                self.raise_user_error(
                    '''Por favor verificar las partidas superiores de los 
                    centros de costos, que se encuentre correcta la asignación.
                    ''')
            self.start.accounts = True
        return 'start'

    def transition_moves(self):
        if not self.start.accounts or self.start.moves:
            return 'start'
        pool = Pool()
        Period = pool.get('account.period')
        Account = pool.get('account.account')
        AnalyticAccount = pool.get('analytic_account.account')
        CostCenter = pool.get('public.cost.center')
        AnalyticLine = pool.get('analytic_account.line')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')

        dict_accounts = defaultdict(lambda : None)
        dict_moves = defaultdict(lambda : None)
        dict_cost_centers = defaultdict(lambda : None)
        to_save = []
        to_save_account = []

        periods = Period.search([
            ('fiscalyear.name', '=', '2020'),
        ])

        moves = Move.search_read([
            ('state', '=', 'posted'),
        ], fields_names=['id', 'date'])
        for move in moves:
            dict_moves[move['id']] = move['date']

        cost_centers = CostCenter.search_read([], fields_names=['id', 'code'])
        for row in cost_centers:
            dict_cost_centers[row['id']] = row['code'].strip()

        analytic_accounts = AnalyticAccount.search_read([
            ('active', '=', True),
        ], fields_names=['id', 'code'])
        for account in analytic_accounts:
            dict_accounts[account['code']] = account['id']

        move_lines = MoveLine.search([
            ('move.period', 'in', [p.id for p in periods]),
            ('move.state', '=', 'posted'),
            ('move.type', 'in', ['financial', 'adjustment']),
            ('cost_center', '!=', None),
        ])
        new_move_lines = []
        for line in move_lines:
            if line.budget_operation == 'acc':
                new_move_lines.append(line)
        accounts = Account.search_read([
            ('id', 'in', [m.account.id for m in new_move_lines]),
        ], fields_names=['id'])

        for account in accounts:
            new = Account(account['id'])
            new.analytic_account = True
            to_save_account.append(new)
        for line in new_move_lines:
            code = dict_cost_centers[line.cost_center.id]
            to_save.append(AnalyticLine(
                move_line = line,
                debit = line.debit,
                credit = line.credit,
                account = dict_accounts[code],
                date = dict_moves[line.move.id],
            ))
        Account.save(to_save_account)
        AnalyticLine.save(to_save)
        self.start.moves = True
        return 'start'


class AnalyticAccountContext(ModelView):
    'Analytic Account Start'
    __name__ = 'analytic_account.account.context'

    fiscalyear = fields.Many2One(
        'account.fiscalyear', 'Año fiscal', required=True)
    period = fields.Many2One(
        'account.period', 'Periodo',
        domain=[
            ('fiscalyear', '=', Eval('fiscalyear')),
        ], depends=['fiscalyear'])
    start_date = fields.Date(
        'Fecha de inicio',
        domain=[
            If(Eval('end_date') & Eval('start_date'),
               ('start_date', '<=', Eval('end_date')),
               ()),
            ],
        depends=['end_date'])
    end_date = fields.Date(
        'Fecha fin',
        domain=[
            If(Eval('start_date') & Eval('end_date'),
               ('end_date', '>=', Eval('start_date')),
               ()),
            ],
        depends=['start_date'])
    company = fields.Many2One('company.company', 'Compañia', required=True)

    @classmethod
    def default_fiscalyear(cls):
        pool = Pool()
        FiscalYear = pool.get('account.fiscalyear')
        context = Transaction().context
        return context.get(
            'fiscalyear',
            FiscalYear.find(context.get('company'), exception=False))

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @fields.depends('period')
    def on_change_period(self):
        if self.period:
            self.start_date = self.period.start_date
            self.end_date = self.period.end_date

    @fields.depends('fiscalyear', 'period')
    def on_change_fiscalyear(self):
        if (self.period
                and self.period.fiscalyear != self.fiscalyear):
            self.period = None


class AnalyticAccountContextLine(AnalyticAccountContext):
    'Analytic Account Start Line'
    __name__ = 'analytic_account.account.context.line'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica',
        domain=[
            ('type', '!=', 'root'),
            ('state', '=', 'opened'),
            ('active', '=', True)
        ])


class AnalyticAccountGeneral(ModelView, ModelSQL):
    'Analytic Account General'
    __name__ = 'analytic_account.account.general'

    name = fields.Char('Nombre')
    code = fields.Char('Codigo')
    accrued_amount = fields.Function(
        fields.Numeric('Monto devengado', digits=(16,2)),
        'get_account'
    )
    executed_amount = fields.Function(
        fields.Numeric('Monto ejecutado', digits=(16,2)),
        'get_account'
    )

    @classmethod
    def __setup__(cls):
        super(AnalyticAccountGeneral, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def get_account(cls, records, name):
        pool = Pool()
        Account = pool.get('analytic_account.account')
        accounts = Account.browse(records)
        return {a.id: getattr(a, name) for a in accounts}

    @classmethod
    def table_query(cls):
        pool = Pool()
        context = Transaction().context
        Account = pool.get('analytic_account.account')
        account = Account.__table__()
        columns = []
        for fname, field in cls._fields.items():
            if not hasattr(field, 'set'):
                columns.append(Column(account, fname).as_(fname))
        return account.select(
            *columns,
            where=(account.company == context.get('company'))
                  & (account.type != 'view'))


class AnalyticAccountGeneralLine(ModelView, ModelSQL):
    'AnalyticAccountGeneralLine'
    __name__ = 'analytic_account.account.general.line'

    name = fields.Char('Nombre')
    code = fields.Char('Codigo')
    accrued_amount = fields.Function(
        fields.Numeric('Monto devengado', digits=(16,2)),
        'get_account'
    )
    executed_amount = fields.Function(
        fields.Numeric('Monto ejecutado', digits=(16,2)),
        'get_account'
    )

    @classmethod
    def __setup__(cls):
        super(AnalyticAccountGeneralLine, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def get_account(cls, records, name):
        pool = Pool()
        Account = pool.get('analytic_account.account')
        with Transaction().set_context(kind='budget'):
            rows = Account.browse(records)
        return {a.id: getattr(a, name) for a in rows}

    @classmethod
    def table_query(cls):
        pool = Pool()
        context = Transaction().context
        Budget = pool.get('public.budget')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Line = pool.get('analytic_account.line')
        move = Move.__table__()
        move_line = MoveLine.__table__()
        line = Line.__table__()
        where = Line.query_get(line)
        query_all = move.join(
            move_line, condition=move.id == move_line.move
        ).join(
            line, condition=line.move_line == move_line.id
        ).select(
            move_line.budget,
            where=where & (move.state=='posted'),
        )
        budget = Budget.__table__()
        columns = []
        for fname, field in cls._fields.items():
            if not hasattr(field, 'set'):
                columns.append(Column(budget, fname).as_(fname))
        return budget.select(
            *columns,
            where=(budget.company == context.get('company'))
                  & (budget.type != 'view') & (budget.id.in_(query_all) ))


class AnalyticAccountGeneralComplete(AnalyticAccountGeneral):
    'Analytic Accuont General Complete'
    __name__ = 'analytic_account.account.general.complete'

    debit = fields.Function(
        fields.Numeric('Debito', digits=(16,2)),
        'get_data'
    )
    credit = fields.Function(
        fields.Numeric('Credito', digits=(16,2)),
        'get_data'
    )
    end_debit = fields.Function(
        fields.Numeric('Fin debito', digits=(16,2)),
        'get_data'
    )
    end_credit = fields.Function(
        fields.Numeric('Fin credito', digits=(16,2)),
        'get_data'
    )

    @classmethod
    def get_data(cls, records, name):
        pool = Pool()
        Account = pool.get('analytic_account.account')
        accounts = Account.browse(records)
        dict = {}
        if 'end' in name:
            fname = 'balance'
            if 'debit' in name:
                for row in accounts:
                    dict[row.id] = (
                        getattr(row, fname) if getattr(row, fname) > 0
                        else Decimal("0"))
            if 'credit' in name:
                for row in accounts:
                    dict[row.id] = (
                        abs(getattr(row, fname)) if getattr(row, fname) < 0
                        else Decimal("0"))
        else:
            for row in accounts:
                dict[row.id] = getattr(row, name)
        return dict


class GeneralLedgerLineContext(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.line.context'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica',
        domain=[
            ('active', '=', True),
        ],
        states={
            'invisible': True,
        })


class AccountCardContext(metaclass=PoolMeta):
    __name__ = 'account.account.card.context'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica',
        domain=[
            ('active', '=', True),
        ],
        states={
            'invisible': True,
        })


class AnalyticAccountGeneralCompleteLine(ModelView, ModelSQL):
    'Analytic Account General Complete Line'
    __name__ = 'analytic_account.account.general.complete.line'

    name = fields.Char('Nombre')
    code = fields.Char('Codigo')
    accrued_amount = fields.Function(
        fields.Numeric('Monto devengado', digits=(16,2)),
        'get_account'
    )
    executed_amount = fields.Function(
        fields.Numeric('Monto ejecutado', digits=(16,2)),
        'get_account'
    )
    debit = fields.Function(
        fields.Numeric('Debito', digits=(16,2)),
        'get_debit_credit'
    )
    credit = fields.Function(
        fields.Numeric('Credito', digits=(16,2)),
        'get_debit_credit'
    )
    end_debit = fields.Function(
        fields.Numeric('Fin debito', digits=(16,2)),
        'on_change_with_end_debit'
    )
    end_credit = fields.Function(
        fields.Numeric('Fin credito', digits=(16,2)),
        'on_change_with_end_credit'
    )

    @classmethod
    def __setup__(cls):
        super(AnalyticAccountGeneralCompleteLine, cls).__setup__()
        cls._order.insert(0, ('code', 'ASC'))
        cls._order.insert(1, ('name', 'ASC'))

    @classmethod
    def get_account(cls, records, name):
        pool = Pool()
        Account = pool.get('analytic_account.account')
        with Transaction().set_context(kind='account'):
            rows = Account.browse(records)
        return {a.id: getattr(a, name) for a in rows}

    @classmethod
    def get_debit_credit(cls, records, name):
        debit = defaultdict(lambda : Decimal("0"))
        credit = defaultdict(lambda : Decimal("0"))
        context = Transaction().context
        ids = [x.id for x in records]
        _domain = [('move_line.move.state', '=', 'posted'),
                   ('move_line.account', 'in', ids)]
        if context.get('start_date'):
            _domain += ('date', '>=', context.get('start_date')),
        if context.get('end_date'):
            _domain += ('date', '<=', context.get('end_date')),
        if context.get('analytic_account'):
            _domain += ('account' , '=', context['analytic_account']),
        Line = Pool().get('analytic_account.line')
        lines = Line.search_read([_domain], fields_names=[
            'account', 'debit', 'credit', 'move_line.budget_operation',
            'move_line.account', 'move_line.budget'])
        variable = 'move_line.account'
        for line in lines:
            if line.get(variable):
                debit[line[variable]] += (line['debit']).quantize(
                    Decimal("0.01"))
                credit[line[variable]] += (line['credit']).quantize(
                    Decimal("0.01"))
        if name == 'debit':
            return debit
        else:
            return credit

    @fields.depends('debit', 'credit')
    def on_change_with_end_debit(self, name=None):
        if not Transaction().context.get('analytic_account'):
            return Decimal("0.00")
        debit = Decimal("0") if self.debit is None else self.debit
        credit = Decimal("0") if self.credit is None else self.credit
        Account = Pool().get('analytic_account.account')
        account = Account(Transaction().context.get('analytic_account'))
        if account.display_balance == 'debit-credit':
            _balance = (debit - credit).quantize(Decimal("0.01"))
        else:
            _balance = Decimal("0.00")
        return _balance

    @fields.depends('debit', 'credit')
    def on_change_with_end_credit(self, name=None):
        if not Transaction().context.get('analytic_account'):
            return Decimal("0.00")
        debit = Decimal("0") if self.debit is None else self.debit
        credit = Decimal("0") if self.credit is None else self.credit
        Account = Pool().get('analytic_account.account')
        account = Account(Transaction().context.get('analytic_account'))
        if account.display_balance == 'debit-credit':
            _balance = Decimal("0.00")
        else:
            _balance = abs((credit - debit).quantize(Decimal("0.01")))
        return _balance

    @classmethod
    def table_query(cls):
        pool = Pool()
        context = Transaction().context
        Account = pool.get('account.account')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Line = pool.get('analytic_account.line')
        move = Move.__table__()
        move_line = MoveLine.__table__()
        line = Line.__table__()
        where = Line.query_get(line)
        query_all = move.join(
            move_line, condition=move.id == move_line.move
        ).join(
            line, condition=line.move_line == move_line.id
        ).select(
            move_line.account,
            where=where & (move.state=='posted'),
        )
        account = Account.__table__()
        columns = []
        for fname, field in cls._fields.items():
            if not hasattr(field, 'set'):
                columns.append(Column(account, fname).as_(fname))
        return account.select(
            *columns,
            where=(account.company == context.get('company'))
                  & (account.kind != 'view') & (account.id.in_(query_all) ))