from collections import defaultdict
from sql import Union, Literal
from sql.operators import Concat
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.wizard import StateReport
from trytond.tools import cursor_dict

from trytond.modules.public_ec.account_card import AccountCard
from trytond.modules.public_ec.report import FinancialReport


__all__ = [
    'AccountCardGenerateStart', 'AccountCardGenerate', 'AnalyticAccountCard',
    'FinancialReportGenerateESFStart', 'FinancialReportESF',
    'FinancialReportGenerateStart', 'FinancialReportERE',
    'FinancialReportEFE'
]


class AccountCardGenerateStart(metaclass=PoolMeta):
    __name__ = 'account.account.card.generate.start'

    analytic_account = fields.Boolean('Generar con cuentas analiticas?')

    @classmethod
    def __setup__(cls):
        super(AccountCardGenerateStart, cls).__setup__()
        cls.process.states.update({
            'readonly': Bool(Eval('analytic_account')),
        })

    @staticmethod
    def default_analytic_account():
        return False

    @fields.depends('analytic_account')
    def on_change_analytic_account(self):
        if self.analytic_account:
            self.process = 'print_'


class AccountCardGenerate(metaclass=PoolMeta):
    __name__ = 'account.account.card.generate'

    print_analytic = StateReport('account.analytic.account.card')

    @classmethod
    def __setup__(cls):
        super(AccountCardGenerate, cls).__setup__()

    def transition_enter(self):
        Transaction().set_context(print_analytic=self.start.analytic_account)
        if self.start.analytic_account:
            return 'print_analytic'
        return super(AccountCardGenerate, self).transition_enter()

    def get_instance(self):
        if self.start.analytic_account:
            return Pool().get('account.analytic.account.card')
        return super(AccountCardGenerate, self).get_instance()

    def get_columns(self, account_card):
        columns = super(AccountCardGenerate, self).get_columns(account_card)
        if self.start.analytic_account:
            columns += [
                account_card.analytic_account,
                account_card.analytic_code
            ]
        return columns


    def do_print_analytic(self, action):
        account_data = self.calc_accounts()
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'accounts': account_data['lines'],
            'low_level': self.start.low_level,
        }
        return action, data


class AnalyticAccountCard(AccountCard):
    'Analytic Account Card'
    __name__ = 'account.analytic.account.card'

    analytic_code = fields.Char('Cuenta analitica')

    @classmethod
    def table_query(cls):
        query_original = super(AnalyticAccountCard, cls).table_query()
        context = Transaction().context
        pool = Pool()
        Line = pool.get('analytic_account.line')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        AnalyticAccount = pool.get('analytic_account.account')
        analytic_account = AnalyticAccount.__table__()
        move = Move.__table__()
        move_line = MoveLine.__table__()
        line = Line.__table__()
        where = Line.query_get(line)
        query_accounts = move.join(
            move_line, condition=move.id == move_line.move
        ).join(
            line, condition=line.move_line == move_line.id
        ).join(
            analytic_account, condition=analytic_account.id == line.account,
        ).select(
            move_line.account.as_('id'),
            analytic_account.id.as_('analytic_account'),
            Concat(Concat(analytic_account.code, ' - '),
                   analytic_account.name).as_('analytic_code'),
            (Literal(context.get('level'))
             if context.get('level') else Literal(0)).as_('level'),
            where=where & (move.state=='posted'),
            group_by=[
                move_line.account,
                analytic_account.id,
            ]
        )
        query = query_original.join(
            query_accounts,
            condition=query_original.id == query_accounts.id
        ).select(
            query_original.id,
            query_original.create_date,
            query_original.create_uid,
            query_original.write_date,
            query_original.write_uid,
            query_original.name,
            query_original.code,
            query_original.company,
            query_original.type,
            query_accounts.level,
            Literal(0).as_('debit'),
            Literal(0).as_('credit'),
            Literal(0).as_('start_debit'),
            Literal(0).as_('end_debit'),
            Literal(0).as_('start_credit'),
            Literal(0).as_('end_credit'),
            Literal(0).as_('start_balance'),
            Literal(0).as_('end_balance'),
            query_accounts.analytic_code,
            query_accounts.analytic_account,
        )
        union = Union(query_original, query, all_=True)
        query_final = union.select(
            order_by=[union.code.asc, union.analytic_account.asc]
        )
        return query_final

    def get_columns(cls, table_a, table_c):
        # columns = super(AnalyticAccountCard, cls).get_columns(table_a, table_c)
        # if Transaction().context.get('print_analytic'):
        #     columns += [
        #         Literal('0').as_('analytic_code'),
        #         Literal(0).as_('analytic_account'),
        #     ]
        # return columns
        return [
            table_a.id,
            table_a.create_date,
            table_a.create_uid,
            table_a.write_date,
            table_a.write_uid,
            table_a.name,
            table_a.code,
            table_a.company,
            table_a.type,
            table_a.level,
            Sum(Coalesce(table_c.debit, 0)).as_('debit'),
            Sum(Coalesce(table_c.credit, 0)).as_('credit'),
            Sum(Coalesce(table_c.start_debit, 0)).as_('start_debit'),
            Sum(Coalesce(table_c.end_debit, 0)).as_('end_debit'),
            Sum(Coalesce(table_c.start_credit, 0)).as_('start_credit'),
            Sum(Coalesce(table_c.end_credit, 0)).as_('end_credit'),
            Sum(Coalesce(table_c.start_balance, 0)).as_('start_balance'),
            Sum(Coalesce(table_c.end_balance, 0)).as_('end_balance'),
            Literal('0').as_('analytic_code'),
            Literal(0).as_('analytic_account'),
        ]


class FinancialReportGenerateESFStart(metaclass=PoolMeta):
    __name__ = 'account.financial.report.generate.esf.start'

    analytic_account = fields.Boolean('Generar con cuentas analiticas?')

    @classmethod
    def __setup__(cls):
        super(FinancialReportGenerateESFStart, cls).__setup__()
        cls.process.states.update({
            'readonly': Bool(Eval('analytic_account')),
        })

    @staticmethod
    def default_analytic_account():
        return False

    @fields.depends('analytic_account')
    def on_change_analytic_account(self):
        if self.analytic_account:
            self.process = 'print_'


class AnalyticFinancialReportMixin(object):

    def calc_analytics(self, fiscalyear, start_date, end_date, level=5):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        AccountCard = pool.get('account.analytic.account.card')
        AnalyticAccount = Pool().get('analytic_account.account')
        account_lines = []
        with Transaction().set_context(
                fiscalyear=fiscalyear, from_date=start_date, to_date=end_date,
                start_date=start_date, end_date=end_date, posted=True):
            account_card = AccountCard.table_query()
            if account_card:
                where = (account_card.level <= level)
                query = account_card.select(
                    account_card.id,
                    account_card.code,
                    account_card.name,
                    account_card.level,
                    account_card.start_debit,
                    account_card.start_credit,
                    account_card.debit,
                    account_card.credit,
                    account_card.end_debit,
                    account_card.end_credit,
                    account_card.start_balance,
                    account_card.end_balance,
                    account_card.analytic_code,
                    account_card.analytic_account,
                    where=where,
                    order_by=[account_card.code.asc]
                )
                cursor.execute(*query)
                for row in cursor_dict(cursor):
                    if row.get('analytic_code') != '0':
                        with Transaction().set_context(
                                account_id=row.get('id'),
                                analytic_account_id=row.get('analytic_account')):
                            analytic_account = AnalyticAccount(
                                row.get('analytic_account'))
                            _balance = analytic_account.balance_by_account
                            row['start_balance'] = _balance
                            row['end_balance'] = _balance
                            text = account_lines[len(account_lines)-1]['code']
                            index = (len(text)
                                     if text.find("-") == -1
                                     else text.find("-"))
                            row['code'] = (
                                f"{text[:index]} - {row['analytic_code']}")
                            if _balance > 0:
                                row['start_debit'] = _balance
                            else:
                                row['start_credit'] = abs(_balance)
                            row['debit'] = analytic_account.debit_by_account
                            row['credit'] = analytic_account.credit_by_account
                            if _balance > 0:
                                row['end_debit'] = _balance
                            else:
                                row['end_credit'] = abs(_balance)
                    account_lines.append(row)
            return {
                'lines': account_lines,
            }

class FinancialReportESF(AnalyticFinancialReportMixin, metaclass=PoolMeta):
    __name__ = 'account.financial.report.esf'

    print_analityc = StateReport('account.analytic.financial.report.esf')

    def transition_enter(self):
        if self.start.analytic_account:
            return 'print_analityc'
        return super(FinancialReportESF, self).transition_enter()

    def do_print_analityc(self, action):
        with Transaction().set_context(exclude_close_moves=True,
                                       level=self.start.level):
            account_data = self.calc_analytics(self.start.fiscalyear.id,
                self.start.start_date, self.start.end_date,
                level=self.start.level)
            account_data_close = self.calc_analytics(self.start.fiscalyear.id,
                self.start.start_date, self.start.end_date,
                level=self.start.level)
            previous_fiscalyear = None
            previous_account_data = {
                'lines': defaultdict(lambda: {}),
            }
            previous_account_data_close = {
                'lines': defaultdict(lambda: {}),
            }
            if self.start.comparison:
                previous_fiscalyear = self.start.previous_fiscalyear.id
                previous_account_data = self.calc_analytics(
                    self.start.previous_fiscalyear.id,
                    self.start.previous_start_date,
                    self.start.previous_end_date, level=self.start.level)
                previous_account_data_close = self.calc_analytics(
                    self.start.previous_fiscalyear.id,
                    self.start.previous_start_date,
                    self.start.previous_end_date, level=self.start.level)
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'previous_start_date': self.start.previous_start_date,
            'previous_end_date': self.start.previous_end_date,
            'accounts': account_data['lines'],
            'previous_fiscalyear': previous_fiscalyear,
            'previous_accounts': previous_account_data['lines'],
            'accounts_close': account_data_close['lines'],
            'previous_accounts_close': previous_account_data_close['lines'],
        }
        return action, data


class FinancialReportGenerateStart(metaclass=PoolMeta):
    __name__ = 'account.financial.report.generate.start'

    analytic_account = fields.Boolean('Generar con cuentas analiticas?')

    @staticmethod
    def default_analytic_account():
        return False


class FinancialReportERE(AnalyticFinancialReportMixin, metaclass=PoolMeta):
    __name__ = 'account.financial.report.ere'

    print_analityc = StateReport('account.analytic.financial.report.ere')

    def transition_enter(self):
        if self.start.analytic_account:
            return 'print_analityc'
        return super(FinancialReportERE, self).transition_enter()

    def do_print_analityc(self, action):
        with Transaction().set_context(exclude_close_moves=True,
                                       level=self.start.level):
            account_data = self.calc_analytics(
                self.start.fiscalyear.id, self.start.start_date,
                self.start.end_date, level=self.start.level)
            previous_fiscalyear = None
            previous_account_data = {
                'lines': [],
            }
            if self.start.comparison:
                previous_fiscalyear = self.start.previous_fiscalyear.id
                previous_account_data = self.calc_analytics(
                    self.start.previous_fiscalyear.id,
                    self.start.previous_start_date,
                    self.start.previous_end_date, level=self.start.level)
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'accounts': account_data['lines'],
            'previous_fiscalyear': previous_fiscalyear,
            'previous_accounts': previous_account_data['lines'],
        }
        return action, data


class FinancialReportEFE(AnalyticFinancialReportMixin, metaclass=PoolMeta):
    __name__ = 'account.financial.report.efe'

    print_analityc = StateReport('account.analytic.financial.report.efe')

    def transition_enter(self):
        if self.start.analytic_account:
            return 'print_analityc'
        return super(FinancialReportEFE, self).transition_enter()

    def do_print_analityc(self, action):
        with Transaction().set_context(exclude_close_moves=True,
                                       level=self.start.level):
            account_data = self.calc_analytics(
                self.start.fiscalyear.id, self.start.start_date,
                self.start.end_date, level=self.start.level)
            previous_fiscalyear = None
            previous_account_data = {
                'lines': defaultdict(lambda: {}),
            }
            if self.start.comparison:
                previous_fiscalyear = self.start.previous_fiscalyear.id
                previous_account_data = self.calc_analytics(
                    self.start.previous_fiscalyear.id,
                    self.start.previous_start_date,
                    self.start.previous_end_date, level=self.start.level)
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'accounts': account_data['lines'],
            'previous_fiscalyear': previous_fiscalyear,
            'previous_accounts': previous_account_data['lines'],
        }
        return action, data