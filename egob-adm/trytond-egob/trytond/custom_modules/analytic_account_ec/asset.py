from collections import defaultdict
from datetime import datetime

from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.tools import (grouped_slice, reduce_ids, cursor_dict, Literal,
    file_open)

__all__ = [
    'Asset', 'AnalyticAsset'
]


class Asset(metaclass=PoolMeta):
    __name__ = 'asset'

    analytic_accounts = fields.Many2Many(
        'analytic_account.asset', 'asset', 'analytic_account',
        'Cuentas analiticas',
        domain=[
            ('state', '=', 'opened'),
            ('type', '=', 'normal')
        ],
        states={
            'readonly': Eval('state')!='draft',
        }, depends=['state'])
    
    analytic_accounts_search = fields.Function(fields.One2Many(
        'analytic_account.account', None,
        'Cuentas analiticas', states={'invisible': True}),
        'get_analytic_accounts', searcher='search_data'
    )
    
    @classmethod
    def get_analytic_accounts(cls, assets, names):
        pool = Pool()
        cursor = Transaction().connection.cursor()
        ids = [a_.id for a_ in assets]
        table = cls.__table__()
        Account = pool.get('analytic_account.account')
        account = Account.__table__()
        Relation = pool.get('analytic_account.asset')
        relation = Relation.__table__()
        accounts = defaultdict(lambda: [])
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(relation,
                condition=table.id == relation.asset
            ).join(account,
                condition=relation.analytic_account == account.id
            ).select(
                table.id,
                account.id,
                where=red_sql,
            )
            cursor.execute(*query)
            for asset_id, account_id in cursor.fetchall():
                accounts[asset_id].append(account_id)
        return {
            'analytic_accounts_search': accounts
        }
    
    @classmethod
    def search_data(cls, name, clause):
        domain = tuple(clause[1:])
        pool = Pool()
        ids = []
        if 'analytic_accounts_search' in name:
            table = cls.__table__()
            Account = pool.get('analytic_account.account')
            account = Account.__table__()
            Relation = pool.get('analytic_account.asset')
            relation = Relation.__table__()
            cursor = Transaction().connection.cursor()
            query = table.join(relation,
                condition=table.id == relation.asset
            ).join(account,
                condition=((relation.analytic_account == account.id)
                & (account.code.like(clause[2])))
            ).select(
                table.id,
            )
            cursor.execute(*query)
            for row in cursor.fetchall():
                ids.append(row[0])
        return [('id', 'in', ids)]


class AnalyticAsset(ModelSQL, ModelView):
    'Analytic Asset'
    __name__ = 'analytic_account.asset'

    analytic_account = fields.Many2One(
        'analytic_account.account', 'Cuenta analitica', required=True)
    asset = fields.Many2One(
        'asset', 'Activo', required=True)