from decimal import Decimal
from collections import OrderedDict

from trytond.pool import Pool
from trytond.transaction import Transaction

from trytond.modules.hr_ec.company import CompanyReportSignature

import pandas as pd

__all__ = [
    'AnalyticFinancialReportERE', 'AccountCardAnalytic',
    'AccountAnalyticFinancialESF', 'AnalyticFinancialReportEFE'
]


CENT_ = Decimal('0.01')


class AccountValue(object):
    code = None
    name = None
    analytic_account = None
    analytic_code = None
    level = None
    start_debit = Decimal('0.00')
    start_credit = Decimal('0.00')
    debit = Decimal('0.00')
    credit = Decimal('0.00')
    sum_debit = Decimal('0.00')
    sum_credit = Decimal('0.00')
    end_debit = Decimal('0.00')
    end_credit = Decimal('0.00')

    def __init__(self, account=None):
        if account:
            self.code = account.get('code')
            self.name = account.get('name')
            self.analytic_account = account.get('analytic_account')
            self.analytic_code = account.get('analytic_code')
            self.level = account.get('level')
            if account.get('start_balance') > 0:
                self.start_debit = account['start_balance']
            else:
                self.start_credit = abs(account['start_balance'])
            self.debit = account['debit'] or Decimal('0.00')
            self.credit = account['credit'] or Decimal('0.00')
            _balance_analytic_account = Decimal("0")
            if account.get('analytic_code') != '0':
                AnalyticAccount = Pool().get('analytic_account.account')
                with Transaction().set_context(
                        account_id=account.get('id'),
                        analytic_account_id=account.get('analytic_account')):
                    analytic_account = AnalyticAccount(
                        account.get('analytic_account'))
                    _balance_analytic_account = analytic_account.balance_by_account
                    self.start_debit = Decimal("0")
                    self.start_credit = Decimal("0")
                    self.debit += analytic_account.debit_by_account
                    self.credit += analytic_account.credit_by_account
            self.sum_debit = self.start_debit + self.debit
            self.sum_credit = self.start_credit + self.credit
            if account.get('end_balance') > 0:
                self.end_debit = account['end_balance']
            else:
                self.end_credit = abs(account['end_balance'])
            if account.get('analytic_code') != '0':
                if _balance_analytic_account > 0:
                    self.end_debit = _balance_analytic_account
                else:
                    self.end_credit = abs(_balance_analytic_account)
            fields = ['start_debit', 'start_credit', 'debit', 'credit',
                      'sum_debit', 'sum_credit', 'end_debit', 'end_credit']
            for field in fields:
                setattr(self, field, getattr(self, field).quantize(CENT_))


def get_account_values(accounts):
    values = []
    for acc in accounts:
        values.append(AccountValue(acc))
    return values


def get_account_totals(accounts, level):
    total = AccountValue()
    fields = ['start_debit', 'start_credit', 'debit', 'credit', 'sum_debit',
              'sum_credit', 'end_debit', 'end_credit']
    for acc in accounts:
        if acc.level == level:
            for field in fields:
                setattr(
                    total, field, getattr(total, field) + getattr(acc, field))
    return total


class AccountCardAnalytic(CompanyReportSignature):
    __name__ = 'account.analytic.account.card'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Fiscalyear = pool.get('account.fiscalyear')
        report_context = super(AccountCardAnalytic, cls).get_context(
            records, data)
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        with Transaction().set_context(
                start_date=data['start_date'], end_date=data['end_date']):
            report_context['accounts'] = get_account_values(data['accounts'])
        report_context['counter'] = get_account_totals(
            report_context['accounts'], level=data['low_level'])
        return report_context


class FinancialReport(CompanyReportSignature):

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Fiscalyear = pool.get('account.fiscalyear')
        report_context = super(FinancialReport, cls).get_context(
            records, data)
        report_context['fiscalyear'] = Fiscalyear(data['fiscalyear'])
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['accounts'] = pd.DataFrame.from_records(data['accounts'])

        report_context['previous_fiscalyear'] = Fiscalyear(
            data['previous_fiscalyear'])
        report_context['previous_accounts'] = pd.DataFrame.from_records(
            data['previous_accounts'])
        report_context['have_data'] = cls._have_data
        report_context['sum_data'] = cls._sum_data

        report_context['accounts_close'] = pd.DataFrame.from_records(
            data.get('accounts_close', {}))
        report_context['previous_accounts_close'] = pd.DataFrame.from_records(
            data.get('previous_accounts_close', {}))
        report_context['get_period_result'] = cls._get_period_result
        return report_context

    @classmethod
    def _get_period_result(cls, fiscalyear, start_date, end_date):
        # TODO: calculate the result of the current year
        # instead search 618.03 account
        pool = Pool()
        AccountCard = pool.get('account.analytic.account.card')
        if fiscalyear:
            with Transaction().set_context(
                    fiscalyear=fiscalyear, from_date=start_date,
                    to_date=end_date, posted=True):
                data = AccountCard.search([('code', '=', '618.03')])
                return data[0].end_balance if data else 0

        return 0

    @classmethod
    def _have_data(cls, code, fields, accounts, prev_accounts):

        def get_data(code, fields, acc, result=OrderedDict(), type_='actual'):
            if not acc.empty:
                query = acc.query('code.str.startswith("%s")' % (code))
                if not query.empty:
                    for item in query.itertuples():
                        for field in fields:
                            if getattr(item, field) != 0:
                                temp = item._asdict()
                                if type_ == 'actual':
                                    result[item.code] = temp
                                else:
                                    if result.get(item.code):
                                        result[item.code]['previous'] = temp
                                    else:
                                        result[item.code] = temp.copy()
                                        fields = ['code', 'name']
                                        for key in result[item.code]:
                                            if key not in fields:
                                                result[item.code][key] = 0
                                        result[item.code]['previous'] = temp
            return result

        result = get_data(code, fields, accounts)
        if not prev_accounts.empty:
            result = get_data(code, fields, prev_accounts, result, 'previous')
        return result

    @classmethod
    def _sum_data(cls, accounts, codes, field):
        total = 0
        if not accounts.empty:
            for code in codes:
                accounts_query = accounts.query(
                    'analytic_code == "0" and code == "%s"' % (code))
                if not accounts_query.empty:
                    total += accounts_query[field].values[0]
        return total


class AccountAnalyticFinancialESF(FinancialReport):
    __name__ = 'account.analytic.financial.report.esf'


class AnalyticFinancialReportERE(FinancialReport):
    __name__ = 'account.analytic.financial.report.ere'


class AnalyticFinancialReportEFE(FinancialReport):
    __name__ = 'account.analytic.financial.report.efe'