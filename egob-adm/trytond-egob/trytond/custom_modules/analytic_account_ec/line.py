from functools import reduce
from decimal import Decimal
from collections import defaultdict

from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.model import ModelView, ModelSQL, fields, Check
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction


__all__ = [
    'MoveLine', 'EnterMoveAnalyticAccount', 'EnterMoveAnalyticAccountStart',
    'MoveLineAccountTemplate', 'Line', 'MoveLineAccountTemplateLine', 'Move'
]


class MoveLineAccountTemplateLine(ModelSQL, ModelView):
    'Move Line Account Template Line'
    __name__ = 'account.move.line.analytic_account.template.line'

    move_template = fields.Many2One(
        'account.move.line.analytic_account.template', 'Move template',
        ondelete='CASCADE')
    move_line = fields.Many2One(
        'account.move.line', 'Lineas de asiento')


class MoveLineAccountTemplate(ModelSQL, ModelView):
    'Move Line Account Template'
    __name__ = 'account.move.line.analytic_account.template'

    account = fields.Many2One(
        'account.account', 'Cuenta', required=True, states={'readonly': True})
    budget = fields.Many2One(
        'public.budget', 'Partida', states={'readonly': True})
    debit = fields.Numeric('Debito', required=True, states={'readonly': True})
    credit = fields.Numeric('Credito', required=True, states={'readonly': True})
    kind = fields.Selection([
        ('manual', 'Manual'),
        ('template', 'Plantilla'),
    ], 'Modo', required=True)
    templates = fields.Function(fields.One2Many(
        'analytic_account.template', None, 'Plantillas'),
        'on_change_with_templates')
    template = fields.Many2One(
        'analytic_account.template', 'Plantilla',
        domain=[
            ('id', 'in', Eval('templates')),
        ],
        states={
            'readonly': Eval('kind')=='manual',
        }, depends=['kind', 'templates'])
    ready = fields.Boolean('Listo?')
    lines = fields.One2Many(
        'account.move.line.analytic_account.template.line', 'move_template',
        'Lineas de asiento')

    @staticmethod
    def default_kind():
        return 'manual'

    @staticmethod
    def default_ready():
        return False

    @staticmethod
    def default_debit():
        return Decimal("0")

    @staticmethod
    def default_credit():
        return Decimal("0")

    @fields.depends('template', 'kind')
    def on_change_kind(self):
        if self.kind == 'manual':
            self.template = None

    @fields.depends('account', 'budget')
    def on_change_with_templates(self, name=None):
        pool = Pool()
        Template = pool.get('analytic_account.template')
        templates = Template.search(['OR',
            ('accounts', 'in', [self.account.id]),
            ('budgets', 'in', [self.budget.id if self.budget else None]),
        ])
        if not templates:
            templates = Template.search([])
        return [x.id for x in templates]


class EnterMoveAnalyticAccountStart(ModelView):
    'Enter Move Analytic Account Start'
    __name__ = 'move.analytic.account.enter.start'


    template = fields.Many2One(
        'analytic_account.template',
        'Seleccionar la plantilla para todas las lineas',
        states={
            'invisible': Eval('state')!='selection',
        }, depends=['templates'])
    state = fields.Selection([
        ('selection', 'Seleccion'),
        ('manual', 'Manual'),
        ('all', 'All'),
    ], 'Estado', required=True, states={'invisible': True})
    lines = fields.One2Many(
        'account.move.line.analytic_account.template', None,  'Lineas',
        states={
            'invisible': Eval('state')!='selection',
            'readonly': True,
        }, depends=['state'])
    current_line = fields.Many2One(
        'account.move.line.analytic_account.template', 'Linea de cuenta/partida',
        states={
            'invisible': True,
            'readonly': True,
        }, depends=['state'])
    account = fields.Function(fields.Many2One(
        'account.account', 'Cuenta',
        states={
            'invisible': Eval('state')!='manual',
            'readonly': True,
        }, depends=['state']), 'on_change_with_account')
    budget = fields.Function(fields.Many2One(
        'public.budget', 'Partida',
        states={
            'invisible': Eval('state')!='manual',
            'readonly': True,
        }, depends=['state']), 'on_change_with_budget')
    debit = fields.Function(fields.Numeric(
        'Debito',
        states={
            'invisible': Eval('state')!='manual',
        }, depends=['state']),
        'on_change_with_debit')
    credit = fields.Function(fields.Numeric(
        'Credito',
        states={
            'invisible': Eval('state')!='manual',
        }, depends=['state']),
        'on_change_with_credit')
    analytic_accounts_temp = fields.One2Many(
        'analytic_account.template.line', None, 'Contabilidad analitica',
        domain=[
            ('template', '=', -1),
        ],
        states={
            'invisible': Eval('state')!='manual',
            'required': Eval('state')=='manual',
        }, depends=['state'])
    analytic_accounts = fields.One2Many(
        'analytic_account.line', None, 'Contabilidad analitica',
        domain=[
            ('move_line.move', 'in', Eval('context', {}).get('active_ids', [])),
            ('move_line.account.analytic_account', '=', True),
        ],
        states={
            'invisible': Eval('state')!='all',
        }, depends=['state'])

    @fields.depends('template', 'lines')
    def on_change_template(self):
        if self.template and self.lines:
            for line in self.lines:
                if self.template in line.templates:
                    line.kind = 'template'
                    line.template = self.template

    @fields.depends('current_line')
    def on_change_with_debit(self, name=None):
        debit = Decimal("0")
        if self.current_line:
            debit += self.current_line.debit
        return debit

    @fields.depends('current_line')
    def on_change_with_credit(self, name=None):
        credit = Decimal("0")
        if self.current_line:
            credit += self.current_line.credit
        return credit

    @fields.depends('current_line')
    def on_change_with_account(self, name=None):
        if self.current_line:
            return self.current_line.account.id
        return None

    @fields.depends('current_line')
    def on_change_with_budget(self, name=None):
        if self.current_line and self.current_line.budget:
            return self.current_line.budget.id
        return None


class EnterMoveAnalyticAccount(Wizard):
    'Enter Move Analytic Account'
    __name__ = 'move.analytic.account.enter'

    start_state = 'enter'
    start = StateView(
        'move.analytic.account.enter.start',
        'analytic_account_ec.move_analytic_account_enter_start_view_form', [
            Button('Cancel', 'cancel', 'tryton-cancel'),
            Button('Siguiente', 'enter', 'tryton-ok', default=True),
            ])
    enter = StateTransition()
    cancel = StateTransition()

    @classmethod
    def __setup__(cls):
        super(EnterMoveAnalyticAccount, cls).__setup__()
        cls._error_messages.update({
            'no_have_template': (
                'La linea con cuenta %(account)s, con una partida %(budget)s, '
                'con un debito de %(debit)s y un credito de %(credit)s esta '
                'definido en modo Plantilla pero es obligatorio definir la '
                'plantilla a utilizar'),
            # 'debit_credit_no_equal': (
            #     'La cuenta %(account)s tiene un débito de %(debit_account)s y'
            #     ' un crédito de %(credit_account)s, pero en la suma de sus '
            #     'analíticas tienen un valor diferente, débito de '
            #     '%(debit_analytic)s y un crédito de %(credit_analytic)s'),
            'no_lines': (
                'Debe agregar cuentas analiticas'),
            'have_analytic_account_lines': (
                'Existen analiticas configuradas, Desea eliminarlas??'),
            'hundred_percent': (
                'La suma de los porcentajes de las lineas debe estar entre 1 '
                'y 100.'),
            'no_cancel': (
                'No se puede cancelar mientras no termine de configurar las '
                'cuentas manuales'),
            'must_have_all_move_lines': (
                'Falta configurar todas lineas del asiento contable')
        })

    def transition_cancel(self):
        if self.start.state == 'manual' and self.start.analytic_accounts:
            self.raise_user_error('no_cancel')
        else:
            return 'end'

    def default_start(self, fields):
        defaults = {}
        defaults['state'] = self.start.state
        defaults['lines'] = [x.id for x in self.start.lines]
        defaults['analytic_accounts'] = (
            None if getattr(self.start, 'analytic_accounts', None) is None
            else [x.id for x in self.start.analytic_accounts])
        defaults['current_line'] = (
            None if getattr(self.start, 'current_line', None) is None
            else self.start.current_line.id)
        return defaults

    def _state_selection(self):
        pool = Pool()
        templates = []
        MoveLine = pool.get('account.move.line')
        LineTemplate = pool.get('account.move.line.analytic_account.template')
        line_templates = LineTemplate.search([
            ('lines.move_line.move', 'in',
             Transaction().context.get('active_ids')),
        ])
        if line_templates and getattr(
                self.start, 'analytic_accounts', None) is None:
            LineTemplate.delete(line_templates)
        if getattr(self.start, 'analytic_accounts', None) is None:
            LineTemplateLine = pool.get(
                'account.move.line.analytic_account.template.line')
            line_template_lines = []
            move_lines = MoveLine.search([
                ('move', 'in', Transaction().context.get('active_ids')),
                ('move.state', 'in', ['draft', 'posted']),
                ('account.analytic_account', '=', True),
            ])
            debit_dict = defaultdict(
                lambda : defaultdict(
                    lambda : defaultdict(lambda : Decimal("0"))))
            credit_dict = defaultdict(
                lambda : defaultdict(
                    lambda : defaultdict(lambda : Decimal("0"))))
            move_line_dict = defaultdict(
                lambda : defaultdict(
                    lambda : defaultdict(lambda : [])))
            for row in move_lines:
                account_id = row.account.id
                budget_id = row.budget.id if row.budget else None
                template_id = row.template.id if row.template else None
                debit_dict[account_id][budget_id][template_id] += row.debit
                credit_dict[account_id][budget_id][template_id] += row.credit
                move_line_dict[account_id][budget_id][template_id].append(row.id)
            for account, value in debit_dict.items():
                for budget in value:
                    for template in debit_dict[account][budget]:
                        new = LineTemplate(
                            account=account,
                            budget=budget,
                            debit=debit_dict[account][budget][template],
                            credit=credit_dict[account][budget][template],
                        )
                        if template:
                            new.kind = 'template'
                            new.template = template
                        templates.append(new)
                        for move_line_id in move_line_dict[account][budget][template]:
                            new_line = LineTemplateLine(
                                move_template=new,
                                move_line=move_line_id,
                            )
                            line_template_lines.append(new_line)
            if templates:
                LineTemplate.save(templates)
                LineTemplateLine.save(line_template_lines)
        self.start.lines = [x.id for x in templates]

    def transition_enter(self):
        if getattr(self.start, 'state', None) is None:
            self.get_analytic_move_line()
            self._state_selection()
            if getattr(self.start, 'analytic_accounts', None) is None:
                self.start.state = 'selection'
            else:
                self.start.state = 'all'
            return 'start'

        if self.start.state == 'selection':
            self.create_template()
            if not self.start.lines:
                return 'end'
            self.process_automatic()

        if self.start.state == 'manual':
            self.process_validate()

        if self.start.state == 'all':
            self.create_analytic_move_line()
            for row in self.start.lines:
                row.ready = False
                row.save()
            return 'end'

        is_next = self.get_next_current_line()
        if is_next:
            self.start.state = 'manual'
        else:
            self.start.state = 'all'
        return 'start'

    def process_validate(self):
        if not self.start.analytic_accounts_temp:
            self.raise_user_error('no_lines')
        percentage = Decimal("0")
        for x in self.start.analytic_accounts_temp:
            if not x.analytic_account:
                self.raise_user_error('no_lines')
            percentage += x.percentage
        if percentage == Decimal("0") or percentage > Decimal("1"):
            self.raise_user_error('hundred_percent')
        rule_list = []
        for rule in self.start.analytic_accounts_temp:
            rule_list.append([rule.analytic_account.id, rule.percentage])
        pool = Pool()
        Line = pool.get('analytic_account.line')
        to_save = self.distribution_move_line(
            self.start.current_line.lines, rule_list, Line)
        Line.save(to_save)
        temp = [x.id for x in self.start.analytic_accounts] + [
            y.id for y in to_save]
        self.start.analytic_accounts = temp

    def get_next_current_line(self):
        for row in self.start.lines:
            if not row.ready:
                self.start.current_line = row.id
                row.ready = True
                row.save()
                return True
        return False

    def get_analytic_move_line(self):
        pool = Pool()
        Line = pool.get('analytic_account.line')
        list = Line.search([
            ('move_line.move', 'in', Transaction().context.get('active_ids')),
        ])
        if list:
            self.start.analytic_accounts = [x.id for x in list]

    def process_automatic(self):
        pool = Pool()
        TemplateLine = pool.get('analytic_account.template.line')
        Line = pool.get('analytic_account.line')
        to_save = []
        for line in self.start.lines:
            if line.kind=='template' and line.template:
                rules = TemplateLine.get_match_rules(line.template.id)
                rule_list = []
                for rule in rules:
                    rule_list.append(
                        [rule.analytic_account.id, rule.percentage])
                to_save += self.distribution_move_line(
                    line.lines, rule_list, Line)
                line.ready = True
                line.save()
        if to_save:
            Line.save(to_save)
        self.start.analytic_accounts = [x.id for x in to_save]

    def distribution_move_line(self, move_lines, rule_list, Line):
        to_save = []
        total_percentage = reduce(lambda a, b: a + b, [x[1] for x in rule_list])
        for row in move_lines:
            total_debit = row.move_line.debit
            total_credit = row.move_line.credit
            count = len(rule_list)
            for analytic_account_id, percentage in rule_list:
                if count == 1 and total_percentage == Decimal("1"):
                    debit = total_debit
                    credit = total_credit
                else:
                    debit = (row.move_line.debit*percentage).quantize(
                        Decimal("0.01"))
                    credit = (
                            row.move_line.credit*percentage).quantize(
                        Decimal("0.01"))
                    total_debit = (
                            total_debit-debit).quantize(Decimal("0.01"))
                    total_credit = (
                            total_credit-credit).quantize(Decimal("0.01"))
                new = Line(
                    move_line = row.move_line,
                    debit = debit,
                    credit = credit,
                    account = analytic_account_id,
                    date = row.move_line.move.date,
                )
                to_save.append(new)
                count -= 1
        return to_save

    def create_template(self):
        Line = Pool().get('account.move.line.analytic_account.template')
        if self.start.lines:
            for line in self.start.lines:
                if line.kind == 'template' and not line.template:
                    self.raise_user_error('no_have_template', {
                        'account': line.account.code,
                        'budget': (line.budget.code
                                   if line.budget else 'no definida'),
                        'debit': line.debit,
                        'credit': line.credit,
                    })
                line.save()
        else:
            to_delete = Line.search([
                ('lines.move_line.move', 'in',
                 Transaction().context.get('active_ids')),
            ])
            if to_delete:
                Line.delete(to_delete)

    def create_analytic_move_line(self):
        pool = Pool()
        debit_dict = defaultdict(lambda : Decimal("0"))
        credit_dict = defaultdict(lambda : Decimal("0"))
        move_line_dict = defaultdict(lambda : None)
        for row in self.start.analytic_accounts:
            debit_dict[row.move_line.id] += row.debit
            credit_dict[row.move_line.id] += row.credit
            move_line_dict[row.move_line.id] = row.move_line.id
        if self.start.analytic_accounts:
            MoveLine = pool.get('account.move.line')
            move_lines = MoveLine.search([
                ('move', 'in', Transaction().context.get('active_ids')),
                ('move.state', '=', 'posted'),
                ('account.analytic_account', '=', True),
            ])
            for line in move_lines:
                if not move_line_dict[line.id]:
                    self.raise_user_error('must_have_all_move_lines')
                # if (line.debit != debit_dict[line.id] or
                #         line.credit != credit_dict[line.id]):
                #     self.raise_user_error('debit_credit_no_equal', {
                #         'account': line.account.code,
                #         'debit_account': line.debit,
                #         'credit_account': line.credit,
                #         'debit_analytic': debit_dict[line.id],
                #         'credit_analytic': credit_dict[line.id],
                #     })
        Line = pool.get('analytic_account.line')
        to_delete = Line.search([
            ('move_line.move', 'in', Transaction().context.get('active_ids')),
            ('id', 'not in', [x.id for x in self.start.analytic_accounts])
        ])
        if to_delete:
            Line.delete(to_delete)
        if self.start.analytic_accounts:
            for line in self.start.analytic_accounts:
                line.save()



class MoveLine(ModelSQL, ModelView):
    __name__ = 'account.move.line'

    templates = fields.Function(fields.One2Many(
        'analytic_account.template', None, 'Plantillas'),
        'on_change_with_templates')
    template = fields.Many2One(
        'analytic_account.template', 'Plantilla',
        domain=[
            ('id', 'in', Eval('templates')),
        ],
        depends=['templates'])

    @classmethod
    def __setup__(cls):
        super(MoveLine, cls).__setup__()
        cls._error_messages.update({
            # 'debit_credit_no_equal': (
            #     'La cuenta %(account)s tiene un débito de %(debit_account)s y'
            #     ' un crédito de %(credit_account)s, pero en la suma de sus '
            #     'analíticas tienen un valor diferente, débito de '
            #     '%(debit_analytic)s y un crédito de %(credit_analytic)s'),
        })

    # @classmethod
    # def validate(cls, lines):
    #     for line in lines:
    #         if line.move.state == 'draft' and line.analytic_lines:
    #             debit = reduce(lambda a, b: a + b, [
    #                 x.debit for x in line.analytic_lines])
    #             credit = reduce(lambda a, b: a + b, [
    #                 x.credit for x in line.analytic_lines])
                # if debit < line.debit or credit != line.credit:
                #     cls.raise_user_error('debit_credit_no_equal', {
                #         'account': line.account.code,
                #         'debit_account': line.debit,
                #         'credit_account': line.credit,
                #         'debit_analytic': debit,
                #         'credit_analytic': credit,
                #     })

    @fields.depends('account', 'budget')
    def on_change_with_templates(self, name=None):
        if self.account and not self.account.analytic_account:
            return []
        pool = Pool()
        Template = pool.get('analytic_account.template')
        templates = Template.search(
            ['OR',
             ('accounts', 'in', [self.account.id]),
             ('budgets', 'in', [self.budget.id if self.budget else None]),
            ])
        if not templates:
            templates = Template.search([])
        return [x.id for x in templates]

    @classmethod
    def check_modify(cls, lines, modified_fields=None):
        '''
        Check if the lines can be modified
        '''
        if (modified_fields is not None
                and modified_fields <= cls._check_modify_exclude):
            return
        journal_period_done = []
        for line in lines:
            journal_period = (line.journal.id, line.period.id)
            if journal_period not in journal_period_done:
                cls.check_journal_period_modify(line.period,
                                                line.journal)
                journal_period_done.append(journal_period)


class Line(metaclass=PoolMeta):
    __name__ = 'analytic_account.line'

    _history = True

    move_line_debit = fields.Function(fields.Numeric(
        'Total debito'), 'get_data')
    move_line_credit = fields.Function(fields.Numeric(
        'Total credito'), 'get_data')
    move = fields.Function(
        fields.Many2One('account.move', 'Asiento contable'),
        'on_change_with_move', searcher='search_data')
    budget = fields.Function(
        fields.Many2One('public.budget', 'Partida'),
        'on_change_with_budget', searcher='search_data')
    asset = fields.Many2One(
        'asset', 'Activo',
        domain=[
            ('analytic_accounts', 'in', [Eval('account', -1)]),
        ], depends=['account'])

    @classmethod
    def __setup__(cls):
        super(Line, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('credit_debit_zero',
             Check(t,
                   (t.credit * t.debit == 0)),
             'El valor de crédito o débito .'),
        ]
        cls.date.states['readonly'] = True
        cls.account.domain = [
            #TODO por migracion se acepta cargar las partidas tipo raiz
            ('type', 'not in', ['view', 'distribution', 'root']),
            ('state', '=', 'opened'),
        ]
        cls._order = [
            ('move_line', 'ASC'),
            ('date', 'ASC'),
        ]
        cls._error_messages.update({
            'must_have_analytic_perdios': (
                'No existen periodos analiticos en estado Abierto en '
                'Ejercicios fiscales para la fecha %(date)s')
        })
        cls.order_move = cls._order_field_model(
            'account.move.line', 'move.number')
        cls.order_budget = cls._order_field_model(
            'account.move.line', 'budget.code')

    @classmethod
    def __register__(cls, module_name):
        super(Line, cls).__register__(module_name)
        _table = cls.__table_handler__(module_name)
        _table.drop_constraint('credit_debit')

    @classmethod
    def _order_field_model(cls, model, name):
        def order_field(tables):
            Model = Pool().get(model)
            tbl_model = Model.__table__()
            fname, _, oexpr = name.partition('.')
            field = Model._fields[fname]
            table, _ = tables[None]
            model_tables = tables.get('move_line')
            if model_tables is None:
                model_tables = {
                    None: ((tbl_model, tbl_model.id == table.move_line)
                           if 'move.line' in model else
                           (tbl_model, tbl_model.id == table.request_detail) )
                }
                if 'move.line' in model:
                    tables['move_line'] = model_tables
            return field.convert_order(name, model_tables, Model)
        return staticmethod(order_field)

    @classmethod
    def validate(cls, lines):
        pool = Pool()
        AnalyticPeriod = pool.get('analytic_account.period')
        for line in lines:
            analytic_periods = AnalyticPeriod.search([
                ('company', '=', Transaction().context.get('company')),
                ('fiscalyear', '=', line.move_line.move.period.fiscalyear),
                ('start_date', '<=', line.date),
                ('end_date', '>=', line.date),
                ('state', '=', 'open'),
            ])
            if not analytic_periods:
                cls.raise_user_error('must_have_analytic_perdios', {
                    'date': line.date,
                })

    @classmethod
    def search_data(cls, name, clause):
        domain = tuple(clause[1:])
        if name == 'move':
            return [('move_line.move.number',) + domain]
        if name == 'budget':
            return [('move_line.budget.code',) + domain]
        return clause

    @classmethod
    def get_data(cls, lines, names=None):
        debit = defaultdict(lambda: Decimal("0"))
        credit = defaultdict(lambda: Decimal("0"))
        for line in lines:
            debit[line.id] = line.move_line.debit,
            credit[line.id] = line.move_line.credit,
        return {
            'move_line_debit': debit,
            'move_line_credit': credit,
        }

    @fields.depends('move_line')
    def on_change_with_budget(self, name=None):
        if self.move_line and self.move_line.budget:
            return self.move_line.budget.id
        return None

    @fields.depends('move_line')
    def on_change_with_move(self, name=None):
        if self.move_line and self.move_line.move:
            return self.move_line.move.id
        return None

    @classmethod
    def query_get(cls, line):
        '''
        Return SQL clause for analytic line depending of the context.
        table is the SQL instance of the analytic_account_line table.
        '''
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        move_line = MoveLine.__table__()
        clause = super(Line, cls).query_get(line)
        context = Transaction().context
        if context.get('from_date'):
            clause &= line.date >= context['from_date']
        if context.get('to_date'):
            clause &= line.date <= context['to_date']
        if context.get('analytic_account'):
            clause &= line.account == context['analytic_account']
        if context.get('accounts'):
            clause &= move_line.account.in_(context['accounts'])
        if context.get('account_moves'):
            clause &= move_line.move.in_(context['account_moves'])
        return clause


class Move(metaclass=PoolMeta):
    __name__ = 'account.move'

    @classmethod
    def check_modify(cls, moves):
        pass