from trytond.model import fields
from trytond.pool import PoolMeta


class Location(metaclass=PoolMeta):
    __name__ = "stock.location"

    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[
            ('kind', '!=', 'view'),
            ],
        select=True)
