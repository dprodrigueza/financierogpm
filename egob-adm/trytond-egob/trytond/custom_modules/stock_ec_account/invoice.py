from trytond.model import ModelView, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, StateTransition, \
    Button

__all__ = ['CreateInvoiceStart', 'CreateInvoice']


class CreateInvoiceStart(ModelView):
    'Create Invoice Start'
    __name__ = 'wizard.create.invoice.start'
    invoice = fields.Many2One('account.invoice', 'Invoice', required=True)
    invoice_party = fields.Function(
        fields.Many2One('party.party', 'Party'), 'on_change_with_invoice_party')
    invoice_company = fields.Function(
        fields.Many2One('company.company', 'Company'),
        'on_change_with_invoice_company')
    type_ = fields.Selection(
        [
            ('purchases', 'From purchases'),
            ('shipments', 'From shipments'),
        ], 'Create from', required=True)
    purchases = fields.Many2Many('purchase.purchase', None, None, 'Purchases',
        states={
            'invisible': Eval('type_') != 'purchases',
            'required': Eval('type_') == 'purchases',
        },
        domain=[
            ('party', '=', Eval('invoice_party')),
            ('company', '=', Eval('invoice_company')),
        ], depends=['party'])
    shipments = fields.Many2Many('stock.shipment.in', None, None, 'Shipments',
        states={
            'invisible': Eval('type_') != 'shipments',
            'required': Eval('type_') == 'shipments',
        },
        domain=[
            ('party', '=', Eval('invoice_party')),
            ('company', '=', Eval('invoice_company')),
            ('state', '=', 'done'),
        ], depends=['party'])

    @classmethod
    def default_invoice(cls):
        return Transaction().context['active_id']

    @fields.depends('invoice')
    def on_change_with_invoice_party(self, name=None):
        if self.invoice:
            return self.invoice.party.id
        return None

    @fields.depends('invoice')
    def on_change_with_invoice_company(self, name=None):
        if self.invoice:
            return self.invoice.company.id
        return None


class CreateInvoice(Wizard):
    'Create Invoice'
    __name__ = 'wizard.create.invoice'

    start = StateView('wizard.create.invoice.start',
        'stock_ec_account.wizard_create_invoice_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'handle', 'tryton-ok', default=True),
            ])
    handle = StateTransition()

    def transition_handle(self):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        invoice = self.start.invoice
        lines = []
        if self.start.type_ == 'purchases':
            for purchase in self.start.purchases:
                for pline in purchase.lines:
                    line = InvoiceLine()
                    line.invoice = invoice
                    line.product = pline.product
                    line.on_change_product()
                    line.quantity = pline.quantity
                    line.unit = pline.unit
                    line.unit_price = pline.unit_price
                    line.taxes = pline.taxes
                    line.description = pline.description
                    lines.append(line)
        InvoiceLine.save(lines)
        return 'end'
