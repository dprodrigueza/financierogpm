# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import location
from . import invoice

__all__ = ['register']


def register():
    Pool.register(
        invoice.CreateInvoiceStart,
        location.Location,
        module='stock_ec_account', type_='model')
    Pool.register(
        invoice.CreateInvoice,
        module='stock_ec_account', type_='wizard')
    Pool.register(
        module='stock_ec_account', type_='report')
