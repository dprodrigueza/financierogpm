# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import guard
from . import report
from . import company
from . import seizure


__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        guard.Operative,
        guard.OperativeType,
        guard.OperativeLine,
        guard.OperativeSupply,
        guard.OperativeEmployee,
        guard.LegalDocument,
        guard.Complaint,
        guard.GuardReport,
        guard.ComplaintCulpable,
        guard.ComplaintAffected,
        guard.GuardReportCulpable,
        guard.GuardReportAffected,
        guard.Article,
        guard.CatalogFines,
        guard.CatalogFinesLine,
        guard.OperativeArticleInvolved,
        guard.CaseFileArticleInvolved,
        guard.CaseFile,
        guard.CaseFileOrigin,
        guard.AppearanceLine,
        guard.TechnicalReportLine,
        guard.ImpugnationLine,
        guard.AditionalDocument,
        guard.NotificationDocument,
        guard.Station,
        company.Employee,
        seizure.Mark,
        seizure.Model,
        seizure.Seizure,
        seizure.ObjectSeizure,
        seizure.SeizureArticleInvolved,
        seizure.SeizureReturn,
        seizure.SeizureReturnLine,
        module='citizen_guard', type_='model')
    Pool.register(
        module='citizen_guard', type_='wizard')
    Pool.register(
        report.ComplaintReport,
        report.SeizuresReport,
        report.OperativeReport,
        report.CaseCoverPage,
        module='citizen_guard', type_='report')
