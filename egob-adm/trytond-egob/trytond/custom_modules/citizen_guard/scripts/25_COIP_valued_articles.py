from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from proteus import config, Model

from decimal import Decimal
from datetime import datetime
import pandas as pd
 

def create_departments(database, trytond_config, filename):
    config.set_trytond(database, config_file=trytond_config)
    field_names = ['article','description', 'minimal_day', 'maximal_day',
        'minimal_value', 'maximal_value']
    with open(filename, 'rb') as departments_file:
        df = pd.read_excel(
            departments_file, names=field_names, dtype='object', header=0)
        Article = Model.get('citizen_guard.legal_document.article')
        ValuedArticle = Model.get('citizen_guard.legal_document.catalog_fines')
        Line = Model.get('citizen_guard.legal_document.catalog_fines.line')
        for row in df.itertuples():
            find_document = Article.find([('title', '=', row.article)])
            if find_document:
                document = find_document[0]
                find_valued = ValuedArticle.find([('article', '=', document.id)])
                valued = None
                if find_valued:
                    valued = find_valued[0]
                else:
                    valued = ValuedArticle()
                    valued.article = document
                line = Line()
                line.description = row.description
                line.minimal_day = row.minimal_day
                line.maximal_day = row.maximal_day
                line.minimal_value = Decimal("{0:.2f}".format(row.minimal_value))
                line.maximal_value = Decimal("{0:.2f}".format(row.maximal_value))
                valued.lines.append(line)
                valued.save()
            else:
                print("NO se encontro articulo ", row.article)    
            print("Articulo valorado", row.article, " procesado")
        print('finalizado------------------------------')

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--database', dest='database', required=True,
        help='database')
    parser.add_argument('--config-file', dest='config_file', required=True,
        help='trytond config file')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_departments(options.database, options.config_file, options.filename)
