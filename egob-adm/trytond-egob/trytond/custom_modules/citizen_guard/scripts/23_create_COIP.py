from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from proteus import config, Model

from decimal import Decimal
from datetime import datetime
import pandas as pd


def create_departments(database, trytond_config, filename):
    config.set_trytond(database, config_file=trytond_config)
    field_names = ['code']
    with open(filename, 'rb') as departments_file:
        df = pd.read_excel(
            departments_file, names=field_names, dtype='object', header=0)
        cont = 0
        LegalDocument = Model.get('citizen_guard.legal_document')
        base = LegalDocument.find([('name', '=', 'Código Orgánico Integral Penal (COIP)')])
        parent = None
        if not base:
            node = LegalDocument()
            node.name = 'Código Orgánico Integral Penal (COIP)'
            node.save()
            base = node
            parent = base
        else:
            parent = base[0]
        for row in df.itertuples():
            cont += 1
            print(' Linea 1: ')
            data = str(row.code).split('/')
            if len(data) == 1:
                print(data[0])
                node = LegalDocument()
                node.name = data[0]
                node.parent = base
                node.save()
            else:
                for record in data: #[LIBRO PRE, TIT 1]
                    ask_record = LegalDocument.find([('name', '=', record)])
                    if ask_record:
                        # YA FUE CREADO ANTERIORMENTE
                        parent = ask_record[0]
                        print('X - ',record)
                    else: #[ ]
                        print('O - ',record)
                        node = LegalDocument()
                        node.name = record
                        node.parent = parent
                        node.save()
                        parent = node
            parent = base

            print("Linea ", cont, " procesada")
        print('finalizado------------------------------')

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--database', dest='database', required=True,
        help='database')
    parser.add_argument('--config-file', dest='config_file', required=True,
        help='trytond config file')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_departments(options.database, options.config_file, options.filename)

# HAY QUE AGRWEGAAR UNA CATEGORIA POR DEFECTO


# code
# serial
# short_name
# asset_state         good(Bueno) bad                            
# employee < M20
# life_time
# sbye
# brand
# model
# purchase_date
# input_date
# invoice_number
# type                bld(BLD (larga duracion)) bca
# asset_type          artistic biological property book vehicle furniture(Bienes muebles)
# possesion_type      own(Propio) leasing pignoracion
# value
# residual_value
# depreciation_method linear(Línea recta)
# frequency           yearly(anual) monthly


# pd.to_datetime(purchase_date, format='%Y%m%d', errors='ignore')