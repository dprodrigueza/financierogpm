from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from proteus import config, Model

from decimal import Decimal
from datetime import datetime
import pandas as pd


def create_departments(database, trytond_config, filename):
    config.set_trytond(database, config_file=trytond_config)
    field_names = ['article', 'name', 'description', 'document']
    with open(filename, 'rb') as departments_file:
        df = pd.read_excel(
            departments_file, names=field_names, dtype='object', header=0)
        cont = 0
        Article = Model.get('citizen_guard.legal_document.article')
        LegalDocument = Model.get('citizen_guard.legal_document')
        for row in df.itertuples():
            cont += 1
            article = Article()
            article.title = row.article
            article.name = row.name
            article.description = row.description

            document = LegalDocument.find([('name', '=', row.document)])[0]
            article.legal_document = document
            article.save()

            print("Articulo ", row.article, " procesado")
        print('finalizado------------------------------')

if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('--database', dest='database', required=True,
        help='database')
    parser.add_argument('--config-file', dest='config_file', required=True,
        help='trytond config file')
    parser.add_argument('--filename', dest='filename', required=True,
        help='filename')
    options = parser.parse_args()
    create_departments(options.database, options.config_file, options.filename)

# HAY QUE AGRWEGAAR UNA CATEGORIA POR DEFECTO


# code
# serial
# short_name
# asset_state         good(Bueno) bad                            
# employee < M20
# life_time
# sbye
# brand
# model
# purchase_date
# input_date
# invoice_number
# type                bld(BLD (larga duracion)) bca
# asset_type          artistic biological property book vehicle furniture(Bienes muebles)
# possesion_type      own(Propio) leasing pignoracion
# value
# residual_value
# depreciation_method linear(Línea recta)
# frequency           yearly(anual) monthly


# pd.to_datetime(purchase_date, format='%Y%m%d', errors='ignore')