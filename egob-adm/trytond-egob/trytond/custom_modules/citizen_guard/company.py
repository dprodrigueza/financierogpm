
from trytond.pool import PoolMeta

__all__ = ['Employee']


class Employee(metaclass=PoolMeta):
    'Employee'
    __name__ = 'company.employee'
