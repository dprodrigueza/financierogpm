# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.citizen_guard.tests.test_citizen_guard import suite
except ImportError:
    from .test_citizen_guard import suite

__all__ = ['suite']
