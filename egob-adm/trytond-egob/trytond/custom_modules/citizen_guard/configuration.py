from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.modules.company.model import (CompanyMultiValueMixin,
    CompanyValueMixin)
from trytond.pool import Pool
from trytond.pyson import Eval

__all__ = ['Configuration', 'ConfigurationSequence']


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Citizen Guard Configuration'
    __name__ = 'citizen_guard.configuration'

    operative_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Secuencia de operativos', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'operative'),
        ]))
    seizure_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Incautaciones', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'seizure'),
        ]))
    seizure_return_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Devolución de objetos incautados', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'seizure_return'),
        ]))
    complaint_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Denuncias', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'complaint'),
        ]))

    guard_report_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Informes de guardias', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'guard_report'),
        ]))

    case_file_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence', 'Expedientes', required=True,
        domain=[
            ('company', 'in',
             [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'case_file'),
        ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['operative_sequence', 'seizure_sequence',
                'seizure_return_sequence', 'complaint_sequence',
                'guard_report_sequence', 'case_file_sequence']:
            return pool.get('citizen_guard.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_operative_sequence(cls, **pattern):
        return cls.multivalue_model(
            'operative_sequence').default_operative_sequence()

    @classmethod
    def default_seizure_sequence(cls, **pattern):
        return cls.multivalue_model(
            'seizure_sequence').default_seizure_sequence()

    @classmethod
    def default_seizure_return_sequence(cls, **pattern):
        return cls.multivalue_model(
            'seizure_return_sequence').default_seizure_return_sequence()

    @classmethod
    def default_complaint_sequence(cls, **pattern):
        return cls.multivalue_model(
            'complaint_sequence').default_complaint_sequence()

    @classmethod
    def default_guard_report_sequence(cls, **pattern):
        return cls.multivalue_model(
            'guard_report_sequence').default_guard_report_sequence()

    @classmethod
    def default_case_file_sequence(cls, **pattern):
        return cls.multivalue_model(
            'case_file_sequence').default_case_file_sequence()


class ConfigurationSequence(ModelSQL, CompanyValueMixin):
    'Citizen Guard Configuration Sequence'
    __name__ = 'citizen_guard.configuration.sequence'

    operative_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de Operativos',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'operative'),
        ], depends=['company'])
    seizure_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Incautación',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'seizure'),
        ], depends=['company'])
    seizure_return_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Devolución de objetos incautados',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'seizure_return'),
        ], depends=['company'])
    complaint_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Denuncia',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'complaint'),
        ], depends=['company'])
    guard_report_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia Informe de Guardias',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'guard_report'),
        ], depends=['company'])
    case_file_sequence = fields.Many2One(
        'ir.sequence', 'Secuencia de expedientes',
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'case_file'),
        ], depends=['company'])

    @classmethod
    def default_operative_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'sequence_guard_operative')
        except KeyError:
            return None

    @classmethod
    def default_seizure_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'sequence_seizure')
        except KeyError:
            return None

    @classmethod
    def default_seizure_return_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'sequence_seizure_return')
        except KeyError:
            return None

    @classmethod
    def default_complaint_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'sequence_complaint')
        except KeyError:
            return None

    @classmethod
    def default_guard_report_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'guard_report')
        except KeyError:
            return None

    @classmethod
    def default_case_file_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('citizen_guard', 'case_file')
        except KeyError:
            return None
