from trytond.modules.hr_ec.company import CompanyReport


__all__ = ['ComplaintReport', 'SeizuresReport', 'OperativeReport']


class ComplaintReport(CompanyReport):
    __name__ = 'citizen_guard.complaint.report'


class SeizuresReport(CompanyReport):
    __name__ = 'citizen_guard.seizure.report'


class OperativeReport(CompanyReport):
    __name__ = 'citizen_guard.operative.report'


class CaseCoverPage(CompanyReport):
    __name__ = 'citizen_guard.case_file.cover_page'
