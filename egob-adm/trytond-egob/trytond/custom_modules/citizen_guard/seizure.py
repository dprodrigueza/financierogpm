import functools

from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from decimal import Decimal
from trytond.transaction import Transaction

__all__ = ['Mark', 'Model', 'Seizure', 'ObjectSeizure', 'SeizureReturn',
    'SeizureReturnLine']

_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class Mark(ModelSQL, ModelView):
    "mark"
    __name__ = 'citizen_guard.seizure.object.mark'
    name = fields.Char('Marca')


class Model(ModelSQL, ModelView):
    "Model"
    __name__ = 'citizen_guard.seizure.object.model'
    name = fields.Char('Modelo')
    mark = fields.Many2One('citizen_guard.seizure.object.mark', 'Marca')


class ObjectSeizure(ModelSQL, ModelView):
    'Object Seizure'
    __name__ = 'citizen_guard.seizure.object'
    name = fields.Char('Descripción objeto ')
    mark = fields.Many2One('citizen_guard.seizure.object.mark', 'Marca')
    model = fields.Many2One('citizen_guard.seizure.object.model', 'Modelo',
        domain=[
            ('mark', '=', Eval('mark'))
            ], depends=['mark'])
    series = fields.Char('Serie')
    quantity = fields.Numeric('Cantidad', digits=(16, 2))
    price = fields.Numeric('Precio unitario', digits=(16, 2))
    total = fields.Function(fields.Numeric(
        'Valor total', digits=(16, 2), required=True),
        'on_change_with_total'
    )
    state_object = fields.Selection(
        [
            ('bad', 'Malo'),
            ('good', 'Bueno')
        ], 'Estado objeto')
    state_translated = state_object.translated('state_object')
    observation = fields.Char('Observaciones')
    seizure = fields.Many2One('citizen_guard.seizure', 'Objetos Incautados')

    @fields.depends('quantity', 'price')
    def on_change_with_total(self, name=None):
        if self.quantity and self.price:
            return self.quantity * self.price
        return Decimal(0.0)

    def get_rec_name(self, name):
        if self.mark and self.model and self.name:
            return self.mark.name + ' / ' + self.model.name + ' / ' + \
                self.name
        elif self.mark and self.name:
            return self.mark.name + ' / ' + self.name
        elif self.model and self.name:
            return self.model.name + ' / ' + self.name
        elif self.name:
            return self.name

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        if table_h.column_exist('object'):
            table_h.column_rename('object', 'seizure')
        super(ObjectSeizure, cls).__register__(module_name)


class ArticleInvolvedMixin(object):
    'Articles Involved'
    reason = fields.Text('Razón del artículo')
    article = fields.Many2One('citizen_guard.legal_document.article',
        'Artículo Relacionado', states={
            'required': True
        })


class SeizureArticleInvolved(ModelSQL, ModelView, ArticleInvolvedMixin):
    'Seizure Articles Involved'
    __name__ = 'citizen_guard.seizure.article_involved'

    seizure = fields.Many2One('citizen_guard.seizure', 'Incautación')


class Seizure(Workflow, ModelSQL, ModelView):
    'Seizure'
    __name__ = 'citizen_guard.seizure'
    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número incautación', readonly=True)
    registered_date = fields.Date('Fecha de registro', readonly=True)
    registered_by = employee_field('Registrado por')
    seizure_date = fields.Date('Fecha de la incautación', states=_STATES,
        depends=_DEPENDS)
    city = fields.Many2One('country.subdivision', 'Ciudad',
        domain=[
            ('type', '=', 'city'),
            ('country.code', '=', 'EC')
        ], required=True, states=_STATES, depends=_DEPENDS)
    parish = fields.Many2One('country.subdivision', 'Parroquia',
        domain=[
            ('type', '=', 'parish'),
            ('country.code', '=', 'EC')
        ], required=True, states=_STATES, depends=_DEPENDS)
    location_detail = fields.Text('Detalle de la ubicación', states=_STATES,
        depends=_DEPENDS)
    type_activity = fields.Selection([
        ('export', 'Exportación'),
        ('import', 'Importación'),
        ('transit', 'Tránsito'),
        ('sale', 'Ventas'),
        ('ownership', 'Posesión'),
        ], 'Actividad', states=_STATES, depends=_DEPENDS)
    detail = fields.Text('Detalle de la incautación', states=_STATES,
        depends=_DEPENDS)
    type_translated = type_activity.translated('type_activity')
    country_origin = fields.Many2One('country.country', 'País Origen',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': ~(Eval('type_activity') == 'export')
            },
        depends=['state', 'type_activity']
        )
    country_destiny = fields.Many2One('country.country', 'País Destino',
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': ~(Eval('type_activity') == 'export')
            },
        depends=['state', 'type_activity']
        )
    seizure_objects = fields.One2Many('citizen_guard.seizure.object', 'seizure',
        'Objetos incautados', states=_STATES, depends=_DEPENDS)
    articles_involved = fields.One2Many(
        'citizen_guard.seizure.article_involved', 'seizure',
        'Artículos Implicados')
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('registered', 'Registrada'),
    ], 'Estado', readonly=True)

    @classmethod
    def __setup__(cls):
        super(Seizure, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registered'),
            ('registered', 'draft'),

        ))
        cls._buttons.update(
            {
                'register': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'draft': {
                    'invisible': ~Eval('state').in_(['registered']),
                }
            })
        cls._error_messages.update({
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'no_sequence': ('No existe una secuencia cofigurada para las '
                            'incautaciones.')
        })

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_registered_date(cls):
        today = Pool().get('ir.date')
        return today.today()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_country_origin():
        pool = Pool()
        Country = pool.get('country.country')
        country = Country.search([('code', '=', 'EC')])
        if country:
            return country[0].id

    @classmethod
    def set_number(cls, seizures):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')
        config = Config(1)
        if not config.seizure_sequence:
            cls.raise_user_error('no_sequence')
        for seizure in seizures:
            if seizure.number:
                continue
            seizure.number = Sequence.get_id(
                config.seizure_sequence.id)
        cls.save(seizures)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, seizures):
        pass

    @classmethod
    @ModelView.button
    @set_employee('registered_by')
    @Workflow.transition('registered')
    def register(cls, seizures):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for seizure in seizures:
            seizure.registered_date = today
        cls.set_number(seizures)
        super(Seizure, cls).save(seizures)
        pass


class SeizureReturn(Workflow, ModelSQL, ModelView):
    'Seizure Return'
    __name__ = 'citizen_guard.seizure.return'
    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        # ('request', 'Solicitado'),
        ('authorized', 'Autorizado'),
    ], 'Estado', readonly=True)
    number = fields.Char('Número', readonly=True)
    lines = fields.One2Many('citizen_guard.seizure.return.line', 'return_',
        'Objetos reclamados')
    name = fields.Char('Nombres y apellidos del propietario')
    date = fields.Date('Fecha de devolución')
    address = fields.Char('Dirección del propietario')
    email = fields.Char('Correo del propietario')
    authorized_by = employee_field("Tramitada por")
    authorized_date = fields.Date('Fecha de tramitación', readonly=True)
    seizure = fields.Many2One('citizen_guard.seizure', 'Incautación')

    @classmethod
    def __setup__(cls):
        super(SeizureReturn, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'authorized'),
            ('authorized', 'draft'),

        ))
        cls._buttons.update({
            'authorize': {
                'invisible': ~Eval('state').in_(['draft']),
            },
            'draft': {
                'invisible': ~Eval('state').in_(['authorized']),
            }
        })
        cls._error_messages.update({
            'no_sequence': ('No existe una secuencia cofigurada para las '
                            'devoluciones.')
        })

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def set_number(cls, returns):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')
        config = Config(1)
        if not config.seizure_return_sequence:
            cls.raise_user_error('no_sequence')
        for return_ in returns:
            if return_.number:
                continue
            return_.number = Sequence.get_id(
                config.seizure_return_sequence.id)
        cls.save(returns)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, returns):
        pass

    @classmethod
    @ModelView.button
    @set_employee('authorized_by')
    @Workflow.transition('authorized')
    def authorize(cls, returns):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for return_ in returns:
            return_.authorized_date = today
        cls.set_number(returns)
        super(SeizureReturn, cls).save(returns)
        pass


class SeizureReturnLine(ModelSQL, ModelView):
    'Seizure Return Line'
    __name__ = 'citizen_guard.seizure.return.line'

    return_ = fields.Many2One('citizen_guard.seizure.return', 'Devolución')
    object_ = fields.Many2One('citizen_guard.seizure.object', 'Objeto')
    quantity = fields.Integer('Cantidad')
    property_file = fields.Binary('Documento de propiedad',
        filename='filename', file_id='path')
    filename = fields.Char('Nombre del Archivo', readonly=True)
    path = fields.Char('Ubicación del archivo', readonly=True)
