import functools

from trytond.pyson import Eval, Bool, If
from trytond.pool import Pool
from trytond.model import Workflow, ModelView, ModelSQL, fields, tree
from trytond.transaction import Transaction
from .seizure import ArticleInvolvedMixin

__all__ = ['Operative', 'OperativeType', 'OperativeLine', 'OperativeSupply',
    'OperativeEmployee', 'LegalDocument', 'Complaint', 'GuardReport',
    'ComplaintCulpable', 'ComplaintAffected', 'GuardReportCulpable',
    'GuardReportAffected', 'Article', 'CatalogFines', 'Station',
    'CatalogFinesLine', 'OperativeArticleInvolved', 'CaseFile',
    'AppearanceLine', 'TechnicalReportLine', 'NotificationDocument',
    'ImpugnationLine']


_STATES = {'readonly': Eval('state') != 'draft', }
_DEPENDS = ['state']


def employee_field(string):
    return fields.Many2One(
        'company.employee', string,
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': True,
        },
        depends=['company'])


def set_employee(field):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(cls, items, *args, **kwargs):
            pool = Pool()
            User = pool.get('res.user')
            user = User(Transaction().user)
            result = func(cls, items, *args, **kwargs)
            employee = user.employee
            if employee:
                company = employee.company
                cls.write(
                    [it for it in items
                        if not getattr(it, field) and it.company == company], {
                        field: employee.id,
                        })
            return result
        return wrapper
    return decorator


class BasicMixin(object):
    name = fields.Char('Nombre')
    description = fields.Text('Descripción')


class OperativeArticleInvolved(ModelSQL, ModelView, ArticleInvolvedMixin):
    'Operative Articles Involved'
    __name__ = 'citizen_guard.operative.article_involved'
    operative = fields.Many2One('citizen_guard.operative', 'Incautación')


class Operative(Workflow, ModelSQL, ModelView):
    'Operative'
    __name__ = 'citizen_guard.operative'

    name = fields.Char('Nombre', states=_STATES, depends=_DEPENDS)
    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('request', 'Solicitado'),
        ('authorized', 'Autorizado'),
        ('denied', 'Negado'),
        ('closed', 'Cerrado')
    ], 'Estado', readonly=True)
    number = fields.Char('Número', readonly=True)
    reason = fields.Text('Razon del operativo', states=_STATES,
        depends=_DEPENDS)
    type_ = fields.Many2One('citizen_guard.operative.type', 'Tipo',
        states=_STATES, depends=_DEPENDS)
    lines = fields.One2Many('citizen_guard.operative.line', 'operative',
        'Lineas', states=_STATES, depends=_DEPENDS)
    employees = fields.Many2Many('citizen_guard.operative-company.employee',
        'operative', 'employee', 'Empleados', states=_STATES, depends=_DEPENDS)
    external_staff = fields.Text('Personal externo que asistirá',
        states=_STATES, depends=_DEPENDS)
    supplies = fields.One2Many('citizen_guard.operative.supply',
        'operative', 'Implementos a utilizar', states=_STATES,
        depends=_DEPENDS)
    pos_info = fields.Text('Observación post - operativo',
        states={
            'readonly': ~(Eval('state') == 'authorized')
        }, depends=_DEPENDS)
    request_date = fields.Date('Fecha de registro',
        states={
            'readonly': True
        }, depends=_DEPENDS)
    request_by = employee_field('Solicitado por')
    authorization_date = fields.Date('Fecha de autorización',
        states={
            'readonly': True
        }, depends=_DEPENDS)
    authorized_by = employee_field('Autorizado por')
    articles_involved = fields.One2Many(
        'citizen_guard.operative.article_involved', 'operative',
        'Artículos Implicados')

    @classmethod
    def __setup__(cls):
        super(Operative, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'request'),
            ('request', 'authorized'),
            ('request', 'draft'),
            ('request', 'denied'),
            ('authorized', 'closed'),
        ))
        cls._buttons.update(
            {
                'authorize': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
            })
        cls._error_messages.update({
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'not_postoperative_information': ('Aún no se ha ingresado la '
                'información del postoperativo en todas las lineas que '
                'conforman el operativo'),
            'no_sequence': ('No existe una secuencia cofigurada para los '
                'expedientes.'),
        })
        cls._buttons.update(
            {
                'request': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'deny': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'draft': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'authorize': {
                    'invisible': ~Eval('state').in_(['request']),
                },
                'close': {
                    'invisible': ~Eval('state').in_(['authorized']),
                },
            })

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def set_number(cls, operatives):
        '''
        Fill the number field with the operative sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')

        config = Config(1)
        if not config.operative_sequence:
            cls.raise_user_error('no_sequence')
        for operative in operatives:
            if operative.number:
                continue
            operative.number = Sequence.get_id(
                config.operative_sequence.id)
        cls.save(operatives)

    def get_rec_name(self, name):
        if self.name:
            return name

    @classmethod
    @ModelView.button
    @Workflow.transition('request')
    @set_employee('request_by')
    def request(cls, operatives):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for operative in operatives:
            operative.request_date = today
        cls.set_number(operatives)
        # cls.send_email(cls, operatives)
        super(Operative, cls).save(operatives)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('authorized')
    @set_employee('authorized_by')
    def authorize(cls, operatives):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for operative in operatives:
            operative.authorization_date = today
        super(Operative, cls).save(operatives)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('denied')
    def deny(cls, operatives):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, operatives):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, operatives):
        for operative in operatives:
            for line in operative.lines:
                if not line.postoperative_information:
                    cls.raise_user_error('not_postoperative_information')
        pass


class OperativeEmployee(ModelSQL):
    'Operative Employee'
    __name__ = 'citizen_guard.operative-company.employee'

    _states = {
        'readonly': (Eval('_parent_operative', {}).get('state') != 'draft')
    }
    _depends = ['operative']

    operative = fields.Many2One('citizen_guard.operative', 'Operativo',
        states=_states, depends=_depends, ondelete='CASCADE', select=True,
        required=True)
    employee = fields.Many2One('company.employee', 'Empleado',
        states=_states, depends=_depends, ondelete='CASCADE', select=True,
        required=True)


class OperativeType(ModelSQL, ModelView, BasicMixin):
    'Operative Type'
    __name__ = 'citizen_guard.operative.type'


class OperativeLine(ModelSQL, ModelView):
    'Operative Line'
    __name__ = 'citizen_guard.operative.line'

    _states = {
        'readonly': (Eval('_parent_operative', {}).get('state') != 'draft')
    }
    _depends = ['operative']

    operative = fields.Many2One('citizen_guard.operative', 'Operativo',
        states=_states, depends=_depends)
    date = fields.Date('Fecha', states=_states, depends=_depends)
    main_street = fields.Char('Calle principal', states=_states,
        depends=_depends)
    secondary_street = fields.Char('Calle secundaria', states=_states,
        depends=_depends)
    parish = fields.Many2One('country.subdivision', 'Parroquia', domain=[
        ('type', '=', 'parish'),
        ('country.code', '=', 'EC')
    ], states=_states, depends=_depends)
    start_time = fields.Time('Hora inicio', states=_states, depends=_depends)
    end_time = fields.Time('Hora fin', states=_states, depends=_depends)
    postoperative_information = fields.Text('Información Postoperativo',
        states={
            'readonly': ~(Eval('_parent_operative',
                              {}).get('state', '') == 'authorized')
        }
    )
    responsable = fields.Many2One('company.employee', 'Responsable',
        domain=[
            ('id', 'in', Eval('_parent_operative', {}).get('employees', []))
        ], states=_states
    )


class OperativeSupply(ModelSQL, ModelView):
    'Operative Supply'
    __name__ = 'citizen_guard.operative.supply'

    operative = fields.Many2One('citizen_guard.operative', 'Operativo')
    quantity = fields.Numeric('Cantidad', digits=(16, 0))
    asset = fields.Many2One('asset', 'Implemento')


class LegalDocument(tree(separator='/'), ModelSQL, ModelView):
    'Legal Document'
    __name__ = 'citizen_guard.legal_document'

    name = fields.Char('Nombre', required=True)
    parent = fields.Many2One(
        'citizen_guard.legal_document', 'Padre', left="left", right='right')
    left = fields.Integer('left')
    right = fields.Integer('right')
    children = fields.One2Many('citizen_guard.legal_document',
        'parent', 'Hijos')

    @classmethod
    def __setup__(cls):
        super(LegalDocument, cls).__setup__()
        cls._order[0] = ('name', 'ASC')

    @staticmethod
    def default_left():
        return 0

    @staticmethod
    def default_right():
        return 0


class ContraventionReportMixin(object):
    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número', readonly=True)
    location_complaint = fields.Text('Lugar de los hechos',
        states=_STATES, depends=_DEPENDS)
    date_complaint = fields.Date('Fecha de los hechos',
        states=_STATES, depends=_DEPENDS)
    time_complaint = fields.Time('Hora de los hechos',
        states=_STATES, depends=_DEPENDS)
    description = fields.Text('Descripcion de lo ocurrido',
        states=_STATES, depends=_DEPENDS)
    state = fields.Selection([
        ('draft', 'Borrador'),
        ('registered', 'Registrada'),
        ('processed', 'Tramitada'),
        ('rejected', 'Rechazada'),
    ], 'Estado', readonly=True)
    processed_by = employee_field("Tramitada por")
    processed_date = fields.Date('Fecha de tramitación', readonly=True)
    rejected_by = employee_field("Rechazada por")
    rejected_date = fields.Date('Fecha de rechazo', readonly=True)
    registered_by = employee_field("Registrada por")
    registered_date = fields.Date('Fecha de registro', readonly=True)


class GuardReport(
        Workflow, ModelSQL, ModelView, ContraventionReportMixin):
    'Report By Guards'
    __name__ = 'citizen_guard.guard_report'

    employee = fields.Many2One('company.employee', 'Empleado registrante',
        required=True)
    culpables = fields.One2Many('citizen_guard.guard_report.culpable',
        'guard_report', 'Presunto(s) culpable(s) de los hechos',
        states=_STATES, depends=_DEPENDS)
    affected = fields.One2Many('citizen_guard.guard_report.affected',
        'guard_report', 'Persona(s) afecata(s)',
        states=_STATES, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(GuardReport, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registered'),
            ('registered', 'processed'),
            ('registered', 'rejected'),
            ('registered', 'draft'),
        ))
        cls._buttons.update(
            {
                'register': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'reject': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
                'draft': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
                'process': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
            })
        cls._error_messages.update({
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'no_sequence': ('No existe una secuencia cofigurada para los '
                'informes de guardianía.')
        })

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def set_number(cls, records):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')
        config = Config(1)
        if not config.guard_report_sequence:
            cls.raise_user_error('no_sequence')
        for report in records:
            if report.number:
                continue
            report.number = Sequence.get_id(
                config.guard_report_sequence.id)
        cls.save(records)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('registered')
    @set_employee('registered_by')
    def register(cls, records):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for report in records:
            report.registered_date = today
        cls.set_number(records)
        super(GuardReport, cls).save(records)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('rejected')
    @set_employee('rejected_by')
    def reject(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    @set_employee('processed_by')
    def process(cls, records):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for report in records:
            report.processed_date = today
        super(GuardReport, cls).save(records)
        pass


class Complaint(Workflow, ModelSQL, ModelView, ContraventionReportMixin):
    "complaint"
    __name__ = 'citizen_guard.complaint'

    name = fields.Char('Nombres y apellidos del denunciante',
        states=_STATES, depends=_DEPENDS)
    address = fields.Char('Dirección del denunciante',
        states=_STATES, depends=_DEPENDS)
    email = fields.Char('Correo del denunciante',
        states=_STATES, depends=_DEPENDS)
    culpables = fields.One2Many('citizen_guard.complaint.culpable',
        'complaint', 'Presunto(s) culpable(s) de los hechos',
        states=_STATES, depends=_DEPENDS)
    affected = fields.One2Many('citizen_guard.complaint.affected',
        'complaint', 'Persona(s) afecata(s)',
        states=_STATES, depends=_DEPENDS)

    @classmethod
    def __setup__(cls):
        super(Complaint, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'registered'),
            ('registered', 'processed'),
            ('registered', 'rejected'),
            ('registered', 'draft'),
        ))
        cls._buttons.update(
            {
                'register': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'reject': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
                'draft': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
                'process': {
                    'invisible': ~Eval('state').in_(['registered']),
                },
            })
        cls._error_messages.update({
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'no_sequence': ('No existe una secuencia cofigurada para las '
                'denuncias.'),
        })

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def set_number(cls, complaints):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')
        config = Config(1)
        if not config.complaint_sequence:
            cls.raise_user_error('no_sequence')
        for complaint in complaints:
            if complaint.number:
                continue
            complaint.number = Sequence.get_id(
                config.complaint_sequence.id)
        cls.save(complaints)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('registered')
    @set_employee('registered_by')
    def register(cls, complaints):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for complaint in complaints:
            complaint.registered_date = today
        cls.set_number(complaints)
        super(Complaint, cls).save(complaints)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('rejected')
    @set_employee('rejected_by')
    def reject(cls, complaints):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    @set_employee('processed_by')
    def process(cls, complaints):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for complaint in complaints:
            complaint.authorization_date = today
        super(Complaint, cls).save(complaints)
        pass


class PersonMixin(object):
    name = fields.Char('Nombres y apellidos')
    phone = fields.Char('Teléfono de contacto')
    identifier = fields.Char('Cédula')
    address = fields.Char('Dirección')
    email = fields.Char('Correo')


class ComplaintAffected(ModelSQL, ModelView, PersonMixin):
    'Complaint Affected Person'
    __name__ = 'citizen_guard.complaint.affected'
    complaint = fields.Many2One('citizen_guard.complaint', 'Denuncia')


class ComplaintCulpable(ModelSQL, ModelView, PersonMixin):
    'Complaint Culpable Person'
    __name__ = 'citizen_guard.complaint.culpable'
    complaint = fields.Many2One('citizen_guard.complaint', 'Denuncia')


class GuardReportAffected(ModelSQL, ModelView, PersonMixin):
    'Guard Report Affected Person'
    __name__ = 'citizen_guard.guard_report.affected'
    guard_report = fields.Many2One('citizen_guard.guard_report', 'Denuncia')


class GuardReportCulpable(ModelSQL, ModelView, PersonMixin):
    'Guard Report Culpable Person'
    __name__ = 'citizen_guard.guard_report.culpable'
    guard_report = fields.Many2One('citizen_guard.guard_report', 'Denuncia')


class Article(ModelSQL, ModelView):
    'Article'
    __name__ = 'citizen_guard.legal_document.article'

    title = fields.Char('Título')
    name = fields.Char('Nombre')
    description = fields.Text('Descripción')
    legal_document = fields.Many2One('citizen_guard.legal_document',
        'Documento Legal')

    def get_rec_name(self, name):
        if self.name and self.title:
            return self.title + '.- ' + self.name


class CatalogFines(ModelSQL, ModelView):
    'Catalog Fines'
    __name__ = 'citizen_guard.legal_document.catalog_fines'

    article = fields.Many2One('citizen_guard.legal_document.article',
        'Artículo')
    lines = fields.One2Many('citizen_guard.legal_document.catalog_fines.line',
        'catalog', 'Valores')


class CatalogFinesLine(ModelSQL, ModelView):
    'Catalog Fines Line'
    __name__ = 'citizen_guard.legal_document.catalog_fines.line'
    description = fields.Char('Descripción')
    minimal_value = fields.Numeric('Valor Mímino', digits=(16, 2))
    maximal_value = fields.Numeric('Valor Máximo', digits=(16, 2))
    minimal_day = fields.Integer('Mínimo de días')
    maximal_day = fields.Integer('Máximo de días')
    catalog = fields.Many2One(
        'citizen_guard.legal_document.catalog_fines', 'Artículo')


class Station(ModelSQL, ModelView):
    "Station"
    __name__ = 'citizen_guard.station'
    name = fields.Char('Nombre', states={
        'required': True
    })
    company = fields.Many2One('company.company', 'Company',
        states=_STATES,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], readonly=True)
    sequence = fields.Many2One('ir.sequence', "Secuencia", required=True,
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('code', '=', 'case_file'),
            ])

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class CaseFileArticleInvolved(ModelSQL, ModelView, ArticleInvolvedMixin):
    'Case File Articles Involved'
    __name__ = 'citizen_guard.case_file.article_involved'

    case_file = fields.Many2One('citizen_guard.case_file', 'Expediente')

    def get_rec_name(self, name):
        if self.article:
            return self.article.name


class CaseFile(Workflow, ModelSQL, ModelView):
    'Case File'
    __name__ = 'citizen_guard.case_file'
    _states_draft = {
        'readonly': ~(Eval('state') == 'draft'),
        'required': (Eval('state') == 'draft')
    }
    _states_processing = {
        'readonly': ~(Eval('state') == 'processing')
    }
    _depends = ['state']

    company = fields.Many2One('company.company', 'Empresa',
        states={
            'readonly': True
        }, required=True)
    number = fields.Char('Número', readonly=True)
    station = fields.Many2One('citizen_guard.station', 'Comisaría',
        states=_states_draft, depends=_depends)
    opening_date = fields.Date('Fecha de apertura', readonly=True)
    open_by = employee_field('Abierto por')
    close_date = fields.Date('Fecha de cierre', readonly=True)
    closed_by = employee_field('Cerrado por')
    state = fields.Selection([
        ('draft', 'En borrador'),
        ('on_citation', 'En citación'),
        ('on_appearance', 'En comparecencia'),
        ('on_transaction', 'En acta de transacción'),
        ('on_technical_reports', 'En informes tecnicos'),
        ('on_notification', 'En notificaciones'),
        ('on_resolution', 'En resolución'),
        ('on_impugnation', 'En impugnación'),
        ('closed', 'Cerrado')
    ], 'Estado', readonly=True)
    articles_involved = fields.One2Many(
        'citizen_guard.case_file.article_involved', 'case_file',
        'Artículos Implicados',
        states=_states_draft, depends=_depends)
    # 1
    origin = fields.Many2One('citizen_guard.case_file.origin',
        'Origen del expediente', states=_states_draft,
        depends=_depends)
    is_complaint = fields.Boolean(
        'Es una denuncia?', states={
            'readonly': ~(Eval('state') == 'draft')
        },
        depends=_depends)
    complaint = fields.Many2One('citizen_guard.complaint', 'Denuncia',
        states={
            'readonly': (~(Eval('state') == 'draft') |
                ~(Eval('is_complaint'))),
            'required': ((Eval('state') == 'draft') &
                (Eval('is_complaint'))),
        }, depends=['state', 'is_complaint'])
    is_guard_report = fields.Boolean('Es un parte de un Agente Municipal?',
        states={
            'readonly': ~(Eval('state') == 'draft')
        }, depends=_depends)
    guard_report = fields.Many2One('citizen_guard.guard_report',
        'Parte de Agente Municipal',
        states={
            'readonly': (~(Eval('state') == 'draft') |
                ~(Eval('is_guard_report'))),
            'required': ((Eval('state') == 'draft') &
                (Eval('is_guard_report'))),
        },
        depends=['state', 'is_guard_report'])
    # 2
    citation_file = fields.Binary('Documento de citación',
        filename='citation_filename', file_id='citation_path',
        states={
            'readonly': ~(Eval('state') == 'on_citation')
        }, depends=_depends)
    citation_filename = fields.Char('Nombre del Archivo', readonly=True)
    citation_path = fields.Char('Ubicación del archivo', readonly=True)
    # 3
    appearance_lines = fields.One2Many('citizen_guard.appearance.line',
        'case_file', 'Detalle de comparecencia',
        states={
            'readonly': ~(Eval('state') == 'on_appearance')
        }, depends=_depends)
    pleaded_guilty = fields.Boolean('El denunciado se declaró culpable?',
        states={
            'readonly': ~(Eval('state') == 'on_appearance')
        }, depends=_depends)
    # transition_file = fields.Binary('Acta de transición',
    #     filename='transition_filename', file_id='transition_path',
    #     states={
    #         'readonly': ~(Eval('state') == 'processing'),
    #         'invisible': ~(Bool(Eval('pleaded_guilty'))),
    #         'required': Bool((Eval('pleaded_guilty'))),
    #     }, depends=['state', 'pleaded_guilty'])
    # transition_filename = fields.Char('Nombre del Archivo', readonly=True)
    # transition_path = fields.Char('Ubicación del archivo', readonly=True)
    # 4
    transaction_certificate = fields.Binary('Acta de transacción',
        filename='transaction_certificate_filename',
        file_id='transaction_certificate_path',
        states={
            'readonly': ~(Eval('state') == 'on_transaction')
        }, depends=_depends)
    transaction_certificate_filename = fields.Char(
        'Nombre del Archivo', readonly=True)
    transaction_certificate_path = fields.Char(
        'Ubicación del archivo', readonly=True)
    compromise_certificate = fields.Binary('Acta de compromiso',
        filename='compromise_certificate_filename',
        file_id='compromise_certificate_path',
        states={
            'readonly': ~(Eval('state') == 'on_transaction')
        }, depends=_depends)
    compromise_certificate_filename = fields.Char(
        'Nombre del Archivo', readonly=True)
    compromise_certificate_path = fields.Char(
        'Ubicación del archivo', readonly=True)
    # 5
    technical_reports = fields.One2Many('citizen_guard.technical_report.line',
        'case_file', 'Informe(s) Técnico(s)',
        states={
            'readonly': ~(Eval('state') == 'on_technical_reports')
        }, depends=_depends)
    aditional_documents = fields.One2Many('citizen_guard.aditional.document',
        'case_file', 'Documento(s) adicionales',
        states={
            'readonly': ~(Eval('state') == 'on_technical_reports')
        }, depends=_depends)
    # 6
    notification_documents = fields.One2Many('citizen_guard.notification.document',  # noqa
        'case_file', 'Notificación(es)',
        states={
            'readonly': ~(Eval('state') == 'on_notification')
        }, depends=_depends)
    # 7
    resolution_file = fields.Binary('Documento de resolución',
        filename='resolution_filename', file_id='resolution_path',
        states={
            'readonly': ~(Eval('state') == 'on_resolution')
        }, depends=_depends)
    resolution_filename = fields.Char('Nombre del Archivo', readonly=True)
    resolution_path = fields.Char('Ubicación del archivo', readonly=True)
    # 8
    impugnation_lines = fields.One2Many('citizen_guard.impugnation.line',
        'case_file', 'Documento(s) de impugnación',
        states={
            'readonly': ~(Eval('state') == 'on_impugnation')
        }, depends=_depends)

    @classmethod
    def __setup__(cls):
        super(CaseFile, cls).__setup__()
        cls._transitions |= set((
            ('draft', 'on_citation'),
            ('on_citation', 'on_appearance'),
            ('on_appearance', 'on_transaction'),
            ('on_appearance', 'on_technical_reports'),
            ('on_technical_reports', 'on_notification'),
            ('on_notification', 'on_resolution'),
            ('on_resolution', 'on_impugnation'),
            ('on_transaction', 'closed'),
            ('on_impugnation', 'closed'),
            ('closed', 'on_impugnation'),
            ('on_citation', 'draft'),
            ('on_appearance', 'draft'),
            ('on_technical_reports', 'draft'),
            ('on_notification', 'draft'),
            ('on_resolution', 'draft'),
            ('on_transaction', 'draft'),
            ('on_impugnation', 'draft'),
            ('closed', 'draft')
        ))

        cls._buttons.update(
            {
                'draft': {
                    'invisible': Eval('state').in_(['draft']),
                },
                'on_citation': {
                    'invisible': ~Eval('state').in_(['draft']),
                },
                'on_appearance': {
                    'invisible': ~Eval('state').in_(['on_citation']),
                },
                'on_transaction': {
                    'invisible': (~Eval('state').in_(['on_appearance']) |
                        ~Bool(Eval('pleaded_guilty'))),
                },
                'on_technical_reports': {
                    'invisible': (~Eval('state').in_(['on_appearance']) |
                        Bool(Eval('pleaded_guilty'))),
                },
                'on_notification': {
                    'invisible': ~Eval('state').in_(['on_technical_reports']),
                },
                'on_impugnation': {
                    'invisible': ~Eval('state').in_(['on_resolution']),
                },
                'on_resolution': {
                    'invisible': ~Eval('state').in_(['on_notification']),
                },
                'close': {
                    'invisible': (~Eval('state').in_(
                        ['on_transaction', 'on_resolution'])),
                },
                'notify_departments': {
                    'readonly': ~Eval('state').in_(['on_technical_reports']),
                },
            })
        cls._error_messages.update({
            'not_employee': ('El usuario actual no tiene un empleado '
                'configurado'),
            'no_sequence': ('No existe una secuencia cofigurada para los '
                'expedientes de la comisaría seleccionada.')
        })

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def set_number(cls, cases):
        '''
        Fill the number field with the station case sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('citizen_guard.configuration')
        config = Config(1)
        for case in cases:
            if not config.case_file_sequence:
                cls.raise_user_error('no_sequence')
            if case.number:
                continue
            case.number = Sequence.get_id(
                config.case_file_sequence.sequence.id
            )
        cls.save(cases)

    def get_rec_name(self, name):
        if self.number and self.origin:
            return self.number + ': ' + self.origin.name
        else:
            return self.origin.name

    @classmethod
    @ModelView.button
    @Workflow.transition('on_citation')
    @set_employee('open_by')
    def on_citation(cls, cases):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for case in cases:
            case.opening_date = today
        cls.set_number(cases)
        # cls.send_email(cls, cases)
        super(CaseFile, cls).save(cases)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_appearance')
    def on_appearance(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_transaction')
    def on_transaction(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_technical_reports')
    def on_technical_reports(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_notification')
    def on_notification(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_resolution')
    def on_resolution(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('on_impugnation')
    def on_impugnation(cls, cases):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    @set_employee('closed_by')
    def close(cls, cases):
        pool = Pool()
        Date = pool.get('ir.date')
        today = Date.today()
        User = pool.get('res.user')
        user = User(Transaction().user)
        if not user.employee:
            cls.raise_user_error('not_employee')
        for case in cases:
            case.close_date = today
        # cls.send_email(cls, cases)
        super(CaseFile, cls).save(cases)

    @classmethod
    @ModelView.button
    def notify_departments(cls, cases):
        for case in cases:
            for report in case.technical_reports:
                if not report.file:
                    # TODO: remove print statement
                    print('Enviando alerta a ' + report.department.name)

    @classmethod
    def view_attributes(cls):
        return super(CaseFile, cls).view_attributes() + [(
            '//page[@id="technical_reports_page"]', 'states', {
                'invisible': Bool(Eval('pleaded_guilty')), }), (
            '//page[@id="notification_page"]', 'states', {
                'invisible': Bool(Eval('pleaded_guilty')), }), (
            '//page[@id="resolution_page"]', 'states', {
                'invisible': Bool(Eval('pleaded_guilty')), }), (
            '//page[@id="impugnation_page"]', 'states', {
                'invisible': Bool(Eval('pleaded_guilty')), }), (
            '//page[@id="transaction_certificate_page"]', 'states', {
                'invisible': ~Bool(Eval('pleaded_guilty')), })
        ]


class CaseFileOrigin(ModelSQL, ModelView):
    "Case File Origin"
    __name__ = 'citizen_guard.case_file.origin'
    name = fields.Char('Nombre')


class FileLineMixin(object):
    case_file = fields.Many2One('citizen_guard.case_file', 'Expediente')
    description = fields.Char('Descripción')
    file = fields.Binary('Documento',
        filename='filename', file_id='path')
    filename = fields.Char('Nombre del Archivo', readonly=True)
    path = fields.Char('Ubicación del archivo', readonly=True)


class AppearanceLine(FileLineMixin, ModelSQL, ModelView):
    'Appearance Line'
    __name__ = 'citizen_guard.appearance.line'


class TechnicalReportLine(FileLineMixin, ModelSQL, ModelView):
    'Technical Report Line'
    __name__ = 'citizen_guard.technical_report.line'
    department = fields.Many2One('company.department', 'Departamento',
        required=True)
    employee = fields.Many2One('company.employee', 'Empleado', required=True,
        domain=[
            ('contract_department', '=', Eval('department'))
        ], depends=['department']
    )
    request_file = fields.Binary('Solicitud de informe técnico',
        filename='request_filename', file_id='request_path')
    request_filename = fields.Char('Nombre del Archivo', readonly=True)
    request_path = fields.Char('Ubicación del archivo', readonly=True)


class ImpugnationLine(FileLineMixin, ModelSQL, ModelView):
    'Impugnation Line'
    __name__ = 'citizen_guard.impugnation.line'


class AditionalDocument(FileLineMixin, ModelSQL, ModelView):
    'Aditional Document'
    __name__ = 'citizen_guard.aditional.document'


class NotificationDocument(FileLineMixin, ModelSQL, ModelView):
    'Notification Document'
    __name__ = 'citizen_guard.notification.document'
