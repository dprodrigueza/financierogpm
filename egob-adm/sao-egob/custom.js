Sao.config.title = '';
Sao.config.icon_color = '#ffffff';
moodledomain = 'http://www.planerp.ec/virtualtrain';
moodletoken = '28119f5d3fe0b440caf9655713efe50d';
moodleuser = 'emacep_uk_1tebq4vpv7';
moodleemail = 'info3@emac.gob.ec';
router = 'https://siim.emac.gob.ec:8082';
enable_gestion_documental = false
url_gestion_documental = 'http://181.39.99.73:80'
/*
// HIDE OR NOT PREFERENCES AND LOGOUT
$(window).on('load', function() {
    jQuery('#user-preferences').hide();
    jQuery('#user-logout').hide();
});
*/

/*
// CAS Section
backup_dialog = Sao.Session.login_dialog;

var session_database = 'siim';
var cas_login_url = 'https://siim.daule.gob.ec:8443/cas/login/?service=';
var cas_logout_url = 'https://siim.daule.gob.ec:8443/cas/logout';
var siim_adm_url = 'http://siim.daule.gob.ec:8000/' + '#' + session_database;

Sao.Session.login_dialog = function() {
    var login_dialog = backup_dialog();
    var ticket_split = window.location.toString().split("ticket=");
    if (ticket_split.length != 2) {
        window.location.replace(cas_login_url+siim_adm_url);
    }
    else {
        login_dialog.database_input[0].value = session_database;
        login_dialog.login_input[0].value = ticket_split[1];
        login_dialog.content.hide();
        window.setTimeout(function() {
            login_dialog.button.click();
        }.bind(login_dialog), 1000);
        return login_dialog;
    }

    // GESTOR DOCUMENTAL

    if(enable_gestion_documental){
        jQuery('#title').after( '\
                <div class="navbar-option-div">\
                    <a class="navbar-option" href="'+url_gestion_documental+'">\
                        <img src="../images/tryton-public.svg" alt="Gestor documental">\
                        Gestión documental\
                    </a>\
                </div>\
        ');
    }
};

Sao.logout = function() {
    var session = Sao.Session.current_session;
    Sao.Tab.tabs.close(true).done(function() {
        jQuery('#user-preferences').children().remove();
        jQuery('#user-logout').children().remove();
        jQuery('#user-favorites').children().remove();
        jQuery('#global-search').children().remove();
        jQuery('#menu').children().remove();
        session.do_logout();
        window.location.replace(cas_logout_url);
    });
};
*/


function cargarMenuLateral(){
        path  = window.location.href;
        if(path.search("model")==-1){
            jQuery('.row-offcanvas').toggleClass('active');
        }
}

preferences = Sao.preferences;
backup_menu = Sao.menu;
Sao.menu = function(preferences) {
    var backup = backup_menu(preferences);
    cargarMenuLateral();
};
