const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3001
const db = require('./queries')
var cors = require('cors');
const geojson = require('geojson');

app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, }));

app.get('/location/:id', db.getPoints)
app.get('/fence/:id', db.getFence)


app.listen(port, () => {
    console.log('app run')
})
