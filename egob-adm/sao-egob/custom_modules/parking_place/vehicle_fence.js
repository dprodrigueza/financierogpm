Tryton.VehicleFence = function(screen, record, mymap) {
    this.screen = screen;
    this.record = record;
    this.mymap = mymap;
    var functions = {
        initialize: function(screen, record, mymap) {
            var id = record.id;
            var name = screen.model.fields.name.get_client(record);
            var contains_vehicle = screen.model.fields.contains_vehicle.get_client(record);
            console.log('contains vehicle ' + contains_vehicle);

            var vehicle_fence_filter = {
                filter: new L.Filter.EQ().append('name', name),
                crs: L.CRS.EPSG4326,
                showExisting: true,
                geometryField: 'geofence',
                url: 'http://'+window.CONFIG.GEOSERVER+'/geoserver/'+window.CONFIG.GEOSERVER_NAME_SPACE+'/wfs',
                typeNS: window.CONFIG.GEOSERVER_NAME_SPACE,
                typeName: 'parking_vehicle_fence',
                // opacity: 0.8,
                fillOpacity: 0.0,
                style: function(layer) {
                    return {
                        color: 'blue',
                        weight: 2
                    };
                },
            };
            var fence = new L.WFST(vehicle_fence_filter).addTo(mymap).bindPopup(
                function(e) {
                    e.feature.properties.name
                    e.feature.properties.fence
                    e.feature.properties.contains_vehicle
                    return '<b> Cerca: ' + name + '<br> id: ' + id + '<b><br> Vehículo:' + contains_vehicle;
                }, {
                    'maxWidth': '200'
                }
            );
            var drawControl = new L.Control.Draw({
                draw: {
                    circle: false,
                    circlemarker: false,
                    rectangle: false,
                },
                edit: {
                    featureGroup: fence
                }
            });
            mymap.addControl(drawControl);

            ///// Add the Overview to the map minimap
            var osm2 = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
            var miniMap = new L.Control.MiniMap(osm2, { toggleDisplay: true }).addTo(mymap);
            //Add to find
            var osmGeocoder = new L.Control.OSMGeocoder();
            mymap.addControl(osmGeocoder);
            ///// Add the scale control to the map
            L.control.scale().addTo(mymap);
            ///// Add the Navigation Bar to the map history
            L.control.navbar({ position: 'topleft' }).addTo(mymap);
            ///// Add the mouse position to the map 
            L.control.mousePosition().addTo(mymap);
            mymap.on('draw:created', function(e) {
                var layer = e.layer;
                feature = layer.feature = layer.feature || {};
                feature.type = feature.type || "Feature";
                var atribute = feature.properties = feature.properties || {};
                atribute.name = null;
                //var coordinates = geom.getCoordinates();
                //console.log(coordinates);
                fence.addLayer(layer);
                addPopup(layer);
                // console.log(layer.getLatLngs())
            });
            mymap.on('draw:edited', function(e) {
                var layers = e.layers;
                layers.eachLayer(function(layer) {
                    fence.editLayer(layer);
                    saveFence;
                });
            });
            mymap.on('draw:deleted', function(e) {
                var layers = e.layers;
                layers.eachLayer(function(layer) {
                    fence.deleteLayer(layer);
                });
            });
            var saveFence = L.easyButton('fa-save', function() {
                fence.save();
            }, 'Guardar Cambios').addTo(mymap);

            function addPopup(layer) {
                var content = document.createElement("textarea");
                content.addEventListener("keyup", function() {
                    layer.feature.properties.name = content.value;
                });
                layer.on("popupopen", function() {
                    content.value = layer.feature.properties.name;
                    content.focus();
                });
                layer.bindPopup(content).openPopup();
            }
            //Fond  de base
            var OpenStreetMap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
            var WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');
            /////layers de base		
            var baseLayers = {
                "Open Street Map": OpenStreetMap,
                "World Imagery": WorldImagery
            };

            var all_vehicles = new L.WFS({
                crs: L.CRS.EPSG4326,
                showExisting: true,
                geometryField: 'point',
                url: 'http://'+window.CONFIG.GEOSERVER+'/geoserver/'+window.CONFIG.GEOSERVER_NAME_SPACE+'/wfs',
                typeNS: window.CONFIG.GEOSERVER_NAME_SPACE,
                typeName: 'parking_vehicle_vehicle',
            }).addTo(mymap);


            var vehicle_info = new L.WFST(all_vehicles).addTo(mymap).bindPopup(
                function(e) {
                    e.feature.properties.plaque
                    e.feature.properties.mark
                    return '<b> Vehicle: ' + plaque + '</b><br> ' + mark;
                }, {
                    'maxWidth': '200'
                }
            );

            ///// Groupe layers
            var overlays = {
                //"Cercas": all_fences,
                "vehículos": all_vehicles
            };

            ///// Ajout des couches de base + couches geoserver
            L.control.layers(baseLayers, overlays).addTo(mymap);

            function alert() {
                if (contains_vehicle !== true) {
                    alert("Vehículo fuera de la cerca");
                }
            }


            ///////// slide menu
            var div = L.DomUtil.create('div', 'info-legend');
            var fences = 'Geocercas';
            contents1 = div.innerHTML = '<br><br>';

            // var vehicles = 'Vehículos';
            // contents2 = div.innerHTML = '<br><img src="bower_components/images/check.png" </img><br>';
            // var slideMenu = L.control.slideMenu('', { position: 'topright', delay: '5' }).addTo(mymap);
            // slideMenu.setContents(fences + contents1 + vehicles + contents2);



        },
    }
    functions.initialize(this.screen, this.record, this.mymap);
};
