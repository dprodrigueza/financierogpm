var realtime = null

Tryton.Vehicle = function(screen, record, mymap) {
    document.getElementById('alert_message').innerHTML = "<div id='vehicle_state' class='blue-map-alert'>El vehículo se encuentra fuera de la geocerca</div>";
    this.screen = screen;
    this.record = record;
    this.mymap = mymap;
    var functions = {
        initialize: function(screen, record, mymap) {
            var id = record.id;
            // console.log('id' + id)
            var plaque = undefined;
            var mark = undefined;
            var model = undefined;
            var fence = undefined;
            var latitude = undefined;
            var longitude = undefined;
            var vehicle_name = undefined;

            if(screen.model.fields.plaque != undefined){
                plaque = screen.model.fields.plaque.get_client(record);
                mark = screen.model.fields.mark.get_client(record);
                model = screen.model.fields.model.get_client(record);
                fence = screen.model.fields.fence.get_client(record);

                url = 'http://'+window.CONFIG.NODE_SERVER+'/location/' + id;
                var urlFence = 'http://'+window.CONFIG.NODE_SERVER+'/fence/' + id;
                var insideFence = '';
                var vehicle_filter = {
                    filter: new L.Filter.EQ().append('plaque', plaque),
                    crs: L.CRS.EPSG4326,
                    showExisting: true,
                    geometryField: 'point',
                    url: 'http://'+window.CONFIG.GEOSERVER+'/geoserver/'+window.CONFIG.GEOSERVER_NAME_SPACE+'/wfs',
                    typeNS: window.CONFIG.GEOSERVER_NAME_SPACE,
                    typeName: 'parking_vehicle_vehicle',
                };

                // console.log(fence);
                var vehicle = new L.WFST(vehicle_filter).addTo(mymap).bindPopup(
                    function(e) {
                        e.feature.properties.plaque
                        e.feature.properties.fence
                        return '<b> Vehículo: ' + plaque + '</b><br> ' + mark + '</b><br> ' + model + '<b> <br> Cerca:' + fence;
                    }, {
                        'maxWidth': '200'
                    }
                );

                //stop try catch
                    

                //corres

                console.log("Actual realtime state")
                console.log(realtime)
                if(realtime != null){
                    realtime.stop()
                }
                realtime = L.realtime(url, {
                    interval: 9 * 1000
                }).addTo(mymap);
                realtime.on('update', function() {
                    mymap.fitBounds(realtime.getBounds());
                    getFence();
                
                });
                url = '';
                // console.log('------>')
            }

            if(screen.model.fields.latitude != undefined){
                latitude = screen.model.fields.latitude.get_client(record);
                longitude = screen.model.fields.longitude.get_client(record);
                vehicle_name = screen.model.fields.vehicle_name.get_client(record);
                L.marker([latitude,longitude]).addTo(mymap).bindPopup(
                    function(e) {
                        return '<b> Activo: ' + vehicle_name + '</b>';
                    }, {
                        'maxWidth': '200'
                    }
                );;
            }

            
            console.log(latitude + " ///// " + longitude)


            async function getFence() {
                axios.get(urlFence)
                    .then(function(res) {
                        // console.log(urlFence);
                        insideFence = (res.data);
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .then(function() {});

                // console.log(insideFence);
                if (insideFence === true) {
                    // console.log('dentro');
                    $('#vehicle_state').removeClass('red-map-alert')
                    $('#vehicle_state').addClass('blue-map-alert')
                    $('#vehicle_state').text('El vehículo se encuentra dentro de la geocerca');

                } else if (insideFence !== true) {
                    $('#vehicle_state').removeClass('blue-map-alert')
                    $('#vehicle_state').addClass('red-map-alert')
                    $('#vehicle_state').text('El vehículo se encuentra fuera  de la geocerca');
                }

            }

            // function alertFence() {
            //     alert("Vehiculo salio");

            // }

            function notificationme() {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn"
                };
                toastr.warning('MY MESSAGE!');
            }




        }

    }
    functions.initialize(this.screen, this.record, this.mymap);
};
