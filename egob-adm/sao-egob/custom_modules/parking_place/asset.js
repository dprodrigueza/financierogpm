var realtime = null

Tryton.Asset = function(screen, record, mymap) {
    this.screen = screen;
    this.record = record;
    this.mymap = mymap;
    var functions = {
        initialize: function(screen, record, mymap) {
            var id = record.id;
            var latitude = undefined;
            var longitude = undefined;
            var name = undefined;
            var code = undefined;

            if(screen.model.fields.latitude != undefined){
                latitude = screen.model.fields.latitude.get_client(record);
                longitude = screen.model.fields.longitude.get_client(record);
                name = screen.model.fields.name.get_client(record);
                code = screen.model.fields.code.get_client(record);
                L.marker([latitude,longitude]).addTo(mymap).bindPopup(
                    function(e) {
                        return '<b> Activo: </b>' + name + '</br><b> Código: </b>' + code;
                    }, {
                        'maxWidth': '200'
                    }
                );;
            }

        }

    }
    functions.initialize(this.screen, this.record, this.mymap);
};
