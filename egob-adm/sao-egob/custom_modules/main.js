(function (window, document, undefined) {
    var oldTryton = window.Tryton,
    Tryton = {};

    // define Tryton as a global Tryton variable, saving the original Tryton to restore later if needed
    Tryton.noConflict = function () {
        window.Tryton = oldTryton;
        return this;
    };

    window.Tryton = Tryton;
}(window, document));