/* This file is part of Tryton.  The COPYRIGHT file at the top level of
   this repository contains the full copyright notices and license terms. */
(function() {
    'use strict';

    Sao.View.Map = Sao.class_(Sao.View, {
        /* Fullmap works with utc date, the default week start day depends on
           the user language, the events dates are handled by moment object. */
        init: function(screen, xml) {
            Sao.View.Map._super.init.call(this, screen, xml);
            // Used to check if the events are still processing
            this.processing = true;
            this.view_type = 'map';
            this.el = jQuery('<div/>', {
                'id': 'map-container'
            });
            var lang = Sao.i18n.getlang();
            lang = lang.slice(0, 2);

            this.fields = [];
            xml.find('map').children().each(function(pos, child) {
                if (child.tagName == 'field') {
                    this.fields.push(child.attributes.name.value);
                }
            }.bind(this));
        },
        get_colors: function(record) {
            var colors = {};
            colors.text_color = 'black';
            if (this.attributes.color) {
                colors.text_color = record.field_get(
                    this.attributes.color);
            }
            colors.background_color = 'lightblue';
            if (this.attributes.background_color) {
                colors.background_color = record.field_get(
                    this.attributes.background_color);
            }
            return colors;
        },
        display: function() {

            // RECUPERAR TODOS LOS RECORDS QUE HAYAN SIDO BUSCADOS DESDE EL TREE

            // var record = this.screen.current_record;
            // var record_id = record.id;
            // console.log(record);
            // document.getElementById('map-container').innerHTML = "<div id='mapid' style='dislpay='block';'></div>";
            // var mymap = new L.map(
            //     'mapid', {editable: true}
            //     ).setView(
            //         [-2.9134324, -79.0306025,15],
            //         13);
            // var layer = new L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            //     maxZoom: 18,
            //     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            //         '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            //         'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            //     id: 'mapbox.streets'
            // });
            // mymap.addLayer(layer);

            // this.processing = true;
            // var prm = jQuery.when();
            // this.events =  [];
            // var promisses = [];
            // prm.then(function()  {
            //     this.screen.group.forEach(function(record) {
            //         var record_promisses = [];
            //         this.fields.forEach(function(name) {
            //         record_promisses.push(record.load(name));
            //         });
            //         var prm = jQuery.when.apply(jQuery, record_promisses).then(
            //             function(){
            //                 this.insert_event(record, mymap);
            //             }.bind(this));
            //         promisses.push(prm);
            //     }.bind(this));
            //     return jQuery.when.apply(jQuery, promisses).then(function() {
            //         // callback(this.events);
            //     }.bind(this)).always(function() {
            //         this.processing = false;
            //     }.bind(this));
            // }.bind(this));


            var record = this.screen.current_record;
            document.getElementById('map-container').innerHTML = "<div id='mapid' style='dislpay='block';'></div><div id='alert_message' style='dislpay='block';'></div>";

            var mymap = new L.map(
                'mapid', {
                    editable: true,
                    fullscreenControl: true,
                    fullscreenControlOptions: {
                        position: 'topleft'
                    }
                }
            ).setView(
                [-2.9134324, -79.0306025, 13],
                9);
            var layer = new L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 15,
                // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                //     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                //     'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox.streets'
            });

            mymap.addLayer(layer);



            this.insert_event(record, mymap);

        },
        get_events: function(start, end, timezone, callback) {

        },
        insert_event: function(record, mymap) {
            if (this.screen.model_name === "parking.vehicle.vehicle") {
                var tryton_vehicle = new Tryton.Vehicle(this.screen, record, mymap);
            } else if (this.screen.model_name === "parking.vehicle.fence") {
                var tryton_vehicleFence = new Tryton.VehicleFence(this.screen, record, mymap);
            } else if (this.screen.model_name === "asset") {
                var tryton_asset = new Tryton.Asset(this.screen, record, mymap);
            }

        },
    });

}());